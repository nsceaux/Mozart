\version "2.19.80"
\include "common.ily"

\opusTitle "K.420 – Per pietà, non ricercate"

\header {
  title = \markup\center-column {
    Rondo
    \italic Per pietà, non ricercate
  }
  subtitle = "für Tenor mit Begleitung des Orchesters"
  opus = "K.420"
  date = "1783"
  copyrightYear = "2018"
}

\includeScore "K.420"

