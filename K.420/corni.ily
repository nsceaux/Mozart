\clef "treble" \transposition mib
r2 |
R1*3 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''4~ do''8 s re''4~ re''8 s |
  }
  \tag #'(corno2 corni) {
    sol1 |
    sol'4~ sol'8 s sol'4~ sol'8 s |
  }
  { s1\f | s4. r8 s4. r8 | }
>>
R1*2 |
r2 \tag #'corni <>^"a 2." sol'8\p\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( mi'8) r8 sol'8\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( do''8) r8 r2 |
R1 |
r2 <<
  \tag #'(corno1 corni) {
    sol'4 sol' |
    sol' s mi''8 s re'' s |
    do'' s re'' s mi'' s fa'' s |
    re''
  }
  \tag #'(corno2 corni) {
    do'4 do' |
    do' s do''8 s sol' s |
    mi' s sol' s do'' s re'' s |
    sol'
  }
  { s2\f | s4 r s8\p r s r | s r s r s r s r }
>> r8 r4 r2 |
R1*5 |
r2 \tag #'corni <>^"a 2." sol'8\p\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( mi'8) r8 sol'8\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( do''8) r8 r2 |
R1*5 |
<<
  \tag #'corno1 { R1 | }
  \tag #'(corno2 corni) {
    \tag #'corni <>^\markup\concat { 2 \super o }
    do'8-.(\mf do'-. do'-. do'-.) do'4 r |
  }
>>
<<
  \tag #'(corno1 corni) {
    \tag #'corni <>^\markup\concat { 1 \super o }
    re''8-.(\mf re''-. re''-. re''-.) re''4 r |
    R1 |
  }
  \tag #'corno2 { R1*2 }
>>
<<
  \tag #'(corno1 corni) {
    sol'8-.( sol'-. sol'-. sol'-.) sol'2~ |
    sol'1~ |
    sol' |
  }
  \tag #'(corno2 corni) {
    sol8-.( sol-. sol-. sol-.) sol2~ |
    sol1~ |
    sol |
  }
  { s2 s\p }
>>
\tag #'corni <>^"a 2." << { s2 s\cresc } re''1~ >> |
re''4\f r r2 |
R1*11 |
r2\fermata <<
  \tag #'(corno1 corni) {
    mi''8 r re'' r |
    do'' s re'' s mi'' s fa'' s |
    re''
  }
  \tag #'(corno2 corni) {
    do''8 s sol' s |
    mi' s sol' s do'' s re'' s |
    sol'
  }
  { s8\p r s r | s r s r s r s r | }
>> r8 r4 r2 |
R1*5 |
r2 \tag #'corni <>^"a 2." sol'8\p\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( mi'8) r8 sol'8\<-.( sol'-. sol'-. sol'-.) |
sol'4\p( do''8) r8 r2 |
<<
  \tag #'corno1 { R1*7 | }
  \tag #'(corno2 corni) {
    R1*5 |
    \tag #'corni <>^\markup\concat { 2 \super o }
    do'8-.(\mf do'-. do'-. do'-.) do'4 r |
    R1 |
  }
>>
<>\mf <<
  \tag #'(corno1 corni) {
    do''8-.( do''-. do''-. do''-.) do''4
  }
  \tag #'(corno2 corni) {
    do'8-.( do'-. do'-. do'-.) do'4
  }
>> r4 |
R1*5 |
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol' }
  \tag #'(corno2 corni) { sol1~ | sol }
>>
R1^\fermataMarkup |
r4 <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4\fermata |
%%
\tag #'corni <>^"a 2." do''4.\f sol'8 do'' sol' do'' sol' |
re'' sol' re'' sol' re''4 r |
<<
  \tag #'(corno1 corni) { do''1 | do'' | do''4. }
  \tag #'(corno2 corni) { do'1 | do' | do'4. }
  { s1\p | s\fp | s4.\f }
>> \tag #'corni <>^"a 2." sol'8 do'' sol' do'' sol' |
re'' sol' re'' sol' re''4 r |
<<
  \tag #'(corno1 corni) {
    do''1 | do'' | do''~ | do'' |
    re''4 sol' sol' sol' | sol'2
  }
  \tag #'(corno2 corni) {
    do'1 | do' | do'~ | do' |
    sol'4 sol sol sol | sol2
  }
  { s1\p | s\fp | s1*2\cresc | s1\f }
>> r2 |
R1 | \allowPageTurn
\tag #'corni <>^"a 2." sol'2(\p mi'4) r |
R1 |
<<
  \tag #'(corno1 corni) { do''2( sol'4) }
  \tag #'(corno2 corni) { do'2( sol4) }
>> r4 |
R1*2 |
<<
  \tag #'(corno1 corni) {
    s4 sol''2( fad''4) |
    s4 fa''!2( mi''4) |
  }
  \tag #'(corno2 corni) {
    s4 mi''2( re''4) |
    s4 re''2( do''4) |
  }
  { r4 s2.\sfp | r4 s2.\sfp | }
>>
R1*2 |
R1\fermataMarkup |
R1\fermataMarkup |
R1 |
\tag #'corni <>^"a 2." sol'2(\p mi'4) r |
R1 |
do''2 ( sol'4 ) r |
R1*2 |
<<
  \tag #'(corno1 corni) {
    s4 sol''2( fad''4) |
    s4 fa''!2( mi''4) |
  }
  \tag #'(corno2 corni) {
    s4 mi''2( re''4) |
    s4 re''2( do''4) |
  }
  { r4 s2.\sfp | r4 s2.\sfp | }
>>
R1*2 |
<<
  \tag #'(corno1 corni) {
    sol'1~ | sol'~ | sol'~ | sol'~ |
    sol'~ | sol'~ | sol'~ | sol'~ |
    sol'4 sol' sol' sol' |
    sol'
  }
  \tag #'(corno2 corni) {
    sol1~ | sol~ | sol~ | sol~ |
    sol~ | sol~ | sol~ | sol |
    do'4 do' do' do' |
    do'
  }
  { \ru#4 { s2\p s\cresc | s2 s\f } s4 s2.\ff }
>> r4\fermataMarkup r2 |
R1*2 |
<<
  \tag #'(corno1 corni) { sol'1~ | sol'4 }
  \tag #'(corno2 corni) { do'1~ | do'4 }
  { s2.\p\< s4\> | s4\! }
>> r4 r2\fermata |
\tag #'corni <>^"a 2." do'2\sf r |
do'2\sf r |
R1*6 |
do'2\sf r |
do'2\sf r |
R1*6 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''4 do'' do'' do'' |
    do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''2 re'' |
    mi''4 s re'' s |
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'4 do' do' do' |
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'2 sol' |
    do''4 s sol' s |
  }
  { s1*2\p | s4 s2.\cresc | s1 | s1\f | s1*4\p | s4 r s r | }
>>
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''4 do'' do'' do'' |
    do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''2 re'' |
    mi''4 s re'' s |
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'4 do' do' do' |
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'2 sol' |
    do''4 s sol' s |
  }
  { s1*2 | s4 s2.\cresc | s1 | s1\f | s1*4\p | s4 r s r | }
>>
\ru#2 {
  <<
    \tag #'(corno1 corni) { mi''2~ mi''4 }
    \tag #'(corno2 corni) { do''2~ do''4 }
    { s2\f s4\p }
  >> r4 |
  R1 |
}
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | re'' | do''~ | do'' | }
  { do''1~ | do''2 sol' | do'1~ | do' | }
  { s1 | s\cresc | s\f }
>>
<<
  \tag #'(corno1 corni) {
    do''4 sol' sol' sol' |
    sol' s sol' s |
    sol'2
  }
  \tag #'(corno2 corni) {
    do'4 do' do' do' |
    do' s do' s |
    do'2
  }
  { s1 | s4 r s r | }
>> r2\fermata |
