\clef "bass" r2 |
R1*3 |
<<
  \tag #'(fagotto1 fagotti) {
    sib2( lab) |
    sol4~ sol8 s fa4~ fa8 s |
  }
  \tag #'(fagotto2 fagotti) {
    sib,2( lab,) |
    sol,4~ sol,8 s fa,4~ fa,8 s |
  }
  { s1\f | s4. r8 s4. r8 | }
>>
R1*2 |
r2 <<
  \tag #'(fagotto1 fagotti) {
    sib8-.( sib-. sib-. sib-.) |
    sib4( sol8) s sib8-.( sib-. sib-. sib-.) |
    sib4( mib'8)
  }
  \tag #'(fagotto2 fagotti) {
    sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( sol,8) s sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( mib8)
  }
  { s4.\p\< s8\! |
    s4.\p r8 s4.\< s8\! |
    s4\p }
>> r8 r2 |
R1 |
r2 <>\f <<
  \tag #'(fagotto1 fagotti) { mib'4 mib' | mib' }
  \tag #'(fagotto2 fagotti) { sol sol | sol }
>> r4 r2 |
R1*7 |
r2 <<
  \tag #'(fagotto1 fagotti) {
    sib8-.( sib-. sib-. sib-.) |
    sib4( sol8) s sib8-.( sib-. sib-. sib-.) |
    sib4( mib'8)
  }
  \tag #'(fagotto2 fagotti) {
    sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( sol,8) s sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( mib8)
  }
  { s4.\p\< s8\! |
    s4.\p r8 s4.\< s8\! |
    s4\p }
>> r8 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1*7 |
    \tag #'fagotti <>^\markup\concat { 1 \super o }
    fa8-.(\mf fa-. fa-. fa-.) fa4 r |
    R1 |
  }
  \tag #'fagotto2 {
    R1*9 |
  }
>>
<>\p <<
  \tag #'(fagotto1 fagotti) { mi'2( mib' | re' reb') | do'4 }
  \tag #'(fagotto2 fagotti) { sol2( solb | fa mi) | fa4 }
>> r4 r2 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'1~ | fa'4 }
  { fa'2( mib'!) | re'!4 }
  { s1\f | s4\p }
>> r4 r2 |
R1*10 |
r2\fermata r |
R1*7 |
r2 <<
  \tag #'(fagotto1 fagotti) {
    sib8-.( sib-. sib-. sib-.) |
    sib4( sol8) s sib8-.( sib-. sib-. sib-.) |
    sib4( mib'8)
  }
  \tag #'(fagotto2 fagotti) {
    sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( sol,8) s sib,8-.( sib,-. sib,-. sib,-.) |
    sib,4( mib8)
  }
  { s4.\p\< s8\! | s4.\p r8 s4.\< s8\! | s4\p }
>> r8 r2 |
R1*9 |
<>\mf <<
  \tag #'(fagotto1 fagotti) {
    do'8-.( do'-. do'-. do'-.) do'4
  }
  \tag #'(fagotto2 fagotti) {
    do8-.( do-. do-. do-.) do4
  }
>> r4 |
R1*5 |
R1^\fermataMarkup |
r4 <>\f <<
  \tag #'(fagotto1 fagotti) { sib4 sib }
  \tag #'(fagotto2 fagotti) { sib, sib, }
>> r4\fermata |
%%
\tag #'fagotti <>^"a 2." mib4.\f sib,8 mib sib, mib sib, |
fa sib, fa sib, fa4 r |
R1 |
<<
  \tag #'(fagotto1 fagotti) { fa'1 | mib'4. }
  \tag #'(fagotto2 fagotti) { sib1 | mib4. }
  { s1\fp | s4.\fp }
>> \tag #'fagotti <>^"a 2." sib,8 mib sib, mib sib, |
fa sib, fa sib, fa4 r |
R1 |
<>\fp <<
  \tag #'(fagotto1 fagotti) { fa'1 }
  \tag #'(fagotto2 fagotti) { sib }
>>
R1 |
r2 <>\f <<
  \tag #'(fagotto1 fagotti) {
    mib'2 |
    re'4 sib sib sib |
    sib2
  }
  \tag #'(fagotto2 fagotti) {
    la2 |
    sib4 sib, sib, sib, |
    sib,2
  }
>> r2 |
R1 | \allowPageTurn
<>\p <<
  \tag #'(fagotto1 fagotti) { sib2( sol4) }
  \tag #'(fagotto2 fagotti) { sib,2( sol,4) }
>> r4 |
R1 |
<<
  \tag #'(fagotto1 fagotti) { mib'2( sib4) }
  \tag #'(fagotto2 fagotti) { mib2( sib,4) }
>> r4 |
R1*2 |
r4 <<
  \tag #'(fagotto1 fagotti) {
    do'2~ do'4 |
    s4 sib2~ sib4 |
  }
  \tag #'(fagotto2 fagotti) {
    \tag #'fagotti \slurDown sib2( la4) |
    s lab!2( sol4) | \slurNeutral
  }
  { s2.\sfp | r4 s2.\sfp | }
>>
R1*2 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1 |
<>\p <<
  \tag #'(fagotto1 fagotti) { sib2( sol4) }
  \tag #'(fagotto2 fagotti) { sib,2( sol,4) }
>> r4 |
R1 |
<<
  \tag #'(fagotto1 fagotti) { mib'2( sib4) }
  \tag #'(fagotto2 fagotti) { mib2( sib,4) }
>> r4 |
R1*2 |
r4 <<
  \tag #'(fagotto1 fagotti) {
    do'2~ do'4 |
    s4 sib2~ sib4 |
  }
  \tag #'(fagotto2 fagotti) {
    \tag #'fagotti \slurDown sib2( la4) |
    s lab!2( sol4) | \slurNeutral
  }
  { s2.\sfp | r4 s2.\sfp | }
>>
R1*3 |
r2 <>\f <<
  \tag #'(fagotto1 fagotti) { fa'2( | mib'4) }
  \tag #'(fagotto2 fagotti) { lab2( | sol4) }
>> r4 r2 |
r2 <<
  \tag #'(fagotto1 fagotti) { fa'2( | mib'4) }
  \tag #'(fagotto2 fagotti) { lab2( | sol4) }
>> r4 r2 |
r2 <<
  \tag #'(fagotto1 fagotti) { fa'2( | mib'4) }
  \tag #'(fagotto2 fagotti) { lab2( | sol4) }
>> r4 r2 |
r2 <<
  \tag #'(fagotto1 fagotti) { fa'2( | mib'4) mib' mib' mib' | mib' }
  \tag #'(fagotto2 fagotti) { lab2( | sol4) sol sol sol | sol }
  { s2 | s4 s2.\ff }
>> r4\fermata r2 |
R1*2 |
\clef "tenor" <<
  \tag #'(fagotto1 fagotti) { sol'1\fermata~ | sol'4 }
  \tag #'(fagotto2 fagotti) { mib'1\fermata~ | mib'4 }
  { s2.\p\< s4\> | s4\! }
>> r4 r2\fermata |
\clef "bass" \ru#2 {
  <>\sf <<
    \tag #'(fagotto1 fagotti) { mib2 }
    \tag #'(fagotto2 fagotti) { mib, }
  >> r2 |
}
R1*6 |
\ru#2 {
  <>\sf <<
    \tag #'(fagotto1 fagotti) { mib2 }
    \tag #'(fagotto2 fagotti) { mib, }
  >> r2 |
}
R1*10 |
<>\f <<
  \tag #'(fagotto1 fagotti) {
    sib1 |
    s2 mib( |
    fa sol |
    lab sib |
    do' re') |
    mib'4 s sib s |
  }
  \tag #'(fagotto2 fagotti) {
    sol1 |
    s2 mib,( |
    fa, sol, |
    lab, sib, |
    do re) |
    mib4 s sib, s |
  }
  { s1 | r2 s\p | s1*3 | s4 r s r | }
>>
R1*4 |
<>\f <<
  \tag #'(fagotto1 fagotti) {
    sib1 |
    s2 mib( |
    fa sol |
    lab sib |
    do' re') |
    mib'4 s sib s |
  }
  \tag #'(fagotto2 fagotti) {
    sol1 |
    s2 mib,( |
    fa, sol, |
    lab, sib, |
    do re) |
    mib4 s sib, s |
  }
  { s1 | r2 s\p | s1*3 | s4 r s r | }
>>
\ru#2 {
  <<
    \tag #'(fagotto1 fagotti) { sib2( do'4) }
    \tag #'(fagotto2 fagotti) { mib2( do4) }
    { s2\f s4\p }
  >> r4 |
  R1 |
}
r2 <<
  \tag #'(fagotto1 fagotti) { do'2( | lab sib) | mib4 }
  \tag #'(fagotto2 fagotti) { do2( | lab, sib,) | mib,4 }
  { s2 | s1\cresc | s4\f }
>> r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'2 | sol' fa' | }
  { sib2~ | sib1 | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    mib'4 mib' mib' mib' |
    mib' s mib' s |
    mib'2
  }
  \tag #'(fagotto2 fagotti) {
    sol4 sol sol sol |
    sol s sol s |
    sol2
  }
  { s1 | s4 r s r | }
>> r2\fermata |
