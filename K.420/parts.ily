\piecePartSpecs
#`((clarinetti #:instrument "in B." #:score-template "score-clarinetti"
                #:tag-global clarinetti)
   (fagotti #:score-template "score-fagotti" #:tag-global all)
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in Es")
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all))
