\clef "alto" sib8\p r sib r |
sol r sib r sib r do' r |
sib r r4 re'8.( mib'16 fa'8) fa'-. |
mib'4 r lab4.( la8) |
sib16-. sib\f( la sib la sib la sib lab sib lab sib lab sib lab sib) |
mib'4:32 mib'8 r fa'4:32 fa'8 r |
sol'32\p sol' sol' sol' sol'8 fa'32 fa' fa' fa' fa'8 fa' r r4 |
mib'4~ mib'8.( do'16) sib4 sib |
sib~ sib8 r r2 |
R1 |
r2 mib'8 r mib' r |
mib'4 r sib4.( lab8) |
sol4 r mib'\f mib' |
mib r mib'8\p r sib r |
sol r sib r sib r do' r |
sib r r4 re'8.( mib'16 fa'8) fa'-. |
mib'2.( do'4) |
sib sib sib r |
mib'4:32\fp mib'8 r fa'4:32\fp fa'8 r |
sol'32\p sol' sol' sol' sol'8 fa'32 fa' fa' fa' fa'8 fa' r r4 |
mib'4~ mib'8.( do'16) sib4 sib |
sib2 r |
R1 |
r2 sol8\p r sol r |
lab r lab r sib r re' r |
re'4( mib') sol8 r sol r |
lab2( sib)~ |
sib1~ |
sib4 r r2 |
r mib'4\p mib' |
fa'4 r r2 |
r fa'4 fa' |
sib4 r sib' sib' |
r8 sib-.( sib-. sib-.) r mib'-.( mib'-. mib'-.) |
r fa'-.( fa'-. fa'-.) r sol'-.( sol'-. sol'-.) |
do'4:16 fa':16\cresc fa':16 sol':16 |
fa'8\f fa'-.( fa'-. fa'-. mib'!-. mib'-. mib' -. mib'-.) |
re'!4\p fa'2~ fa'8([ mib']) |
mib'([ re']) re'2 do'8([ sib]) |
do'1 |
do'2*1/2( s4\cresc la2) |
r8 re'\p re' re' r re' re' re' |
r do' do' do' r do' do' do' |
r re' re' re' r fa'-. fa'( la') |
sol'1 |
r8 fa'( solb' fa' solb' fa' solb' fa') |
r fa' fa' fa' r mib'( do' mib') |
r fa' fa' fa' r fa' fa' fa' |
fa'4 r\fermata mib'8\p r sib r |
sol8 r sib r sib r do' r |
sib r r4 re'8.( mib'16 fa'8) fa'-. |
mib'2.( do'4) |
sib sib sib r |
mib'4:32\fp mib'8 r fa'4:32\fp fa'8 r |
sol'32\p sol' sol' sol' sol'8 fa'32 fa' fa' fa' fa'8 fa' r r4 |
mib'4~ mib'8.( do'16) sib4 sib |
sib2 r |
R1 | \allowPageTurn
r2 sol8\p r sol r |
lab r lab r sib r re' r |
re'4( mib') sol8 r sol r |
lab2( sib)~ |
sib1~ |
sib4 r r2 |
r mib'4 mib' |
mib' r r2 |
r mib'4 mib' |
mi'4 r r2 |
r2 do'4 do' |
do' lab2\cresc lab4~ |
lab8 fa'[\p fa' fa'] fa' fa' fa'( mib') |
r re'-. fa'-. re'-. fa'-. re'-. sib-. re'-. |
mib' r sib r sol r mib r |
mib' r sib r sol r mib r |
do'1\fp\fermata |
r4 sib\f sib r\fermata |
mib'4.\p sib8 mib' sib mib' sib |
fa' sib fa' sib fa'4 r |
sib2:16\p sib:16 |
<sib re'>1\fp |
mib'4.\f sib8 mib' sib mib' sib |
fa' sib fa' sib fa'4 r |
sib2:16\p sib:16 |
<sib re'>1\fp |
sib2:16\p\cresc mib'2:16 |
mib':16 do':16 |
sib4\f sib sib sib |
sib2 r |
R1*4 | \allowPageTurn
<<
  { sol'2( fa' | mib' reb') | } \\
  { mib'(\p reb' | do' sib) | }
>>
r4 sol2(\sfp fa4) |
r4 fa2(\sfp mib4) |
mib\p r lab r |
sib sib sib sib |
<sib re'>1\fermata_( |
<sib mib'>4) r r2\fermata |
R1*4 |
<<
  { sol'2( fa' | mib' reb') | } \\
  { mib'(\p reb' | do' sib) | }
>>
r4 sol2(\sfp fa4) |
r4 fa2(\sfp mib4) |
mib\p r lab r |
sib sib sib sib |
\ru#4 {
  mib'4.\p mi'8( fa'4.)\cresc fad'8( |
  sol'4.) mib'!8( lab')\f fa'!-. re'-. sib-. |
}
mib'4 mib'\ff mib' mib' |
mib' r\fermata r2 |
R1*2 |
R1^\fermataMarkup |
R1^\fermataMarkup |
mib'2:16\sf mib':16\p |
mib':16\sf mib':16\p |
sol2:16\p sol:16 |
sol:16 sol:16 |
lab:16 lab:16 |
lab:16 lab:16 |
<fa lab>:16 q:16 |
q:16 q:16 |
<mib sib>:16\f q:16 |
q:16 q:16 |
do':16\p do':16 |
sib:16 sib:16 |
do':16 do':16 |
do':16 do':16 |
sib:16 sib:16 |
lab:16 lab:16 |
sol4\p r mib' r |
mib' r mib' r |
mib' mib'\cresc mib' mib' |
mib' mib' mib' mib' |
mib'8\f mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
R1*2 |
r2 sib\p |
sib2.( lab4) |
sol r mib' r |
mib' r mib' r |
mib' mib'\cresc mib' mib' |
mib' mib' mib' mib' |
mib'8\f mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
R1*2 |
r2 sib\p |
sib1 |
mib'2:16\f sol'4\p sol' |
r fa' r fa' |
mib'2:16\f sol'4\p sol' |
r fa' r fa' |
mib'2:16 sol':16 |
do'':16\cresc sib':16 |
<mib' sib'>:16\f <lab' sib'>:16 |
<sol' sib'>:16 <lab' sib'>:16 |
<sol' sib'>4 mib' mib' mib' |
mib' r mib' r |
mib'2 r\fermata |
