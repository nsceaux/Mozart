\tag #'clarinetti \key fa \major
\tag #'all \key mib \major
\tempo "Andante" \midiTempo#100
\time 2/2 \partial 2 s2 s1*75 \bar "||"
\tempo "Allegro assai" \midiTempo#160 s1*43 s2
\tempo "Adagio" s2 s1*4
\tempo "Tempo primo" s1*47 \bar "|."
