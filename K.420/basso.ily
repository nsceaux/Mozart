\clef "bass" mib8\p r sib, r |
sol, r sib, r mib r lab, r |
sib,4 r r2 |
R1 |
r2 lab16(\sf sib lab sib lab sib lab sib) |
sol8 sol sol r fa fa fa r |
mi\p r fa r sib, r r4 |
mib! lab, sib, sib, |
mib~ mib8 r r2 |
R1 |
r2 sol8 r sol r |
lab4 r sib8 r sib, r |
mib4 r mib\f mib |
mib r r2 |
r4 sib,8\p r mib r lab, r |
sib, r r4 r2 |
mib2( lab,) |
sib,4 sib, sib, r |
sol8-!\fp sol sol r fa-!\fp fa fa r |
mi\p r fa r sib, r r4 |
mib!4 lab, sib, sib, |
mib2 r |
R1 |
r2 sol8\p r sol r |
lab r lab r sib r sib, r |
do2 sol8 r sol r |
lab r lab4( fa mib) |
sib,8 r sib, r sib, r sib, r |
mib4 r r2 |
r mib4\p mib |
fa4 r r2 |
r2 fa4 fa |
sib, r sib sib |
sol r solb r |
fa r mi r |
fa fa\cresc fa fa |
fa\f r r2 |
sib2(\p fa) |
sol1 |
do2( mib) |
fa2.*1/3( s2\cresc mib4) |
re\p r re r |
mib r mib r |
fa r fa r |
sol1( |
la) |
sib4 r mib r |
fa r fa, r |
sib, r\fermata r2 |
r4 sib,8\p r mib r lab,! r |
sib, r r4 r2 |
mib2( lab,) |
sib,4 sib, sib, r |
sol8-!\fp sol sol r fa-!\fp fa fa r |
mi\p r fa r sib, r r4 |
mib!4 lab, sib, sib, |
mib2 r |
R1 |
r2 sol8\p r sol r |
lab r lab r sib r sib, r |
do2 sol8 r sol r |
lab r lab4( fa mib) |
sib,8 r sib, r sib, r sib, r |
mib4 r r2 |
r mib4 mib |
mib r r2 |
r2 mib4 mib |
mi4 r r2 |
r do4 do |
fa4( mib!\cresc re do) |
sib,\p sib, sib, sib, |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
sib, sib, sib, sib, |
do1\fermata\fp |
r4 sib,\f sib, r\fermata |
%%
mib4.\f sib,8 mib sib, mib sib, |
fa sib, fa sib, fa4 r |
mib8\p mib mib mib mib mib mib mib |
mib1\fp |
mib4.\f sib,8 mib sib, mib sib, |
fa sib, fa sib, fa4 r |
mib8\p mib mib mib mib mib mib mib |
mib1\fp |
mib8\p mib mib\cresc mib mib mib mib mib |
do do do do do do do do |
sib,4\f sib, sib, sib, |
sib,2 r |
R1*4 | \allowPageTurn
mib4\p r mib r |
mib r mib r |
r4 mi2(\sfp fa4) |
r4 re2(\sfp mib!4) |
do4\p r lab, r |
sib, sib, sib, sib, |
mib1\fermata~ |
mib4 r r2\fermata |
R1*4 |
mib4\p r mib r |
mib r mib r |
r mi2(\sfp fa4) |
r re2(\sfp mib!4) |
do4\p r lab, r |
sib, sib, sib, sib, |
mib4.\p mi8( fa4.)\cresc fad8( |
sol4.) mib!8( lab)\f fa!-. re-. sib,-. |
mib4.\p mi8( fa4.)\cresc fad8( |
sol4.) mib!8( lab)\f fa!-. re-. sib,-. |
mib4.\p mi8( fa4.)\cresc fad8( |
sol4.) mib!8( lab)\f fa!-. re-. sib,-. |
mib4.\p mi8( fa4.)\cresc fad8( |
sol4.) mib!8( lab)\f fa!-. re-. sib,-. |
mib4 mib\ff mib mib |
mib r\fermata r2 |
R1*2 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*2 |
mib8(\p fa sol fa mib4) r |
do8( re mib re do4) r |
lab,8( sib, do sib, lab,4) r |
fa,8( sol, lab, sol, fa,4) r |
sib,8( do re do sib,4) r |
sib,8( do re do sib,4) r |
mib8(\sf fa sol fa mib4) r |
mib8(\sf fa sol fa mib4) r |
do8(\p re mib re do4) r |
sol,8( lab, sib, lab, sol,4) r |
lab,8( sib, do sib, lab,4) r |
fa,8( sol, lab, sol, fa,4) r |
sib,8( do re do sib,4) r |
sib,8( do re do sib,4) r |
mib4\p r mib r |
mib r mib r |
mib mib\cresc mib mib |
mib mib mib mib |
mib8\f mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
R1*2 |
r2 re(\p |
mib sib,) |
mib4 r mib r |
mib r mib r |
mib mib\cresc mib mib |
mib mib mib mib |
mib8\f mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
R1*2 |
r2 re(\p |
mib sib,) |
mib'8\f mib' mib' mib' do'4\p r |
lab r sib r |
mib'8\f mib' mib' mib' do'4\p r |
lab4 r sib r |
mib'8 mib' mib' mib' do' do' do' do' |
lab\cresc lab lab lab sib sib sib sib |
mib\f mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
mib4 mib mib mib |
mib r mib r |
mib2 r\fermata |
