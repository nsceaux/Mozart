\clef "treble"
<<
  \tag #'violino1 {
    sol'8\p r fa' r |
    mib' r fa' r sol' r lab' r |
    fa' r r4 sib'-.( sib'-.) |
    sib'8.( do''32 re'' mib''8) re''-. do''-. sib'-. lab'-. sol'-. |
    fa'4 r sib'4..(\sf do''32 re'') |
    mib''4:32 mib''8 r reb''4:32 reb''8 r |
    do''32\p do'' do'' do'' do''8 do''32 do'' do'' do'' do''8 re''! r mib''16( re'' mib'' do'') |
    sib'8.( sol'16) lab'8.( fa'16) mib'4.( fa'8) |
    fa'8([ fad'] sol'8) r r2 |
    R1 |
    r2 mib''16( sol''8.) sib'16( mib''8.) |
    si'16( do''8.) r8 mib''32( do''16.) do''32( sib'!16.) sib'32( lab'16.) lab'32( sol'16.) sol'32( fa'16.) |
    mib'4 r <mib' sib' sol''>4\f <mib' sib' sib''> |
    <mib' sol> r sol'8\p r fa' r |
    mib' r fa' r sol' r lab' r |
    fa' r r4 sib'-.( sib'-.) |
    sib'8.( do''32 re'' mib''8 re'' do'' sib' lab' sol') |
    sib'8.( sol'16) fa'8-. fa'-. fa'4 r |
    mib''4:32\fp mib''8 r reb''4:32\fp reb''8 r |
    do''32\p do'' do'' do'' do''8 do''32 do'' do'' do'' do''8 re''! r mib''16( re'' mib'' do'') |
    sib'8.( sol'16) lab'8.( fa'16) mib'4.( fa'8) |
    fa'4( sol'8) r r2 |
    R1 |
    r2 sib'8\p r sib' r |
    do'' r do'' r fa' r sib' r |
    lab'4( sol') sib'8 r sib' r |
    r do'' do'' do'' re'' re'' mib'' mib'' |
    mib'' r sib' r sol' r fa' r |
    mib'4 r sol'16(\p fad' sol') sol'-. lab'( sol' lab') lab'-. |
    sib'4 r sol''16( fad'' sol'' fa'' mib'' re'' do'' sib') |
    la'4 r do''16( si' do'') do''-. re''( dod'' re'') re''-. |
    mib''4 r do'''16( si'' do''' sib'' la'' sol'' fa'' mib'') |
    re''4 r sib''16( do''' re''' do''' sib'' la'' sol'' fa'') |
    r8 mi''-.( mi''-. mi''-.) r mib''-.( mib''-. mib''-.) |
    r re''-.( re''-. re''-.) r reb''-.( reb''-. reb''-.) |
    do''4:16 do'':16\cresc reb'':16 mi'':16 |
    fa''4\f r r2 |
    re''!2\p~ re''8( fa'' mib''! do'') |
    do''8( sib') sib'4. sib'8( do'' re'') |
    r mib''( re'' mib'') r do''( si' do'') |
    r la'~ la'16(\cresc sib'! do'' sib' do'' re'' mib'' re'' fa'' mib'' do'' la') |
    r8 sib'\p sib' sib' r sib' sib' sib' |
    r sib' sib' sib' r sib' sib' sib' |
    r sib' sib' sib' r do''-. do''( fa'') |
    r4 mi''2. |
    r4 mib''!2. |
    r8 re'' re'' re'' r do''( mib'' do'') |
    r8 sib' sib' sib' r sib'( do'' la') |
    sib'4 r\fermata sol'8\p r fa' r |
    mib'8 r fa' r sol' r lab'! r |
    fa' r r4 sib'-.( sib'-.) |
    sib'8.( do''32 re'' mib''8 re'' do'' sib' lab' sol') |
    sib'8.( sol'16) fa'8-. fa'-. fa'4 r |
    mib''4:32\fp mib''8 r reb''4:32\fp reb''8 r |
    do''32\p do'' do'' do'' do''8 do''32 do'' do'' do'' do''8 re''! r mib''16( re'' mib'' do'') |
    sib'8.( sol'16) lab'8.( fa'16) mib'4.( fa'8) |
    fa'([ fad'] sol') r r2 |
    R1 | \allowPageTurn
    r2 sib'8\p r sib' r |
    do'' r do'' r fa' r sib' r |
    lab'4( sol') sib'8 r sib' r |
    r do'' do'' do'' re'' re'' mib'' mib'' |
    mib'' r sib' r sol' r fa' r |
    mib'4 r sol'16( fad' sol') sol'-. lab'( sol' lab') lab'-. |
    sib'4 r mib''16( re'' mib'' re'' mib'' mi'' fa'' do'') |
    reb''4 r sib'16( la' sib') sib'-. do''( si' do'') do''-. |
    reb''4 r sib''16( la'' sib'' lab'' sol'' fa'' mib'' reb''!) |
    do''4 r sol'16( fad' sol') sol'-. lab'( sol' lab') lab'-. |
    sib'4 r sol''16( fad'' sol'' fa'' mi'' reb'' do'' sib') |
    lab'8 lab''4\cresc sol'' fa'' mib''!8 |
    r8 re''!\p re'' re'' re'' re'' re''( mib'') |
    r fa'' r re'' r sib' r lab' |
    r8 sol'16.( fad'32 sol'8) sol'-. sol'( sib') sib'-. sib'-. |
    r8 sib'16.( la'32 sib'8) sib'-. sib'( mib'') mib''-. mib''-. |
    la'1\fp\fermata |
    r4 <re' sib' fa''>\f <re' sib' sib''> r\fermata |
  }
  \tag #'violino2 {
    mib'8\p r re' r |
    sib r re' r mib' r fa' r |
    re' r r4 fa'8.( sol'16 lab'8) lab' |
    sol'4 r8 sib'-. lab'-. sol'-. fa'-. mib'-. |
    re'16-. sib(\f la sib la sib la sib lab sib lab sib lab sib lab sib) |
    sib'4:32 sib'8 r sib'4:32 sib'8 r |
    sib'32\p sib' sib' sib' sib'8 la'32 la' la' la' la'8 lab' r do'16( sib do' lab) |
    sol8.( sib16) do'8.( lab16) sol4.( re'8) |
    re'4( mib'8) r r2 |
    R1 |
    r2 sib'8 r sib' r |
    lab'4 r8 do''32( lab'16.) lab'32( sol'16.) sol'32( fa'16.) fa'32( mib'16.) mib'32( re'16.) |
    mib'4 r <sib sol' mib''>\f <mib' sib' sol''> |
    <mib' sol>4 r mib'8\p r re' r |
    sib r re' r mib' r fa' r |
    re' r r4 fa'8.( sol'16 lab'8) lab'-. |
    sol'4.( sib'8 lab' sol' fa' mib') |
    sol'8.( mib'16) re'8-. re'-. re'4 r |
    sib'4:32\fp sib'8 r sib'4:32\fp sib'8 r |
    sib'32\p sib' sib' sib' sib'8 la'32 la' la' la' la'8 lab' r do'16( sib do' lab) |
    sol8.( sib16) do'8.( lab16) sol4.( re'8) |
    re'4( mib'8) r r2 |
    R1 |
    r2 mib'8\p r mib' r |
    mib' r mib' r re' r fa' r |
    fa'4( mib') mib'8 r mib' r |
    r mib' mib' mib' lab' lab' sol' sol' |
    sol' r sol' r mib' r re' r |
    mib'4 r mib'16(\p re' mib') mib'-. fa'( mi' fa') fa'-. |
    sol'4 r sib'16( la' sib' la' sol' fa' mib'! re') |
    do'4 r la'16( sold' la') la'-. sib'( la' sib') sib'-. |
    do''4 r mib''16( re'' mib'' re'' do'' sib' la' do'') |
    sib'4 r re''16( mib'' fa'' mib'' re'' fa'' mib'' re'') |
    r8 sib'-.( sib'-. sib'-.) r sib'-.( sib'-. sib'-.) |
    r sib'-.( sib'-. sib'-.) r sib'-.( sib'-. sib'-.) |
    la'2*1/2:16 s4\cresc sib'2:16 |
    la'8\f fa'-.( fa'-. fa'-. fa'-. fa'-. fa'-. fa'-.) |
    fa'8\p sib( re' fa' sib' re'' do'' la') |
    r sol( sib re' fad' sol' la' sib') |
    sol'1 |
    fa'!4 fa'2\cresc fa'4 |
    r8 fa'\p fa' fa' r fa' fa' fa' |
    r sol' sol' sol' r sol' sol' sol' |
    r fa' fa' fa' r la'-. la'( do'') |
    r sib'( do'' reb'') reb''( do'') do''( sib') |
    do''1 |
    r8 sib' sib' sib' r sol' sol' sol' |
    r re' re' re' r re'( mib' do') |
    re'4 r\fermata mib'8\p r re' r |
    sib8 r re' r mib' r fa' r |
    re' r r4 fa'8.( sol'16 lab'8) lab'-. |
    sol'4.( sib'8 lab' sol' fa' mib') |
    sol'8.( mib'16) re'8-. re'-. re'4 r |
    sib'4:32\fp sib'8 r sib'4:32\fp sib'8 r |
    sib'32\p sib' sib' sib' sib'8 la'32 la' la' la' la'8 lab' r do'16( sib do' lab) |
    sol8.( sib16) do'8.( lab16) sol4.( re'8) |
    re'4( mib'8) r r2 |
    R1 | \allowPageTurn
    r2 mib'8\p r mib' r |
    mib' r mib' r re' r fa' r |
    fa'4( mib') mib'8 r mib' r |
    r mib' mib' mib' lab' lab' sol' sol' |
    sol' r sol' r mib' r re' r |
    mib'4 r mib'16( re' mib') mib'-. fa'( mi' fa') fa'-. |
    sol'4 r do'16( si do' si do') do'( reb' la) |
    sib!4 r sol'16( fad' sol') sol'-. lab'( sol' lab') lab'-. |
    sib'4 r reb''16( do'' reb'' do'' sib' lab' sol' sib') |
    sol'4 r mi'16( red' mi') mi'-. fa'( mi' fa') fa'-. |
    sol'4 r sib'16( la' sib' lab' sol' fa' mi' sol') |
    fa'8 lab'4\cresc sol' fa' mib'!8 |
    r lab'\p lab' lab' lab' lab' lab'( sol') |
    r fa'-. lab'-. fa'-. lab'-. fa'-. re'-. fa'-. |
    r mib'16.( re'32 mib'8) mib'-. mib'( sol') sol'-. sol'-. |
    r sol'16.( fad'32 sol'8) sol'-. sol'( sib') sol'-. sol'-. |
    mib'1\fp\fermata |
    r4 <sib fa' re''>\f q r\fermata |
  }
>>
<sol mib'>4.\f sib8 mib' sib mib' sib |
fa' sib fa' sib fa'4 r |
<<
  \tag #'violino1 {
    sol'2:16\p sol':16 |
    <lab' sib>1\fp |
  }
  \tag #'violino2 {
    mib'2:16\p mib':16 |
    <fa' re'>1\fp |
  }
>>
<sol mib'>4.\f sib8 mib' sib mib' sib |
fa' sib fa' sib fa'4 r |
<<
  \tag #'violino1 {
    sol'2:16\p sol':16 |
    <lab' sib>1\fp |
  }
  \tag #'violino2 {
    mib'2:16\p mib':16 |
    <fa' re'>1\fp |
  }
>>
<<
  \tag #'violino1 {
    sol'2:16\p\cresc sib':16 |
    mib'':16 mib'':16 |
    re''4\f
  }
  \tag #'violino2 {
    mib'2:16\p\cresc sol':16 |
    sol':16 la':16 |
    sib'4\f
  }
>> <re' sib' sib''>4 sib sib |
sib2 r |
R1*4 |
<<
  \tag #'violino1 {
    r8 sol''\p sol'' sol'' r fa'' fa'' fa'' |
    r mib'' mib'' mib'' r reb'' reb'' reb'' |
    do''4 do'2.\sfp |
    sib'4 sib2.\sfp |
    lab4\p r mib''( do'') |
    sib'8 sib' lab' lab' sol' sol' fa' fa' |
    lab'1(\fermata |
    sol'4)
  }
  \tag #'violino2 {
    r8 mib''\p mib'' mib'' r reb'' reb'' reb'' |
    r do'' do'' do'' r sib' sib' sib' |
    do''4 sib2(\sfp la4) |
    sib'4 lab!2(\sfp sol4) |
    lab4\p r do''( lab') |
    sol'8 sol' fa' fa' mib' mib' re' re' |
    fa'1(\fermata |
    mib'4)
  }
>> r4 r2\fermata |
R1*4 |
<<
  \tag #'violino1 {
    r8 sol''\p sol'' sol'' r fa'' fa'' fa'' |
    r mib'' mib'' mib'' r reb'' reb'' reb'' |
    do''4 do'2.\sfp |
    sib'4 sib2.\sfp |
    lab4\p r mib''( do'') |
    sib'8 sib' lab' lab' sol' sol' fa' fa' |
    \ru#4 {
      mib''4.\p mi''8( fa''4.)\cresc fad''8( |
      sol''4.) mib''!8( lab'')\f fa''!-. re''-. sib'-. |
    }
    mib''4 <mib' sib' sib''>\ff <mib' sib' sol''> <mib' sib' sib''> |
    <mib' sib' sol''>4
  }
  \tag #'violino2 {
    r8 mib''\p mib'' mib'' r reb'' reb'' reb'' |
    r do'' do'' do'' r sib' sib' sib' |
    do''4 sib2(\sfp la4) |
    sib'4 lab!2(\sfp sol4) |
    lab4\p r do''( lab') |
    sol'8 sol' fa' fa' mib' mib' re' re' |
    <<
      \ru#16 \rt#4 { sib'16 sib'' }
      \ru#4 { s2\p s\cresc | s s\f | }
    >>
    sol''4 <mib' sib' sol''>\ff <sol mib'> <mib' sib' sol''> |
    <sol mib'>
  }
>> r4\fermata r2 |
R1*2 |
R1\fermataMarkup |
R1\fermataMarkup |
mib'2:16\sf mib':16\p |
mib'2:16\sf mib':16\p |
<<
  \tag #'violino1 {
    mib'2:16\p mib':16 |
    mib':16 mib':16 |
    mib':16 mib':16 |
    mib':16 mib':16 |
    re':16 re':16 |
    re':16 re':16 |
    <sol' mib''>16\f
  }
  \tag #'violino2 {
    sib2:16\p sib:16 |
    do':16 do':16 |
    do':16 do':16 |
    do':16 do':16 |
    sib:16 sib:16 |
    sib:16 sib:16 |
    <mib' sib'>16\f
  }
>> <sol mib'>16[ q q] q4:16 q2:16 |
<<
  \tag #'violino1 { <sib' sol''>16 }
  \tag #'violino2 { <sol' mib''>16 }
>> <sol mib'>16[ q q] q4:16 q2:16 |
<<
  \tag #'violino1 {
    sol'2:16\p sol':16 |
    sol':16 sol':16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    mib''8(\p re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-.\cresc sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    sol''2:16\f sol'':16 |
    mib''4
  }
  \tag #'violino2 {
    mib'2:16\p mib':16 |
    mib':16 mib':16 |
    mib':16 mib':16 |
    mib':16 mib':16 |
    re':16 re':16 |
    <re' sib>:16 q:16 |
    sol'8(\p sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-.\cresc sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sib'2:16\f sib':16 |
    sol'4
  }
>> r4 r2 |
R1*2 |
r2 <<
  \tag #'violino1 {
    lab'2\p |
    sol'4( sib') \appoggiatura sib'8 lab'4( sol'8 fa') |
    mib''8( re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-.\cresc sib'-. mib''( re'') do''-. sib'-. |
    mib''8( re'') do''-. sib'-. mib''( re'') do''-. sib'-. |
    sol''2:16\f sol'':16 |
    mib''4
  }
  \tag #'violino2 {
    fa'2\p |
    mib'4( sol') \appoggiatura sol'8 fa'4( mib'8 re') |
    sol'8( sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-.\cresc sol'-. sol'( sib') lab'-. sol'-. |
    sol'( sib') lab'-. sol'-. sol'( sib') lab'-. sol'-. |
    sib'2:16\f sib':16 |
    sol'4
  }
>> r4 r2 |
R1*2 |
r2 <<
  \tag #'violino1 {
    lab'2\p |
    sol'4( sib') \appoggiatura sib'8 lab'4( sol'8 fa') |
    sol''2:16\f mib''4\p mib'' |
    r mib'' r re'' |
    sol''2:16\f mib''4\p mib'' |
    r mib'' r re'' |
    sol''2:16 sol'':16 |
    fa'':16\cresc fa'':16 |
    mib''4\f r <sib' sib''>4.( \tuplet 3/2 { lab''16 sol'' fa'') } |
    mib''4.( \tuplet 3/2 { fa''16 sol'' lab'') } sib''4.( \tuplet 3/2 { lab''16 sol'' fa'') } |
    mib''4
  }
  \tag #'violino2 {
    fa'2\p |
    mib'4( sol') \appoggiatura sol'8 fa'4( mib'8 re') |
    sib'2:16\f do''4\p do'' |
    r do'' r sib' |
    sib'2:16\f do''4\p do'' |
    r do'' r sib' |
    sib'2:16 mib'':16 |
    mib'':16\cresc re'':16 |
    <sib' mib''>:16\f <fa' re''>:16 |
    <mib' mib''>:16 <fa' re''>:16 |
    <mib' mib''>4
  }
>> <mib' sib' sol''>4 <sol mib'> <mib' sib' sol''> |
<sol mib'> r q r |
q2 r\fermata |
