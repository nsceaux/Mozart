Per pie -- tà, non ri -- cer -- ca -- te
la ca -- gion del moi tor -- men -- to,
si cru -- de -- le in me lo sen -- to,
che nep -- pur lo so sper -- giar,
si cru -- de -- le in me lo sen -- to,
che nep -- pur lo so sper -- giar,
che nep -- pur __ lo so __ sper -- giar.

Vo pen -- san -- do… ma poi co -- me?
per us -- cir… ma che mi gio -- va
di far que -- sta o quel -- la pro -- va,
se __ non tro -- vo,
se non tro -- vo in che spe -- rar,
se non tro -- vo in che spe -- rar,
se non tro -- vo, __
se non tro -- vo in che spe -- rar?

Per pie -- tà, non ri -- cer -- ca -- te
la ca -- gion del moi tor -- men -- to,
si cru -- de -- le in me lo sen -- to,
che nep -- pur lo so sper -- giar, __
si cru -- de -- le in me lo sen -- to,
che nep -- pur lo so sper -- giar,
che nep -- pur __ lo so __ sper -- giar.

Vo pen -- san -- do… ma poi co -- me?
per us -- cir… ma che mi gio -- va
di far que -- sta o quel -- la pro -- va,
se __ non tro -- vo,
se non tro -- vo in che spe -- rar?

Ah tra l’i -- re e tra gli sde -- gni
del -- la mia fu -- ne -- sta sor -- te,
del -- la mia fu -- ne -- sta sor -- te,
chia -- mo, chia -- mo,
chia -- mo so -- lo, oh Dio! la mor -- te,
che mi ven -- ga a con -- so -- lar; __
chia -- mo, chia -- mo,
chia -- mo so -- lo, oh Dio! la mor -- te,
che mi ven -- ga a con -- so -- lar.

Ah tra l’i -- re e tra gli sde -- gni
del -- la mia,
del -- la mia fu -- ne -- sta sor -- te,
chia -- mo so -- lo, oh Dio! la mor -- te,
che mi ven -- ga a con -- so -- lar,
che mi ven -- ga a con -- so -- lar;
chia -- mo so -- lo, oh Dio! la mor -- te,
oh Dio! la mor -- te,
che mi ven -- ga a con -- so -- lar, a con -- so -- lar;
chia -- mo so -- lo, oh Dio! la mor -- te,
oh Dio! la mor -- te,
che mi ven -- ga a con -- so -- lar, a con -- so -- lar,
che mi ven -- ga a con -- so -- lar,
che mi ven -- ga a con -- so -- lar, a con -- so -- lar.
