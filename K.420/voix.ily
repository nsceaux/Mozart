\clef "tenor/G_8" r2 |
R1*12 |
r2 sol4 fa |
mib r8 fa sol4. lab8 |
fa fa r4 sib sib |
sib8( mib'4) re'8 do'([ sib]) lab([ sol]) |
sib8.([ sol16]) fa4 r sib8. sib16 |
mib'4.. mib'16 reb'4.. reb'16 |
do'8 do' r4 r mib'16([ re'!]) mib'([ do']) |
sib8.([ sol16]) lab8.([ fa16]) mib4. fa8 |
fa4( sol8) r r4 sib8. sib16 |
sib4( sol8.) sib16 sib4. sib8 |
sib8.([ mib'16]) mib'8 r mib'([ re']) do'([ sib]) |
do'4 re'8.([ mib'16]) fa4 sib lab( sol) mib'8([ re']) do'([ si]) |
do'2( re'4) mib' |
mib'( sib4. lab8) sol([ fa]) |
mib4 r r2 |
r r4 mib'8. do'16 |
la4 fa r2 |
r r4 fa'8. do'16 |
re'4 re' r sib8. fa'16 |
mi'4 r r2 |
r r8 reb' reb'8. reb'16 |
do'8 do' do' do' reb'8. reb'16 mi'8. mi'16 |
fa'4 fa r2 |
re'!2~ re'8([ fa']) mib'!([ do']) |
sib4 sib2 do'8 re' |
\appoggiatura fa'8 mib'4 re'8([ mib']) \appoggiatura re' do'4 si8([ do']) |
\appoggiatura sib!4 la2 r |
sib re |
do4( sol'2) \appoggiatura fa'16 mib'8([ re'16 do']) |
sib2 do'4.( fa'8) |
\appoggiatura fa' mi'1 |
mib'!2. mib'4 |
re' re'4.( do'16[ re']) mib'8 do' |
sib4 sib sib do'8([ la]) |
sib2\fermata sol4 fa |
mib4 r8 fa sol4. lab!8 |
fa fa r4 sib sib |
sib8( mib'4) re'8 do'([ sib]) lab([ sol]) |
sib8.([ sol16]) fa4 r sib8. sib16 |
mib'4.. mib'16 reb'4.. reb'16 |
do'8 do' r4 r mib'16([ re'!]) mib'([ do']) |
sib8.([ sol16]) lab8.([ fa16]) mib4. fa8 |
fa8([ fad] sol8) r r4 sib8. sib16 |
sib4( sol8.) sib16 sib4. sib8 |
sib8.([ mib'16]) mib'8 r mib'([ re']) do'([ sib]) |
do'4 re'8.([ mib'16]) fa4 sib |
lab( sol) mib'8([ re']) do'([ si]) |
do'2( re'4) mib' |
mib'( sib4. lab8) sol([ fa]) |
mib4 r r2 |
r r4 mib'8. fa'16 |
reb'4 reb' r2 |
r r4 reb'8. mib'16 |
do'4 do' r2 |
r r4 do'8. sol16 |
lab4 r r8 lab lab lab |
lab([ re'!]) re'2 re'8 mib' |
sol'([ fa']) mib'([ re']) \appoggiatura do'8 sib4. lab8 |
sol4 sol r sib8 sib |
sib([ mib']) mib'4 r mib'8 mib' |
mib'4\fermata re'8([ do']) do'([ sib]) sib([ la]) |
sib4 r r2\fermata |
R1*2 |
mib'2. sol'4 |
lab' fa' re' sib |
mib' mib' r2 |
R1 |
mib'2. sol'4 |
lab' fa' re' sib |

mib' mib' r mib'8 sol' |
mib'4. mib'8 mib'4. la8 |
sib4 sib r2 |
R1 |
sib2 sol4 r |
R1 |
mib'2 sib4 r |
r2 sib4. sib8 |
sol'2 fa' |
mib' reb' |
do'4 do' r2 |
R1 |
r2 mib'4. do'8 |
sib4 r8 lab sol4 r8 fa |
fa2.(\fermata\melisma sol8[ lab]) |
sol4\melismaEnd r r2\fermata |
sib2 sol4 r |
R1 |
mib'2 sib4 r |
r2 sib4. sib8 |
sol'2 fa' |
mib' reb' |
do'4 do' r2 |
R1 |
r2 mib'4. do'8 |
sib4 r8 lab sol4 r8 fa |
mib4 r r2 |
r2 sib4. sib8 |
mib'4 mib' r2 |
r4 r8 sib sib4. sib8 |
mib'4 mib' r2 |
r sib4. sib8 |
mib'4.( mi'8) fa'4. fad'8 |
sol'4. mib'!8 lab'8([ fa'!]) re'([ sib]) |
mib'4 mib' r2 |
r\fermata sib4. sib8 |
sib2( sol4.) sib8 |
sib2. mib'4 |
mib'2.(\fermata sol'8[ fa']) |
mib'4 r r2\fermata |
R1*2 |
mib'1 |
sol |
fa |
do' |
re |
sib |
sol2 r |
R1 |
mib'1 |
sol |
fa |
lab' |
sib~ |
sib2. do'8([ re']) |
mib'4 r r2 |
r mib'8([ re']) do'([ sib]) |
mib'([ re']) do'([ sib]) mib'([ re']) do'([ sib]) |
mib'([ re']) do' sib mib'([ re']) do'([ sib]) |
sol'2~ sol'4.( mib'8) |
mib'4 r r2 |
re'4 r reb' r |
do' r sib r |
la r lab r |
sol sib \appoggiatura sib8 lab4 sol8([ fa]) |
mib4 r r2 |
r mib'8([ re']) do'([ sib]) |
mib'([ re']) do'([ sib]) mib'([ re']) do'([ sib]) |
mib'([ re']) do' sib mib'([ re']) do'([ sib]) |
sol'2~ sol'4.( mib'8) |
mib'4 r r2 |
r4 re' r reb' |
r do' r sib |
r la r lab |
sol sib \appoggiatura sib8 lab4 sol8([ fa]) |
sol'2 mib'4 r8 mib' |
do'4 r8 do' re'4 r8 re' |
sol'2 mib'4 r8 mib' |
do'4 r8 do' re'4 r8 re' |
sol'2 mib' |
lab sib |
mib4 r r2 |
R1*3 |
R1^\fermataMarkup |
