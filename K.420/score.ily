\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = "Clarinetti in B"
        shortInstrumentName = "Cl."
        \consists "Metronome_mark_engraver"
      } <<
        \keepWithTag #'clarinetti \global
        \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in Es"
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Tenore
    } \withLyrics <<
      \keepWithTag #'all \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \vccbInstr
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global \includeNotes "basso"
      \origLayout {
        s2 s1*4\break s1*6\pageBreak
        s1*7\break s1*6\break s1*6\pageBreak
        s1*6\break s1*6\break s1*7\pageBreak
        s1*6\break s1*7\break s1*5\pageBreak
        s1*6\break s1*8\break s1*9\pageBreak
        s1*10\break s1*10\break s1*8\pageBreak
        s1*11\break s1*8\break s1*8\pageBreak
        s1*9\break s1*9\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
