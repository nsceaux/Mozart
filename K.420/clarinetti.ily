\clef "treble" \transposition sib
r2 |
R1*3 |
<>\f <<
  \tag #'(clarinetto1 clarinetti) {
    do''1 |
    fa''4~ fa''8 s mib''4~ mib''8 s |
  }
  \tag #'(clarinetto2 clarinetti) {
    do'1 |
    do''4~ do''8 s do''4~ do''8 s |
  }
  { s1 | s4. r8 s4. r8 | }
>>
R1*2 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    do''8-.( do''-. do''-. do''-.) |
    do''4( la'8) s do''-.( do''-. do''-. do''-.) |
    do''4( fa''8)
  }
  \tag #'(clarinetto2 clarinetti) {
    do'8-.( do'-. do'-. do'-.) |
    do'4( la8) s do'-.( do'-. do'-. do'-.) |
    do'4( fa'8)
  }
  { s4.\p\< s8\! | s2\p s4.\< s8\! | s4\p }
>> r8 r2 |
R1 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    fa''4 fa'' |
    fa'' s la''8 s sol'' s |
    fa'' s sol'' s la'' s sib'' s |
    sol''
  }
  \tag #'(clarinetto2 clarinetti) {
    la'4 la' |
    la' s fa''8 r mi'' r |
    do'' r mi'' r fa'' r sol'' r |
    mi''
  }
  { s2\f | s4 r s8\p r s r | s r s r s r s r | }
>> r8 r4 r2 |
R1*5 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    do''8-.( do''-. do''-. do''-.) |
    do''4( la'8) s do''-.( do''-. do''-. do''-.) |
    do''4( fa''8)
  }
  \tag #'(clarinetto2 clarinetti) {
    do'8-.( do'-. do'-. do'-.) |
    do'4( la8) s do'-.( do'-. do'-. do'-.) |
    do'4( fa'8)
  }
  { s4.\p\< s8\! | s2\p s4.\< s8\! | s4\p }
>> r8 r2 |
<<
  \tag #'(clarinetto1 clarinetti) {
    R1*4 |
    r2 \tag #'clarinetti <>^\markup\concat { 1 \super o }
    fa''8-.\mf( fa''-. fa''-. fa''-.) |
    fa''4 r r2 |
    r2 sol''8-.( sol''-. sol''-. sol''-.) |
    sol''4 r r2 |
  }
  \tag #'clarinetto2 { R1*8 }
>>
<<
  \tag #'(clarinetto1 clarinetti) {
    do'''8-.( do'''-. do'''-. do'''-.) do'''4
  }
  \tag #'(clarinetto2 clarinetti) {
    do''8-.( do''-. do''-. do''-.) do''4
  }
>> r4 |
R1*2 |
r4 
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''4( mib'' fad'') | sol''4 }
  { si'4( do''2) | si'4 }
  { s4 s2\cresc | s4\f }
>> r4 r2 |
R1*11 |
r2\fermata r2 |
r4 <<
  \tag #'(clarinetto1 clarinetti) { sol''8 s la'' s sib'' s | sol'' }
  \tag #'(clarinetto2 clarinetti) { mi''8 s fa'' s sol'' s | mi'' }
  { s8\p r s r s r }
>> r8 r4 r2 |
R1*5 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    do''8-.( do''-. do''-. do''-.) |
    do''4( la'8) s do''-.( do''-. do''-. do''-.) |
    do''4( fa''8)
  }
  \tag #'(clarinetto2 clarinetti) {
    do'8-.( do'-. do'-. do'-.) |
    do'4( la8) s do'-.( do'-. do'-. do'-.) |
    do'4( fa'8)
  }
  { s4.\p\< s8\! | s2\p s4.\< s8\! | s4\p }
>> r8 r2 |
<<
  \tag #'(clarinetto1 clarinetti) {
    R1*4 |
    r2 \tag #'clarinetti <>^\markup\concat { 1 \super o }
    fa''8-.\mf( fa''-. fa''-. fa''-.) |
    fa''4 r r2 |
  }
  \tag #'clarinetto2 { R1*6 }
>>
<>\mf <<
  \tag #'(clarinetto1 clarinetti) {
    fa''8-.( fa''-. fa''-. fa''-.) fa''4
  }
  \tag #'(clarinetto2 clarinetti) {
    fa'8-.( fa'-. fa'-. fa'-.) fa'4
  }
>> r4 |
R1 |
<>\mf <<
  \tag #'(clarinetto1 clarinetti) {
    re''8-.( re''-. re''-. re''-.) re''4
  }
  \tag #'(clarinetto2 clarinetti) {
    re'8-.( re'-. re'-. re'-.) re'4
  }
>> r4 |
R1*6 |
R1^\fermataMarkup |
r4 <>\f <<
  \tag #'(clarinetto1 clarinetti) { sol''4 do''' }
  \tag #'(clarinetto2 clarinetti) { mi''4 mi'' }
>> r4\fermata |
%%
\tag #'clarinetti <>^"a 2." fa''4.\f do''8 fa'' do'' fa'' do'' |
sol'' do'' sol'' do'' sol''4 r |
R1 |
<<
  \tag #'(clarinetto1 clarinetti) { mi''1 | fa''4. }
  \tag #'(clarinetto2 clarinetti) { sib'1 | la'4. }
  { s1\fp | s4.\f }
>> \tag #'clarinetti <>^"a 2." do''8 fa'' do'' fa'' do'' |
sol'' do'' sol'' do'' sol''4 r |
R1 | \allowPageTurn
<<
  \tag #'(clarinetto1 clarinetti) { mi''1 | fa''4 }
  \tag #'(clarinetto2 clarinetti) { sib'1 | la'4 }
  { s1\fp | }
>> r4 r2 |
r2 <>\f <<
  \tag #'(clarinetto1 clarinetti) { si''2 | do'''4 }
  \tag #'(clarinetto2 clarinetti) { fa''2 | mi''4 }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''4 do'' do'' | do''2 }
  { do''4 do'' do'' | do''2 }
>> r2 |
R1 |
<>\p <<
  \tag #'(clarinetto1 clarinetti) { do''2( la'4) }
  \tag #'(clarinetto2 clarinetti) { do'2( la4) }
>> r4 |
R1 |
<<
  \tag #'(clarinetto1 clarinetti) { fa''2( do''4) }
  \tag #'(clarinetto2 clarinetti) { fa'2( do'4) }
>> r4 |
R1*2 |
r4
<<
  \tag #'(clarinetto1 clarinetti) {
    do'''2( si''4) |
    s sib''!2( la''4) |
  }
  \tag #'(clarinetto2 clarinetti) {
    la''2( sol''4) |
    s sol''2( fa''4) |
  }
  { s2.\sfp | r4 s2.\sfp | }
>>
R1*2 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1 |
<>\p <<
  \tag #'(clarinetto1 clarinetti) { do''2( la'4) }
  \tag #'(clarinetto2 clarinetti) { do'2( la4) }
>> r4 |
R1 |
<<
  \tag #'(clarinetto1 clarinetti) { fa''2( do''4) }
  \tag #'(clarinetto2 clarinetti) { fa'2( do'4) }
>> r4 |
R1*2 |
r4
<<
  \tag #'(clarinetto1 clarinetti) {
    do'''2( si''4) |
    s sib''!2( la''4) |
  }
  \tag #'(clarinetto2 clarinetti) {
    la''2( sol''4) |
    s sol''2( fa''4) |
  }
  { s2.\sfp | r4 s2.\sfp | }
>>
R1*3 |
r2 <>\f <<
  \tag #'(clarinetto1 clarinetti) { sib''2( | la''4) }
  \tag #'(clarinetto2 clarinetti) { sol''2( | fa''4) }
>> r4 r2 |
r2 <<
  \tag #'(clarinetto1 clarinetti) { sib''2( | la''4) }
  \tag #'(clarinetto2 clarinetti) { sol''2( | fa''4) }
>> r4 r2 |
r2 <<
  \tag #'(clarinetto1 clarinetti) { sib''2( | la''4) }
  \tag #'(clarinetto2 clarinetti) { sol''2( | fa''4) }
>> r4 r2 |
r2 <<
  \tag #'(clarinetto1 clarinetti) { sib''2( | la''4) fa'' fa'' fa'' | fa'' }
  \tag #'(clarinetto2 clarinetti) { sol''2( | fa''4) la' la' la' | la' }
  { s2 | s4 s2.\ff | }
>> r4\fermata r2 |
R1*2
<<
  \tag #'(clarinetto1 clarinetti) { fa''1\fermata~ | fa''4 }
  \tag #'(clarinetto2 clarinetti) { do''1\fermata~ | do''4 }
  { s2.\p\< s4\> | s4\! }
>> r4 r2\fermata |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'2 s | fa' s | }
  { fa'2 s | fa' s | }
  { s2\sf r | s\sf r }
>>
R1*6 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'2 s | fa' s | }
  { fa'2 s | fa' s | }
  { s2\sf r | s\sf r }
>>
R1*10 |
<>\f <<
  \tag #'(clarinetto1 clarinetti) { fa''1 | }
  \tag #'(clarinetto2 clarinetti) { la'1 | }
>>
r2 <>\p <<
  \tag #'(clarinetto1 clarinetti) {
    fa''2( |
    mi'' mib'' |
    re'' do'' |
    si' sib') |
    la'!4 s sol' s |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'2( |
    sib' do'' |
    sib' la' |
    lab' sol') |
    fa'4 s mi' s |
  }
  { s2 | s1*3 | s4 r s r | }
>>
R1*4 |
<>\f <<
  \tag #'(clarinetto1 clarinetti) { fa''1 | }
  \tag #'(clarinetto2 clarinetti) { la'1 | }
>>
r2 <>\p <<
  \tag #'(clarinetto1 clarinetti) {
    fa''2( |
    mi'' mib'' |
    re'' do'' |
    si' sib') |
    la'!4 s sol' s |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'2( |
    sib' do'' |
    sib' la' |
    lab' sol') |
    fa'4 s mi' s |
  }
  { s2 | s1*3 | s4 r s r | }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { la''1 | sol'' | la'' | sol'' | la'' | sol'' | fa''4 }
  { fa''1~ |
    fa''2( mi'') |
    fa''1~ |
    fa''2( mi'') |
    fa''1~ |
    fa''2( mi'') |
    fa''4 }
  { s2\f s\p | s1 | s\f | s1*2 | s1\cresc | s4\f }
>> r4 <<
  \tag #'(clarinetto1 clarinetti) {
    do'''4.( \tuplet 3/2 { sib''16 la'' sol'') } |
    fa''4.( \tuplet 3/2 { sol''16 la'' sib'') } do'''4.( \tuplet 3/2 { sib''16 la'' sol'') } |
    fa''4 la'' fa'' la'' |
    fa'' s fa'' s |
    fa''2
  }
  \tag #'(clarinetto2 clarinetti) {
    do''4.( \tuplet 3/2 { sib'16 la' sol') } |
    fa'4.( \tuplet 3/2 { sol'16 la' sib') } do''4.( \tuplet 3/2 { sib'16 la' sol') } |
    fa'4 do'' la' do'' |
    la' s la' s |
    la'2
  }
  { s2 | s1*2 | s4 r s r | }
>> r2\fermata |
