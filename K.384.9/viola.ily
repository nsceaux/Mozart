\clef "alto" mib'4 r8 mib'4 r8 |
mib'4 r8 r4 r8 |
r4 r8 r4 sib'8\p |
mib'' sib' sol' lab' fa' re' |
mib'4 r8 mib'4 r8 |
re'8 sib re' re' sib re' |
sib4 r8 sib4 r8 |
sol8 sib sol sol sib sol |
sib4 r8 mib'4 mib'8 |
sib' lab' sol' lab'4 mib'8 |
sib' lab' sol' lab'4 mib'8 |
mib'4 r8 r4 r8 |
sib4.~ sib8( do') do'-. |
do'4 r8 r4 r8 |
re'4. fa'8 sib sib |
sib4 r8 sol'-. fa'-. mib'-. |
lab'4 r8 lab'-. sol'-. fa'-. |
sib-.\f re'-. mib'-. fa'-. sol'-. la'-. |
sib'4\p r8 r4 r8 |
sib8-.\f re'-. mib'-. fa'-. sol'-. la'-. |
sib'4\p r8 r4 \tuplet 3/2 { fa16([ sol la] } |
sib8) sib-. sib-. sib do' re' |
sol sol sol sol la sib |
mib mib mib mib fa sol |
do'\f re' mib' la sib do' |
sib4\p r8 r4 r8 |
lab4 r8 r4 r8 |
sol4 r8 r4 r8 |
fa4 r8 r4 r8 |
mi4 r8 r4 r8 |
fa2.\fermata\p |
re'8 re' re' sib sib sib |
fa'\fp fa' fa' fa' fa' fa' |
re' re' re' sib sib sib |
fa'\fp fa' fa' fa' fa' fa' |
re'2.\fp |
mib'\fp |
fa'\fp |
mib'\fp |
re'4.\fp mib'\fp |
sib2.:8\p\cresc |
sib:8\f |
mib'4 r8 r4 r8\fermata |
mib'4\p r8 r4 r8 |
reb'4 r8 r4 r8 |
do'4 r8 r4 r8 |
sib4 r8 r4 r8 |
la4 r8 r4 r8 |
fa'2.\fermata |
sol'8 sol' sol' mib' mib' mib' |
sib\fp sib sib sib sib sib |
sol' sol' sol' mib' mib' mib' |
sib\fp sib sib sib sib sib |
sib'\fp sib' sib' sib' sib' sib' |
sib'4\f r8 r4\fermata r8 |
do'8\p r r do' r r |
do' r r do' r r |
si r r si r r |
do' r r lab r r |
sol r r sol r r |
do' r r do' r r |
si r r si r r |
do' r r lab r r |
sol r r sol r r |
do' r r do' r r |
do'4.( sib!8) r r |
re'8-. sib-. re'-. mib' r r |
la-. sib-. do'-. sib-. sib-. re'-. |
sol si re' sol do' mib' |
sol re' fa' sol mib' sol' |
sol si re' sol do' mib' |
mib'4.( re'8) r r |
si r r si r r |
si r r r4 r8 |
do'-. fa'-. sol'-. lab' r r |
fa'-. mib'-. re'-. do' r r |
si r r si r r |
si r r r4 r8 |
do'-. fa'-. sol'-. lab'-. r r |
re'-. mib'-. fa'-. mib'-. r r |
mib-. fa-. sol-. lab-. r r |
re-. mib-. fa-. mib-. r r\fermata | \allowPageTurn
sib'8\f sib' sib' sib' sib' sib' |
sib'4 r8 r4 r8 |
mib'4\p r8 mib'4 r8 |
mib'4 r8 mib'4 r8 |
sib'8\f sib' sib' sib' sib' sib' |
sib'4 r8 r4 r8 |
sib4\p r8 sib4 r8 |
sib4 r8 sib4 r8 |
mib'4 r8 mib'4\p r8 |
\ru#5 { mib'4\p r8 mib'4\f r8 | }
mib'8 sib'\p sib' sib' sib' sib' |
sib'\fp sib' sib' sib' sib' sib' |
sol'\fp sol' sol' sol' sol' sol' |
sol'\fp sol' sol' sol' sol' sol' |
mib'\fp mib' mib' mib' mib' mib' |
mib'4 r8 sib\p sib sib |
mib' mib' mib' sol' sol' sol' |
sib' sib' sib' mib'' mib'' mib'' |
sib'2.:8\fermata\cresc |
mib':8 |
fa':8\f |
fa'4 r8 r4 r8 |
sol'2.\p~ |
sol'8 r r r4 r8 |
mib'2.~ |
mib'8 r r r4 r8 |
sib2.:8\f |
mib'8 mib' mib' sol' sol' sol' |
sib' sib' sib' sib' sib' sib' |
sol'2.\p~ |
sol'8 r r r4 r8 |
mib'2.~ |
mib'8 r r r4 r8 |
sib2.:8\f |
mib'8 mib' mib' do'' do'' do'' |
do'' do'' do'' sib' sib' sib' |
sib'4 r8 r4 r8 |
R2. |
r4 r8 <fa lab>8\p q q |
\ru#5 { <mib sol>4 r8 <fa lab> q q | }
<mib sol>4. sol'~ |
sol'8 r r r4 r8 |
mib'2.~ |
mib'8 r r r4 r8 |
sib2.:8\f |
mib'8 mib' mib' sol' sol' sol' |
sib'2.:8 |
sib'4 r8 r4 r8 |
sol'2.\p~ |
sol'8 r r r4 r8 |
mib'2.~ |
mib'8 r r r4 r8 |
sib2.:8\f |
mib'8 mib' mib' sol' sol' sol' |
sib'2.:8 |
sib'8 sib' sib' do''\p do'' do'' |
do'' do'' do'' sib' sib' sib' |
sib' sib' sib'\cresc do'' do'' do'' |
do'' do'' do'' sib' sib' sib' |
sib'\f mib'' mib'' mib'' mib'' mib'' |
mib'' sib' sib' sib' sib' sib' |
sib' sol' sol' sol' sol' sol' |
sol' mib' mib' mib' mib' mib' |
mib' sib sib sib sib sib |
sib sol sol sol sol sol |
sol4 r8 mib'4 r8 |
mib'4 r8 r4 r8 |
