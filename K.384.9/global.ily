\tag #'all \key mib \major
\tempo "Allegro" \midiTempo#120
\time 6/8 s2.*55 \bar "||"
\tempo "Andante" s2.*27 \bar "||"
\tempo "Allegro assai" s2.*75 \bar "|."
