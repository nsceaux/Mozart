\clef "bass" \tag#'fagotti <>^"a 2." mib4\f r8 mib4 r8 |
mib4 r8 r4 r8 |
R2.*16 |
<<
  \tag #'(fagotto1 fagotti) {
    fa'4. mib'8( re' mib') |
    re'4
  }
  \tag #'(fagotto2 fagotti) {
    re'4. do'8( si do') |
    sib!4
  }
  { s2.\p | s4\f }
>> r8 r4 r8 |
<<
  \tag #'(fagotto1 fagotti) {
    fa'4. mib'8( re' mib') |
    re'4
  }
  \tag #'(fagotto2 fagotti) {
    re'4. do'8( si do') |
    sib!4
  }
  { s2.\p | }
>> r8 r4 \tag#'fagotti <>^"a 2." sib,8 |
mib mib mib mib fa sol |
do do do do re mib |
la,\f sib, do fa, sol, la, |
sib,4\p r8 r4 r8 |
lab,!4 r8 r4 r8 |
sol,4 r8 r4 r8 |
fa,4 r8 r4 r8 |
mi,4 r8 r4 r8 |
mib,!2.\p\fermata |
R2. |
<>\fp <<
  \tag #'(fagotto1 fagotti) { re'4.( do') | fa'4 }
  \tag #'(fagotto2 fagotti) { sib4.( la) | re'4 }
>> r8 r4 r8 |
<>\fp <<
  \tag #'(fagotto1 fagotti) { re'4.( do') | }
  \tag #'(fagotto2 fagotti) { sib4.( la) | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib4 } { sib }
>> r8 r4 r8 |
\tag#'fagotti <>^"a 2." \ru#3 { sib,4\f r8 r4 r8 | }
sib,4\f r8 sib,4\f r8 |
sib,2.:8\p\cresc |
sib,:8\f |
mib4 r8 r4 r8\fermata |
mib4\p r8 r4 r8 |
reb4 r8 r4 r8 |
do4 r8 r4 r8 |
sib,4 r8 r4 r8 |
la,4 r8 r4 r8 |
lab,2.\fermata |
R2. |
<>\fp <<
  \tag #'(fagotto1 fagotti) { sol4.( fa) | sib8 }
  \tag #'(fagotto2 fagotti) { mib4.( re) | sol8 }
>> r8 r r4 r8 |
R2. |
<>\fp <<
  \tag #'(fagotto1 fagotti) { sol4.( fa) | }
  \tag #'(fagotto2 fagotti) { mib4.( re) | }
>>
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib4 } { mib }
>> r8 r4\fermata r8 |
<<
  \tag #'(fagotto1 fagotti) {
    r4 r8 r4
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    sol,8\p |
    do-. mib-. sol-. do-. mib-. sol-. |
    si,4( re8 fa4) sol,8 |
    do-. mib-. sol-. do'-. lab-. fad-. |
    sol4.( sol,8) r sol, |
    do-. mib-. sol-. do-. mib-. sol-. |
    si,4( re8 fa4) sol,8 |
    do mib sol do' lab fad |
    sol4.( sol,8) r sol, |
    do mib sol do mib sol |
    do'( mib' re') re'( sib!) sol-. |
    re-. sib,-. sol,-. do la, sib, |
    do re re sol, r r |
    sol2.\p~ |
    sol~ |
    sol |
    sol4.( sol,8) r sol, |
    si,-. re-. fa-. si,-. re-. fa-. |
    si,( re fa) lab r sol |
    do'-. si-. sib-. lab-. lab-. sol-. |
    fa-. sol-. sol,-. do r do |
    si,-.\p re-. fa-. si,-. re-. fa-. |
    si,( re fa) lab r sol |
    do'-. si-. sib-. lab-. lab-. sol-. |
    fa-. sol-. sol-. do'( sol) mib-. |
    do-. si,-. sib,-. lab,-. lab,-. sol,-. |
    fa,-. sol,-. sol,-. do,-. r r\fermata |
  }
  \tag #'fagotto2 { R2.*26 | R2.^\fermataMarkup }
>> \allowPageTurn
\tag#'fagotti <>^"a 2." mib'8\f mib' mib' sib sib sib |
mib4 r8 r4 r8 |
mib4\p r8 mib4 r8 |
mib4 r8 mib4 r8 |
sib\f sib sib mib mib mib |
sib,4 r8 r4 r8 |
sib,4\p r8 sib,4 r8 |
sib,4 r8 sib,4 r8 |
mib4 r8 \ru#5 {
  <>\fp <<
    \tag #'(fagotto1 fagotti) { do'4.( | sib8) }
    \tag #'(fagotto2 fagotti) { lab4.( | sol8) }
  >> r8 r
}
<>\fp <<
  \tag #'(fagotto1 fagotti) {
    do'4.( |
    sib8) sol' sol' sol' sol' sol' |
    fa'2.:8 |
    mib':8 |
    re':8 |
    do':8 |
    sib4
  }
  \tag #'(fagotto2 fagotti) {
    lab4.( |
    sol8) mib' mib' mib' mib' mib' |
    re'2.:8 |
    do':8 |
    sib:8 |
    lab:8 |
    sol4
  }
  { s4. |
    s8 s4\p s4. |
    s2.\fp |
    s2.\fp |
    s2.\fp |
    s2.\fp |
    s4\p }
>> r8 r4 r8 |
R2.*2 |
\tag#'fagotti <>^"a 2." mib2.:8\cresc |
do:8 |
sib,:8\f |
sib,4 r8 r4 r8 |
R2.*14 |
r4 r8 r4 \tag#'fagotti <>^"a 2." sib,8\f |
mib sib, sol lab fa re |
mib4 r8 <>\p \ru#5 {
  <<
    \tag #'(fagotto1 fagotti) { fa'8 fa' fa' | sol'4 }
    \tag #'(fagotto2 fagotti) { sib8 sib sib | mib'4 }
  >> r8
}
<<
  \tag #'(fagotto1 fagotti) { fa'8 fa' fa' | sol' }
  \tag #'(fagotto2 fagotti) { sib8 sib sib | mib' }
>> r8 r8 r4 r8 |
R2.*7 |
<>\p <<
  \tag #'(fagotto1 fagotti) { sol'4.( fa' | mib'8) }
  \tag #'(fagotto2 fagotti) { mib'4.( re' | do'8) }
>> r8 r r4 r8 |
<<
  \tag #'(fagotto1 fagotti) { mib'4.( re' | do'8) }
  \tag #'(fagotto2 fagotti) { do'4.( sib | lab8) }
>> r8 r r4 r8 |
R2.*3 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol'2. | fa' | sol' | fa' | mib'4 }
  { mib'2.~ | mib'4.( re') | mib'2.~ | mib'4.( re') | mib'4 }
  { s2.\p | s | s\cresc | s | s4\f }
>> r8 \tag#'fagotti <>^"a 2." mib8 mib mib |
\ru#5 mib2.:8 |
mib4 r8 mib4 r8 |
mib4 r8 r4 r8 |
