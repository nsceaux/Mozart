\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \keepWithTag #'all \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in Es."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Blonde
        shortInstrumentName = \markup\character B.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"      
      \new Staff \with {
        instrumentName = \markup\character Osmin
        shortInstrumentName = \markup\character O.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncello e Basso }
      shortInstrumentName = \markup\center-column { Vc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global
      \includeNotes "basso"
      \origLayout {
        s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*5\break s2.*8\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*7\break s2.*5\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
