<<
  \tag #'voix1 {
    \clef "soprano/treble" R2.*7 |
    r4 r8 r4 mib''8 |
    mib'' fa'' sol'' do'' re'' mib'' |
    sib'4 sib'8 do'' re'' mib'' |
    sib'4 sib'8 do'' re'' mib'' |
    sib'4 sib'8 sol''4.~ |
    sol''8 fa'' mib'' re'' do'' sib' |
    sib' lab' lab' fa''4.~ |
    fa''8 mib'' re'' do'' sib' lab' |
    lab' sol' sib' mib'' fa'' sol'' |
    sol'' do'' do'' fa'' sol'' lab'' |
    re'' sib' r r4 fa''8 |
    fa''4. mib''8([ re'']) mib'' |
    re''4. r4 r8 |
    fa''4 r8 mib'' re'' mib'' |
    re''4 r8 r4 r8 |
    R2.*8 |
    R2.^\fermataMarkup |
    R2.*3 |
    r4 r8 r4 fa'8 |
    sib'4 sib'8 r4 sib'8 |
    do''4 do''8 r4 do''8 |
    re''4 re''8 r4 re''8 |
    mib''4 mib''8 r4 mib''8 |
    fa''8 fa'' fa'' sol'' sol'' sol'' |
    lab''!4. fa''4 fa''8 |
    re''4. sib'4 sib'8 |
    mib'' mib'' r r4 r8\fermata |
    mib'2. |
    reb' |
    do'4.~ do'4 do'8 |
    sib2. |
    la4.~ la4 la8 |
    lab!2.\fermata |
    sol'4 sib'8 mib''4 sol''8 |
    sib''4. sib'4 sib'8 |
    mib''4 sib'8 mib''4 sol''8 |
    sib''4.~ sib''8 r sib' |
    sib''4. sib'4 sib'8 |
    mib''4. r4\fermata r8 |
    R2.*4 |
    r4 r8 r4 sol'8 |
    fa''16([ mib'']) re''([ do'']) si'([ do'']) fa''([ mib'']) re''([ do'']) si'([ do'']) |
    lab''4~ lab''16([ sol'']) sol''4 re''8 |
    fa''16([ mib'']) re''([ do'']) si'([ do'']) fa''([ mib'']) re''([ do'']) si'([ do'']) |
    si'4. re''8 r sol' |
    fa''16([ mib'']) re''([ do'']) si'([ do'']) fa''([ mib'']) re''([ do'']) si'([ do'']) |
    fad''4~ fad''16([ sol'']) sol''8 r r |
    sol''8 re'' sib'! la'4 r8 |
    mib''8 re'' fad' sol' r sol' |
    re'' re'' re'' re'' fa''16([ mib'']) re''([ do'']) |
    si'4. re''8 r sol' |
    re'' re'' re'' re'' fa''16([ mib'']) re''([ do'']) |
    fad''4. sol''8 r sol'' |
    lab''16([ sol'']) fa''!([ mib'']) re''([ fa'']) lab''([ sol'']) fa''([ mib'']) re''([ fa'']) |
    lab''4~ lab''16([ fa'']) fa''8 r r |
    mib'' re'' mi'' fa'' r r |
    lab'' sol'' si' do'' r sol'' |
    lab''16([ sol'']) fa''([ mib'']) re''([ fa'']) lab''([ sol'']) fa''([ mib'']) re''([ fa'']) |
    lab''4~ lab''16([ fa'']) fa''8 r r |
    mib'' re'' mi'' fa'' r r |
    lab'' sol'' si' do'' r r |
    mib''! re'' mi'' fa'' r r |
    lab'' sol'' si' do'' r r\fermata |
    R2. |
    r4 r8 r4 sib'8 |
    mib''4. sib'8 r r |
    R2.*2 |
    r4 r8 r4 sib'8 |
    fa''4. sib'8 r r |
    R2. |
    r4 r8 r4 mib''8 |
    mib'' sib' r r4 r8 |
    r4 r8 r4 mib''8 |
    mib'' sib' r r4 r8 |
    R2. |
    r4 r8 r4 mib''8 |
    sol'' sol'' sol'' sol'' sol'' sol'' |
    fa'' sib' r r4 sib'8 |
    mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' sol' r r4 sol'8 |
    do'' do'' do'' do'' do'' do'' |
    sib'8 mib' r r4 sol'8 |
    sib' sib' sib' sib' sib' sib' |
    mib''4. mib'' |
    sol''2. |
    la''4.~ la''4 la''8 |
    sib''4. sib' |
    r4 r8 r4 sib'8 |
    sol'' sol'' sol'' fa'' fa'' fa'' |
    mib'' mib'' r r4 sol'8 |
    mib'' mib'' mib'' re'' re'' re'' |
    do'' do'' r r4 r8 |
    fa''2. |
    sol''4. mib''4 mib''8 |
    sib'4. re''4 re''8 |
    mib''4 r8 r4 r8 |
    R2.*5 |
    r4 r8 r4 sib'8 |
    mib'' sib' r r4 sib'8 |
    mib'' sib' r r4 sib'8 |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib'' sib' r r4 sib'8 |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib'' sib' r r4 sib'8 |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib'' r r r4 sib'8 |
    sol'' sol'' sol'' fa'' fa'' fa'' |
    mib'' mib'' r r4 r8 |
    R2.*2 |
    fa''2. |
    sol''4. mib''4 mib''8 |
    sib'4. re''4 re''8 |
    mib''4 r8 r4 sib'8 |
    sol'' sol'' sol'' fa'' fa'' fa'' |
    mib'' mib'' r r4 sol'8 |
    mib'' mib'' mib'' re'' re'' re'' |
    do''4 r8 r4 r8 |
    fa''2. |
    sol''4. mib''4 mib''8 |
    sib'4. re''4 re''8 |
    mib''4. sol'' |
    fa'' fa''4 fa''8 |
    sol''4. mib'' |
    lab'' sib''4 sib''8 |
    mib''4 r8 r4 r8 |
    R2.*7 |
  }
  \tag #'voix2 {
    \clef "bass" R2. |
    r4 r8 r4 sib8 |
    mib' sib sol lab fa re |
    mib4. r4 sib,8 |
    mib sib, mib mib sib, mib |
    fa4. sib,8 r sib, |
    fa sib, fa fa sib, fa |
    sol4. mib8 r r |
    R2.*8 |
    r4 r8 r4 fa8 |
    re'4. sib8 r r |
    r4 r8 r4 fa8 |
    re'4. sib8 r r |
    r4 r8 r4 fa8 |
    sib sib sib sib do' re' |
    sol sol sol sol la sib |
    mib mib mib mib fa sol |
    do do r r4 r8 |
    sib,2. |
    lab,! |
    sol,4.~ sol,4 sol,8 |
    fa,2. |
    mi,4.~ mi,4 mi,8 |
    mib,!2.\fermata |
    re4 fa8 sib4 re'8 |
    fa'4. fa4 fa8 |
    sib4( fa8) sib4 re'8 |
    fa'4. fa4 fa8 |
    sib4 r8 r4 r8 |
    R2.*6 |
    R2.^\fermataMarkup |
    R2.*5 |
    R2.^\fermataMarkup |
    R2.*5 |
    R2.^\fermataMarkup |
    r4 r8 r4 sol,8 |
    do mib sol do mib sol |
    si,4( re8) fa4 sol,8 |
    do mib sol do' lab fad |
    sol4. sol,8 r r |
    R2.*3 |
    r4 r8 r4 sol,8 |
    do mib sol do mib sol |
    do'([ mib' re']) re'([ sib!]) sol |
    re8 sib, sol, do la, sib, |
    do re re sol, r sol |
    sol sol, sol, r r sol |
    sol sol, sol, r r sol |
    sol sol sol sol sol sol |
    sol4. sol,8 r sol, |
    si, re fa si, re fa |
    si,([ re fa]) lab r sol |
    do' si sib lab lab sol |
    fa sol sol, do r do |
    si, re fa si, re fa |
    si,([ re fa]) lab r sol |
    do' si sib lab lab sol |
    fa sol sol do'([ sol]) mib |
    do si, sib, lab, lab, sol, |
    fa, sol, sol, do r r\fermataMarkup |
    R2.*2 |
    r4 r8 r4 sib8 |
    sol4. fa4 mib8 |
    sib4 r8 r4 r8 |
    R2. |
    r4 r8 r4 sib8 |
    lab4. sib4 fa8 |
    sol4 r8 r4 r8 |
    r r sib do' lab do' |
    sib4 r8 r4 r8 |
    r4 sib8 do' lab do' |
    sib4 sib8 do' lab do' |
    sib4 sib8 do' lab do' |
    sib4 r8 r4 sol8 |
    sib sib sib sib sib sib |
    mib' mib r r4 mib8 |
    sol sol sol sol sol sol |
    do' do r r4 do8 |
    mib mib mib mib mib mib |
    sol4. sol4 sol8 |
    sib4. sib4 sib8 |
    mib'2. |
    mib'4.~ mib'4 mib'8 |
    re'4. sib |
    R2.*7 |
    r4 r8 r4 sib8 |
    mib' mib' mib' re' re' re' |
    do'8 do' r r4 sol8 |
    do' do' do' sib sib sib |
    lab lab r r4 r8 |
    re'2. |
    mib'4. do'4 do'8 |
    lab4. sib4 sib8 |
    mib8 r sib sib sib sib |
    sol r sib sib sib sib |
    sol r r r4 sib8 |
    mib' sib sol lab fa re |
    mib mib r r4 sib8 |
    mib' sib sol lab fa re |
    mib mib r r4 sib8 |
    mib' sib sol lab fa re |
    mib4 r8 r4 r8 |
    r4 r8 r4 sol8 |
    mib' mib' mib' re' re' re' |
    do' do' r r4 r8 |
    re'2. |
    mib'4. sib4 sib8 |
    sol4. fa4 fa8 |
    mib4 r8 r4 r8 |
    r4 r8 r4 sol8 |
    mib' mib' mib' re' re' re' |
    do' do' r r4 sol8 |
    do'8 do' do' sib sib sib |
    re'4. sib |
    mib' sib4 sib8 |
    sol4. fa4 fa8 |
    mib4. mib' |
    mib' re'4 re'8 |
    mib'4. do' |
    lab sib4 sib8 |
    mib4 r8 r4 r8 |
    R2.*7 |
  }
>>
