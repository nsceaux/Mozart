\clef "treble" <>
<<
  \tag #'(oboe1 oboi) {
    mib''4 s8 sol''4 s8 |
    sib''4
  }
  \tag #'(oboe2 oboi) {
    sol'4 s8 mib''4 s8 |
    sol''4
  }
  { s4\f r8 s4 r8 | }
>> r8 r4 r8 |
R2.*16 |
<<
  \tag #'(oboe1 oboi) { fa''4. mib''8( re'' mib'') | re''4 }
  \tag #'(oboe2 oboi) { re''4. do''8( si' do'') | sib'!4 }
  { s2.\p | s4\f }
>> r8 r4 r8 |
<<
  \tag #'(oboe1 oboi) { fa''4. mib''8( re'' mib'') | re''4 }
  \tag #'(oboe2 oboi) { re''4. do''8( si' do'') | sib'!4 }
  { s2.\p | }
>> r8 r4 r8 |
R2.*2 |
<>\f <<
  \tag #'(oboe1 oboi) { mib''2.( | re''8) }
  \tag #'(oboe2 oboi) { do''2.( | sib'8) }
>> r8 r r4 r8 |
R2.*4 |
<>\p <<
  \tag #'(oboe1 oboi) { la''2.\fermata }
  \tag #'(oboe2 oboi) { do''\fermata }
>>
R2.*3 |
<>\fp <<
  \tag #'(oboe1 oboi) { re'''4.( do''') | }
  \tag #'(oboe2 oboi) { sib''( la'') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4 } { sib'' }
>> r8 r4 r8 |
R2.*4 |
<<
  \tag #'(oboe1 oboi) { re''2.~ | re'' | mib''4 }
  \tag #'(oboe2 oboi) { lab'!2.~ | lab' | sol'4 }
  { s2.\p\cresc | s\f }
>> r8 r4 r8\fermata |
R2.*5 |
<>\p <<
  \tag #'(oboe1 oboi) { re'''2.\fermata }
  \tag #'(oboe2 oboi) { fa''\fermata }
>>
R2.*3 |
<<
  \tag #'(oboe1 oboi) { sol''4.( fa'') | sol''( fa'') | }
  \tag #'(oboe2 oboi) { mib''4.( re'') | mib''( re'') | }
  { s2.\fp | s\fp | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''4 } { mib'' }
>> r8 r4\fermata r8 |
<<
  \tag #'(oboe1 oboi) {
    r4 r8 r4
    \tag#'oboi <>^\markup\concat { 1 \super o }
    sol'8\p |
    fa''16( mib'' re'' do'' si' do'') fa''( mib'' re'' do'' si' do'') |
    lab''4~ lab''16( sol'') sol''4 re''8 |
    fa''16( mib'' re'' do'' si' do'') fa''( mib'' re'' do'' si' do'') |
    si'4.( re''8) r r |
  }
  \tag #'oboe2 R2.*5
>>
R2.*8 |
<>\p <<
  \tag #'(oboe1 oboi) {
    sol''2.~ |
    sol''~ |
    sol'' |
    do'''4.( si''8)
  }
  \tag #'(oboe2 oboi) {
    sol'2.~ |
    sol'~ |
    sol' |
    mib''4.( re''8)
  }
>> r8 r |
R2.*4 |
<<
  \tag #'(oboe1 oboi) {
    lab''8 s s lab'' s s |
    lab''4.( fa''8)
  }
  \tag #'(oboe2 oboi) {
    fa''8 s s fa'' s s |
    fa''4.( re''8)
  }
  { s8\p r r s r r | }
>> r8 r |
R2.*2 |
<<
  \tag #'(oboe1 oboi) { do'''8-. do'''-. do'''-. do'''-. }
  \tag #'(oboe2 oboi) { mib''-. re''-. mi''-. fa''-. }
>> r8 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { lab''8-. sol''-. si'-. do''-. }
  { fa''-. mib''!-. re''-. do''-. }
>> r8 r\fermata |
<>\f <<
  \tag #'(oboe1 oboi) {
    mib''8 mib'' mib'' fa'' fa'' fa'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    sib'!8 sib' sib' re'' re'' re'' |
    mib''4
  }
>> r8 r4 r8 |
R2.*2 | \allowPageTurn
<>\f <<
  \tag #'(oboe1 oboi) {
    fa''8 fa'' fa'' sol'' sol'' sol'' |
    lab''4
  }
  \tag #'(oboe2 oboi) {
    re''8 re'' re'' mib'' mib'' mib'' |
    re''4
  }
>> r8 r4 r8 |
R2.*2 |
r4 r8 \ru#6 {
  <>\fp <<
    \tag #'(oboe1 oboi) { do'''4.( | sib''8) }
    \tag #'(oboe2 oboi) { lab''4.( | sol''8) }
  >> r8 r
} r4 r8 |
R2.*4 |
\tag#'oboi <>^"a 2." mib''2.\p |
sib' |
sol' |
mib'\cresc |
<<
  \tag #'(oboe1 oboi) {
    mib''2. |
    re''8 sib' sib' sib' sib' sib' |
    sib'4
  }
  \tag #'(oboe2 oboi) {
    la'2. |
    sib'8 re' re' re' re' re' |
    re'4
  }
  { s2. | s8\f }
>> r8 r4 r8 |
R2.*13 |
r4 r8 r4 \tag#'oboi <>^"a 2." sib'8\f |
mib'' sib' sol'' lab'' fa'' re'' |
mib''4 r8 r4 r8 |
r4 r8 <>\p <<
  \tag #'(oboe1 oboi) {
    re''8 re'' re'' |
    mib''4 s8 lab'' lab'' lab'' |
    sol''4 s8 re'' re'' re'' |
    mib''4 s8 lab'' lab'' lab'' |
    sol''4 s8 re'' re'' re'' |
    mib''4 s8 lab'' lab'' lab'' |
    sol''
  }
  \tag #'(oboe2 oboi) {
    lab'8 lab' lab' |
    sol'4 s8 re'' re'' re'' |
    mib''4 s8 lab' lab' lab' |
    sol'4 s8 re'' re'' re'' |
    mib''4 s8 lab' lab' lab' |
    sol'4 s8 re'' re'' re'' |
    mib''8
  }
  { s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s4 r8 s4. | }
>> r8 r r4 r8 |
R2.*7 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4.( si'' | do'''8) }
  { sol''2.~ | sol''8 }
>> r8 r r4 r8 |
<<
  \tag #'(oboe1 oboi) { sol''2.~ | sol''8 }
  \tag #'(oboe2 oboi) { mib''2.~ | mib''8 }
>> r8 r r4 r8 |
R2.*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2. |
    fa'' |
    sol'' |
    fa'' |
    mib''4 s8 mib' mib' mib' |
    mib'4 }
  { mib''2.~ |
    mib''4.( re'') |
    mib''2.~ |
    mib''4.( re'') |
    mib''4 s8 mib' mib' mib' |
    mib'4 }
  { s2.\p | s | s\cresc | s | s4\f r8 }
>> r8 <<
  \tag #'(oboe1 oboi) {
    sol'8 sol' sol' |
    sol'4 s8 sib' sib' sib' |
    sib'4 s8 mib'' mib'' mib'' |
    mib''4 s8 sol'' sol'' sol'' |
    sol''4 sib''8 sol''4 sib''8 |
    sol''4 s8 mib''4 s8 |
    mib''4
  }
  \tag #'(oboe2 oboi) {
    mib'8 mib' mib' |
    mib'4 s8 sol' sol' sol' |
    sol'4 s8 sib' sib' sib' |
    sib'4 s8 mib'' mib'' mib'' |
    mib''4 sol''8 mib''4 sol''8 |
    mib''4 s8 sol'4 s8 |
    sol'4
  }
  { s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s2. s4 r8 s4 r8 | }
>> r8 r4 r8 |
