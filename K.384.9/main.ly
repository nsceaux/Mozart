\version "2.19.80"
\include "common.ily"

\opusTitle "K.384 – N°9 – Ich gehe, doch rathe ich dir"

\header {
  title = \markup\center-column {
    \line\italic { Die Entführung aus dem Serail }
    \line { N°9 – Duett: \italic { Ich gehe, doch rathe ich dir } }
  }
  opus = "K.384"
  date = "1782"
  copyrightYear = "2019"
}
\paper {
  systems-per-page = #(if (symbol? (ly:get-option 'part))
                          #f
                          2)
}
\includeScore "K.384.9"
