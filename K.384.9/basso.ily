\clef "bass" mib4\f r8 mib4 r8 |
mib4 r8 r4 r8 |
r4 r8 r4 sib8\p |
mib' sib sol lab fa re |
mib4 r8 mib4 r8 |
re sib, re re sib, re |
sib,4 r8 sib,4 r8 |
mib sol mib mib sol mib |
mib4 r8 lab4 lab8 |
sol fa mib lab4 lab8 |
sol fa mib lab4 lab8 |
sol4 r8 r4 r8 |
mib4. mi |
fa r4 r8 |
sib,4. re |
mib!4 r8 sol-. fa-. mib-. |
lab4 r8 lab-. sol-. fa-. |
sib,-.\f re-. mib-. fa-. sol-. la-. |
sib4.\p fa |
sib,8-.\f re-. mib-. fa-. sol-. la-. |
sib4.\p fa |
sib,4. r8 r sib, |
mib mib mib mib fa sol |
do8 do do do re mib |
la,\f sib, do fa, sol, la, |
sib,4\p r8 r4 r8 |
lab,!4 r8 r4 r8 |
sol,4 r8 r4 r8 |
fa,4 r8 r4 r8 |
mi,4 r8 r4 r8 |
mib,!2.\fermata\p |
re8 re re sib, sib, sib, |
fa\fp fa fa fa fa fa |
re re re sib, sib, sib, |
fa\fp fa fa fa fa fa |
\ru#4 { sib,4\f r8 r4 r8 | }
sib,4\f r8 sib,4\f r8 |
sib,2.:8\p\cresc |
sib,:8\f |
mib4 r8 r4 r8\fermata |
mib4\p r8 r4 r8 |
reb4 r8 r4 r8 |
do4 r8 r4 r8 |
sib,4 r8 r4 r8 |
la,4 r8 r4 r8 |
lab,!2.\fermata |
sol8 sol sol mib mib mib |
sib\fp sib sib sib sib sib |
sol sol sol mib mib mib |
sib\fp sib sib sib sib sib |
sib,\fp sib, sib, sib, sib, sib, |
mib4\f r8 r4\fermata r8 |
do8\p r r do r r |
do r r do r r |
si, r r si, r r |
do r r lab, r r |
sol, r r sol, r r |
do r r do r r |
si, r r si, r r |
do r r lab, r r |
sol, r r sol, r r |
do r r do r r |
do4.( sib,!8) r r |
sib,-. sol,-. sib,-. do r r |
do-. re-. re-. sol, r r |
sol r r r4 r8 |
sol, r r r4 r8 |
sol r r r4 r8 |
sol r r sol, r r |
si, r r si, r r |
si, r r r4 r8 |
do'-. si-. sib-. lab r r |
fa-. sol-. sol,-. do r r |
si, r r si, r r |
si, r r r4 r8 |
do'-. si-. sib-. lab-. r r |
fa-. sol-. sol-. do'-. r r |
do-. si,-. sib,-. lab,-. r r |
fa,-. sol,-. sol,-. do-. r r\fermata | \allowPageTurn
mib'8\f mib' mib' sib sib sib |
mib4 r8 r4 r8 |
mib4\p r8 mib4 r8 |
mib4 r8 mib4 r8 |
sib8\f sib sib mib mib mib |
sib,4 r8 r4 r8 |
sib,4\p r8 sib,4 r8 |
sib,4 r8 sib,4 r8 |
mib4 r8 mib4\f r8 |
\ru#5 { mib4\p r8 mib4\f r8 | }
mib4 r8 mib'4\p r8 |
re4\f r8 re'4\p r8 |
do4\f r8 do'4\p r8 |
sib,4\f r8 sib4\p r8 |
lab,4\f r8 lab4\p r8 |
sol4\f r8 r4 r8 |
R2.*2 |
mib2.:8\cresc |
do:8 |
sib,:8\f |
sib,4 r8 r4 r8 |
mib'4.(\p re' |
do'8) r r r4 r8 |
do'4.( sib |
lab8) r r r4 r8 |
sib2.:8\f |
mib8 mib mib sol sol sol |
sib sib sib sib, sib, sib, |
mib4.(\p re |
do8) r r r4 r8 |
do4.( sib, |
lab,8) r r r4 r8 |
sib,2.:8\f |
mib8 mib mib do' do' do' |
lab lab lab sib sib sib |
<>\p \ru#8 { mib4 r8 sib,4 r8 | }
mib'4.( re' |
do'8) r r r4 r8 |
do'4.( sib |
lab8) r r r4 r8 |
sib2.:8\f |
mib8 mib mib sol sol sol |
sib sib sib sib, sib, sib, |
mib4 r8 r4 r8 |
mib4.(\p re |
do8) r r r4 r8 |
do4.( sib, |
lab,8) r r r4 r8 |
sib,2.:8\f |
mib8 mib mib sol sol sol |
sib sib sib sib, sib, sib, |
mib mib mib do'\p do' do' |
lab lab lab sib sib sib |
mib' mib' mib'\cresc do' do' do' |
lab lab lab sib sib sib |
<>\f \ru#6 mib2.:8 |
mib4 r8 mib4 r8 |
mib4 r8 r4 r8 |


