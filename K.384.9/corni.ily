\clef "treble" \transposition mib <>
<<
  \tag #'(corno1 corni) { sol'4 s8 sol'4 s8 | sol'4 }
  \tag #'(corno2 corni) { do'4 s8 do'4 s8 | do'4 }
  { s4\f r8 s4 r8 | }
>> r8 r4 r8 |
R2.*15 |
<>\f <<
  \tag #'(corno1 corni) { sol'4.~ sol'8 }
  \tag #'(corno2 corni) { sol4.~ sol8 }
>> r8 r |
R2. |
<>\f <<
  \tag #'(corno1 corni) { sol'4.~ sol'8 }
  \tag #'(corno2 corni) { sol4.~ sol8 }
>> r8 r |
R2. |
\tag#'corni <>^"a 2." sol'8\p sol' sol' sol' r r |
sol' sol' sol' sol' r r |
do''8 do'' do'' do'' r r |
re''\f re'' re'' re'' re'' re'' |
<>\p <<
  \tag #'(corno1 corni) { sol'2.~ | sol'~ | sol' | }
  \tag #'(corno2 corni) { sol2.~ | sol~ | sol | }
>>
R2.*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2.\fermata | }
  { re''\fermata | }
  { s\p }
>>
R2. |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { re''2. | } { re'' | }
>>
R2. |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { re''2. | } { re'' | }
>>
\ru#4 {
  <>\fp <<
    \tag #'(corno1 corni) { sol'2. | }
    \tag #'(corno2 corni) { sol }
  >>
}
<<
  \tag #'(corno1 corni) {
    sol'4. sol' |
    sol'2.~ |
    sol' |
    do''4
  }
  \tag #'(corno2 corni) {
    sol4. sol |
    sol2.~ |
    sol |
    do'4
  }
  { s4.\fp s\fp | s2.\p\cresc | s\f | }
>> r8 r4 r8\fermata |
<>\p <<
  \tag #'(corno1 corni) { do''2.~ | do''~ | do'' | }
  \tag #'(corno2 corni) { do'2.~ | do'~ | do' | }
>>
R2.*2 |
<>\p <<
  \tag #'(corno1 corni) { sol'2.\fermata | }
  \tag #'(corno2 corni) { sol\fermata | }
>>
R2.*4 |
<<
  \tag #'(corno1 corni) { mi''4.( re'') | do''4 }
  \tag #'(corno2 corni) { do''4.( sol') | mi'4 }
  { s2.\fp | s4\f }
>> r8 r4\fermata r8 |
R2.*26 |
R2.^\fermataMarkup |
<>\f <<
  \tag #'(corno1 corni) { do''8 do'' do'' re'' re'' re'' | mi''4 }
  \tag #'(corno2 corni) { mi'8 mi' mi' sol' sol' sol' | do''4 }
>> r8 r4 r8 |
R2.*2 |
<>\f <<
  \tag #'(corno1 corni) { re''8 re'' re'' mi'' mi'' mi'' | re''4 }
  \tag #'(corno2 corni) { sol'8 sol' sol' do'' do'' do'' | sol'4 }
>> r8 r4 r8 | \allowPageTurn
R2.*2 |
<>\p <<
  \tag #'(corno1 corni) {
    do''2.~ | do''~ | do''~ | do''~ |
    do''~ | do''~ | do''8
  }
  \tag #'(corno2 corni) {
    do'2.~ | do'~ | do'~ | do'~ |
    do'~ | do'~ | do'8
  }
>> r8 r r4 r8 |
R2.*4 |
\tag#'corni <>^"a 2." do'2.:8\p |
mi'4. mi'4 mi'8 |
sol'2.:8 |
<<
  \tag #'(corno1 corni) { do''2.~ | do'' | sol':8 | sol'4 }
  \tag #'(corno2 corni) { do'2.~ | do' | sol:8 | sol4 }
  { s2.\cresc | s | s\f }
>> r8 r4 r8 |
R2.*15 |
\twoVoices #'(corno1 corno2 corni) <<
  { r4 r8 r4 sol'8\p |
    do'' sol' r r4 sol'8 |
    do'' sol' r r4 sol'8 |
    do'' sol' r r4 sol'8 |
    do'' sol' r r4 sol'8 |
    do'' sol' r r4 sol'8 |
    do'' sol' r r4 sol'8 |
    do'' }
  { R2. |
    r8 r sol'\p sol' sol' sol' |
    mi' r sol' sol' sol' sol' |
    mi' r sol' sol' sol' sol' |
    mi' r sol' sol' sol' sol' |
    mi' r sol' sol' sol' sol' |
    mi' r sol' sol' sol' sol' |
    mi' }
>> r8 r r4 r8 |
R2.*7 |
<>\p <<
  \tag #'(corno1 corni) { mi''2.~ | mi''8 }
  \tag #'(corno2 corni) { mi'2.~ | mi'8 }
>> r8 r r4 r8 |
<<
  \tag #'(corno1 corni) { do''2.~ | do''8 }
  \tag #'(corno2 corni) { do'2.~ | do'8 }
>> r8 r r4 r8 |
R2.*6 |
r4 r8 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'4. | do''4 s8 do' do' do' | do'4 }
  { sol'4. | mi'4 s8 do' do' do' | do'4 }
  { s4. | s4 r8 }
>> r8 <<
  \tag #'(corno1 corni) {
    mi'8 mi' mi' |
    mi'4 s8 sol' sol' sol' |
    sol'4 s8 do'' do'' do'' |
    do''4 s8 mi'' mi'' mi'' |
    mi''4 sol''8 mi''4 sol''8 |
    mi''4 s8 do''4 s8 |
    do''4
  }
  \tag #'(corno2 corni) {
    do'8 do' do' |
    do'4 s8 mi' mi' mi' |
    mi'4 s8 sol' sol' sol' |
    sol'4 s8 do'' do'' do'' |
    do''4 mi''8 do''4 mi''8 |
    do''4 s8 mi'4 s8 |
    mi'4
  }
  { s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s4 r8 s4. |
    s2. |
    s4 r8 s4 r8 | }
>> r8 r4 r8 |
