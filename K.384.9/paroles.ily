% OSMIN
\tag #'voix2 {
  Ich ge -- he, doch ra -- the ich dir
  den Schur -- ken Pe -- dril -- lo zu mei -- den,
  den Schur -- ken Pe -- dril -- lo zu mei -- den.
}
% BLONDE
\tag #'voix1 {
  O pack dich, be -- fiehl nicht mit mir,
  be -- fiehl nicht mit mir,
  be -- fiehl nicht mit mir,
  du weißt __ ja, ich kann es nicht lei -- den,
  du weißt __ ja, ich kann es nicht lei -- den,
  ich kann es nicht lei -- den,
  ich kann es nicht lei -- den.
}
% OSMIN
\tag #'voix2 {
  Ver -- sprich mir…
}
% BLONDE
\tag #'voix1 {
  Was fällt dir da ein?
}
% OSMIN
\tag #'voix2 {
  zum Hen -- ker…
}
% BLONDE
\tag #'voix1 {
  Fort, laß mich al -- lein!
}
% OSMIN
\tag #'voix2 {
  Wahr -- haf -- tig, kein Schritt von der Stel -- le,
  kein Schritt von der Stel -- le,
  kein Schritt von der Stel -- le!
  bis du zu __ ge -- hor -- chen __ mir schwörst,
  bis du zu ge -- hor -- chen mir schwörst,
  zu ge -- hor -- chen mir schwörst.
}
% BLONDE
\tag #'voix1 {
  Nicht so viel,
  nicht so viel,
  nicht so viel,
  nicht so viel,
  nicht so viel,
  nicht so viel,
  nicht so viel, du ar -- mer Ge -- sel -- le!
  und wenn du __ der Groß -- mo -- gul wärst!
  und wenn du der Groß -- mo -- gul wärst,
  wenn du der Groß des Groß -- mo -- gul wärst.
}
% OSMIN
\tag #'voix2 {
  O Eng -- län -- der! seid ihr nicht Tho -- ren,
  ihr laßt eu -- ren Wei -- bern den Wil -- len!
  Wie ist man ge -- plagt und ge -- scho -- ren,
  wenn solch ei -- ne Zucht,
  ei -- ne Zucht man er -- hält!
  O Eng -- län -- der!
  o Eng -- län -- der!
  o Eng -- län -- der! seid ihr nicht Tho -- ren,
  wie ist man ge -- plagt und ge -- scho -- ren,
  wenn solch ei -- ne Zucht, ei -- ne Zucht man er -- hält,
  wie ist man ge -- plagt und ge -- scho -- ren,
  wenn solch ei -- ne Zucht, ei -- ne Zucht man er -- hält,
  wenn solch ei -- ne Zucht, ei -- ne Zucht man er -- hält.
  
}
% BLONDE
\tag #'voix1 {
  Ein Herz, so in Frei -- heit ge -- bo -- ren,
  läßt nie -- mals sich skla -- visch be -- han -- deln,
  bleibt, wenn schon die Frei -- heit ver -- lo -- ren,
  noch stolz auf sie, la -- chet der Welt,
  ein Herz so in Frei -- heit ge -- bo -- ren,
  läßt nie -- mals sich skla -- visch be -- han -- deln,
  bleibt, wenn schon die Frei -- heit ver -- lo -- ren,
  noch stolz auf sie, la -- chet der Welt,
  bleibt, wenn schon die Frei -- heit ver -- lo -- ren,
  noch stolz auf sie, la -- chet der Welt,
  noch stolz auf sie, la -- chet der Welt.
  
  Nun troll dich!
}
% OSMIN
\tag #'voix2 {
  So sprichst du mit mir?
}
% BLONDE
\tag #'voix1 {
  nun troll dich!
}
% OSMIN
\tag #'voix2 {
  so sprichst du mit mir?
}
% BLONDE
\tag #'voix1 {
  Nicht an -- ders!
}
% OSMIN
\tag #'voix2 {
  nun bleib ich erst hier.
}
% BLONDE
\tag #'voix1 {
  nicht an -- ders!
}
% OSMIN
\tag #'voix2 {
  nun bleib ich erst hier,
  nun bleib ich erst hier,
  nun bleib ich erst hier!
}
% BLONDE (Stößt ihn fort)
\tag #'voix1 {
  Ein an -- der -- mal, jetzt mußt du ge -- hen,
  ein an -- der -- mal, jetzt mußt du ge -- hen,
  ein an -- der -- mal, jetzt mußt du ge -- hen,
  ein an -- der -- mal, jetzt mußt du ge -- hen,
  jetzt mußt du ge -- hen!
}
% OSMIN
\tag #'voix2 {
  Wer hat sol -- che Frech -- heit ge -- se -- hen!
  wer hat sol -- che Frech -- heit ge -- se -- hen,
  wer hat sol -- che Frech -- heit ge -- se -- hen,
  wer hat sol -- che Frech -- heit ge -- se -- hen?
}
% BLONDE (Stellt sich, als wollte sie ihm die Augen auskratzen)
\tag #'voix1 {
  Es ist um die Au -- gen ge -- sche -- hen,
  es ist um die Au -- gen ge -- sche -- hen,
  wo -- fern du noch län -- ger ver -- weilst.
}
% OSMIN (Furchtsam zurückweichend)
\tag #'voix2 {
  Nur ru -- hig, ich will ja gern ge -- hen,
  nur ru -- hig, ich will ja gern ge -- hen,
  be -- vor du gar Schlä -- ge er -- theilst.
}
%%%

% BLONDE
\tag #'voix1 {
  Nun troll dich!
}
% OSMIN
\tag #'voix2 {
  So sprichst du mit mir?
}
% BLONDE
\tag #'voix1 {
  nicht an -- ders!
}
% OSMIN
\tag #'voix2 {
  nun bleib ich erst hier.
}
% BLONDE (Stößt ihn fort)
\tag #'voix1 {
  Ein an -- der -- mal, jetzt mußt du ge -- hen!
  Es ist um die Au -- gen ge -- sche -- hen
  wo -- fern du noch län -- ger ver -- weilst,
  es ist um die Au -- gen ge -- sche -- hen,
  wo -- fern du noch län -- ger ver -- weilst,
  es ist um die Au -- gen ge -- sche -- hen,
  wo -- fern du noch län -- ger ver -- weilst,
  wo -- fern du noch län -- ger ver -- weilst,
  noch län -- ger ver -- weilst,
  noch län -- ger ver -- weilst.
}
% OSMIN
\tag #'voix2 {
  Wer hat sol -- che Frech -- heit ge -- se -- hen!
  Nur ru -- hig, ich will ja gern ge -- hen,
  be -- vor du gar Schlä -- ge er -- theilst,
  nur ru -- hig, ich will ja gern ge -- hen,
  be -- vor du gar Schlä -- ge er -- theilst,
  nur ru -- hig, ich will ja gern ge -- hen,
  be -- vor du gar Schlä -- ge er -- theilst,
  be -- vor du gar Schlä -- ge er -- theilst,
  gar Schlä -- ge er -- theilst,
  gar Schlä -- ge er -- theilst.
}
