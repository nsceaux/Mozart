\clef "treble" <sib sol' mib''>4\f r8 <mib' sib' sol''>4 r8 |
<mib' sib' sib''>4 r8 r4 r8 |
r4 r8 r4 sib'8\p |
mib'' sib' sol' lab' fa' re' |
mib' sib mib' mib' sib mib' |
fa'4. sib4 sib8 |
<<
  \tag #'violino1 {
    fa'8 sib fa' fa' sib fa' |
    sol'4. mib'4 mib''8 |
    mib'' fa'' sol'' do'' re'' mib'' |
    sib' sib' sib' do'' re'' mib'' |
    sib' sib' sib' do'' re'' mib'' |
    sib' sib' sib' sol''4.\fp~ |
    sol''8 fa'' mib'' re'' do'' sib' |
    sib'( lab') lab'-. fa''4.\fp~ |
    fa''8 mib'' re'' do'' sib' lab' |
    lab'( sol') sib'-. mib''-. fa''-. sol''-. |
    sol''( do'') do''-. fa''-. sol''-. lab''-. |
    re''4.(\fp sib'8) r r |
    r4 r8 r4 fa'32( sol' la' sib'64 do'') |
    re''4.(\fp sib'8) r r |
    R2.*2 |
    r4 r8 r4 \tuplet 3/2 { sib'16([ do'' re''] } |
    mib''8) mib''-. mib''-. mib'' fa'' sol'' |
    do''4
  }
  \tag #'violino2 {
    re'8 sib re' re' sib re' |
    mib'4. sib4 sib8 |
    sol' fa' mib' mib' re' do' |
    mib'4 mib'8 mib' re' do' |
    mib'4 mib'8 mib' re' do' |
    sib4 r8 r4 r8 |
    sol'2. |
    sol'8( fa') fa'-. lab'4.\fp~ |
    lab'8 do'' sib' lab' sol' fa' |
    fa'( mib') sol'-. sib'-. lab'-. sol'-. |
    do''4 r8 do''-. sib'-. lab'-. |
    fa'4.(\fp re'8) r r |
    R2. |
    fa'4.(\fp re'8) r r |
    R2. |
    r4 r8 r4 \tuplet 3/2 { re'16([ mib' fa'] } |
    sol'8) sol'-. sol'-. sol' la' sib' |
    mib' mib' mib' mib' fa' sol' |
    do'4
  }
>> r8 <la fa' mib''>4 r8 |
r8 <>\p sib8( re' fa' re' sib) |
r sib( re' fa' re' sib) |
r sib( mib' sol' mib' sib) |
r do'( fa' la' fa' do') |
r do'( sol' sib' sol' do') |
<<
  \tag #'violino1 {
    la'2.\p\fermata |
    sib'8 sib' sib' sib' sib' sib' |
    re''\fp re'' re'' do'' do'' do'' |
    fa'' fa'' fa'' re'' re'' re'' |
    re''\fp re'' re'' do'' do'' do'' |
    sib'\f fa' re' sib'\p fa' re' |
    do''\f sol' mib' do''\p sol' mib' |
    re''\f lab'! fa' re''\p lab' fa' |
    mib''\fp sib' sol' mib'' sib' sol' |
    fa''8\fp re'' sib' sol''\fp mib'' sib' |
    <sib' lab''!>2.:8\p\cresc |
    q:8\f |
    <sib' sol''>4
  }
  \tag #'violino2 {
    do'2.\p\fermata |
    fa'8 fa' fa' re' re' re' |
    sib'\fp sib' sib' la' la' la' |
    sib' sib' sib' sib' sib' sib' |
    sib'\fp sib' sib' la' la' la' |
    sib'2.\fp |
    sib'\fp |
    lab'!\fp |
    sol'\fp |
    lab'4.\fp sol'4.\fp |
    <fa' re''>2.:8\p\cresc |
    q:8\f |
    <sol' mib''>4
  }
>> r8 r4 r8\fermata |
r8 mib'(\p sol' sib' sol' mib') |
r mib'( sol' sib' sol' mib') |
r mib'( lab' do'' lab' mib') |
r fa'( sib' re'' sib' fa') |
r fa'( do'' mib'' do'' fa') |
<<
  \tag #'violino1 {
    re''2.\fermata |
    mib''8 mib'' mib'' sib' sib' sib' |
    sol'\fp sol' sol' fa' fa' fa' |
    sib' sib' sib' mib'' mib'' mib'' |
    sol'\fp sol' sol' fa' fa' fa' |
    sol''\fp sol'' sol'' fa'' fa'' fa'' |
    <mib'' mib' sol>4\f
  }
  \tag #'violino2 {
    sib'2.\fermata |
    sib'8 sib' sib' sol' sol' sol' |
    mib'\fp mib' mib' re' re' re' |
    mib' mib' mib' sol' sol' sol' |
    mib'\fp mib' mib' re' re' re' |
    mib''\fp mib'' mib'' re'' re'' re'' |
    <mib'' sol' sib>4\f
  }
>> r8 r4\fermata r8 |
do'8-.\p mib'-. sol'-. do'-. mib'-. sol'-. |
do' mib' sol' do' mib' sol' |
re' fa' sol' re' fa' sol' |
do' mib' sol' do' mib' fad' |
si re' sol' si re' sol' |
do'-. mib'-. sol'-. do'-. mib'-. sol'-. |
si re' fa' si re' fa' |
do' mib' sol' do' mib' fad' |
si re' sol' si re' sol' |
<<
  \tag #'violino1 {
    fa''16( mib'' re'' do'' si' do'') fa''( mib'' re'' do'' si' do'') |
    fad''4~ fad''16( sol'') sol''8 r r |
    sol''-. re''-. sib'!-. la' r r |
    mib''-. re''-. fad'-. sol' r sol' |
    re'' re'' re'' re''( fa''16 mib'' re'' do'') |
    si'4.( re''8) r sol' |
    re'' re'' re'' re''( fa''16 mib'' re'' do'') |
    fad''4.( sol''8) r sol'' |
    \ru#2 { lab''16(_\markup\dynamic mfp sol'' fa''! mib'' re'' fa'') } |
    lab''4_\markup\dynamic mfp ~ lab''16( fa'') fa''8 r r |
    mib''-. re''-. mi''-. fa'' r r |
    lab''-. sol''-. si'-. do'' r sol'' |
    \ru#2 { lab''16(_\markup\dynamic mfp sol'' fa''! mib'' re'' fa'') } |
    lab''4_\markup\dynamic mfp ~ lab''16( fa'') fa''8 r r |
    mib''-. re''-. mi''-. fa''-. r r |
    lab''-. sol''-. si'-. do''-. r r |
    mib'!-. re'-. mi'-. fa'-. r r |
    lab'-. sol'-. si-. do'-. r r\fermata |
  }
  \tag #'violino2 {
    do'8-. mib'-. sol'-. do'-. mib'-. sol'-. |
    mib' fad' la' re' sol' sib'! |
    sol'8-. sol'-. sol'-. sol' r r |
    sol'-. sol'-. la-. sol r r |
    r4 r8 r4 sol8 |
    re' re' re' re'( fa'16 mib' re' do') |
    si8 si si si( re'16 do' si la) |
    do'4.( si8) re'-. sol'-. |
    si-. re'-. fa'-. si-. re'-. fa'-. |
    re' fa' lab' si re' sol' |
    sol'-. sol'-. do''-. do'' r r |
    do''-. do''-. fa'-. mib' mib' sol' |
    si-. re'-. fa'-. si-. re'-. fa'-. |
    re'8 fa' lab' si re' sol' |
    sol'-. sol'-. do''-. do''-. r r |
    do''-. do''-. re'-. do'-. r r |
    do'-. do'-. do'-. do'-. r r |
    do'-. do'-. re'-. do'-. r r\fermata |
  }
>> \allowPageTurn
<sol' mib''>8\f q q <<
  \tag #'violino1 { <sib' fa''>8 q q | <sib' sol''>4 }
  \tag #'violino2 { <fa' re''>8 q q | <sol' mib''>4 }
>> r8 r4 r8 |
<>\p \ru#4 { mib'8 sol' sib' } |
<<
  \tag #'violino1 { <sib' fa''>8\f q q <sib' sol''> q q | <sib' lab''>4 }
  \tag #'violino2 { <fa' re''>8\f q q <sol' mib''> q q | <sib' fa''>4 }
>> r8 r4 r8 |
<>\p \ru#4 { re'8 fa' sib' } |
mib'8 sol' sib' mib'\f lab' do'' |
\ru#5 { mib'\p sol' sib' mib'\f lab' do'' | }
<<
  \tag #'violino1 {
    sib'4 r8 r mib''16(\f fa'' sol'' lab'' |
    sib''8) sib''\p sib'' sib'' sib'' sib'' |
    sol''4 r8 r do''16(\f re'' mib'' fa'' |
    sol''8) sol''\p sol'' sol'' sol'' sol'' |
    mib''4 r8 r lab16(\f sib do' re' |
    mib'8) mib' mib' sol'\p sol' sol' |
    sib' sib' sib' mib'' mib'' mib'' |
    sol'' sol'' sol'' sib'' sib'' sib'' |
    mib'''2.:8\cresc |
    mib''':8 |
    re'''8\f sib'' sib'' sib'' sib'' sib'' |
    sib''4 r8 r4 r8 |
    sol''4.(\p fa'' |
    mib''8) r r r4 r8 |
    mib''4.( re'' |
    do''8) r r r4 r8 |
    <sib' fa''>2.:8\f |
    <sib' sol''>8 q q mib'' mib'' mib'' |
    sib' sib' sib' re'' re'' re'' |
    sol''\p sol'' sol'' fa'' fa'' fa'' |
    mib''8 r r r4 r8 |
    mib'' mib'' mib'' re'' re'' re'' |
    do'' r r r4 r8 |
    <sib' fa''>2.:8\f |
    <sib' sol''>8 q q sol'' sol'' sol'' |
    fa''2.:8 |
    mib''4 r8 r4 r8 |
    r4 r8 r4 sib'8\p |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib''4 r8 r4 sib'8 |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib''4 r8 r4 sib'8 |
    mib'' sib' sol'' lab'' fa'' re'' |
    mib''4 r8 r4 sib'8 |
    sol''4.( fa'' |
    mib''8) r r r4 r8 |
    mib''4.( re'' |
    do''8) r r r4 r8 |
    <sib' fa''>2.:8\f |
    <sib' sol''>8 q q mib'' mib'' mib'' |
    sib' sib' sib' re'' re'' re'' |
    mib'' sib' sib' sib' sib' sib' |
    sol''4.(\p fa'' |
    mib''8) r r r4 r8 |
    mib''4.( re'' |
    do''8) r r r4 r8 |
    <sib' fa''>2.:8\f |
    <sib' sol''>8 q q mib'' mib'' mib'' |
    sib' sib' sib' re'' re'' re'' |
    mib'' mib'' mib'' sol''\p sol'' sol'' |
    fa''2.:8 |
    sol'':8\cresc |
    fa'':8 |
    mib''8\f sib'' sib'' sib'' sib'' sib'' |
    sib'' sol'' sol'' sol'' sol'' sol'' |
    sol'' mib'' mib'' mib'' mib'' mib'' |
    mib'' sib' sib' sib' sib' sib' |
    sib' sol' sol' sol' sol' sol' |
    sol' mib' mib' mib' mib' mib' |
    mib'4
  }
  \tag #'violino2 {
    <sib' sol''>8 q\p q q q q |
    <sib' fa''>4 r8 r4 \tuplet 3/2 { sib'16([\f do'' re''] } |
    mib''8) mib''\p mib'' mib'' mib'' mib'' |
    re''4 r8 r4 \tuplet 3/2 { sol'16\f( lab' sib' } |
    do''8) do''\p do'' do'' do'' do'' |
    sib'4 r8 mib'\p mib' mib' |
    sol' sol' sol' sib' sib' sib' |
    mib'' mib'' mib'' sol'' sol'' sol'' |
    sol''2.:8\cresc |
    la'':8 |
    sib''8\f re'' re'' re'' re'' re'' |
    re''4 r8 r4 r8 |
    sib'4.(\p si' |
    do''8) r r r4 r8 |
    sol'2.( |
    lab'8) r r r4 r8 |
    <fa' re''>2.:8\f |
    <sol' mib''>8 q q sib' sib' sib' |
    sol' sol' sol' fa' fa' fa' |
    sib'4.(\p si' |
    do''8) r r r4 r8 |
    sol'2.( |
    lab'8) r r r4 r8 |
    <fa' re''>2.\f |
    <sol' mib''>2.:8 |
    mib''8 mib'' mib'' re'' re'' re'' |
    mib''4 r8 r4 r8 |
    R2. |
    r4 r8 r4 sib8\p |
    mib' sib sol' lab' fa' re' |
    mib'4 r8 r4 sib8 |
    mib' sib sol' lab' fa' re' |
    mib'4 r8 r4 sib8 |
    mib' sib sol' lab' fa' re' |
    sib'4.( si' |
    do''8) r r r4 r8 |
    sol'2.( |
    lab'!8) r r r4 r8 |
    <fa' re''>2.:8\f |
    <sol' mib''>8 q q sib' sib' sib' |
    sol' sol' sol' fa' fa' fa' |
    sol'4 r8 r4 r8 |
    sib'4.(\p si' |
    do''8) r r r4 r8 |
    sol'2.( |
    lab'8) r r r4 r8 |
    <fa' re''>2.:8\f |
    <sol' mib''>8 q q sib' sib' sib' |
    sol' sol' sol' fa' fa' fa' |
    sol' sol' sol' mib''\p mib'' mib'' |
    mib'' mib'' mib'' re'' re'' re'' |
    mib''2.:8\cresc |
    mib''8 mib'' mib'' re'' re'' re'' |
    mib''\f sol'' sol'' sol'' sol'' sol'' |
    sol'' mib'' mib'' mib'' mib'' mib'' |
    mib'' sib' sib' sib' sib' sib' |
    sib' sol' sol' sol' sol' sol' |
    sol' mib' mib' mib' mib' mib' |
    mib' sib sib sib sib sib |
    sib4
  }
>> r8 <mib' sib' sol''>4 r8 |
<sol mib'>4 r8 r4 r8 |
