\clef "alto" r4 |
la16-\sug\f la la la la4:16 |
si:16 sold:16 |
la4 r8 fad' |
mi'2:16\p |
mi':16 |
mi'8 r si-.\f si'-. |
la'[ r16 la'] mi'16. fad'32 mi'16. re'32 |
dod'8 r r4 |
la2:16\p |
si4:16 sold:16 |
la4 r8 fad' |
mi'4 r |
la2:16 |
si4:16 re'16 re' mi' mi' |
mi'8 re' mi' mi' |
\ru#4 mi'2:16 |
mi'4 r8 si |
la2 |
si4\fermata r |
la2:16 |
si4:16 sold:16 |
la4 r8 fad' |
mi'4 r |
la2:16 |
si4:16 re'16 re' mi' mi' |
mi'8 re' mi' mi' |
mi' la la r |
la2\fp |
r8 si la4\fp |
r8 si mi'4 |
R2 |
r16 si mi' sold' si8 dod' |
si4 si8\f si' |
si4 r r |
mi'2\p r4 |
fad'2 r4 |
si4. si8 dod' red' |
mi'4 r r |
la' r r |
mi' r r |
r8 r16 dod' si4( la8.)\trill sold16 |
sold8 r mi' r |
dod'2\fermata\fp |
si8 fad' red' si |
dod'2\fermata\fp |
si8 r r\fermata r |
R2 |
r16 si mi' si dod'8 si16 la |
sold4 mi'8\f mi' |
mi'4\fermata <<
  { mi'8 mi' |
    \grace fad'16 mi'8 re'!16 dod' si8-. re'-. |
    re'( dod') } \\
  { dod'8 dod' |
    \grace re'!16 dod'8 si16 la sold8-.\p si-. |
    si( la) }
>> dod'4 |
sold la |
mi <<
  { mi'8-. mi'-. |
    \grace fad'16 mi'8 re'16 dod' si8-. re'-. |
    re'( dod') } \\
  { dod'8-. dod'-. |
    \grace re'16 dod'8 si16 la sold8-.\p si-. |
    si( la) }
>> dod'4 |
sold4 la8 mi' |
mi'16-. mi'( red' mi') r mi'( red' mi') |
r mi'( red' mi') r mi'( red' mi') |
r16 mi( red mi) r mi( red mi) |
r mi( red mi) r mi( red mi) |
mi8 r\fermata r4 |
la2:16 |
si4:16 sold:16 |
la8 si mi mi' |
mi'[ r16 mi']-.\f re'-. dod'-. si-. la-. |
<>\p \ru#6 mi'2:16 |
mi'4 r\fermata |
r\fermata r |
la2:16 |
si4:16 sold:16 |
la4 r8 fad' |
mi'4 r |
la2:16 |
si4:16 re'16 re' mi' mi' |
mi'8 re' mi' mi' |
mi'2:16 |
mi':16 |
mi'8 fad'4 mi'8 |
mi'2:16 |
mi':16 |
mi'8 fad'4 mi'8 |
mi'8 dod' mi' mi\cresc |
la dod' mi' mi |
la16\f si dod' re' mi' mi' mi' mi' |
mi'2:16 |
mi'8 r si-. si'-. |
la'[ r16 la'] mi'16.\trill fad'32 mi'16.\trill re'32 |
dod'8 mi' la r |
