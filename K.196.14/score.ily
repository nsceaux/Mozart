\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Nardo
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncello e Basso }
      shortInstrumentName = \markup\center-column { Vcl. B. }
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s2*8\break \grace s16 s2*8\break s2*8\break s2*7\pageBreak
        s2*5 s2.*2\break s2.*6 s2*2\break s2*8\break s2*8\pageBreak
        s2*7\break s2*7\break s2*7\break s2*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
