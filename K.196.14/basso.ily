\clef "bass" r4 |
la-\sug\f la, |
mi r8 mi |
re4 r8 si, |
mi4 r |
R2 |
r4 mi8-. mid-. |
fad[ r16 re] mi!8[ mi] |
la, r r4 |
la\p la, |
mi r8 mi |
re4 r8 si, |
mi4 r |
la la, |
mi r8 sold |
la re mi mi |
la,4 r |
R2 |
mi4 r |
R2 mi4 r8 mi |
fa2 |
mi4\fermata r |
la la, |
mi r8 mi |
re4 r8 si, |
mi4 r |
la la, |
mi r8 sold |
la re mi mi |
la, la la, r |
fad2\fp |
r8 mi fad4\fp |
r8 mi mi4 |
R2 |
r16 mi sold si mi8 la, |
si,4 si,8\f si |
si,4 r r |
mi2\p r4 |
fad2 r4 |
si,4. si,8 dod red |
mi4 r r |
la r r |
mi r r |
r8 r16 la si4 si, |
mi8 r mi r |
dod2\fp\fermata |
si,8 r si, r |
dod2\fp\fermata |
si,8 r r\fermata r |
R2 |
r16 mi sold mi la8 si |
mi4 mi8\f mi' |
mi4\fermata r |
la, mi\p |
la, dod |
sold, la, |
mi r |
la\f mi\p |
la, dod |
sold la8 mi |
la, r do' r |
sold r la r |
mi r do' r |
sold r la r |
mi r\fermata r4 |
la la, |
mi r8 mi |
re si, mi mi |
la[ r16 mi]-.\f re-. dod-. si,-. la,-. |
mi4 r |
R2 |
mi4\p r |
R2 |
mi4 r |
R2 |
mi4 r\fermata |
r\fermata r |
la4 la, |
mi r8 mi |
re4 r8 si, |
mi4 r |
la la, |
mi r8 sold |
la re mi mi |
la16 mi la mi sold mi sold mi |
la mi la mi si mi si mi |
dod'8 fad re mi |
la16 mi la mi sold mi sold mi |
la mi la mi si mi si mi |
dod'8 fad re mi |
la8 dod' mi' mi\cresc |
la dod' mi' mi |
la16\f si dod' re' mi'8 r |
R2 |
r4 mi8-. mid-. |
fad[ r16 re] mi!8 mi |
la mi la, r |
