\clef "treble"
<<
  \tag #'violino1 {
    mi''8.(-\sug\f dod''16) |
    \grace si'16 la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' mi'8. mi'16 |
    fad'8. sold'16 la' si' dod'' re'' |
    dod''8( si') mi''-.\p dod''-. |
    re''-. si'-. dod''-. la'-. |
    si'16( sold') mi'8 re'''-.\f sold''-. |
    la''[ r16 fad''] mi''16.\trill re''32 dod''16.\trill si'32 |
    la'8 la mi''8.(\p dod''16) |
    \grace si'16 la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' mi'8. mi'16 |
    fad'8. sold'16 la' si' dod'' re'' |
    dod''8( si') mi''8.( dod''16) |
    la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' si'8. re''16 |
    re''( dod'') re''( si') la'8 sold' |
    la'4 mi''8 dod'' |
    re'' si' dod'' la' |
    si'16( sold') mi'8 mi'' dod'' |
    re'' si' dod'' la' |
    si'16( sold') mi'( sold') si'8 mi'' |
    red''( do'' si' la') |
    sold'4\fermata mi''8.( dod''!16) |
    la'8 la' \grace re''!16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' mi'8. mi'16 |
    fad'8. sold'16 la' si' dod'' re'' |
    dod''8( si') mi''8.( dod''16) |
    la'8 la' \grace re''16 dod''8 si'16 la' |
    sold'8 sold' si'8. re''16 |
    re''( dod'') re''( si') la'8 sold' |
    la' mi' dod' r |
    la'2\fp |
    r8 sold' la'4\fp |
    r8 sold' sold' si'16 si' |
    mi'' si' dod'' la' si' sold' dod'' la' |
    sold'8 si' r16 si' la' sold' |
    sold'8\trill fad' <si fad' red''>8 fad'' |
    <si'' red'' fad'>4 r si'8.\p la'16 |
    sold'8.\trill la'16 si' sold' la' si' dod''8. si'16 |
    la'8.\trill sold'16 la'4 la'8. lad'16 |
    si'4.\trill dod''8 la'! si' |
    sold'4 r si'8. sold'16 |
    dod''4.^\markup\concat\vcenter { \sharp \musicglyph#"scripts.trill" } mi''8 red'' dod'' |
    si'4.\trill r16 la'-. sold'8[-. r16 fad'-.] |
    sold'8. la'16 sold'4( fad'8.)\trill mi'16 |
    mi'8 si' r si' |
    lad'2\fermata\fp |
    si'8 si' r si' |
    lad'2\fp\fermata |
    si'8 r r\fermata si'16 si' |
    mi'' si' dod'' la' si' sold' dod'' la' |
    si'8 sold' r16 la' sold' fad' |
    mi'4 <mi' si' mi''>8\f <mi' si' sold''> |
    <mi' si' mi''>4\fermata mi''8 mi'' |
    \grace fad''16 mi''8 re''!16 dod'' si'8-.\p re''-. |
    re''( dod'') la'-. mi'-. |
    si'-. mi'-. dod''16( mi'') re''-. dod''-. |
    dod''8( si') mi''-.\f mi''-. |
    \grace fad''16 mi''8 re''16 dod'' si'8\p re'' |
    re''( dod'') la'-. mi'-. |
    si'-. mi'-. dod''16( mi'') re''( si') |
    la'8 do'' r mi'' |
    r re'' r do'' |
    r si' r mi'' |
    r re'' r do'' |
    si' r\fermata mi''8.( dod''!16) |
    \grace si'16 la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' mi'8. mi'16 |
    fad'8 re''16( si') la'8 sold' |
    la'[ r16 mi']-.\f re'-. dod'-. si-. la-. |
    mi'4 mi''8-.\p dod''-. |
    re'' si' dod'' la' |
    si'16( sold') mi'8 mi'' dod'' |
    re'' si' dod'' la' |
    si'16( sold') mi'8 mi'' dod'' |
    re'' si' dod'' la' |
    <sold' si>4 r\fermata |
    r\fermata mi''8.( dod''16) |
    \grace si'16 la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' mi'8. mi'16 |
    fad'8. sold'16 la' si' dod'' re'' |
    dod''8( si') mi''8.( dod''16) |
    la'8 la' \grace re''16 dod''8 si'16 la' |
    \grace la'16 sold'8 sold' si'8. re''16 |
    re''16( dod'') re''( si') la'8 sold' |
    la'4 si'16( mi') mi'-. mi'-. |
    dod''16( la') la'-. la'-. re''( si') si'-. si'-. |
    mi''8 dod'' si'16( re'') sold'( si') |
    la'4 si'16( mi') mi'-. mi'-. |
    dod''( la') la'-. la'-. re''( si') si'-. si'-. |
    mi''8 dod'' si'16( re'') sold'( si') |
    la'8
  }
  \tag #'violino2 {
    r4 |
    <<
      { mi'16 mi' mi' mi' mi'4:16 | mi':16 } \\
      { dod'16 dod' dod' dod' dod'4:16 | re':16 }
    >> si4:16 |
    re'8. mi'16 fad' sold' la' si' |
    la'8( sold') dod''-.\p la'-. |
    si'-. sold'-. la'-. fad'-. |
    sold' r sold-.\f re''-. |
    dod''[ r16 re''] dod''16.\trill si'32 la'16.\trill sold'32 |
    la'8 la r4 |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16\p | re'4:16 }
    >> si4:16 |
    re'8. mi'16 fad' sold' la' si' |
    la'8( sold') r4 |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16 | re'4:16 }
    >> mi'16 mi' si' si' |
    si'( la') fad'( re') dod'8 si |
    la4 dod''8 la' |
    si' sold' la' fad' |
    sold'4 dod''8 la' |
    si' sold' la' fad' |
    sold'4 r8 sold' |
    la' red'4 red'8 |
    mi'4\fermata r |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16 | re'!4:16 }
    >> si4:16 |
    re'8. mi'16 fad' sold' la' si' |
    la'8( sold') r4 |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16 | re'4:16 }
    >> mi'16 mi' si' si' |
    si'( la') fad'( re') dod'8 si |
    dod' dod' la r |
    red'2\fp |
    r8 mi' red'4\fp |
    r8 mi' sold' sold'16 sold' |
    si' sold' la' fad' sold' mi' la' fad' |
    mi'4 r16 sold' fad' mi' |
    mi'8\trill red' <si fad' si'>\f red'' |
    <red'' fad' si>4 r4 sold'8.\p fad'16 |
    mi'8.\trill fad'16 sold' mi' fad' sold' la'8. sold'16 |
    fad'8.\trill mid'16 fad'4 fad'8. mi'16 |
    red'4. red'8 mi' fad' |
    si4 r sold'8. mi'16 |
    la'4.\trill dod''8 si' la' |
    sold'4.\trill r16 fad'-. mi'8[-. r16 red']-. |
    mi'8. fad'16 mi'4( red'8.)\trill mi'16 |
    mi'8 sold' r sold' |
    mi'2\fermata\fp |
    red'8 red' r red' |
    mi'2\fp\fermata |
    red'8 r r\fermata r |
    si'16 sold' la' fad' sold' mi' la' fad' |
    sold'8 mi' r16 fad' mi' red' |
    mi'4 <<
      { mi''8 mi'' | mi''4\fermata } \\
      { <sold' si'>8\f q | q4 }
    >> dod''8 dod'' |
    \grace re''16 dod''8 si'16 la' sold'8-.\p si'-. |
    si'( la') r16 mi'( dod' mi') |
    r mi'( re' mi') mi'( dod'') si'-. la'-. |
    la'8( sold') dod''-.\f dod''-. |
    \grace re''16 dod''8 si'16 la' sold'8-.\p si'-. |
    si'( la') r16 mi'( dod' mi') |
    r mi'( re' mi') mi'( dod'') si'( sold') |
    la'8 la' r la' |
    r si' r la' |
    r8 sold' r do' |
    r si r la |
    sold r\fermata r4 |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'!2:16\p | re'4:16 }
    >> si4:16 |
    re'8 fad'16( re') dod'8 si |
    dod'[ r16 mi']-.\f re'-. dod'-. si-. la-. |
    mi'4 dod''8-.\p la'-. |
    si' sold' la' fad' |
    sold'4 dod''8 la' |
    si' sold' la' fad' |
    sold'4 dod''8 la' |
    si' sold' la' fad' |
    mi'4 r\fermata |
    r\fermata r |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16\p | re'4:16 }
    >> si4:16 |
    re'8. mi'16 fad' sold' la' si' |
    la'8( sold') r4 |
    <<
      { mi'2:16 | mi'4:16 } \\
      { dod'2:16\p | re'4:16 }
    >> mi'16 mi' si' si' |
    si'( la') fad'( re') dod'8 si |
    dod'16( mi') mi'-. mi'-. si( sold') sold'-. sold'-. |
    la'( dod') dod'-. dod'-. si'( sold') sold'-. sold'-. |
    la'8 la' fad'16( re') si( re') |
    dod'( mi') mi'-. mi'-. si( sold') sold'-. sold'-. |
    la'( dod') dod'-. dod'-. si'( sold') sold'-. sold'-. |
    la'8 la' fad'16( re') si( re') |
    dod'8
  }
>> dod''8 mi'' mi'\cresc |
la' dod'' mi'' mi' |
la'16\f si' dod'' re'' <<
  \tag #'violino1 {
    mi''8-. dod''-. |
    re''-. si'-. dod''-. la'-. |
    si'16( sold') mi'8 re'''-. sold''-. |
    la''[ r16 fad''] mi''16.\trill re''32 dod''16.\trill si'32 |
    la'8 mi' la r |
  }
  \tag #'violino2 {
    dod''8-. la' |
    si'-. sold'-. la'-. fad'-. |
    sold' r sold-. re''-. |
    dod''[ r16 re''] dod''16.\trill si'32 la'16.\trill sold'32 |
    la'8 mi' la r |
  }
>>
