\clef "bass" r4 |
R2*7 |
r4 mi'8. dod'16 |
\grace si16 la8 la \grace re'16 dod'8 si16([ la]) |
\grace la16 sold8 sold mi8. mi16 |
fad8. sold16 la([ si]) dod'([ re']) |
dod'8 si mi'8. dod'16 |
la8 la \grace re'16 dod'8 si16([ la]) |
\grace la16 sold8 sold si8. re'16 |
re'([ dod']) re'([ si]) la8 sold |
la4 mi'8 dod' |
re' si dod' la |
si16([ sold]) mi8 mi' dod' |
re' <>^\markup\italic (amoroso) si dod' la |
si16([ sold]) mi8 r si16 mi' |
red'8 do' si la |
sold sold mi'8. dod'16 |
la8 la \grace re'!16 dod'8 si16([ la]) |
\grace la16 sold8 sold mi8. mi16 |
fad8. sold16 la([ si]) dod'([ re']) |
dod'8 si mi'8. dod'16 |
la8 la \grace re'16 dod'8 si16([ la]) |
sold8 sold si8. re'16 |
re'([ dod']) re'([ si]) la8 sold |
la4 r8 la16 si |
dod'8 la la la |
sold8 si r la16 la |
sold8 si r si16 si |
mi' si dod' la si sold dod' la |
sold8 si r16 si la sold |
sold8 fad r4 |
r r si8. la16 |
sold2\trill dod'8. si16 |
la2\trill la8. lad16 |
si4.\trill dod'8 la! si |
sold4 r si8. sold16 |
\override Script.avoid-slur = #'outside
dod'4.\trill( mi'8) red' dod' |
si4.\trill\melisma r16 la sold8 r16 fad |
sold8. la16\melismaEnd sold4 fad8.\trill mi16 |
mi4 r8 si16 si |
lad8 dod' mi' lad |
si4 r8 si16 si |
lad8 dod' mi' lad |
si4 r8\fermata si16 si |
mi' si dod' la si sold dod' la |
si8 sold r16 la sold fad |
mi8 mi r4 |
r\fermata r |
r si8 re'! |
re'([ dod']) la mi |
si mi dod' re'16 dod' |
dod'8([ si]) r4 |
r si8 re' |
re'([ dod']) la mi |
si mi dod' re'16 si |
la4 r |
re'8.([ si16]) do'8.([ la16]) |
si4 r |
re'8.([ si16]) do'8. la16 |
si4\fermata mi'8. dod'!16 |
\grace si la8 la \grace re'16 dod'8 si16([ la]) |
\grace la16 sold8 sold mi8. mi16 |
fad8 re'16([ si]) la8 sold |
la la r4 |
r mi'8 dod' |
re' si dod' la |
si16([ sold]) mi8 mi' dod' |
re' si dod' la |
si16([ sold]) mi8 mi' dod' |
re' si dod' la |
mi'4 mi~ |
mi16\fermata[ fad32( sold la si dod' re')] mi'8. dod'16 |
\grace si la8 la \grace re'16 dod'8 si16([ la]) |
\grace la16 sold8 sold mi8. mi16 |
fad8. sold16 la([ si]) dod'([ re']) |
dod'8([ si]) mi'8. dod'16 |
la4 \grace re'16 dod'8 si16([ la]) |
\grace la16 sold8 sold si8. re'16 |
re'16([ dod']) re'([ si]) la8 sold |
la4 si8 mi |
dod' la re' si |
mi' dod' si16([ re']) sold([ si]) |
la4 si8 mi |
dod' la re' si |
mi' dod' si16([ re']) sold([ si]) |
la8 dod' mi' mi |
la dod' mi' mi |
la4 r |
R2*4 |
