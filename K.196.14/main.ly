\version "2.19.80"
\include "common.ily"

\opusTitle "K.196 – N°14 – Con un vezzo all’Italiana"

\header {
  title = \markup\center-column {
    \line\italic { La finta giardiniera }
    \line { N°14 – Aria: \italic { Con un vezzo all’Italiana } }
  }
  opus = "K.196"
  date = "1775"
  copyrightYear = "2019"
}

\includeScore "K.196.14"
