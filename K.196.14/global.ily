\key la \major
\tempo "Andantino grazioso" \midiTempo#72
\time 2/4 \partial 4 s4 s2*36 \bar "||"
\time 3/4 s2.*8 \bar "||"
\time 2/4 s2*8 s4
\tempo "Andante" s4 s2*11 s4
\tempo "Allegretto" s4 s2*32 \bar "|."
