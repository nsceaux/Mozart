\clef "bass" sib,2(\p re) |
do4-. fa-. r2 |
mib4\f r8 sol32( fa mib re) do4 r8 mib32( re do sib,) |
la,4-. sib,-. r2 |
sib,4\p r r2 |
sib,4 r r2 |
do4 r r2 |
fa,4 r r2 |
fa4 r r2 |
sib,4 r r2 |
sib4..(\f fa16) re'4..( la16) |
sib8-. mi'-. fa'-. dod'-. re'-. la-. sib-. mi-. |
fa4 r fa\p fa |
fa r fa fa |
fa r r2\fermata |
sib,2(\p re) |
do4-. fa-. r2 |
mib4 r do-. r |
la,-. sib,-. r2 |
sib,4 r r2 |
sib,4 r r2 |
sib,4 r r2 |
do4 r r2 |
fa,4 r r2 |
fa4 r r2 |
fa,4 r r2 |
sib,4 sib sib, r |
fa-. fa,-. r2 |
fa4-. sib,-. r2 |
sib,4 r do r |
re r la, r |
sib, r re r |
mib8 r mib r mib r mib r |
mib2\fp mib\fp |
fa8\p fa fa fa fa fa fa fa |
sib,4 r r2 |
r16 re'\f do' sib la sib la sol fa sol fa mib re mib re do |
sib,4 r r2 |
r4 sib sol mi |
do r r2 |
r8 fa fa fa fa4 r |
re'2^"Vcl."\sf do'\sf |
sib\sf la\sf |
r8 sol-.^"Bassi"\f sib-. sol-. mi-. sol-. do-.\p mi-. |
fa fa fa fa fa fa fa fa |
re\cresc re re re re re re re |
do\f do' do do do4 r |
R1 |
do2(\p mi) |
re4-. sol-. r2 |
R1 |
fa2( la) |
sol4-. do'-. r2 |
sol,2( si,) |
do4-. mi-. r do |
fa r r2 |
do4 r r2 |
fa4 r r2 |
do4 r r2 |
fa8 fa fa fa fa4 r |
R1 |
mib'4-.\f do'-. la-. fa-. |
la,8\p la, la, la, la, la, la, la, |
sib, sib, sib, sib, sib, sib, sib, sib, |
do do do do do do sib, sib, |
la,\fp la, la, la, sib,\fp sib, sib, sib, |
do\fp do do do do do sib, sib, |
la, la la la sib\cresc sib sib sib |
do' do' do' do' do do do do |
fa\f fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
fa4 r r2 | \allowPageTurn
R1*5 |
sib4\p r r2 |
fa4 r r2 |
sib,4 r r2 |
fa4 r r2 |
re4 r la, r |
sib, r do r |
sib,1\fermata |
fa4 r\fermata fa, r\fermata |
R1*2 |
fa1\p |
sib,4 r r2 |
R1 |
fa1 |
sib,4 r fa\f r |
sib,4\p r fa\f r |
sib,4\p fa, do\cresc fa, |
re8 re fa fa re re sib, sib, |
fa,4 fa\f fa, r |
re8-.\f re-.\p mib-. fa-. sol-. re-. sol-. fa-. |
mib do re mib fa do fa mib |
re\f re\p mib fa sol re sol fa |
mib do re mib fa do fa mib |
re\f re\p mib fa sol re sol fa |
mib do re mib fa mib re do |
sib,4\f r sib, r |
sib, r sib, r |
sib, r sib, r |
sib, r sib, r |
sib,\p r r2 |
R1*3 |
mib8\p mib mib mib mib mib mib mib |
fa\cresc fa fa fa fa, fa, fa, fa, |
sib,4\f r4 r8 sol-.\p la-. sib-. |
do'-. sol-. do'-. sib-. la fa sol la |
sib fa sib la sol sol la sib |
do' sol do' sib la fa sol la |
sib\cresc fa sib la sol sol la si |
do' sol do' sib! la fa sol la |
sib4\f r sib r |
sib r sib r |
sib r sib r |
sib r sib r |
sib\p r r2 |
R1*3 |
re8\f re re re mib\p mib mib mib |
fa\cresc fa fa fa fa, fa, fa, fa, |
sib,8\f r la2\sfp( sib8) r |
mib' r la2\sfp( sib8) r |
mib' r la2\sfp( sib8) r |
mib' r la2(\sfp sib8) r |
mib8\p mib mib mib mib mib mib mib |
fa fa fa fa fa fa fa fa |
sib r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
mib1\p |
fa2( mib) |
re8\f re\p re re re re re re |
mib\f mib\p mib mib mib mib mib mib |
fa fa fa fa fa\cresc fa fa fa |
fa fa fa fa fa, fa, fa, fa, |
sib,-.\f sib,-. do-. re-. mib-. sib,-. mib-. re-. |
do-. do-. re-. mib-. fa-. do-. fa-. mib-. |
re4-. mib-. fa-. fad-. |
sol-. mib-. fa!-. fa,-. |
sib, r sib, r |
sib,2 r |
