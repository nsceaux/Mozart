\piecePartSpecs
#`((oboi #:score-template "score-oboi")
   (fagotti #:score-template "score-fagotti")
   (trombe #:tag-global ()
           #:score-template "score-trombe"
           #:instrument "Trombe in B")
   (violino1)
   (violino2)
   (viola)
   (basso)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
