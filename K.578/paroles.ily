Al -- ma gran -- de e no -- bil co -- re
le tue pa -- ri o -- gnor __ di -- sprez -- za,
le tue pa -- ri o -- gnor di -- sprez -- za, 
al -- ma gran -- de e no -- bil co -- re
le tue pa -- ri o -- gnor __ di -- sprez -- za.

So -- no da -- ma al fa -- sto av -- vez -- za
e so far -- mi, so far -- mi ri -- spet -- tar,
e so far -- mi __ ri -- spet -- tar.

Al -- ma gran -- de, al -- ma gran -- de e no -- bil co -- re
le tue pa -- ri,
le tue pa -- ri o -- gnor di -- sprez -- za.
So -- no da -- ma al fa -- sto av -- vez -- za
e so far -- mi ri -- spet -- tar,
e so far -- mi ri -- spet -- tar,
e so far -- mi ri -- spet -- tar,
e __ so far -- mi ri -- spet -- tar.

Va', fa -- vel -- la, a quell’ in -- gra -- to,
gli __ di -- rai che fi -- da io so -- no,
che fi -- da, che fi -- da,
che fi -- da io so -- no.

Ma non me -- ri -- ta per -- do -- no,
ma non me -- ri -- ta per -- do -- no,
sì mi vo -- glio ven -- di -- car,
sì mi vo -- glio ven -- di -- car,
in -- gra -- to,
in -- gra -- to non me -- ri -- ta per -- do -- no,
sì mi vo -- glio ven -- di -- car,
sì mi vo -- glio ven -- di -- car,
sì mi vo -- glio,
ah, sì mi vo -- glio, mi vo -- glio,
mi vo -- glio ven -- di -- car,
in -- gra -- to,
in -- gra -- to,
in -- gra -- to non me -- ri -- ta per -- do -- no,
sì mi vo -- glio ven -- di -- car,
sì mi vo -- glio ven -- di -- car, __
sì mi vo -- glio,
sì mi vo -- glio ven -- di -- car, __
ven -- di -- car,
sì mi vo -- glio ven -- di -- car,
sì mi vo -- glio ven -- di -- car, __
ven -- di -- car,
sì mi vo -- glio ven -- di -- car, __
ven -- di -- car,
sì mi vo -- glio, mi vo -- glio __ ven -- di -- car.
