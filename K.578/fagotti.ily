\clef "bass" R1*2 |
\tag#'fagotti <>^"a 2." mib4\f r8 sol32( fa mib re) do4 r8 mib32( re do sib,) |
la,4-. sib,-. r2 |
R1*6 |
sib4..(\f fa16) re'4..( la16) |
sib8-. mi'-. fa'-. dod'-. re'-. la-. sib-. mi-. |
fa4 r <>\p <<
  \tag #'(fagotto1 fagotti) { do'4( re'8.) do'16 | do'4-. }
  \tag #'(fagotto2 fagotti) { la4( sib8.) la16 | la4-. }
>> r4  <<
  \tag #'(fagotto1 fagotti) { do'4( re'8.) do'16 | do'4-. }
  \tag #'(fagotto2 fagotti) { la4( sib8.) la16 | la4-. }
>> r4 r2\fermata |
R1*21 |
r8 <>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa8 fa fa fa fa fa sol16 la | }
  { fa8 fa fa fa fa fa mib | }
>>
<<
  \tag #'(fagotto1 fagotti) { sib4 }
  \tag #'(fagotto2 fagotti) { re }
>> r4 r2 |
r4 <<
  \tag #'(fagotto1 fagotti) { mi'2 mi'4 | mi' }
  \tag #'(fagotto2 fagotti) { sol2 sol4 | sol }
>> r4 r2 |
r8 <<
  \tag #'(fagotto1 fagotti) { do'8 do' do' do'4 }
  \tag #'(fagotto2 fagotti) { la8 la la la4 }
>> r4 |
R1*2 |
r4 \tag#'fagotti <>^"a 2." reb'4( do') r |
R1*2 |
r8 <<
  \tag #'(fagotto1 fagotti) { do'8 do' do' do'4 }
  \tag #'(fagotto2 fagotti) { do8 do do do4 }
>> r4 |
R1*3 |
\tag#'fagotti <>^"a 2." do'4\p sol mi do |
fa r r2 |
R1*3 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1~ | do'~ | do'8-. la-. sib-. do'-. }
  { do'1~ | do'~ | do'8-. fa-. sol-. la-. }
>> <<
  \tag #'(fagotto1 fagotti) {
    re'8-. la-. re'-. do'-. |
    sib sol la sib do' sol do' sib |
    la fa' fa' fa' fa'4
  }
  \tag #'(fagotto2 fagotti) {
    sib8-. fa-. sib-. la-. |
    sol mi fa sol la mi la sol |
    fa la la la la4
  }
>> r4 |
<<
  \tag #'(fagotto1 fagotti) { fa'1 | mib'! | }
  \tag #'(fagotto2 fagotti) { fa1 | do' | }
  { s s\f }
>>
R1*5 |
<>\p <<
  \tag #'(fagotto1 fagotti) { \voiceOne fa'2~ \oneVoice }
  \tag #'(fagotto2 fagotti) \new Voice { \voiceTwo do'4.( dod'8) }
>> <>\cresc <<
  \tag #'(fagotto1 fagotti) {
    fa'8( sol'16 fa') mi'8-. re'-. |
    do'( fa') la-. do'-. do'( sib) la-. sol-. |
  }
  \tag #'(fagotto2 fagotti) {
    re'8( mi'16 re') do'8-. sib-. |
    la( do') fa-. la-. la( sol) fa-. mi-. |
  }
>>
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa4 }
  { fa4 }
>> <<
  \tag #'(fagotto1 fagotti) {
    la8. la16 la4 do' |
    la la8. la16 la4 do' |
    la
  }
  \tag #'(fagotto2 fagotti) {
    fa8. fa16 fa4 la |
    fa fa8. fa16 fa4 la |
    fa
  }
>> r4 r2 |
R1*2 |
\tag#'fagotti <>^"a 2." si2(\p re') |
do'4-. sol'-. r2 |
la2( do') |
sib!4-. fa'-. r2 |
R1*5 |
R1^\fermataMarkup |
R1^\fermataMarkup | \allowPageTurn
r2 <>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'8 mib' fa' fa' | }
  { mib'8 mib' re' re' | }
>>
<<
  \tag #'(fagotto1 fagotti) { mib'4 }
  \tag #'(fagotto2 fagotti) { do' }
>> r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'8-. fa'-. sol'-. sol'-. | fa'4 }
  { fa'8-. fa'-. do'-. do'-. | fa4 }
>> r4 r2 |
r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'8 mib' fa' fa' | }
  { mib'8 mib' re' re' | }
>>
<<
  \tag #'(fagotto1 fagotti) { mib'4 }
  \tag #'(fagotto2 fagotti) { do' }
>> r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'8 fa' sol' sol' | fa'4 }
  { fa'8 fa' do' do' | fa4 }
>> r4 r2 |
R1*2 |
\tag#'fagotti <>^"a 2." sib2 do'\cresc |
re'4 fa' re' sib |
fa fa\f fa r |
<<
  \tag #'(fagotto1 fagotti) { sib4 }
  \tag #'(fagotto2 fagotti) { re }
>> r4 r2 |
R1 |
<<
  \tag #'(fagotto1 fagotti) { sib4 }
  \tag #'(fagotto2 fagotti) { re }
>> r4 <>\p <<
  \tag #'(fagotto1 fagotti) { si2( | do'4) }
  \tag #'(fagotto2 fagotti) { re2( | mib4) }
>> r4 <<
  \tag #'(fagotto1 fagotti) { la2( | sib!4) fa'-. re'-. }
  \tag #'(fagotto2 fagotti) { do2( | re4) re'-. si-. }
  { s2 | s4\f s\p }
>> r4 |
r <<
  \tag #'(fagotto1 fagotti) { mib'4-. do'-. }
  \tag #'(fagotto2 fagotti) { do'-. la-. }
>> r4 |
<>\f <<
  \tag #'(fagotto1 fagotti) {
    sib!4-. s sib-. s |
    sib s sib s |
    sib s sib s |
    sib s sib s |
  }
  \tag #'(fagotto2 fagotti) {
    re-. s re-. s |
    re s re s |
    re s re s |
    re s re s |
  }
  \ru#4 { s4 r s r }
>>
\tag#'fagotti <>^"a 2." sib8(\p fa') re'-. do'-. sib( re') sib-. la-. |
sol8( sib) sol-. fa-. mib( mib') do'-. sib-. |
la( do') la-. sol-. fa( la) fa-. mib-. |
re( fa) re-. do-. sib,( sib) sol-. fa-. |
mib4 r r2 |
r <>\cresc <<
  \tag #'(fagotto1 fagotti) { fa'8( mib') re'-. do'-. | }
  \tag #'(fagotto2 fagotti) { re'8( do') sib-. la-. | }
>>
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib4 }
  { sib }
>> r4 r2 |
R1*5 |
\ru#4 <<
  \tag #'(fagotto1 fagotti) { sib4 s sib s | }
  \tag #'(fagotto2 fagotti) { re s re s | }
  { s r s r }
>>
<<
  \tag #'(fagotto1 fagotti) {
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    sib2\p do'4-. re'-. |
    mib'4.( fa'8) sol'4-. r |
    fa2 sol4-. la-. |
    sib4.( do'8) re'4-. r4 |
    R1 |
  }
  \tag #'fagotto2 R1*5
>>
r2 <>\cresc <<
  \tag #'(fagotto1 fagotti) { fa'8( mib') re'-. do'-. | }
  \tag #'(fagotto2 fagotti) { re'8( do') sib-. la-. | }
>>
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib8 }
  { sib }
>> r \ru#3 <<
  \tag #'(fagotto1 fagotti) { mib'2( re'8) s | do' s }
  \tag #'(fagotto2 fagotti) { do'2( sib8) s | la s }
  { s2\sfp s8 r | s r }
>> <>\sfp <<
  \tag #'(fagotto1 fagotti) { mib'2( re'8) }
  \tag #'(fagotto2 fagotti) { do'2( sib8) }
>> r8 |
R1*2 |
r4 \ru#3 <<
  \tag #'(fagotto1 fagotti) { mib'2( re'8) s | do' s }
  \tag #'(fagotto2 fagotti) { do'2( sib8) s | la s }
  { s2\sfp s8 r | s r }
>> <>\sfp <<
  \tag #'(fagotto1 fagotti) { mib'2( re'8) }
  \tag #'(fagotto2 fagotti) { do'2( sib8) }
>> r8 |
R1*5 |
<>\cresc <<
  \tag #'(fagotto1 fagotti) { re'4 re'8. re'16 do'4 do'8. do'16 | }
  \tag #'(fagotto2 fagotti) { sib4 sib8. sib16 la4 la8. la16 | }
>>
<>\f \tag#'fagotti <>^"a 2." sib8 sib,-. do-. re-. mib-. sib,-. mib-. re-. |
do-. do-. re-. mib-. fa-. do-. fa-. mib-. |
re4-. mib-. fa-. fad-. |
sol-. mib-. fa!-. fa,-. |
sib, r sib, r |
sib,2 r |
