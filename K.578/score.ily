\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trombe in B }
        shortInstrumentName = \markup Tr.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Soprano
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \vccbInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\break s1*5\pageBreak
        s1*8\break s1*6\break s1*5\pageBreak
        s1*5\break s1*5\break s1*5\pageBreak
        s1*8\break s1*7\break s1*6\pageBreak
        s1*5\break s1*7\break \grace s8 s1*7\pageBreak
        s1*7\break s1*8\break s1*7\pageBreak
        s1*7\break s1*7\break s1*7\pageBreak
        s1*7\break s1*7\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
