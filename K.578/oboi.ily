\clef "treble" R1*2 |
\tag#'oboi <>^"a 2." mib''4\f r8 sol''32( fa'' mib'' re'') do''4 r8 mib''32( re'' do'' sib') |
la'4-. sib'-. r2 |
<<
  \tag #'(oboe1 oboi) {
    \tag#'oboi <>^\markup\concat { 1 \super o }
    fa''2.(\p mib''8 re'') |
    mib''( re'' do'' sib') sib'( do''16 re'' do''8 sib') |
    sib'(\trill la') la'4 r2 |
    mib''2.( re''8 do'') |
    do''( re'' mib'' mi'') fa''4.( do''8) |
    mib''!( re'') re''4 r2 |
  }
  \tag #'oboe2 R1*6
>>
<>\f <<
  \tag #'(oboe1 oboi) {
    sib''4..( fa''16) re'''4..( la''16) |
    sib''8-.
  }
  \tag #'(oboe2 oboi) {
    sib'4..( fa'16) re''4..( la'16) |
    sib'8-.
  }
>> \tag#'oboi <>^"a 2." mi''-. fa''-. dod''-. re''-. la'-. sib'-. mi'-. |
fa'4 r fa''\p fa'' |
fa'' r fa'' fa'' |
fa'' r r2\fermata |
R1*21 |
r8 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8 fa'' fa'' fa'' fa'' fa'' sol''16 la'' | sib''4 }
  { fa''8 fa'' fa'' fa'' fa'' fa'' mib'' | re''4 }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { sol''2 sol''4 | sol'' }
  \tag #'(oboe2 oboi) { mi''2 mi''4 | mi'' }
>> r4 r2 |
r8 <<
  \tag #'(oboe1 oboi) { la''8 la'' la'' la''4 }
  \tag #'(oboe2 oboi) { do''8 do'' do'' do''4 }
>> r4 |
R1*2 |
r4 <<
  \tag #'(oboe1 oboi) { sib''8. sib''16 sib''4 }
  \tag #'(oboe2 oboi) { sol''8. sol''16 sol''4 }
>> r4 |
R1*2 |
r8 <<
  \tag #'(oboe1 oboi) { do'''8 do''' do''' do'''4 }
  \tag #'(oboe2 oboi) { mi''8 mi'' mi'' mi''4 }
>> r4 |
R1*10 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1~ | do''~ | do''8 la'' la'' la'' la''4 }
  { do''1~ | do''~ | do''8 fa'' fa'' fa'' fa''4 }
>> r4 |
R1 |
<>\f <<
  \tag #'(oboe1 oboi) { mib''!1 | }
  \tag #'(oboe2 oboi) { do'' }
>>
R1*5 |
<>\p <<
  \tag #'(oboe1 oboi) { \voiceOne fa''2~ \oneVoice }
  \tag #'(oboe2 oboi) \new Voice { \voiceTwo do''4.( dod''8) }
>> <>\cresc <<
  \tag #'(oboe1 oboi) {
    fa''8( sol''16 fa'') mi''8-. re''-. |
    do''( fa'') la''-. do'''-. do'''( sib'') la''-. sol''-. |
  }
  \tag #'(oboe2 oboi) {
    re''8( mi''16 re'') do''8-. sib'-. |
    la'( do'') fa''-. la''-. la''( sol'') fa''-. mi''-. |
  }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 }
  { fa''4 }
>> <<
  \tag #'(oboe1 oboi) {
    fa''8. fa''16 fa''4 la'' |
    fa'' fa''8. fa''16 fa''4 la'' |
    fa''
  }
  \tag #'(oboe2 oboi) {
    la'8. la'16 la'4 do'' |
    la' la'8. la'16 la'4 do'' |
    la'
  }
>> r4 r2 |
R1 |
\tag#'oboi <>^"a 2." fa''2(\p lab'') |
sol''4-. re'''-. r2 |
mib''2( solb'') |
fa''4-. do'''-. r2 |
R1*6 |
R1^\fermataMarkup |
R1^\fermataMarkup | \allowPageTurn
r2 <>\p <<
  \tag #'(oboe1 oboi) {
    sib'8 sib' si' si' |
    do''4 s do''8-. do''-. mi''-. mi''-. |
    fa''4
  }
  \tag #'(oboe2 oboi) {
    sol'8 sol' sol' sol' |
    sol'4 s la'8-. la'-. sib'-. sib'-. |
    la'4
  }
  { s2 | s4 r }
>> r4 r2 |
r2 <<
  \tag #'(oboe1 oboi) {
    sib'8 sib' si' si' |
    do''4 s do''8 do'' mi'' mi'' |
    fa''4
  }
  \tag #'(oboe2 oboi) {
    sol'8 sol' sol' sol' |
    sol'4 s la'8 la' sib' sib' |
    la'4
  }
  { s2 | s4 r }
>> r4 r2 |
R1*2 |
\tag#'oboi <>^"a 2." sib'2 do''\cresc |
re''4 fa'' re'' sib' |
<<
  \tag #'(oboe1 oboi) { fa''4 fa'' fa'' s | sib'' }
  \tag #'(oboe2 oboi) { fa'4 la' la' s | re'' }
  { s4 s2\f r4 }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    sib''4 s si''2( |
    do'''4) s la''2( |
    sib''!4) s s si''-. |
    do'''-. s s la''-. |
    sib''!-. s sib''-. s |
    sib'' s sib'' s |
    sib'' s sib'' s |
    sib'' s sib'' s |
    sib''
  }
  \tag #'(oboe2 oboi) {
    re''4 s re''2( |
    mib''4) s do''2( |
    re''4) s s re''-. |
    mib''-. s s do''-. |
    re''-. s re''-. s |
    re'' s re'' s |
    re'' s re'' s |
    re'' s re'' s |
    re''
  }
  { s4 r s2\p |
    s4 r s2 |
    s4\f r r s |
    s r r s |
    s r s r |
    s r s r |
    s r s r |
    s r s r |
    s4\p }
>> r4 r2 |
R1*4 |
r2 <>\cresc <<
  \tag #'(oboe1 oboi) { fa''8( mib'') re''-. do''-. }
  \tag #'(oboe2 oboi) { re''8( do'') sib'-. la'-. }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 }
  { sib' }
>> r4 r2 |
R1*5 |
\ru#4 <<
  \tag #'(oboe1 oboi) { sib''4 s sib'' s | }
  \tag #'(oboe2 oboi) { re''4 s re'' s | }
  { s r s r | }
>>
<<
  \tag #'(oboe1 oboi) {
    \tag#'oboi <>^\markup\concat { 1 \super o }
    sib'2\p do''4-. re''-. |
    mib''4.( fa''8) sol''4-. r |
    fa'2 sol'4-. la'-. |
    sib'4.( do''8) re''4-. r |
    R1 |
  }
  \tag #'oboe2 R1*5
>>
r2 <>\cresc <<
  \tag #'(oboe1 oboi) { fa''8( mib'') re''-. do''-. | }
  \tag #'(oboe2 oboi) { re''8( do'') sib'-. la'-. | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'8 }
  { sib' }
>> r8 \ru#3 <<
  \tag #'(oboe1 oboi) { mib''2( re''8) s | do'' s }
  \tag #'(oboe2 oboi) { do''2( sib'8) s | la' s }
  { s2\sfp s8 r | s r }
>> <>\sfp <<
  \tag #'(oboe1 oboi) { mib''2( re''8) }
  \tag #'(oboe2 oboi) { do''2( sib'8) }
>> r8 |
R1*2 |
r4 \ru#3 <<
  \tag #'(oboe1 oboi) { mib''2( re''8) s | do'' s }
  \tag #'(oboe2 oboi) { do''2( sib'8) s | la' s }
  { s2\sfp s8 r | s r }
>> <>\sfp <<
  \tag #'(oboe1 oboi) { mib''2( re''8) }
  \tag #'(oboe2 oboi) { do''2( sib'8) }
>> r8 |
R1*5 |
<>\cresc <<
  \tag #'(oboe1 oboi) { re''4 re''8. re''16 do''4 do''8. do''16 | }
  \tag #'(oboe2 oboi) { sib'4 sib'8. sib'16 la'4 la'8. la'16 | }
>>
<>\f \tag#'oboi <>^"a 2." sib'8-. sib'-. do''-. re''-. mib''-. sib'-. mib''-. re''-. |
do''-. do''-. re''-. mib''-. fa''-. do''-. fa''-. mib''-. |
re''4-. <<
  \tag #'(oboe1 oboi) {
    sol''4-. fa''-. la''-. |
    sib''-. sol''-. fa''-. la'-. |
    sib' s sib'' s |
    sib''2
  }
  \tag #'(oboe2 oboi) {
    sib'4-. re''-. mib''-. |
    re''-. mib''-. re''-. mib'-. |
    re' s re'' s |
    re''2
  }
  { s2. | s1 | s4 r s r | }
>> r2 |
