\version "2.19.80"
\include "common.ily"

\opusTitle "K.578 – Alma grande e nobil core"

\header {
  title = \markup\center-column {
    Arie
    \italic Alma grande e nobil core
  }
  subtitle = "für Sopran mit Begleitung des Orchesters"
  opus = "K.578"
  date = "1789"
  copyrightYear = "2019"
}

\includeScore "K.578"
