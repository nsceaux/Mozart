\clef "alto" sib2(\p re') |
do'4-. fa'-. r2 |
mib'4\f r8 sol'32( fa' mib' re') do'4 r8 mib'32( re' do' sib) |
la4-. sib-. r2 |
r8 <fa sib>\p q q q q q q |
r8 q q q q q q q |
r8 <fa la> q q q q q q |
r q q q q q q q |
r q q q q q q q |
r <fa sib> q q q q q q |
sib'4..(\f fa'16) re''4..( la'16) |
sib'8-. mi'-. fa'-. dod'-. re'-. la-. sib-. mi-. |
fa4 r fa\p fa |
fa r fa fa |
fa r r2\fermata |
sib2(\p re') |
do'4-. fa'-. r2 |
mib'4 r8 sol'32( fa' mib' re') do'4 r8 mib'32( re' do' sib) |
la4-. sib-. r2 |
r8 fa fa fa fa fa fa fa |
r fa fa fa fa fa fa fa |
r fa fa fa fa fa fa fa |
r <fa la> q q q q q q |
r fa fa fa fa fa fa fa |
r fa fa fa fa fa fa fa |
r fa fa fa fa fa fa fa |
fa4 r r2 |
mib'4( re') r2 |
mib'4( re') r2 |
fa1~ |
fa~ |
fa4 sib-. r re'-. |
sol8-. r do'-. r mib'-. r sol'-. r |
sol2\fp sol\fp |
mib'4(\p re'4.) fa'8( mib' do') |
re'4 r r2 |
r16 re''\f do'' sib' la' sib' la' sol' fa' sol' fa' mib' re' mib' re' do' |
sib4 r r2 |
r4 mi'8.( do'16) sib4-. sib'8.( sol'16) |
mi'4 r r2 |
r8 do' do' do' do'4 r |
re'2\sf do'\sf |
sib\sf la\sf |
sol4\f sol'2 do'4\p |
do'8 do' do' do' do' do' do' do' |
re'2:16\cresc re':16 |
do'8\f do'' do' do' do'4 r |
fa'8\p fa' fa' fa' fa' fa' fa' fa' |
fa' fa' fa' fa' mi' mi' mi' mi' |
mi'( re') re'( do') si( la sol si) |
do'( re' mi' fa') sol'( mi') mi'( do') |
do'4 r r2 |
sol'8 sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol'-. sol-. do'-. mi'-. mi'-. sol'-. do'-. mi'-. |
fa'4 r r2 |
do'4 r r2 |
fa'4 r r2 |
do'4 r r2 |
do'8 do' do' do' do'4 r |
R1 |
<>\f \ru#4 { la16 do' fa' do' } |
fa1\p |
fa4 sib2( re'4) |
do'8 do' do' do' do' do' do' do' |
do'\fp la la la sib\fp sib sib sib |
do'\fp do' do' do' do' do' do' do' |
do' la la la sib\cresc sib sib sib |
do' do' do' do' do' do' do' sib |
la\f do'4 do' do' do'8~ |
do' do'4 do' do' do'8~ |
do'4 r r2 |
mib'!1\sf |
reb'2(\p do') |
si4 r r2 | \allowPageTurn
R1*2 |
sib!8( fa sib fa sib fa sib fa) |
do'( fa do' fa do' fa la fa) |
sib fa sib fa sib fa sib fa |
do' fa do' fa do' fa la fa |
r fa fa fa r fa fa fa |
r fa fa fa r fa fa fa |
fa1\fermata |
fa4 r\fermata mib r\fermata |
sib8-.\p sib-. re'-. re'-. mib'4 r |
do'8 do' mib' mib' fa'4 r |
fa1 |
sib8 sib re' re' mib'4 r |
do'8 do' mib' mib' fa'4 r |
fa1 |
sib4 r fa'\f r |
sib\p r fa'\f r |
sib\p fa do'\cresc fa |
re'8 re' fa' fa' re' re' sib sib |
fa4 fa'\f fa r |
re'8-.\f re'-.\p mib'-. fa'-. sol'-. re'-. sol'-. fa'-. |
mib' do' re' mib' fa' do' fa' mib' |
re'\f re'\p mib' fa' sol' re' sol' fa' |
mib' do' re' mib' fa' do' fa' mib' |
re'\f re'\p mib' fa' sol' re' sol' fa' |
mib' do' re' mib' fa' mib' re' do' |
sib4\f r sib r |
sib r sib r |
sib r sib r |
sib r sib r |
sib\p r r2 |
R1*3 |
mib'8 mib' mib' mib' mib' mib' mib' mib' |
fa'\cresc fa' fa' fa' fa fa fa fa |
sib4\f r r8 sol-.\p la-. sib-. |
do'-. sol-. do'-. sib-. la-. fa-. sol-. la-. |
sib fa sib la sol sol la sib |
do' sol do' sib la fa sol la |
sib\cresc fa sib la sol sol la si |
do' sol do' sib! la fa sol la |
sib4\f r sib r |
sib r sib r |
sib r sib r |
sib r sib r |
sib\p r r2 |
R1*3 |
re'8\f re' re' re' mib'\p mib' mib' mib' |
fa'\cresc fa' fa' fa' fa fa fa fa |
sib8\f r la2(\sfp sib8) r |
mib'8 r la2(\sfp sib8) r |
mib'8 r la2(\sfp sib8) r |
mib'8 r la2(\sfp sib8) r |
sol1\p |
fa2. mib4 |
re8 r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
mib' r la2(\sfp sib8) r |
sol1\p( |
fa) |
re'8\f re'\p re' re' re' re' re' re' |
mib'\f mib'\p mib' mib' mib' mib' mib' mib' |
sib sib sib sib sib\cresc sib sib sib |
sib sib sib sib la la la la |
sib-.\f sib-. do'-. re'-. mib'-. sib-. mib'-. re'-. |
do'-. do'-. re'-. mib'-. fa'-. do'-. fa'-. mib'-. |
re'4-. mib'-. sib'-. mib''-. |
re''-. sib'-. sib'-. fa'-. |
fa' r sib' r |
sib2 r |
