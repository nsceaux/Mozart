\clef "treble" sib'2(\p re'') |
do''4-. fa''-. r2 |
mib''4\f r8 sol''32( fa'' mib'' re'') do''4 r8 mib''32( re'' do'' sib') |
la'4-. sib'-. r2 |
<<
  \tag #'violino1 {
    R1*2 |
    r8 la'16(\p sib' do'' re'' mib'' fa'') mib''8-. re''16( mib'' fa'' sol'' la'' sib'') |
    la''4 r r2 |
    R1 |
    r8 sib'16( do'' re'' mib'' fa'' sol'') fa''8-. fa''16( sol'' la'' sib'' do''' re''') |
  }
  \tag #'violino2 {
    r8 re'\p re' re' re' re' re' re' |
    r re' re' re' re' re' re' re' |
    r mib' mib' mib' mib' mib' mib' mib' |
    r do' do' do' do' do' do' do' |
    r do' do' do' do' do' do' do' |
    r re' re' re' re' re' re' re' |
  }
>>
sib''4..(\f fa''16) re'''4..( la''16) |
sib''16 sib'' mi'' mi'' fa'' fa'' dod'' dod'' re''8:16 la':16 sib':16 mi':16 |
fa'4 r <>\p <<
  \tag #'violino1 {
    do'4( re'8.) do'16 |
    do'4-. r do'4( re'8.) do'16 |
    do'4-.
  }
  \tag #'violino2 {
    la4( sib8.) la16 |
    la4-. r la4( sib8.) la16 |
    la4-.
  }
>> r4 r2\fermata |
sib'2(\p re'') |
do''4-. fa''-. r2 |
mib''4 r8 sol''32( fa'' mib'' re'') do''4 r8 mib''32( re'' do'' sib') |
la'4-. sib'-. r2 |
<<
  \tag #'violino1 {
    r8 re' re' re' re' re' re' re' |
    r8 re' re' re' re' re' re' re' |
    r8 re' re' re' re' re' re' re' |
    r8 la'16( sib' do'' re'' mib'' fa'') mib''8-. re''16( mib'' fa'' sol'' la'' sib'') |
    la''8-. do' do' do' do' do' do' do' |
    r do' do' do' do' do' do' do' |
    r do' do' do' do' do' do' do' |
    re'4 sib''16( fa'' sib'' re''') sib''4-. r |
    r sib''16( fa'' sib'' re''') sib''4-. r |
    r sib''16( fa'' sib'' re''') sib''4-. r |
  }
  \tag #'violino2 {
    r8 sib sib sib sib sib sib sib |
    r8 sib sib sib sib sib sib sib |
    r8 sib sib sib sib sib sib sib |
    r mib' mib' mib' mib' mib' mib' mib' |
    r la la la la la la la |
    r la la la la la la la |
    r la la la la la la la |
    sib4 r r2 |
    la'4( sib') r2 |
    la'4( sib') r2 |
  }
>>
r8 re'16( dod' re' dod' re' sib) la8 mib'16( re' mib' re' mib' do') |
sib8 fa'16( mi' fa' mi' fa' re') do'8 sol'16( fad' sol' fad' sol' mib') |
re'4-. <<
  \tag #'violino1 {
    fa'4-. r sib'-. |
    mib'8-. r sol'-. r do''-. r mib''-. r |
    \ru#2 { sol''8(\fp re'' mib'' do'') } |
    do''4(\p sib'4.) re''8( do'' la') |
    sib'4 r16 re'''\f do''' sib'' la'' sib'' la'' sol'' fa'' sol'' fa'' mib'' |
    re''8 fa''4 fa'' fa'' sol''16 la'' |
    sib''4 r r2 |
    r8 mi'16 sol' do''8 sol'16 do'' mi''8 do''16 mi'' sol''8 mi''16 sol'' |
    sib''8 r mi'4(\p fa' sol') |
    r8 la'16\f do'' fa'' do'' fa'' la'' fa''4 r |
    fa''16(\fp re'' la' sib' la' sib' la' sib') mi''(\fp do'' sold' la' sold' la' sold' la') |
    re''(\fp sib' fad' sol'! fad' sol' fad' sol') do''(\fp la' mi' fa' mi' fa' mi' fa') |
    sib'8\f sib'4 sib' sib'' sib'8\p |
    la' la' la' la' la' la' la' la' |
    la'2:16\cresc si':16 |
    do''8\f <mi'' do'''> do' do' do'4 r |
    fa''2(\p la'') |
    sol''4-. do'''-. r do''4~ |
    do''8( si') si'4 r fa''~ |
    fa''8( mi'' re'' do'') do''4.( sol'8) |
    la'4 r r2 |
    do''2( mi'') |
    re''4-. sol''-. r sol'8( sol'') |
    fa''( mi'' re'' do'') do''4.( sol'8) |
    la'-. la'-. sib'-. do''-. re''-. la'-. re''-. do''-. |
    sib'-. sol'-. la'-. sib'-. do''-. sol'-. do''-. sib'-. |
    la' la' sib' do'' re'' la' re'' do'' |
    sib' sol' la' sib' do'' sol' do'' sib' |
    la'16( sib' do'' sib' la' sib' do'' sib') la'4 r |
    R1 |
    mib''4-.\f do''-. la'-. fa'-. |
    mib'2( dod') |
    re'8 r fa'' r re'' r sib' r |
    la' la' la' la' sol' sol' sol' sol' |
    fa''8\fp fa''4 fa''8 fa''-.\fp fa''4( re''8) |
    do''8-.\fp do''4( la'8) do''( sib') la'( sol') |
    fa'' fa''4 fa''4*1/2( s8\cresc sol''16 fa'') mi''8-. re''-. |
    do''( fa'') la'-. do''-. do''( sib') la'-. sol'-. |
    fa'4\f r8 la'16 do'' fa''( do'' la') do''-. fa''( do'' fa'') la''-. |
    fa''8 fa'' r la'16 do'' fa''( do'' la') do''-. fa''( do'' fa'') la''-. |
    fa''8 fa'[\p fa' fa'] fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' solb' solb' solb' solb' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa'4 re'2 re'4( |
    mib') mib'2 mib'4( |
    re') re'2 re'4( |
    mib') mib'2( do'4) |
    r8 sib sib sib r do' do' do' |
    r re' re' re' r mib' mib' mib' |
    re'1\fermata |
    re'4 r\fermata do' r\fermata |
    re''8-.\p re''-. sib'-. sib'-. sol'4 r |
    mib''8-. mib''-. do''-. do''-. la'4 r |
    la'1 |
    re''8 re'' sib' sib' sol'4 r |
    mib''8 mib'' do'' do'' la'4 r |
    la'1 |
    sib'4.( fa'8) mib''4.(\f la'8) |
    sib'4.(\p fa'8) mib''4.(\f la'8) |
    sib'\p sib'
  }
  \tag #'violino2 {
    re'4-. r fa'-. |
    do'8-. r mib'-. r sol'-. r do''-. r |
    \ru#2 { mib'8(\fp si do' sol) } |
    do'4(\p sib!4.) re'8( do' la) |
    sib4 r16 sib''\f la'' sol'' fa'' sol'' fa'' mib'' re'' mib'' re'' do'' |
    sib' fa'' mib'' re'' do'' re'' do'' sib' la' sib' la' sol' fa' sol' fa' mib' |
    re'4 r r2 |
    r8 do'16 mi' sol'8 mi'16 sol' do''8 sol'16 do'' mi''8 do''16 mi'' |
    sol''8 r do'4(\p re' mi') |
    r8 <fa' la'>\f q q q4 r |
    fa'2\sf mi'\sf |
    re'\sf do'\sf |
    re'4(\f reb') do' mi'8\p sol' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa'2:16\cresc fa':16 |
    mi'8\f <do'' mi''> do' do' do'4 r |
    R1 |
    sol'8\p sol' sol' sol' sol' sol' sol' sol' |
    fa' fa'4( mi'8 re' do' si re') |
    mi'( fa' sol' la') sib'!( sol') sol'( mi') |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' mi' mi' do' do' |
    do' do'( si do' re' mi' fa' re') |
    do'-. mi'-. sol'-. sib'!-. sol'-. sib'-. mi'-. sol'-. |
    fa'-. fa'-. sol'-. la'-. sib' fa' sib' la' |
    sol' mi' fa' sol' la' mi' la' sol' |
    fa' fa' sol' la' sib' fa' sib' la' |
    sol' mi' fa' sol' la' mi' la' sol' |
    fa'16( sol' la' sol' fa' sol' la' sol') fa'4 r |
    \ru#4 { la16 do' fa' do' } |
    <>\f \ru#4 { la16 do' fa' do' } |
    mib'2(\p dod') |
    re'8 r re'' r sib' r sol' r |
    fa'8 fa' fa' fa' fa' fa' mi' mi' |
    do''8\fp do''4 do''8 re''-.\fp re''4( sib'8) |
    la'8-.\fp la'4( fa'8) la'( sol') fa'( mi') |
    fa' do'4( dod'8) re'8(\cresc mi'16 re') do'8-. sib-. |
    la( do') fa'-. la'-. la'( sol') fa'-. mi'-. |
    fa'\f <fa' la>4 q q q8~ |
    q q4 q q q8~ |
    q4 r r2 |
    mib'1\sf |
    reb'2(\p do') |
    fa'8 fa' fa' fa' fa' fa' fa' fa' |
    mib' mib' mib' mib' mib' mib' mib' mib' |
    mib' mib' mib' mib' mib' mib' mib' mib' |
    re'4 sib2 sib4( |
    do') do'2 do'4( |
    sib) sib2 sib4( |
    do') do'2( la4) |
    r8 sib sib sib r do' do' do' |
    r sib sib sib r la la la |
    sib1\fermata |
    sib4 r la r\fermata |
    fa'8\p fa' fa' fa' mib'4 r |
    sol'8 sol' sol' sol' fa'4 r |
    <mib' do'>1 |
    fa'8 fa' fa' fa' mib'4 r |
    sol'8 sol' sol' sol' fa'4 r |
    <mib' do'>1 |
    re'8 fa' re' sib la\f do' fa' mib' |
    re'\p fa' re' sib la\f do' fa' mib' |
    re'\p re'
  }
>> fa'8 fa' do''\cresc do'' fa' fa' |
re'' re'' fa'' fa'' re'' re'' sib' sib' |
fa''4 <fa' la>\f q r |
re'8-.\f re'-.\p mib'-. fa'-. sol'-. re'-. sol'-. fa'-. |
mib' do' re' mib' fa' do' fa' mib' |
re'\fp re'\p mib' fa' sol' re' sol' fa' |
mib' do' re' mib' fa' do' fa' mib' |
re'\f re'\p mib' fa' sol' re' sol' fa' |
mib' do' re' mib'
<<
  \tag #'violino1 {
    fa'8 fa' sol' la' |
    \ru#8 { sib'8(\fp mib'') re''-. do''-. } |
    sib'2\p do''4-. re''-. |
    mib''4.( fa''8) sol''4-. r |
    fa'2 sol'4-. la'-. |
    sib'4.( do''8) re''4-. r |
    do''2 mib''4.( sol''8) |
    sol''8(\cresc fa'') mib''-. re''-. re''( do'') sib'-. la'-. |
    sib'8-.\f
  }
  \tag #'violino2 {
    fa'8 do' fa' mib' |
    \ru#8 { re'8(\fp sol') fa'-. mib'-. } |
    re'4\p r r2 |
    R1*3 |
    sol'2 do''4.( mib''8) |
    mib''(\cresc re'') do''-. sib'-. fa'( mib') re'-. do'-. |
    re'-.\f
  }
>> re''8-.\p mib''-. fa''-. sol''-. re''-. sol''-. fa''-. |
mib'' do'' re'' mib'' fa'' do'' fa'' mib'' |
re'' re'' mib'' fa'' sol'' re'' sol'' fa'' |
mib'' do'' re'' mib'' fa'' do'' fa'' mib'' |
re''\cresc re'' mib'' fa'' sol'' re'' sol'' fa'' |
mib'' do'' re'' mib'' <<
  \tag #'violino1 {
    fa''8 mib'' re'' do'' |
    \ru#8 { sib'(\fp mib'') re''-. do''-. } |
    sib'(\p re'') fa''-. mib''-. re''-. do''-. sib'-. lab'-. |
    sol'( sib') sol'-. fa'-. mib'( mib'') do''-. sib'-. |
    la'!( sib') do''-. sib'-. la'-. sol'-. fa'-. mib'-. |
    re'( fa') sol'-.\cresc la'-. sib'-. do''-. re''-. mib''-. |
    fa''2\f sol''4.\p sol''8 |
    sol''(\cresc fa'') mib''-. re''-. re''( do'') sib'-. la'-. |
    sib'8\f r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    mib'2.(\p fa'8 sol') |
    re'4 fa'( mib') do' |
    sib8 r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    do'' r <mib'' fa'>2\sfp( re''8) r |
    mib'8\p fa' mib' re' mib' fa' sol' mib' |
    re' mib' fa' re' do' re' mib' do' |
    fa''\f fa''\p fa'' fa'' re'' re'' sib' sib' |
    sol''\f sol''\p sol'' sol'' mib'' mib'' do'' do'' |
    sib'4 r <sib fa' re''>\cresc r |
    <re' sib' fa''> r <fa' do'' la''> r |
    <fa' re'' sib''>8\f
  }
  \tag #'violino2 {
    fa''4 fa'8 mib' |
    \ru#8 { re'(\fp sol') fa'-. mib'-. } |
    re'4\p r r2 |
    R1*3 |
    sib'8\f sib'4 sib'4*1/2 s8\p mib''4 mib''8 |
    mib''(\cresc re'') do''-. sib'-. fa'( mib') re'-. do'-. |
    re'8\f fa'( do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    do'2.(\p re'8 mib') |
    sib4 re'( do') la |
    sib8 fa'( do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    la'( fa' do''\sfp fa' do'' fa' sib' fa') |
    do'8\p re' do' si do' re' mib' do' |
    sib! do' re' sib la sib do' la |
    sib\f sib\p sib sib re' re' fa' fa' |
    do'\f do'\p do' do' mib' mib' sol' sol' |
    re' re' re' re' re'\cresc re' re' re' |
    re' re' re' re' do' do' do' do' |
    sib\f
  }
>> sib'8-. do''-. re''-. mib''-. sib'-. mib''-. re''-. |
do''-. do''-. re''-. mib''-. fa''-. do''-. fa''-. mib''-. |
re''4-. <<
  \tag #'violino1 {
    sol''4-. fa''-. la''-. |
    sib''-. sol''-. fa''-. la'-. |
    sib'4 r <fa' re'' sib''> r |
  }
  \tag #'violino2 {
    sib'4-. re''-. do''-. |
    sib'-. mib''-. re''-. <mib' do'>-. |
    <sib re'>4 r <sib fa' re''> r |
  }
>>
sib2 r |
