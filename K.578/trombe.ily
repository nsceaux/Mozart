\clef "treble" \transposition sib
R1*10 |
<>\f <<
  \tag #'(tromba1 trombe) { do''2 mi'' | do''4 }
  \tag #'(tromba2 trombe) { do'2 mi' | do'4 }
>> r4 r2 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r4 \tag#'trombe <>^"a 2." sol'4\p sol' |
sol' r sol' sol' |
sol' r r2\fermata |
R1*11 |
r4 <>\p <<
  \tag #'(tromba1 trombe) { do''8. do''16 do''4 }
  \tag #'(tromba2 trombe) { mi'8. mi'16 mi'4 }
>> r4 |
r4 <<
  \tag #'(tromba1 trombe) { do''8. do''16 do''4 }
  \tag #'(tromba2 trombe) { mi'8. mi'16 mi'4 }
>> r4 |
r4 <<
  \tag #'(tromba1 trombe) { do''8. do''16 do''4 }
  \tag #'(tromba2 trombe) { mi'8. mi'16 mi'4 }
>> r4 |
R1*7 |
r8 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 sol' sol' sol' sol' sol' sol' | }
  { sol'8 sol' sol' sol' sol' sol' sol' | }
>>
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { do' }
>> r4 r2 |
r4 \tag#'trombe <>^"a 2." re''2 re''4 |
re'' r r2 |
r8 sol' sol' sol' sol'4 r |
R1*2 |
<<
  \tag #'(tromba1 trombe) { do''2~ do''4 }
  \tag #'(tromba2 trombe) { do'2~ do'4 }
>> r4 |
R1*2 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' re'' re''4 }
  { re''8 re'' re'' re''4 }
>> r4 |
R1*14 |
<>\f <<
  \tag #'(tromba1 trombe) { sol'1 | }
  \tag #'(tromba2 trombe) { sol | }
>>
R1*7 |
\tag#'trombe <>^"a 2." sol'4\f sol'8. sol'16 sol'4 sol' |
sol' sol'8. sol'16 sol'4 sol' |
sol' r r2 |
R1*5 |
<>\p <<
  \tag #'(tromba1 trombe) { sol'1~ | sol'~ | sol'~ | sol' | }
  \tag #'(tromba2 trombe) { sol1~ | sol~ | sol~ | sol | }
>>
R1*2 |
R1^\fermataMarkup |
R1^\fermataMarkup | \allowPageTurn
R1*2 |
\tag#'trombe <>^"a 2." sol'1\p |
R1*2 |
sol'1 |
R1*3 |
r4 <<
  \tag #'(tromba1 trombe) {
    sol''4 mi'' do'' |
    sol'' sol' sol' s |
    do''
  }
  \tag #'(tromba2 trombe) {
    sol'4 mi' do' |
    sol' sol sol s |
    mi'
  }
  { s2.\cresc | s4 s2\f r4 | }
>> r4 r2 |
R1 |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { mi' }
>> r4 r2 |
R1 |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { mi' }
>> r4 r2 |
R1 |
<>\f <<
  \tag #'(tromba1 trombe) {
    do''4 s do'' s |
    do'' s do'' s |
    do'' s do'' s |
    do'' s do'' s |
    do''
  }
  \tag #'(tromba2 trombe) {
    mi'4 s mi' s |
    mi' s mi' s |
    mi' s mi' s |
    mi' s mi' s |
    mi'
  }
  { \ru#4 { s r s r } s4\p }
>> r4 r2 |
R1*4 |
r2 \tag#'trombe <>^"a 2." sol'2\cresc |
do'4\f r r2 |
R1*5 |
\ru#4 <<
  \tag #'(tromba1 trombe) { do''4 s do'' s | }
  \tag #'(tromba2 trombe) { mi' s mi' s | }
  { s r s r | }
>>
R1*5 |
r2 \tag#'trombe <>^"a 2." sol'\cresc |
do'8\f r <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
R1*2 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
r4 <>\sfp <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
R1*5 |
<<
  \tag #'(tromba1 trombe) { sol'4 sol'8. sol'16 sol'4 sol' | do'' }
  \tag #'(tromba2 trombe) { sol sol8. sol16 sol4 sol | do' }
  { s1\cresc | s4\f }
>> r4 r2 |
R1 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4-. mi''-. re''-. | do''-. }
  { do'-. sol'-. re''-. | do''-. }
>> <<
  \tag #'(tromba1 trombe) {
    do''4-. do''-. sol'-. |
    sol' s mi' r |
    mi'2 
  }
  \tag #'(tromba2 trombe) {
    do'4-. do'-. sol-. |
    do' s do' s |
    do'2
  }
  { s2. | s4 r s r | }
>> r2 |
