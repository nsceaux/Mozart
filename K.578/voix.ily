\clef "soprano/treble" R1*14 |
R1^\fermataMarkup |
<>^\markup\character (Madama) sib'2 re'' |
do''4 fa'' r fa'' |
mib''2 do'' |
la'4 sib' r2 |
fa''2. mib''8([ re'']) |
re''2. do''8([ sib']) |
sib'2~ sib'8([ do''16 re''] do''8) sib' |
la'4 la' r2 |
mib''2. re''8([ do'']) |
do''2~ do''8([ re'']) mib''([ mi'']) |
fa''2. do''4 |
re'' re'' r sib'8 re'' |
re''([ do'']) sib'4 r8 fa' sib' re'' |
re''([ do'']) sib'4 r re''8 sib' |
fa''1~ |
fa''~ |
fa''8[\melisma fa''16( mi''] fa''[ mi'' fa'' re''] sib'8)[ re''16( dod''] re''[ dod'' re'' sib'] |
sol'8)[ sol'16( fad'] sol'8)[ do''!16( si'] do''8)[ mib''16( re''] mib''8)[ sol''16( fad''] |
sol''8)[ re'' mib'' do'']\melismaEnd sol''([ re'']) mib''([ do'']) |
do''4( sib'!2) la'4 |
sib' sib' r2 |
R1 |
sib'2. re''4 |
mi'4 mi' r do'' |
do''2. sol'4 |
la' la' r do''8 do'' |
fa''4.( sib'8) la'4 r8 la' |
re''4. sol'8 fa'4. do''8 |
sib'4 r r sib'8 sib' |
la'4.( do''8) do''4.( fa''8) |
fa''2. si'4 |
do'' r r2 |
R1 |
r2 r4 do''8 do'' |
do''([ si']) si'4 r fa''8 fa'' |
fa''([ mi'']) re''([ do'']) do''4. sol'8 |
la'4 la' r2 |
do''2 mi'' |
re''4 sol'' r sol'8 sol'' |
fa''([ mi'']) re''([ do'']) do''4. sol'8 |
la'4 la' r2 |
do''2. do''4 |
re''8([ do'']) do''4 r do'' |
do'2. sib'4 |
la' la' r do''8 do'' |
mib''!4 do'' la' fa' |
mib' r r2 |
mib''2 dod'' |
re''4.( mi''16[ fa''] mi''8[ re'']) do''!([ sib']) |
la'4.( sib'8) sol'2\trill |
fa''4 r fa''4. re''8 |
do''4. la'8 do''([ sib']) la'([ sol']) |
fa''4 r fa''8([ sol''16 fa''] mi''8) re'' |
do''([ fa'']) la'([ do'']) do''([ sib']) la'([ sol']) |
fa'4 r r2 |
R1*3 |
fa''2 r4 r8 fa'' |
fa''4 sol' r sol' |
mib''2. mib''4 |
mib'' fa' r2 |
re''~ re''8([ fa'']) re''([ sib']) |
\appoggiatura sib'8 la'2 r4 do'' |
re''2~ re''8([ fa'']) re''([ sib']) |
la'4 la' r la' |
sib'4.( fa'8) do''4 r8 la' |
re''4.( sib'8) mib''4 r8 do'' |
<>^\markup\italic a piacere fa''4.( sol''16[ fa'']) mib''8([ re'']) do''([ sib']) |
re''2\fermata do''4 r\fermata |
R1 |
r2 r4 do''8 do'' |
fa''([ mi'']) mib''([ re'']) fa''([ mib'']) re''([ do'']) |
sib'4 sib' r2 |
r r4 do''8 do'' |
fa''([ mi'']) mib''([ re'']) fa''([ mib'']) re''([ do'']) |
sib'4 sib' mib''4. la'8 |
sib'4 fa' mib''4. la'8 |
sib'4 r do''4. do''8 |
re''4 fa'' re'' sib' |
fa'' r r2 |
R1 |
r2 r4 fa'' |
fa'' sib' r2 |
r r4 fa'' |
fa'' sib' r sol'' |
mib'' do'' la' fa'' |
re'' sib' r sib'8 sib' |
sib'([ mib'']) re''([ do'']) sib'([ mib'']) re''([ do'']) |
sib'4 r r sib'8 sib' |
sib'([ mib'']) re''([ do'']) sib'( mib'']) re''([ do'']) |
sib'2 do''4 re'' |
mib''4.( fa''8) sol''4 r |
fa'2 sol'4 la' |
sib'4.( do''8) re''4 r8 re'' |
do''2 mib''4. sol''8 |
sol''([ fa'']) mib''([ re'']) re''([ do'']) sib'([ la']) |
sib'4 r r2 |
r r4 fa'' |
re'' sib' r sol'' |
mib'' do'' r fa'' |
re'' sib' r sol'' |
mib'' do'' la' fa'' |
re'' sib' r sib'8 sib' |
sib'([ mib'']) re''([ do'']) sib'([ mib'']) re''([ do'']) |
sib'4 r r sib'8 sib' |
sib'([ mib'']) re''([ do'']) sib'([ mib'']) re''([ do'']) |
sib'([ re'' fa'' mib'']) re''([ do'']) sib'([ lab']) |
sol'4.( fa'8) mib'4 do''8 sib' |
la'!([ sib']) do''([ sib']) la'([ sol']) fa'([ mib']) |
re'([ fa' sol' la']) sib'([ do'']) re''([ mib'']) |
fa''2 sol''4. sol''8 |
sol''8([ fa'']) mib''([ re'']) re''([ do'']) sib'([ la']) |
sib'4 r r2 |
r4 mib''2 re''4 |
do''4.\melisma mib''8\melismaEnd mib''4 r |
r mib''2 re''4 |
do''2.( re''8[ mib'']) |
sib'2 la' |
sib'4 r r2 |
r4 mib''2 re''4 |
do''4.( mib''8) mib''4 r |
r mib''2 re''4 |
do''2.( re''8[ mib'']) |
sib'2 la' |
fa''2 re''4 sib' |
sol''2 mib''4 do'' |
sib'( re'') fa''2~ |
fa''4( sol''8.[ fa''16]) \appoggiatura fa''16 mib''4 re''8([ do'']) |
sib'4 r r2 |
R1*5 |
