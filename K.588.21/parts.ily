\piecePartSpecs
#`((clarinetti #:score-template "score-clarinetti"
               #:tag-global clarinetti
               #:instrument , #{ \markup\center-column { Clarinetti in B } #})
   (fagotti #:score-template "score-fagotti"
            #:tag-global all)
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in Es")
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Duetto: TACET } #}))
