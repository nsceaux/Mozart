\clef "treble" \transposition mib
<>\p <<
  \tag #'corno1 {
    do''4.~ |
    do''~ |
    do''~ |
    do''8. sol''16 mi'' do'' |
    sol'4.~ |
    sol'8( re'' do'') |
    R4. |
    sol''8.( mi''16 fa'' re'') |
    do''4.~ |
    do''~ |
    do''~ |
    do''8. sol''16 mi'' do'' |
    sol'4.~ |
    sol'8( re'' do'') |
    fa''( mi'' re'') |
    re''( do'') r |
    r sol'' sol'' |
    r fad'' fad'' |
    r fad'' fad'' |
    r sol'' sol'' |
    sol'4.~ |
    sol'~ |
    sol' |
    sol'4\f~ sol'16\p\fermata r |
    do''4.\p~ |
    do''~ |
    do''~ |
    do''8. sol''16 mi'' do'' |
    sol'4.~ |
    sol'8( re'' do'') |
    R4.*2 |
    re''4.~ |
    re''~ |
    re''~ |
    re''4 r8 |
    mi''4.~ |
    mi''8 re''4 |
    r8 re'' re'' |
    re''( fad'' sol'') |
    r re'' re'' |
    re''16 sol' sol' sol' sol' sol' |
    sol'4.~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'16 sol' sol' sol' sol' sol' |
    sol'8 sol' r |
    do''4.~ |
    do''~ |
    do''~ |
    do''8. sol''16 mi'' do'' |
    sol'4.~ |
    sol'8( re'' do'') |
    fa''( mi'' re'') |
    do''( re'' mi'') |
    fa''4 mi''8 |
    fa''16 r mi'' r re'' r |
    do''4 r8 |
  }
  \tag #'corno2 {
    do'4.~ |
    do'~ |
    do'~ |
    do'8. sol'16 mi' do' |
    sol4. |
    R4.*3 |
    do'4.~ |
    do'~ |
    do'~ |
    do'8. sol'16 mi' do' |
    sol4. |
    R4. |
    r8 sol' sol' |
    sol'8. sol'16 mi' do' |
    sol8 r r |
    R4.*3 |
    sol4.~ |
    sol~ |
    sol |
    sol4\f~ sol16\p\fermata r |
    do'4.\p~ |
    do'~ |
    do'~ |
    do'8. sol'16 mi' do' |
    sol4. |
    R4.*3 |
    re''4.~ |
    re''~ |
    re''~ |
    re''4 r8 |
    si'8( do'' re'') |
    re''16( do'') re''4 |
    r8 re'' re'' |
    re'' r r |
    r re'' re'' |
    sol'16 sol sol sol sol sol |
    sol4.~ |
    sol~ |
    sol~ |
    sol~ |
    sol16 sol sol sol sol sol |
    sol8 sol r |
    do'4.~ |
    do'~ |
    do'~ |
    do'8. sol'16 mi' do' |
    sol4. |
    R4. |
    r8 sol' sol' |
    sol'4.~ |
    sol'8( re'' do'') |
    re''16 r do'' r sol' r |
    \sug mi'4 r8 |
  }
>>