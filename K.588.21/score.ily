\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Clarinetti in B }
        shortInstrumentName = "Cl."
      } <<
        \new Staff <<
          \keepWithTag #'clarinetti \global
          \keepWithTag #'clarinetto1 \includeNotes "clarinetti"
        >>
        \new Staff <<
          \keepWithTag #'clarinetti \global
          \keepWithTag #'clarinetto2 \includeNotes "clarinetti"
        >>
      >>
      \new GrandStaff \with { \fagottiInstr } <<
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'fagotto1 \includeNotes "fagotti"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'fagotto2 \includeNotes "fagotti"
          \origLayout {
            s4.*11\break s4.*9\break s4.*9\pageBreak
            s4.*12\break s4.*10\break
          }
        >>
      >>
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Corni in Es }
        shortInstrumentName = \markup Cor.
      } <<
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'corno1 \includeNotes "corni"
        >>
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'corno2 \includeNotes "corni"
        >>
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ferrando
      shortInstrumentName = \markup\character Fer.
    } \withLyrics <<
      \keepWithTag #'all \global
      \keepWithTag#'voix1 \includeNotes "voix"
    >> \keepWithTag#'voix1 \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Guglielmo
      shortInstrumentName = \markup\character Gu.
    } \withLyrics <<
      \keepWithTag #'all \global
      \keepWithTag#'voix2 \includeNotes "voix"
    >> \keepWithTag#'voix2 \includeLyrics "paroles"
  >>
  \layout { }
  \midi { }
}
