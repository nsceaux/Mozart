\clef "treble" \transposition sib
<>_\markup { \dynamic p \italic dol. }
<<
  \tag #'clarinetto1 {
    do''8( la' re''16. do''32) |
    do''8( la'' sol'') |
    fa''( mi''16 re'' do'' re'') |
    re''8( do'') r |
    do''4.~ |
    do''8( mi'' fa'') |
    sol''( sib''16 la'' sol'' fa'') |
    fa''8( mi'') r |
    do''8( la' re''16. do''32) |
    do''8( la'' sol'') |
    fa''( mi''16 re'' do'' re'') |
    re''8( do'') r |
    do''4.~ |
    do''8( mi'' fa'') |
    sol''16.([ la''32] fa''8 mi'') |
    mi''8( fa'') r |
    mi''8.( do''16 mi'' sol'') |
    sol''32( fa'' mi'' re'') re''8 r |
    fa''8.( re''16 fa'' la'') |
    la''32([ sol'' fa'' mi'']) mi''8 r |
    do''8.([ si'32 do''] re'' do'' fa'' mi'') |
    mi''8.([ red''32 mi''] fa'' mi'' la'' sol'') |
    sol''8.([ fad''32 sol''] la'' sol'' do''' sib''!) |
    sib''8.([\f sol''16]) mi''\fermata\p r |
    do''8(\p la' re''16. do''32) |
    do''8( la'' sol'') |
    fa''( mi''16 re'' do'' re'') |
    re''8( do'') r |
    do''4.~ |
    do''8( mi'' fa'') |
    sol''( sib''16 la'' sol'' fa'') |
    fa''8( mi'') r |
    r mi'' mi'' |
    mi''16.([ fa''32]) re''16-. sol'( si' re'') |
    fa''8 fa'' fa'' |
    fa''16.([ sol''32]) mi''16-. sol'([ do'' mi'']) |
    sol''8( fa'' mi'') |
    mi''16([ fa'']) fa''8.( mi''16) |
    sol''16([ fa''] mi''8 re'') |
    sol''( fa'' mi'') |
    fa''32([ sol'' la'' fa''] mi''8 re'') |
    do''4 r8 |
    r16 do'' do'' do'' do'' do'' |
    r re'' re'' re'' re'' re'' |
    r mi'' mi'' mi'' mi'' mi'' |
    r fa''-. fa''-. fa''( la'' fa'') |
    do''8.([ re''32 do''] sib'16 la') |
    do''16.([ la'32]) sol'16( la' sib' si') |
    do''8( la' re''16. do''32) |
    do''8( la'' sol'') |
    fa''( mi''16 re'' do'' re'') |
    re''8( do'') r |
    do''4.~ |
    do''8( mi'' fa'') |
    sol''16.([ la''32] fa''8 mi'') |
    fa''( mi'' re'') |
    do''( sib'' la'') |
    sol''16 r fa'' r mi'' r |
    \sug fa''4 r8 |
  }
  \tag #'clarinetto2 {
    la'8( fa' sib'16. la'32) |
    la'8( fa'' mi'') |
    re''( do''16 sib' la' sib') |
    sib'8( la') r |
    r sol'( la') |
    sib'4( la'8) |
    sib'( re''16 do'' sib' la') |
    la'8( sol') r |
    la'8( fa' sib'16. la'32) |
    la'8( fa'' mi'') |
    re''( do''16 sib' la' sib') |
    sib'8( la') r |
    r sol'( la') |
    sib'4( la'8) |
    re''( do'' sib') |
    sib'( la') r |
    \ru#3 { sol32[ mi' do' mi'] } |
    \ru#6 { sol[ re' si re'] } |
    \ru#3 { sol32[ mi' do' mi'] } |
    do'8 r r |
    do''8.([ si'32 do''] re'' do'' fa'' mi'') |
    mi''8.([ red''32 mi''] fa'' mi'' la'' sol'') |
    sol''8.([\f mi''16]) sib'!16\fermata\p r |
    la'8(\p fa' sib'16. la'32) |
    la'8( fa'' mi'') |
    re''( do''16 sib' la' sib') |
    sib'8( la') r |
    r sol'( la') |
    sib'4( la'8) |
    sib'8( re''16 do'' sib' la') |
    la'8( sol') r |
    r do'' do'' |
    do''16.([ re''32]) si'8 r |
    re'' re'' re'' |
    re''16.([ mi''32]) do''16-. mi'([ sol' do'']) |
    mi''8( re'' dod'') |
    dod''16([ re'']) re''8.( do''16) |
    mi''16([ re''] do''8 si') |
    mi''( re'' do'') |
    re''32([ mi'' fa'' re''] do''8 si') |
    do''4 r8 |
    r16 sol' sol' sol' sol' sol' |
    r lab' lab' lab' lab' lab' |
    r do'' do'' do'' do'' do'' |
    r do''-. do''-. do''( fa'' do'') |
    la'8.([ sib'32 la'] sol'16 fa') |
    la'16.([ fa'32]) mi'16( fa' sol' sold') |
    la'8( fa' sib'16. la'32) |
    la'8( fa'' mi'') |
    re''( do''16 sib' la' sib') |
    sib'8( la') r |
    r sol'( la') |
    sib'4( la'8) |
    re''( do''4)~ |
    do''8( mi' fa') |
    sol'( mi'' fa'') |
    re''16 r do'' r sib' r |
    \sug la'4 r8 |
  }
>>