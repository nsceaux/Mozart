<<
  \tag #'voix1 {
    \clef "tenor/G_8" R4.*23 |
    R4.^\fermataMarkup |
    sib8([ sol]) do'16.([ sib32]) |
    sib8([ sol']) fa' |
    mib'([ re'16 do']) sib([ do']) |
    do'8 sib r |
    sib4 sib8 |
    sib([ re']) mib' |
    fa'([ lab'16 sol']) fa'([ mib']) |
    mib'16.([ fa'32]) re'8 r |
    r re' re' |
    re'16.([ mib'32]) do'8 r |
    mib' mib' mib' |
    mib'16.([ fa'32]) re'8 r |
    fa'([ mib']) re' |
    re'16([ mib']) mib'8. re'16 |
    fa'([ mib' re'8]) do' |
    fa'([ mib']) re' |
    mib'32([ fa' sol' mib'] re'8) do' |
    sib4 r8 |
    sib4 r16 sib |
    sib([ do']) do'8 r |
    re'4 re'8 |
    re'16([ mib']) mib'8 sol'16 mib' |
    sib8. do'32([ sib]) lab16 sol |
    sib16.([ sol32]) fa8 r |
    sib8([ sol]) do'16.([ sib32]) |
    sib8([ sol']) fa' |
    mib'([ re'16 do']) sib([ do']) |
    do'8 sib r |
    sib4 sib8 |
    sib([ re']) mib' |
    fa'16.([ sol'32 mib'8]) re' |
    mib' re' do' |
    sib([ lab']) sol' |
    fa'16.([ sol'32 mib'8]) re' |
    mib'4 r8 |
  }
  \tag #'voix2 {
    \clef "bass" R4.*23 |
    R4.^\fermataMarkup |
    sol8([ mib]) lab16.([ sol32]) |
    sol8([ mib']) re' |
    do'([ sib16 lab]) sol([ lab]) |
    lab8 sol r |
    r re mib |
    lab4 sol8 |
    fa([ do'16 sib]) lab([ sol]) |
    sol16.([ lab32]) fa8 r |
    r sib sib |
    sib16.([ do'32]) la8 r |
    do' do' do' |
    do'16.([ re'32]) sib8 r |
    re'([ do']) si |
    si16([ do']) do'8. sib16 |
    re'([ do' sib8]) la |
    re'([ do']) sib |
    do'32([ re' mib' do'] sib8) la |
    sib4 r8 |
    sib4 r16 sib |
    sib([ la]) la8 r |
    lab!4 lab8 |
    lab16([ sol]) sol8 mib'16 sib |
    sol8. lab32([ sol]) fa16 mib |
    sol16.([ mib32]) re8 r |
    sol([ mib]) lab16.([ sol32]) |
    sol8([ mib']) re' |
    do'([ sib16 lab]) sol([ lab]) |
    lab8 sol r |
    r re mib |
    lab4 sol8 |
    lab16.([ sib32 sol8]) fa |
    sib lab sol |
    fa([ re']) mib' |
    do'16.([ lab32 sol8]) fa |
    mib4 r8 |
  }
>>