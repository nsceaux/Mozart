\version "2.19.80"
\include "common.ily"

\opusTitle "K.588 – N°21 – Secondate, aurette amiche"

\header {
  title = \markup\center-column {
    \line\italic { Così fan tutte }
    \line { N°21 – Duetto: \italic { Secondate, aurette amiche } }
  }
  opus = "K.588"
  date = "1790"
  copyrightYear = "2019"
}

\includeScore "K.588.21"
