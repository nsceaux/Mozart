\clef "bass"
<>_\markup { \dynamic p \italic dol. }
<<
  \tag #'fagotto1 {
    sib8( sol do'16. sib32) |
    sib8( sol' fa') |
    mib'( re'16 do' sib do') |
    do'8( sib) r |
    r8 re'( mib') |
    fa'( si do') |
    lab8( fa16 sol lab la) |
    sib!4 r8 |
    sib8( sol do'16. sib32) |
    sib8( sol' fa') |
    mib'( re'16 do' sib do') |
    do'8( sib) r8 |
    r8 re'( mib') |
    fa'( si do') |
    lab8( sib!) sib-. |
    mib4 r8 |
    r8 re' re' |
    r mib' mib' |
    r do' do' |
    r re' re' |
    \ru#9 { sib,16 sib } |
    fa'4\f~ fa'16\fermata\p r |
    sib8(\p sol do'16. sib32) |
    sib8( sol' fa') |
    mib'( re'16 do' sib do') |
    do'8( sib) r |
    r re'( mib') |
    fa'8( si do') |
    lab( fa16 sol lab la) |
    sib!8
  }
  \tag #'fagotto2 {
    sol8( mib lab16. sol32) |
    sol8( mib' re') |
    do'( sib16 lab sol lab) |
    lab8( sol) r8 |
    sib,4.~ |
    sib,8( si, do) |
    lab,8( fa,16 sol, lab, la,) |
    sib,!4 r8 |
    sol8( mib lab16. sol32) |
    sol8( mib' re') |
    do'( sib16 lab sol lab) |
    lab8( sol) r8 |
    sib,4.~ |
    sib,8( si, do) |
    lab,8( sib,!) sib,-. |
    mib,4 r8 |
    sib,4 r8 |
    fa,4 r8 |
    fa4 r8 |
    sib,4 r8 |
    R4.*3 |
    re'4\f~ re'16\p\fermata r |
    sol8(\p mib lab16. sol32) |
    sol8( mib' re') |
    do'( sib16 lab sol lab) |
    lab8( sol) r |
    sib,4.~ |
    sib,8( si, do) |
    lab,( fa,16 sol, lab, la,) |
    sib,!8
  }
>> r16 sib fa re |
sib,8 re sib, |
fa fa, r |
fa la fa |
sib sib, r |
<<
  \tag #'fagotto1 {
    si8( do' re') |
    re'16( do' la8 sib!) |
    sol( fa mib) |
    re( fad sol) |
    sol( fa! mib') |
    re'4 r8 |
    r16 re' re' re' re' re' |
    r mib' mib' mib' mib' mib' |
    r fa' fa' fa' fa' fa' |
    r mib'-. mib'-. mib'( sol' mib') |
    sib8.( do'32 sib lab16[ sol]) |
    sib16.([ sol32]) fa16( sol lab la) |
    sib8( sol do'16. sib32) |
    sib8( sol' fa') |
    mib'( re'16 do' sib do') |
    do'8( sib) r |
    r re'( mib') |
    fa'( si do') |
    lab( sib! lab) |
    sol( fa mib) |
    re( si do') |
    lab16 r sib r sib, r |
    \sug sib,4 r8 |
  }
  \tag #'fagotto2 {
    sol4. |
    do8( la, sib,) |
    mib( fa mib) |
    re( fad sol) |
    mib( fa! fa,) |
    sib,4 r8 |
    r16 sib sib sib sib sib |
    r la la la la la |
    r lab! lab lab lab lab |
    r sol-. sol-. sol( mib' sib) |
    sol8.( lab32 sol fa16[ mib]) |
    sol16.([ mib32]) re16( mib fa fad) |
    sol8( mib lab16. sol32) |
    sol8( mib' re') |
    do'( sib16 lab sol lab) |
    lab8( sol) r |
    sib,4.~ |
    sib,8( si, do) |
    lab,( sib,! lab,) |
    sol,( fa, mib,) |
    re,( si, do) |
    lab,16 r sib, r sib, r |
    \sug mib,4 r8 |
  }
>>
