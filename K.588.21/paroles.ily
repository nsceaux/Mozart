Se -- con -- da -- te, au -- ret -- te a -- mi -- che,
se -- con -- da -- te i miei de -- si -- ri,
e por -- ta -- te i miei so -- sp -- iri
al -- la de -- a di que -- sto cor, di que -- sto cor.
Voi, che u -- di -- ste mil -- le vol -- te
il te -- nor del -- le mie pe -- ne,
ri -- pe -- te -- te al ca -- ro be -- ne
tut -- to quel che u -- di -- ste al -- lor,
tut -- to quel che u -- di -- ste al -- lor.
