\piecePartSpecs
#`((corni #:tag-global ()
          #:score-template "score-corni"
          #:instrument ,(if (eqv? (ly:get-option 'en-re) #t)
                             "Corni in D"
                             "Corni in Es"))
   (oboi #:tag-global all #:notes "oboi")
   (clarinetti #:score "score-clarinetti")
   (violino1 #:notes "violino1" #:tag-global all)
   (violino2 #:notes "violino2" #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all))
