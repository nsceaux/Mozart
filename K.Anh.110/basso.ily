\clef "bass" mib4(\f^"tasto solo" sol sib sib,) |
mib8(\p sol _"legato" sib mib) re( fa sib re) |
do(\fp mib sol sol,) lab,( fa sib, sib) |
mib(\p ^"Solo" sol sib mib) re( fa sib re) |
do( mib sol do) sol,( sib, mib sol,) |
lab,( do mib la,) sib,( re fa si,) |
do( mib sol do) lab( sol fa mib) |
re( do sib, lab,) sol,( mib lab, fa) |
sib,( mib re sib,) mib,-. mib(\fp fa sib,) |
r8 mib(\fp fa sib,) r mib(\fp fa sib,) |
mib( sol sib mib) re( fa sib re) |
do(\f mib la do) sib,(\> fa sib sol)\! |
mi(\p sib fa sib) sol( sib fa sib) |
mi( sib fa sib) sol( sib fa sib) |
mib!( sol sib mib) mi(\< sol sib mi)\! |
fa( sib re' fa') fa( mi fa fa,) |
sib,4 r4 fa^\mf fa, |
sib,8^"unis" reb'(\> sib lab! sol sib sol fa)\! |
mi(\p sol do mi) fa( lab fa mib) |
reb( fa reb do) si,( re sol, si,) |
do( do' lab! sol) fad( do' sol do') |
lab!( do' sol do') fad( do' sol do') |
lab!( do' sol do') fad( do' sol do') |
la(\< re' sib re')\! do'(\> la re' re)\! |
sol( sib reb'\fp fa) mi( sol sib mi) |
fa( lab dob'\fp mib) re!( fa lab re) |
mib( sol sib mib) re( fa sib re) |
mib(\p sol sib mib) re( fa sib re) |
do( mib sol do) sol,( sib, mib sol,) |
lab,( do mib la,) sib,( re fa si,) |
do( mib sol do) lab( sol fa mib) |
re( do sib, lab,) sol,( mib lab, fa) |
sib,( mib re sib,) mib,( mib\fp fa sib,) |
r8 mib(\fp fa sib,) r mib(\fp fa sib,) |
mib( sol sib reb) do( mib lab do) |
sib,(\f reb sol sib,) lab,(\> mib do sib,)\! |
la,\p( mib sib, mib) do( mib sib, mib) |
la,( mib sib, mib) do( mib sib, mib) |
lab,!( do mib lab,) la,(\< do mib la,)\! |
sib,( mib sol sib) sib,(\f la, sib,) sib,-. |
mib4 r sib sib, |
mib8\p mib-. mib-. mib-. mib-. mib-. mib-. mib-. |
mib mib mib mib mib mib mib mib |
<mib sol reb'>8 q q q <do' lab mib> q <mib lab re'> q |
<mib sol mib'> r sib4(\p mib8) r sib4( |
mib8) r sib,4(\pp mib,8) r r4 |
