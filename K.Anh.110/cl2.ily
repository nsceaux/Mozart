\clef "treble" \transposition sib
\transpose sib do' {
R1*7 |
r2 r8 sol'( sib'16 lab' sol' fa') |
mib'4 sol'16( fa' mib' re') mib'8 r r4 |
R1*5 |
r2 sol''2^( |
fa''8.) re''16 re''8[ do''16 sib'] sib'4 la'\trill |
sib' r r2 |
R1*4 |
r4 r8 do'' re''4( mi'' |
fa''4) r8 do'' re''4( mi'' |
fad'' sol'') mib''!( re''8 do'') |
sib'8 r sol''2 sol'4( |
fa'8-.) r fa''2 fa'4( |
mib')-. r4 r2 |
mib'4 r r2 |
R1*2 |
r2 do''8( sib' lab' sol') |
fa'2 mib'8 sol' sib'16 lab' sol' fa' |
mib'16( fa' fad' sol') sol'( fa' mib' re') mib'8 r r4 |
R1*2 |
sol'2 lab'4 r4 |
r2 r4 r8 mib'' |
fa''4( sol'' lab'') r8 mib' |
do''2 mib'' |
sib'4 r4 r2 |
sol''4~ sol''16( fad'' fa'' mib'') mib''4~ mib''16( re'' do'' re'') |
mib''4 r sol'2( |
lab'8) do''( lab' fa') mib' r r4 |
sol'2( lab'8) do''(\p lab' fa') |
mib'8 r fa''8.( mib''16-.) mib''8-. r re''8.( mib''16-.) |
mib''8 r re''8-.\pp r sib'-. r r4 |
}