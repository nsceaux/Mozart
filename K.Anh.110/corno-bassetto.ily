\clef "treble" \transposition fa
R1*6 |
r2 sol''4\p sol' |
do''' la sib8 sib'' \appoggiatura la''16 sol''8( fa''16 mib'') |
re''( mib'' mi'' fa'') fa''( mib'' re'' do'') sib'4 r |
re'16( fa' sib' re'') mib''8.(\fp la'16) do''( sib') sib'8-. r4 |
sib4 re'' fa'' la |
sol mi'' fa'' r |
R1 |
r2 fa''4 \appoggiatura sol''32 fa''16([ mi''32 re'']) \appoggiatura re'' do''16([ sib'32 la']) |
sol'2(\< lab') |
\override Script.avoid-slur = #'outside
la'!8\> do'( fa' la')\! do''4( sib')\trill |
la' r r2 |
R1 |
r2 do' |
mib''( re''4.) re''32( do'' sib' do'') |
sib'4 r r2 |
R1 |
sol''4 \appoggiatura la''32 sol''16([ fa''32 mib'']) \appoggiatura mib''32 re''16([ do''32 sib']) la'4 r |
r8 la' la''4~ la''16( sol'' sib'' sol'' fa''8 mi'') |
re''4 r lab''8.(\sfp sol''16) sol''8 r |
r2 solb''8.(\sfp fa''16) fa''8 r |
R1*4 |
r2 sol''4 sol' |
do''' la sib8 sib'' \appoggiatura la''16 sol''8( fa''16 mib'') |
re''( mib'' mi'' fa'') fa''( mib'' re'' do'') sib'4 r |
re'16( fa' sib' re'') mib''8.(\fp la'16) do''( sib') sib'8-. r4 |
sib4 re'' mib'' sol |
fa lab''( sol'') r |
R1 |
r2 sib'4 \appoggiatura do''32 sib'16([ la'32 sol']) \appoggiatura sol'32 fa'16([ mib'32 re']) |
do'2 reb'' |
re''!4~ re''16( dod'' do'' sib') sib'4~ sib'16( la' sol' la') |
fa''4\p~ fa''16( mi'' mib'' re'') re''4~ re''16( do'' sib' do'') |
sib'4 r re''2\<( |
mib''8)\! sol''(\p mib'' do'') sib' r r4 |
re''2\<( mib''8\!) sol''(\p mib'' do'') |
sib'16 fa( sib re' fa'8) r r16 fa( sib re' fa'8) r |
r16 fa\pp( sib re' fa'8) r sib' r r4 |

