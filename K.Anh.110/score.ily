\score {
  \notemode <<
    \new StaffGroup <<
      \new Staff \with { instrumentName = "Oboe" } <<
        \keepWithTag #'all \global \includeNotes "oboi"
      >>
      \new Staff \with { instrumentName = "Clarinetto in B" } <<
        \keepWithTag #'clarinetto \global \includeNotes "clarinetto"
      >>
      \new Staff \with { instrumentName = "Corno di bassetto" } <<
        \keepWithTag #'corno-bassetto \global \includeNotes "corno-bassetto"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column {
          Corno I, II
          #(if (eqv? (ly:get-option 'en-re) #t)
               "In D"
               "In Es")
        }
      } << \keepWithTag #'corni \global \keepWithTag #'corni \includeCorni "corni" >>
    >>
    \new StaffGroup <<
      \new GrandStaff <<
        \new Staff \with { instrumentName = "Violino I" } <<
          \keepWithTag #'all \global \includeNotes "violino1"
        >>
        \new Staff \with { instrumentName = "Violino II" } <<
          \keepWithTag #'all \global \includeNotes "violino2"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \keepWithTag #'all \global \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'vtenor \includeNotes "voix"
      >> \keepWithTag #'vtenor \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncello Contrabasso }
    } <<
      \keepWithTag #'all \global \includeNotes "basso"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
      }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
