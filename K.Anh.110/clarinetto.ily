\transposition sib
\override Script.avoid-slur = #'outside 
\clef "treble" R1*4 |
r2 fa''4\p~ fa''16 fa''(\trill sol'' la'') |
fad''8.( sol''16) sol''4 la''16( sol'') fa''( mi'') \appoggiatura re''16 do''8.( sib'16) |
\appoggiatura do''16 sib'4\trill( la') r2  |
R1 |
r2 la'16( do'' fa'' la'') sib''8.(\fp mi''16) |
sol''16( fa'') fa''8-. r4 r2 |
r2 sol4 do'' |
fa'' fa' mi' r8 do'' |
re''4( mi'' fa'') r8 do'' |
re''4( mi'' fa'') r |
la''1\< |
sol''8.(\> mi''16 fa''16 mi'' re'' do'')\! do''4( si'\trill) |
do''4 r r2 |
R1*3 |
r4 r8 re'' mi''4( fad'' |
sol'') r8 re'' mi''4( fad'' |
sol'') r8 re'' mi''4( fad'' |
sold'' la'') fa''!4( mi''8 re'') |
do''8 r la''2\sfp la'4( |
sol'8) r sol''2\sf sol'4( |
fa') r4 r2 |
R1 |
r2 fa''4~ fa''16 fa''(\trill sol'' la'') |
fad''8.( sol''16) sol''4 la''16( sol'') fa''( mi'') \appoggiatura re''16 do''8.( sib'16) |
\appoggiatura do''16 sib'4(\trill la') r2 |
R1 |
r2 la'16( do'' fa'' la'') sib''8.(\fp mi''16) |
sol''16( fa'') fa''8-. r4 r2 |
r2 fa4 sib' |
mib'' la sib r8 fa' |
sol'4( la' sib') r8 fa'' |
sol''4( la'' sib'') r8 fa' |
re''2( fa'') |
do''4~ do''16( si' sib' la') la'4~ la'16( sol' fa' sol') |
la''4\p~ la''16( sold'' sol'' fa'') fa''4~ fa''16( mi'' re'' mi'') |
fa''4 r mib''2\<( |
re''8)\! fa''(\p re'' sib') la'8 r r4 |
mib''2\<( re''8\!) fa''(\p re'' sib') |
la'16-. do'( fa' la' do''8) r r16 do'( fa' la' do''8) r |
r16 do'(\pp fa' la' do''8) r fa'' r r4 |
