<<
  \tag #'vsoprano {
    \clef "treble" R1 |
    r2 sib'4^\p r |
    mib''^\f r fa'8^\p fa' lab'[ re'] |
    mib'4 r r2 |
    r2 sib'4^\f r |
    do''^\p do'' fa'4.^\< sol'16[ lab']\! |
    lab'4( sol') do''^\f r8 do'' |
    sib'4 sib'8 sib' sib'8.[ mib''16] re''[^\> do''] sib'[ lab']\! |
    sol'4( sib'16[ lab' sol' fa']) mib'4 r |
    R1 |
    mib'4 r sib'^\cresc r |
    mib''4.^\sf do''8 re'' sib' sib'8. sib'16 |
    sib'8^"sotto voce" sib' sib' sib' sib'4 sib'8 r |
    sib'4 sib' sib'8. sib'16 sib'8 r |
    do''4 r reb''^\< r8 reb''\! |
    re''!8. fa''16 fa''16[ sol''32 fa''] mib''16[ re''] re''4~ re''16[ do'' sib' do''] |
    sib'4 r r2 |
    R1 |
    do''4.^\p sib'8 lab'4 r |
    r2 sol'4.^\decresc fa'8 |
    mib'4 r do''^\p do'' |
    do'' do''8 r do''4. do''8 |
    do''4 do''8 r do''4 do'' |
    do''4 sib'8 sib' la'2 |
    sol'4 r do''2^\f |
    fa'4 r sib'4.^\> sib'8\! |
    mib'4 r r2 |
    R1*7 |
    mib'4. sol'8 lab'8.[ mib'16] mib'8 r |
    reb''4^\f\melisma sib'\melismaEnd do'' r |
    r2 mib''4.^\p mib''8 |
    mib''8 mib'' r4 mib''8 mib'' mib'' mib'' |
    fa'4 fa'8 r solb'4.^\< solb'8\! |
    sol'!4 sol'16[ sib'] mib''[^\< sol'']\! sol''4^\f~ sol''16[ fa''] mib''[ fa''] |
    mib''4 r r2 |
    mib'8^\p mib' mib' mib' mib'4 mib'8 mib' |
    mib'16. mib'32 mib'8 r4 mib'8 mib' mib' mib' |
    mib'4 mib'8 mib' mib'16. mib'32 mib'8 r4 |
    R1 |
    r8 sib'^\pp re'8. mib'16 mib'8 r r4 |
  }
  \tag #'valto {
    \clef "treble" R1 |
    r2 fa'4^\p r |
    sol'^\f r mib'8^\p mib' re'[ fa'] |
    mib'4 r r2 |
    r2 mib'4^\f r |
    mib'^\p mib' re'4.^\< re'8\! |
    re'4( mib') mib'8[^\f mi'] fa' sol' |
    fa'8.[ mib'16] re'8 fa' mib'[ sol'] sib'16[^\> lab'] sol'[ fa']\! |
    mib'4( sol'16[ fa' mib' re']) mib'4 r |
    R1 |
    r4 sol' r fa'^\< |
    fa'4.\sf fa'8 fa' re' re'8. re'16 |
    sol'8^"sotto voce" sol' fa' fa' mib'4 fa'8 r |
    sol'4 fa' mib'8. mib'16 fa'8 r |
    sol'4 r sib'4^\< r8 sib'\! |
    sib'8. sib'16 sib'8 fa' fa'4( mib') |
    re' r r2 |
    R1 |
    sol'4.^\p sol'8 fa'4 r |
    r2 re'4.^\decresc re'8 |
    do'4 r re'^\p mi' |
    fa'4 mib'!8 r re'4 mi' |
    fa'4 mib'!8 r re'4 mi' |
    fad'4 sol'8 sol' sol'4\melisma fad'\melismaEnd |
    sol' r sol'2^\f |
    lab'4 r fa'4.^\> fa'8\! |
    mib'4 r r2 |
    R1*7 |
    r2 mib'4 mib' |
    sol'4.^\f sib'8 mib'[ lab'] lab' r |
    fa'4^\p sol' lab' sol'8 r |
    fa'8 fa' sol' sol' lab'4 sol' |
    fa' fa'8 r do'4.^\< do'8\! |
    sib4 mib'16[ sol'] sol'[ sib'] sib'4^\f~ sib'16[ lab'] sol'[ lab'] |
    sol'4 r r2 |
    mib'8^\p mib' mib' mib' mib'4 mib'8 mib' |
    mib'16. mib'32 mib'8 r4 mib'8 mib' mib' mib' |
    mib'4 mib'8 mib' mib'16. mib'32 mib'8 r4 |
    R1 |
    r8 sol'^\pp fa'8. mib'16 mib'8 r r4 |
  }
  \tag #'vtenor {
    \clef "G_8" R1 |
    r2 sib4^\p r |
    sol^\f r do'8^\p lab fa[ lab] |
    sol4 r r2 |
    r2 mib'4^\f r |
    fa^\p fa fa4.^\< fa8\! |
    fa4( mib) do'8[^\f sib] lab do' |
    fa8.[ sib16] sib8 re mib4 mib8^\> do'\! |
    sib4..( lab16) sol4 r |
    R1 |
    r4 sib r sib^\< |
    la4.^\sf la8 sib8. sib16 sib8 sib |
    do'^"sotto voce" do' re' re' mib'4 re'8 r |
    do'4 re' mib'8. mib'16 re'8 r |
    mib'4 r sol'^\< r8 sol'\! |
    fa'8. re'16 mib'16[ re'] do'[ sib] sib4~ sib16[ la sol la] |
    sib4 r r2 |
    R1 |
    do'4.^\p do'8 do'4 r |
    r2 sol4.^\decresc sol8 |
    sol4 r la^\p sib |
    do' do'8 r la4 sib |
    do' do'8 r la4 sib |
    do'4 re'8 re' mib'4\melisma re'8[ do']\melismaEnd |
    sib4 r sib2^\f |
    lab4 r sib4.^\> lab8\! |
    sol4 r r2 |
    R1*7 |
    sol4. sib8 lab[ do'] do'[ mib'] |
    mib'2^\f mib'4 r |
    mib'4.^\p mib'8 mib mib r4 |
    mib'4. mib'8 lab4 sib |
    do'4 do'8 r mib'4.^\< mib'8\! |
    mib'4 mib8 mib mib4^\f re |
    mib r r2 |
    mib8^\p mib mib mib mib4 mib8 mib |
    mib16. mib32 mib8 r4 mib8 mib mib mib |
    mib4 mib8 mib mib16. mib32 mib8 r4 |
    R1 |
    r8 sib^\pp lab8. sol16 sol8 r r4 |
  }
  \tag #'vbasse {
    \clef "bass" R1 |
    r2 re4^\p r |
    do^\f r lab8^\p do' sib[ sib,] |
    mib4 r r2 |
    r2 sol4^\f r |
    lab,4.^\p la,8 sib,4.^\< si,8\! |
    do2 lab8[^\p sol] fa mib |
    re8.[ mib16] fa8 fa sol[ mib] lab^\> fa\! |
    sib4( sib,) mib4 r |
    R1 |
    mib4 r re^\cresc r |
    do^\sf fa sib,8 sib, re sol |
    mi^"sotto voce" mi fa fa sol4 fa8 r |
    mi4 fa sol8. sol16 fa8 r |
    mib!4 r mi^\< r8 mi\! |
    fa8. fa16 fa8 fa fa4( fa,) |
    sib, r r2 |
    R1 |
    mi4.^\p mi8 fa4 r |
    r2 si,4.^\decresc si,8 |
    do4 r fad^\p sol |
    lab! sol8 r fad4 sol |
    lab! sol8 r fad4 sol |
    la4 sib8 sib do'4( re') |
    sol4 r mi2^\f |
    fa4 r re4.^\> re8\! |
    mib4 r r2 |
    R1*7 |
    r2 do'4 do' |
    sib4.^\f sol8 lab4 lab,8 r |
    la,4^\p sib, do sib,8 r |
    la,8 la, sib, sib, do4 sib, |
    lab,! lab,8 r la,4.^\< la,8\! |
    sib,8.[ sib16] sib[ sol] sol[ mib] sib4^\f sib, |
    mib r r2 |
    mib8^\p mib mib mib mib4 mib8 mib |
    mib16. mib32 mib8 r4 mib8 mib mib mib |
    mib4 mib8 mib mib16. mib32 mib8 r4 |
    R1 |
    r8 sib,^\pp sib,8. sib,16 mib8 r r4 |
  }
>>
