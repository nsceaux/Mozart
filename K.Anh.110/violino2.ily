\clef "treble" R1 |
sib16-.\p sib-. r sib~ sib sib-. r sib( fa') fa'-. r fa'~ fa' fa'-. r fa'( |
mib') mib'-. r sol'~ sol' sol'-. r mib'~ mib' mib'-. r mib'( re') re'-. r re'( |
mib') sib-. r sib~ sib sib-. r sib( fa') fa'-. r fa'~ fa' fa'-. r fa'( |
sol') sol'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r sol'( |
fa') fa'-. r fa'~ fa' fa'-. r fa'~ fa' fa'-. r fa'~ fa' fa'-. r fa'~ |
fa' fa'-. r fa'( mib') mib'-. r mib'~ mib' mib'-. r sib'( lab') lab'-. r sol'( |
fa') fa'-. r mib'( re') re'-. r re'( mib') mib'-. r sol'( fa') fa'-. r fa'( |
mib') mib'-. r sib~ sib sib-. r lab( sol) sol-. r sib\fp~ sib sib-. r sib~ |
sib sib-. r sib\fp~ sib sib-. r sib~ sib sib-. r sib\fp~ sib sib-. r sib~ |
sib sib-. r sib~ sib sib-. r sol'( fa') fa'-. r fa'~ fa' fa'-. r fa'( |
mib') mib'-. r mib'~ mib' mib'-. r la'( sib') sib'-. r sib'( re') re'-. r re'( |
sol') sol'-. r sol'( fa') fa'-. r fa'( mib') mib'-. r mib'( fa') fa'-. r fa'( |
sol') sol'-. r sol'( fa') fa'-. r fa'( mib') mib'-. r mib'( fa') fa'-. r fa'( |
sol') sol'-. r sol'( sib') sib'-. r sib'\<~ sib' sib'-. r sib'~ sib' sib'-. r sib'\!~ |
sib'\> sib'-. r sib'~ sib' sib'-. r fa'( re'') re''-.\! r re'( mib') mib'-. r mib' |
re'8.(\mf re''16 mib'' re'' do'' sib') sib'4 sib'16( la' sol' la') |
sib'16-.\p fa'-. r fa'~ fa' fa'-. r fa'( mi') mi'-. r mi'~ mi' mi'-. r fa'( |
sol') sol'-. r sol'~ sol' sol'-. r sol'( fa') fa'-. r fa'~ fa' fa'-. r fa'~ |
fa' fa'-. r fa'~ fa' fa'-. r fa'~ fa' fa'-. r fa'~ fa' fa'-. r fa'( |
mib') mib'-. r mib'~ mib' mib'-. r mib'( re') re'-. r re'( mi') mi'-. r mi'( |
fa') fa'-. r fa'( sol') sol'-. r mib'( re') re'-. r re'( mi') mi'-. r mi'( |
fa') fa'-. r fa'( sol') sol'-. r mib'!( re') re'-. r re'( mi') mi'-. r mi'( |
fad')\< fad'-. r fad'( sol')\! sol'-.\> r sol'~ sol' sol'-.\! r sol'( fad') fad'-. r fad'( |
sol') sol'-. r sol'\fp~ sol' sol'-. r sol'~ sol' sol'-. r do'~ do' do'-. r do'~ |
do' do'-. r fa'\fp~ fa' fa'-. r fa'~ fa' fa'-. r sib~ sib sib-. r sib~ |
sib4 r r2 |
sib16-.\p sib-. r sib~ sib sib-. r sol'( fa') fa'-. r fa'~ fa' fa'-. r fa'( |
sol') sol'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'( re') re'-. r fa'~ fa' fa'-. r re'( |
fa') fa'-. r fa'( mib') mib'-. r mib'~ mib' mib'-. r sib'( lab') lab'-. r sol'( |
fa') fa'-. r mib'( re') re'-. r re'( mib') mib'-. r sol'( fa') fa'-. r fa'( |
mib') mib'-. r sib~ sib sib-. r lab( sol) sol-. r sib\fp~ sib sib-. r sib~ |
sib sib-. r sib\fp~ sib sib-. r sib~ sib sib-. r sib\fp~ sib sib-. r sib~ |
sib sib-. r sib~ sib sib-. r sib( do') do'-. r do'~ do' do'-. r do'( |
reb') reb'-. r reb'~ reb' reb'-. r reb'( do') do'-. r do'~ do' do'-. r do'~ |
do' do'-. r do'( sib) sib-. r sib( lab!) lab-. r lab( sib) sib-. r sib( |
do') do'-. r do'( sib) sib-. r sib( lab) lab-. r lab( sib) sib-. r sib( |
do') do'-. r do'~ do' do'-. r do'( mib')\< mib'-. r mib'~ mib' mib'-. r mib'\!~ |
mib' mib'-. r mib'( sol') sol'-. r sol'(\< sib') sib'-.\! r sib'(\f re') re'-. r re'( |
mib') mib'-. r mib'(\> sol') sol'-. r sib( sol) sol-. r sol( lab) lab-.\! r lab( |
sol)\p mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib'8 r lab'8.(\> sol'16)\! sol'8 r fa'8.(\> mib'16)\! |
mib'8 r sib4\pp~ sib8 r r4 |
