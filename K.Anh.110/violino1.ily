\clef "treble" R1 |
mib'16-.\p mib'-. r mib'~ mib' mib'-. r mib'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
mib'') mib''-. r mib''~ mib'' mib''-. r sol'( fa') fa'-. r fa'( lab') lab'-. r fa'( |
mib') mib'-. r mib'~ mib' mib'-. r mib'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
mib'') mib''-. r sol'~ sol' sol'-. r sol'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
do'') do''-. r do''~ do'' do''-. r do''( lab') lab'-. r lab'~ lab' lab'-. r lab'~ |
lab' lab'-. r lab'( sol') sol'-. r sol'( do'') do''-. r do''~ do'' do''-. r do''( |
sib') sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'( lab') lab'-. r lab'( |
sol') sol'-. r sol'( fa') fa'-. r re'( mib') mib'-. r mib'(\fp re') re'-. r fa'( |
mib') mib'-. r mib'(\fp re') re'-. r fa'( mib') mib'-. r mib'(\fp re') re'-. r fa'( |
mib') mib'-. r mib'~ mib' mib'-. r mib'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
la') la'-. r la'( do'') do''-. r do''( re'') re''-. r re''( sib') sib'-. r sib'~ |
sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'~ |
sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'( |
do'') do''-. r do''~ do'' do''-. r do''(\< reb'') reb''-. r reb''~ reb'' reb''-. r reb''(\! |
re''!)\> re''-. r re''~ re'' re''-. r sib'( fa'') fa''-.\! r fa''~ fa'' fa''-. r fa'' |
sib''8.(\mf fa''16) sol''( fa'' mib'' re'') re''4 re''16( do'' sib' do'') |
sib'16-.\p sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'~ |
sib' sib'-. r sib'~ sib' sib'-. r sib'( lab'!) lab'-. r lab'~ lab' lab'-. r lab'( |
si'16) si'-. r si'~ si' si'-. r do''( re'') re''-. r re''~ re'' re''-. r sol'16~ |
sol' sol'-. r sol'( do'') do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ |
do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ |
do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ |
do''\< do''-. r do''( sib')\! sib'-.\> r sib'( la') la'-.\! r la'( re'') re''-. r re''~ |
re'' re''-. r sib'~\fp sib' sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'( |
lab'!) lab'-. r lab'\fp~ lab' lab'-. r lab'~ lab' lab'-. r fa'~ fa' fa'-. r fa'( |
mib'4) r r2 | \allowPageTurn
mib'16-.\p mib'-. r mib'~ mib' mib'-. r mib'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
mib''16) mib''-. r sol'~ sol' sol'-. r sol'( sib') sib'-. r sib'~ sib' sib'-. r sib'( |
do'') do''-. r do''~ do'' do''-. r do''( lab') lab'-. r lab'~ lab' lab'-. r lab'~ |
lab' lab'-. r lab'( sol') sol'-. r sol'( do'') do''-. r do''~ do'' do''-. r do''( |
sib') sib'-. r sib'~ sib' sib'-. r sib'~ sib' sib'-. r sib'( lab') lab'-. r lab'( |
sol') sol'-. r sol'( fa') fa'-. r re'( mib') mib'-. r mib'(\fp re') re'-. r fa'( |
mib') mib'-. r mib'(\fp re') re'-. r fa'( mib') mib'-. r mib'(\fp re') re'-. r fa'( |
mib') mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'( |
sol') sol'-. r sol'~ sol' sol'-. r sol'( lab') lab'-. r lab'( mib') mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'( |
mib'') mib''-. r mib''~ mib'' mib''-. r mib''~ mib'' mib''-. r mib''~ mib'' mib''-. r sol'( |
fa') fa'-. r fa'~ fa' fa'-. r fa'( solb')\< solb'-. r solb'~ solb' solb'-. r solb'\!( |
sol'!) sol'-. r mib'( sib') sib'-. r sol'(\< mib'') mib''-.\! r mib''(\f sib'') sib''-. r sib'( |
sol'') sol''-. r sol'(\> mib'') mib''-. r mib'( sib') sib'-. r sib'( re') re'-.\! r re'( |
mib')\p mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ mib' mib'-. r mib'~ |
mib'8 r re'8.(\> sib'16)\! sib'8 r lab'8.(\> sol'16)\! |
sol'8 r fa'8.(\pp mib'16) mib'8 r r4 |
