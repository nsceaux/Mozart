\clef "treble" R1*3 |
sib''2~ sib''4.( lab''16 sol'') |
fa''8( mib'')~ mib''16( re''32 mib'' fa''[ mib'' re'' do'']) sib'4 r |
R1 |
r2 do''4-. do'-. |
fa''-. re'( mib') r |
R1 |
sol'16(\< sib' mib'' sol'')\! lab''8.\sfp re''16 fa''( mib'') mib''8-. r4 |
mib'4\cresc sol' sib'\! r |
fa'\f do'''\> re'''16.(\! sib''32) sib''8-. r4 |
r2 sib''4( \grace do'''16 sib'')( la''32 sol'') \grace sol''16 fa''([ mib''32 re'']) |
do''4( re'' mib'') r |
do'''2(\< reb'''\!) |
re'''!8.( fa'''16) fa'''8( mib'''16 re''') re'''4 do'''\trill |
sib'' r r2 |
sib'2 reb'''\sf |
do'''4. do'''32( sib'' la'' sib'') lab''4 r |
R1 |
r4 r8 do''( re''4 mi'') |
fa'' r do'2 |
do'''4( \grace re'''16 do''')( sib''32 lab''!) \grace lab''16 sol''([ fa''32 mib'']) re''4 r |
r8 re''( re'''4)~ re'''16 do'''( mib''' do''') sib''8 la'' |
sol''4 r reb'''8.(\sfp do'''16) do'''8 r |
r2 dob'''8.(\sfp sib''16) sib''8 r |
R1 |
sib''2~ sib''4.( lab''16 sol'') |
fa''8( mib'')~ mib''16([ re''32 mib''] fa'' mib'' re'' do'') sib'4 r |
R1 |
r2 do'''4-. fa''-. |
fa'''-. re''-.( mib''8)( mib''')( \grace re'''16 do'''8 sib''16 lab'') |
sol''( lab'' la'' sib'') sib''( lab'' sol'' fa'') mib''4 r |
sol'16(\< sib' mib'' sol'') lab''8.\sfp re''16 fa''( mib'') mib''8-. r4 |
R1 |
mib'4 sib''(\sf do'''16.) lab''32 lab''8-. r4 |
r2 mib'''4( \grace fa'''16 mib''' re'''32 do''') \grace do'''16 sib''([ lab''32 sol'']) |
fa''4 r r2 |
r4 r8 mib'' do'''2 |
sib''4~ sib''16( la'' lab'' sol'') sol''4~ sol''16 fa''( mib'' fa'') |
mib''4 r r2 |
r2 sib''4.(\< si''8)\! |
do'''(\p \grace sib''!16 lab''8 \grace sol''16 fa''8 \grace mib''16 re''8) mib''8 r r4 |
reb''2 do''4.( dob''8) |
sib'16-. sib'( mib'' sol'') lab''8.( sol''16-.) sol''8-. r fa''8.( mib''16-.) |
mib'' sib'( mib'' sol'') sib''8 r mib'''-. r r4 |
