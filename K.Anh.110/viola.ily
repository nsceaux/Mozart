\clef "alto" R1 |
sol16-.\p~ sol r sol~ sol sol-. r sol( sib) sib-. r re'~ re' re'-. r re'( |
do') do'-. r do'( sib) sib-. r sib( lab) lab-. r lab( fa) fa-. r lab( |
sol) sol-. r sol~ sol sol-. r sol( sib) sib-. r re'~ re' re'-. r re'( |
do') do'-. r do'~ do' do'-. r do'( sol) sol-. r sol~ sol sol-. r sol( |
lab) lab-. r lab~ lab lab-. r la( sib) sib-. r sib~ sib sib-. r si( |
re') re'-. r re'( mib') mib'-. r mib'( do') do'-. r mi'( fa') fa'-. r mib'( |
re') re'-. r fa'~ fa' fa'-. r lab!( sol) sol-. r sib( do') do'-. r do'( |
sib) sib-. r sib( lab) lab-. r fa( mib) mib-. r sol(\fp lab) lab-. r lab( |
sol) sol-. r sol(\fp lab) lab-. r lab( sol) sol-. r sol(\fp lab) lab-. r lab( |
sol) sol-. r sol~ sol sol-. r mib'( re') re'-. r re'~ re' re'-. r re'( |
do') do'-. r do'~ do' do'-. r do'( sib) sib-. r sib~ sib sib-. r sib( |
do') do'-. r do'( re') re'-. r re'( mib') mib'-. r mib'( re') re'-. r re'( |
do') do'-. r do'( re') re'-. r re'( mib') mib'-. r mib'( re') re'-. r re'( |
mib') mib'-. r mib'( sol') sol'-. r sol'\<~ sol' sol'-. r sol'~ sol' sol'-. r sol'(\! |
fa')\> fa'-. r fa'~ fa' fa'-. r re''( sib') sib'-.\! r sib( do') do'-. r << mib' \\ la >> |
<sib re'>8.(\mf <re' fa'>16) <mib' sol'>( <re' fa'> <do' mib'> <sib re'>) q4 q16( <la do'> <sol sib> <la do'>) |
<sib reb'>-.\p q-. r reb'~ reb' reb'-. r reb'~ reb' reb'-. r reb'~ reb' reb'-. r reb'( |
do') do'-. r mi'~ mi' mi'-. r do'~ do' do'-. r do'~ do' do'-. r do'( |
lab!) lab-. r lab~ lab lab-. r lab( sol) sol-. r sol( si) si-. r si( |
do') do'-. r do'~ do' do'-. r sol( la) la-. r la( sib!) sib-. r sib( |
do') do'-. r do'( mib') mib'-. r sol( la) la-. r la( sib) sib-. r sib( |
do') do'-. r do'( mib') mib'-. r sol( la) la-. r la( sib) sib-. r sib( |
la)\< la-. r la( sol)\! sol-.\> r sol'( mib') mib'-.\! r mib'( re') re'-. r do'( |
sib) sib-. r sib(\fp reb') reb'-. r reb'( do') do'-. r sol'~ sol' sol'-. r sol'( |
fa') fa'-. r do'(\fp dob') dob'-. r dob'( sib) sib-. r lab!~ lab lab-. r lab( |
sol4) r r2 |
sol16-.\p sol-. r sol~ sol sol-. r mib'( re') re'-. r re'~ re' re'-. r re'( |
do') do'-. r do'~ do' do'-. r do'( sol) sol-. r sol~ sol sol-. r sol( |
lab) lab-. r lab~ lab lab-. r la( sib) sib-. r sib~ sib sib-. r si( |
re') re'-. r re'( mib') mib'-. r mib'( do') do'-. r mi'( fa') fa'-. r mib'( |
re') re'-. r fa'~ fa' fa'-. r lab( sol) sol-. r sib( do') do'-. r do'( |
sib) sib-. r sib( lab) lab-. r fa( mib) mib-. r sol(\fp lab) lab-. r lab( |
sol) sol-. r sol(\fp lab) lab-. r lab( sol) sol-. r sol(\fp lab) lab-. r lab( |
sol) sol-. r sol~ sol sol-. r sol( lab) lab-. r lab~ lab lab-. r lab( |
sib) sib-. r sib~ sib sib-. r sib( lab) lab-. r lab~ lab lab-. r sib( |
la) la-. r fa( sol) sol-. r sol( lab) lab-. r lab( sol) sol-. r sol( |
fa) fa-. r fa( sol) sol-. r sol( lab) lab-. r lab( sol) sol-. r sol( |
lab) lab-. r lab~ lab lab-. r lab( do')\< do'-. r do'~ do' do'-. r do'\!( |
sib) sib-. r sib( mib') mib'-. r mib'(\< sol') sol'-.\! r sol'(\f fa') fa'-. r fa( |
mib) mib-. r sol(\> sib) sib-. r sol( mib) mib-. r mib( fa) fa-.\! r fa( |
mib)\p mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ |
mib mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ |
mib mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ mib mib-. r mib~ |
mib8 r fa'8.([\> mib'16])\! mib'8 r sib4\>~ |
sib8\! r lab8.([\pp sol16]) sol8 r r4 |
