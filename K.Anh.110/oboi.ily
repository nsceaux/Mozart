\clef "treble" R1*3 |
\override Script.avoid-slur = #'outside 
sib''2\p~ sib''4. lab''16( sol'') |
fa''8( mib'')~ mib''16 re''32( mib'' fa'' mib'' re'' do'') sib'4 r |
R1*4 |
r2 sol'32-. lab'-. sib'-. do''-. re''-. mib''-. fa''-. sol''-. lab''8.(\fp re''16) |
fa''16( mib'') mib''8-. r4 r2 |
fa'4 do''' re'''16.( sib''32) sib''8-. r4 |
r2 sib''4 \appoggiatura do'''32 sib''16([ la''32 sol'']) \appoggiatura sol''32 fa''16([ mib''32 re'']) |
do''4 r r2 |
r4 r8 do''8 sib''2\<~ |
sib''8.(\> fa''16) fa''16 sol''32 fa'' mib''16 re''\! re''4( do'')\trill |
sib' r r2 |
sib'2 reb'''( |
do'''4.) do'''32( sib'' lab'' sib'') lab''4 r |
R1*2 |
do'''4 \appoggiatura re'''32 do'''16([ sib''32 lab'']) \appoggiatura lab''32 sol''16([ fa''32 mib'']) re''4 r |
r2 r8 do'' do'''4~ |
do''' sib''~ sib''16( la'' do''' la'' sol''8 fad'') |
sol''8 r sib''2\sfp sib'4( |
lab'!8) r lab''2\sfp lab'4( |
sol') r r2 |
sib''2~ sib''4. lab''16( sol'') |
fa''8( mib'')~ mib''16 re''32( mib'' fa'' mib'' re'' do'') sib'4 r |
R1*4 |
r2 sol'32-. lab'-. sib'-. do''-. re''-. mib''-. fa''-. sol''-. lab''8.(\fp re''16) |
fa''16( mib'') mib''8-. r4 r2 |
mib'4 sib'' do'''16.( lab''32) lab''8-. r4 |
r2 mib''4 \appoggiatura fa''32 mib''16([ re''32 do'']) \appoggiatura do''32 sib'16([ lab'32 sol']) |
fa'4 r r2 |
r4 r8 mib'' do'''2 |
sib''4~ sib''16( la'' lab'' sol'') sol''4~ sol''16( fa'' mib'' fa'') |
sib''4\p~ sib''16( la'' lab'' sol'') sol''4~ sol''16( fa'' mib'' fa'') |
mib''4 r sib''4.(\< si''8\! |
do''') \appoggiatura sib''!32 lab''8\p \appoggiatura sol''32 fa''8 \appoggiatura mib''32 re''8 mib'' r r4 |
sib''4.(\< si''8 do'''\!) \appoggiatura sib''!32 lab''8\p \appoggiatura sol''32 fa''8 \appoggiatura mib''32 re''8 |
mib''16 sib'( mib'' sol'' sib''8) r r16 sib'16( mib'' sol'' sib''8) r |
r16 sib'(\pp mib'' sol'' sib''8) r mib''' r r4 |
