\score {
  \new GrandStaff <<
    \new Staff \with { instrumentName = "Clarinetto in B" } <<
      \keepWithTag #'clarinetto \global \includeNotes "clarinetto"
    >>
    \new Staff \with { instrumentName = "Corno di bassetto" } <<
      \keepWithTag #'corno-bassetto \global \includeNotes "corno-bassetto"
    >>
  >>
  \layout { indent = \largeindent }
}
