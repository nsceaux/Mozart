\version "2.19.80"
\include "common.ily"

\opusTitle "KV-Anh. 110 – Quis te comprehendat"

\header {
  title = \markup\italic { Quis te comprehendat }
  subtitle = "Motette nach dem 3. Satz aus der Bläserserenade K.361 (Gran Partita)"
  opus = "KV-Anh. 110"
  date = "Anonyme (ca. 1820)"
  copyrightYear = "2019"
  shortcopyright = ##f
  tagline = "Édition : Nicolas Sceaux"
}

includeCorni = 
#(define-music-function (parser this-location pathname) (string?)
   ;; use locations from the included file,
   ;; and not from where \includeNotes is called
   (with-location #f
  (let ((include-file (include-pathname pathname)))
    #{ \notemode { \include $include-file } #})))

includeNotes = 
#(define-music-function (parser this-location pathname) (string?)
   ;; use locations from the included file,
   ;; and not from where \includeNotes is called
   (with-location #f
  (let ((include-file (include-pathname pathname)))
    (if (and (eqv? (ly:get-option 'en-re) #t)
             (not (eqv? (ly:get-option 'part) 'corni)))
        #{ \notemode { \transpose mib re { \include $include-file } } #}
        #{ \notemode { \include $include-file } #}))))

global = 
#(define-music-function (parser this-location) ()
   (with-location #f
  (let* ((global-symbol
          (string->symbol (format "global~a~a" (*path*) (*piece*))))
         (global-music (ly:parser-lookup global-symbol)))
   (if (not (ly:music? global-music))
       (let* ((global-file (include-pathname "global")))
         (set! global-music 
               (if (eqv? (ly:get-option 'en-re) #t)
                   #{ \notemode { \transpose mib re { \include $global-file } } #}
                   #{ \notemode { \include $global-file } #}))
         (ly:parser-define! global-symbol global-music)))
   (ly:music-deep-copy global-music))))


\includeScore "K.Anh.110"
