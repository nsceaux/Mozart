\score {
  \new PianoStaff <<
    \new Staff << \keepWithTag #'all \global \includeNotes "organo" >>
    \new Staff <<
      \keepWithTag #'all \global \includeNotes "basso"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
