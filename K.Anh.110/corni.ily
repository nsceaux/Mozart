\clef "treble"
#(context-spec-music
  (if (eqv? (ly:get-option 'en-re) #t)
      (make-property-set 'instrumentTransposition (ly:make-pitch -1 1))
      (make-property-set 'instrumentTransposition (ly:make-pitch -1 2 -1/2)))
  'Staff)

<>^"Solo" <<
  \tag #'(corno1 corni) {
    do''4( mi'' sol'' sol') |
    do''2 re'' |
  }
  \tag #'(corno2 corni) {
    do'4( mi' sol' sol) |
    mi'2 sol' |
  }
  { s1\f | s\p }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re'' | }
  { do''2. sol'4 | }
  { s1\fz }
>>
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { mi' }
>> r4 r2 |
R1*4 |
r2 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'2~ | sol'1( | do'4) }
  { sol2~ | sol1( | do'4) }
>> r4 r2 |
R1 |
r2 <<
  \tag #'(corno1 corni) {
    sol'2~ |
    sol'1~ |
    sol'~ |
    sol'2
  }
  \tag #'(corno2 corni) {
    sol2~ |
    sol1~ |
    sol~ |
    sol2
  }
  { s2 | s1 | s\< | s2\! }
>> r2 |
<<
  \tag #'(corno1 corni) { sol'2 }
  \tag #'(corno2 corni) { sol }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 }
  { re''4( do'') }
>>
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { sol }
>> r4 r2 |
R1*8 |
\tag #'corni <>^"a 2." do'16-. do'-. r do'~ do' do'-. r do'( sol') sol'-. s sol'~ sol' sol'-. s sol' |
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { mi'4 }
>> r4 r2 |
R1*4 |
r2 <>\p <<
  \tag #'(corno1 corni) {
    sol'2~ |
    sol'1 |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do'' |
    sol'~ |
    sol' |
    do''16-. do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-. r do''~ |
    do'' do''-. r do''~ do'' do''-. r do''~ do'' do''-.
  }
  \tag #'(corno2 corni) {
    sol2~ |
    sol1 |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do' |
    sol~ |
    sol |
    do'16-. do'-. r do'~ do' do'-. r do'~ do' do'-. r do'~ do' do'-. r do'~ |
    do' do'-. r do'~ do' do'-. r do'~ do' do'-.
  }
  { s2 | s1 | s | s\f | s\p | s | s\< | s\f | s\decresc | s2\! s\p }
>> r8 r4 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''8 r \tag#'corni \tieUp sol'4~ sol'8 r sol'4~ |
    sol'8 r sol'4( mi'8)
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'8 r sol4( do'8) r sol4( |
    do'8) r sol4( do'8)
  }
  { s1\p | s4 s2.\p | s4 s\pp }
>> r8 r4 |
