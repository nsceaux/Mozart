Quis te com -- pre -- hen -- dat,
te, te, Al -- tis -- si -- me,
te, \tag #'(valto vtenor vbasse) te, qui e -- ras, qui es et qui e -- ris,
me, me, quam fe -- li -- cem, qui te me -- um es -- se pa -- trem
cor -- de cre -- de -- re
et te, te ap -- pel -- la -- re pos -- sum.
Che -- ru -- bim, Se -- ra -- phim,
o -- mnes cho -- ri an -- ge -- lo -- rum hy -- mnum
lae -- ti can -- ta -- te Pa -- tri o -- pti -- mo.

Hy -- mnum no -- strum ve -- stro,
hy -- mnum no -- strum,
hy -- mnum no -- strum ve -- stro,
ve -- stro,
\tag #'(valto vbasse) { ve -- stro, }
no -- strum con -- jun -- gi -- mus.
hy -- mnum no -- strum ve -- stro con -- jun -- gi -- mus,
hy -- mnum no -- strum ve -- stro con -- jun -- gi -- mus,
con -- jun -- gi -- mus.
