\transposition sib
\clef "treble" R1*4 |
r2 fa''4^"Solo"~ fa''16 \once\override Script.avoid-slur = #'outside fa''(\trill sol'' la'') |
fad''8.( sol''16) sol''4 la''16( sol'' fa'' mi'') \grace re'' do''8. sib'16 |
\grace do'' sib'4\trill la' r re'' |
do''2~ do''8 fa''( mi''16 re'' do'' sib') |
la'4 do''16( sib' la' sol') la'( do'' fa'' la'') sib''8.( mi''16) |
sol''16( fa'') fa''8-. r4 la'32([-. sib'-. do''-. re''-.] mi''[-. fa''-. sol''-. la''-.]) sib''8. mi''16 |
sol''16( fa'') fa''8-. r4 sol'' do'' |
fa'' fa'\trill mi' r8 do''( |
re''4 mi'' fa'') r |
r2 do'''4( \grace re'''16 do''' si''32) la'' \grace la''16 sol''([ fa''32 mi'']) |
re''2 do'''2~ |
do'''8. sol''16 sol''16[ la''32 sol''] fa''16[ mi''] mi''4 re''\trill |
do'' r r2 |
R1*2 |
sib''!2^"Solo" la''4.~ la''32( sol'' fad'' sol'') |
fa''!4 r re''2( |
re'''4)( \grace mi'''16 re'''[ do'''32 sib''!]) \grace sib''16 la''([ sol''32 fa'']) mi''4( fad'') |
sol'' r8 re'' mi''4( re''')~ |
re'''4 do'''! do'''16 si'' re''' si'' la''8( sold'') |
la''8 r do'''2( do''4)( |
sib'!8-.) r sib''2( sib'4)( |
la')-. r4 r2 |
la'4 r r2 |
r2 fa''4~ fa''16 \once\override Script.avoid-slur = #'outside fa''\trill( sol'' la'') |
fad''8.( sol''16) sol''4 la''16( sol'' fa''! mi'') \grace re''16 do''8. sib'16 |
\grace do'' sib'4\trill la' re''2 |
do''~ do''8 fa''( mi''16 re'' do'' sib') |
la'16( sib' si' do'') do''( sib' la' sol') la'( do'' fa'' la'') sib''8. mi''16 |
sol''16( fa'')-. fa''8-. r4 la'32(-.[ sib'-. do''-. re''-.] mi''-. fa''-. sol''-. la''-.) sib''8. mi''16 |
sol''16( fa''-.) fa''8-. r4 sib'2( |
mib''2) re''4 r8 fa'( |
sol'4 la' sib') r8 fa''( |
sol''4 la'') <fa'' sib''>4\( \grace sol''16 fa''([ mi''32 re''])\) \grace re''16 do''([ sib'32 la']) |
re''2 lab''( |
la''!4)~ la''16( sold'' sol'' fa'') fa''4~ fa''16 mi''( re'' mi'') |
do'''4~ do'''16( si'' sib'' la'') la''4~ la''16( sol'' fa'' sol'') |
fa''4 r mib''2( |
re''8) fa''( re'' sib') la' r r4 |
do'''4.( dod'''8 re'''8)( \grace do'''16 sib''8 \grace la''16 sol''8 \grace fa''16 mi''8) |
fa''16-. do'( fa' la') do''4 r16 do''( fa'' la'') do'''8 r |
r16 do'([ fa' la']) do''8-.\pp r fa''-. r r4 |
