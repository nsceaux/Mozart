\clef "bass" sib8.\f fa16 fa4 r8. fa16 |
fa8. re16 re4 r8. re16 |
re8. sib,16 sib,4 r |
r r8. sib16 sib,4 |
r4 r8. fa16 fa,4 |
R2. |
fa2.\p( |
sib,4) r r |
sib, r r |
sib,4 r8 sib,( do re) |
mib4( re do) |
fa8 r fa r fa r |
sib,4 r r |
mib8 r fa r fa, r |
sib,4 r r |
fa r r |
fa r r |
sib, r r |
do8\f r do\p r do r |
r16. fa32\f fa16. fa32 fa16. fa32 fa16. fa32 re8 r |
do8. do16 do4 r |
fa4\p r r |
do r r |
fa r r |
sib,8 r do r do r |
fa4 r r |
do r r |
fa r r |
sib,8 r do r do r |
fa2.~ |
fa~ |
fa~ |
fa~ |
fa4 r r |
la,8 r la, r la, r |
sib, r sib, r sib, r |
sib, r sib, r sib, r |
do r do r sib, r |
la,2( sib,4) |
do8 r do r sib, r |
la,2( sib,4) |
do8 r do r do r |
fa4 fa, r\fermata |
sib2\f r8 sib-. re'-. sib-. |
fa-. fa-. sib-. fa-. re-. re-. fa-. re-. |
sib,4 r r2 |
sib,4\p r r2 |
sib,4 r r2 |
fa1\fp |
sib,4\f r r2 |
mib4 r r2 |
fa4-. sib,-. r2 |
fa4 r fa r |
fa\p r fa r |
fa r fa r |
fa r8 fa\f fa fa fa fa |
fa4 fa, r2 |
R1*2 |
fa1\p( |
sib,4) r r2 |
R1*2 |
fa1( |
sol4) r fa r |
mib mib mib mib |
fa2 fa, |
sib,4 <<
  { r8^"Vcl" do'( reb' re' mib' mi') |
    fa'4( re' sib re') |
    mib'! } \\
  { r4 r2 | R1 | r4 }
>> r8^"Bassi" sib,( do re mib mi) |
fa4( re sib, re) |
mib!4 r r2 |
si,1\fp |
do4 r r2 |
\ru#3 { r4 la2( sib4) | }
mib4 r mib r |
mi r mi r |
fa r r2 |
do4 r do r |
do\f r r2\fermata |
r4 fa\f r2\fermata |
fa4\p r r2\fermata |
fa4 r\fermata r2 |
R1*2 | \allowPageTurn
fa1( |
sib,4) r r2 |
R1*2 |
fa1 |
sol4 r fa r |
mib mib mib mib |
fa2 fa, |
sib,4 r <<
  { sib8\f^"Vcl" mib' re' do' |
    sib sol' fa' mib' re' mib' re' do' |
    sib } \\
  { s2 | R1 | s8 }
>> <>^"Bassi" sib,8 do re mib fa sol la |
sib4 r fa\p r |
sib, r <<
  { sib8\f^"Vcl" mib' re' do' |
    sib sol' fa' mib' re' mib' re' do' |
    sib } \\
  { s2 | R1 | r8 }
>> <>^"Bassi" sib,8 do re mib fa sol la |
sib4 r fa\p r |
sib,2 r4 sib,( |
la,2) r4 fa,( |
sib,2) r4 sib,( |
la,2) r4 fa,( |
sib,2) r4 sib,( |
mib2) r4 mib( |
fa) r fa, r |
sib, r la, r |
sib, r do r |
re r r2 |
mib4 r r2 |
fa2:8\f fa,:8 |
sib,4 r r2 |
R1*2 |
sib4\p r fa r |
sib, r fa, r |
sib, r r2 |
mib4 r r2 |
fa4 r fa, r |
sib, r la, r |
sib, r do r |
re r r2 |
mib4 r r2 |
fa2:8\f fa,:8 |
sib,4 r r2 |
mib4\p r r2 |
fa4 r r2 |
sib,4 r r2 |
mib4 r r2 |
fa4 r r2 |
re4 r r2 |
lab1\sfp |
sol4 r mib r |
mib r mib r |
fa r r2 |
fa2:8\f fa:8 |
re4-.\p r mib-. r |
fa-. r mib-. r |
re-. r mib-. r |
fa-. r mib-. r |
re2:8 mib:8\cresc |
fa:8 fa,:8 |
<>\f <<
  { sib8 mib' re' do' sib sol' fa' mib' |
    re' mib' re' do' sib sol fa mib | } \\
  { sib,4 r4 r2 |
    r2 sib,8 sol fa mib | }
>>
re8 sib, do re mib fa sol la |
sib4 mib fa fa, |
sib, r sib, sib, |
sib,2 r |
