\clef "treble" \transposition sib
\tag#'corni <>^"a 2." do''8.\f sol'16 sol'4 r8. sol'16 |
sol'8. mi'16 mi'4 r8. mi'16 |
mi'8. do'16 do'4 r |
R2.*4 |
<>\p <<
  \tag #'(corno1 corni) { do''2.~ | do''4 }
  \tag #'(corno2 corni) { do'2.~ | do'4 }
>> r4 r |
R2.*20 |
\tag#'corni <>^"a 2." sol'2.\p~ |
sol'4 r r |
sol'2.~ |
sol'4 r r |
R2.*9 |
R2.^\fermataMarkup |
<>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r4 \tag#'corni <>^"a 2." mi''8-. do''-. |
sol'4 do''8-. sol'-. mi'4 sol'8-. mi'-. |
do'4 r r2 |
R1*9 |
r4 r8 <>\f <<
  \tag #'(corno1 corni) {
    mi''8 re'' mi'' re'' mi'' |
    re''4 sol'
  }
  \tag #'(corno2 corni) {
    do''8 sol' do'' sol' do'' |
    sol'4 sol
  }
>> r2 |
R1*24 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*10 |
<<
  \tag #'(corno1 corni) {
    do''4 s do'' s |
    do'' s do'' s |
    do'' s s2 |
    do''4 s re'' s |
    do'' s do'' s |
    do'' s do'' s |
    do'' s s2 |
    do''4 s re'' s |
    do''
  }
  \tag #'(corno2 corni) {
    do'4 s do' s |
    do' s do' s |
    do' s s2 |
    mi'4 s sol' s |
    do' s do' s |
    do' s do' s |
    do' s s2 |
    mi'4 s sol' s |
    mi'
  }
  { s4\f r s r |
    s r s r |
    s r r2 |
    s4 r s\p r |
    s\f r s r |
    s r s r |
    s r r2 |
    s4 r s\p s | }
>> r4 r2 |
R1*10 |
<>\f <<
  \tag #'(corno1 corni) { sol'1 | sol'4 }
  \tag #'(corno2 corni) { sol'1 | do'4 }
>> r4 r2 |
R1*11 |
<>\f <<
  \tag #'(corno1 corni) { sol'1 | sol'4 }
  \tag #'(corno2 corni) { sol'1 | do'4 }
>> r4 r2 |
R1*16 |
<<
  \tag #'(corno1 corni) {
    mi''2 re'' |
    do''4 s do'' s |
    do'' s do'' s |
    do'' s s2 |
    s4 do'' mi'' re'' |
    do'' s do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    do''2 sol' |
    do'4 s do' s |
    do' s do' s |
    do' s s2 |
    s4 do' do'' sol' |
    mi' s mi' mi' |
    mi'2
  }
  { s1\cresc |
    s4\f r s r |
    s r s r |
    s r r2 |
    r4 s2. |
    s4 r }
>> r2 |
