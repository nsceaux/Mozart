\clef "treble" \transposition sib
do'''8.\f sol''16 sol''4 r8. sol''16 |
sol''8. mi''16 mi''4 r8. mi''16 |
mi''8. do''16 do''4 r |
R2.*9 |
sol''4.( la''16 sol'' do''' si'' la'' sol'') |
sold''( si''32 la'' sol'' fa'' mi'' re'') sol''16.( mi''32 do''4 mi''16 re'') |
do''4 r r8 sol' |
fa''4.( sol''16 la'' sol'' fa'' mi'' re'') |
do''8.( re''32 do'' si'4) r8 sol' |
sol''4~ sol''16 la''32( si'' do'''[ si'' la'' sol''] fad''[ sol'' la'' sol''] fa''[ mi'' re'' do'']) |
do'4\fp r r |
R2.*2 |
re''4.( mi''16 fad'' sol'' la'' si'' sol'') |
fad''16( la'' do'''8)~ do'''32([ la'' fad'' re''] \tuplet 6/4 { do''[ la' fad' re' do' la]) } fad8 r |
R2.*2 |
re''4.( mi''16 fad'' sol'' la'' si'' sol'') |
fad''16( la'' do'''8)~ do'''32([ la'' fad'' re''] \tuplet 6/4 { do''[ la' fad' re' do' la]) } fad8 r |
R2.*2 |
sol''4~ sol''16( sold'' la'' si'' do''' la'' mi'' fad'') |
sol''!4~ sol''32[ si''( re''' si''] sol''[ re'' si' sol'] fad'[ la' fad' re'] do'[ la fad re]) |
sol8 sol''~ sol''16( sold'' la'' si'' do''' la'' mi'' fad'') |
sol''!4~ sol''32[ si''( re''' si''] sol''[ re'' si' sol'] fad'[ la' fad' re'] do'[ la fad re]) |
sol4 r r |
R2. |
r8 do8~ do32[ mi sol do'] mi'[ do' sol' mi'] do''[ sol' mi' sol'] do''[ sol' mi'' do''] |
sol''4 r r |
R2. |
r16 re'''( do''' si'' la'' sol'' fad'' fa'' mi''8) r |
r32 sol'[ si' re''] sol''[ si'' re''' si''] sol''[ re'' si' sol'] re' si sol si re8 r |
r16 re'''( do''' si'' la'' sol'' fad'' fa'' mi''8) r |
r32 sol'[ si' re''] sol''[ si'' re''' si''] sol''[ re'' si' sol'] re' si sol si re8 r |
R2.^\fermataMarkup |
R1*2 |
do''2~ do''16 re'' do'' si' do'' re'' mi'' fa'' |
mi''4 r mi''16 fa'' mi'' re'' mi'' fa'' sol'' la'' |
sol''4 r sol''16 la'' sol'' fad'' sol'' la'' si'' do''' |
si'' sol'' si'' re''' si'' sol'' fa''! re'' si' sol' fa' re' si sol fa re |
mi4 r r2 |
R1*3 |
r16 sol si re' sol' si' re'' sol'' mi'' sol'' do''' sol'' mi'' sol'' mi'' do'' |
si' sol si re' sol' si' re'' sol'' mi'' sol'' do''' sol'' mi'' sol'' mi'' do'' |
sol'4 r r2 |
r2 r8 sol''(_\markup\italic dolce fad'' sol'') |
mi''-. sol''([ fad'' sol'']) re''-. sol''([ fad'' sol'']) |
mi''4 r r2 |
R1 |
r2 r8 sol''( la''16 sol'' fad'' sol'') |
mi''8-. sol''( la''16 sol'' fad'' sol'') re''8-. sol''( la''16 sol'' fad'' sol'') |
mi''4 r r2 |
R1*16 | \allowPageTurn
r8 sol si re' sol' si' re'' sol'' |
fad''16 la'' do''' la'' do''' la'' fad'' re'' do'' la' fad' re' do' la fad la |
re4 r r2\fermata |
r4 sol r2\fermata |
R1^\fermataMarkup |
r2\fermata r8 sol''(_\markup\italic dolce fad'' sol'') |
mi''-. sol''([ fad'' sol'']) re''-. sol''([ fad'' sol'']) |
mi''4 r r2 |
R1 |
r2 r8 sol''( la''16 sol'' fad'' sol'') |
mi''8-. sol''( la''16 sol'' fad'' sol'') re''8-. sol''( la''16 sol'' fad'' sol'') |
mi''4 r r2 |
R1*12 |
\tuplet 3/2 { sol8 do' mi' } \tuplet 3/2 { sol' mi' do' } sol8*2/3 do' mi' sol' mi' do' |
sol re' fa' sol' fa' re' sol re' fa' sol' fa' re' |
sol do' mi' sol' mi' do' sol do' mi' sol' mi' do' |
sol re' fa' sol' fa' re' sol re' fa' sol' fa' re' |
sol do' mi' sol' mi' do' sol do' mi' sol' mi' do' |
fa la do' fa' do' la fa la do' fa' do' la |
sol do' mi' sol' do'' sol' mi' do' sol fa re sol |
do4 r r2 |
R1*4 |
do''8*2/3 si' do'' mi'' re'' do'' re'' do'' re'' fa'' mi'' re'' |
mi'' re'' mi'' sol'' fa'' mi'' fa'' mi'' fa'' la'' sol'' fa'' |
sol''4 mi''8*2/3 re'' do'' re'' mi'' re'' fa'' mi'' re'' |
mi'' sol'' do''' sol'' mi'' do'' fa'' re'' si' sol' fa' re' |
mi' sol' do'' sol' mi' do' fa' re' si sol fa re |
mi sol do' mi' sol' do'' mi'' sol'' do''' sol'' mi'' do'' |
la' re'' dod'' re'' fa'' mi'' fa'' la'' do''' la'' sol'' fa'' |
mi'' sol'' do''' sol'' mi'' do'' fa'' re'' si' sol' fa' re' |
do'4 r r2 |
R1 |
r4 mi( sol do') |
r fa( la re') |
sol4 r r2 |
\omit TupletBracket \tuplet 3/2 { r8 do' mi' } \tuplet 3/2 { sol' do'' mi'' } \tuplet 3/2 { sol'' mi'' sol'' } do'''4 |
r8*2/3 la re' fa' la' re'' fa'' re'' fa'' la''4 |
r8*2/3 sol' do'' mi'' sol'' do''' sol'' mi'' do'' fa'' re'' si' |
r8*2/3 do' mi' sol' do'' mi'' sol'' mi'' sol'' do'''4 |
r8*2/3 la re' fa' la' re'' fa'' re'' fa'' la''4 |
r8*2/3 sol' do'' mi'' sol'' do''' sol'' mi'' do'' fa'' re'' si' |
do''4 r r2 |
R1 |
r8*2/3 fa la do' fa' la' do'' la do' fa' la' do'' |
fa'' do' fa' la' do'' fa'' la'' fa' la' do'' fa'' la'' |
sol'' do''' si'' sib'' la'' lab'' sol'' fad'' fa'' mi'' re'' do'' |
si'4 r r2 |
r8*2/3 do'''( si'' do''' sol'' mi'') r la''( sold'' la'' fa'' re'') |
r sol''!( fad'' sol'' mi'' do'') r fa''( mi'' fa'' re'' si') |
r8*2/3 do'''( si'' do''' sol'' mi'') r la''( sold'' la'' fa'' re'') |
r sol''!( fad'' sol'' mi'' do'') r fa''( mi'' fa'' re'' si') |
do''4 r r2 |
R1*7 |
