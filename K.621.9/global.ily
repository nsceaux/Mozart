\tag #'all \key sib \major
\tag #'clarinetti \key do \major
\tempo "Adagio" \midiTempo#60
\time 3/4 s2.*43 \bar "||"
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*52
\tempo "Allegro assai" s1*57 \bar "|."
