\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \keepWithTag #'all \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Clarinetto Solo in B }
        shortInstrumentName = "Cl."
      } <<
        \keepWithTag #'clarinetti \global
        \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in B alto"
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Sesto
      shortInstrumentName = \markup\character Ses.
    } \withLyrics <<
      \keepWithTag #'all \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column {
        Violoncello e Basso
      }
      shortInstrumentName = \markup\center-column { Vc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global \includeNotes "basso"
      \origLayout {
        s2.*9\pageBreak
        s2.*7\break s2.*5\break s2.*6\pageBreak
        s2.*5\break s2.*6\break s2.*5\pageBreak
        s1*6\break s1*7\pageBreak
        s1*7\break \grace s8 s1*9\break s1*8\pageBreak
        s1*8\break s1*7\break s1*7\pageBreak
        s1*5\break s1*5\break s1*5\pageBreak
        s1*5\break s1*6\break s1*5\pageBreak
        \grace s4 s1*5\break s1*6\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
