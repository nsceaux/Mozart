\clef "treble" <re' sib' sib''>8. fa''16 fa''4 r8. fa''16 |
fa''8. re''16 re''4 r8. re''16 |
re''8. sib'16 sib'4 r |
<<
  \tag #'violino1 {
    r4 r8. re''32( fa'' sib''4) |
    r4 r8. fa''32( la'' do'''4) |
  }
  \tag #'violino2 {
    r4 r8. sib'32( fa' re'4) |
    r4 r8. fa'32( do' la4) |
  }
>>
R2. |
<>\p <<
  \tag #'violino1 {
    do'2.( |
    re'8) re''( mib'' mi'' fa'' fad'' |
    sol'') r sib' r sib' r |
    sib'4 r8 fa''16 fa'' mib'' mib'' re'' re'' |
    do''4( re'' mib''8. do''16) |
    sib'8-. fa'-. r re'-. r do'-. |
    sib-. fa'-. re'-. fa'-. re'-. fa'-. |
    r8 mib' r re' r do' |
    sib-. fa'-. re'-. fa'-. re'-. fa'-. |
    r mib'-. do'-. mib'-. do'-. mib'-. |
    r mib'-. do'-. mib'-. do'-. mib'-. |
    r fa'-. re'-. fa'-. re'-. fa'-. |
    sib''8-.\fp sib'-. r sib' r sib' |
    r16. la'32\f la'16. do''32 do''16. fa''32 fa''16. la''32 si''8 r |
    do'''8. do'16 do'4 r |
    r8 la'-.\p fa'-. la'-. fa'-. la'-. |
    r sol'-. mi'-. sol'-. mi'-. sol'-. |
    r do'-. fa'-. la'-. do''-. fa''-. |
    r8 sib' r la' r sol' |
    r la'-. fa'-. la'-. fa'-. la'-. |
    r sol'-. mi'-. sol'-. mi'-. sol'-. |
    r do'-. fa'-. la'-. do''-. fa''-. |
    r8 sib' r la' r sol' |
    fa'8 do''4 do'' do''8~ |
    do'' r la' r sol' r |
    fa' do''4 do'' do''8~ |
    do'' r la' r sol' r |
    la'4 r r |
    r8 mib'! r mib' r mib' |
    r re' r re' r re' |
    r re'' r re'' r sib' |
    r la' r sol' r sol' |
    do''2( re''4) |
    r8 fa' r la' r sol' |
    do''2( re''4) |
    r8 fa' r la' r sol' |
    fa'8. la'16 fa'4 r\fermata |
  }
  \tag #'violino2 {
    la2.( |
    sib8) sib'( do'' dod'' re'') re''( |
    mib'') r sol' r mib' r |
    re'4 r8 re''16 re'' do'' do'' sib' sib' |
    sol'4( fa' sol'8. mib'16) |
    re'8-. re'-. r sib-. r la-. |
    sib-. re'-. sib-. re'-. sib-. re'-. |
    r do' r sib r la |
    sib-. re'-. sib-. re'-. sib-. re'-. |
    r do'-. la-. do'-. la-. do'-. |
    r do'-. la-. do'-. la-. do'-. |
    r re'-. sib-. re'-. sib-. re'-. |
    mi''8-.\fp sol'-. r sol' r sol' |
    r16. fa'32\f fa'16. la'32 la'16. do''32 do''16. fa''32 fa''8 r |
    mi''8. do'16 do'4 r |
    r8 do'-.\p la-. do'-. la-. do'-. |
    r sib-. sol-. sib-. sol-. sib-. |
    r la-. la-. fa'-. la'-. la'-. |
    r sol' r fa' r mi' |
    r do'-. la-. do'-. la-. do'-. |
    r sib-. sol-. sib-. sol-. sib-. |
    r la-. la-. fa'-. la'-. la'-. |
    r sol' r fa' r mi' |
    fa'8 la'4 la'8( sib') sib'( |
    la') r fa' r mi' r |
    fa' la'4 la'8( sib') sib'( |
    la') r fa' r mi' r |
    fa'4 r r |
    r8 do' r do' r do' |
    r sib r sib r sib |
    r sib' r sib' r sol' |
    r fa' r fa' r mi' |
    fa'2. r8 la r fa' r mi' |
    fa'2. |
    r8 la r fa' r mi' |
    fa'8. do'16 la4 r\fermata |
  }
>>
<<
  \tag #'violino1 { sib''2\f }
  \tag #'violino2 { <re' sib'>4\f r }
>> r8 \grace { do'''16([ sib'' la'']) } sib''8( re''') sib''-. |
fa''-.[ \grace { sol''16([ fa'' mi'']) } fa''8( sib'') fa''-.]
re''-.[ \grace { mib''16([ re'' do'']) } re''8( fa'') re''-.] |
sib'8 <>\p <<
  \tag #'violino1 {
    re'8[ re' re'] re'4 r |
    r8 re' re' re' re'4 r |
    r8 re' re' re' re'4 r |
    mib'1\fp |
  }
  \tag #'violino2 {
    sib8[ sib sib] sib4 r |
    r8 sib sib sib sib4 r |
    r8 sib sib sib sib4 r |
    do'1\fp |
  }
>>
<re' sib' fa''>4\f r r2 |
<do' sol' mib''>4 r r2 |
<mib' la'>4-. <re' sib'>-. r2 |
<la fa' do''>4 r <sib fa' re''> r |
<<
  \tag #'violino1 {
    r8 do'\p do' do' r re' re' re' |
    r do' do' do' r re' re' re' |
    do'4 r8 re'''16\f re''' do''' do''' re''' re''' do''' do''' re''' re''' |
    do'''4-.
  }
  \tag #'violino2 {
    r8 la\p la la r sib sib sib |
    r la la la r sib sib sib |
    la4 r8 sib''16\f sib'' la'' la'' sib'' sib'' la'' la'' sib'' sib'' |
    la''4-.
  }
>> <fa' la>4-. r2 |
<>\p <<
  \tag #'violino1 {
    fa'4 r fa' r |
    fa' r r2 |
    mib'1( |
    re'4) r r2 |
    fa'4 r fa' r |
    fa' r r2 | \allowPageTurn
    la'1 |
    r4 sib' r si' |
    r8 mib' sol' mib' do'' sol' mib'' do'' |
    re''( do'') sib'!-. sib'-. sib'( re'' do'' la') |
    sib'8 sib'4 sib' sib' sib'8~ |
    sib' sib'4 sib' sib' sib'8~ |
    sib' sib4 sib sib sib8~ |
    sib sib4 sib sib sib8~ |
    sib4 r r2 |
    <sol fa'>1\fp |
    r8 do'( mib' sol' do'') do''-. do''-. do''-. |
    do''4( mib''2 re''4) |
    do''( mib''2 re''4) |
    do''( mib''2 re''4) |
    r8 do'' do'' do'' r do''-. do''( reb'') |
    r reb'' reb'' reb'' r reb' reb' reb' |
    do'4 r r2 |
    sib'4 r sol' r |
    <sol mi'>4\f r r2\fermata |
    r4 <fa' la>4\f r2\fermata |
    la'4\p r r2\fermata |
    do''4 r r2\fermata |
    fa'4 r fa' r |
    fa' r r2 |
    la'1( |
    sib'4) r r2 |
    fa'4 r fa' r |
    fa' r r2 |
    la'1 |
    r4 sib' r si' |
    r8 mib' sol' mib' do'' sol' mib'' do'' |
    re''( do'') sib'!-. sib'-. sib'( re'' do'' la') |
    sib'4 r r2 |
    sib'8:16\f mib'':16 re'':16 do'':16 sib':16 sol'':16 fa'':16 mib'':16 |
    re'':16 re''':16 do''':16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 |
    re''4 r do''\p r |
    sib' r r2 |
    sib'8:16\f mib'':16 re'':16 do'':16 sib':16 sol'':16 fa'':16 mib'':16 |
    re'':16 re''':16 do''':16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 |
    re''4 r do''\p r |
    r4 sib'( re'' fa'') |
    r do''( mib'' fa'') |
    r sib'( re'' fa'') |
    r do''( mib'' fa'') |
    r sib'( re'' fa'') |
    r sol'( do'' mib'') |
    r re''( fa'' la') |
    r4 sib' r do'' |
    r re'' r mib'' |
    fa'' r r2 |
    mib''4 r r2 |
  }
  \tag #'violino2 {
    re'4 r do' r |
    re' r r2 |
    do'1( |
    sib4) r r2 |
    re'4 r do' r |
    re' r r2 | \allowPageTurn
    mib'1 |
    r4 re' r lab' |
    r8 do' mib' do' sol' mib' do'' sol' |
    fa'( mib') re'-. re'-. re'( fa' mib' do') |
    re'4 r r2 |
    lab'8 lab'4 lab' lab' lab'8( |
    sol') sol4 sol sol sol8( |
    lab) lab4 lab lab lab8( |
    sol4) r r2 |
    <sol re'>1\fp~ |
    <sol mib'>4 r r2 |
    r8 fa'( mib' fa' mib' fa' re' fa') |
    do'( fa' mib' fa' mib' fa' re' fa') |
    do'( fa' mib' fa' mib' fa' re' fa') |
    r sol' sol' sol' r sol'-. sol'( sib') |
    r sib' sib' sib' r sib sib sib |
    la4 r r2 |
    sol'4 r mi' r |
    sib4\f r r2\fermata |
    r4 <la fa'>\f r2\fermata |
    do'4\p r r2\fermata |
    la'4 r r2\fermata |
    re'4 r do' r |
    re' r r2 |
    mib'1( |
    re'4) r r2 |
    re'4 r do' r |
    re' r r2 |
    mib'1 |
    r4 re' r lab' |
    r8 do' mib' do' sol' mib' do'' sol' |
    fa'( mib') re'-. re'-. re'( fa' mib' do') |
    sib16\f sib mib' mib' re'8:16 do':16 sib:16 sol':16 fa':16 mib':16 |
    re'16 sib' sib' sib' sib'4:16 sib'2:16 |
    sib'8:16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 re'':16 do'':16 |
    sib'4 r la'\p r |
    sib8:16\f mib':16 re':16 do':16 sib:16 sol':16 fa':16 mib':16 |
    re'16 sib' sib' sib' sib'4:16 sib'2:16 |
    sib'8:16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 re'':16 do'':16 |
    sib'4 r la'\p r |
    sib' r r re'( |
    mib'2) r4 mib'( |
    re'2) r4 re'( |
    mib'2) r4 mib'( |
    re'2) r4 fa'( |
    sol'2) r4 mib'( |
    re'2) r4 do'( |
    sib) re' r fa' |
    r sib' r la' |
    sib' r r2 |
    do''4 r r2 |
  }
>>
\tuplet 6/4 <re'' sib''>2.:8\f \tuplet 6/4 <do'' la''>:8 |
<sib'' sib'>4
<<
  \tag #'violino1 {
    sib'4-.\p r do''-. |
    r re''-. r mib''-. |
    fa''4-. r r2 |
    r4 re''-. r do''-. |
    r re' r do' |
    r re''-. fa''-. re''-. |
    r do''-. mib''-. do''-. |
    r sib' r la' |
    r sib' r do'' |
    r re'' r mib'' |
    fa'' r r2 |
    mib''4 r r2 |
  }
  \tag #'violino2 {
    re'4-.\p r fa'-. |
    r sib'-. r do''-. |
    re''-. r r2 |
    r4 sib'-. r la'-. |
    r sib r la |
    r sib'-. re''-. sib'-. |
    r mib'-. sol'-. mib'-. |
    r re' r do' |
    r re' r fa' |
    r sib' r la' |
    sib' r r2 |
    do''4 r r2 |
  }
>>
\tuplet 6/4 <re'' sib''>2.:8\f \tuplet 6/4 <do'' la''>:8 |
<sib'' sib'>4 <>\p <<
  \tag #'violino1 {
    re'4-. fa'-. re'-. |
    r mib'-. sol'-. mib'-. |
    re' fa' re' do' |
    r re' fa' re' |
    r mib' sol' mib' |
    re'4 fa' re' do' |
    r8 fa'' fa'' fa'' fa''2:8 |
    re'8\sfp re'4 re' re' re'8 |
    r4 mib' r sol' |
    r sib'-. mib''-. sol'-. |
    fa' r r2 |
    do':16\f do':16 |
    fa'4 fa''-.\p r mib''-. |
    r re''-. r do''-. |
    r fa''-. r mib''-. |
    r re''-. r do''-. |
    fa''2:16 sol'':16\cresc |
    re''':16 do''':16 |
    sib''4\f r sib'8:16 mib'':16 re'':16 do'':16 |
    sib':16 sol'':16 fa'':16 mib'':16 re'':16 sib'':16 la'':16 sol'':16 |
    fa'':16 re''':16 do''':16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 |
    re''4 <mib' sib' sol''> <re' sib' fa''> <do' fa' la'> |
    <re' sib'> <re' sib' sib''> sib sib |
    sib2 r |
  }
  \tag #'violino2 {
    sib4-. re'-. sib-. |
    r do'-. mib'-. do'-. |
    sib re' sib la |
    r sib re' sib |
    r do' mib' do' |
    sib re' sib la |
    r8 sib' sib' sib' sib'2:8 |
    sib8\sfp sib4 sib sib sib8 |
    r4 sib r mib' |
    r sol'-. sol'-. mib'-. |
    re' r r2 |
    la2:16\f la:16 |
    sib4 re''-.\p r do''-. |
    r sib'-. r la'-. |
    r re''-. r do''-. |
    r sib'-. r la'-. |
    sib'2:16 sib':16\cresc |
    sib'':16 la'':16 |
    sib''4\f r r2 |
    sib8:16 mib':16 re':16 do':16 sib16 sib' sib' sib' sib'4:16 |
    sib'8:16 sib'':16 la'':16 sol'':16 fa'':16 mib'':16 re'':16 do'':16 |
    sib'4 <mib' sib' sol''> <re' sib' fa''> <mib' la'> |
    <re' sib'> <sib fa' re''> sib sib |
    sib2 r |
  }
>>
