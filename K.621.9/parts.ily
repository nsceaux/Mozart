\piecePartSpecs
#`((oboi #:score "score-voix" #:tag-global all
         #:music ,#{ s2.*43\break #})
   (fagotti #:score "score-voix" #:tag-global all
            #:music ,#{ s2.*43\break #})
   (clarinetti #:tag-global clarinetti
               #:instrument ,#{\markup\center-column { Clarinetto Solo in B }#})
   (corni #:score "score-voix" #:tag-global ()
          #:music ,#{ s2.*43\break #}
          #:instrument "Corni in B")
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
