\version "2.19.80"
\include "common.ily"

\opusTitle "K.621 – N°9 – Parto, ma tu ben mio"

\header {
  title = \markup\center-column {
    \line\italic { La clemenza di Tito }
    \line { N°9 – Aria: \italic { Parto, ma tu ben mio } }
  }
  opus = "K.621"
  date = "1791"
  copyrightYear = "2019"
}

\includeScore "K.621.9"
