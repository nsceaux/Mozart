Par -- to, par -- to, ma tu ben mi -- o,
me -- co ri -- tor -- na in pa -- ce,
me -- co ri -- tor -- na in pa -- ce;
sa -- rò qual più ti pia -- ce,
sa -- rò, qual più ti pia -- ce,
quel che vor -- rai fa -- rò, vor -- rai fa -- rò.

Par -- to, ma tu ben mi -- o,
me -- co ri -- tor -- na in pa -- ce,
sa -- rò __ qual più ti pia -- ce,
quel che vor -- rai fa -- rò, si, fa -- rò,
qual più ti pia -- ce,
quel __ che vor -- rai fa -- rò,
quel che __ vor -- rai fa -- rò,
quel che vor -- rai fa -- rò.

Guar -- da -- mi, e tut -- to o -- bli -- o,
e a ven -- di -- car -- ti io vo -- lo,
e a ven -- di -- car -- ti io vo -- lo.
A que -- sto sguar -- do so -- lo
da me sì pen -- se -- rà,
da me sì pen -- se -- rà.

Par -- to, ma tu ben mi -- o,
me -- co ri -- tor -- na in pa -- ce,
sa -- rò qual più ti pia -- ce, __
quel che vor -- rai fa -- rò,
quel che vor -- rai, fa -- rò.

Guar -- da -- mi, guar -- da -- mi!
A que -- sto sguar -- do so -- lo
da me sì pen -- se -- rà,
da me __ sì pen -- se -- rà.

Guar -- da -- mi e tut -- to ob -- bli -- o,
e a ven -- di -- car -- ti io vo -- lo.
Ah qual po -- ter, o De -- i!
do -- na -- ste al -- la bel -- tà,
do -- na -- ste al -- la __ bel -- tà, __
al -- la bel -- tà,
do -- na -- ste al -- la bel -- tà,
do -- na -- ste al -- la bel -- tà, __
al -- la bel -- tà,
ah qual po -- ter, o Dei!
do -- na -- ste al -- la bel -- tà,
do -- na -- ste al -- la bel -- tà,
al -- la bel -- tà,
al -- la bel -- tà,
al -- la bel -- tà.
