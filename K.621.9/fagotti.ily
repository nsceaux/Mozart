\clef "bass" \tag#'fagotti <>^"a 2." sib8.\f fa16 fa4 r8. fa16 |
fa8. re16 re4 r8. re16 |
re8. sib,16 sib,4 r |
R2.*39 |
R2.\fermataMarkup |
\tag#'fagotti <>^"a 2." sib2\f r8 sib-. re'-. sib-. |
fa-. fa-. sib-. fa-. re-. re-. fa-. re-. |
sib,4 r r2 |
R1*9 |
r4 r8 fa8\f fa fa fa fa |
fa4 fa, r2 |
R1*24 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*10 |
\tag#'fagotti <>^"a 2." sib8\f mib' re' do' sib mib' re' do' |
sib sol' fa' mib' re' mib' re' do' |
sib sib, do re mib fa sol la |
sib4 r fa\p r |
sib, r sib8\f mib' re' do' |
sib sol' fa' mib' re' mib' re' do' |
sib sib, do re mib fa sol la |
sib4 r fa\p r |
sib, r r2 |
R1*10 |
<>\f <<
  \tag #'(fagotto1 fagotti) { re'2 do' | }
  \tag #'(fagotto2 fagotti) { sib2 la | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib4 } { sib }
>> r4 r2 |
R1*11 |
<>\f <<
  \tag #'(fagotto1 fagotti) { re'2 do' | }
  \tag #'(fagotto2 fagotti) { sib2 la | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib4 } { sib }
>> r4 r2 |
R1*16 |
<>\cresc <<
  \tag #'(fagotto1 fagotti) { re'2 do' | }
  \tag #'(fagotto2 fagotti) { sib2 la | }
>>
\tag#'fagotti <>^"a 2." sib8\f mib' re' do' sib sol' fa' mib' |
re' mib' re' do' sib sol fa mib |
re sib, do re mib fa sol la |
sib4 mib fa fa, |
sib, r sib, sib, |
sib,2 r |
