\clef "alto" sib'8.\f fa'16 fa'4 r8. fa'16 |
fa'8. re'16 re'4 r8. re'16 |
re'8. sib16 sib4 r |
r r8. sib'16 sib4 |
r r8. fa'16 fa4 |
R2. |
<<
  { fa2.(\p |
    sib8) re'( mib' mi' fa' fad' |
    sol'4) } \\
  { s2. |
    s8 sib( do' dod' re') re'( |
    mib'4) }
>> r r |
sib4 r8 sib( do' re') |
mib'4( re' do') |
fa8 r fa r fa r |
sib4 r r |
mib8 r fa r fa r |
sib4 r r |
fa r r |
fa r r |
sib r r |
sol'8-.\fp mi'-. r mi' r mi' |
r16. do'32\f do'16. fa'32 fa'16. la'32 la'16. do''32 fa'8 r |
sol'8. <do' do>16 q4 r |
fa4\p r r |
do r r |
fa r r |
r8 re' r do' r do' |
fa4 r r |
do r r |
fa4 r r |
r8 re' r do' r sib |
la4 r r |
do'2.~ |
do'4 r r |
do'2.~ |
do'4 r r |
fa2.~ |
fa8 r fa r fa r |
fa'2~ fa'8( re') |
do'2. |
fa4( la sib) |
do'2. |
fa4( la sib) |
do'2( sib4) |
la8. fa'16 fa4 r\fermata |
sib'2\f r8 sib'-. re''-. sib'-. |
fa'-. fa'-. sib'-. fa'-. re'-. re'-. fa'-. re'-. |
sib4 r r2 |
sib4\p r r2 |
sib4 r r2 |
la1\fp |
sib4\f r r2 |
mib'4 r r2 |
fa'4-. sib-. r2 |
fa'4 r fa' r |
fa\p r fa r |
fa r fa r |
fa r8 fa'\f fa' fa' fa' fa' |
fa'4-. fa-. r2 |
sib4\p r la r |
sib r r2 |
la1( |
sib4) r r2 |
sib4 r la r |
sib r r2 |
do'1 |
r4 sib r re' |
mib' mib' mib' mib' |
fa'2 fa |
sib4 r8 do'( reb' re' mib' mi') |
fa'4( re' sib re') |
mib'!4 r r2 |
fa8 fa4 fa fa fa8( |
mib4) r r2 |
si1\fp |
do'4 r r2 |
r4 do'2( sib!4) |
la4( do'2 sib4) |
la4( do'2 sib4) |
mib'4 r mib' r |
mi' r mi r |
fa r r2 |
do'4 r do' r |
<do' do>4\f r r2\fermata |
r4 fa\f r2\fermata |
fa'4\p r r2\fermata |
fa'4 r r2\fermata | \allowPageTurn
sib4 r la r |
sib r r2 |
do'1( |
sib4) r r2 |
sib4 r la r |
sib r r2 |
do'1 |
r4 sib r re' |
mib' mib' mib' mib' |
fa'2 fa |
sib4 r sib8:16\f mib':16 re':16 do':16 |
sib:16 sol':16 fa':16 mib':16 re':16 mib':16 re':16 do':16 |
sib:16 re:16 mib:16 fa:16 sol:16 la:16 sib:16 do':16 |
re'4 r fa'\p r |
sib r sib8:16\f mib':16 re':16 do':16 |
sib:16 sol':16 fa':16 mib':16 re':16 mib':16 re':16 do':16 |
sib:16 re:16 mib:16 fa:16 sol:16 la:16 sib:16 do':16 |
re'4 r fa'\p r |
re' r r sib( |
do'2) r4 do'( |
sib2) r4 sib( |
do'2) r4 do'( |
sib2) r4 re'( |
mib'2) r4 do'( |
sib2) r4 la( |
sib) r la r |
sib r do' r |
re' r r2 |
sol'4 r r2 |
fa'2:8\f fa:8 |
sib4 r la\p r |
sib r sib r |
sib r r2 |
sib'4 r fa' r |
sib r fa r |
sib r r2 |
mib'4 r r2 |
fa'4 r fa r |
sib r la r |
sib r do' r |
re' r r2 |
sol'4 r r2 |
fa'2:8\f fa:8 |
sib4 r r2 |
mib'4\p r r2 |
fa'4 r r2 |
sib4 r r2 |
mib'4 r r2 |
fa4 r r2 |
r8 re' re' re' re'2:8 |
fa8\sfp fa4 fa fa fa8 |
r4 sol r sib |
r mib' r mib |
fa4 r r2 |
fa2:8\f fa:8 |
re'4-.\p r mib'-. r |
fa'-. r mib'-. r |
re'-. r mib'-. r |
fa'-. r mib'-. r |
re'2:8 mib':8\cresc |
fa':8 fa:8 |
sib16\f sib mib' mib' re'8:16 do':16 sib:16 sol':16 fa':16 mib':16 |
re'16 sib' sib' sib' sib'4:16 sib'8:16 sol':16 fa':16 mib':16 |
re':16 re:16 mib:16 fa:16 sol:16 la:16 sib:16 do':16 |
re'4 mib' fa' fa |
sib r sib sib |
sib2 r |

