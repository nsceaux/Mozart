\clef "soprano/treble"  R2.*3 |
sib'4 fa' r |
do'' fa' r |
sib' re'' fa'' |
fa'2( mib'4) |
re'4 r r |
sol'4 sib'8 sib' mib''8. sol'16 |
sib'8.([ sol'16]) fa'4 r |
do''4 re'' mib''8.([ do''16]) |
sib'4.( re''8) do''([ la']) |
sib'8 sib' r4 r |
r r r8 fa' |
re''4. do''16([ sib']) la'([ sib']) mib''([ re'']) |
si'8.([ do''16]) do''4 r8 fa' |
mib''4. re''16([ do'']) si'([ do'']) fa''([ mib'']) |
dod''8.([ re''16]) re''4 r |
sib'4 sol'8 mi' do'8. sib'16 |
la'4 r8 do'' fa''8. si'16 |
do''4 r r |
R2.*2 |
fa''8.([ do''16]) do''4 r |
sib' la' sol' |
sib'16([ la' re'' do'']) do''4 r |
R2. |
fa''8.([ do''16]) do''4 r8 do'' |
do''([ sib']) la'4 sol' |
fa'8 fa' r4 r8 do'' |
do''([ fa''])~ fa''16([ mi'']) re''([ do'']) sib'8. do''16 |
la'8 la' r4 r |
do''4 fa''16([ mi'']) re''([ do'']) sib'8. do''16 |
la'4 do'' r8 do'' |
mib''!4. do''8 la'8. mib'16 |
re'8 re' r4 r |
re''4.( fa''8) re'' sib' |
la'8.([ sib'16]) \override Script.avoid-slur = #'outside \afterGrace sol'2\trill( { la'16[ sib']) } |
do''4 r re''8([ mi''16 fa'']) |
fa''4~ fa''16([ mi'']) re''([ do'']) do''([ sib']) la'([ sol']) |
do''4 r re''8([ mi''16 fa'']) |
fa''4. do''16([ la']) sol'8. do''16 |
fa'4 r r\fermata |
R1*6 |
fa''2 re''4. sib'8 |
mib''2. do''4 |
la' sib' r r8 fa' |
do''4. la'8 re''4. sib'8 |
do''4 fa' r r8 re'' |
do''4. fa''8 mib''([\trill re'']) do''([\trill sib']) |
fa''4 fa' r2 |
R1*2 |
r2 r4 re''8([ sib']) |
la'4. la'8 la'4. do''8 |
sib'4 sib' r2 |
R1 |
r2 r4 re''8([ sib']) |
\appoggiatura sib'8 la'4. la'8 la'4. do''8 |
sib'2 r4 si' |
\appoggiatura si'4 do''2 r4 mib''8([ do'']) |
re''([ do''] sib'!2) la'4 |
sib' r r2 |
R1 |
mib''4.( sib'8) sib'4 r |
r r8 sib' sib'4. lab'8 |
sol'4 sol' r2 |
re''4 si'8 sol' fa'4. re'8 |
mib'4 mib' r do'' |
do''( mib''2) re''4 |
do''( mib''2) re''4 |
do''8([ mib'']) mib''2( re''4) |
do''2 do''4 do''8([ reb'']) |
reb''2. sib'4 |
la' r r2 |
sib'2 sol'4. sib'8 |
mi'2.\fermata do''4 |
fa' r\fermata do''8.\fermata la'16 la'4 |
r2\fermata mib''!8.\fermata do''16 do''4 |
r2\fermata r |
R1 |
r2 r4 re''8([ sib']) |
\appoggiatura sib'4 la'4. la'8 la'4. do''8 |
sib'4 sib' r2 |
R1 |
r2 r8 fa''([ re'' sib']) |
\appoggiatura sib'8 la'4. la'8 la'4( sib'8) do''-. |
\appoggiatura do''4 sib'2 r4 si' |
\appoggiatura si'4 do''2~ do''8([ sol']) mib''([ do'']) |
re''([ do''] sib'!2) la'4 |
sib' r r2 |
R1*2 |
sib'4 re''8. sib'16 mib''4. la'8 |
sib'4 sib' r2 |
R1*2 |
sib'4 re''8. fa''16 mib''4. la'8 |
sib'4 sib' r2 |
fa''8([ mib'']) re'' do'' do''([ sib']) sib'([ la']) |
sib'4.( do''8) re''4 r8 re'' |
fa''([ mib'']) re''([ do'']) do''([ sib']) sib'([ la']) |
sib'2 r4 re''8([ sib']) |
\appoggiatura la'4 sol'2. mib''8([ do'']) |
re''([ do''] sib'2) la'4 |
sib'8*2/3[\melisma la' sib'] re''[ do'' sib'] do''[ sib' do''] mib''[ re'' do''] |
re''[ mib'' re''] fa''[ mib'' re''] mib''[ fa'' mib''] sol''[ fa'' mib''] |
fa''[ sib'' la''] sib''[ la'' sol''] fa''[ mib'' re''] do''[ sib' la'] |
sol'[ do'' si'] do''[ mib'' re''] mib''[ sol'' fa'']\melismaEnd mib''[ re'' do''] |
sib'!2 do''\trill |
sib'4 r r fa' |
sib'8*2/3[\melisma la' sib'] re''[ do'' sib'] do''[ sib' do''] mib''[ re'' do''] |
re''[ mib'' re''] fa''[ mib'' re''] mib''[ fa'' mib''] sol''[ fa'' mib''] |
fa''2.\melismaEnd fa'4 |
fa'2. mib'4 |
re'2 r4 fa''8([ re'']) |
do''2. mib''8([ do'']) |
sib'2 la' |
sib'8*2/3[\melisma la' sib'] re''[ do'' sib'] do''[ sib' do''] mib''[ re'' do''] |
re''[ mib'' re''] fa''[ mib'' re''] mib''[ fa'' mib''] sol''[ fa'' mib''] |
fa''[ sib'' la''] sib''[ la'' sol''] fa''[ mib'' re''] do''[ sib' la'] |
sol'[ do'' si'] do''[ mib'' re''] mib''[ sol'' fa'']\melismaEnd mib''[ re'' do''] |
sib'!2 do'' |
re'' r4 fa''8([ re'']) |
\appoggiatura re''4 do''2 r4 mib''8([ do'']) |
\appoggiatura do''4 sib'2. do''4 |
\appoggiatura do''4 re''2 r4 fa''8([ re'']) |
\appoggiatura re''4 do''2 r4 mib''8([ do'']) |
\appoggiatura do''4 sib'2 r4 la' |
fa''1 |
re' |
mib'4\melisma sol'2 sib'4~ |
sib' mib''2\melismaEnd sol''4 |
fa'1 |
do''\trill |
fa''4 r mib'' r |
re'' r do'' r |
fa'' r mib'' r |
re'' r do'' r |
fa''2 sol'' |
fa'' la' |
sib'4 r r2 |
R1*5 |
