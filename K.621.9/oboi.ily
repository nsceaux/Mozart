\clef "treble" \tag#'oboi <>^"a 2." sib''8.\f fa''16 fa''4 r8. fa''16 |
fa''8. re''16 re''4 r8. re''16 |
re''8. sib'16 sib'4 r |
R2.*39 |
R2.\fermataMarkup |
<>\f <<
  \tag #'(oboe1 oboi) { sib''2 }
  \tag #'(oboe2 oboi) { sib' }
>> r4 \tag#'oboi <>^"a 2." re'''8-. sib''-. |
fa''4 sib''8-. fa''-. re''4 fa''8-. re''-. |
sib'4 r r2 |
R1*9 |
r4 r8 <>\f <<
  \tag #'(oboe1 oboi) {
    sib''8-. la''-. sib''-. la''-. sib''-. |
    la''4-. fa''-.
  }
  \tag #'(oboe2 oboi) {
    re''8-. do''-. re''-. do''-. re''-. |
    do''4-. la'-.
  }
>> r2 |
R1*24 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*11 |
\tag#'oboi <>^"a 2." sib'8\f mib'' re'' do'' sib' sol'' fa'' mib'' |
re'' <<
  \tag #'(oboe1 oboi) {
    re'''8 do''' sib'' la'' sol'' fa'' mib'' |
    re''4 s do'' s |
  }
  \tag #'(oboe2 oboi) {
    sib''8 la'' sol'' fa'' mib'' re'' do'' |
    sib'4 s la' s |
  }
  { s4. s2 | s4 r s\p r | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 } { sib' }
>> r4 r2 |
\tag#'oboi <>^"a 2." sib'8\f mib'' re'' do'' sib' sol'' fa'' mib'' |
re'' <<
  \tag #'(oboe1 oboi) {
    re'''8 do''' sib'' la'' sol'' fa'' mib'' |
    re''4 s do'' s |
  }
  \tag #'(oboe2 oboi) {
    sib''8 la'' sol'' fa'' mib'' re'' do'' |
    sib'4 s la' s |
  }
  { s4. s2 | s4 r s\p r | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 } { sib' }
>> r4 r2 |
R1*10 |
<>\f <<
  \tag #'(oboe1 oboi) { re''2  do'' | }
  \tag #'(oboe2 oboi) { sib'2 la' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 } { sib' }
>> r4 r2 |
R1*11 |
<>\f <<
  \tag #'(oboe1 oboi) { re''2  do'' | }
  \tag #'(oboe2 oboi) { sib'2 la' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'4 } { sib' }
>> r4 r2 |
R1*16 |
<<
  \tag #'(oboe1 oboi) { sib''2 la'' | sib''4 }
  \tag #'(oboe2 oboi) { re''2 do'' | sib'4 }
  { s1\cresc | s4\f }
>> r4 \tag#'oboi <>^"a 2." sib'8 mib'' re'' do'' |
sib' sol'' fa'' mib'' re'' sib'' la'' sol'' |
fa'' <<
  \tag #'(oboe1 oboi) {
    re'''8 do''' sib'' la'' sol'' fa'' mib'' |
    re''4 sol'' fa'' la'' |
    sib'' s sib'' sib'' |
    sib''2
  }
  \tag #'(oboe2 oboi) {
    sib''8 la'' sol'' fa'' mib'' re'' do'' |
    sib'4 mib'' re'' do'' |
    re'' s re'' re'' |
    re''2
  }
  { s4. s2 | s1 | s4 r }
>> r2 |
