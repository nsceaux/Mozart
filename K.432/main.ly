\version "2.19.80"
\include "common.ily"

\opusTitle "K.432 – Cosi dunque tradisei"

\header {
  title = \markup\center-column {
    Recitativ und Arie
    \italic Cosi dunque tradisei
  }
  subtitle = "für Bass mit Begleitung des Orchesters"
  opus = "K.432"
  date = "1783"
  copyrightYear = "2018"
}

\includeScore "K.432/recit"
\pageBreakCond #(not (symbol? (*part*)))
\includeScore "K.432/aria"
