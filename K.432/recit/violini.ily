\clef "treble" fa'\f r8 fa' lab' fa' re' do' |
<si sol' re''>4 r r2 |
R1 |
r8. <<
  \tag #'violino1 {
    sol''16 sol''4 r8. do'''16 do'''8. re''16 |
    re''1~ |
    re''2 r8. sib''16 sib''8. re'''16 |
    re'''8. la''16 la''8. do'''16 do'''2\fermata |
  }
  \tag #'violino2 {
    re''16 re''4 r8. sol''16 sol''8. la'16 |
    la'1~ |
    la'2 r8. re''16 re''8. la''16 |
    la''8. re''16 re''8. sol''16 sol''2\fermata |
  }
>>
r8 <>\p <<
  \tag #'violino1 {
    do''8 do''( re'') mib''( re'') do''4~ |
    do''
  }
  \tag #'violino2 {
    sol'8 sol'( si') do''( fa') sol'4~ |
    sol'
  }
>> r4 <<
  \tag #'violino1 { re''2( | mib''4)(\f la') sib'2\p | }
  \tag #'violino2 { fad'4( fa')~ | fa'\f( mib'8.\trill re'32 mib') reb'2\p | }
>>
r8 <<
  \tag #'violino1 { sib'8-.(\p sib'-. sib'-.) dob''2\fp | }
  \tag #'violino2 { fa'8(-.\p fa'-. fa'-.) lab'2\fp | }
>>
r8. <>\f <<
  \tag #'violino1 { sib'16 sib'4 r8. sib''16 sib''4 | }
  \tag #'violino2 { mi'!16 mi'4 r8. mi''16 mi''4 | }
>>
r4 <>\p <<
  \tag #'violino1 {
    do''2.~ |
    do''2. lab''4:32\fp |
    lab''2:32 lab'':32 |
    lab'':32 sol''4:32 solb''4:32\fp |
    solb''2:32 solb'':32 |
    solb'':32 fa''8
  }
  \tag #'violino2 {
    sol'2.~ |
    sol'2( lab'4) fa'8.\fp fa'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    fa'8. fa'16 fa'8. fa'16 mib'8. mib'16 mib'8.\fp mib'16 |
    mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 |
    mib'8. mib'16 mib'8. mib'16 reb'8
  }
>> r8 r4\fermata |
<<
  \tag #'violino1 {
    R1 |
    sol'!8.\p sol'16 sol'8. sol'16 sol'8. sol'16 la'!8. la'16 |
    sib'8.\cresc sib'16 do''8. do''16 reb''4\f
  }
  \tag #'violino2 {
    fa'8.\p fa'16 fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    fa'8.\cresc fa'16 mib'8. mib'16 reb'4\f
  }
>> r4 |
r8 <<
  \tag #'violino1 {
    <<
      { reb'''4 reb'''8~ reb'''4 } \\
      { mi''4 mi''8~ mi''4 }
    >> r4 |
    lab'\fermata\p r r mi' |
  }
  \tag #'violino2 {
    <<
      { sol''4 sol''8~ sol''4 } \\
      { sib'4 sib'8~ sib'4 }
    >> r4 |
    fa'4\fermata\p r r sib |
  }
>>
