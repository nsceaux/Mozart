\clef "bass" fa4\f r8 fa lab fa re do |
si,1~ |
si, |
r8. si,16 si,4 r8. mib16 mib8. fad16 |
fad1~ |
fad2 r8. sol16 sol8. fad16 |
fad8. fa16 fa8. mib16 mib2\fermata |
r8 mib\p mib( re) do( re) mib4~ |
mib r re2( |
do)\f sib,\p |
r8 reb-.(\p reb-. reb-.) re2\fp |
r8. reb16\f reb4 r8. do16 do4 |
r4 mi2.\p~ |
mi2( fa4) si,\fp~ |
si,1~ |
si,2( do4) la,\fp~ |
la,1~ |
la,2( sib,8) r r4\fermata |
sib,8.\p sib,16 do8. do16 reb8. reb16 do8. do16 |
si,!8. si,16 do8. do16 re!8. re16 do8. do16 |
reb8.\cresc reb16 la,8. la,16 sib,4\f r |
r8 sib,( sib) sib~ sib4 r |
si,!\p\fermata r r do |
