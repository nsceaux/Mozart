Co -- sì dun -- que tra -- di -- sci,
di -- sle -- al prin -- ci -- pes -- sa…
Ah, fol -- le! Ed i -- o
son d’ac -- cur -- sar -- la ar -- di -- to!
Si la -- gna un tra -- di -- tor d’es -- ser tra -- di -- to!
Il me -- ri -- ta -- i. Fug -- gì, Se -- ba -- ste, fug -- gi.
Ah do -- ve, do -- ve fug -- gi -- rò da me stes -- so?
Ah por -- to in se -- no il car -- ne -- fi -- ce mi -- o.
Do -- vun -- que io va -- da,
il ter -- ror, lo spa -- ven -- to
se -- gui -- ran la mia trac -- cia;
la col -- pa mi -- a mi sta -- rà sem -- pre in fac -- cia.
