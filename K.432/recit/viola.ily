\clef "alto" fa'4\f r8 fa' lab' fa' re' do' |
si4 r r2 |
R1 |
r8. si16 si4 r8. mib'16 mib'8. fad'16 |
fad'1~ |
fad'2 r8. sol'16 sol'8. fad'16 |
fad'8. fa'16 fa'8. mib'16 mib'2\fermata |
r8 mib'\p mib'( fa') sol'( si) mib'4~ |
mib' r la2~ |
la4\f( fa) fa2\p |
r8 reb'-.(\p reb'-. reb'-.) fa'2\fp |
r8. sol'16\f sol'4 r8. sol'16 sol'4 |
r4 mi'2.\p~ |
mi'2( fa'4) re'!8.\fp re'16 |
re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
re'8. re'16 re'8. re'16 sol8. sol16 do'8.\fp do'16 |
do'8. do'16 do'8. do'16 do'8. do'16 do'8. do'16 |
do'8. do'16 do'8. do'16 fa8 r r4\fermata |
reb'8.\p reb'16 la!8. la16 sib8. sib16 do'8. do'16 |
re'!8. re'16 do'8. do'16 si!8. si16 mib'8. mib'16 |
reb'8.\cresc reb'16 fa'8. fa'16 fa'4\f r4 |
r8 <<
  { reb''4 reb''8~ reb''4 } \\
  { mi'!4 mi'8~ mi'4 }
>> r4 |
re'!4\fermata\p r r sol |
