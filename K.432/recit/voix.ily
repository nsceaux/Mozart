\clef "bass" R1 |
r4 r8 sol sol4 sol8 sol16 la |
si8 si r si16 si si4 do'8. re'16 |
re'8 sol r sol do' do' r4 |
r4 r8 fad la la r4 |
la8 la16 la re'8. la16 sib8 sib r4 |
r2 r4\fermata r8 sol |
do' do' do' re' mib'4 r |
do'4 do'8 sol la la r4 |
r2 r8 fa fa reb |
sib, sib, r4 dob'4. sib16 lab |
sol8 sol r4 sib8 mi r sol |
do'8 do' r4 do'8 sol r sol16 lab |
sib4 do'8 sol lab lab r4 |
si!4 si8. re'16 re'8 fa r fa16 fa |
lab4. lab16 sol mib8 mib r16 do' do' do' |
la!8 la r la16 la la8 sib16 do' do'8 mib |
r8 mib16 mib solb8 solb16 fa reb8 reb r4\fermata |
R1*2 |
r2 r8 sib sib sib |
reb'2~ reb'8 mi! fa sol |
lab4\fermata mi8. fa16 fa8 do r4 |
