\clef "treble" <<
  \tag #'flauto1 {
    R1 |
    r2 r4 r8 fa''32(\p sol'' lab'' sib'') |
    do'''4.(\fp lab''8 fa''4) r |
    r2 r4 r8 sol''32( lab'' sib'' do''') |
    reb'''4.(\fp sib''8 sol''4) r |
  }
  \tag #'flauto2 {
    R1*2 |
    fa''2\fp r |
    R1 |
    sol''2\fp r |
  }
>>
R1*2 |
r4 <>\p <<
  \tag #'flauto1 {
    fa'''4( mib''' reb''' |
    do''' lab'' sib'' sol'') |
    fa''4 r r r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.\fp( do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r |
    R1*2 |
    sol''1\fp |
    R1 |
    lab''1\fp |
    R1 |
    do'''1\fp |
  }
  \tag #'flauto2 {
    reb'''4( do''' sib'' |
    lab'' fa'' sol'' mi'') |
    fa''4 r r2 |
    r2 r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' do''') |
    fa''4 r r2 |
    R1 |
    mib''!1\fp |
    R1 |
    fa''\fp |
    R |
    sol''\fp |
  }
>>
R1*2 |
<>\fp <<
  \tag #'flauto1 { mib'''1 | re'''4 }
  \tag #'flauto2 { do'''1 | si''4 }
>> r4 r2 |
<<
  \tag #'flauto1 {
    R1 |
    r2 mib'''\p~ |
    mib'''1( |
    re'''4)
  }
  \tag #'flauto2 {
    R1*2 |
    do'''1(\p |
    sib''!4)
  }
>> r4 r2 |
R1 |
r4 <>\p <<
  \tag #'flauto1 {
    la''2 la''4 |
    sib''2\cresc sol''' |
    fad'''8(\fp mib''' do''' la'' fad'' mib'' do'' la') |
    sol'4
  }
  \tag #'flauto2 {
    fad''2 fad''4 |
    sol''2\cresc mi''' |
    mib'''!2.(\fp fad''4) |
    sol''4
  }
>> r4 r2 |
R1 |
r2 r4 <<
  \tag #'flauto1 { fad'''4(\f | sol''')\p }
  \tag #'flauto2 { mib'''4(\f | re''')\p }
>> r4 r2 |
R1 |
<<
  \tag #'flauto1 {
    sol''1\p( |
    sib''1*1/2 s\cresc |
    reb'''!1 |
    mi'''\f |
    fa'''4)\p r r r8 fa''32( sol'' lab'' sib'') |
    do'''4.(\fp lab''8 fa''4) r |
    r2 r4 r8 sol''32( lab'' sib'' do''') |
    reb'''4.(\fp sib''8 sol''4) r |
  }
  \tag #'flauto2 {
    sol''1\p~ |
    sol''1*1/2( s2\cresc |
    sib''1 |
    do''')\f~ |
    do'''4\p r r2 |
    fa''2\fp r |
    R1 |
    sol''2\fp r |
  }
>>
R1*2 |
r4 <>\p <<
  \tag #'flauto1 {
    fa'''4( mib''' reb''' |
    do''' lab'' sib'' sol'') |
    fa''4 r r r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r |
    R1*2 |
    fa'''1\f |
    mib'''!\p |
    reb'''2( do''') |
    reb'''2.( do'''4) |
    si''1\f~ |
    si''4
  }
  \tag #'flauto2 {
    reb'''4( do''' sib'' |
    lab'' fa'' sol'' mi'') |
    fa''4 r r2 |
    r r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' fa'') |
    mi''2 r4 r16*2/3 fa''( sol'' lab'' sib'' do''') |
    reb'''4.(\fp do'''8 sib'' lab'' sol'' do''') |
    fa''4 r r2 |
    R1 |
    fa''1\f~ |
    fa''\p~ |
    fa''~ |
    fa'' |
    lab''\f~ |
    lab''4
  }
>> r4 r2 |
R1 |
<<
  \tag #'flauto1 {
    fa'''1 |
    mi'''4 do''' do''' do''' |
    do'''
  }
  \tag #'flauto2 {
    re'''1 |
    do'''4 do''' do''' do''' |
    do'''
  }
>> r4 r2 |
<<
  \tag #'flauto1 {
    R1*6 |
    reb'''!1\p(
    do''' |
    sib'' |
    lab'' |
    sol'') |
    fa''4
  }
  \tag #'flauto2 {
    R1*5 |
    r2 do'''\p~ |
    do'''( sib'')~ |
    sib''( lab'')~ |
    lab''( sol'')~ |
    sol''( fa'')~ |
    fa''( mi'') |
    fa''4
  }
>> r4 r2 |
R1*5 |
R1^\fermataMarkup |
<<
  \tag #'flauto1 {
    R1*5 |
    do'''1\p~ |
    do'''2 mi''' |
    fa'''1~ |
    fa'''2( mi''') |
    fa'''4 r r2 |
    r4 sol'''( fa''' mi''') |
    fa'''4 fa'' fa'' fa'' |
    fa''2 r |
  }
  \tag #'flauto2 {
    R1*9 |
    r4 reb'''(\p do''' sib'' |
    lab'' sol'' fa'' mi'') |
    fa'' do'' do'' do'' |
    do''2 r |
  }
>>
