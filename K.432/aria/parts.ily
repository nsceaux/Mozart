\piecePartSpecs
#`((flauti #:score-template "score-flauti")
   (oboi #:score-template "score-oboi")
   (fagotti #:score-template "score-fagotti")
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in F")
   (violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (basso #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
