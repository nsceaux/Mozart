\clef "alto" \tuplet 3/2 { fa8\p do' do' } \tuplet 3/2 { do' do' do' } do'8*2/3 do' do' do' do' do' |
do' do' do' do' do' do' do' do' do' do' do' do' |
<>\mf do' do' do' do' do' do' do' do' do' do' do' do' |
<>\p sib sib sib sib sib sib sib sib sib sib sib sib |
<>\mf sib sib sib sib sib sib sib sib sib sib sib sib |
<>\p sib sib sib sib sib sib sib sib sib sib sib sib |
lab lab lab lab lab lab do' do' do' do' do' do' |
sib sib sib fa fa fa fa fa fa fa fa fa |
fa fa fa fa fa fa mi mi mi mi mi mi |
fa lab lab lab lab lab lab lab lab lab lab lab |
<>\fp sol sol sol sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol mi' mi' mi' |
fa'4( reb') lab( fa) |
sol4 r r2 |
R1 |
mib!8*2/3 mib mib mib mib mib mib mib mib mib mib mib |
<>\fp fa fa fa fa fa fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa fa fa fa fa |
<>\fp mib mib mib mib mib mib mib mib mib mib mib mib |
<>\p mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' |
<>\cresc sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
<>\fp fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' |
sol' sol' sol' sol'\p sol' sol' re'2( |
mib' sib)~ |
sib( do')~ |
do'1( |
sib2) si4( re')\f |
do'2.(\p mib'4)\f |
mib'4\p mib'2 mib'4 |
mi'4\cresc mi'2 mi'4 |
mib'!4\fp mib'2 mib'4 |
re'8*2/3 re' re' re' re' re' do' do' do' sol sol sol |
re' re' re' re' re' re' re' re' re' re' re' re' |
<>\cresc sib sib sib sib sib sib sol' sol' sol' <>\f fad' fad' fad' |
<>\p sol' sol' sol' fa'! fa' fa' mib' mib' mib' sol sol sol |
re' re' re' re' re' re' re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' re' re' re' re' |
sib sib sib sib sib sib <>\cresc sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib sib sib sib sib |
<>\f sib sib sib sib sib sib sib sib sib sib sib sib |
lab(\p do') do' do' do' do' do' do' do' do' do' do' |
<>\mf do' do' do' do' do' do' do' do' do' do' do' do' |
<>\p sib sib sib sib sib sib sib sib sib sib sib sib |
<>\mf sib sib sib sib sib sib sib sib sib sib sib sib |
<>\p sib sib sib sib sib sib sib sib sib sib sib sib |
lab lab lab lab lab lab do' do' do' do' do' do' |
sib sib sib fa fa fa fa fa fa fa fa fa |
fa fa fa fa fa fa mi mi mi mi mi mi |
fa( lab) lab lab lab lab lab lab lab lab lab lab |
<>\fp sol sol sol sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol fa fa fa |
<>\fp mi mi mi sol sol sol sol sol sol mi' mi' mi' |
fa'4( reb') lab( fa) |
sol4 r r2 |
<>\f fa8*2/3 fa fa fa fa fa fa fa fa fa fa fa |
<>\p fa fa fa fa fa fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa fa fa fa fa |
fa4 sib2 lab4 |
<<
  { lab8*2/3 lab lab lab lab lab lab lab lab lab lab lab | lab4 } \\
  { fa8*2/3\f fa fa fa fa fa fa fa fa fa fa fa | fa4 }
>> r4 r2 |
<<
  { re'8*2/3 re' re' re' re' re' re' re' re' re' re' re' | } \\
  { si\p si si si si si si si si si si si | }
>>
si'\f si' si' si' si' si' si' si' si' si' si' si' |
do'' do'' do'' do'' do'' do'' do'' do' do' do' do' do' |
do'4 r sol'2\p( |
lab' mib')~ |
mib'( fa')~ |
fa'( do')~ |
do'( reb')~ |
reb'( do'4 sib) |
lab4 r sol2( |
lab sol)~ |
sol( fa)~ |
fa( do)~ |
do( reb) |
reb'( do'4 sib) |
lab do'\f do'2\p~ |
do'1 |
r4 reb' r sib |
do' do' do' do' |
do' do'\f do'2\p~ |
do'4 do'8\f( re') re'2\p~ |
re' fa'\fermata |
fa'4 do'2 do'4~ |
do'1~ |
do'~ |
do'2 reb'!~ |
reb' do'~ |
do'1~ |
do'~ |
do'2 reb'~ |
reb'( do') |
do'4 fa'( mib' reb' |
do' sib lab sol) |
fa do' do' do' |
do'2 r |
