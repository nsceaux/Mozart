\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \flautiInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'flauto1 \includeNotes "flauti"
        >>
        \new Staff <<
          \global \keepWithTag #'flauto2 \includeNotes "flauti"
        >>
      >>
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new GrandStaff \with { \fagottiInstr } <<
        \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "fagotti" >>
        \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "fagotti" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corni in F }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Basso
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \vccbInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*5\break s1*6\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*7\pageBreak
        s1*9\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
