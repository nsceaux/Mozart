\clef "bass" R1 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { r2 r4 r8 fa32(\p sol lab sib) |
    do'4.( lab8 fa4) r |
    r2 r4 r8 sol32( lab sib do') |
    reb'4.( sib8 sol4) r | }
  { R1 | lab2 r | R1 | sib2 r | }
  { s1 | s\fp | s1 | s\fp | }
>>
R1*5 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { r2 r4 r16*2/3 fa( sol lab sib do') |
    reb'4.( do'8 sib lab sol fa) |
    mi2 r4 r16*2/3 fa( sol lab sib do') |
    reb'4.( do'8 sib lab sol do') |
    fa4 r r2 | }
  { do,1\fp~ | do,~ | do,\fp~ | do, | R1 | }
  { s1 | s\fp | s | s\fp | }
>>
R1 |
<>\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol1 | }
  { mib! | }
>>
R1 |
<>\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { si1 | }
  { lab | }
>>
R1 |
<>\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1 | }
  { sol | }
>>
R1 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'1 | do' | si4 }
  { sol1 | la | sol4 }
  { s1\p\cresc | s\fp | }
>> r4 r2 |
R1*7 |
<>\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'1( | re'4) }
  { fad1( | sol4) }
>> r4 r2 |
R1 |
r2 r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'4( | re'4) }
  { fad4( | sol) }
  { s4\f | s4\p }
>> r4 r2 |
R1*4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'1( |
    fa'4) r r r8 fa32( sol lab sib) |
    do'4.( lab8 fa4) r |
    r2 r4 r8 sol32( lab sib do') |
    reb'4.( sib8 sol4) r | }
  { sib1( | lab4) r r2 | lab2 r | R1 | sib2 r | }
  { s1\f | s\p | s\fp | s1 | s\fp | }
>>
R1*5 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { r2 r4 r16*2/3 fa( sol lab sib do') |
    reb'4.( do'8 sib lab sol fa) |
    mi2 r4 r16*2/3 fa( sol lab sib do') |
    reb'4.( do'8 sib lab sol do') |
    fa4 r r2 | }
  { do,1\fp~ | do,~ | do,\fp~ | do, | R1 | }
  { s1 | s\fp | s | s\fp | }
>>
R1 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1 | sib2( la) | fa'( mib') | reb'4 }
  { fa,1~ | fa,~ | fa,~ | fa,4 }
  { s1\f | s\p | }
>> r4 r2 |
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { si1~ | si4 }
  { lab1~ | lab4 }
>> r4 r2 |
R1 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1 | mi'4 }
  { lab1 | sol4 }
>> \tag #'fagotti <>^"a 2." do4 do do |
do r mi2(\p |
fa sol |
lab fa |
re mi |
fa reb! |
sib, do) |
fa4 r \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { r2 | reb'1( | do' | sib | lab | sol) | fa4 }
  { do'2~ |
    do'( sib)~ |
    sib( lab)~ |
    lab( sol)~ |
    sol( fa)~ |
    fa( mi) |
    fa4 }
>> r4 r2 |
R1*5 |
R1^\fermataMarkup |
R1*5 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1~ |
    do'2 mi' |
    fa'1~ |
    fa'2( mi') |
    fa'4 }
  { do'1~ |
    do'~ |
    do'2 lab |
    sol1 |
    lab4 }
>> r4 r2 |
R1 |
r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'4 do' do' | do'2 }
  { lab4 lab lab | lab2 }
>> r2 |

