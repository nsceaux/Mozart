\clef "treble" R1*2 |
<<
  \tag #'oboe1 { do''4.(\fp lab'8 fa'4) r | }
  \tag #'oboe2 { lab'2\fp r | }
>>
R1 |
<<
  \tag #'oboe1 { reb''4.(\fp sib'8 sol'4) r | }
  \tag #'oboe2 { sib'2\fp r | }
>>
R1 |
r2 <>\p <<
  \tag #'oboe1 {
    fa''2~ |
    fa''( mib''4 reb'' |
    do'' lab' sib' sol') |
    fa' r r r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r |
    R1*2 |
    sol'1\fp |
    R1 |
    si'\fp |
    R1 |
    mib''\fp |
  }
  \tag #'oboe2 {
    mib''2( |
    reb'')( do''4 sib' |
    lab' fa' sol' mi') |
    fa'4 r r2 |
    r2 r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' do'') |
    fa'4 r r2 |
    R1 |
    mib'!1\fp |
    R1 |
    lab'\fp |
    R1 |
    do''\fp |
  }
>>
R1*2 |
<<
  \tag #'oboe1 {
    mib''1\fp |
    re''4 r r2 |
    lab''1\p( |
    sol'' |
    fa'')~ |
    fa''4
  }
  \tag #'oboe2 {
    do''1\fp |
    si'4 r sol''2\p~ |
    sol''( fa'')~ |
    fa''( mib'')~ |
    mib''1( |
    re''4)
  }
>> r4 r2 |
R1*2 |
r4 <<
  \tag #'oboe1 {
    sol''2\cresc sib''4 |
    la''2.(\fp mib''4) |
    re''4
  }
  \tag #'oboe2 {
    mi''2\cresc sol''4 |
    fad''2.(\fp la'4) |
    sib'4
  }
>> r4 r2 |
R1 |
r4 <<
  \tag #'oboe1 { dod'''4(\cresc re''' fad''\f | sol'')\p }
  \tag #'oboe2 { dod''4\cresc( re'' mib''\f | re'')\p }
>> r4 r2 |
R1*2 |
<<
  \tag #'oboe1 {
    mib''1*1/2( s2\cresc |
    mi''1 |
    sib''\f |
    lab''4)\p
  }
  \tag #'oboe2 {
    << { s2 s\cresc } reb''!1~ >> |
    reb''( |
    mi''\f |
    fa''4)\p
  }
>> r4 r2 |
<<
  \tag #'oboe1 { do''4.(\fp lab'8 fa'4) r | }
  \tag #'oboe2 { lab'2\fp r | }
>>
R1 |
<<
  \tag #'oboe1 { reb''4.(\fp sib'8 sol'4) r | }
  \tag #'oboe2 { sib'2\fp r | }
>>
R1 |
r2 <<
  \tag #'oboe1 {
    fa''2\p~ |
    fa''( mib''4 reb'' |
    do'' lab' sib' sol') |
    fa'4 r r r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r |
    R1*2 |
    la'1\f |
    sib'2(\p do'') |
    sib'( mib'') |
    reb''4
  }
  \tag #'oboe2 {
    mib''2\p( |
    reb'')( do''4 sib' |
    lab' fa' sol' mi') |
    fa'4 r r2 |
    r r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' fa') |
    mi'2 r4 r16*2/3 fa'( sol' lab' sib' do'') |
    reb''4.(\fp do''8 sib' lab' sol' do'') |
    fa'4 r r2 |
    R1 |
    fa'1\f |
    solb'2(\p la') |
    reb''( do'') |
    sib'4
  }
>> r4 r2 |
<>\f <<
  \tag #'oboe1 { fa''1~ | fa''4 }
  \tag #'oboe2 { si'1~ | si'4 }
>> r4 r2 |
R1 |
<<
  \tag #'oboe1 {
    lab''1 |
    sol''4 do'' do'' do'' |
    do''
  }
  \tag #'oboe2 {
    fa''1 |
    mi''4 do'' do'' do'' |
    do''
  }
>> r4 r2 |
<<
  \tag #'oboe1 {
    R1*6 |
    reb''1(\p | do'' | sib' | lab' |
    sol') | fa'4
  }
  \tag #'oboe2 {
    R1*5 | r2 do''\p~ |
    do''( sib')~ | sib'( lab')~ | lab'( sol')~ | sol'( fa')~ |
    fa'( mi') | fa'4
  }
>> r4 r2 |
R1*5 |
R1^\fermataMarkup |
<<
  \tag #'oboe1 {
    R1*5 |
    r8 fa''(\p mi'' fa'') r sol''( fad'' sol'') |
    r lab''( sol'' lab'') r sib''( la'' sib'') |
    do'''2 lab''! |
    sol''1 |
    fa''4
  }
  \tag #'oboe2 {
    R1*7 |
    r2 fa''\p~ |
    fa''( mi'') |
    fa''4
  }
>> r4 r2 |
R1 |
r4 <<
  \tag #'oboe1 { lab'4 lab' lab' | lab'2 }
  \tag #'oboe2 { fa'4 fa' fa' | fa'2 }
>> r2 |
