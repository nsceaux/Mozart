\clef "treble"
<>\p <<
  \tag #'violino1 {
    \tuplet 3/2 { fa'8 fa' fa' } \tuplet 3/2 { fa'8 fa' fa' } fa'8*2/3 fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    <>\mf fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    <>\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    <>\mf sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    <>\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    lab' lab' lab' lab' lab' lab' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' mib' mib' mib' reb' reb' reb' |
    do' do' do' do' do' do' do' do' do' sib sib sib |
    lab fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    <>\fp mi' mi' mi' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    <>\fp sol' sol' sol' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    <>\fp sol' sol' sol' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    <>\fp sol' sol' sol' mi' mi' mi' mi' mi' mi' sol' sol' sol' |
    fa'4.( sol'8) lab'4.( sib'8) |
    do''4 r r2 |
    R1 |
    mib'!8*2/3 mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' |
    <>\fp si si si si si si si si si si si si |
    lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' |
    <>\fp do' do' do' do' do' do' do' do' do' do' do' do' |
    <>\p do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    <>\cresc do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' |
    <>\fp <mib'' do'''> q q q q q q q q q q q |
    <reb'' si''>4 r r2 |
    <>\p lab'8*2/3 lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fad' fad' fad' sol'\f sol' sol' sol' sol' sol' |
    sol'\p sol' sol' sol' sol' sol' lab'\f lab' lab' lab' lab' lab' |
    la'\p la' la' la' la' la' la' la' la' la' la' la' |
    sib'\cresc sib' sib' sib' sib' sib' sib' sib' sib' sib' sib' sib' |
    la'\fp la' la' la' la' la' la' la' la' la' la' la' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' fad' fad' fad' fad' fad' fad' |
    sol'\cresc sol' sol' sol' sol' sol' sib' sib' sib' la'\f la' la' |
    sol'\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' fad' fad' fad' fad' fad' fad' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol' sib' sib' sib' sib' sib' sib'\cresc sib' sib' sib' sib' sib' |
    sib'( reb'') reb'' reb'' reb'' reb'' reb''( mi'') mi'' mi'' mi'' mi'' |
    mi''(\f sol'') sol'' sol'' sol'' sol'' sol''( sib'') sib'' sib''( mi'') mi'' |
    fa''(\p fa') fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    fa'\mf fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    sol'\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol'\mf sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    sol'\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    lab' lab' lab' lab' lab' lab' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' mib' mib' mib' reb' reb' reb' |
    do' do' do' do' do' do' do' do' do' sib sib sib |
    lab( fa') fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    mi'\fp mi' mi' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    sol'\fp sol' sol' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    sol'\fp sol' sol' mi' mi' mi' mi' mi' mi' fa' fa' fa' |
    sol'\fp sol' sol' mi' mi' mi' mi' mi' mi' sol' sol' sol' |
    fa'4.( sol'8) lab'4.( sib'8) |
    do''4 r r2 |
    fa''8*2/3\f fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa''\p fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' do'' do'' do'' |
    si' si' si' si' si' si' fa'' fa'' fa'' fa'' fa'' fa'' |
    lab''4 r r2 |
    lab'8*2/3\p lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' |
    <fa'' si''>\f q q q q q q q q q q q |
    <mi'' do'''>( sol'') sol'' sol''( mi'') mi'' mi''( do'') do'' do''( do') do' |
    do' do''\p do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    reb''! reb'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' reb'' |
    do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    sib' sib' sib' sib' sib' sib' sib' sib' sib' sib' sib' sib' |
    lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    fa' do' do' do' do' do' do' do' do' do' do' do' |
    reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
    do' do' do' do' do' do' do' do' do' do' do' do' |
    sib sib sib sib sib sib sib sib sib sib sib sib |
    lab lab lab lab lab lab lab lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    fa'4 fa''8(\f solb'') solb''2\p~ |
    solb''1 |
    r4 fa'' r reb'' |
    reb''8( do'') sib'-. lab'-. lab'( sol') fa'-. mi'-. |
    fa'4 fa''8(\f solb'') solb''2\p~ |
    solb''4 la'8(\f sib') sib'2\p~ |
    sib' si'\fermata |
    reb''8( do'') sib'!-. lab'!-. lab'( sol') fa'-. mi'-. |
    fa'-. fa'( mi' fa') r sol'( fad' sol') |
    r lab'( sol' lab') r sib'( la' sib') |
    r4 do'' r lab'! |
    r sol' r sol' |
    r8 fa'( mi' fa') r sol'( fad' sol') |
    r lab'( sol' lab') r sib'( la' sib') |
    r4 do'' r lab'! |
    r sol' r sol' |
    fa' reb''( do'' sib' |
    lab' sol' fa' mi') |
    fa' fa' fa' fa' |
    fa'2 r |
  }
  \tag #'violino2 {
    \tuplet 3/2 { lab8 lab lab } \tuplet 3/2 { lab lab lab } lab8*2/3 lab lab lab lab lab |
    lab lab lab lab lab lab lab lab lab lab lab lab |
    <>\mf lab lab lab lab lab lab lab lab lab lab lab lab |
    <>\p reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
    <>\mf reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
    <>\p do' do' do' do' do' do' do' do' do' do' do' do' |
    do' do' do' do' do' do' mib' mib' mib' mib' mib' mib' |
    reb' reb' reb' reb' reb' reb' do' do' do' sib sib sib |
    lab lab lab lab lab lab sol sol sol sol sol sol |
    lab do' do' do' do' do' do' do' do' do' do' do' |
    <>\fp sib sib sib sib sib sib sib sib sib lab lab lab |
    <>\fp sib sib sib sib sib sib sib sib sib lab lab lab |
    <>\fp sib sib sib sib sib sib sib sib sib lab lab lab |
    <>\fp sib sib sib sib sib sib sib sib sib sib sib sib |
    lab fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    mi'4 r r2 |
    <>\fp sol8*2/3 sol sol sol sol sol sol sol sol sol sol sol |
    sol sol sol sol sol sol sol sol sol sol sol sol |
    <>\fp lab lab lab lab lab lab lab lab lab lab lab lab |
    si si si si si si si si si si si si |
    <>\fp sol sol sol sol sol sol sol sol sol sol sol sol |
    sol'\p sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    mib''\cresc mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    <do'' la''>\fp q q q q q q q q q q q |
    <sol'' si'>4 r sol'8*2/3\p sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' mib' mib' mib' mib' mib' mib' |
    mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' |
    re' re' re' re' re' re' re' re' re' fa'\f fa' fa' |
    mib'\p mib' mib' mib' mib' mib' mib' mib' mib' sol'\f sol' sol' |
    fad'\p fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' |
    sol'\cresc sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' sol' |
    fad'\fp fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' |
    sol' sol' sol' fa'! fa' fa' mib' mib' mib' do' do' do' |
    sib sib sib sib sib sib la la la la la la |
    dod'\cresc dod' dod' dod' dod' dod' re' re' re' re'\f re' re' |
    re'\p re' re' re' re' re' do'! do' do' do' do' do' |
    sib sib sib sib sib sib la la la la la la |
    sib sib sib sib sib sib sib sib sib sib sib sib |
    <<
      { mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' mib' |
        mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' |
        mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' | } \\
      { reb'! reb' reb' reb' reb' reb' reb'\cresc reb' reb' reb' reb' reb' |
        reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
        do'\f do' do' do' do' do' do' do' do' do' do' do' | }
    >>
    fa'(\p lab) lab lab lab lab lab lab lab lab lab lab |
    lab\mf lab lab lab lab lab lab lab lab lab lab lab |
    reb'\p reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
    reb'\mf reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' reb' |
    do'\p do' do' do' do' do' do' do' do' do' do' do' |
    do' do' do' do' do' do' mib' mib' mib' mib' mib' mib' |
    reb' reb' reb' reb' reb' reb' do' do' do' sib sib sib |
    lab lab lab lab lab lab sol sol sol sol sol sol |
    lab( do') do' do' do' do' do' do' do' do' do' do' |
    sib\fp sib sib sib sib sib sib sib sib lab lab lab |
    sib\fp sib sib sib sib sib sib sib sib lab lab lab |
    sib\fp sib sib sib sib sib sib sib sib lab lab lab |
    sib\fp sib sib sib sib sib sib sib sib sib sib sib |
    lab fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    mi'4 r r2 |
    fa'8*2/3\f fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    fa'\p fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' reb' reb' reb' reb' reb' reb' reb' reb' do' do' do' |
    si\f si si si si si fa' fa' fa' fa' fa' fa' |
    lab'4 r r2 |
    fa'8*2/3\p fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' fa' |
    lab''\f lab'' lab'' lab'' lab'' lab'' lab'' lab'' lab'' lab'' lab'' lab'' |
    sol''( mi'') mi'' mi''( sol') sol' sol'( mi') mi' mi'( do') do' |
    do'4 r do''8*2/3\p do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' sib' sib' sib' sib' sib' sib' |
    sib' sib' sib' sib' sib' sib' lab' lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' mi' mi' mi' mi' mi' mi' |
    fa'4 r do'8*2/3 do' do' do' do' do' |
    do' do' do' do' do' do' sib sib sib sib sib sib |
    sib sib sib sib sib sib lab lab lab lab lab lab |
    lab lab lab lab lab lab sol sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' sol' sol' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' mi' mi' mi' mi' mi' mi' |
    fa'4 lab'8(\f la') la'2\p~ |
    la'1 |
    r4 sib' r fa' |
    fa'8( lab'!) sol'-. fa'-. do'( sib) lab-. sol-. |
    lab4 lab'8(\f la') la'2\p~ |
    la'4 solb'8(\f fa') fa'2\p~ |
    fa'2 lab'!\fermata |
    lab'4 sol'8-. fa'-. do'( sib) lab-. sol-. |
    lab-. do'-. lab-. do'-. sol-. do'-. sib-. do'-. |
    lab-. fa'-. do'-. fa'-. sib-. mi'-. do'-. mi'-. |
    fa'4 lab' r fa' |
    r fa' r mi' |
    fa'8-. do'-. lab-. do'-. sol-. do'-. sib-. do'-. |
    lab-. fa'-. do'-. fa'-. sib-. mi'-. do'-. mi'-. |
    fa'4 lab' r fa' |
    r fa' r mi' |
    fa' lab'( sol' fa' |
    mib'! reb' do' sib) |
    lab lab lab lab |
    lab2 r |
  }
>>
