\clef "bass" fa4\p fa fa fa |
fa fa fa fa |
fa\mf fa fa fa |
fa\p fa fa fa |
fa\mf fa fa fa |
mi\p mi mi mi |
fa fa la, la, |
sib, sib, sib, sib, |
do do do do |
fa, fa lab fa |
do4\f do'2\fp do'4 |
do4\p do'2\fp do'4 |
do4\p do'2\fp do'4 |
do4\p do'2\fp do'4 |
reb4 reb reb reb |
do r r2 |
R1 |
do4 do do do |
re\fp re re re |
re re re re |
mib\fp mib mib mib |
R1*2 |
fad4\fp fad fad fad |
sol r si,2\p( |
do re |
mib do |
lab, la, |
sib, si,\f) |
do\p do\f~ |
do4\p do do do |
dod\cresc dod dod dod |
do!\fp do do do |
sib, si, do mib |
re1 |
mib4\cresc mib( re do\f |
sib,)\p si,( do mib) |
re1 |
sol4 sol sol sol |
sol sol sol\cresc sol |
sol sol sol sol |
sol\f sol sol sol |
fa\p fa fa fa |
fa\mf fa fa fa |
fa\p fa fa fa |
fa\mf fa fa fa |
mi\p mi mi mi |
fa fa la, la, |
sib, sib, sib, sib, |
do do do do |
fa, fa lab fa |
do\f do'2\fp do'4 |
do\f do'2\fp do'4 |
do\f do'2\fp do'4 |
do\f do'2\fp do'4 |
reb reb reb reb |
do r r2 |
R1 |
r2 r4 r8 fa |
solb4. fa8 la4. fa8 |
sib4 r sib, do |
re4\f re8. re16 re4 re |
reb! r r2 |
R1 |
si,4 si,8. si,16 si,4 si, |
do do do do |
do r mi2\p( |
fa sol |
lab fa |
re mi |
fa reb! |
sib, do) |
fa4 r mi2( |
fa sol |
mi fa |
re mi |
fa reb! sib, do) |
fa4 fa8(\f mib!) mib2\p~ |
mib1 |
reb4 r sib, r |
do do do do |
fa, fa8(\f mib) mib2\p~ |
mib4 mib8(\f re) re2\p~ |
re reb!\fermata |
do4 do do do |
fa r mi r |
fa r sol r |
lab r reb' r |
sib r do' r |
fa r mi r |
fa r sol r |
lab r reb r |
sib, r do r |
fa,1~ |
fa,~ |
fa,4 fa, fa, fa, |
fa,2 r |
