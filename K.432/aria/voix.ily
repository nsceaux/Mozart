\clef "bass" R1 |
do'4 do'8 do' do'4. lab8 |
fa4 fa r2 |
reb'4 reb'8 reb' reb'4. sib8 |
sol4 sol r2 |
sol4 sol8 sol do'4. sol8 |
lab4 lab fa2~ |
fa mib4. reb8 |
do2. sib,4 |
lab, fa, r r8 fa |
reb'4. do'8 sib([ lab]) sol fa |
mi4 mi r r8 fa |
reb'4. do'8 sib([ lab]) sol fa |
mi4 mi r r8 mi |
fa4. sol8 lab4. sib8 |
do'4 r r2 |
r r4 r8 sol |
mib!4. sol8 mib4. do8 |
si,4.( lab8) lab4 r |
lab fa8 fa re4 si, |
do4.( sol8) sol4 r |
sol2 sol4. sol8 |
do'2 do'4. do'8 |
mib'2. do'4 |
si4 sol r r8 sol |
lab4. do8 sib,!4. lab8 |
sol4 sol r2 |
fa4 fa8 fa fa4. do8 |
re4 r r r8 sol |
\appoggiatura fa16 mib4 r r r8 lab |
\appoggiatura sol16 fad4 r r r8 la |
sib4. sib8 la4 sol |
mib'4.( fad8) fad4 r |
r4 sol mib do |
re2 <<
  { \voiceOne re2 | mib4 \oneVoice }
  \new Voice \with { autoBeaming = ##f } { \voiceTwo re,2 | mib,4 }
>> r4 r2 |
r4 sol mib do |
re2 <<
  { \voiceOne re2 | \oneVoice }
  \new Voice \with { autoBeaming = ##f } { \voiceTwo re,2 | }
>>
sol,4 r r2 |
R1*3 |
do'4 do'8 do' do'4. lab8 |
fa4 fa r2 |
reb'4 reb'8 reb' reb'4. sib8 |
sol4 sol r2 |
sol4 sol8 sol do'4. sol8 |
lab4 lab fa2~ |
fa mib4. reb8 |
do2. sib,4 |
lab, fa, r r8 fa |
reb'4. do'8 sib([ lab]) sol fa |
mi4 mi r r8 fa |
reb'4. do'8 sib([ lab]) sol fa |
mi4 mi r r8 mi |
fa4. sol8 lab4. sib8 |
do'4 r r2 |
r r4 r8 fa |
solb4. fa8 la4. fa8 |
sib2 do' |
reb'4 reb'8 reb' reb'4. do'8 |
si4 si r2 |
lab2 fa4 reb |
si,1( |
fa'2.) fa'4 |
mi' do' r2 |
r r4 r8 do' |
reb'4. reb'8 reb'4. reb'8 |
do'4 do' r2 |
sib4 sib8 sib do'4. sol8 |
lab4 r r2 |
sol4 sol8 sol do'4. mi8 |
fa4 r r r8 do |
reb4. reb8 reb4. reb8 |
do4 do r2 |
sib,4 sib,8 sib, do4. sol,8 |
lab,4 r r2 |
sol,4 sol,8 sol, do4. mi8 |
fa4 fa8([ solb]) solb2~ |
solb4 mib! do la, |
sib,4.( reb'8) reb'4 r |
\appoggiatura reb'8 do'4 sib8 lab \appoggiatura lab sol4 fa8([ mi]) |
fa4 fa8([ solb]) solb2~ |
solb4 la8([ sib]) sib2~ |
sib( \once\override Script.avoid-slur = #'outside si)\fermata |
\appoggiatura reb'8 do'4 sib!8 lab! \appoggiatura lab sol4 fa8([ mi]) |
fa4 r r r8 sol |
lab4 r8 lab sib4 r8 sib |
do'2 lab4 r8 lab |
sol4 r8 sol do'4 r8 mi |
fa4 r r r8 sol |
lab2 mi |
fa lab, |
sol, do |
fa,4 r r2 |
R1*3 |
