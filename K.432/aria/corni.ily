\clef "treble" \transposition fa
R1*2 |
<>\fp <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1 |
<>\fp <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*5 |
<>\fp <<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol'~ | sol' | }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol~ | sol }
>>
R1*2 |
<>\fp <<
  \tag #'(corno1 corni) { sol'1 | }
  \tag #'(corno2 corni) { sol | }
>>
R1 |
<>\fp <<
  \tag #'(corno1 corni) { do''1 }
  \tag #'(corno2 corni) { do' }
>>
R1 |
<<
  \tag #'(corno1 corni) { re''1 | sol'~ | sol'~ | sol' | }
  \tag #'(corno2 corni) { sol1~ | sol sol~ | sol | }
  { s1\fp | s1\pp | s\cresc | s\fp | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 }
  { re'' }
>> r4 r2 |
R1*7 |
<>\fp <<
  \tag #'(corno1 corni) { sol'1 | }
  \tag #'(corno2 corni) { sol1 | }
>>
R1*5 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1~ |
    re''~ |
    re''~ |
    re''( |
    do''4) }
  { re''1~ |
    re''~ |
    re'' |
    sol( |
    do'4) }
  { s1\pp | s2 s\cresc | s1 | s1\f | s4\p }
>> r4 r2 |
<>\fp <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r |
R1 |
<>\fp <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r |
R1*5 |
<<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol'~ | sol' | }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol~ | sol | }
  { s1\fp | s\fp | s\fp | s\fp | }
>>
R1*2 |
<<
  \tag #'(corno1 corni) { mi'1 | do''~ | do''~ | do'' | do''~ | do''4 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'~ | do' | do'~ | do'4 }
  { s1\f | s\p | s1*2 | s1\f }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    do''1 |
    sol'4 sol' sol' sol' |
    sol'
  }
  \tag #'(corno2 corni) {
    do'1 |
    sol4 sol sol sol |
    sol
  }
>> r4 r2 |
R1*17 |
R1^\fermataMarkup |
R1*9 |
<>\p <<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''4 do'' do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'4 do' do' do' |
    do'2
  }
>> r2 |
