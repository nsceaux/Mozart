\clef "bass" <>^"pizz."\p sib8 r sib r |
la r la r |
sib r sol mib |
fa r fa, r |
sib, r sib, r |
mib r mib r |
fa r fa, r |
sib, sib, sib, r |
sib r sib r |
la r la r |
sib r sol mib |
fa r fa, r |
sib, r sib, r |
sib, r sib, r |
fa r fa r |
fa r fa r |
sib, r sib, r |
mib r mib r |
fa r fa, r |
sib, r sib r |
fa r fa r |
fa r fa r |
do r do r |
fa r fa r |
re r la, r |
re r re r |
fa r sol r |
do r sib! r |
la r fa r |
sol r mi r |
fa r re sib, |
do r sib r |
la r fa r |
sol r mi r |
fa r reb r |
do r do r |
lab, r lab, r |
sol, r sol, r |
mib,! r mib, r |
lab, r lab, r |
do r do r |
reb r reb r |
mib r mib r |
lab, r lab, r |
lab r lab r |
sol r sol r |
fad r fad r |
sol r sol r |
do r re mib |
re r mib r |
do r re r |
sol, r sol r |
re r re r |
mib r mib r |
mi r mi r |
fa r fa r |
fad r fad r |
sol r do' sib |
la fa sib sol |
mi r mi r |
fa mib! re do |
sib, r sib r |
la r la r |
sib r sol mib |
fa r fa, r |
sib, r sib, r |
sib, r sib, r |
fa r fa r |
fa r fa r |
sib, r sib, r |
mib r mib r |
fa r fa r |
sol r sol r |
re r re r |
mib r mib r |
fa r fa, r |
sib, r fa r |
sib r fa r |
sib, r r4 |
