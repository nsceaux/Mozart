\version "2.19.80"
\include "common.ily"

\opusTitle "K.482 – N°11 – Voi, che sapete"

\header {
  title = \markup\center-column {
    \line\italic { Le nozze di Figaro }
    \line { N°11 – Canzona: \italic { Voi, che sapete } }
  }
  opus = "K.492"
  date = "1786"
  copyrightYear = "2019"
}

\includeScore "K.492.11"
