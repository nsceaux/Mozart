\piecePartSpecs
#`((clarinetti #:tag-global clarinetti
               #:instrument , #{ \markup\center-column { Clarinetto in B } #})
   (fagotti #:tag-global all)
   (oboi #:tag-global all)
   (flauti #:tag-global all)
   (corni #:tag-global ()
          #:score-template "score-corni"
          #:instrument "Corni in Es")
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Duetto: TACET } #}))
