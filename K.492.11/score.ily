\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        instrumentName = "Flauto"
        shortInstrumentName = "Fl."
      } <<
        \keepWithTag #'all \global
        \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with {
        instrumentName = "Oboe"
        shortInstrumentName = "Ob."
      } <<
        \keepWithTag #'all \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Clarinetto in B }
        shortInstrumentName = "Cl."
      } <<
        \keepWithTag #'clarinetti \global
        \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with {
        instrumentName = "Fagotto"
        shortInstrumentName = "Fg."
      } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corni in Es }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Cherubino
      shortInstrumentName = \markup\character Che.
    } \withLyrics <<
      \keepWithTag #'all \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncello e Basso }
      shortInstrumentName = \markup\center-column { Vlc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global \includeNotes "basso"
      \origLayout {
        s2*6\break s2*7\pageBreak
        s2*7\break s2*7\pageBreak
        s2*7\break s2*7\pageBreak
        s2*7\break s2*6\pageBreak
        s2*6\break s2*6\pageBreak
        s2*6\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
