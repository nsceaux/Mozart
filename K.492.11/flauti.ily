\clef "treble" R2*6 |
<>_\markup\italic dolce
sib''4 la''16.( sib''32) do'''16.( re'''32) |
sib''8-. re'''-. sib''-. r |
R2*5 |
\omit TupletBracket
r16 \tuplet 3/2 { fa''32( sol'' fa'' } sib''16) la''-. sol''-. fa''-. mib''-. re''-. |
do''8 r r4 |
r16 \tuplet 3/2 { mib''32( fa'' mib'' } do'''16) sib''-. la''-. sol''-. fa''-. mib''-. |
re''8 r r4 |
R2 |
sib''4 la''16.( sib''32) do'''16.( re'''32) |
sib''8-. re'''-. sib''-. r |
R2*5 |
r8 la'( re'' fa'') |
la''( re''' do''' si'') |
do'''2~ |
do'''8 r r4 |
R2*2 |
r8 do'''4 do'''8~ |
do''' r r4 |
R2 |
lab''4( sol''8 fa'') |
do'''2~ |
do''' |
reb'''~ |
reb''' |
do'''8( lab'' mib'' do'') |
R2*2 |
lab''4 sol''16.( lab''32) sib''16.( do'''32) |
lab''8-. do'''-. lab''-. r |
R2*3 |
sol''2 |
mib'''4( re'''8 dod''') |
re'''4( sib'') |
la''!2 |
sol''4 sib''~ |
sib''2~ |
sib''8 si''(_\markup\dynamic mfp do'''4)~ |
do'''2~ |
do'''8 dod'''(_\markup\dynamic mfp re'''4)~ |
re'''2~ |
re'''4 mib''' |
mib'''( re''') |
reb'''4( do'''8 sib'') |
fa''2~ |
fa''8 r r4 |
R2*4 |
r16 \tuplet 3/2 { fa''32( sol'' fa'' } sib''16) la''-. sol''-. fa''-. mib''-. re''-. |
do''8 r r4 |
r16 \tuplet 3/2 { mib''32( fa'' mib'' } do'''16) sib''-. la''-. sol''-. fa''-. mib''-. |
re''8 r r4 |
R2*2 |
r8 re''( sol'' sib'') |
re'''2( |
do''') |
sib''4 la''16.( sib''32) do'''16.( re'''32) |
sib''8 r r4 |
\override Script.avoid-slur = #'outside
r do'''8.(\trill sib''32 do''') |
sib''8 r r4 |
