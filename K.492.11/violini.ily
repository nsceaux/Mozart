\clef "treble" <>\p ^"pizz." \ru#2 { r16 sib re' fa' } |
\ru#2 { r16 do' mib' fa' } |
r sib re' fa' <<
  \tag #'violino1 {
    r sib' sol' do'' |
    \ru#2 { r do' fa' la' } |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r mib' sol' do'' } |
    r re' fa' sib' r do' mib' la' |
    sib' fa' sib' re'' sib'8 r |
    \ru#2 { r16 re' fa' sib' } |
    \ru#2 { r mib' fa' do'' } |
    r re' fa' sib' r sib' sol' mib'' |
    \ru#2 { r fa' la' do'' } |
    \ru#2 { r sib' sib'' sib' } |
    r sib' sib'' sib'' sib'' sib'' sib'' sib'' |
    r fa'' fa'' fa'' r fa'' fa'' fa'' |
    r fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r mib' sol' do'' } |
    r re' fa' sib' r do' mib' la' |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r do' fa' la' } |
    r re' fa' sib' r do' fa' la' |
    \ru#2 { r sib mi' sol' } |
    \ru#2 { r do' fa' la' } |
    r re' fa' la' r dod' mi' la' |
    \ru#2 { r re' fa' la' } |
    r re' fa' la' mi' sol' re' fa' |
    \ru#2 { r do' mi' sol' } |
    \ru#2 { r fa' la' do'' } |
    r mi' sol' sib' r do' sol' sib' |
    r do' fa' la' r fa' re' sib' |
    r mi' sol' sib' r do' mi' sol' |
    \ru#2 { r fa' la' do'' } |
    r mi' sol' sib' r do' sol' sib' |
    r do' fa' lab' r fa' lab' si' |
    \ru#2 { r mi' sol' do'' } |
    r mib'! lab' do'' r mib' lab' do'' |
    \ru#2 { r mib' sib' reb'' } |
    \ru#2 { r sib reb' sol' } |
    \ru#4 { r do' mib' lab' } |
    \ru#2 { r reb' fa' sib' } |
    r do' mib' lab' r sib reb' sol' |
    \ru#2 { r do' mib' lab' } |
    \ru#2 { r mib' lab' do'' } |
    \ru#2 { r sol' do'' mib'' } |
    r mib' la'! do'' r la' do'' mib'' |
    r sol' sol'' sol'' sol'' sol'' sol'' sol'' |
    r sol' do'' mib'' re'' sib'! sol' sib' |
    r fad' la' re'' r sib sol' sib' |
    r mib' sol' la' r do' fad' la' |
    \ru#2 { r sib re' sol' } |
    r re' fa'! lab' r re' fa' lab' |
    r sol' mib'' sol'' r do' mib' sol'
    \ru#2 { r mi' sol' sib' } |
    r la'! fa'' la'' r re' fa' la' |
    \ru#2 { r fad' la' do'' } |
    r sib' re'' sol'' r sol' do'' mib'' |
    r fa' la' do'' r fa' sib' re'' |
    r sol' sib' reb'' r mi' sol' sib' |
    r la' do'' la' re''! sib' mib''! la' |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r mib' fa' do'' } |
    r re' fa' sib' r sib' sol' mib'' |
    r fa' la' do'' fa'' fa'' fa'' fa'' |
    \ru#2 { r sib' sib'' sib' } |
    r sib' sib'' sib'' sib'' sib'' sib'' sib'' |
    \ru#2 { r fa'' fa'' fa'' } |
    r fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r mib' sol' do'' } |
    r re' fa' sib' r do' mib' la' |
    \ru#2 { r sib re' sol' } |
    \ru#2 { r re' fa' sib' } |
    \ru#2 { r mib' sol' do'' } |
    r re' fa' sib' r do' mib' la' |
    r re' fa' sib' r fa' do'' mib'' |
    r fa' sib' re'' r mib' la' do'' |
    sib'8 r r4 |
  }
  \tag #'violino2 {
    r16 re' mib' sol' |
    \ru#2 { r la do' fa' } |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r do' mib' sol' } |
    r sib re' fa' r la do' mib' |
    re' sib re' fa' re'8 r |
    \ru#2 { r16 sib re' fa' } |
    \ru#2 { r do' mib' fa' } |
    r sib re' fa' r re' mib' do'' |
    \ru#2 { r la fa' la' } |
    \ru#2 { r sib sib' sib } |
    r16 sib sib' sib' sib' sib' sib' sib' |
    \ru#2 { r fa' fa' fa' } |
    r fa' fa' fa' fa' fa' fa' fa' |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r do' mib' sol' } |
    r sib re' fa' r la do' mib' |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r la do' fa' } |
    r sib re' fa' r la do' fa' |
    \ru#2 { r sol sib mi' } |
    \ru#2 { r la do' fa' } |
    r la re' fa' r la dod' mi' |
    \ru#2 { r la re' fa' } |
    r la re' fa' do'! mi' si re' |
    \ru#2 { r sol do' mi' } |
    r do' fa' la' r la fa' la' |
    r sib mi' sol' r sol do' sol' |
    r la do' fa' r la sol sol' |
    r do' mi' sol' r sol do' mi' |
    r do' fa' la' r la fa' la' |
    r sib mi' sol' r sol do' sol' |
    r lab do' fa' r si fa' lab' |
    \ru#2 { r do' mi' sol' } |
    r do' mib'! lab' r do' mib' lab' |
    \ru#2 { r sib mib' sib' } |
    \ru#2 { r sol sib reb' } |
    \ru#4 { r lab do' mib' } |
    \ru#2 { r sib reb' fa' } |
    r lab do' mib' r sol sib reb' |
    \ru#2 { r lab do' mib' } |
    \ru#2 { r do' mib' lab' } |
    \ru#2 { r mib' sol' do'' } |
    r do' mib' la'! r fad' la' do'' |
    r sol sol' sol' sol' sol' sol' sol' |
    r mib' sol' do'' sib'! sol' sol sol' |
    r la fad' la' r sol sib sol' |
    r la mib' sol' r la do' fad' |
    \ru#2 { r sol sib re' } |
    r sib re' fa'! r sib re' fa' |
    r mib' sol' mib'' r sol do' mib' |
    \ru#2 { r do' mi' sol' } |
    r fa' la'! fa'' r la! re' fa' |
    \ru#2 { r re' fad' la' } |
    r sol' sib' re'' r mib' solb' do'' |
    r do' fa' la' r re' fa' sib' |
    r mi' sol' sib' r do' mi' sol' |
    r do' la' fa' sib' fa' la' mib'! |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r do' mib' fa' } |
    r sib re' fa' r re' mib' do'' |
    r la fa' la' fa' fa' fa' fa' |
    \ru#2 { r sib sib' sib } |
    r sib sib' sib' sib' sib' sib' sib' |
    \ru#2 { r fa' fa' fa' } |
    r fa' fa' fa' fa' fa' fa' fa' |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r do' mib' sol' } |
    r sib re' fa' r la do' mib' |
    \ru#2 { r sol sib re' } |
    \ru#2 { r sib re' fa' } |
    \ru#2 { r do' mib' sol' } |
    r sib re' fa' r la do' mib' |
    r sib re' fa' r la fa' do'' |
    r re' fa' sib' r do' fa' mib' |
    re'8 r r4 |
  }
>>
