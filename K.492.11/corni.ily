\clef "treble" \transposition mib
R2*3 |
r8 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' | sol'4 }
  { re''8 re'' re'' | sol'4 }
>> r4 |
R2*2 |
r8 <<
  \tag #'(corno1 corni) { sol'8 sol' sol' | sol' }
  \tag #'(corno2 corni) { sol sol sol | sol }
>> r8 r4 |
R2*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'2~ | sol'8 }
  { sol'2~ | sol'8 }
>> r8 r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re''8 }
  { re''2~ | re''8 }
>> r8 r4 |
R2*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 | sol'8 sol' sol' }
  { re''2 | sol'8 sol sol }
>> r8 |
R2*16 |
<<
  \tag #'(corno1 corni) { do''2~ | do''~ | do''~ | do'' }
  \tag #'(corno2 corni) { do'2~ | do'~ | do'~ | do' }
>>
R2*2 |
<<
  \tag #'(corno1 corni) { do''2~ | do''~ | do''~ | do''~ | do'' | mi'' | }
  \tag #'(corno2 corni) { do'2~ | do'~ | do'~ | do'~ | do' | mi' | }
>>
R2*10 |
\mergeDifferentlyDottedOn
\twoVoices #'(corno1 corno2 corni) <<
  { re''4. mi''8 | }
  { re''4 sol' | }
>>
<<
  \tag #'(corno1 corni) { sol'2 | }
  \tag #'(corno2 corni) { sol | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re''8 }
  { re''2 | sol'8 }
>> r8 r4 |
R2*3 |
<<
  \tag #'(corno1 corni) { sol'2~ | sol'8 }
  \tag #'(corno2 corni) { sol2~ | sol8 }
>> r8 r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re''8 }
  { re''2~ | re''8 }
>> r8 r4 |
R2*3 |
<<
  \tag #'(corno1 corni) { sol'2~ | sol'~ | sol' | }
  \tag #'(corno2 corni) { sol2~ | sol~ | sol | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re''~ | re''~ | re''8 }
  { re''2~ | re''~ | re'' | sol'8 }
>> r8 r4 |

