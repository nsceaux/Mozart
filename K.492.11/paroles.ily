Voi, che sa -- pe -- te
che co -- sa è a -- mor,
don -- ne ve -- de -- te,
s’io l’ho nel cor,
don -- ne ve -- de -- te,
s’io l’ho nel cor.

Quel -- lo ch’io pro -- vo, vi ri -- di -- rò,
è per me nuo -- vo, ca -- pir nol so.
Sen -- to un af -- fet -- to pien di de -- sir,
ch’o -- ra è di -- let -- to,
ch’o -- ra è mar -- tir.

Ge -- lo, e poi sen -- to l’al -- ma av -- vam -- par,
e in un mo -- men -- to tor -- no a ge -- lar.
Ri -- cer -- co un be -- ne fuo -- ri di me,
non so chi il tie -- ne, non so cos’ è.

So -- spi -- ro e ge -- mo sen -- za vo -- ler,
pal -- pi -- to e tre -- mo sen -- za sa -- per;
non tro -- vo pa -- ce not -- te, né dì,
ma pur mi pia -- ce lan -- guir co -- sì.

Voi, che sa -- pe -- te
che co -- sa è a -- mor,
don -- ne ve -- de -- te,
s’io l’ho nel cor,
don -- ne ve -- de -- te,
s’io l’ho nel cor,
don -- ne ve -- de -- te,
s’io l’ho nel cor.
