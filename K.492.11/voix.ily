\clef "soprano/treble" R2*8 |
sib'4 fa'8 fa' |
do''4 fa' |
re''4 sib'16.([ do''32]) re''16.([ mib''32]) |
do''4 r |
re''4 mib''8 mi'' |
fa''8.([ re''16]) sib'8 r |
do''4 reb''8 re'' |
mib''4 r |
fa''8([ re'']) fa'' re'' |
do''([ mib'']) do''([ mib'']) |
sib'4 la'16.([ sib'32]) do''16.([ re''32]) |
sib'4 r |
do''4 do''8 do'' |
re''16([ mi'' fa'' re'']) do''8 r |
sol'8.([ la'16]) sib'8 do'' |
\appoggiatura sib'8 la'4 r |
re''4 mi''8 mi'' |
fa''8.([ re''16]) la'8 r |
la'8.([ re''16]) do''8 si' |
do''4 r |
do''4 do''8 fa'' |
mi''([ do'']) sib'8 r |
la'4 fa'8 sib' |
\appoggiatura la'8 sol'4 r |
do''4 do''16([ re'']) mi''([ fa'']) |
fa''([ mi'' re'' do'']) sib'8 r |
lab'4 sol'8 fa' |
do''4 r |
do''4 do''8 do'' |
mib''4 reb'' |
reb''8([ sib']) sol' reb' |
do'4 r |
mib''8([ do'']) mib'' do'' |
sib'([ reb'']) sib'([ reb'']) |
lab'4 \appoggiatura do''8 sib'8. lab'16 |
lab'4 r |
do'' do''8 do'' |
do''16([ re''! mib'' re'']) do''4 |
mib''4 re''8 do'' |
sol'2 |
mib''4 re''8 dod'' |
re''4 sib' |
la' re''8 re'' |
sol'4 r16 sib' sib' sib' |
do''([ sib']) sib'8 r16 sib' sib' sib' |
sol'8 r do'' do''16 do'' |
re''([ do'']) do''8 r16 do'' do'' do'' |
la'4 r16 re'' re'' re'' |
mib''([ re'']) re''8 re'' re''16 re'' |
sib'8 sib' mib'' mib'' |
mib''8.([ fa''16]) re''4 |
reb'' do''8 sib' |
fa'2 |
sib'4 fa'8 fa' |
do''4 fa' |
re''! sib'16.([ do''32]) re''16.([ mib''32]) |
do''4 r |
re''4 mib''8 mi'' |
fa''8.([ re''16]) sib'8 r |
do''4 reb''8 re'' |
mib''!4 r |
fa''8([ re'']) fa'' re'' |
do''([ mib'']) do''([ mib'']) |
sib'4 fa'8. fa'16 |
re'4 r |
fa''8([ re'']) fa'' re'' |
do''([ mib'']) do''([ mib'']) |
sib'4 la'16.([ sib'32]) do''16.([ re''32]) |
sib'4 r |
R2*2 |
