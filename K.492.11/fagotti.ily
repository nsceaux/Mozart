\clef "bass" <>_\markup\italic dolce
re'2( |
mib'4. do'8) |
sib4~ sib16.( la32) sib16.( do'32) |
la4 r |
re'8( sib16) r re'8( sib16) r |
sol8( do'16) r mib'8( sol'16) r |
re'4 do'16.( re'32) mib'16.( fa'32) |
re'8-. sib-. sib,-. r |
R2*4 |
sib4( do'8 dod' |
re'8) r r4 |
la( sib8 si |
do') r r4 |
fa'2( |
mib') |
re'4 do'16.( re'32) mib'16.( fa'32) |
re'8-. fa'-. re'-. r |
R2*3 |
r8 fa( la do') |
fa'4( dod') |
re'8.( la16) fa8 r |
r fa'( mi' re') |
do'2~ |
do'8 r r4 |
R2*2 |
r8 mi'4( sol'8) |
fa' r r4 |
R2*5 |
sol2( |
lab) |
mib'( |
reb') |
do'4 sib16.( do'32) reb'16.( mib'32) |
do'8-. mib'-. do'-. r |
R2*3 |
sol2 |
mib'4( re'8 dod') |
re'4( sol) |
mib'( re'8 do'!) |
sib8 r r4 |
fa2( |
mib8) r r4 |
sol2( |
fa8) r r4 |
la2( |
sol8) r r4 |
R2 |
reb'4( do'8 sib) |
fa2~ |
fa8 r r4 |
R2*2 |
r8 la-.([ la-. la-.]) |
sib4( do'8 dod' |
re') r r4 |
la4( sib8 si |
do') r r4 |
fa'2( |
mib') |
re'4( do') |
sib2 |
R2*2 |
re'4( mib') |
\override Script.avoid-slur = #'outside
re'8 r mib'8.(\trill re'32 mib') |
re'8 r fa fa, |
sib, r r4 |
