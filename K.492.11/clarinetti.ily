\clef "treble" \transposition sib
<>_\markup\italic dolce
do''4( sol'8) sol' |
re''4( sol'8) sol' |
mi''4 do''16.( re''32) mi''16.( fa''32) |
re''4 r |
R2*2 |
sol''4.( si'8) |
do''8-. sol'-. mi'-. r |
R2*4 |
mi''4( fa''8 fad'' |
sol''8) r r4 |
re''4( mib''8 mi'' |
fa''8) r r4 |
mi''2( |
re'') |
sol''4.( si'8) |
do''-. mi''-. do''-. r |
R2*5 |
r8 sol'( si' mi'') |
sol'' si'( la' sol') |
fad'4.( la'8) |
sol' r r4 |
R2*2 |
r8 re''4 re''8~ |
re'' r r4 |
R2*4 |
do''2~ |
do'' |
sib'8( re'' sib' fa') |
re''2( |
do'') |
sib'4 la'16.( sib'32) do''16.( re''32) |
sib'8-. re''-. sib'-. r |
R2*3 |
dod''8( mi''! re'' dod'') |
R2*4 |
sib'2( |
la'8) r r4 |
do''2( |
si'!8) r r4 |
re''2( |
do''8) r r4 |
re''4( do'')~ |
do''( re''8 mib'') |
re''4( mi''!8 si') |
do'' r r4 |
R2*2 |
r8 re''4( red''8) |
mi''4( fa''8 fad'' |
sol'') r r4 |
re''4( mib''8 mi'' |
fa'') r r4 |
mi''2( |
re'') |
sol'4( fa') |
mi'2 |
do'' |
la' |
sol'~ |
\override Script.avoid-slur = #'outside
sol'8 r re''8.(\trill do''32 re'') |
do''8 r fa''8.(\trill mi''32 fa'') |
mi''8 r r4 |
