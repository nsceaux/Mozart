\clef "treble" R2*4 |
<>_\markup\italic dolce
fa''8( re''16) r fa''8( re''16) r |
do''8( mib''16) r do''8( mib''16) r |
sib'4 la'16.( sib'32) do''16.( re''32) |
sib'8-. re''-. sib'-. r |
R2*5 |
r16 re''8( fa''16) mib''-. re''-. do''-. sib'-. |
la'8 r r4 |
\omit TupletBracket
r16 \tuplet 3/2 { do''32( re'' do'' } mib''16) re''-. do''-. sib'-. la'-. do''-. |
sib'8 r r4 |
R2*6 |
r8 la'( do'' fa'') |
la''2~ |
la'' |
R2 |
r8 sol''( sib''! mi'') |
fa'' r r4 |
R2*2 |
r8 sol''( sib'' mi'') |
fa'' r r4 |
R2*2 |
mi''2 |
mib''!~ |
mib''~ |
mib''~ |
mib''4( do''8 lab') |
R2*2 |
mib''4.( sol'8) |
lab'-. mib''-. do''-. r |
R2*3 |
sol''8( fa'' mib'' re'') |
R2 |
fad''4( sol'')~ |
sol''( fad'') |
sol''8 r sib'4~ |
sib'2~ |
sib'8 si'(_\markup\dynamic mfp do''4)~ |
do''2~ |
do''8 dod''(_\markup\dynamic mfp re''4)~ |
re''2~ |
re''8 sol''4( solb''8 |
fa'' la'' sib'') r |
sol'2 |
la'4( sib'8 mib'') |
re'' r r4 |
R2*4 |
r16 re''8( fa''16) mib''-. re''-. do''-. sib'-. |
la'8 r r4 |
r16 \tuplet 3/2 { do''32( re'' do'' } mib''16) re''-. do''-. sib'-. la'-. do''-. |
sib'8 r r4 |
R2 |
sib'4( la') |
sib'4( re''8 sol'') |
fa''2( |
mib'') |
re''4( do''8 la') |
\override Script.avoid-slur = #'outside
sib'8 r la''8.(\trill sol''32 la'') |
sib''8 r la''8.(\trill sol''32 la'') |
sib''8 r r4 |
