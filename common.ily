\header {
  composer = "W.A. Mozart"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'use-ancient-clef (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((eqv? #t (ly:get-option 'urtext)) 14)
        ((not (symbol? (ly:get-option 'part))) 16)
        ((eqv? 'piano (ly:get-option 'part)) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (cond ((eqv? (ly:get-option 'part) 'reduction) ly:optimal-breaking)
           ((symbol? (ly:get-option 'part)) ly:page-turn-breaking)
           (else ly:optimal-breaking)))
}

\layout {
  reference-incipit-width = #(* 1/2 mm)
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "."
\opusPartSpecs
#`((trombe "Trombe" () (#:notes "trombe"))
   (corni "Corni" () (#:notes "corni"))
   (timpani "Timpani" () (#:notes "timpani" #:clef "bass"))
   
   (flauti "Flauti" () (#:notes "flauti"))
   (oboi "Oboi" () (#:notes "oboi"))
   (clarinetti "Clarinetti" () (#:notes "clarinetti"))
   (fagotti "Fagotti" () (#:clef "bass" #:notes "fagotti"))

   (violino1 "Violino I" ()  (#:notes "violini" #:tag-notes violino1))
   (violino2 "Violino II" () (#:notes "violini" #:tag-notes violino2))
   (viola "Viola" () (#:notes "viola" #:clef "alto"))
   (basso "Basso" () (#:notes "basso" #:clef "bass"))

   (mandolino "Mandolino" () (#:notes "mandolino" #:clef "treble"))
   (piano "Pianoforte" () (#:score-template "score-piano"))
   (organo "Organo" () (#:score "score-organo")))

\opusTitle "Mozart"

\paper {
  bookTitleMarkup = \markup\fill-line\fontsize#1 {
    \column {
      \fromproperty #'header:composer
      \on-the-fly #(lambda (layout props arg)
                     (if (*part*)
                         (interpret-markup layout props (markup (*part-name*)))
                         empty-stencil)) \null
    }
    \override #'(baseline-skip . 8) \center-column {
      \fontsize#4 \bold\override #'(baseline-skip . 4) \fromproperty #'header:title
      \fromproperty #'header:subtitle
    }
    \right-column {
      \fromproperty #'header:opus
      \fromproperty #'header:date
    }
  }
}

\paper {
  % de la place en bas de page pour les annotations
  last-bottom-spacing.padding = #3
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      (* 8 mm))
  ragged-last = ##f
}

\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'inside
    \override DynamicTextSpanner.style = #'none
  }
  \context {
    \DrumVoice
    \override DynamicTextSpanner.style = #'none
  }
}

act =
#(define-music-function (parser location act-number act-title) (string? string?)
  (increase-rehearsal-major-number)
  (let ((title (string-concatenate (list act-number " – " act-title))))
    (add-toc-item parser 'tocActMarkup title)
    (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
    (*act-title* title)
    (add-odd-page-header-text parser (string-upper-case (*act-title*)) #f)
    (add-toplevel-markup parser #{\markup\act\center-column {
  $(string-upper-case act-number)
  $(string-upper-case act-title)
} #}))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))

corniInstr = \with {
  instrumentName = "Corni"
  shortInstrumentName = "Cor."
}
trombeInstr = \with {
  instrumentName = "Trombe"
  shortInstrumentName = "Tr."
}
tromboniInstr = \with {
  instrumentName = "Tromboni"
  shortInstrumentName = "Trb."
}
flautoInstr = \with {
  instrumentName = "Flauto"
  shortInstrumentName = "Fl."
}
flautiInstr = \with {
  instrumentName = "Flauti"
  shortInstrumentName = "Fl."
}
oboiInstr = \with {
  instrumentName = "Oboi"
  shortInstrumentName = "Ob."
}
clarinettiInstr = \with {
  instrumentName = "Clarinetti"
  shortInstrumentName = "Cl."
}
fagottiInstr = \with {
  instrumentName = "Fagotti"
  shortInstrumentName = "Fg."
}

violiniInstr = \with {
  instrumentName = \markup\concat { \combine V \concat { \hspace#0.65 V } iolini }
  shortInstrumentName = "Vn."
}
altoInstr = \with {
  instrumentName = "Alto"
  shortInstrumentName = "Vla."
}
violaInstr = \with {
  instrumentName = "Viola"
  shortInstrumentName = "Vla."
}
bassoInstr = \with {
  instrumentName = "Basso"
  shortInstrumentName = "B."
}
violoncelliInstr = \with {
  instrumentName = "Violoncelli"
  shortInstrumentName = "Vc."
}
cbInstr = \with {
  instrumentName = "Contrabasso"
  shortInstrumentName = "Cb."
}
bcbInstr = \with {
  instrumentName = \markup\center-column {
    Basso Contrabasso
  }
  shortInstrumentName = \markup\center-column { B. Cb. }
}
vccbInstr = \with {
  instrumentName = \markup\center-column {
    Violoncelli Contrabasso
  }
  shortInstrumentName = \markup\center-column { Vc. Cb. }
}
timpaniInstr = \with {
  instrumentName = "Timpani"
  shortInstrumentName = "Timp."
}

trill = #(make-articulation "trill")

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET }
                                             #}))

#(define-markup-command (tacet-text layout props text) (markup?)
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small $text }
                                             #}))

%% Cues
cueTreble =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuringWithClef $cue-name #CENTER "treble" $music #})

cue =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuring $cue-name #CENTER $music #})

cueTransp =
#(define-music-function (parser location cue-name pitch music)
     (string? ly:pitch? ly:music?)
   #{ \transposedCueDuring $cue-name #CENTER $pitch $music #})


quoteViolinoI =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino1 { \includeNotes "violini" }
#}))

quoteViolinoII =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino2 { \includeNotes "violini" }
#}))

quoteBasso =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'basso { \includeNotes "basso" }
#}))
