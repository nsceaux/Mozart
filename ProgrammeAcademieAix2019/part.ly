\version "2.19.80"
\include "../common.ily"

\header {
  composer = ##f
    maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.ensemblepygmalion.com/" \line {
      Pygmalion – Raphaël Pichon
    }
  }

}
\paper { bookTitleMarkup = ##f }

\bookpart {  
  \paper {
    bookTitleMarkup = \nenuvarBookTitleMarkup
    #(define page-breaking ly:minimal-breaking)
  }
  \header {
    title = "Mozart"
    editions = \markup\center-column {
      Académie 2019
      Pygmalion
    }
    copyrightYear = "2018"
  }
  \markup\null\pageBreak
  
  \markuplist
  \abs-fontsize-lines #9
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 1)
  \table-of-contents
}
#(define-public rehearsal-number #f)
#(let ((major-number 0))
  (set! rehearsal-number
        (lambda ()
          (set! major-number (1+ major-number))
          (format #f "~a" major-number))))


\pieceToc\markup {
  \italic { Les petits riens } – Ouverture [K.Anh.10]
}
\markup\null\pageBreak
\pieceToc\markup {
  \italic { Così fan tutte } –
  N°13 – Sestetto: \italic { Alla bella Despinetta } [K.588]
}
\includeScore "K.588.13"

\pieceToc\markup {
  \italic { La finta giardiniera } –
   N°14 – Aria: \italic { Con un vezzo all’Italiana } [K.196]
 }
\includeScore "K.196.14"

\pieceToc\markup {
  \italic { La clemenza di Tito } –
  N°7 – Duetto: \italic { Ah! perdona al primo affetto } [K.621]
}
\includeScore "K.621.7"

\pieceToc\markup { Arie \italic Alma grande e nobil core [K.578] }
\includeScore "K.578"

\pieceToc\markup {
  \italic { Le nozze di Figaro } –
  N°11 – Canzona: \italic { Voi, che sapete } [K.492]
}
\includeScore "K.492.11"

\pieceToc\markup {
  \italic { Così fan tutte } –
  N°21 – Duetto: \italic { Secondate, aurette amiche } [K.588]
}
\includeScore "K.588.21"

\pieceToc\markup {
  \italic { Die Zauberflöte } –
  N°3 – Aria: \italic { Dies Bildnis ist bezaubernd schön } [K.620]
}
\includeScore "K.620.3"

\pieceToc\markup {
  \italic { Le nozze di Figaro } –
  N°18 – Sestetto: \italic { Riconosci in questo amplesso } [K.492]
}
\includeScore "K.492.18"

\pieceToc\markup {
  \italic { La clemenza di Tito } –
  N°9 – Aria: \italic { Parto, ma tu ben mio } [K.621]
}
\includeScore "K.621.9"

\pieceToc\markup {
  \italic { Don Giovanni } –
  N°21a – Duetto: \italic { Per queste tue manine } [K.527]
}
\includeScore "K.527.21A"

\pieceToc\markup {
  Recitativ und Arie: \italic Cosi dunque tradisei [K.432]
}
\includeScore "K.432/recit"
\includeScore "K.432/aria"

\pieceToc\markup {
  Arie: \italic Voi avete un cor fedele [K.217]
}
\includeScore "K.217"

\pieceToc\markup {
  \italic { Die Entführung aus dem Serail } –
  N°9 – Duett: \italic { Ich gehe, doch rathe ich dir } [K.384]
}
\includeScore "K.384.9"

\pieceToc\markup {
  Rondo – \italic Per pietà, non ricercate [K.420]
}
\includeScore "K.420"

\pieceToc\markup {
  Recitativ und Arie: \italic Ah non lasciarmi, no [K.486a]
}
\includeScore "K.486a/recit"
\includeScore "K.486a/aria"

\pieceToc\markup {
  Aria: \italic { Io ti lascio o cara addio } [K.Anh 245]
}
\includeScore "K.Anh.245"

\pieceToc\markup {
   Duetto: \italic { Spiegarti non poss’io } [K.489]
}
\includeScore "K.489"

\pieceToc\markup {
  \italic { Le nozze di Figaro } –
   N°28 – Tutti: \italic { Gente, gente } [K.492]
}
\includeScore "K.492.28"
