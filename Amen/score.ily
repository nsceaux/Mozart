\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'vsoprano \includeNotes "voix"
    >> \keepWithTag #'vsoprano \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'valto \includeNotes "voix"
    >> \keepWithTag #'valto \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtenor \includeNotes "voix"
    >> \keepWithTag #'vtenor \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
      \origLayout { s2.*8\break }
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
  >>
  \layout {
    indent = \smallindent
    short-indent = 0
  }
  \midi { }
}