<<
  \tag #'vsoprano {
    \clef "soprano/treble" s2. |
    r4 re''\melisma mi'' |
    fa'' mi''8[ re'']\melismaEnd do''4 |
    r si'\melisma dod'' |
    re'' do''!8[ sib']\melismaEnd la'4 |
    r sol'8[\melisma fa' sol' la'] |
    sib'[ mi' re' mi' fa' sol']\melismaEnd |
    la'4 r r |
    r4 sol' do'' |
    do''( si') si' |
    s2.*6
  }
  \tag #'valto {
    \clef "alto/treble" la'2.\melisma |
    sib' |
    la' |
    sol' |
    fa' |
    mi'\melismaEnd |
    re'4 r r |
    |
    r la'\melisma si' |
    do''4 si'8[ la']\melismaEnd sol'4 |
    r fad'\melisma sold' |
    la'4 sol'8[ fa']\melismaEnd mi'4 |
    r re'8[\melisma fa' mi' re'] |
    do'[ si la si dod' la]\melismaEnd |
    re'4 r r |
    r do' fa' |
    fa'( mi') mi' |
    
  }
  \tag #'vtenor {
    \clef "tenor/G_8" s2.*6 |
    re'2.\melisma |
    fa' |
    mi' |
    re' |
    do' |
    si\melismaEnd |
    la4 r r |
    r re'\melisma mi' |
    fa' mib'8[ re']\melismaEnd do'4 |
    r si dod' |
  }
  \tag #'vbasse {
    \clef "bass/bass" s2.*12 |
    la2.\melisma |
    sib |
    la\melismaEnd |
    sol |
    
  }
>>
R2.^\fermataMarkup