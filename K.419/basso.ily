\clef "bass" r4 r8. \tuplet 3/2 { sol,32(\f la, si,) } do2 |
r4 r8. \tuplet 3/2 { si,32( do re) } mi2 |
sol\p mi4. do8 |
la2. do4 |
si,-. sol-. r2 |
sol8 sol sol sol sol sol sol sol |
sol8 sol sol sol sol sol sol sol |
sol8 sol sol sol sol sol sol sol |
sol8 sol sol sol sol sol sol sol |
sol8 sol sol sol sol sol sol sol |
la4 r16 do'\f si la sol si la sol fa la sol fa |
mi8\p mi mi mi mi mi mi mi |
fa fa fa fa fa fa fa fa |
sol sol sol sol sol sol sol sol |
do4 r r2 |
r4 do'8\f do' fa fa sol sol |
do4 do8. do16 do4 r |
do8\fp do'4 do'8 do8\fp do'4 do'8 |
si,8\fp si si si si si si si |
fad fad fad fad fad fad fad fad |
sol sol sol sol sol sol sol sol |
do do do do do do do do |
do\cresc do do do do do dod dod |
re4\f fad la fad |
re r r2 |
r8 re\p re re re4 r |
r8 re re re re4 r |
r8 re re4 r8 re re4 |
re8 r re r re r re r |
re4 r r2 |
re1~ |
re4 re re re |
re r r2 |
re1~ |
re~ |
re4 red2(\fp mi4) |
do r8. do16\cresc la,4 r8. la,16 |
si,4\f si, r r8. do16\p |
re4 re re re |
sol r re16(\f mi fad sol la8) r |
sol4\p r re16(\f mi fad sol la8) r |
sol4\p r re16(\f mi fad sol la8) r |
sol4\p r mi r |
re r sol, r |
do r8 sol, do4 r8 sol, |
do4 r8 sol, do sol, do sol, |
do do do do do do do do |
re re re re re re re re |
re\cresc re re re re re re re |
sol\f sol sol sol si si si si |
la la la la do' do' do' do' |
si4 sol8 sol do do re re |
sol,4 r sol8\p r la r |
sib8 r sol r mi r re r |
dod4 r la,\f r\fermata |
re4\p r fa la |
re' r sol2\sfp |
do4 r mi sol |
do' r r2 |
fa8 fa fa fa fa fa fa fa |
fa fa fa\cresc fa fad fad fad fad |
sol4\f si-. re'-. si-. |
sol4 r r2 |
r8 sol\p sol sol sol4 r |
r8 sol sol sol sol4 r |
sol r sol r |
sol r r2 |
r8 sol sol sol sol4 r |
r8 sol sol sol sol4 r |
sol r sol r |
sol r r2 |
sol1~ |
sol~ |
sol~ |
sol2.\fermata r4 | \allowPageTurn
r2 do'4.(\f si16 la |
sol8) sol-. sol-. sol-. sol4 r |
r2 do'4.( si16 la |
sol8) sol-. sol-. sol-. sol4 r |
\ru#4 { do4\f do\p do do | }
do4 r do8(\sf si, do) sol,-. |
r2 re8(\sf dod re) sol,-. |
r2 mi8(\sf red mi) do!-. |
fad8\p fad fad\cresc fad fad fad fad fad |
sol4\f sol sol, sol |
sol,2 r |
R1*3 |
r2 dod'2\f~ |
dod'4 mi'-.\p dod'-. la-. |
re'-. si-. do'!-. fa-. |
sol sol sol sol |
do r r2 |
R1*3 |
r2 dod'2\f~ |
dod'4 mi'-.\p dod'-. la-. |
re'-. si-. do'!-. fa-. |
sol sol sol sol |
do r r2 |
R1*2 |
\ru#4 { r4 do r2 | }
r4 fa-. si,-. r |
r mi-. la,-. r |
re2( sol,) |
do4 r r2 |
do4 r r2 |
R1*2 |
fa8\f fa fa fa sol sol sol sol |
do4 r r2 |
R1*2 |
r4 do\p r2 |
r4 do r2 |
r4 do r2 |
r4 do r2 |
r4 fa-. si,-. r |
r mi-. la,-. r |
re2( sol,) |
do4 r r2 |
do4 r r2 |
R1*2 |
fa8\f fa fa fa sol sol sol sol |
\tuplet 3/2 { do'(\sf re' mi') } \tuplet 3/2 { re'(\p do' si) } la4 r |
fa r sol r |
do'8*2/3(\sf re' mi') re'(\p do' si) la4 r |
fa r sol r |
do'8 do' do' do' la la\cresc la la |
fa fa fa fa sol sol sol sol |
<>\f \tuplet 3/2 { do8 do' do' } \tuplet 3/2 { do' do' do' } do'4 r |
do8*2/3 do' do' do' do' do' do'4 r |
do do do do |
do2 r |
