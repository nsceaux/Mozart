\clef "alto" r4 r8. \tuplet 3/2 { sol32\f( la si) } do'2 |
r4 r8. \tuplet 3/2 { si32( do' re') } mi'2 |
sol'\p mi'4. do'8 |
la'2. do'4 |
si-. sol'-. r2 |
sol2.( <fad la>4) |
<sol si>2.( <la do'>4) |
<si re'>2.( <mi' do'>4) |
<re' fa'>1~ |
q2. si4 |
do' r16 do''\f si' la' sol' si' la' sol' fa' la' sol' fa' |
mi'8\p mi' mi' mi' mi' mi' mi' mi' |
fa' fa' fa' fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
do'4 r r2 |
r4 do''8 do'' fa' fa' sol' sol' |
do'4 do'8. do'16 do'4 r |
do'16\fp do' do' do' do'4:16 do'2:16\fp |
si8\fp re'4 re' re' re'8~ |
re' re'4 re' re' re'8~ |
re' re'4 re' re' re'8 |
do' do' do' do' do' do' do' do' |
do'2:16\cresc la:16 |
re':16\f re':16 |
re'4 r r2 |
r8 fad'\p fad' fad' fad'4 r |
r8 fad' fad' fad' fad'4 r |
r8 la( fad4) r8 do'( la4) |
fad'8 r fad' r fad' r fad' r |
re'4 r r2 |
re'1~ |
re'4 re'2 re'4 |
re' r r2 |
re'1 |
re1 |
r4 fad2(\fp sol4) |
sol2:16\fp do':16\fp |
re'4\f si r r8. do'16\p |
re'1 |
sol'16( la' si' do'' re''8) r fad'16(\f sol' la' si' do''8) r |
sol'16(\p la' si' do'' re''8) r fad'16(\f sol' la' si' do''8) r |
sol'16(\p la' si' do'' re''8) r fad'16(\f sol' la' si' do''8) r |
r8 si\p si si la la la la |
la la la la sol sol sol sol |
sol( mi' do' si) do'( mi' do' si) |
do'( mi' do' si do' si do' si) |
do' do' do' do' do' do' do' do' |
re' re' re' re' re' re' re' re' |
re'\cresc re' re' re' do' do' do' do' |
si\f si si si re' re' sol' sol' |
fad' fad' do'' do'' la' fad' la la |
si4 sol'8 sol' do' do' re' re' |
sol4 r sol'8\p r la' r |
sib'8 r sol' r mi' r re' r |
dod'4 r la\f r\fermata |
r8 fa'\p fa' fa' fa' fa' fa'( la') |
r4 re'\sf~ re'16(\p mi' fa' mi' re' do'! re' si) |
r8 mi' mi' mi' mi' mi' mi'( sol') |
r4 do'4\sf~ do'16(\p re' mi' re' mi' re' mi' do') |
fa'2:16 re':16 |
la'2*1/2:16 s4\cresc la'4:16 re':16 |
<si sol'>2:16\f q:16 |
q4 r r2 |
r8 si\p si si si4 r |
r8 re' re' re' re'4 r |
sol1~ |
sol2~ sol4 r |
r8 sol' sol' sol' sol'4 r |
r8 sol' sol' sol' sol'4 r |
sol1~ |
sol~ |
sol4 r r2 |
sol4 r r2 |
sol1\pp~ |
sol2.\fermata r4 | \allowPageTurn
%%
r2 do''4.(\f si'16 la' |
sol'8) sol'-. sol'-. sol'-. sol'4 r |
r2 do''4.( si'16 la' |
sol'8) sol'-. sol'-. sol'-. sol'4 r |
r8 mi'\sf sol'\p mi' fa' la' re' fa' |
\ru#3 { mi' mi'\sf sol'\p mi' fa' la' re' fa' | }
mi'4 r do'8(\sf si do') sol-. |
r2 re'8(\sf dod' re') sol-. |
r2 mi'8(\sf red' mi') do'!-. |
fad'8\p fad' fad'\cresc fad' fad' fad' fad' fad' |
sol'4\f sol' sol sol' |
sol2 r |
R1*3 |
r2 dod'2\f~ |
dod'4 mi'-.\p dod'-. la-. |
re'-. si-. do'!-. fa-. |
mi8( sol mi sol mi sol fa sol) |
mi4 r r2 |
R1*3 |
r2 dod'\f~ |
dod'4 mi'-.\p dod'-. la-. |
re'-. si-. do'!-. fa-. |
mi8( sol mi sol mi sol fa sol) |
mi4 r r2 |
R1*2 |
\ru#4 { r4 do' r2 | }
r4 fa'-. re'-. r |
r mi'-. do'-. r |
re'1 |
do'4 r r2 |
do'4 r r2 |
R1*2 |
fa'2:16\f re':16 |
do'4 r r2 |
R1*2 |
r4 do'\p r2 | 
\ru#3 { r4 do' r2 | }
r4 fa'-. re'-. r |
r mi'-. do'-. r |
re'1 |
do'4 r r2 |
do'4 r r2 |
R1*2 |
fa'2:16\f re':16 |
do'4\p r \tuplet 3/2 { la8(\sf si do') } \tuplet 3/2 { si(\p la sol) } |
fa4 la' r sol' |
sol' r la8*2/3(\sf si do') si(\p la sol) |
fa4 la' r sol' |
sol'2:16 la'2*1/4:16 s4.\cresc |
fa'2:16 sol':16 |
\tuplet 3/2 { do'8\f do'' do'' } \tuplet 3/2 { do'' do'' do'' } do''4 r |
do'8*2/3 do'' do'' do'' do'' do'' do''4 r |
do' do' do' do' |
do'2 r |


