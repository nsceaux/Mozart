\version "2.19.80"
\include "common.ily"

\opusTitle "K.419 – No, che non sei capace"

\header {
  title = \markup\center-column {
    Arie
    \italic No, che non sei capace
  }
  subtitle = "für Sopran mit Begleitung des Orchesters"
  opus = "K.419"
  date = "1783"
  copyrightYear = "2018"
}

\includeScore "K.419"
