\clef "soprano/treble" do''2 r |
mi'' r |
sol'' mi''4. do''8 |
la''2. do''4 |
si' sol'' r2 |
R1*2 |
r2 r4 r8 sol'' |
fa''2 re'' |
si'2. fa'4 |
mi' mi' r2 |
do''1 |
re'2. la''4 |
\appoggiatura la''4 sol''2. \appoggiatura sol''16 fa''8([ mi''16 re'']) |
do''4 do'' r2 |
R1 |
r2 r4 r8 sol' |
do''4. sol'8 mi''4. do''8 |
sol''4 mi'' r2 |
re''8.([ do''16]) do''4 r r8 do'' |
\appoggiatura do''4 si'2 r |
la'2 do''4. mi''8 |
la''2. sol''4 |
fad'' r r2 |
re''2 re''4 re'' |
do''2\melisma r16 do''[ si' do''] re''[ do'' si' do''] |
fad''2 r16 fad''[ mi'' fad''] sol''[ fad'' mi'' fad''] |
la''4 r8 fad''16\trill[ mi''32 fad''] la''4 r8 la''16[\trill sol''32 la''] |
do'''8[ do'''16( si'')] la''8[ la''16( sol'')] fad''8[ fad''16( mi'')] re''8[ re''16( do'')] |
si'[ do'' si' la'] si'[ re'' do'' mi''] re''4 r |
r16 sol'[ la' si'] do''[ re'' mi'' fad''] sol''[ la'' si'' do'''] re'''8[ re'''] |
re''' si''4 sol'' re'' si'8 |
do''16[ re'' si' re''] do''[ re'' si' re''] do''4 r |
re'8[ mi'16 fad'] sol'[ la' si' do''] re''[ mi'' fad'' sol''] la''[ si'' do''' re'''] |
mi'''8[ re''' do''' si''] la''[ sol'' fad'' mi''] |
re''4\melismaEnd do''2 si'4 |
mi''2 fad'' |
sol''8.([ re''16]) re''2~ re''8. mi''16 |
sol'2.( si'8[ la']) |
sol'4 r r r8 re'' |
sol''4 re'' r8 re'' re'' re'' |
sol''4 re'' r2 |
sol''2\melisma la''16([ sol'' fad'' sol''] fad''[ sol'' la'' sol'']) |
fad''2 sol''16([ fa'' mi'' fa''] mi''[ fa'' sol'' fa'']) |
mi''4. fa''16[ re''] mi''4. fa''16[ re''] |
mi''4. fa''16[ re''] mi''8[ fa''16 re''] mi''8[ fa''16 re''] |
mi''16[ fad'' sol'' fad''] sol''[ fad'' sol'' fad''] \grace la''16 sol''8[ fad''16 mi''] \grace fad''16 mi''8[ re''16 do''] |
si'16[ re'' do'' si'] do''[ re'' mi'' fad''] sol''[ la'' si'' do'''] re'''8\melismaEnd r |
la'2\startTrillSpan~ la'4.*2/3 s8\stopTrillSpan sol'8 |
sol'4 r r2 |
R1*4 |
mi''4 r la''2\fermata |
re''4 re''8. re''16 re''4. mi''8 |
mi''([ fa'']) fa''4 r2 |
do''4 do''8. do''16 do''4. re''8 |
re''([ mi'']) mi''4 r r8 mi'' |
re''2 fa'' |
la''2. do''4 |
si' sol' r2 |
sol''2 re''4 si' |
sol'2~ sol'16[\melisma la' sol' fad'] sol'[ si' la' do''] |
si'4 r si'16[ do'' si' la'] si'[ re'' do'' mi''] |
re''8[( si') fa''-. fa''-.] fa''[( re'') si''-. si''-.] |
si''[( sol'') re'''-. re'''-.] re'''4 r |
sol'2 \grace la'16 sol'8[ fa'16 mi'] fa'[ sol' la' si'] |
do''4 r do''16[ re'' do'' si'] do''[ re'' mi'' fa''] |
mi''8[( do'') sol''-. sol''-.] sol''[( mi'') do'''-. do'''-.] |
do'''[( sol'') mi'''-. mi'''-.] mi'''4 r |
fa''16[ sol'' la'' sol''] fa''[ mi'' re'' do''] si'[ do'' si' la'] si'[ do'' re'' mi''] |
fa''[ sol'' la'' sol''] fa''[ mi'' re'' do''] si'[ do'' si' la'] si'[ do'' re'' mi''] |
fa''[ sol'' la'' sol''] <>^\markup\italic ad libitum fa''[ mi'' re'' do''] si'[ do'' si' la'] si'[ do'' re'' mi''] |
sol''8([ fa''])\melismaEnd fa''2\fermata( fad''4) |
sol''4 do'' r2 |
r4 r8 sol'' sol''4. si'8 |
do''4 do'' r2 |
r4 r8 sol'' sol''4. si'8 |
do''4 do'' r r8 si' |
do''4. do''8 fa''4. si'8 |
do''4 do'' r r8 si' |
do''4. do''8 fa''4. si'8 |
do''4 do'' r r8 sol' |
re''4 re'' r r8 sol' |
mi''4 mi'' r r8 do'' |
fad''2. fad''4 |
sol'' sol' r2 |
r r4 r8 sol' |
la'4. la'8 si'4. si'8 |
do''4 do'' r r8 si' |
do''4. do''8 re''4. re''8 |
mi''4 mi'' la''2~ |
la''4 sol''8. fa''16 mi''4 la'' |
fa'' sol'' mi'' la'' |
do''2. mi''8([ re'']) |
do''4 r r r8 sol' |
la'4. re''8 si'4. mi''8 |
do''4 do'' r r8 si' |
do''4. fa''8 re''4. sol''8 |
mi''4 mi'' la''2~ |
la''4 sol''8. fa''16 mi''4 la'' |
fa'' sol'' mi'' la'' |
do''2. mi''8([ re'']) |
do''4 r r2 |
r8 sol'([ la' si']) do''([ re'']) mi''([ fa'']) |
sol''8[\melisma do'' re'' mi''] fa''[ sol'' la'' si''] |
do'''4 r8 do'''( si''[ la'' sol'' fad''] |
la''[ sol'']) r do'''( si''[ la'' sol'' fad''] |
la''[ sol'']) r do'''( si''[ la'' sol'' fad''] |
sol''4) r8 mi''( fa''![ fad'' sol'' sold''] |
la''4) r8 dod''( re''[ mi'' fa''! fad''] |
sol''4) r8 si'( do''![ re'' mib'' mi'']) |
fa''([ mi'' re'' mi'']) fa''([ sol'' la'' si'']) |
do'''8[ sol''] sol''4\trill mi''8[ sol''] sol''4\trill |
do'''8[ sol''] sol''4\trill mi''8[ sol''] sol''4\trill |
do'''8[ sol'' mi'' sol''] do'''[ sol'' do''' mi''']\melismaEnd |
do'''4 sol'' mi'' do'' |
la''2 si' |
do''4 r r2 |
r8 sol'([ la' si']) do''([ re'']) mi''([ fa'']) |
sol''8[\melisma do'' re'' mi''] fa''[ sol'' la'' si''] |
do'''4 r8 do'''( si''[ la'' sol'' fad''] |
la''[ sol'']) r do'''( si''[ la'' sol'' fad''] |
la''[ sol'']) r do'''( si''[ la'' sol'' fad''] |
sol''4) r8 mi''( fa''![ fad'' sol'' sold'']) |
sold''([ la'']) r dod''( re''[ mi'' fa''! fad'']) |
fad''([ sol'']) r8 si'( do''[ re'' mib'' mi'']) |
fa''([ mi'' re'' mi''] fa''[ sol'' la'' si'']) |
do'''8[ sol''] sol''4\trill mi''8[ sol''] sol''4\trill |
do'''8[ sol''] sol''4\trill mi''8[ sol''] sol''4\trill |
do'''8[ sol'' mi'' sol''] do'''[ sol'' do''' mi''']\melismaEnd |
do'''4 sol'' mi'' do'' |
la''2 si' |
do''4 r r mi'' |
re'' r8 re'' sol''4 r8 sol'' |
mi''4 r r mi'' |
re''4 r8 re'' sol''4 r8 sol'' |
mi''4. mi''8 la''4. la''8 |
fa''2 sol'' |
do''4 r r2 |
R1*3 |
