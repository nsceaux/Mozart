\clef "treble" r2 \tag #'trombe <>^"a 2." do'2\f |
r2 mi' |
R1*8 |
r2 <<
  \tag #'(tromba1 trombe) { do''4 do'' | do'' }
  \tag #'(tromba2 trombe) { do'4 do' | do' }
>> r4 r2 |
R1*2 |
r2 <<
  \tag #'(tromba1 trombe) {
    re''2 |
    mi''4 s s re'' |
    mi'' do''8. do''16 do''4
  }
  \tag #'(tromba2 trombe) {
    sol'2 |
    do''4 s s sol' |
    sol' do'8. do'16 do'4
  }
  { s2 | s4 r r s | }
>> r4 |
R1*6 |
\tag #'trombe <>^"a 2." r4 re'' re'' re'' |
re'' r r2 |
R1*14 |
r2 re'' |
r re'' |
r re'' |
R1*7 |
<<
  \tag #'(tromba1 trombe) { re''1 | re'' | re''4 }
  \tag #'(tromba2 trombe) { sol'1 | re'' | sol'4 }
>> r4 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 | re'' }
  { re'' | sol' }
>> r4 r2 |
R1 |
r2 r\fermata |
R1*6 |
\tag #'trombe <>^"a 2." sol'4 sol' sol' sol' |
sol' r r2 |
R1*11 |
r2 r4\fermata r |
%%
r2 <>\f <<
  \tag #'(tromba1 trombe) { do''2 | }
  \tag #'(tromba2 trombe) { do' | }
>>
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 sol' sol' sol' sol'4 }
  { sol'8 sol' sol' sol' sol'4 }
>> r4 |
r2 <<
  \tag #'(tromba1 trombe) { do''2 | }
  \tag #'(tromba2 trombe) { do' | }
>>
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 sol' sol' sol' sol'4 }
  { sol'8 sol' sol' sol' sol'4 }
>> r4 |
\ru#4 {
  <<
    \tag #'(tromba1 trombe) { do''4 }
    \tag #'(tromba2 trombe) { do' }
  >> r4 r2 |
}
r2 <>\sf <<
  \tag #'(tromba1 trombe) { do''2 }
  \tag #'(tromba2 trombe) { do' }
>>
r2 <>\sf <<
  \tag #'(tromba1 trombe) { re''2 }
  \tag #'(tromba2 trombe) { sol' }
>>
r2 <>\sf <<
  \tag #'(tromba1 trombe) { mi''2 }
  \tag #'(tromba2 trombe) { mi' }
>>
R1 |
r4 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 sol' sol' | sol'2 }
  { sol'4 sol' sol' | sol'2 }
>> r2 |
R1*45 |
<<
  \tag #'(tromba1 trombe) { do''2 }
  \tag #'(tromba2 trombe) { mi' }
>> r2 |
R1 |
<<
  \tag #'(tromba1 trombe) { do''2 }
  \tag #'(tromba2 trombe) { mi' }
>> r2 |
R1*2 |
r2 <<
  \tag #'(tromba1 trombe) { re''2 | do''4 }
  \tag #'(tromba2 trombe) { sol'2 | mi'4 }
  { s2 | s4\f }
>> r4 <<
  \tag #'(tromba1 trombe) { sol'4 sol' | sol' }
  \tag #'(tromba2 trombe) { mi'4 mi' | mi' }
>> r4 <<
  \tag #'(tromba1 trombe) { sol'4 sol' | sol' mi' mi' mi' | mi'2 }
  \tag #'(tromba2 trombe) { mi'4 mi' | mi' do' do' do' | do'2 }
>> r2 |
