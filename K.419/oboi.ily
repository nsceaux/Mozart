\clef "treble" r2 \tag #'oboi <>^"a 2." do''2\f |
r2 mi'' |
R1*8 |
r4 r8. <<
  \tag #'(oboe1 oboi) { do'''16 si''4-. la''-. | sol'' }
  \tag #'(oboe2 oboi) { do''16 si'4-. la'-. | sol' }
>> r4 r2 |
R1*2 |
r2 <>\f <<
  \tag #'(oboe1 oboi) {
    fa''2( |
    mi''4) sol''8 sol'' la'' la'' re'' re'' |
    mi''4
  }
  \tag #'(oboe2 oboi) {
    si'2( |
    do''4) do''8 do'' do'' do'' si' si' |
    do''4
  }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8. do''16 do''4 }
  { do''8. do''16 do''4 }
>> r4 |
R1*6 |
\tag #'oboi <>^"a 2." r4 fad'' la'' fad'' |
re'' r r2 |
R1*14 |
\ru#3 {
  r2 \twoVoices #'(oboe1 oboe2 oboi) <<
    { do'''16( si'' la'' sol'' fad''8) }
    { fad'16( sol' la' si' do''8) }
  >> r8 |
}
R1*2 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1~ | sol''~ | sol''4 }
  \tag #'(oboe2 oboi) { sol'1~ | sol'~ | sol'4 }
>> r4 r2 |
R1 |
r2 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 | sol''1 | fad'' | }
  { fad''2 | sol''( re'') | do''( la') | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''4 si''8 si'' do''' do''' la'' la'' |
  }
  \tag #'(oboe2 oboi) {
    si'4 re''8 re'' mi'' mi'' fad'' fad'' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 }
  { sol'' }
>> r4 r2 |
R1 |
r2 r\fermata |
R1*6 |
<>\f <<
  \tag #'(oboe1 oboi) { sol''1~ | sol''4 }
  \tag #'(oboe2 oboi) { si'1~ | si'4 }
>> r4 r2 |
R1*2 |
<>\p <<
  \tag #'(oboe1 oboi) {
    fa''1~ |
    fa''2~ fa''8 fa''( mi'' re'') |
    mi''4
  }
  \tag #'(oboe2 oboi) {
    re''1~ |
    re''2~ re''8 re''( do'' si') |
    do''4
  }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    sol''1~ |
    sol''2~ sol''8 sol''( fa'' mi'') |
    fa''4 s fa''4( re''8) s |
    s2 fa''4( re''8) s |
  }
  \tag #'(oboe2 oboi) {
    mi''1~ |
    mi''2~ mi''8 mi''( re'' dod'') |
    re''4 s re''4( si'8) s |
    s2 re''4( si'8) s |
  }
  { s1*2 | s4 r s4. r8 | r2 s4. r8 | }
>>
R1 |
r2 r4\fermata r |
%%
r2 <>\f <<
  \tag #'(oboe1 oboi) {
    do'''4.( si''16 la'' |
    sol''8) sol''-. sol''-. sol''-. sol''4
  }
  \tag #'(oboe2 oboi) {
    do''4.( si'16 la' |
    sol'8) sol'-. sol'-. sol'-. sol'4
  }
>> r4 |
r2 <<
  \tag #'(oboe1 oboi) {
    do'''4.( si''16 la'' |
    sol''8) sol''-. sol''-. sol''-. sol''4
  }
  \tag #'(oboe2 oboi) {
    do''4.( si'16 la' |
    sol'8) sol'-. sol'-. sol'-. sol'4
  }
>> r4 |
\ru#4 <<
  \tag #'(oboe1 oboi) { do'''1 }
  \tag #'(oboe2 oboi) { do'' }
  { s1\sfp }
>>
r2 \tag #'oboi <>^"a 2." do''8(\sf si' do'') sol'-. |
r2 re''8(\sf dod'' re'') sol'-. |
r2 mi''8(\sf red'' mi'') do''!-. |
fad''1*1/4\p s2.\cresc |
sol''4\f <<
  \tag #'(oboe1 oboi) { sol''4 sol'' sol'' | sol''2 }
  \tag #'(oboe2 oboi) { sol'4 sol' sol' | sol'2 }
>> r2 |
R1*3 |
r2 <<
  \tag #'(oboe1 oboi) { la''2~ | la''4 }
  \tag #'(oboe2 oboi) { la'2~ | la'4 }
>> r4 r2 |
R1*6 |
r2 <<
  \tag #'(oboe1 oboi) { la''2~ | la''4 }
  \tag #'(oboe2 oboi) { la'2~ | la'4 }
>> r4 r2 |
R1*32 |
r2 <>\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2( | re''1) | mi''4 }
  { do''2~ | do''( si') | do''4 }
>> r4  <>\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2( | re''1) | mi''1( | re'') | }
  { do''2~ | do''( si') | do''1~ | do''2( si') | }
  { s2 | s1 | s2 s8 s4.\cresc | }
>>
\tag #'oboi <>^"a 2." do''1\f~ |
do''~ |
do''4 do'' do'' do'' |
do''2 r |
