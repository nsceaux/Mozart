\clef "treble" r4 r8. \tuplet 3/2 { sol'32\f( la' si') } do''2 |
r4 r8. \tuplet 3/2 { si'32( do'' re'') } mi''2 |
\override Script.avoid-slur = #'outside
<<
  \tag #'violino1 {
    sol''8\p sol'' sol'' sol'' mi'' mi'' mi'' do'' |
    la'' la'' la'' la'' la''8. do''16 do''8.\trill si'32 do'' |
    si'4-. sol''-. r2 |
    sol'2. la'16( si') do''-. do''-. |
    do''( si') si'-. si'-. si'2 do''16( re'') mi''-. mi''-. |
    mi''( re'') re''-. re''-. re''2 mi''16( fa'') sol''-. sol''-. |
    r8 fa''(\trill la'' fa'') r re''(\trill fa'' re'') |
    si'8 r re'' r fa'' r fa' r |
    mi'4
  }
  \tag #'violino2 {
    sol'8\p sol' sol' sol' mi' mi' mi' do' |
    la' la' la' la' la'8. do'16 do'8.\trill si32 do' |
    si4-. sol'-. r2 |
    sol'2. fad'16( sol') la'-. la'-. |
    la'( sol') sol'-. sol'-. sol'2 la'16( si') do''-. do''-. |
    do''( si') si'-. si'-. si'2 do''16( re'') mi''-. mi''-. |
    r8 re''(\trill fa'' re'') r si'(\trill re'' si') |
    re' r si' r re'' r re' r |
    do'4
  }
>>
r16 mi''\f re'' do'' si' re'' do'' si' la' do'' si' la' |
<<
  \tag #'violino1 {
    sol'8 r do''\p r mi'' r sol'' r |
    la' r re'' r fa'' r la'' r |
    sol'' r mi'' r sol'' r si' r |
    do''4
  }
  \tag #'violino2 {
    <>\p \ru#2 { sol'16( mi') do'-. do'-. do'4:16 } |
    \ru#2 { la'16( re') do'-. do'-. do'4:16 } |
    sol'16( mi') do'-. do'-. do'4:16 si16( re') fa'-. fa'-. fa'4:16 |
    mi'4
  }
>> do'''16\f si'' la'' sol'' fa'' mi'' re'' do'' si' la' sol' fa' |
mi'4 <mi' do'' sol''>8 q <fa' do'' la''> q <re' si' sol''> q |
<mi' do'' mi''>4 do'8. do'16 do'4 r |
<<
  \tag #'violino1 {
    sol'16\fp sol' sol' sol' sol'4:16 sol'2:16\fp |
    sol'8\fp sol''16([\trill fad''32 sol''] si''8) sol''-. re''-. sol''-. si'-. re''-. |
    r8 la''16\trill( sol''32 la'' do'''8) la''-. fad''-. la''-. la'-. do''-. |
    r8 si''16(\trill la''32 si'' re'''8) si''-. sol''-. re''-. si'-. sol'-. |
    la'2:16 la':16 |
    la':16\cresc la'4:16 sol':16 |
    fad'4\f <re' la' fad''> <re' la' la''> <re' la' fad''> |
    re''8 fad'[-.\p fad'( sol')] la'( sol' la') si'-. |
    do''( la') do''-. do''-. do''4 r |
    r8 do'' do'' do'' do''4 r |
    r8 fad'( la'4) r8 la'( do''4) |
    do''8 r do'' r do'' r do'' r |
    si'4 r si'16( do'' si' la' si' re'' do'' mi'') |
    re''1~ |
    re''4 re''2( si'4) |
    do''4 r do''16( re'' si' re'' do'' re'' si' re'') |
    do''2. la'16( si' do'' re'') |
    mi''8( re'' do'' si' la' sol' fad' mi') |
    re'4 do'2(\fp si4) |
    mi'8 sol'16 do'' mi''8[\cresc r16 mi''] fad'8 la'16 do'' fad''8[ r16 fad''] |
    sol''\f re'' sol'' si'' re'''4 r r8. mi''16\p |
    sol'2.( fad'4) |
    re'''16( do''' si'' la'' sol''8) r do'''16(\f si'' la'' sol'' fad''8) r |
    re'''16(\p do''' si'' la'' sol''8) r do'''16(\f si'' la'' sol'' fad''8) r |
    re'''16(\p do''' si'' la'' sol''8) r do'''16(\f si'' la'' sol'' fad''8) r |
    r8 sol'\p sol' sol' sol' sol' sol' sol' |
    fad' fad' fad' fad' fa' fa' fa' fa' |
    <<
      { mi'4.( fa'8) mi'4.( fa'8) |
        mi'4.( fa'8 mi' fa' mi' fa') | } \\
      { sol1 | sol | }
    >>
    mi'8 r sol' r do'' r mi'' r |
    r si' si' si' si' si' si' si' |
    la'2:16\cresc la':16 |
    sol'8\f la'16 si' do'' re'' mi'' fad'' sol'' la'' si'' do''' re'''4~ |
    re'''8 re'''4 re''' re'''8-. re'''-. re'''-. |
    re'''4 <si'' re''>8 q <mi'' do'''> q <re'' la''> q |
  }
  \tag #'violino2 {
    mi'16\fp mi' mi' mi' mi'4:16 mi'2:16\fp |
    re'8\fp sol'4 sol' sol' sol'8( |
    la') la'4 la' la' la'8( |
    si') si'4 si' si' si'8 |
    mi'2:16 mi':16 |
    mi':16\cresc mi':16 |
    <la fad'>\f q:16 |
    q8 re'[-.\p re'( mi')] fad'( mi' fad') sol'-. |
    la'( fad') la'-. la'-. la'4 r |
    r8 la' la' la' la'4 r |
    r8 re'( fad'4) r8 fad'( la'4) |
    la'8 r la' r la' r la' r |
    sol'4 r sol'16( la' sol' fad' sol' si' la' do'') |
    si'1~ |
    si'4 si'2( sold'4) |
    la'4 r la'16( si' sold' si' la' si' sold' si') |
    la'2. fad'16( sol'! la' si') |
    do''8( si' la' sol' fad' mi' re' do') |
    si4 la2(\fp sol4) |
    mi'2:16\fp fad':16\fp |
    sol'4\f <re' si' sol''>4 r r8. sol'16\p |
    si8( re' si re' si re' do' re') |
    si16( la sol la si8) r re''4:16\f re''8 r |
    si16(\p la sol la si8) r re''4:16\f re''8 r |
    si16(\p la sol la si8) r re''4:16\f re''8 r |
    r8 re'\p re' re' dod' dod' dod' dod' |
    do'! do' do' do' si si si si |
    do'( sol' mi' re') mi'( sol' mi' re') |
    mi'( sol' mi' re' mi' re' mi' re') |
    do' r mi' r sol' r do'' r |
    r sol' sol' sol' sol' sol' sol' sol' |
    sol'2:16\cresc fad':16 |
    sol'4\f r r2 |
    do'''8 si''16 la'' sol'' fad'' mi'' re'' do'' si' la' sol' fad'4 |
    sol' <si' sol''>8 q <do'' sol''> q <re' la' fad''> q |
  }
>>
<re' si' sol''>4 r sol'\p la'8.(\trill sol'32 la') |
sib'8 r sol' r mi' r re' r |
dod'4 r <<
  \tag #'violino1 {
    <mi' dod'' la''>4\f r\fermata |
    r8 re''\p re'' re'' re'' re'' re''( mi'') |
    r4 fa''\sf~ fa''16\p sol''( la'' sol'' fa'' mi'' fa'' re'') |
    r8 do''! do'' do'' do'' do'' do''( re'') |
    r4 mi''\sf~ mi''16\p fa''( sol'' fa'' sol'' fa'' sol'' mi'') |
    r8 re''16(\trill dod'' re''8) la'-. r fa''16(\trill mi'' fa''8) re''-. |
    r8 la''16(\trill sold'' la''8)\cresc la''-. do'''-. la''-. fad''-. do''-. |
    si'4\f si''-. re'''-. si''-. |
    <sol'' si' re'>4 r r2 |
    r8 sol'\p sol' sol' sol'4 r |
    r8 si' si' si' si'4 r |
    fa'1~ |
    fa'2~ fa'8 re'( mi' fa') |
    mi' do'' do'' do'' do''4 r |
    r8 mi'' mi'' mi'' mi''4 r |
    mi'1~ |
    mi'2~ mi'8 mi'( fa' sol') |
    fa'4( re'8) r r2 |
    fa'4( re'8) r r2 |
    <sol fa'>1\pp~ |
    q2.\fermata r4 |
  }
  \tag #'violino2 {
    <la mi' dod''>4\f r\fermata |
    r8 la'\p la' la' la' la' la'( dod'') |
    r4 re'\sf~ re'16\p mi'( fa' mi' re' do'! re' si) |
    r8 sol' sol' sol' sol' sol' sol'( si') |
    r4 do'\sf~ do'16\p re'( mi' re' mi' re' mi' do') |
    la'2:16 la':16 |
    re''2*1/2:16 s4\cresc re''4:16 la':16 |
    <re' si'>2:16\f q:16 |
    <sol re' si'>4 r r2 |
    r8 re'\p re' re' re'4 r |
    r8 sol' sol' sol' sol'4 r |
    <re' si>1~ |
    q2~ q8 si( do' re') |
    do' mi' mi' mi' mi'4 r |
    r8 do'' do'' do'' do''4 r |
    do'1~ |
    do'2~ do'8 do'( re' mi') |
    re'4( si8) r r2 |
    re'4( si8) r r2 |
    <si re'>1\pp~ |
    q2.\fermata r4 |
  }
>> \allowPageTurn
%%
r2 do'''4.(\f si''16 la'' |
sol''8) sol''-. sol''-. sol''-. sol''4 r |
r2 do'''4.( si''16 la'' |
sol''8) sol''-. sol''-. sol''-. sol''4 r |
<<
  \tag #'violino1 {
    r8 sol''\sf mi''\p do'' la' do'' si' re'' |
    do'' sol''\sf mi''\p do'' la' do'' si' re'' |
    do'' sol''\sf mi''\p do'' la' do'' si' re'' |
    do'' sol''\sf mi''\p do'' la' do'' si' re'' |
    do''( si' do'') sol'-.
  }
  \tag #'violino2 {
    r8 mi'\sf sol'\p mi' fa' la' re' fa' |
    mi' mi'\sf sol'\p mi' fa' la' re' fa' |
    mi' mi'\sf sol'\p mi' fa' la' re' fa' |
    mi' mi'\sf sol'\p mi' fa' la' re' fa' |
    do'( si do') sol-.
  }
>> do''8(\sf si' do'') sol'-. |
<<
  \tag #'violino1 { re''8(\p dod'' re'') sol'-. }
  \tag #'violino2 { re'8(\p dod' re') sol-. }
>> re''8(\sf dod'' re'') sol'-. |
<<
  \tag #'violino1 { mi''8\p( red'' mi'') do''!-. }
  \tag #'violino2 { mi'8\p( red' mi') do'!-. }
>> mi''8\sf( red'' mi'') do''-. |
fad''2*1/2:16\p s4\cresc fad''2:16 |
sol''4\f <re' si' sol''> sol q |
sol2 <<
  \tag #'violino1 {
    sol'8\p fa'! sol' do'' |
    la' sol' la' re'' si' la' si' mi'' |
    do'' si' do'' fa'' si' la' si' mi'' |
    do'' si' do'' fa'' re'' do'' re'' sol'' |
    mi''4 r <la' la''>2\f~ |
    q4 sol''8.\p fa''16 mi''4-. la''-. |
    fa''-. sol''-. mi''-. la''-. |
    do''2.( mi''8 re'') |
    do''8 sol' mi' fa' sol' fa' sol' do'' |
    la' sol' la' re'' si' la' si' mi'' |
    do'' si' do'' fa'' si' la' si' mi'' |
    do'' si' do'' fa'' re'' do'' re'' sol'' |
    mi''4 r <la' la''>2\f~ |
    q4 sol''8.\p fa''16 mi''4-. la''-. |
    fa''-. sol''-. mi''-. la''-. |
    do''2.( mi''8 re'') |
    do''8 do'-. re'-. mi'-. fa'-. sol'-. la'-. si'-. |
    do''-. sol-. la-. si-. do'-. re'-. mi'-. fa'-. |
    sol' do' re' mi' fa' sol' la' si' |
    do''4 r8 do''( si' la' sol' fad') |
    la'( sol') r do''( si' la' sol' fad') |
    la'( sol') r do''( si' la' sol' fad') |
    sol'4-. sol''-. r2 |
    r4 la'-. la''-. r |
    r sol'-. sol''-. r |
    la'2( si') |
    do''4 sol' mi' sol' |
    do'' sol' mi' sol' |
    mi'8 mi' sol' sol' mi' mi' sol' sol' |
    mi'4 r r2 |
    la''2:16\f si':16 |
    do''8 do'-.\p re'-. mi'-. fa'-. sol'-. la'-. si'-. |
    do''-. sol-. la-. si-. do'-. re'-. mi'-. fa'-. |
    sol' do' re' mi' fa' sol' la' si' |
    do''4 r8 do''( si' la' sol' fad') |
    la'( sol') r do''( si' la' sol' fad') |
    la'( sol') r do''( si' la' sol' fad') |
    sol'4-. sol''-. r2 |
    r4 la'-. la''-. r |
    r sol'-. sol''-. r |
    la'2( si') |
    do''4 sol' mi' sol' |
    do'' sol' mi' sol' |
    mi'8 mi' sol' sol' mi' mi' sol' sol' |
    mi'4 r r2 |
    la''2:16\f si':16 |
    do''4\p <mi'' do'''>2\sf mi''4\p |
    r re'' r sol'' |
    mi'' <mi'' do'''>2\sf mi''4\p |
    r re'' r sol'' |
    <mi'' do'''>2:16 q2*1/4:16 s4.\cresc |
    <re'' do'''>2:16 <re'' si''>:16 |
    do'''4\f
  }
  \tag #'violino2 {
    mi'8\p re' mi' do' |
    fa' mi' fa' re' sol' fa' sol' mi' |
    la' sol' la' fa' sol' fa' sol' mi' |
    la' sol' la' fa' si' la' si' sol' |
    do''4 r <la la'>2\f~ |
    q4 sol'8.\p fa'16 mi'4-. la'-. |
    fa'-. sol'-. mi'-. la'-. |
    do'2.( mi'8 re') |
    do'8 sol do' re' mi' re' mi' do' |
    fa' mi' fa' re' sol' fa' sol' mi' |
    la' sol' la' fa' sol' fa' sol' mi' |
    la' sol' la' fa' si' la' si' sol' |
    do''4 r <la la'>2\f~ |
    q4 sol'8.\p fa'16 mi'4-. la'-. |
    fa'-. sol'-. mi'-. la'-. |
    do'2.( mi'8 re') |
    do'8 do'-. si-. do'-. re'-. mi'-. fa'-. re'-. |
    mi'4-. r8 sol-. la-. si-. do'-. re'-. |
    mi' do' si do' re' mi' fa' re' |
    mi'4 r8 la'( sol' fa' mi' red') |
    fa'( mi') r la'( sol' fa' mi' red') |
    fa'( mi') r la'( sol' fa' mi' red') |
    mi'4-. mi''-. r2 |
    r4 re'-. fa''-. r |
    r do'-. mi''-. r |
    fa'1 |
    mi'4 mi' do' mi' |
    mi' mi' do' mi' |
    do'8 do' mi' mi' do' do' mi' mi' |
    do'4 r r2 |
    do''2:16\f fa':16 |
    mi'8 do'-.\p si-. do'-. re'-. mi'-. fa'-. re'-. |
    mi'4-. r8 sol-. la-. si-. do'-. re'-. |
    mi' do' si do' re' mi' fa' re' |
    mi'4 r8 la'( sol' fa' mi' red') |
    fa'( mi') r la'( sol' fa' mi' red') |
    fa'( mi') r la'( sol' fa' mi' red') |
    mi'4-. mi''-. r2 |
    r4 re'-. fa''-. r |
    r do'-. mi''-. r |
    fa'1 |
    mi'4 mi' do' mi' |
    mi' mi' do' mi' |
    do'8 do' mi' mi' do' do' mi' mi' |
    do'4 r r2 |
    do''2:16\f fa':16 |
    mi'4\p r \tuplet 3/2 { la'8(\sf si' do'') } \tuplet 3/2 { si'8(\p la' sol') } |
    fa'4 do'' r si' |
    do'' r la'8*2/3(\sf si' do'') si'(\p la' sol') |
    fa'4 do'' r si' |
    <do'' mi''>2:16 q2*1/4:16 s4.\cresc |
    <do'' la''>2:16 <si' sol''>:16 |
    <do'' mi''>4\f
  }
>> r8 \tuplet 3/2 { sol'16( la' si') } \tuplet 3/2 { do''8 do'' do'' } \tuplet 3/2 { mi'' mi'' mi'' } |
sol''4 r8 do''16( re'') mi''8*2/3 mi'' mi'' sol'' sol'' sol'' |
do'''4 do' do' do' |
do'2 r |
