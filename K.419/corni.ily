\clef "treble" r2 <>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do'2 }
>>
r2 <<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { mi' }
>>
R1*3 |
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol'~ | sol'4 }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol~ | sol4 }
>> r4 r2 |
R1 |
r2 <>\f <<
  \tag #'(corno1 corni) { do''4 do'' | do'' }
  \tag #'(corno2 corni) { do'4 do' | do' }
>> r4 r2 |
R1*2 |
r2 <>\f <<
  \tag #'(corno1 corni) {
    re''2( |
    mi''4) mi''8 mi'' fa'' fa'' re'' re'' |
    mi''4 do''8. do''16 do''4
  }
  \tag #'(corno2 corni) {
    sol'2( |
    do''4) do''8 do'' do'' do'' sol' sol' |
    sol'4 do'8. do'16 do'4
  }
>> r4 |
R1*6 |
\tag #'corni <>^"a 2." r4 re'' re'' re'' |
re'' r r2 |
R1*14 |
r2 re'' |
r re'' |
r re'' |
R1*5 |
re''1\p~ |
re''\cresc |
<>\f <<
  \tag #'(corno1 corni) {
    re''2~ re''8 re'' re'' re'' |
    re''2~ re''8 re'' re'' re'' |
    re''4 re''8 re''
  }
  \tag #'(corno2 corni) {
    sol'2~ sol'8 sol' sol' sol' |
    do''2~ do''8 do'' do'' do'' |
    sol'4 sol'8 sol'
  }
>> \twoVoices #'(corno1 corno2 corni) <<
  { mi''8 mi'' re'' re'' | re''4 }
  { do''8 do'' re'' re'' | sol'4 }
>> r4 r2 |
R1 |
r2 r\fermata |
R1*3 |
r2 <>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>>
R1*2 |
<<
  \tag #'(corno1 corni) { re''1~ | re''4 }
  \tag #'(corno2 corni) { sol'1~ | sol'4 }
>> r4 r2 |
R1*2 | 
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol'4 }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol4 }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'2.\fermata
  }
  \tag #'(corno2 corni) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol2.\fermata
  }
  { s1*4 | s1\pp }
>> r4 |
%%
r2 <>\f <<
  \tag #'(corno1 corni) {
    do''2 |
    sol'8 sol' sol' sol' sol'4
  }
  \tag #'(corno2 corni) {
    do'2 |
    sol8 sol sol sol sol4
  }
>> r4 |
r2 <<
  \tag #'(corno1 corni) {
    do''2 |
    sol'8 sol' sol' sol' sol'4
  }
  \tag #'(corno2 corni) {
    do'2 |
    sol8 sol sol sol sol4
  }
>> r4 |
\ru#4 {
  <>\sfp <<
    \tag #'(corno1 corni) { do''1 }
    \tag #'(corno2 corni) { do' }
  >>
}
r2 <>\sf <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>>
r2 <>\sf <<
  \tag #'(corno1 corni) { re'' }
  \tag #'(corno2 corni) { sol' }
>>
r2 <>\sf <<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { mi' }
>>
R1 |
r4 <>\f <<
  \tag #'(corno1 corni) { sol'4 sol' sol' | sol'2 }
  \tag #'(corno2 corni) { sol4 sol sol | sol2 }
>> r2 |
R1*3 |
r2 <<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { mi' }
>>
R1*7 |
r2 <<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { mi' }
>>
R1*6 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do''~ | do''~ | do''4 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'~ | do'4 }
>> r4 r2 |
R1*11 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do''~ | do''~ | do''4 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'~ | do'4 }
>> r4 r2 |
R1*8 |
<>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { mi' }
>> r2 |
R1 |
<<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { mi' }
>> r2 |
R1 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 | re''1 | }
  { do''2~ | do'' sol' | }
  { s2\p\cresc }
>>
<>\f <<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''4 do'' do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    mi'1~ |
    mi'~ |
    mi'4 do' do' do' |
    do'2
  }
>> r2 |
