\piecePartSpecs
#`((oboi #:score-template "score-oboi")
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in C")
   (trombe #:tag-global ()
           #:score-template "score-trombe"
           #:instrument "Trombe in C")
   (timpani)
   (violino1)
   (violino2)
   (viola)
   (basso))
