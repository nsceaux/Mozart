\clef "bass" r2 do\f |
r do |
R1*8 |
r2 do4 do |
do r r2 |
R1*2 |
r2 sol, |
do4 r r sol, |
do do8. do16 do4 r |
R1*32 |
sol,4 r r2 |
R1*2 |
sol,4 r r2 |
R1 |
r2 r\fermata |
R1*6 |
sol,4 sol, sol, sol, |
sol, r r2 |
R1*11 |
r2 r4\fermata r |
%%
r2 do4 r |
sol, r r2 |
r do4 r |
sol, r r2 |
\ru#4 { do4 r r2 | }
r2 do4 r |
r2 sol,4 r |
r2 do4 r |
R1 |
r4 sol, sol, sol, |
sol,2 r |
R1*45 |
do2 r |
R1 |
do2 r |
R1*2 |
r2 sol, |
do4 r do do |
do r do do |
do do do do |
do2 r |
