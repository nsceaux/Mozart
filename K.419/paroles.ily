No, no, no, che non sei ca -- pa -- ce
di cor -- te -- sia, d’o -- no -- re,
di cor -- te -- sia, d’o -- no -- re,
e van -- ti a tor -- to un co -- re,
ch’ar -- de d’a -- mor,
ch’ar -- de d’a -- mor per me,
non sei ca -- pa -- ce
di cor -- te -- si -- a, __ d’o -- no -- re,
e van -- ti a tor -- to un co -- re,
ch’ar -- de __ per me,

No, no, no, che non sei ca -- pa -- ce,
no, che non sei ca -- pa -- ce
di cor -- te -- sia, d’o -- no -- re,
non sei ca -- pa -- ce. __

Van -- ne: t’a -- bor -- ro, in -- gra -- to,
t’a -- bor -- ro, in -- gra -- to,
e più me stes -- so ab -- bor -- ro,
e più me stes -- so ab -- bor -- ro,
me stes -- so, me stes -- so,
me stes -- so ab -- bor -- ro,
che t’ho un i -- stan -- te a -- ma -- to,
che t’ho un i -- stan -- te a -- ma -- to,
che __ sos -- pi -- rai per te,
che sos -- pi -- rai per te,
che t’ho un i -- stan -- te a -- ma -- to,
che t’ho un i -- stan -- te a -- ma -- to,
che __ sos -- pi -- rai per te,
che sos -- pi -- rai per te,
che __ sos -- pi -- ra -- i,
che sos -- pi -- rai per te,
che __ sos -- pi -- ra -- i,
che sos -- pi -- rai per te,
che sos -- pi -- rai per te,
che sos -- pi -- rai per te,
che sos -- pi -- rai per te.
