\piecePartSpecs
#`((clarinetti #:tag-global clarinetti
               #:score-template "score-clarinetti"
               #:instrument , #{ \markup\center-column { Clarinetti in B } #})
   (fagotti #:tag-global all #:score-clarinetti "score-fagotti")
   (corni #:tag-global ()
          #:score-template "score-corni"
          #:instrument "Corni in Es")
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
