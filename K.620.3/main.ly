\version "2.19.80"
\include "common.ily"

\opusTitle "K.620 – N°3 – Dies Bildnis ist bezaubernd schön"

\header {
  title = \markup\center-column {
    \line\italic { Die Zauberflöte }
    \line { N°3 – Aria: \italic { Dies Bildnis ist bezaubernd schön } }
  }
  opus = "K.620"
  date = "1791"
  copyrightYear = "2019"
}

\includeScore "K.620.3"
