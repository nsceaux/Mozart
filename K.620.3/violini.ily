\clef "treble"
<<
  \tag #'violino1 {
    r32 sib'\p |
    sib'8.( sol'16) sol'8-! r16. mib''32 |
    mib''8.( sib'16) sib'8-! r |
    sol''4 r |
    lab' r |
    fa'' r |
    <sol' sib> r |
    r8 do''( reb'' do'') |
    r sib'( la' sib') |
    <fa' re''>4\sfp~ q8 r |
    r mib'' r do'' |
    r sib' r re'' |
    r16 mib''( do'' la') r fa''( re'' sib') |
    sol''8-! sib'-! r lab'! |
    r sol' r fa' |
    mib'-! sol'-! mib'-! r |
    r fa' r fa' |
    r fa'( sol') r |
    r fa' r fa' |
    r fa' r sol' |
    r fa' r fa' |
    r re''~ re''16.([ sib'32]) mib''16.([ do''32]) |
    fa''8-! sib'-! r sib' |
    r sib''\mf~ sib''32([ la'' re''' do'''] sib''[ la'' sol'' fa'']) |
    mi''8-!\p sib'-! r mi' |
    fa'4 r |
    r r8 r16 do''(\mf |
    fa''4) r8 r16 sol''\p |
    \grace sol''8 sib'8. sib'16 sib'8( re''16 do'') |
    sib'8 fa''([\sfp lab''! re'']) |
    r mib''( lab'' re'') |
    r mib''( lab'' re'') |
    mib''8.\([\f \appoggiatura { fa''32[ mib'' re'' mib''] } fa''16]\) sol''8-! r |
    fa'4.\(\p \appoggiatura { sol'32[ fa' mi' fa'] } sol'16. la'32\) |
    sib'16-! sib'8( re''32\cresc do'' mib''[ re'' fa'' mib''] sol''[ fa'' lab''! sol'']) |
    sib''32(\f lab'') r16 sol''32(\p fa'') r16 mib''32( re'') r16 do''32( sib') r16 |
    r16  sib'8( re''32\cresc do'' mib''[ re'' fa'' mib''] sol''[ fa'' lab'' sol'']) |
    sib''32(\f lab'') r16 sol''32(\p fa'') r16 mib''32( re'') r16 do''32( sib') r16 |
    r16  sib'8( re''32\cresc do'' mib''[ re'' fa'' mib''] sol''[ fa'' lab'' sol'']) |
    sib''32(\f lab'') r16 sol''32(\p fa'') r16 mib''32( re'') r16 do''32( sib') r16 |
    reb''32( do'') r16 fa''32( mi'') r16 sol''32( fa'') r16 si''32( do''') r16 |
    dob''32( sib') r16 mib''!32( re''!) r16 fa''32( mib'') r16 la''32( sib''!) r16 |
    la'32( do''!) r16 do''32( mib'') r16 mib''32( solb'') r16 solb''32( mib'') r16 |
    re''32( sib') r16 mib''32( la') r16 fa''32( lab') r16 lab''32( fa'') r16 |
    R2 |
    r16 mib''(\p fa'' sol'') lab''16.([ fa''64 re''] sib'16) r16 |
    r16 mib''( fa'' sol'') lab''16.([ fa''64 re''] sib'16) r16 |
    r16 mib''( fa''\cresc sol'') lab''16.( fa''64 re'' sib'16) r |
    r mib''( sol'' mib'' lab'' fa'' mib'' re'') |
    mib''16.(\f reb''32) reb''8 r4 |
    do''8.(\p re''!16 mi'' fa'' sol'' lab'') |
    \appoggiatura mib''!4 re''4. r8 |
    r mib'' r do'' |
    r sib' r re'' |
    r16 mib''( do'' la') r fa''( re'' sib') |
    sol''8-! sib'-! r lab' |
    r sol' r fa' |
    r16 sib'( sol'' mib'') r do''( mib'' do'') |
    sib'16( mib'') sol'( sib') sib'( lab') sol'( fa') |
    r sib'( sol'' mib'') r do''(\cresc lab'' fa'') |
    mib''( sol'') sol''( sib'') sib''( lab'') sol''( fa'') |
    mib''\f sib'( sib''8)~ sib''16( lab'' fa'' re'') |
    mib''-! sol'\p( sib'8)~ sib'16( lab' fa' re') |
    fa'4( mib'8) r |
  }
  \tag #'violino2 {
    r32 sol'\p |
    sol'8.( mib'16) mib'8-! r16. sib'32 |
    sib'8.( sol'16) sol'8-! r |
    sib'4 r |
    fa' r |
    lab' r |
    <mib' sol>4 r |
    r8 do'( reb' do') |
    r sib( la sib) |
    <sib lab'!>4\sfp~ q8 r |
    r sib' r lab' |
    r sol' r lab' |
    solb'4( fa') |
    mib'8-! sol'!-! r fa' |
    r mib' r re' |
    mib'-! sib-! sol-! r |
    r re' r do' |
    r re'( mib') r |
    r re' r do' |
    r re' r mib' |
    r re' r do' |
    r fa'16.( re'32) sib8( la) |
    sib-! fa'-! r fa' |
    r mi'4( sol'8) |
    sib'8-! mi'-! r sib |
    la4 r |
    r r8 r16 la'\mf( |
    sib'4) r8 r16 sib'\p |
    re'4.( mib'8) |
    re' re'([\sfp fa' sib']) |
    r mib'8( lab' re') |
    r mib'( lab' re') |
    sib'4\f( reb''8) r |
    fa'4(\p mi'8 mib' |
    re'16) sib'8( la'16\cresc lab' sol' fa' mib') |
    re'16\f r fa'\p r lab' r fa' r |
    re'16-! sib'8( la'16\cresc lab' sol' fa' mib') |
    re'16\f r fa'\p r lab' r fa' r |
    re'16-! sib'8( la'16\cresc lab' sol' fa' mib') |
    re'16\f r fa'\p r lab' r fa' r |
    sol' r sib' r do'' r lab' r |
    fa' r lab' r sib' r sol' r |
    mib' r solb' r la' r do'' r |
    sib' r la' r lab' r fa'' r |
    R2 |
    mib'4(\p re'8 fa') |
    mib'4( re'8 fa') |
    mib'4*1/2( s8\cresc re'8 fa') |
    mib'4( re'8 fa') |
    mib'8(\f sib') r4 |
    do'2\p |
    lab'4. r8 |
    r sib' r lab' |
    r sol' r lab' |
    solb'4( fa') |
    mib'8-! sol'!-! r fa' |
    r mib' r re' |
    r16 sol'( sib' sol') r lab'( do'' lab') |
    sol'16( sib') mib'( sol') sol'( fa') mib'( re') |
    r sol'( sib' sol') r lab'(\cresc do'' lab') |
    sol'( sib') mib''( sol'') sol''( fa'') mib''( re'') |
    mib''8\f[ r16 sol']( fa'8.) lab'16( |
    sol'8)[ r16 mib'16](\p re'8.) fa'16 |
    re'4( mib'8) r |
  }
>>
