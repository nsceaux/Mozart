\clef "bass" r16 |
mib4\p r |
mib r |
mib r |
r8 sib,-.( sib,-. sib,-.) |
sib,4 r |
r8 mib-.( mib-. mib-.) |
r8 lab( sol lab) |
r sol( fad sol) |
sib,4\sfp~ sib,8 r |
sol, r lab, r |
sib, r sib, r |
do4( re) |
mib8 r lab r |
sib r sib, r |
mib-! mib-! mib-! r |
sib r fa r |
sib, r mib r |
sib r fa r |
sib, r mib r |
sib r fa r |
sib, r sib,-! do-! |
re r re r |
sol r sol r |
sol r sol r |
fa4 r |
r r8 r16 mib(\mf |
re4) r8 r16 mib(\p |
fa2) |
sol8( lab!4.)\sfp |
r8 sol( fa lab) |
r sol( fa lab) |
sol8.(\f fa16) mi8-! r |
fa2\p |
sib,16 sib, sib, sib,\cresc sib, sib, sib, sib, |
sib,\f r sib,\p r sib, r sib, r |
sib, sib, sib,\cresc sib, sib, sib, sib, sib, |
sib,\f r sib,\p r sib, r sib, r |
sib, sib, sib,\cresc sib, sib, sib, sib, sib, |
sib,\f r sib,\p r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r do r re r re' r |
R2 |
<>\p \ru#2 { mib16-.( mib-. mib-. mib-.) } |
<<
  \ru#6 { mib16-. mib-. mib-. mib-. }
  { s2 s8 s4\cresc }
>> mib8(\f sol) r4 |
lab,2\p |
sib,4. r8 |
sol, r lab, r |
sib, r sib, r |
do4( re) |
mib8 r lab r |
sib r sib, r |
mib r lab r |
sib r sib, r |
mib r lab\cresc r |
sib sib sib, sib, |
mib4\f sib |
mib'8 r sib\p r |
sib,4( mib8) r |
