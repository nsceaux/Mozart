\clef "treble" \transposition mib
r16 |
<<
  \tag #'(corno1 corni) {
    do''4 s |
    mi'' s |
  }
  \tag #'(corno2 corni) {
    mi'4 s |
    sol' s |
  }
  { s4^"ten." \p r | s^"ten." r | }
>>
R2 |
r8 <<
  \tag #'(corno1 corni) { sol'8-.( sol'-. sol'-.) | sol'4 }
  \tag #'(corno2 corni) { sol8-.( sol-. sol-.) | sol4 }
>> r4 |
r8 \twoVoices #'(corno1 corno2 corni) <<
  { do'8-.( do'-. do'-.) | do'2~ | do' | }
  { do'8-.( do'-. do'-.) | do'2~ | do' | }
>>
<>\fp <<
  \tag #'(corno1 corni) { sol'4~ sol'8 }
  \tag #'(corno2 corni) { sol4~ sol8 }
>> r8 |
R2*2 |
<>\p <<
  \tag #'(corno1 corni) { do''4( sol') | }
  \tag #'(corno2 corni) { do'4( sol) | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do'8 } { do' }
>> r8 r4 |
R2*5 |
r8 <<
  \tag #'(corno1 corni) { re''8( mi'') }
  \tag #'(corno2 corni) { sol'8( do'') }
>> r8 |
R2 |
r8 \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 re''8 | re'' }
  { sol'4 re''8 | sol' }
>> r8 r4 |
R2*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2~ | re''~ | re''4 }
  { re''2~ | re'' | sol'4 }
  { s2\p | s\cresc | s4\f }
>> r4 |
R2 |
r8 <>\fp <<
  \tag #'(corno1 corni) { sol'4.~ | sol'2~ | sol'~ | sol'4~ sol'8 }
  \tag #'(corno2 corni) { sol4.~ | sol2~ | sol~ | sol4~ sol8 }
  { s4. s2*2 | s4\f }
>> r8 |
R2 |
<<
  \tag #'(corno1 corni) {
    sol'2~ | sol'16 sol' s sol' s sol' s sol' |
    sol'2~ | sol'16 sol' s sol' s sol' s sol' |
    sol'2~ | sol'16 sol' s sol' s sol' s sol' |
  }
  \tag #'(corno2 corni) {
    sol2~ | sol16 sol s sol s sol s sol |
    sol2~ | sol16 sol s sol s sol s sol |
    sol2~ | sol16 sol s sol s sol s sol |
  }
  { s8.\p s16\cresc s4 | s16\f s\p r s r s r s |
    s8. s16\cresc s4 | s16\f s\p r s r s r s |
    s8. s16\cresc s4 | s16\f s\p r s r s r s | }
>>
R2 |
<<
  \tag #'(corno1 corni) {
    s16 sol' s sol' s sol' s sol' |
    s sol' s sol' s sol' s sol' |
    s sol' s do'' s sol' s sol' |
  }
  \tag #'(corno2 corni) {
    s sol s sol s sol s sol |
    s sol s sol s sol s sol |
    s sol s do' s sol s sol |
  }
  \ru#3 { r16 s r s r s r s | }
>>
R2 |
\tag#'corni <>^"a 2." do'2\p~ |
do'~ |
do'~ |
do'\cresc~ |
do'8\f do'-! r4 |
R2*4 |
<>\p <<
  \tag #'(corno1 corni) { do''4( sol') | }
  \tag #'(corno2 corni) { do'4( sol) | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do'8 } { do' }
>> r8 r4 |
R2*3 |
<<
  \tag #'(corno1 corni) {
    sol''4( fa'') |
    mi''8.( sol''16) sol''( fa'' mi'' re'') |
    do''4 re'' |
    mi''8
  }
  \tag #'(corno2 corni) {
    mi''4( re'') |
    do''8.( mi''16) mi''( re'' do'' sol') |
    mi'4 sol' |
    do''8
  }
  { s2\p | s8. s16\cresc s4 | s\f }
>> r8 r4 |
<>\p <<
  \tag #'(corno1 corni) { re''4( do''8) }
  \tag #'(corno2 corni) { sol'4( do'8) }
>> r8 |
