\clef "bass" r16 |
<<
  \tag #'(fagotto1 fagotti) { sib4 s | mib' s | }
  \tag #'(fagotto2 fagotti) { mib s | sol s | }
  { s4^"ten." \p r | s^"ten." r | }
>>
R2 |
r8 <<
  \tag #'(fagotto1 fagotti) { re'8-.( re'-. re'-.) | re'4 }
  \tag #'(fagotto2 fagotti) { lab8-.( lab-. lab-.) | lab4 }
>> r4 |
r8 <<
  \tag #'(fagotto1 fagotti) { sib8-.( sib-. sib-.) | }
  \tag #'(fagotto2 fagotti) { sol-.( sol-. sol-.) | }
>>
R2*2 |
<>\fp <<
  \tag #'(fagotto1 fagotti) { lab4~ lab8 }
  \tag #'(fagotto2 fagotti) { fa4~ fa8 }
>> r8 |
R2*9 |
r8 <<
  \tag #'(fagotto1 fagotti) { fa8( sol) }
  \tag #'(fagotto2 fagotti) { sib,8( mib) }
>> r8 |
R2 |
r8 <<
  \tag #'(fagotto1 fagotti) { fa'4( mib'8) | re' }
  \tag #'(fagotto2 fagotti) { re'4( do'8) | sib }
>> r8 r4 |
R2*2 |
r4 r8 r16 <<
  \tag #'(fagotto1 fagotti) {
    do'16( |
    re'8) s16 re'( mib'8) s16 mib'( |
    fa'4)
  }
  \tag #'(fagotto2 fagotti) {
    la16( |
    sib8) s16 sib( do'8) s16 do'( |
    re'4)
  }
  { s16\p | s8\cresc r16 s s8 r16 s\f | }
>> r4 |
R2 |
r8 <>\fp <<
  \tag #'(fagotto1 fagotti) { fa'4. }
  \tag #'(fagotto2 fagotti) { re' }
>>
R2*5 |
\clef "tenor"
<<
  \tag #'(fagotto1 fagotti) { s16 fa' s fa' s fa' s fa' | }
  \tag #'(fagotto2 fagotti) { s re' s re' s re' s re' | }
  { r16 s\p r s r s r s | }
>>
R2 |
<<
  \tag #'(fagotto1 fagotti) { s16 fa' s fa' s fa' s fa' | }
  \tag #'(fagotto2 fagotti) { s re' s re' s re' s re' | }
  { r16 s r s r s r s | }
>>
R2 |
<<
  \tag #'(fagotto1 fagotti) { s16 fa' s fa' s fa' s fa' | }
  \tag #'(fagotto2 fagotti) { s re' s re' s re' s re' | }
  { r16 s r s r s r s | }
>>
\clef "bass"
<<
  \tag #'(fagotto1 fagotti) {
    s16 sib s mi' s fa' s do' |
    s lab s re' s mib'! s sib |
    s solb' s solb' s solb' s solb' |
    s fa' s mib' s fa' s lab |
  }
  \tag #'(fagotto2 fagotti) {
    s sol s sib s lab! s lab |
    s fa s lab s sol s sol |
    s mib' s mib' s mib' s mib' |
    s re' s la s lab s fa |
  }
  \ru#4 { r16 s r s r s r s | }
>>
R2*4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib2~ | sib8 mib'-! }
  { sol4( lab) | sol8-! sib-! }
  { s2\p\cresc | s4\f }
>> r4 |
R2*9 |
<<
  \tag #'(fagotto1 fagotti) {
    sol'4( fa') |
    mib'8.( sol'16) sol'( fa' mib' re') |
    mib'8[ s16 mib']( re'8.) fa'16( |
    mib'8)
  }
  \tag #'(fagotto2 fagotti) {
    sib4( lab) |
    sol8.( sib16) sib( lab sol fa) |
    sol8[ s16 sol]( fa8.) lab16( |
    sol8)
  }
  { s2\p | s8. s16\cresc s4 | s8\f r16 }
>> r8 r4 |
<>\p <<
  \tag #'(fagotto1 fagotti) { re'4( mib'8) }
  \tag #'(fagotto2 fagotti) { lab4( sol8) }
>> r8 |
