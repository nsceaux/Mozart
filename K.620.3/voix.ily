\clef "G_8" r16 |
R2 r4 r8 sib |
sol'8. fa'16 mib' re' do' sib |
lab4 r8 lab |
fa'8. mib'16 re' do' sib lab |
sol4 r8 mib' |
si8 do' r mib' |
la sib! r sib16 sib |
lab'!8. fa'16 re'8 r16 sib |
mib'4~ mib'16([ fa'32 mib' re'16]) do' |
\appoggiatura do'8 sib8. sib16 sib8 do'16[ re'] |
mib'4 r8 sib |
sol'4~ sol'16([ fa']) lab'([ fa']) |
\appoggiatura fa'8 mib'8. mib'16 mib'8 sol'16([ fa']) |
mib'4 r |
R2 |
r4 r8 sol'16.([ fa'32]) |
fa'8. mib'32[ re'] do'16 do' re' mib' |
dod'8 re' r mib'16.([ sol'32]) |
\appoggiatura sol'8 fa'8. mib'32[ re'] do'!16 do' re' mib' |
dod'8 re' r4 |
sib8 sib16 sib sib([ re']) re'([ fa']) |
fa' mi' mi'8 r4 |
sol'16([ sib]) sib sib sib([ do'32 sib la16]) sib |
re'16 do' do'8 r4 |
r r8 r16 do' |
fa'4 r8 r16 sol' |
\appoggiatura sol'8 sib8. sib16 sib8 re'16([ do']) |
sib4 r8 sib |
fa' mib' r sib |
fa' mib' r sib |
mib'8.\([ \appoggiatura { fa'32[ mib' re' mib'] } fa'16]\) sol'8 r |
fa4. sol16([ la]) |
sib4 r |
R2 |
sib8 sib16 do' re' mib' fa' sol' |
lab'8.([ fa'16]) re'8 r |
sib8 sib16 do' re' mib' fa' sol' |
lab'8.([ fa'16]) re'8 r |
r r16 do' do'([ fa']) fa'8 |
r4 sib16([ mib']) mib'8 |
r8 solb'4 mib'8 |
re' r16 do' fa'16. lab!32 lab8 |
R2 |
r4 r16 sib do' re' |
fa'([ mib']) r8 r16 sib([ do']) re' |
fa'([ mib']) mib'8 r16 sib do' re' |
mib'[ sib] sol'[ mib'] lab'[ fa'] mib'[ re'] |
mib'16.([ reb'32]) reb'8 r reb' |
do'8. re'!16 mi' fa' sol' lab' |
\appoggiatura mib'!4 re' r8 sib |
mib'4~ mib'16([ fa'32 mib' re'16]) do' |
\appoggiatura do'8 sib8. sib16 sib8 do'16([ re']) |
mib'4 r8 sib |
sol'4~ sol'16([ fa']) lab'([ fa']) |
\appoggiatura fa'16 mib'8. mib'16 mib'8 re' |
sol'8.([ mib'16]) mib'8. do'16 |
sib16([ mib']) sol([ sib]) sib([ lab]) sol([ fa]) |
sol'8.([ mib'16]) lab'8. fa'16 |
mib'([ sib]) mib'([ sol']) sol'([ fa']) mib'([ re']) |
mib'4 r |
R2*2 |
