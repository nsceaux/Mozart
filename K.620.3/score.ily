\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        instrumentName = \markup\center-column { Clarinetti in B }
        shortInstrumentName = "Cl."
      } <<
        \keepWithTag #'clarinetti \global
        \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corni in Es }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Tamino
      shortInstrumentName = \markup\character Ta.
    } \withLyrics <<
      \keepWithTag #'all \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncello e Basso }
      shortInstrumentName = \markup\center-column { Vlc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global \includeNotes "basso"
      \origLayout {
        s16 s2*6\break s2*7\pageBreak
        \grace s8 s2*6\break \grace s8 s2*5\pageBreak
        s2*8\break s2*5\pageBreak
        s2*4\break s2*5\pageBreak
        s2*4\break \grace s4 s2*7\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
