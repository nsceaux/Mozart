\clef "treble" \transposition sib
r16 |
<<
  \tag #'(clarinetto1 clarinetti) {
    do''4 s |
    fa'' s |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'4 s |
    do'' s |
  }
  { s4^"ten." \p r | s4^"ten." r | }
>>
R2 |
r8 <<
  \tag #'(clarinetto1 clarinetti) { sib'8-.( sib'-. sib'-.) | sib'4 }
  \tag #'(clarinetto2 clarinetti) { sol'8-.( sol'-. sol'-.) | sol'4 }
>> r4 |
r8 <<
  \tag #'(clarinetto1 clarinetti) { la'-.( la'-. la'-.) | }
  \tag #'(clarinetto2 clarinetti) { fa'-.( fa'-. fa'-.) | }
>>
R2*2 |
<>\fp <<
  \tag #'(clarinetto1 clarinetti) { sib''4~ sib''8 }
  \tag #'(clarinetto2 clarinetti) { mi''4~ mi''8 }
>> r8 |
R2*2 |
r4 <>\p <<
  \tag #'(clarinetto1 clarinetti) { sib'4( | la'8) }
  \tag #'(clarinetto2 clarinetti) { sol'4( | fa'8) }
>> r8 r4 |
R2 |
r4 r8 <<
  \tag #'(clarinetto1 clarinetti) {
    la''16.( sol''32) |
    sol''8.( fa''32 mi'') re''16-! re''( mi'' fa'') |
    red''8( mi'')
  }
  \tag #'(clarinetto2 clarinetti) {
    fa''16.( mi''32) |
    mi''8.( re''32 do'') si'16-! si'( do'' re'') |
    si'8( do'')
  }
>> r4 |
R2 |
r8 <<
  \tag #'(clarinetto1 clarinetti) { sol''8( la'') }
  \tag #'(clarinetto2 clarinetti) { mi''8( fa'') }
>> r8 |
R2 |
r8 <<
  \tag #'(clarinetto1 clarinetti) { sol''4( si''8) | do''' }
  \tag #'(clarinetto2 clarinetti) { mi''4( fa''8) | sol'' }
>> r8 r4 |
R2*2 |
r4 r8 r16 <<
  \tag #'(clarinetto1 clarinetti) {
    re''16( |
    mi''8) s16 mi''( fa''8) s16 fa''( |
    sol''4)
  }
  \tag #'(clarinetto2 clarinetti) {
    si'16( |
    do''8) s16 do''( re''8) s16 re''( |
    mi''4)
  }
  { s16\p | s8\cresc r16 s s8 r16 s\f | }
>> r4 |
R2 |
r8 <>\fp <<
  \tag #'(clarinetto1 clarinetti) { mi''4. }
  \tag #'(clarinetto2 clarinetti) { do'' }
>>
R2*5 |
<<
  \tag #'(clarinetto1 clarinetti) { s16 sol'' s sol'' s sol'' s sol'' | }
  \tag #'(clarinetto2 clarinetti) { s mi'' s mi'' s mi'' s mi'' | }
  { r16 s\p r s r s r s | }
>>
R2 |
<<
  \tag #'(clarinetto1 clarinetti) { s16 sol'' s sol'' s sol'' s sol'' | }
  \tag #'(clarinetto2 clarinetti) { s mi'' s mi'' s mi'' s mi'' | }
  { r16 s r s r s r s | }
>>
R2 |
<<
  \tag #'(clarinetto1 clarinetti) {
    s16 sol'' s sol'' s sol'' s sol'' |
    s do''' s do''' s sib''! s sib'' |
    s sib'' s sib'' s la'' s la'' |
    s si'' s si'' s si'' s si'' |
    s do''' s si'' s sib'' s sib'' |
  }
  \tag #'(clarinetto2 clarinetti) {
    s mi'' s mi'' s mi'' s mi'' |
    s la'' s la'' s sol'' s sol'' |
    s sol'' s sol'' s fa'' s fa'' |
    s re'' s re'' s re'' s re'' |
    s mi'' s fa'' s sol'' s sol''
  }
  \ru#5 { r16 s r s r s r s | }
>>
R2*4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''4( mi'') | fa''16.( mib''32) mib''8-! }
  { la'4( sol') | fa'8-! do''-! }
  { s2\p\cresc | s4\f }
>> r4 |
R2*4 |
r4 <>\p <<
  \tag #'(clarinetto1 clarinetti) { sib'4( | la'8) }
  \tag #'(clarinetto2 clarinetti) { sol'4( | fa'8) }
>> r8 r4 |
R2*3 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { la''4( sol'') |
    fa''8.( la''16) la''( sol'' fa'' mi'') |
    fa''8[ s16 la'']( sib''8.) sol''16( |
    fa''16) la''(\p do'''8)~ do'''16( sib'' sol'' mi'') |
    sol''4( fa''8) }
  { do''4( sib') |
    la'8.( do''16) do''( sib' la' sol') |
    la'8[ s16 fa'']( sol''8.) mi''16( |
    fa''8) r8 r4 |
    mi''4(\p fa''8) }
  { s2\p | s8. s16\cresc s4 | s8\f r16 s }
>> r8 |
