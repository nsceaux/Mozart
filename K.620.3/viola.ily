\clef "alto" r32 mib'\p |
mib'8.( sib16) sib8-! r16. sol'32 |
sol'8.( mib'16) mib'8-! r |
mib'4 r |
<sib re'> r |
q r |
mib r |
r8 lab( sol lab) |
r sol( fad sol) |
<sib re'>4\sfp~ q8 r |
r mib' r mib' |
r mib' r fa' |
la4( sib)~ |
sib8 mib'-! r do' |
r sib r lab! |
sol-! mib'-! mib-! r |
r sib r la |
r sib sib r |
r sib r la |
r sib r sib |
r sib r la |
r sib4( do'8) |
re'-! re'-! r re' |
r8 sib4( mi'8) |
sol'-! sol-! r sol |
fa4 r |
r r8 r16 mib'(\mf |
re'4) r8 r16 do'\p |
fa'4.( la'8) |
sib' sib([\sfp re' fa']) |
r sib( re' fa') |
r sib( re' fa') |
mib'8.(\f re'16) sib'8-! r |
re'4(\p dod'8 do' |
sib16) re'8( mib'16\cresc fa' mib' re' do') |
sib\f r re'\p r fa' r re' r |
sib16-! re'8( mib'16\cresc fa' mib' re' do') |
sib\f r re'\p r fa' r re' r |
sib16-! re'8( mib'16\cresc fa' mib' re' do') |
sib\f r re'\p r fa' r re' r |
mi'16 r sol' r lab'! r fa' r |
re' r fa' r sol' r mib'! r |
solb r mib' r solb' r la' r |
fa' r mib' r sib r sib' r |
R2 |
<<
  \ru#4 { sol32([ sib sol sib] sol[ sib sol sib] fa[ sib fa sib] lab[ sib lab sib]) | }
  { s2\p | s | s8 s4.\cresc | }
>>
sol8(\f mib') r4 |
fa2\p |
fa'4. r8 |
r mib' r mib' |
r mib' r fa' |
la4( sib) |
sib8-! mib'-! r do' |
r sib r lab! |
mib' r lab' r |
sib' r sib r |
mib' r lab'\cresc r |
sib' sib' sib sib |
mib'\f r16 mib'( re'8.) fa'16( |
mib'8) r16 sol16(\p fa8.) lab16-! |
lab4( sol8) r |
