\clef "treble" \transposition la
R1*113 |
R1*3 |
<>\f <<
  \tag #'(clarinetto1 clarinetti) { lab''1 | sol'' | }
  \tag #'(clarinetto2 clarinetti) { re''1 | mi'' | }
>>
R1*5 |
r2 <>\f <<
  \tag #'(clarinetto1 clarinetti) { sol''2( | la''!4) }
  \tag #'(clarinetto2 clarinetti) { mi''2( | fa''4) }
>> r4 r2 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    sol''2 |
    la'' do'' |
    re'' do'' |
    mi''1 |
    fa''2 s4 fa'' |
    mi''2 r4 mi'' |
    fa''2 s4 fa'' |
    mi''2 s4 mi'' |
    fa''2 s4 fa'' |
    mi''1 |
    fa''4
  }
  \tag #'(clarinetto2 clarinetti) {
    mi''2 |
    fa'' la' |
    sib' la' |
    sol'1 |
    la'2 s4 la' |
    sol'2 s4 sol' |
    la'2 s4 la' |
    sol'2 s4 sol' |
    la'2 s4 la' |
    sol'1 |
    la'4
  }
  { s2 |
    s1*3 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag#'clarinetti <>^"a 2." do''4 la' fa' |
do'' do'' do'' r |
fa'2\p~ fa'8 do' la do' |
fa'2~ fa'8 la' do'' la' |
\twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'8 sol' la' sib' }
  { fa' mi' fa' sol' }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    do''8 re'' mi'' fa'' |
    fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'8 sib' sol' la' |
    la'( sol') sib'-. la'-. la'( sol') fa'-. mi'-. |
  }
>>
\tag#'clarinetti <>^"a 2." fa'2~ fa'8 do' la do' |
fa'2~ fa'8 la' do'' la' |
\twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'8 sol' la' sib' }
  { fa' mi' fa' sol' }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    do''8 re'' mi'' fa'' |
    fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'8 sib' sol' la' |
    la'( sol') sib'-. la'-. la'( sol') fa'-. mi'-. |
  }
>>
\tag#'clarinetti <>^"a 2." fa'2~ fa'8 do' la do' |
fa'2~ fa'8 la' do'' la' |
\twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'8 sol' la' sib' }
  { fa' mi' fa' sol' }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    do''8 re'' mi'' fa'' |
    fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
  }
  \tag #'(clarinetto2 clarinetti) {
    la'8 sib' sol' la' |
    la'( sol') sib'-. la'-. la'( sol') fa'-. mi'-. |
  }
>>
<>\f \twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { fa'2 } { fa' }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    do''2 |
    re'' do'' |
    mi''1 |
    fa''4
  }
  \tag #'(clarinetto2 clarinetti) {
    la'2 |
    sib' la' |
    sol'1 |
    la'4
  }
>> r4 r2 |
R1*3 |
<<
  \tag #'(clarinetto1 clarinetti) {
    fa'1~ | fa' | la' | do'' |
    fa'' | mi'' | fa''4
  }
  \tag #'(clarinetto2 clarinetti) {
    do'1~ | do' | fa' | la' |
    la' | sol' | la'4
  }
  { s4\p s2.\cresc | s1 | s\f | s1*3 | }
>> r4 r2 |
R1*3 |
<<
  \tag #'(clarinetto1 clarinetti) {
    fa'1~ | fa' | la' | do'' |
    fa'' | mi'' |
    fa''2 s4 fa'' |
    mi''2 s4 mi'' |
    fa''2 s4 fa'' |
    mi''2 s4 mi'' |
    fa''2 s4 fa'' |
    mi''2 s4 mi'' |
    fa''1~ |
    fa'' |
    la' |
    la'4 do'' la' do'' |
    la'
  }
  \tag #'(clarinetto2 clarinetti) {
    do'1~ | do' | fa' | la' |
    la' | sol' |
    la'2 s4 la' |
    sol'2 s4 sol' |
    la'2 s4 la' |
    sol'2 s4 sol' |
    la'2 s4 la' |
    sol'2 s4 sol' |
    la'1~ |
    la' |
    fa' |
    fa'4 la' fa' la' |
    fa'
  }
  { s4\p s2.\cresc | s1 | s\f | s1*3 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag#'clarinetti <>^"a 2." fa'8 fa' fa'4 fa' |
fa' do' fa' do' |
fa'2 r4 fa'8 fa' |
fa'2 r4 fa'8 fa' |
fa'2 r |
