\version "2.19.80"
\include "common.ily"

\opusTitle "K.482 – N°28 – Gente, gente"

\header {
  title = \markup\center-column {
    \line\italic { Le nozze di Figaro }
    \line { N°28 – Tutti: \italic { Gente, gente } }
  }
  opus = "K.492"
  date = "1786"
  copyrightYear = "2019"
}

\includeScore "K.492.28"
