\clef "bass" sol4\f r r2 |
sol4 r r2 |
sol4 r r2 |
re1\p |
sol4\f r r2 |
sol4 r r2 |
sol4 r r2 |
re1\p |
sol2:8\f sol:8 |
sol:8 sol:8 |
sol4 re sol si |
re'2\sf do'4.(\p la8) |
sol4( si) re'4(\sf do'8 la) |
sol4(\p si) re'4(\sf do'8 la) |
sol8\sf re sol re fad\sf re fad re |
sol8\sf re sol re la\sf re la re |
si4 r r2 |
R1 |
mi2(\p sol) |
si,4 r r2 |
mi2( sol) |
si,4 r si r |
la r sol r |
do r dod r |
re re\f fad re |
la1\p |
r4 re\f fad re |
la1\p |
r4 re\f fad la |
re'2\sf dod'4\p re' |
si2\sf lad4\p si |
fad2:8\sf sold:8\sf |
la!8\f r dod\p r mi r dod r |
la, r re r fad r re r |
la, r mi r sol r mi r |
fad r la r fad r re r |
la, r dod r mi r dod r |
la, r re r fad r re r |
la, r mi r sol r mi r |
fad r la r fad r re r |
la4 la,\cresc dod mi |
la2\f sol4.(\p mi8) |
re4( fad) la4(\sf sol8 mi) |
re4(\p fad) la4(\sf sol8 mi) |
re2*1/2:8\p s4\cresc si2:8 |
sol:8\f la:8 |
re4 r r2 |
re4\p r r2 |
re4 r r2 |
la,1\f |
re4 r r2 |
re4\p r r2 |
re4 r r2 |
la2:8\f la:8 |
re4 r r2 |
r r4 re'\f |
re' re r2 |
r r4 re'\f |
re' re r2 |
r2 r4 do'16(\f si do' re') |
mi'1\sf |
la2\sf do'\sf |
fad\sf la\sf |
do1\sf~ |
do\p( |
si,4) r do r |
re r re r |
sol, r r2 |
sol4\pp r4 r2 |
fa4 r r2 |
fa4 r r2 |
mib4 r r2 |
mib4 r r2 |
re4 r r2 |
re4 r r2 |
do4 r r2 |
do4 r r2 |
fa4 r r2 |
sib,4 r r2 |
mib4 r r2 |
mib4 r r2 |
re4 r r2 |
sol4 r r2 |
re4 r r2 |
sol,4 r r2 |
re2 r4\fermata r |
sol1 |
re2 r |
re1 |
sol,2\fermata r4\fermata r | \allowPageTurn
<>^"Vcl." si2(\p do') |
re'4( mi') r2 |
si2( do') |
re'2.( mi'4) |
do'2( re') |
sol4 r r <>^"Bassi" sol,\p |
si,2( do) |
re4( mi) r mi |
si,2( do) |
re2.*1/3( s2\cresc mi4) |
do2(\p re) |
sol,4 r r sol8.(\f do'16) |
do'1\decresc |
re'2\p re |
sol4 r r sol8.(\f do'16) |
do'2.( la4) |
r la\p la( fad) |
r fad(\f sol la) |
sol sol do do |
re\p re re re |
sol r8 si(\pp sol4) r8 fad( |
mi4) r8 sol( mi4) r8 re( |
dod4) r8 mi( dod4) r8 si,( |
la,2:8)\p la,:8 |
la,:8 la,:8 |
la,:8 la,:8 |
la,:8\f la,:8 |
la,8 la la la la2:8 |
re'2\p fa'4.( re'8) |
la2 re'4.( la8) |
fa4.( la8) fa4.( re8) |
la4 la, la, r |
re1\p |
re4( fad!\sf mi la,) |
re1\p |
re4( fad\sf mi la,) |
re2:8\f re:8 |
sol:8 re:8 |
la:8 la,:8 |
re4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
la2:8 la:8 |
re'8 re' la la fad fad re re |
la4 la, la, r |
re8\p la, re la, re4 r |
re8 la, re la, re4 r |
re8 la, re la, fad re fad re |
la4 la,8 la, la,4 r |
re8 la, re la, re4 r |
re8 la, re la, re4 r |
re8 la, re la, fad re fad re |
la4 la,8 la, la,4 r |
re8 la, re la, re4 r |
re8 la, re la, re4 r |
re8 la, re la, fad re fad re |
la4 la,8 la, la,4 r |
re4\f re8 re re4 r |
sol4 sol8 sol re4 re8 re |
la4 la8 la la,4 la,8 la, |
re la,\p si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si,\cresc dod re mi fad mi |
re la, si, dod re mi fad mi |
re\f la, si, dod re mi fad mi |
re la, si, dod re mi fad re |
la2:8 la:8: |
la,:8 la,:8 |
re8 la,\p si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si,\cresc dod re mi fad mi |
re la, si, dod re mi fad mi |
re\f la, si, dod re mi fad mi |
re la, si, dod re mi fad re |
la2:8 la:8: |
la,:8 la,:8 |
re4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
la2:8 la:8 |
re:8 re:8 |
re:8 re:8 |
re:8 re:8 |
re:8 re:8 |
re4 re8 re re4 re |
re la, re la, |
re2 r4 re8 re |
re2 r4 re8 re |
re2 r |
