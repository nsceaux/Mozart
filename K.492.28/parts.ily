\piecePartSpecs
#`((clarinetti #:tag-global clarinetti
               #:score-template "score-clarinetti"
               #:instrument , #{ \markup\center-column { Clarinetto in A } #})
   (fagotti #:tag-global all #:score-template "score-fagotti")
   (oboi #:tag-global all #:score-template "score-oboi")
   (flauti #:tag-global all #:score-template "score-flauti")
   (corni #:tag-global ()
          #:score-template "score-corni"
          #:instrument "Corni in Es")
   (trombe #:tag-global ()
          #:score-template "score-trombe"
          #:instrument "Trombe in D")
   (timpani #:tag-global ())
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Duetto: TACET } #}))
