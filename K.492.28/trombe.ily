\clef "treble" \transposition re
R1*113 |
%%
<>\p <<
  \tag #'(tromba1 trombe) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4 sol' sol' sol' |
  }
  \tag #'(tromba2 trombe) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol4 sol sol sol |
  }
>>
R1*5 |
<>\f <<
  \tag #'(tromba1 trombe) { do''2 sol' | }
  \tag #'(tromba2 trombe) { do'2 sol | }
>>
R1 |
<<
  \tag #'(tromba1 trombe) {
    do''2 sol' |
    do''1 |
    do''2 do'' |
    sol'1 |
    do''2 s4 do'' |
    re''2 s4 re'' |
    mi''2 s4 do'' |
    re''2 s4 re'' |
    mi''2 s4 do'' |
    re''1 |
    mi''4 sol'' mi'' do'' |
    sol'' sol' sol'
  }
  \tag #'(tromba2 trombe) {
    do'2 sol |
    do'1 |
    do'2 do' |
    sol1 |
    do'2 s4 mi' |
    sol'2 s4 sol' |
    do''2 s4 mi' |
    sol'2 s4 sol' |
    do''2 s4 mi' |
    sol'1 |
    do''4 sol' mi' do' |
    sol' sol sol
  }
  { s1 s\f s1*2 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
  }
>> r4 |
\tag#'trombe <>^"a 2." do'4\p do'8 do' do'4 r |
do'4 do'8 do' do'4 r |
do'4 do'8 do' mi'4 mi'8 mi' |
sol'4 <<
  \tag #'(tromba1 trombe) { sol'8 sol' sol'4 }
  \tag #'(tromba2 trombe) { sol8 sol sol4 }
>> r4 |
\tag#'trombe <>^"a 2." do'4 do'8 do' do'4 r |
do'4 do'8 do' do'4 r |
do'4 do'8 do' mi'4 mi'8 mi' |
sol'4 <<
  \tag #'(tromba1 trombe) { sol'8 sol' sol'4 }
  \tag #'(tromba2 trombe) { sol8 sol sol4 }
>> r4 |
\tag#'trombe <>^"a 2." do'4 do'8 do' do'4 r |
do'4 do'8 do' do'4 r |
do'4 do'8 do' mi'4 mi'8 mi' |
sol'4 <<
  \tag #'(tromba1 trombe) { sol'8 sol' sol'4 }
  \tag #'(tromba2 trombe) { sol8 sol sol4 }
>> r4 |
<>\f <<
  \tag #'(tromba1 trombe) {
    do''2 do''4. do''8 |
    do''2 do'' |
    sol'1 |
    mi'4
  }
  \tag #'(tromba2 trombe) {
    do'2 do'4. do'8 |
    do'2 do' |
    sol1 |
    do'4
  }
>> r4 r2 |
R1*5 |
<>\f <<
  \tag #'(tromba1 trombe) {
    do''2 do''4. do''8 |
    mi''2 do'' |
    sol''1 |
    sol' |
    mi'4
  }
  \tag #'(tromba2 trombe) {
    do'2 do'4. do'8 |
    mi'2 do' |
    sol'1 |
    sol |
    do'4
  }
>> r4 r2 |
R1*5 |
<>\f <<
  \tag #'(tromba1 trombe) {
    do''2 do''4. do''8 |
    mi''2 do'' |
    sol''1 |
    sol' |
    do''2 s4 do'' |
    re''2 s4 re'' |
    mi''2 s4 do'' |
    re''2 s4 re'' |
    mi''2 s4 do'' |
    re''2 s4 re'' |
    do'' s do'' do'' |
    do'' s do'' do'' |
    do'' s do'' do'' |
    do'' do'' do'' do'' |
    do''
  }
  \tag #'(tromba2 trombe) {
    do'2 do'4. do'8 |
    mi'2 do' |
    sol'1 |
    sol |
    do'2 s4 mi' |
    sol'2 s4 sol' |
    do''2 s4 mi' |
    sol'2 s4 sol' |
    do''2 s4 mi' |
    sol'2 s4 sol' |
    mi' s mi' mi' |
    mi' s mi' mi' |
    mi' s mi' mi' |
    mi' mi' mi' mi' |
    mi'
  }
  { s1*4 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s4 r s2 |
    s4 r s2 |
    s4 r s2 | }
>> \tag#'trombe <>^"a 2." do'8 do' do'4 do' |
do' sol do' sol |
do'2 r4 do'8 do' |
do'2 r4 do'8 do' |
do'2 r |
