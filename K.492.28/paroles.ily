% CONTE
\tag #'voix5 {
  Gen -- te! gen -- te! all’ ar -- mi, all’ ar -- mi!
}
% FIGARO (con finto spavento)
\tag #'voix7 {
  Il pa -- dro -- ne!
}
% CONTE
\tag #'voix5 {
  Gen -- te! gen -- te! a -- iu -- to, a -- iu -- to!
}
% (Entrano Antonio, Basilio, Bartolo e Curzio.)

% FIGARO
\tag #'voix7 {
  Son per -- du -- to!
}
% BASILIO, CURZIO, ANTONIO, BARTOLO
\tag #'(voix4 voix6) {
  Cos’ av -- ven -- ne? Cos’ av -- ven -- ne?
}
% CONTE
\tag #'voix5 {
  Il scel -- le -- ra -- to
  m’ha tra -- di -- to, m’ha in -- fa -- ma -- to,
  e con chi sta -- te a ve -- der!
}
% BASILIO, CURZIO, ANTONIO, BARTOLO
\tag #'(voix4 voix6) {
  Son stor -- di -- to, sba -- lor -- di -- to,
  non mi par che ciò sia ver!
}
% FIGARO
\tag #'voix7 {
  Son stor -- di -- ti, sba -- lor -- di -- ti,
  o che sce -- na, che pia -- cer!
}
% (Dal padiglione a sinistra escono in rapida
% successione Cherubino, Barbarina, Marcellina e Susanna.)

% CONTE
\tag #'voix5 {
  In -- van re -- si -- ste -- te,
  u -- sci -- te, ma -- da -- ma!
  il pre -- mio or a -- vre -- te
  di vo -- stra o -- ne -- stà!
  Il pag -- gio!
}
% ANTONIO
\tag #'voix6 {
  Mia fi -- glia!
}
% FIGARO
\tag #'voix7 {
  Mia ma -- dre!
}
% BASILIO, CURZIO, ANTONIO, BARTOLO
\tag #'(voix4 voix6 voix7) {
  Ma -- da -- ma!
}
% CONTE
\tag #'voix5 {
  Sco -- per -- ta è la tra -- ma,
  la per fi -- da è qua,
  la per fi -- da è qua!
}
% SUSANNA (inginocchiandosi)
\tag #'voix1 {
  Per -- do -- no, per -- do -- no!
}
% CONTE
\tag #'voix5 {
  No, no! non spe -- rar -- lo!
}
% FIGARO (inginocchiandosi)
\tag #'voix7 {
  Per -- do -- no, per -- do -- no!
}
% CONTE
\tag #'voix5 {
  no, no! non vuò dar -- lo!
}
% TUTTI SALVO IL CONTE
% (inginocchiandosi)
\tag #'(voix1 voix3 voix4 voix6 voix7) {
  Per -- do -- no!
  per -- do -- no!
  per -- do -- no!
}
% CONTE
\tag #'voix5 {
  no! no! no, no, no, no, no, no!
}
% (La Contessa esce dal padiglione a destra.)

% CONTESSA
\tag #'voix2 {
  Al -- me -- no io per lo -- ro per -- do -- no ot -- ter -- rò!
}
% BASILIO, CURZIO, CONTE, ANTONIO, BARTOLO
\tag #'(voix4 voix5 voix6) {
  Oh cie -- lo! che veg -- gio!
  de -- li -- ro! va -- neg -- gio!
  che cre -- der, che cre -- der non sò, non sò, non sò?
}
% CONTE (inginocchiandosi)
\tag #'voix5 {
  Con -- tes -- sa per -- do -- no! Per -- do -- no, per -- do -- no!
}
% CONTESSA
\tag #'voix2 {
  Più do -- ci -- le io so -- no,
  e di -- co di sì,
  e di -- co di sì.
}
% TUTTI
Ah! tut -- ti con -- ten -- ti
sa -- re -- mo co -- sì,
sa -- re -- mo co -- sì,
ah! tut -- ti con -- ten -- ti
sa -- re -- mo co -- sì,
ah tut -- ti con -- ten -- ti
sa -- re -- mo, sa -- re -- mo co -- sì.

Que -- sto gior -- no di tor -- men -- ti,
di ca -- pric -- ci e di fol -- li -- a,
in con -- ten -- ti e in al -- le -- gri -- a
so -- lo a -- mor può ter -- mi -- nar,
so -- lo a -- mor può ter -- mi -- nar.
Spo -- si! a -- mi -- ci! al bal -- lo! al gio -- co!
al -- le mi -- ne da -- te fo -- co, da -- te fo -- co!
\tag #'(voix1 voix2 voix3 voix4) { Ed al suon }
\tag #'voix5 { Ed __ al suon __ al suon __ }
\tag #'(voix6 voix7) { Ed __ al suon __ }
di lie -- ta mar -- cia
cor -- riam \tag #'voix5 { tut -- ti } tut -- ti a fe -- steg -- giar,
cor -- riam tut -- ti a fe -- steg -- giar!
Cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti a fe -- steg -- giar!
Cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti,
cor -- riam tut -- ti a fe -- steg -- giar,
a fe -- steg -- giar,
a fe -- steg -- giar,
a fe -- steg -- giar!
