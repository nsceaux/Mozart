\clef "treble" <>
<<
  \tag #'(oboe1 oboi) {
    si'4 s2. |
    re''4 s2. |
    sol''4 s2. |
    fad''1 |
    sol''4 s2. |
    re''4 s2. |
    sol''4 s2. |
    fad''1 |
    sol''2 do'' |
    si' mi'' |
    re''4
  }
  \tag #'(oboe2 oboi) {
    sol'4 s2. |
    si'4 s2. |
    re''4 s2. |
    do''1 |
    si'4 s2. |
    si'4 s2. |
    re''4 s2. |
    do''1 |
    si'2 mi' |
    re' do'' |
    si'4
  }
  { <>\f \ru#3 { s4 r r2 } |
    s1\p |
    <>\f \ru#3 { s4 r r2 } |
    s1\p |
    s1\f | }
>> \tag#'oboi <>^"a 2." re'4 sol' si' |
re''2\sf do''4.(\p la'8) |
sol'4( si') re''4(\sf do''8 la') |
sol'4(\p si') re''(\sf do''8 la') |
<<
  \tag #'(oboe1 oboi) {
    re''2 re'' |
    re'' fad'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    sol'2 do'' |
    si' do'' |
    re''4
  }
  { s2\sf s\sf | s2\sf s\sf | }
>> r4 r2 |
R1*4 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1~ | re'' | }
  { re''1 | do''2( si') | }
>>
<<
  \tag #'(oboe1 oboi) {
    do''2( si') |
    la'4 fad''2 fad''4 |
    mi''1 |
    fad''4 fad''2 fad''4 |
    mi''1 |
    fad''4
  }
  \tag #'(oboe2 oboi) {
    la'2( sol') |
    fad'4 re''2 re''4 |
    dod''1 |
    re''4 re''2 re''4 |
    dod''1 |
    re''4
  }
  { s1 | s4 s2.\f | s1\p | s4 s2.\f | s1\p | }
>> \tag#'oboi <>^"a 2." re'4 fad' la' |
re''2(\sf dod''4\p re'') |
si'2(\sf lad'4\p si') |
fad'2\sf sold'\sf |
la'!4 r r2 |
R1 |
<>\p <<
  \tag #'(oboe1 oboi) {
    mi''1( |
    fad'' |
    sol'' |
    fad'') |
    mi''( |
    fad'' |
    mi''4)
  }
  \tag #'(oboe2 oboi) {
    dod''1( |
    re'' |
    mi'' |
    re'') |
    dod''( |
    re'' |
    dod''4)
  }
>> \tag#'oboi <>^"a 2." la'\cresc dod''! mi'' |
la''2\sf sol''4.(\p mi''8) |
re''4( fad'') la''4(\sf sol''8 mi'') |
re''4(\p fad'') la''4(\sf sol''8 mi'') |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1 | mi'' | re''4 }
  { re''1~ | re''2 dod'' | re''4 }
  { s\p s2.\cresc | s1\f }
>> r4 r2 |
R1*2 |
<>\f <<
  \tag #'(oboe1 oboi) { dod''1( | re''4) }
  \tag #'(oboe2 oboi) { sol'1( | fad'4) }
>> r4 r2 |
R1*2 |
<>\f <<
  \tag #'(oboe1 oboi) { mi''1( | fad''4) }
  \tag #'(oboe2 oboi) { dod''1( | re''4) }
>> r4 r2 |
r2 r4 <>\f <<
  \tag #'(oboe1 oboi) { la'4-. | si'-. si'-. }
  \tag #'(oboe2 oboi) { fad'4-. | sol'-. sol'-. }
>> r2 |
r2 r4 <>\f <<
  \tag #'(oboe1 oboi) { si'4 | do''! do'' }
  \tag #'(oboe2 oboi) { sol'4 | la' la' }
>> r2 |
r r4 \tag#'oboi <>^"a 2." do''16(\f si' do'' re'' |
mi''1)-.\sf |
<<
  \tag #'(oboe1 oboi) {
    la''2-. do'''-. |
    fad''-. la''-. |
    do''1 |
  }
  \tag #'(oboe2 oboi) {
    la'2-. do''-. |
    fad'-. la'-. |
    do'1 |
  }
  { s2\sf s\sf | s2\sf s\sf | s1\sf | }
>>
R1*5 |
r2 r4 <>\pp <<
  \tag #'(oboe1 oboi) {
    re''4 |
    re'' s re'' s |
    s2 s4 sol'' |
    sol'' s sol'' s |
    s2 s4 si'! |
    si' s si' s |
    s2 s4 mib'' |
    mib'' s mib'' s |
  }
  \tag #'(oboe2 oboi) {
    la'4 |
    la' s la' s |
    s2 s4 sib' |
    sib' s sib' s |
    s2 s4 fa' |
    fa' s fa' s |
    s2 s4 do'' |
    do'' s do'' s |
  }
  { s4 | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r | }    
>>
R1*4 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1( | sol'' | fad'' | sol'') | fad''2 }
  { re''1~ | re''~ | re''~ | re''~ | re''2 }
>> r4\fermata r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1\p~ | re''2 r | re''1~ | re''2\fermata }
  { R1*3 | r2\fermata }
>> r4\fermata r |
R1*3 | \allowPageTurn
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    do''2( si'4) |
    do''2( si'4 la') |
  }
  \tag #'(oboe2 oboi) {
    la'2( sol'4) |
    la'2( sol'4 fad') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 } { sol' }
>> r4 r <>\pp \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 |
    re''2( mi'') |
    re''8( do'') si'4 s sol' |
    re''2( mi'') |
    re''4( do''2 si'4) | }
  { sol'4 |
    sol'1 |
    fad'4( sol') s sol' |
    sol'1 |
    fad'2.( sol'4) | }
  { s4 | s1 | s2 r4 s | s1 | s4 s2.\cresc | }
>>
<>\p  <<
  \tag #'(oboe1 oboi) { do''2( si'4 la') | }
  \tag #'(oboe2 oboi) { la'2( sol'4 fad') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 re''8.(\f sol''16) sol''2~ |
    sol''( fad''4 mi'') | }
  { sol' r4 r si'8.(\f mi''16) |
    mi''2( re''4 do'') | }
  { s1 | s\decresc }
>>
<>\pp <<
  \tag #'(oboe1 oboi) {
    \appoggiatura mi''8 re''4( si'8 re'') \appoggiatura re''8 do''4( si'8 la') |
    si'4
  }
  \tag #'(oboe2 oboi) {
    \appoggiatura do''8 si'4( sol'8 si') \appoggiatura si'8 la'4( sol'8 fad') |
    sol'4
  }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8.(\f sol''16) sol''2~ |
    sol''4( mi'') r mi''\pp |
    mi''( do'') r re' |
    re'' la''( sol'' fad'') |
    sol''1 | }
  { r4 r si'8.(\f mi''16) |
    mi''2.( do''4) |
    r do''\pp do''( la') |
    r4 re''2\f re''4~ |
    re''4.( red''8) mi''2 | }
>>
<>\pp <<
  \tag #'(oboe1 oboi) { si'2( \grace re''8 do''4. si'8) | si'4 }
  \tag #'(oboe2 oboi) { sol'2( fad'4. sol'8) | sol'4 }
>> r4 r2 |
<>\pp \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol''~ | sol''4 }
  { si'1 | mi''~ | mi''4 }
  { s1*2 | s4\p }
>> r4 r2 |
R1*2 |
<>\f <<
  \tag #'(oboe1 oboi) { si''1 | dod''' | }
  \tag #'(oboe2 oboi) { sold'' | la'' | }
>>
R1*5 |
<>\f <<
  \tag #'(oboe1 oboi) { la''1 | }
  \tag #'(oboe2 oboi) { la' | }
>>
R1 |
<<
  \tag #'(oboe1 oboi) { la''1~ | la''2 }
  \tag #'(oboe2 oboi) { la'1~ | la'2 }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 | sol'' fad'' | mi''1 | }
  { re''2~ | re''1 | dod'' | }
>>
<<
  \tag #'(oboe1 oboi) {
    fad''2 s4 fad'' |
    mi''2 s4 mi'' |
    fad''2 s4 fad'' |
    mi''2 s4 mi'' |
    fad''2 s4 fad'' |
    mi''1 |
    fad''4
  }
  \tag #'(oboe2 oboi) {
    re''2 s4 re'' |
    dod''2 s4 dod'' |
    re''2 s4 re'' |
    dod''2 s4 dod'' |
    re''2 s4 re'' |
    dod''1 |
    re''4
  }
  { s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag#'oboi <>^"a 2." la''4 fad'' re'' |
la'' la'' la'' r |
re''2\p~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(oboe1 oboi) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(oboe2 oboi) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
\tag #'oboi <>^"a 2." re''2~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(oboe1 oboi) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(oboe2 oboi) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
\tag #'oboi <>^"a 2." re''2~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(oboe1 oboi) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(oboe2 oboi) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 } { re'' }
>> <<
  \tag #'(oboe1 oboi) {
    la''2 |
    si'' la'' |
    la''1~ |
    la''4
  }
  \tag #'(oboe2 oboi) {
    fad''2 |
    sol'' fad'' |
    mi''1 |
    fad''4
  }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    fad'1~ | fad' | la'~ | la' |
    re'' | fad'' | la'' | la'' | la''4
  }
  \tag #'(oboe2 oboi) {
    re'1~ | re' | fad'~ | fad' |
    la' | re'' | fad'' | mi'' | fad''4
  }
  { s1\p | s | s4 s2.\cresc | s1 |
    s1\f | s1*3 | }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    fad'1~ | fad' | la'~ | la' |
    re'' | fad'' | la'' | la'' |
    la''2 s4 fad'' |
    la''2 s4 la'' |
    la''2 s4 fad'' |
    la''2 s4 la'' |
    la''2 s4 fad'' |
    la''2 s4 la'' |
    la''1~ |
    la'' |
    fad'' |
    fad''4 la'' fad'' la'' |
    fad''
  }
  \tag #'(oboe2 oboi) {
    re'1~ | re' | fad'~ | fad' |
    la' | re'' | fad'' | mi'' |
    fad''2 s4 re'' |
    mi''2 s4 mi'' |
    fad''2 s4 re'' |
    mi''2 s4 mi'' |
    fad''2 s4 re'' |
    mi''2 s4 mi'' |
    fad''1~ |
    fad'' |
    re'' |
    re''4 fad'' re'' fad'' |
    re''
  }
  { s1\p | s | s4 s2.\cresc | s1 |
    s1\f | s1*3 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag #'oboi <>^"a 2." re''8 re'' re''4 re'' |
re'' la' re'' la' |
re''2 r4 re''8 re'' |
re''2 r4 re''8 re'' |
re''2 r |
