\clef "bass" R1*113 |
la,1\p\startTrillSpan |
la, |
la, |
la,1*3/4\f s4\stopTrillSpan |
la,4 la, la, r |
R1*5 |
re4\f r la, r |
R1 |
re4 r la, r |
re4 r r2 |
re4 r re r |
la,2 la,4 la, |
re2 r4 re |
la,2 r4 la, |
re2 r4 re |
la,2 r4 la, |
re2 r4 re |
la,2 la,4. la,8 |
re4 re re re |
la, la, la, r |
re\p re8 re re4 r |
re4 re8 re re4 r |
re4 re8 re re4 re8 re |
la,4 la,8 la, la,4 r |
re4 re8 re re4 r |
re4 re8 re re4 r |
re4 re8 re re4 re8 re |
la,4 la,8 la, la,4 r |
re4 re8 re re4 r |
re4 re8 re re4 r |
re4 re8 re re4 re8 re |
la,4 la,8 la, la,4 r |
re2\f re4. re8 |
re4 r re r |
la, r la, r |
re r r2 |
R1*5 |
re2\f re4. re8 |
re4 r re r |
la,2 la,4. la,8 |
la,4 r la, r |
re4 r r2 |
R1*5 |
re2\f re4. re8 |
re4 r re r |
la,2 la,4. la,8 |
la,4 r la, r |
re2 r4 re |
la,2 r4 la, |
re2 r4 re |
la,2 r4 la, |
re2 r4 re |
la,2 r4 la, |
re r re re |
re r re re |
re r re re |
re re re re |
re re8 re re4 re |
re la, re la, |
re2 r4 re8 re |
re2 r4 re8 re |
re2 r |
