\clef "bass"
<>\f \ru#3 {
  <<
    \tag #'(fagotto1 fagotti) { sol4 }
    \tag #'(fagotto2 fagotti) { sol, }
  >> r4 r2 |
}
R1 |
<>\f \ru#3 {
  <<
    \tag #'(fagotto1 fagotti) { sol4 }
    \tag #'(fagotto2 fagotti) { sol, }
  >> r4 r2 |
}
R1 |
\tag#'fagotti <>^"a 2." sol2:8\f sol:8 |
sol:8 sol:8 |
sol4 re sol si |
re'2\sf do'4.(\p la8) |
sol4( si) re'4(\sf do'8 la) |
sol4(\p si) re'4(\sf do'8 la) |
sol8\sf re sol re fad\sf re fad re |
sol\sf re sol re la\sf re la re |
si4 r r2 |
R1*7 |
r4 \tag#'fagotti <>^"a 2." re4\f fad re |
la1\p |
r4 re\f fad re |
la1\p |
r4 re\f fad la |
re'2(\sf dod'4\p re') |
si2(\sf lad4\p si) |
fad2\sf sold\sf |
la!4 r <>\p <<
  \tag #'(fagotto1 fagotti) {
    mi2( |
    fad1 |
    sol |
    fad) |
    mi( |
    fad |
    sol fad |
    \tag #'fagotti \voiceOne mi4) \oneVoice
  }
  \tag #'(fagotto2 fagotti) {
    dod2( |
    re1 |
    mi |
    re) |
    dod( |
    re |
    mi re |
    <<
      \tag #'fagotto2 mi4)
      \tag #'fagotti \new Voice { \voiceTwo mi4 }
    >>
  }
>> \tag#'fagotti <>^"a 2." la,4\f dod mi |
la2\sf sol4.(\p mi8) |
re4( fad) la4(\sf sol8 mi) |
re4(\p fad) la4(\sf sol8 mi) |
re2*1/2:8\p s4\cresc si2:8 |
sol:8\f la:8 |
re4 r r2 |
R1*2 |
<>\f \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { la1 | re4 }
  { la,1 | re4 }
>> r4 r2 |
R1*2 |
\tag#'fagotti <>^"a 2." la2:8\f la:8 |
re4 r r2 |
r2 r4 <>\f <<
  \tag #'(fagotto1 fagotti) { la4-. | si-. si-. }
  \tag #'(fagotto2 fagotti) { fad4-. | sol-. sol-. }
>> r2 |
r r4 <>\f <<
  \tag #'(fagotto1 fagotti) { si4 | do'! do' }
  \tag #'(fagotto2 fagotti) { sol4 | la la }
>> r2 |
r r4 \tag#'fagotti <>^"a 2." do'16(\f si do' re' |
mi'1)-.\sf |
la2-.\sf do'-.\sf |
fad-.\sf la-.\sf |
<>\sf \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do2. la4\p | la4( si do' dod') | }
  { do1 | R1 }
>>
<>\p <<
  \tag #'(fagotto1 fagotti) { re'2( do'!) | si( la) | }
  \tag #'(fagotto2 fagotti) { sol2( la) | sol( fad) | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol4 }
  { sol }
>> r4 r2 |
R1 |
r2 r4 <>\pp <<
  \tag #'(fagotto1 fagotti) {
    fa4 |
    fa s fa s |
    s2 s4 mib |
    mib s mib s |
    s2 s4 re |
    re s re s |
    s2 s4 do |
    do s do s |
  }
  \tag #'(fagotto2 fagotti) {
    fa,4 |
    fa, s fa, s |
    s2 s4 mib, |
    mib, s mib, s |
    s2 s4 re, |
    re, s re, s |
    s2 s4 do, |
    do, s do, s |
  }
  { s4 | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r | }    
>>
R1*4 | \allowPageTurn
<>\p <<
  \tag #'(fagotto1 fagotti) { la1( | sib | la | sib) | la2 }
  \tag #'(fagotto2 fagotti) { fad1( | sol | fad | sol) | fad2 }
>> r4\fermata r |
<<
  \tag #'(fagotto1 fagotti) {
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    sol2(\p do'4 si!) |
    si( la) r2 |
    la2( sol4 fad) |
    fad\fermata sol\fermata r\fermata r |
    si2(\p do') |
    re'4( mi') r2 |
    si2( do') |
    re'2.( mi'4) |
    do'2( re') |
    sol4 r
  }
  \tag #'fagotto2 {
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2
  }
>> r4 <>\pp <<
  \tag #'(fagotto1 fagotti) {
    si4 |
    sol2( do') |
    la4( sol) s si4 |
    sol2( do') |
    la2.( sol4) |
  }
  \tag #'(fagotto2 fagotti) {
    sol,4 |
    si,2( do) |
    re4( mi) s4 mi |
    si,2( do) |
    re2.( mi4) |
  }
  { s4 s1 | s2 r4 s | s1 | s4 s2.\cresc | }
>>
<>\pp \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mi'2( re'4 do') |
    si s s sol4 |
    sol'1~ |
    sol'4 re'2( do'4) |
    si4 s s sol8.( do'16) |
    do'2.( la4) | }
  { do2( re) |
    sol,4 s s sol8.( do'16) |
    do'1 |
    re'2 re |
    sol4 s s sol8.( do'16) |
    do'2.( la4) | }
  { s1 | s4 r r s\f | s1\decresc | s1\pp | s4 r r s\f | }
>>
r4 <>\pp <<
  \tag #'(fagotto1 fagotti) { do'4 do'( la) | }
  \tag #'(fagotto2 fagotti) { la4 la( fad) | }
>>
r4 <>\f \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'2 re'4 | re'4.( red'8) }
  { do'4( si do') | si2 }
>> <<
  \tag #'(fagotto1 fagotti) {
    mi'4( fad'8 sol') |
    si2( \grace re'8 do'4. si8) |
    si4
  }
  \tag #'(fagotto2 fagotti) {
    do'4( re'8 mi') |
    sol2 fad4. sol8 |
    sol4
  }
  { s2 | s1\pp }
>> r4 r2 |
R1*2 |
%%
R1*3 |
\tag#'fagotti <>^"a 2." la,2:8\f la,:8 |
la,8 la la la la2 |
R1*3 |
r2 r4 <>\p \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { la4 } { la }
>>
<<
  \tag #'(fagotto1 fagotti) {
    re'4( fad'! mi' dod') |
    re'
  }
  \tag #'(fagotto2 fagotti) {
    fad!4( la sol mi) |
    fad
  }
  { s1 | s4\f }
>> \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad4 mi } { fad mi }
>> <<
  \tag #'(fagotto1 fagotti) {
    la4 |
    re'( fad' mi' dod') |
    re'
  }
  \tag #'(fagotto2 fagotti) {
    la, |
    fad4( la sol mi) |
    fad
  }
  { s4 | s1\p | s4\f }
>> \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad4 mi la, | } { fad mi la, | }
>>
\tag#'fagotti <>^"a 2." re2:8\f re:8 |
sol:8 re:8 |
la:8 la,:8 |
re4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
la2:8 la:8 |
re'8 re' la la fad fad re re |
la4 la, la, r |
re2\p~ re8 la, re la, |
re2~ re8 fad la fad |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re8 mi fad sol } { re8 dod re mi }
>> <<
  \tag #'(fagotto1 fagotti) {
    la8 si dod' re' |
    re'( dod') si-. la-. la( sol) fad-. mi-. |
  }
  \tag #'(fagotto2 fagotti) {
    fad8 sol mi fad |
    fad( mi) sol-. fad-. fad( mi) re-. dod-. |
  }
>>
\tag#'fagotti <>^"a 2." re2~ re8 la, re la, |
re2~ re8 fad la fad |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re8 mi fad sol } { re8 dod re mi }
>> <<
  \tag #'(fagotto1 fagotti) {
    la8 si dod' re' |
    re'( dod') si-. la-. la( sol) fad-. mi-. |
  }
  \tag #'(fagotto2 fagotti) {
    fad8 sol mi fad |
    fad( mi) sol-. fad-. fad( mi) re-. dod-. |
  }
>>
\tag#'fagotti <>^"a 2." re2~ re8 la, re la, |
re2~ re8 fad la fad |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re8 mi fad sol } { re8 dod re mi }
>> <<
  \tag #'(fagotto1 fagotti) {
    la8 si dod' re' |
    re'( dod') si-. la-. la( sol) fad-. mi-. |
  }
  \tag #'(fagotto2 fagotti) {
    fad8 sol mi fad |
    fad( mi) sol-. fad-. fad( mi) re-. dod-. |
  }
>>
\tag#'fagotti <>^"a 2." re4\f re8 re re4 r |
sol4 sol8 sol re4 re8 re |
la4 la8 la la,4 la,8 la, |
re la,\p si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si,\cresc dod re mi fad mi |
re la, si, dod re mi fad mi |
re\f la, si, dod re mi fad mi |
re la, si, dod re mi fad re |
la2:8 la:8: |
la,:8 la,:8 |
re8 la,\p si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si, dod re mi fad mi |
re la, si,\cresc dod re mi fad mi |
re la, si, dod re mi fad mi |
re\f la, si, dod re mi fad mi |
re la, si, dod re mi fad re |
la2:8 la:8: |
la,:8 la,:8 |
re4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
r4 r8 \tuplet 3/2 { la16([ si dod'] } re'4) re |
r r8 \tuplet 3/2 { mi16([ fad sold] } la4) la, |
re2:8 re:8 |
re:8 re:8 |
re:8 re:8 |
re:8 re:8 |
re4 re8 re re4 re |
re la, re la, |
re2 r4 re8 re |
re2 r4 re8 re |
re2 r |
