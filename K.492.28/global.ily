\tag #'all \key sol \major
\tag #'clarinetti \key sib \major
\tempo "Allegro assai" \midiTempo#160
\time 4/4 s1*85 s2.
\tempo "Andante" \midiTempo#80 s4 s1*27 \bar "||"
\tag #'all \key re \major
\tag #'clarinetti \key fa \major
\tempo "Allegro assai" \midiTempo#160 s1*74 \bar "|."
