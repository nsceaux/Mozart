\clef "treble"
\ru#2 {
  <re' si' sol''>4\f r4 r2 |
  <<
    \tag #'violino1 {
      <sol' re'' si''>4 r r2 |
      <re' re'' re'''>4 re'''8\p si'' re''' si'' re''' si'' |
      do''' si'' la'' sol'' fad'' mi'' re'' do'' |
    }
    \tag #'violino2 {
      <re' si' sol''>4 r r2 |
      <re' re'' si''>4 si''8\p sol'' si'' sol'' si'' sol'' |
      la'' sol'' fad'' mi'' re'' do'' si' la' |
    }
  >>
}
<<
  \tag #'violino1 {
    si'2:8\f do'':8 |
    si':8 mi'':8 |
    re'':8 sol''8 sol'' si'' si'' |
    re'''2\sf do'''4.(\p la''8) |
    sol''4( si'') re'''4(\sf do'''8 la'') |
    sol''4(\p si'') re'''4(\sf do'''8 la'') |
    sol''2:8\sf la'':8\sf |
    si'':8\sf do''':8\sf |
    re'''4 r r2 |
    re''2(\p mi'') |
    do''4-. si'-. r2 |
    re''2( mi'') |
    do''4-. si'-. r2 |
    r8 re''-. mi''-. fad''-. sol''-. la''-. si''-. sol''-. |
    fad''-. do'''-. la''-. fad''-. sol''-. si''-. sol''-. re''-. |
    do''-. la''-. mi''-. do''-. si'-. sol''-. mi''-. si'-. |
    la'8 la''4\f la'' la'' la''8 |
    sol''\p fad'' mi'' re'' dod'' si' la' sol' |
    fad' la''4\f la'' la'' la''8 |
    sol''\p fad'' mi'' re'' dod'' si' la' sol' |
    fad' fad' re''\cresc re'' fad'' fad'' la'' la'' |
    re'''2:8\sf dod'''8\p dod''' re''' re''' |
    si''2:8\sf lad''8\p lad'' si'' si'' |
    fad''2:8\sf sold'':8\sf |
    la''!4 r r la''\p |
    \override Script.avoid-slur = #'outside
    \ru#7 { la''16(\trill sold'' la''8) r4 r la'' | }
    la''16(\trill sold'' la''8) la'8\cresc la' dod'' dod'' mi'' mi'' |
    la''2\sf sol''!4.(\p mi''8) |
    re''4( fad'') la''4(\sf sol''8 mi'') |
    re''4(\p fad'') la''(\sf sol''8 mi'') |
    re''8\p re''' re'''\cresc re''' <re''' fad''>2:8 |
    <mi'' re'''>2:8\f <dod''' mi''>:8 |
    <fad'' re'''>4 fad'(\p la' re'') |
    r la'( re'' fad'') |
    r re''( fad'' la'') |
    dod''8\f re'' mi'' fad'' sol'' la'' si'' dod''' |
    re'''4 fad'(\p la' re'') |
    r la'( re'' fad'') |
    r re''( fad'' la'') |
    sol''8\f fad'' mi'' re'' dod'' si' la' sol' |
    fad'4 r r la'\p |
    la'4.( si'8) la'4 <re' la' la''> |
    <re' si' si''> q r si'\p |
    si'4.( do''!8) si'4 <re' re'' si''>4\f |
    <re' re'' do'''!> q r do''\p |
    do''4.( re''8) do''4 do''16(\f si' do'' re'') |
  }
  \tag #'violino2 {
    sol'8\f re' re' re' mi'2:8 |
    re':8 do'':8 |
    si'8 si' re' re' sol' sol' si' si' |
    re''2\sf do''4.(\p la'8) |
    sol'4( si') re''4(\sf do''8 la') |
    sol'4(\p si') re''4(\sf do''8 la') |
    sol'2:8\sf la':8\sf |
    si':8\sf do'':8\sf |
    re''4 r r2 |
    si'2(\p do'') |
    la'4-. sol'-. r2 |
    si'2( do'') |
    la'4-. sol'-. r2 |
    r8 re'-. mi'-. fad'-. sol'-. la'-. si'-. sol'-. |
    fad'-. do''-. la'-. fad'-. sol'-. si'-. sol'-. re'-. |
    do'-. la'-. mi'-. do'-. si-. sol'-. mi'-. si-. |
    la8 <la' fad''>4\f q q q8 |
    mi''8\p re'' dod'' si' la' sol' fad' mi' |
    re'8 <la' fad''>4\f q q q8 |
    mi''8\p re'' dod'' si' la' sol' fad' mi' |
    re'2*1/2:8 s4\cresc fad'8 fad' la' la' |
    re''2:8\sf dod''8\p dod'' re'' re'' |
    si'2:8\sf lad'8\p lad' si' si' |
    fad'2:8\sf sold':8\sf |
    la'!8 mi'8\p mi' mi' mi'2:8 |
    fad':8 fad':8 |
    sol':8 sol':8 |
    fad':8 fad':8 |
    mi':8 mi':8 |
    fad':8 fad':8 |
    sol':8 sol':8 |
    fad':8 fad':8 |
    mi'8 mi' la'\cresc la' dod'' dod'' mi'' mi'' |
    la'2\sf sol'4.(\p mi'8) |
    re'4( fad') la'4(\sf sol'8 mi') |
    re'4(\p fad') la'(\sf sol'8 mi')
    fad'8\p fad'' fad''\cresc fad'' <re'' fad''>2:8 |
    <re'' mi''>:8\f <dod'' mi''>:8 |
    re''8 fad'(\p re' fad' re' fad' re' fad') |
    re'( fad' re' fad' re' fad' re' fad') |
    re'( fad' re' fad' re' fad' re' fad') |
    mi'8\f fad' sol' la' si' dod'' re'' mi'' |
    fad'' fad'(\p re' fad' re' fad' re' fad') |
    re'( fad' re' fad' re' fad' re' fad') |
    re'( fad' re' fad' re' fad' re' fad') |
    mi''\f re'' dod'' si' la' sol' fad' mi' |
    re'4 r r fad'\p |
    fad'4.( sol'8) fad'4 <re' la' fad''>\f |
    <re' si' sol''> q r sol'\p |
    sol'4.( la'8) sol'4 <re' si' sol''>\f |
    <re' do''! la''> q r la'\p |
    la'4.( si'8) la'4 do''16(\f si' do'' re'') |
  }
>>
mi''2:8\sf mi'':8 |
la':8\sf do'':8\sf |
fad':8\sf la':8\sf do'1\sf |
<<
  \tag #'violino1 {
    la'1\p( |
    si'4) r la' r |
    sol' r fad' r |
    sol'8-.\pp la'-. sib'-. do''-. re''-. do''-. sib'-. la'-. |
    sol'-. la'-. sib'-. do''-. re''-. mi''-. fad''-. sol''-. |
    re'' mi'' fa'' sol'' la'' sol'' fa'' mi'' |
    re'' mi'' fa'' sol'' la'' si''! dod''' re''' |
    sol'' lab'' sib'' lab'' sol'' fa'' mib'' re'' |
    mib'' sib' do'' re'' mib'' fa'' sol'' mib'' |
    fa'' sol'' lab'' sol'' fa'' mib'' re'' do'' |
    si'! la'! sol' la' si' do'' re'' si' |
    mib'' fa'' sol'' fa'' mib'' re'' do'' si'! |
    do'' re'' mib'' re'' do'' sib' la' sol' |
    la' sib' do'' re'' mib'' fa'' sol'' la'' |
    sib'' do''' re''' do''' sib'' do''' sib'' la'' |
    sol'' la'' sib'' fad'' sol'' la'' sib'' fad'' |
    sol'' fad'' sol'' la'' sib'' la'' sib'' sol'' |
    fad'' la'' re''' dod''' re''' do''' sib'' la'' |
    sib'' re''' dod''' re''' sib'' re''' sib'' sol'' |
    fad'' la' re'' dod'' re'' do'' sib' la' |
    sib' re'' dod'' re'' sib' re'' sib' sol' |
    re'2 r4\fermata re'8(\p si'!) |
    si'2( la'4 sol') |
    sol'( fad') r re'8( do'') |
    do''2( si'4 la') |
    lad'4\fermata si'\fermata r\fermata sol'8(\p re'') |
    re''2( mi'') |
    \appoggiatura re''8 do''4( si') r sol'8( re'') |
    re''2( mi'') |
    \appoggiatura re''4 do''2.( si'4) |
    do''4~ do''16( re'' mi'' do'') si'4( la') |
  }
  \tag #'violino2 {
    fad'1( |
    sol'4) r mi' r |
    re' r do' r |
    sib8-.\pp la-. sib-. do'-. re'-. do'-. sib-. la-. |
    sol-. la-. sib-. do'-. re'-. mi'-. fad'-. sol'-. |
    re' mi' fa' sol' la' sol' fa' mi' |
    re' mi' fa' sol' la' si'! dod'' re'' |
    sol' lab' sib' lab' sol' fa' mib' re' |
    mib' sib do' re' mib' fa' sol' mib' |
    fa' sol' lab' sol' fa' mib' re' do' |
    si! la! sol la si do' re' si |
    mib' fa' sol' fa' mib' re' do' si! |
    do' re' mib' re' do' sib la sol |
    la sib do' re' mib' fa' sol' la' |
    sib' do'' re'' do'' sib' do'' sib' la' |
    sol' la' sib' fad' sol' la' sib' fad' |
    sol' fad' sol' la' sib' la' sib' sol' |
    fad' la' re'' dod'' re'' do'' sib' la' |
    sib' re'' dod'' re'' sib' re'' sib' sol' |
    fad' la re' dod' re' do' sib la |
    sib re' dod' re' sib re' sib sol |
    la2 r4\fermata r |
    re'1\p~ |
    re'2 r |
    re'1~ |
    re'2\fermata r4\fermata r | \allowPageTurn
    sol'1\p |
    fad'4( sol') r2 |
    sol'1 |
    fad'2.( sol'4) |
    la'4~ la'16( si' do'' la') sol'4( fad') |
  }
>>
sol'8\noBeam sol''(\cresc si'' re'' sol'' si' re'' sol') |
R1 | \allowPageTurn
r8 fad''(\p sol'' si' mi''\cresc sol' si' mi')\! |
R1 |
<<
  \tag #'violino1 {
    r8 la''(\p\cresc do''' fad'' la'' do'' si' sol'') |
    r mi'(\p mi'' do'') si'( re'' do'' la') |
    sol'4 re''8.(\f sol''16) sol''2~ |
    sol''(\decresc fad''4 mi'') | <>\p
    \appoggiatura mi''8 re''4( si'8 re'') \appoggiatura re''8 do''4( si'8 la') |
    si'4 re''8.(\f sol''16) sol''2~ |
    sol''4( mi'') r mi''\p |
    mi''( do'') r re'\f |
    re''4 re''2 re''4 |
    re''4.( red''8) mi''4( fad''8 sol'') |
    si'4\p si' la' la' |
  }
  \tag #'violino2 {
    r8 la'(\p\cresc do'' fad' la' do' si sol') |
    r8 mi'(\p do'' la') sol'( si' la' fad') |
    sol'4 r r si'8.(\f mi''16) |
    mi''2(\decresc re''4 do'') | <>\p
    \appoggiatura do''8 si'4( sol'8 si') \appoggiatura si'8 la'4( sol'8 fad') |
    sol'4 r r si'8.(\f mi''16) |
    mi''2.( do''4)\p |
    r do'' do''( la') |
    r do''(\f si' do'') |
    si'2 do''4( re''8 mi'') |
    sol'4\p sol' fad' fad' |
  }
>>
sol'4 r8 si'(\pp sol'4) r8 fad'( |
mi'4) r8 sol'( mi'4) r8 re'( |
dod'4) r8 mi'( dod'4) r8 si( |
la4)\p r4 r2 |
<<
  \tag #'violino1 {
    dod''2. \appoggiatura re''16 dod''8( si'16 dod'') |
    re''2. \appoggiatura mi''16 re''8( dod''16 re'') |
    <fa'' re'''>4\f q2 q4 |
    <dod''' mi''>8
  }
  \tag #'violino2 {
    sol'1( |
    fa') |
    <si' sold''>4\f q2 q4 |
    <dod'' la''>8
  }
>> la'8 la' la' la'2:8 |
re''2\p fa''4.( re''8) |
la'2 re''4.( la'8) |
fa'4.( la'8) fa'4.( re'8) |
la'4 la la la' |
<<
  \tag #'violino1 {
    re''4( fad''! mi'' dod'') |
    <la' la''>2\sf~ la''8( sol'' mi'' dod'') |
    re''4(\p fad'' mi'' dod'') |
    <la' la''>2\sf~ la''8( sol'' mi'' dod'') |
    re''4\f <re' la' la''> q q |
    <re' si' si''>4 q <re' la' la''> q |
    <la' mi'' dod'''> q q q |
    <la' fad'' re'''>4
  }
  \tag #'violino2 {
    fad'4( la' sol' mi') |
    fad'8 la'(\sf fad' re' dod' mi' sol' mi') |
    fad'4(\p la' sol' mi') |
    fad'8 la'(\sf fad' re' dod' mi' sol' mi') |
    fad'\f <re'' fad''> q q q2:8 |
    <re'' sol''>:8 <re'' fad''>:8 |
    <dod'' mi''>:8 q:8 |
    <re'' fad''>4
  }
>> r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
r r8 \tuplet 3/2 { mi''16([ fad'' sold''] } la''4) la' |
r r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
r r8 \tuplet 3/2 { mi''16([ fad'' sold''] } la''4) la' |
r r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
<mi'' dod'''>2:8 q:8 |
<fad'' re'''>8 q la'' la'' fad'' fad'' re'' re'' |
la''4 la' la' r |
re'8\p la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
re'8 la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
re'8 la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
<<
  \tag #'violino1 {
    <re' la' la''>4 la''8 la'' la''4 r |
    <re' si' si''>4 si''8 si'' la''4 la''8 la'' |
    <mi'' dod'''>4 q8 q q4 q8 q |
    <fad'' re'''>4
  }
  \tag #'violino2 {
    <re' la' fad''>4\f <re'' fad''>8 q q4 r |
    <re'' sol''>4 q8 q <re'' fad''>4 q8 q |
    <dod'' mi''>4 q8 q q4 q8 q |
    <re'' fad''>4
  }
>> r4 re'8\p mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si\cresc dod' re' mi' fad' mi' |
re' la' si' dod'' re'' mi'' fad'' mi'' |
re''\f la' si' dod'' re'' mi'' fad'' mi'' |
re'' la' si' dod'' re'' mi'' fad'' re'' |
<<
  \tag #'violino1 {
    <fad'' re'''>2:8 q:8 |
    <mi'' dod'''>:8 q:8 |
    <re''' fad''>4 r <>\p
  }
  \tag #'violino2 {
    <fad'' re''>2:8 q:8 |
    <dod'' mi''>:8 q:8 |
    re''8 la\p si dod'
  }
>> re'8 mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si\cresc dod' re' mi' fad' mi' |
re' la' si' dod'' re'' mi'' fad'' mi'' |
re''\f la' si' dod'' re'' mi'' fad'' mi'' |
re'' la' si' dod'' re'' mi'' fad'' re'' |
<<
  \tag #'violino1 {
    <fad'' re'''>2:8 q:8 |
    <mi'' dod'''>:8 q:8 |
    <fad'' re'''>4
  }
  \tag #'violino2 {
    <re'' fad''>2:8 q:8 |
    <dod'' mi''>:8 q:8 |
    re''4
  }
>> r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
r r8 \tuplet 3/2 { mi''16([ fad'' sold''] } la''4) la' |
r r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
r r8 \tuplet 3/2 { mi''16([ fad'' sold''] } la''4) la' |
r r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re'' |
<<
  \tag #'violino1 {
    <mi'' dod'''>2:8 q:8 |
    <fad'' re'''>4 r8 \tuplet 3/2 { la''16([ si'' dod'''] } re'''4) re''' |
    re''' r8 fad''16( sol'' la''4) la'' |
    la'' r8 re''16( mi'' fad''4) fad'' |
    fad''
  }
  \tag #'violino2 {
    <dod'' mi''>2:8 q:8 |
    <re'' fad''>4 r <re' la' fad''>4 q |
    q r q q |
    q r <la fad' re''>4 q |
    q
  }
>> <re' la' la''>4 <re' la' fad''> <re' la' la''> |
<re' la' fad''>4 re'8 re' re'4 re' |
re' la re' la |
re'2 r4 re'8 re' |
re'2 r4 re'8 re' |
re'2 r |
