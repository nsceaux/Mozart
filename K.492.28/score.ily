\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \keepWithTag #'all \global
        \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        \haraKiri
        instrumentName = "Clarinetti in A"
        shortInstrumentName = "Cl."
      } <<
        \keepWithTag #'clarinetti \global
        \keepWithTag #'clarinetti \includeNotes "clarinetti"
        { s1*113 <>^\markup\whiteout "Clarinetti in A." }
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in G."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with {
        \haraKiri
        instrumentName = "Trombe in D."
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
        { s1*113 <>^\markup\whiteout "Trombe in D." }
      >>
      \new Staff \with {
        \haraKiri
        instrumentName = "Timpani in D.A."
        shortInstrumentName = "Tim."
      } <<
        { s1*113 <>^\markup\whiteout "Timpani" }
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Susanne
        shortInstrumentName = \markup\character Su.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character La Contessa
        shortInstrumentName = \markup\concat { C \super a }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\center-column\smallCaps {
          Cherubino Barbarina Marcellina
        }
        shortInstrumentName =  \markup\center-column\smallCaps { Che. Bar. Mar. }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\center-column\smallCaps {
          Basilio Don Curzio
        }
        shortInstrumentName =  \markup\center-column\smallCaps { Ba. Cu. }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Il Conte
        shortInstrumentName = \markup\concat { C \super e }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix5 \includeNotes "voix"
      >> \keepWithTag #'voix5 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\center-column\smallCaps {
          Antonio Bartolo
        }
        shortInstrumentName =  \markup\center-column\smallCaps { Ant. Bar. }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix6 \includeNotes "voix"
      >> \keepWithTag #'voix6 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Figaro
        shortInstrumentName = \markup\character Fi.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix7 \includeNotes "voix"
      >> \keepWithTag #'voix7 \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column {
        Violoncello e Basso
      }
      shortInstrumentName = \markup\center-column { Vc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global
      \includeNotes "basso"
      \origLayout {
        s1*7\pageBreak
        s1*8\break s1*8\pageBreak
        s1*8\break s1*7\pageBreak
        s1*8\break s1*8\pageBreak
        s1*10\pageBreak
        s1*8\break s1*7\pageBreak
        s1*7\break s1*9\pageBreak
        s1*9\pageBreak
        s1*9\pageBreak
        s1*10\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*8\pageBreak
      }
      \modVersion { s1*113\break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
