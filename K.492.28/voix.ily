<<
  \tag #'voix1 {
    \clef "soprano/treble" R1*46 |
    r2 r4 la' |
    la'2 fad'4 re'' |
    re''2 la'4 r |
    R1*5 |
    r2 r4 la'^\p |
    la'4.( si'8) la'4 r |
    r2 r4 si' |
    si'4.(^\cresc do''!8) si'4 r |
    r2 r4 do''!^\f |
    do''4.( re''8) do''4 r |
    R1*25 |
    r2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } sol' |
    re''2 mi''4. mi''8 |
    \appoggiatura re''8 do''4 si' r sol' |
    re''2 mi''4. mi''8 |
    \appoggiatura re''4 do''2. si'4 |
    do''~ do''16([ re'' mi'' do'']) si'4 la' |
    sol' re''8.([^\f sol''16]) sol''2~ |
    sol'' fad''4 mi''^\p |
    \appoggiatura mi''8 re''4 si'8 re'' \appoggiatura re''8 do''4 si'8 la' |
    sol'4 re''8.([^\f sol''16]) sol''2~ |
    sol''4 mi'' r mi''^\p |
    mi'' do'' r re'^\f |
    re''1~ |
    re''4.( red''8) mi''4 fad''8([ sol'']) |
    sol'2^\pp \appoggiatura si'8 la'4. sol'8 |
    sol'4 r r2 |
    R1*2 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R1*63 |
    r2 r4 la' |
    la'( si') do'' dod'' |
    re''2 mi''4 mi'' |
    sol'2 \appoggiatura si'8 la'4. sol'8 |
    sol'4 r r2 |
    R1*17 |
    r2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata sol'8([ re'']) |
    re''2 mi''4. mi''8 |
    \appoggiatura re''8 do''4 si' r sol'8([ re'']) |
    re''2 mi''4. mi''8 |
    \appoggiatura re''4 do''2. si'4 |
    do''~ do''16([ re'' mi'' do'']) si'4 la' |
    sol' r r^\markup\italic { sotto vocce } sol' |
    re''2 mi''4. mi''8 |
    \appoggiatura re''8 do''4 si' r sol' |
    re''2 mi''4. mi''8 |
    \appoggiatura re''4 do''2. si'4 |
    do''~ do''16([ re'' mi'' do'']) si'4 la' |
    sol' re''8.([^\f sol''16]) sol''2~ |
    sol'' fad''4 mi''^\p |
    \appoggiatura mi''8 re''4 si'8 re'' \appoggiatura re''8 do''4 si'8 la' |
    sol'4 re''8.([^\f sol''16]) sol''2~ |
    sol''4 mi'' r mi''^\p |
    mi'' do'' r re'^\f |
    re''1~ |
    re''4.( red''8) mi''4 fad''8([ sol'']) |
    sol'2^\pp \appoggiatura si'8 la'4. sol'8 |
    sol'4 r r2 |
    R1*2 |

  }
  \tag #'voix3 {
    \clef "soprano/treble" R1*54 |
    r2 r4 fad'^\p |
    fad'4.( sol'8) fad'4 r |
    r2 r4 sol' |
    sol'4.(^\cresc la'8) sol'4 r |
    r2 r4 la'^\f |
    la'4.( si'8) la'4 r |
    R1*25 |
    r2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } re' |
    sol'2 sol'4. sol'8 |
    fad'4 sol' r sol' |
    sol'2 sol'4. sol'8 fad'2. sol'4 |
    la'4~ la'16([ si' do'' la']) sol'4 fad' |
    sol' r r si'8.([^\f mi''16]) |
    mi''2 re''4 do''^\p |
    \appoggiatura do''8 si'4 sol'8 si' \appoggiatura si'8 la'4 sol'8 fad' |
    sol'4 r r si'8.([^\f mi''16]) |
    mi''2. do''4 |
    r do''^\p do'' la' |
    r la'(^\f sol' fad') |
    sol'2 sol'4. mi'8 |
    re'2^\pp fad'4. sol'8 |
    sol'4 r r2 |
    R1*2 |
    
  }
  \tag #'voix4 {
    \clef "tenor/G_8" R1*8 |
    r2 <>^"a 2." do'4. do'8 |
    si4 si mi'4. mi'8 |
    re'4 re' r2 |
    R1*6 |
    <>^\markup\italic { sotto vocce } re'2 mi' |
    do'4 si r2 |
    re'2 mi' |
    do'4 si r2 |
    r re'4. re'8 |
    fad4 r sol r4 |
    do'4 r si r |
    la r r2 |
    R1*14 |
    r2 r4 fad' |
    mi' la r2 |
    R1*13 |
    r2 r4 re'^\p |
    re'2 re'4 r |
    r2 r4 re' |
    re'2^\cresc re'4 r |
    r2 r4 re'^\f |
    re'2 re'4 r |
    R1*8 |
    r2^\markup\italic { (da se.) } r4 <>^\markup\italic { sotto vocce } sol |
    re' r re' r |
    r2 r4 re' |
    sol' r sol r |
    r2 r4 sol |
    si! r si r |
    r2 r4 re' |
    mib' r do' r |
    r2 mib' |
    mib'1 |
    re'2 re' |
    re'1 |
    dod'2 dod' |
    re'4 r r2 |
    re'4 r r2 |
    re'4 r r2 |
    re'4 r r2 |
    re'2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } si4 |
    sol2 do'4. do'8 |
    la4 sol r si |
    sol2 do'4. do'8 |
    la2. sol4 |
    mi'2 re'4 do' |
    si r r sol^\f |
    sol'2 sol'4 sol'^\p |
    sol' re'8 re' re'4 re'8 re' |
    re'4 r r sol^\f |
    sol do' r do'^\p |
    do' la r2 |
    r4 do'(^\f si do') |
    si2 do'4. do'8 |
    si2^\pp \appoggiatura re'8 do'4. si8 |
    si4 r r2 |
    R1*2 |
    
  }
  \tag #'voix5 {
    \clef "bass" <>^\markup\italic { (arresta Figaro.) }
    r2 sol4. sol8 |
    sol4. re8 si4. si8 |
    si4 sol r2 |
    R1 |
    r2 sol4. sol8 |
    sol4. re8 si4. si8 |
    si4 sol r2 |
    R1*4 |
    re'2 do'4. la8 |
    sol4 si re' do'8([ la]) |
    sol4 si re' do'8([ la]) |
    sol4. sol8 la4. la8 |
    si4. si8 do'4. do'8 |
    re'4 r r2 |
    R1*7 |
    r2 <>^\markup\italic {
      (il Conte tira per braccio Cherubino; dopo il paggio,
      Barbarina, Marcellina e Susanna.)
    }
    r4 la |
    la2 si4. dod'8 |
    re'4 la r la |
    la2 si4. dod'8 |
    re'4 la r la |
    re'2 dod'4 re' |
    si2 lad4 si |
    fad2 sold4. sold8 |
    la!4 r r2 |
    r2 r4 la |
    dod' dod' r2 |
    R1*5 |
    r2 r4 la |
    la2 la4 dod' |
    re' la2 dod'4 |
    re'2 la4 dod' |
    re'2 si |
    sol la4 la |
    re4 r r2 |
    R1 |
    r2 r4 la |
    la2 la4. sol8 |
    fad4 re r2 |
    R1 |
    r2 r4 la |
    la2 si4 dod' |
    re'4 re r2 |
    R1 |
    re'4 r r2 |
    R1 |
    re'4 r r2 |
    R1 |
    mi'1 |
    la4 r do' r |
    fad r la r |
    do1 |
    R1*4 |
    r2^\markup\italic { (da se.) } r4 sol |
    la r la r |
    r2 r4 la |
    sib r sib r |
    r2 r4 sol |
    fa r fa r |
    r2 r4 si! |
    do' r sol r |
    r2 sol |
    la1 |
    sib2 sib |
    sib1 |
    la2 sol |
    fad!4 r r2 |
    sib4 r r2 |
    la4 r r2 |
    sib4 r r2 |
    la2 r4\fermata re8([ si]) |
    si2 la4 sol |
    sol fad r re8([ do']) |
    do'2 si4 la |
    lad4\fermata si\fermata r\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } si |
    sol2 do'4. do'8 |
    la4 sol r si |
    sol2 do'4. do'8 |
    la2. sol4 |
    mi'2 re'4 do' |
    si r r sol8.([^\f do'16]) |
    do'4 do'2 do'4^\p |
    re'4 re8 re re4 re8 re |
    sol4 r r sol8.([^\f do'16]) |
    do'2. la4 |
    r la^\p la fad |
    r do'4(^\f si do') |
    si2 do'4. do'8 |
    si2^\pp \appoggiatura re'8 do'4. si8 |
    si4 r r2 |
    R1*2 |
    
  }
  \tag #'voix6 {
    \clef "bass" R1*8 |
    r2 <>^"a 2." mi4. mi8 |
    re4 sol do'4. do'8 |
    si4 si r2 |
    R1*6 |
    <>^\markup\italic { sotto vocce } si2 do' |
    la4 sol r2 |
    si2 do' |
    la4 sol r2 |
    r sol4. re8 |
    do4 r si, r |
    la4 r sol r |
    fad r r2 |
    R1*10 |
    <>^\markup\character Antonio r2 r4 re' |
    dod' la r2 |
    R1*2 |
    <>^\markup\character Antonio, Bartolo r2 r4 re' |
    dod'4 la r2 |
    R1*13 |
    r2 r4 re^\p |
    re2 re4 r |
    r2 r4 re |
    re2^\cresc re4 r |
    r2 r4 re^\f |
    re2 re4 r |
    R1*8 |
    r2^\markup\italic { (da se.) } r4 sol |
    fa r fa r |
    r2 r4 fa |
    mib r mib r |
    r2 r4 mib |
    re r re r |
    r2 r4 re |
    do r do r |
    r2 do |
    fa1 |
    sib,2 sib, |
    mib1 |
    mib2 mib |
    re4 r r2 |
    sol4 r r2 |
    fad4 r r2 |
    sol4 r r2 |
    fad2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } sol, |
    si,2 do4. do8 |
    re4 mi r mi |
    si,2 do4. do8 |
    re2. mi4 |
    do2 re4 re |
    sol, r r sol8.([^\f do'16]) |
    do'2 do'4 do'^\p |
    re' re8 re re4 re8 re |
    sol4 r r sol8.([^\f do'16]) |
    do'2. la4 |
    r la^\p la fad |
    r fad^\f( sol la) |
    sol2 do4 do |
    re2^\pp re4. re8 |
    sol,4 r r2 |
    R1*2 |
  }
  \tag #'voix7 {
    \clef "bass" R1*3 |
    re'2. re'4 |
    si sol r2 |
    R1*2 |
    re'2. re'4 |
    si sol r2 |
    R1*9 |
    <>^\markup\italic { sotto vocce } mi2 sol |
    sol4 si, r2 |
    mi2 sol |
    sol4 si, r2 |
    r sol4 r8 sol |
    do4 r8 do dod4 r8 dod |
    re4 r r2 |
    R1*12 |
    r2 r4 la |
    dod'4 dod' r2 |
    r r4 re' |
    la la r2 |
    R1*9 |
    r2 r4 la |
    la2 fad4 re' |
    re'2 la4 r |
    R1 |
    r2 r4 re^\p |
    re2 re4 r |
    r2 r4 re |
    re2^\cresc re4 r |
    r2 r4 re^\f |
    re2 re4 r |
    R1*25 |
    r2 r4\fermata r |
    R1*3 |
    r2\fermata r4\fermata r |
    R1*5 |
    r2 r4^\markup\italic { sotto vocce } sol, |
    si,2 do4. do8 |
    re4 mi r mi |
    si,2 do4. do8 |
    re2. mi4 |
    do2 re4 re |
    sol, r r sol8.([^\f do'16]) |
    do'2 do'4 do'^\p |
    re' re8 re re4 re8 re |
    sol4 r r sol8.([^\f do'16]) |
    do'2. la4 |
    r la^\p la fad |
    r fad^\f( sol la) |
    sol2 do4 do |
    re2^\pp re4. re8 |
    sol,4 r r2 |
    R1*2 |
  }
>>
%% Tutti
<<
  \tag #'(voix1 voix2) {
    R1 |
    dod''2.^\p dod''4 |
    re''2. re''4 |
    sold''2.^\f sold''4 |
    la''4 la' r2 |
    re''2^\p fa'' |
    la' re'' |
    fa' re' |
    la'4 la' r la'8 la' |
    re''4 fad''! mi'' dod'' |
    re'' la' r la'8 la' |
    re''4 fad'' mi'' dod'' |
    re'' r r2 |
    fad''2.^\f fad''4 |
    sol''2 fad'' |
    mi''2. mi''4 |
    re'' r r2 |
    mi''4 mi'' r r8 mi'' |
    fad''4 re'' r re'' |
    mi'' mi'' r mi'' |
    fad'' re'' r re''8 re'' |
    mi''4. mi''8 mi''4. mi''8 |
    fad''4 la'' fad'' re'' |
    la'' la' r2 |
    R1*4 |
    re''2.^\p la'4 |
    fad'2. la'4 |
    re''( fad'') la''( fad'') |
    mi''2 la' |
    re''2. la'4 |
    fad'2. la'4 |
    re''( fad'') la''( fad'') |
    mi''1 |
    fad''2.^\f fad''4 |
    sol''2 fad'' |
    mi''2. mi''4 |
    re'' r r2 |
    R1 |
    r2 fad'4^\p fad' |
    fad' re' r2 |
    r la'4^\cresc la' |
    la' fad' re''^\f re'' |
    re'' la' fad'' fad'' |
    fad'' re'' fad'' re'' |
    la''2. la''4 |
    la'2. la'4 |
    re''4 r r2 |
    R1 |
    r2 fad'4^\p fad' |
    fad' re' r2 |
    r la'4 la'^\cresc |
    la' fad' re''^\f re'' |
    re'' la' fad'' fad'' |
    fad'' re'' fad'' re'' |
    la''2. la''4 |
    la'2. la'4 |
    re'' r r re'' |
    mi''2. la''4 |
    fad''2 r4 re'' |
    mi''2. la''4 |
    fad''2 r4 re'' |
    mi''2. la''4 |
    re''4 r r2 |
  }
  \tag #'voix3 {
    R1 |
    sol'2.^\p sol'4 |
    fa'2. fa'4 |
    fa''2.^\f re''4 |
    dod'' la' r2 |
    re''2^\p fa'' |
    la' re'' |
    fa' re' |
    la'4 la' r la'8 la' |
    fad'!4 la' sol' mi' |
    fad'4 fad' r la'8 la' |
    fad'4 la' sol' mi' |
    fad' r r2 |
    la'2.^\f la'4 |
    si'2 la' |
    dod''2. dod''4 |
    re'' r r2 |
    dod''4 dod'' r r8 dod'' |
    re''4 la' r re'' |
    dod'' dod'' r dod'' |
    re'' la' r re''8 re'' |
    dod''4. dod''8 dod''4. dod''8 |
    re''4 la'' fad'' re'' |
    la'' la' r2 |
    R1*4 |
    la'2.^\p fad'4 |
    re'2. fad'4 |
    la'( re'') fad''( re'') |
    dod''2 la' |
    la'2. fad'4 |
    re'2. fad'4 |
    la'( re'') fad''( re'') |
    dod''1 |
    re''2.^\f la'4 |
    si'2 la' |
    dod''2. dod''4 |
    re'' r r2 |
    R1 |
    r2 re'4^\p re' |
    re' re' r2 |
    r fad'4^\cresc fad' |
    fad' re' la'^\f la' |
    la' fad' re'' re'' |
    re'' la' fad'' re'' |
    la''2. la''4 |
    la'2. la'4 |
    re''4 r r2 |
    R1 |
    r2 re'4^\p re' |
    re' re' r2 |
    r fad'4 fad'^\cresc |
    fad' re' la'^\f la' |
    la' fad' re'' re'' |
    re'' la' fad'' re'' |
    la''2. la''4 |
    la'2. la'4 |
    re''4 r r re'' |
    dod''2. dod''4 |
    re''2 r4 re'' |
    dod''2. dod''4 |
    re''2 r4 re'' |
    dod''2. dod''4 |
    re'' r r2 |
  }
  \tag #'voix4 {
    R1 |
    mi'2.^\p mi'4 |
    re'2. re'4 |
    re'2.^\f fa'4 |
    mi' dod' r2 |
    re'2^\p fa' |
    la re' |
    fa re |
    la4 la r2 |
    r r4 la8 la |
    re'4 la la dod' |
    re' la r la8 la |
    re'4 la la dod' |
    re' r re'4.^\f re'8 |
    re'2 re' |
    la'2. la4 |
    re' r r2 |
    la'4 la r r8 la' |
    la'4 fad' r fad' |
    la' la r la' |
    la' fad' r fad'8 fad' |
    la'4. la'8 la'4. la'8 |
    fad'4 la' fad' re' |
    la' la r2 |
    R1*4 |
    fad'2.^\p re'4 |
    la2. re'4 |
    fad'2. la'4 |
    la'2 la |
    fad'2. re'4 |
    la2. re'4 |
    fad'2. la'4 |
    la'1 |
    la'2.^\f re'4 |
    re'2 re' |
    la'2. la4 |
    re' r r2 |
    r fad4^\p fad |
    fad re r2 |
    r la4 la^\cresc |
    la fad r2 |
    r fad'4^\f fad' |
    fad' re' la' la' |
    la' fad' fad' re' |
    la'2. la'4 |
    la2. la4 |
    re'4 r r2 |
    r fad4^\p fad |
    fad re r2 |
    r la4 la^\cresc |
    la fad r2 |
    r fad'4^\f fad' |
    fad' re' la' la' |
    la'4 fad' fad' re' la'2. la'4 |
    la2. la4 |
    re' r r fad' |
    la'2. la'4 |
    la'2 r4 fad' |
    la'2. la'4 |
    la'2 r4 fad' |
    la'2. la'4 |
    fad' r r2 |
  }
  \tag #'voix5 {
    R1 |
    la2.^\p la4 |
    la2. la4 |
    si2.^\f si4 |
    mi la r2 |
    re'2^\p fa' |
    la re' |
    fa re |
    la4 la r2 |
    r r4 sol8 sol |
    fad!4 fad sol sol |
    fad fad r sol8 sol |
    fad4 fad sol sol |
    fad r re4.^\f re8 |
    sol2 re |
    la2. la4 |
    re r r2 |
    la4 la r r8 la |
    re'4 re r re |
    la la r la |
    re' re r re8 re |
    la4. la8 la4. la8 |
    re'4 la fad re |
    la la r2 |
    R1*4 |
    r2 la^\p~ |
    la4 re' la2~ |
    la4 la la2~ |
    la4 la dod' la |
    re' re la2~ |
    la4 re' la2~ |
    la4 la la2~ |
    la4 la dod' la |
    re'2 re'4 re' |
    sol2 re' |
    la2. la4 |
    fad r r2 |
    r2 fad4^\p fad |
    fad re r2 |
    r la4 la^\cresc |
    la fad r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la2. la4 |
    re'4 r r2 |
    r fad4^\p fad |
    fad re r2 |
    r la4 la^\cresc |
    la4 fad r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la2. la4 |
    re' r r re' |
    la2. la4 |
    re'2 r4 re' |
    la2. la4 |
    re'2 r4 re' |
    la2. la4 |
    re4 r r2 |
  }
  \tag #'voix6 {
    R1 |
    la2.^\p la4 |
    la2. la4 |
    la2.^\f la4 |
    la la r2 |
    re2^\p fa |
    la, re |
    fa re |
    la4 la r2 |
    r r4 re8 re |
    re4 fad! mi la, |
    re re r re8 re |
    re4 fad mi la, |
    re r re4.^\f re8 |
    sol2 re |
    la2. la4 |
    re r r2 |
    la4 la r r8 la |
    re'4 re r re |
    la la r la |
    re' re r re8 re |
    la4. la8 la4. la8 |
    re'4 la fad re |
    la la r2 |
    R1*4 |
    r2 fad^\p~ |
    fad4 la fad2~ |
    fad4 la re fad |
    la2 la, |
    r2 fad~ |
    fad4 la fad2~ |
    fad4 la re fad |
    la1 |
    re2. re4 |
    sol2 re |
    la2. la4 |
    re r r2 |
    r re4^\p re |
    re re r2 |
    r fad4 fad^\cresc |
    fad re r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la,2. la,4 |
    re4 r r2 |
    r2 re4^\p re |
    re re r2 |
    r fad4 fad^\cresc |
    fad re r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la,2. la,4 |
    re r r re |
    la2. la4 |
    re'2 r4 re |
    la2. la4 |
    re'2 r4 re |
    la2. la4 |
    re r r2 |
  }
  \tag #'voix7 {
    R1 |
    la2.^\p la4 |
    la2. la4 |
    la2.^\f la4 |
    la la r2 |
    re2^\p fa |
    la, re |
    fa re |
    la,4 la, r2 |
    r r4 re8 re |
    re4 fad! mi la, |
    re re r re8 re |
    re4 fad mi la, |
    re r re4.^\f re8 |
    sol2 re |
    la2. la4 |
    re r r2 |
    la4 la r r8 la |
    re'4 re r re |
    la la r la |
    re' re r re8 re |
    la4. la8 la4. la8 |
    re'4 la fad re |
    la la r2 |
    R1*4 |
    r2 re^\p~ |
    re4 la, re2~ |
    re4 la, re fad |
    la2 la, |
    r re~ |
    re4 la, re2~ |
    re4 la, re fad |
    la1 |
    re2. re4 |
    sol2 re |
    la2. la4 |
    re r r2 |
    r re4^\p re |
    re re r2 |
    r fad4 fad^\cresc |
    fad re r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la,2. la,4 |
    re4 r r2 |
    r2 re4^\p re |
    re re r2 |
    r fad4 fad^\cresc |
    fad re r2 |
    r re'4^\f re' |
    re' re re' re' |
    re' re fad re |
    la2. la4 |
    la,2. la,4 |
    re r r re |
    la2. la4 |
    re'2 r4 re |
    la2. la4 |
    re'2 r4 re |
    la2. la4 |
    re r r2 |
  }
>>
R1*8 |
