\clef "treble" \transposition sol
\ru#3 {
  <>\f <<
    \tag #'(corno1 corni) { do''4 }
    \tag #'(corno2 corni) { do' }
  >> r4 r2 |
}
<>\p <<
  \tag #'(corno1 corni) { re''1 }
  \tag #'(corno2 corni) { sol' }
>>
\ru#3 {
  <>\f <<
    \tag #'(corno1 corni) { do''4 }
    \tag #'(corno2 corni) { do' }
  >> r4 r2 |
}
<>\p <<
  \tag #'(corno1 corni) {
    re''1 |
    do''2 do''4. do''8 |
    do''2 do''4. do''8 |
    do''4 sol' do'' mi'' |
    sol''2 s |
    s sol''4 s |
    s2 sol''4 s |
    do''2 re'' |
    mi'' re'' |
    do''4
  }
  \tag #'(corno2 corni) { 
    sol'1 |
    do'2 do'4. do'8 |
    do'2 do'4. do'8 |
    do'4 sol do' mi' |
    sol'2 s |
    s sol'4 s |
    s2 sol'4 s |
    mi'2 sol' |
    do'' sol' |
    do'4
  }
  { s1 | s1\f | s1*2 |
    s2 r | r s4 r | r2 s4 r |
    s2\sf s\sf | s2\sf s\sf | }
>> r4 r2 |
R1*7 |
r4 <>\f <<
  \tag #'(corno1 corni) { re''4 re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol' sol' | }
>>
<>\p \twoVoices#'(corno1 corno2 corni) <<
  { re''1~ | re''4 }
  { re''1 | sol'4 }
>> <>\f <<
  \tag #'(corno1 corni) { re''4 re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol' sol' | }
>>
<>\p \twoVoices#'(corno1 corno2 corni) <<
  { re''1~ | re''4 }
  { re''1 | sol'4 }
>> <>\f <<
  \tag #'(corno1 corni) { sol'4 sol' sol' | sol' }
  \tag #'(corno2 corni) { sol4 sol sol | sol }
>> r4 r2 |
R1*6 | \allowPageTurn
\tag#'corni <>^"a 2." re''1\p~ |
re''~ |
re''~ |
re''~ |
re''4 re''\f re'' re'' |
re''2\f r |
r re''4 r |
r2 re''4 r |
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 mi''~ | mi'' re''~ | re''4 }
  { sol'1 | do''2 re'' | sol'4 }
  { s4\p s2.\cresc | s1\f }
>> r4 <<
  \tag #'(corno1 corni) { sol'2~ | sol'1~ | sol' | }
  \tag #'(corno2 corni) { sol2~ | sol1~ | sol | }
>>
<>\f \twoVoices#'(corno1 corno2 corni) <<
  { re''1~ | re''4 }
  { re''1 | sol'4 }
>> r4 <<
  \tag #'(corno1 corni) { sol'2~ | sol'1~ | sol' | }
  \tag #'(corno2 corni) { sol2~ | sol1~ | sol | }
>>
<>\f \twoVoices#'(corno1 corno2 corni) <<
  { re''1~ | re''4 }
  { re''1 | sol'4 }
>> r4 <<
  \tag #'(corno1 corni) {
    sol'2~ | sol'2. sol'4 |
    sol'2~ sol'~ |
    sol'2. sol'4 |
    sol'2~ sol'~ |
    sol'2.
  }
  \tag #'(corno2 corni) {
    sol2~ |
    sol2. sol4 |
    sol2~ sol~ |
    sol2. sol4 |
    sol2~ sol~ |
    sol2.
  }
  { s2\p | s2. s4\f | s2 s\p | s2. s4\f | s2 s\p | }
>> r4 |
R1*3 |
<<
  \tag #'(corno1 corni) { sol'1~ | sol' | }
  \tag #'(corno2 corni) { sol1~ | sol | }
  { s2.\sf s4\p | }
>>
R1*2 |
<>\pp <<
  \tag #'(corno1 corni) {
    do''1~ | do'' | sol'~ | sol' |
    do''~ | do''~ | do''~ | do''~ |
    do''~ | do'' |
  }
  \tag #'(corno2 corni) {
    do'1~ | do' | sol~ | sol |
    do'~ | do'~ | do'~ | do'~ |
    do'~ | do' |
  }
>>
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol'~ | sol'~ | sol'2 }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol~ | sol~ | sol2 }
>> r4\fermata r |
R1*3 |
r2\fermata r4\fermata r |
R1*5 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do'' |
    re''4 do''2.~ |
    do''1 |
    re''2.( do''4) |
    s2 sol'4 sol' |
    sol' s s do'' |
    do''1 |
    sol'~ |
    sol'4 s s do'' |
    do''2 s |
    s s4 sol' |
    sol'1 |
    do'' |
    sol'~ |
    sol'4
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do' |
    sol'4 do'2.~ |
    do'1 |
    sol'2.( do'4) |
    s2 sol4 sol |
    do' s s do' |
    do'1 |
    sol |
    do'4 s s do' |
    do'2 s |
    s s4 sol |
    sol1 |
    do' |
    sol |
    do'4
    
  }
  { s1\pp |
    s1*3 |
    s4 s2.\cresc |
    r2\! s\pp |
    s4 r r s\f |
    s1\decresc |
    s\pp |
    s4 r r s\f |
    s2\p r |
    r r4 s\f |
    s1*2 |
    s1\pp | }
>> r4 r2 |
R1*2 |
%%
R1*3 |
<>\f <<
  \tag #'(corno1 corni) { sol''1 | re'' | }
  \tag #'(corno2 corni) { sol'1 | re'' | }
>>
R1*5 |
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 re'' |
    s1 |
    re''2 re'' |
    re''1 | }
  { sol'2 re'' |
    s1 |
    sol'2 re'' |
    sol'1 | }
  { s1\f | R1 | s | s\f | }
>>
<<
  \tag #'(corno1 corni) { mi''2 re'' | re''1 }
  \tag #'(corno2 corni) { do''2 sol' | re''1 | }
>>
<<
  \tag #'(corno1 corni) { re''2 s4 re'' | }
  \tag #'(corno2 corni) { sol'2 s4 sol' | }
  { s2 r4 s | }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 s4 re'' | }
  { re''2 s4 re'' | }
  { s2 r4 s | }
>>
<<
  \tag #'(corno1 corni) { re''2 s4 re'' | }
  \tag #'(corno2 corni) { sol'2 s4 sol' | }
  { s2 r4 s | }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 s4 re'' | }
  { re''2 s4 re'' | }
  { s2 r4 s | }
>>
<<
  \tag #'(corno1 corni) { re''2 s4 re'' | re''1 | re''2 }
  \tag #'(corno2 corni) { sol'2 s4 sol' | re''1 | sol'2 }
  { s2 r4 s | }
>> r4
\twoVoices#'(corno1 corno2 corni) <<
  { sol'4 | re'' re'' re'' }
  { sol'4 | re'' re'' re'' }
>> r4 |
R1*12 |
<>\f <<
  \tag #'(corno1 corni) {
    re''4 re''8 re'' re''4 s |
    mi''4 mi''8 mi'' re''4 re''8 re'' |
  }
  \tag #'(corno2 corni) {
    sol'4 sol'8 sol' sol'4 s |
    do''4 do''8 do'' sol'4 sol'8 sol' |
  }
  { s2. r4 }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''4 re''8 re'' re''4 re''8 re'' | }
  { re''4 re''8 re'' re''4 re''8 re'' | }
>>
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4 sol' sol' sol' |
  }
  \tag #'(corno2 corni) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol4 sol sol sol |
  }
  { s1\p | s | s4 s2.\cresc | s1 | s\f }
>>
\tag#'corni <>^"a 2." re''4 re''8 re'' re''4 re'' |
re'' re''8 re'' re''4 re'' |
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4 sol' sol' sol' |
  }
  \tag #'(corno2 corni) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol4 sol sol sol |
  }
  { s1\p | s | s4 s2.\cresc | s1 | s\f }
>>
\tag#'corni <>^"a 2." re''4 re''8 re'' re''4 re'' |
re'' re''8 re'' re''4 re'' |
<<
  \tag #'(corno1 corni) { re''2 s4 re''4 | }
  \tag #'(corno2 corni) { sol'2 s4 sol' | }
  { s2 r4 s | }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 s4 re'' | }
  { re''2 s4 re'' | }
  { s2 r4 s | }
>>
<<
  \tag #'(corno1 corni) { re''2 s4 re''4 | }
  \tag #'(corno2 corni) { sol'2 s4 sol' | }
  { s2 r4 s | }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 s4 re'' | }
  { re''2 s4 re'' | }
  { s2 r4 s | }
>>
<<
  \tag #'(corno1 corni) { re''2 s4 re''4 | }
  \tag #'(corno2 corni) { sol'2 s4 sol' | }
  { s2 r4 s | }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { re''2 s4 re'' | }
  { re''2 s4 re'' | }
  { s2 r4 s | }
>>
<<
  \tag #'(corno1 corni) {
    re''4 s re'' re'' |
    re'' s re'' re'' |
    re'' s re'' re'' |
    re'' re'' re'' re'' |
    re'' sol'8 sol' sol'4 sol' |
    sol' s sol' s |
    sol'2 s4 sol'8 sol' |
    sol'2 s4 sol'8 sol' |
    sol'2
  }
  \tag #'(corno2 corni) {
    sol'4 s sol' sol' |
    sol' s sol' sol' |
    sol' s sol' sol' |
    sol' sol' sol' sol' |
    sol' sol8 sol sol4 sol |
    sol s sol s |
    sol2 s4 sol8 sol |
    sol2 s4 sol8 sol |
    sol2
  }
  { s4 r s2 |
    s4 r s2 |
    s4 r s2 |
    s1*2 |
    s4 r s r |
    s2 r4 s |
    s2 r4 s | }
>> r2 |
