\clef "alto" sol'4\f r r2 |
sol'4 r r2 |
sol'4 r r2 |
re'1\p |
sol'4\f r r2 |
sol'4 r r2 |
sol'4 r r2 |
re'1\p |
sol'2:8\f sol':8 |
sol':8 sol':8 |
sol'4 re8 re sol sol si si |
re'2\sf do'4.(\p la8) |
sol4( si) re'4(\sf do'8 la) |
sol4(\p si) re'4(\sf do'8 la) |
si2:8\sf re':8\sf |
re':8\sf fad':\sf |
sol'4 r r2 |
R1 |
mi'2(\p sol') |
sol'4-. si-. r2 |
mi'2( sol') |
sol'4-. si-. <<
  { re'2~ | re'1 | } \\ { re'2( | do' si) | }
>>
<la do'>2( <si sol>) |
<fad la>4 re'\f fad' re' |
la'1\p |
r4 re'\f fad' re' |
la'1\p |
r4 re8\cresc re fad fad la la |
re'2:8\sf dod'8\p dod' re' re' |
si2:8\sf lad8\p lad si si |
fad2:8\sf sold:8\sf |
la!8 dod'\p dod' dod' dod'2:8 |
re':8 re':8 |
mi':8 mi':8 |
re':8 re':8 |
dod':8 dod':8 |
re':8 re':8 |
mi':8 mi':8 |
re':8 re':8 |
dod'8 dod' la\cresc la dod' dod' mi' mi' |
la2\sf sol4.(\p mi8) |
re4( fad) la(\sf sol8 mi) |
re4(\p fad) la(\sf sol8 mi) |
re\p re' re'\cresc re' re'2:8 |
si':8\f la':8 |
la'8 la(\p fad la fad la fad la) |
fad( la fad la fad la fad la) |
fad( la fad la fad la fad la) |
<la sol'>2:8\f q:8 |
<la fad'>8 la(\p fad la fad la fad la) |
fad( la fad la fad la fad la) |
fad( la fad la fad la fad la) |
la2:8\f la:8 |
la8 re'\p re' re' re'2:8 |
re':8 re':8 |
re':8\f re':8\p |
re':8 re':8\f |
re':8 re':8\p |
re'2.:8 do'!16(\f si do' re') |
mi'2:8\sf mi':8 |
la:8\sf do':8\sf
fad:8\sf la:8\sf |
do1:8\sf |
re'1\p~ |
re'4 r do' r |
si r la r |
sol\pp r r2 |
sib4 r r2 |
la4 r r2 |
la4 r r2 |
sib4 r r2 |
sol4 r r2 |
si!4 r r2 |
fa4 r r2 |
sol4 r r2 |
mib4 r r2 |
do'4 r r2 |
re'4 r r2 |
sib1( |
la2 sol) |
la4 r r2 |
sol4 r r2 |
la4 r r2 |
sol4 r r2 |
fad2 r4\fermata r |
sol2(\p do'4 si!) |
si( la) r2 |
la2( sol4 fad) |
fad\fermata sol\fermata r4\fermata r | \allowPageTurn
si2(\p do') |
re'4( mi') r2 |
si2( do') |
re'2.( mi'4) |
do'2( re') |
si4 r r sol\pp |
si2 do'4( do) |
re( mi) r mi |
si2 do'4( do) |
re2.*1/3( s2\cresc mi4) |
do2(\p re) |
sol4 r r sol8.(\f sol'16) |
sol'1\decresc |
sol'4\p re'2( do'4) |
si4 r r sol8.(\f do'16) |
do'4 do r do'\p |
do'( la) r2 |
r4 la'(\f sol' fad') |
sol' sol'2 sol'4 |
re'\p re' <do' re'> q |
<si re'> r8 si(\pp sol4) r8 fad( |
mi4) r8 sol( mi4) r8 re( |
dod4) r8 mi'( dod'4) r8 si( |
la2:8\p) la:8 |
mi'1( |
re') |
la2:8\f la:8 |
la:8 la:8 |
re'2\p fa'4.( re'8) |
la2 re'4.( la8) |
fa4.( la8) fa4.( re8) |
la4 la la r |
la1~ |
la4 la2\sf la4 |
la1\p~ |
la4 la2\sf la4 |
re'2:8\f re':8 |
sol':8 re':8 |
la':8 la:8 |
re'4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
r r8 \tuplet 3/2 { mi'16([ fad' sold'] } la'4) la |
r4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
r r8 \tuplet 3/2 { mi'16([ fad' sold'] } la'4) la |
r4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
la'2:8 la':8 |
re''8 re'' la' la' fad' fad' re' re' |
la'4 la la r |
re'8\p la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
re'8 la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
re'8 la re' la re'4 r |
re'8 la re' la re'4 r |
re'8 la re' la fad' re' fad' re' |
la'4 la8 la la4 r |
re'4\f re'8 re' re'4 r |
sol'4 sol'8 sol' re'4 re'8 re' |
la'4 la'8 la' la4 la8 la |
re' la\p si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si\cresc dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re'\f la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' re' |
la'2:8 la':8 |
la:8 la:8 |
re'8 la\p si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re' la si\cresc dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
re'\f la si dod' re' mi' fad' mi' |
re' la si dod' re' mi' fad' mi' |
la'2:8 la':8 |
la:8 la:8 |
re'4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
r r8 \tuplet 3/2 { mi'16([ fad' sold'] } la'4) la |
r4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
r r8 \tuplet 3/2 { mi'16([ fad' sold'] } la'4) la |
r4 r8 \tuplet 3/2 { la'16([ si' dod''] } re''4) re' |
la'2:8 la':8 |
re'2:8 re':8 |
re':8 re':8 |
re':8 re':8 |
re':8 re':8 |
re'4 re'8 re' re'4 re' |
re' la re' la |
re'2 r4 re8 re |
re2 r4 re8 re |
re2 r |
