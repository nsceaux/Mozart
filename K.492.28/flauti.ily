\clef "treble"
<>\f <<
  \tag #'(flauto1 flauti) {
    sol''4 s2. |
    si''4 s2. |
    re'''4 s2. |
  }
  \tag #'(flauto2 flauti) {
    re''4 s2. |
    sol''4 s2. |
    si''4 s2. |
  }
  \ru#3 { s4 r r2 | }
>>
R1 |
<>\f <<
  \tag #'(flauto1 flauti) {
    sol''4 s2. |
    si''4 s2. |
    re'''4 s2. |
  }
  \tag #'(flauto2 flauti) {
    re''4 s2. |
    sol''4 s2. |
    si''4 s2. |
  }
  \ru#3 { s4 r r2 | }
>>
R1 |
<>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''2 }
  { sol'' }
>> <<
  \tag #'(flauto1 flauti) {
    do'''2 |
    si'' mi''' |
    re'''4
  }
  \tag #'(flauto2 flauti) {
    mi''2 |
    re'' do''' |
    si''4
  }
>> \tag#'flauti <>^"a 2." re'' sol'' si'' |
re'''2\sf do'''4.(\p la''8) |
sol''4( si'') re'''4(\sf do'''8 la'') |
sol''4(\p si'') re'''4(\sf do'''8 la'') |
sol''2\sf <<
  \tag #'(flauto1 flauti) { la''2 | si'' do''' | re'''4 }
  \tag #'(flauto2 flauti) { re''2 | sol'' fad'' | sol''4 }
  { s2\sf | s\sf s\sf }
>> r4 r2 |
R1*5 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { fad''2(\p sol'') | }
  { R1 }
>>
<<
  \tag #'(flauto1 flauti) {
    do'''2( si'') |
    la''4 la''2 la''4 |
    dod'''1 |
    re'''4 la''2 la''4 |
    dod'''1 |
    re'''4
  }
  \tag #'(flauto2 flauti) {
    la''2( sol'') |
    fad''4 fad''2 fad''4 |
    sol''1 |
    fad''4 fad''2 fad''4 |
    sol''1 |
    fad''4
  }
  { s1 | s4 s2.\f | s1\p | s4 s2.\f | s1\p | }
>> \tag#'flauti <>^"a 2." re''4\f fad'' la'' |
re'''2(\sf dod'''4\p re''') |
si''2(\sf lad''4\p si'') |
fad''2\sf sold''\sf |
la''!4 r r2 |
R1*7 |
r4 <>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { la'4 dod'' mi'' | }
  { la'4 dod'' mi'' | }
>>
<<
  \tag #'(flauto1 flauti) {
    la''2.( dod'''4) |
    re'''4 la''2 dod'''4 |
    re''' la''2 dod'''4 |
    re'''2
  }
  \tag #'(flauto2 flauti) {
    la'2.( sol''4) |
    fad''4 la'2 sol''4 |
    fad'' la'2 sol''4 |
    fad''2
  }
  { s1\sf | s4 s2\sf s4\p | s4 s2\sf s4\p | s4 s\cresc }
>> \twoVoices #'(flauto1 flauto2 flauti) <<
  { fad'''2 | mi'''1 | re'''4 }
  { re'''2~ | re'''( dod''') | re'''4 }
  { s2 | s1\f }
>> r4 r2 |
R1*2 |
<>\f <<
  \tag #'(flauto1 flauti) { sol''1( | fad''4) }
  \tag #'(flauto2 flauti) { mi''1( | re''4) }
>> r4 r2 |
R1*2 |
<>\f <<
  \tag #'(flauto1 flauti) { dod'''1( | re'''4) }
  \tag #'(flauto2 flauti) { sol''1( | fad''4) }
>> r4 r2 |
r2 r4 <>\f <<
  \tag #'(flauto1 flauti) { la''4-. | si''-. si''-. }
  \tag #'(flauto2 flauti) { fad''4-. | sol''-. sol''-. }
>> r2 |
r2 r4 <>\f <<
  \tag #'(flauto1 flauti) { si''4 | do'''! do''' }
  \tag #'(flauto2 flauti) { sol'' | la'' la'' }
>> r2 |
r2 \tag#'flauti <>^"a 2." r4 do'''16(\f si'' do''' re''' |
mi'''1)-.\sf |
la''2-.\sf do'''-.\sf |
fad''-.\sf la''-.\sf |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do''2. la''4\p | la''( si'' do''' dod''') | }
  { do''1 | R1 | }
  { s1\sf }
>>
<>\p <<
  \tag #'(flauto1 flauti) { re'''2( do'''!) | si''( la'') | }
  \tag #'(flauto2 flauti) { sol''2( la'') | sol''( fad'') | }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''4 } { sol'' }
>> r4 r2 |
R1 |
r2 r4 <>\pp <<
  \tag #'(flauto1 flauti) {
    re'''4 |
    re''' s re''' s |
    s2 s4 sol''' |
    sol''' s sol''' s |
    s2 s4 si''! |
    si'' s si'' s |
    s2 s4 mib''' |
    mib''' s mib''' s |
  }
  \tag #'(flauto2 flauti) {
    la''4 |
    la'' s la'' s |
    s2 s4 sib'' |
    sib'' s sib'' s |
    s2 s4 fa'' |
    fa'' s fa'' s |
    s2 s4 sol'' |
    sol'' s sol'' s |
  }
  { s4 | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r |
    r2 r4 s | s r s r | }
>>
R1*4 | \allowPageTurn
<>\p \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''1~ | re'''~ | re'''~ | re'''~ | re'''2 }
  { la''1( | sib'' | la'' | sib'') | la''2 }
>> r4\fermata r |
R1*3 |
r2\fermata r4\fermata r |
R1*5 |
r2 r4 <>\pp \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''4 |
    re'''2( mi''') |
    re'''8( do''') si''4 s sol'' |
    re'''2( mi''') |
    re'''4( do'''2 si''4) | }
  { sol''4 |
    sol''1 |
    fad''4( sol'') s sol'' |
    sol''1 |
    fad''2.( sol''4) | }
  { s4 | s1 | s2 r4 s | s1 | s4 s2.\cresc | }
>>
<>\pp <<
  \tag #'(flauto1 flauti) { do'''2( si''4 la'') | }
  \tag #'(flauto2 flauti) { la''2( sol''4 fad'') | }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''4 } { sol'' }
>> r4 r2 |
r2 r4 <>\pp <<
  \tag #'(flauto1 flauti) {
    mi'''4 |
    \appoggiatura mi'''8 re'''4( si''8 re''') \appoggiatura re'''8 do'''4( si''8 la'') |
    si''4
  }
  \tag #'(flauto2 flauti) {
    do'''4 |
    \appoggiatura do'''8 si''4( sol''8 si'') \appoggiatura si''8 la''4( sol''8 fad'') |
    sol''4
  }
>> r4 r2 |
r2 r4 <>\pp <<
  \tag #'(flauto1 flauti) { mi'''4 | mi'''( do''') }
  \tag #'(flauto2 flauti) { do'''4 | do'''4( la'') }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { r4 re''\f | re'''1~ | re'''4.( red'''8) }
  { r2 | r4 do'''(\f si'' do''') | si''2 }
>> <<
  \tag #'(flauto1 flauti) { mi'''4( fad'''8 sol''') | }
  \tag #'(flauto2 flauti) { do'''4( re'''!8 mi''') | }
>>
<>\pp \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''2( \grace si''8 la''4. sol''8) | sol''4 }
  { re''2( fad''4. sol''8) | sol''4 }
>> r4 r2 |
R1 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi'''2.(\pp re'''4) | dod'''4 r r2 | R1*2 | }
  { R1*4 | }
>>
<>\f <<
  \tag #'(flauto1 flauti) { fa'''1 | mi''' | }
  \tag #'(flauto2 flauti) { re'''1 | dod''' | }
>>
R1*3 |
r2 r4 <>\p \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''4 } { la'' }
>>
<<
  \tag #'(flauto1 flauti) { re'''4( fad'''! mi''' dod''') | re'''4 }
  \tag #'(flauto2 flauti) { fad''!4( la'' sol'' mi'') | fad''4 }
  { s1 | s4\f }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''2( dod'''4) | }
  { re''4( sol''2) | }
>>
<<
  \tag #'(flauto1 flauti) { re'''4( fad''' mi''' dod''') | re'''4 }
  \tag #'(flauto2 flauti) { fad''4( la'' sol'' mi'') | fad''4 }
  { s1\p | s4\f }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''2( dod'''4) | re'''1~ | re''' }
  { re''4( sol''2) | fad''2 la'' | si'' la'' | }
>>
<<
  \tag #'(flauto1 flauti) {
    dod'''1 |
    re'''2 s4 re''' |
    dod'''2 s4 dod''' |
    re'''2 s4 re''' |
    dod'''2 s4 dod''' |
    re'''2 s4 re''' |
    dod'''1 |
    re'''4
  }
  \tag #'(flauto2 flauti) {
    la''1 |
    la''2 s4 la'' |
    la''2 s4 la'' |
    la''2 s4 la'' |
    la''2 s4 la'' |
    la''2 s4 la'' |
    la''1~ |
    la''4
  }
  { s1 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag #'flauti <>^"a 2." la''4 fad'' re'' |
la'' la'' la'' r |
re''2\p~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(flauto1 flauti) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(flauto2 flauti) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
\tag #'flauti <>^"a 2." re''2~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(flauto1 flauti) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(flauto2 flauti) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
\tag #'flauti <>^"a 2." re''2~ re''8 la' fad' la' |
re''2~ re''8 fad'' la'' fad'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 mi'' fad'' sol'' }
  { re'' dod'' re'' mi'' }
>> <<
  \tag #'(flauto1 flauti) {
    la''8 si'' dod''' re''' |
    re'''( dod''') si''-. la''-. la''( sol'') fad''-. mi''-. |
  }
  \tag #'(flauto2 flauti) {
    fad''8 sol'' mi'' fad'' |
    fad''( mi'') sol''-. fad''-. fad''( mi'') re''-. dod''-. |
  }
>>
<>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { re''2 fad''' |
    sol''' fad''' |
    mi'''1 |
    re'''4 }
  { re''2 re'''~ |
    re'''1 |
    dod''' |
    re'''4 }
>> r4 r2 |
R1 |
<<
  \tag #'(flauto1 flauti) {
    re''1~ |
    re'' |
    fad''~ |
    fad'' |
    la'' |
    re''' |
    fad''' |
    mi''' |
  }
  \tag #'(flauto2 flauti) {
    la'1~ |
    la' |
    re''~ |
    re'' |
    fad'' |
    la'' |
    re''' |
    dod''' |
  }
  { s1\p | s | s4 s2.\cresc | s1 | s\f | s1*3 | }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''4 } { re''' }
>> r4 r2 |
R1 |
<<
  \tag #'(flauto1 flauti) {
    re''1~ |
    re'' |
    fad''~ |
    fad'' |
    la'' |
    re''' |
    fad''' |
    mi''' |
  }
  \tag #'(flauto2 flauti) {
    la'1~ |
    la' |
    re''~ |
    re'' |
    fad'' |
    la'' |
    re''' |
    dod''' |
  }
  { s1\p | s | s4 s2.\cresc | s1 | s\f | s1*3 | }
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''2 } { re''' }
>> r4 <<
  \tag #'(flauto1 flauti) {
    re'''4 |
    mi'''2 s4 mi''' |
    fad'''2 s4 re''' |
    mi'''2 s4 mi''' |
    fad'''2 r4 re''' |
    mi'''2 s4 mi''' |
    re'''1 |
    fad''' |
    re'''~ |
    re'''4 fad''' re''' fad''' |
    re'''4
  }
  \tag #'(flauto2 flauti) {
    la''4 |
    dod'''2 s4 dod''' |
    re'''2 s4 la'' |
    dod'''2 s4 dod''' |
    re'''2 s4 la'' |
    si''2 s4 dod''' |
    \tag#'flauti \once\tieDown re'''1~ |
    re''' |
    la''~ |
    la''4 re''' la'' re''' |
    la''
  }
  { s4 |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s |
    s2 r4 s | }
>> \tag#'flauti <>^"a 2." re'''8 re''' re'''4 re''' |
re''' la'' re''' la'' |
re'''2 r4 re'''8 re''' |
re'''2 r4 re'''8 re''' |
re'''2 r |
