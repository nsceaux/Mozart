\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'fagotto1 \includeNotes "fagotti"
    >>
    \new Staff <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'fagotto2 \includeNotes "fagotti"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
