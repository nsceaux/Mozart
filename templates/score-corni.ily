\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
           \keepWithTag #(*tag-global*) \global
           \keepWithTag #'corno1 \includeNotes "corni"
    >>
    \new Staff <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'corno2 \includeNotes "corni"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
