\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
           \keepWithTag #(*tag-global*) \global
           \keepWithTag #'flauto1 \includeNotes "flauti"
    >>
    \new Staff <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'flauto2 \includeNotes "flauti"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
