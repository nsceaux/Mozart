\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new PianoStaff \with {
      instrumentName = "Pianoforte"
    } <<
      \new Staff << \keepWithTag #(*tag-global*) \global \includeNotes "piano1" >>
      \new Staff << \keepWithTag #(*tag-global*) \global \includeNotes "piano2" >>
    >>
  >>
  \layout { indent = \largeindent }
}
