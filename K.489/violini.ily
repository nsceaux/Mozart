\clef "treble"
\omit TupletBracket
<<
  \tag #'violino1 {
    r4 sold'8.(\p la'32 si') la'8 r la'8.( si'32 dod'') |
    si'8 r re''8.( mi''32 fad'') fad''16( mi'') re''-. dod''-. dod''( si') la'-. sold'-. |
    la'8( mi''4 dod''8) r re''( si' sold') |
    r16 la'( dod'' la') mi''( dod'') la'-. la'-. r8 si' si' si' |
    r dod'' dod'' dod'' r16 re''( si'') la''-. sold''-. fad''-. mi''-. re''-. |
    r8 dod''( re'' fad'') r la'-. la'( sold') la' mi' dod' r si' r si' r |
    <la' la''>8.\fp( \tuplet 3/2 { sold''32 fad'' mi'' } red''16) red''(-. red''-. red''-.) mi''8( re'' dod'' sid') |
    r dod'' r red'' r mi'' r dod'' |
    r sold'( la' fad') mi'8 sold''16.( mi''32) si''8 r |
    r8 la''16.( fad''32) si''8 r r sold''16.( mi''32) si''8 r |
    r fad''16.( si'32) la''8 r r mi'16( fad'\cresc sold' la' si' dod'') |
    re''!\f re'8 re' re' re'16 re'2\p |
    sold'( la'16) mi''( la'' sold'' fad'' mi'' re'' dod'') |
    r8 re''(\pp mi'' fad'') r la' r si' |
    r16 mi''( dod'''\cresc si'' la'' sold'' fad'' mid'') r8 fad''(\p sold'' la'') |
    r dod'' r si' la'4( dod'' |
    re'' sold') la'( dod'') |
    re''8-. fad''-. la''-. fad''-. mi''4 r |
    r8 dod''( dod'''4)~ dod'''8( si'') r4 |
    r8 si'( si''4)~ si''8( la'') r4 |
    r8 re' re''4~ re''16 sold''( si'' la'' sold'' fad'' mi'' re'') |
    dod''( la' la'' sold'' la'' la' la'' sold'') fad''8 fad''16( mi'') mi''( re'') re''( dod'') |
    si'8 r sold'8.( la'32 si' la'8) r la'8.( si'32 dod'' |
    si'8) r si'8.(\pp dod''32 re'') dod''16( la' dod'' la' re'' si' re'' si') |
    mi''4.( si'8) dod''16( sold' la' dod'') re''( lad' si' re'') |
    mi''2~ mi''8 la''( sold'') re''( |
    dod'') la''( sold'') re''( dod'')[
  }
  \tag #'violino2 {
    r4 si8.\p( dod'32 re') dod'8 r dod'8.( si32 la) |
    sold8 r si8.( dod'32 re') re'16( dod') fad'-. mi'-. mi'( re') dod'-. si-. |
    dod'8-. dod'( mi' sol') r fad'( re' si) |
    r16 dod'( mi' dod') dod''( la') dod'-. dod'-. r8 sold'! sold' sold' |
    r la' la' la' r16 si'( re'') dod''-. si'-. la'-. sold'-. si'-. |
    r8 la'4 la'8 r dod'-. dod'( re') |
    dod' dod' la r sold' r sold' r |
    red'8.(\fp \tuplet 3/2 { mi'32 fad' sold' } la'16) la'-.( la'-. la'-.) si'8 mi'4 mi'8 |
    r mi' r si' r si' r la' |
    r mi'( fad' red') mi'16(\pp sold') si-. si-. mi'( sold') si-. si-. |
    fad'\p( la') si-. si-. fad'( la') si-. si-. mi'( sold') si-. si-. mi'( sold') si-. si-. |
    fad'( la') si-. si-. fad'( la') si-. si-. r8 mi'16( red'\cresc mi' fad' sold' mi') |
    la'\f la8 la la la16 si2\p |
    re'( dod'16) mi'( la' sold' fad' mi' re' dod') |
    r8 la'4 la'8 r dod' r sold' |
    r16 mi'( dod''\cresc si' la' sold' fad' mid') r8 fad'(\p sold' la') |
    r la' r sold' la'16( mi'' dod'' la' sol' mi' la' sol') |
    fad'( la' fad' re' dod' re' mi' re') dod'( mi'' dod'' la' sol' mi' la' sol') |
    fad'( la' re'' la' fad' la' re'' la') dod''( la' mi' re' dod'8) r |
    r2 r8 fad''( fad''4)~ |
    fad''( mi'') r8 mi'( mi''4)~ |
    mi''16 la'( si' dod'' re'' mi'' dod'' re'') si'2 |
    la'16-. la( la' sold' la' la la' sold') fad'8 re''16( dod'') dod''( si') si'( la') |
    sold'8 r si8.( dod'32 re' dod'8) r dod'8.( si32 la |
    sold8) r sold'8.\pp( la'32 si') la'8 r r si |
    dod'16( la dod' la re' si re' si) mi'4.( si8) |
    dod'16( sold la dod') re'( lad si re') mi'8 mi''( re'') si'( |
    la') mi''( re'') si'( la')[
  }
>> r16 la]( sib\cresc si do' dod') |
re'8[ r16 <>\p 
<<
  \tag #'violino1 {
    re''16] lad'( si') r re'' lad'( si') r si'( dod''\cresc re'' mi'' fad'') |
    mi''8.(_\markup { \dynamic p \italic sub } fad''32 re'') dod''8 si' la'!4 r |
    r4 r8 si' dod''16( la' dod'' la' re'' si' re'' si') mi''4.( si'8) dod''16( sold' la' dod'') re''( lad' si' re'') |
    mi''8 la''( sold'') re''( dod'') la''( sold'') re''( |
    dod'')[
  }
  \tag #'violino2 {
    fad'16] sol'( fad') r fad' sol'( fad') r sold'( la'\cresc si' dod'' re'') |
    dod''8.(\p re''32 si') la'8 sold' la'16( mi'' dod'' la' sold' mi' re' si) |
    dod'( la dod' la re' si re' si) mi'4.( si8) |
    dod'16( sold la dod') re'( lad si re') mi'2~ |
    mi'8 mi''8( re'') si'( la') mi''( re'') si'( |
    la'8)[
  }
>> r16 la]( sib\cresc si do' dod') re'8[ r16
<<
  \tag #'violino1 {
    si'16\p] mi''16( re'') r si' |
    mi''16( re'') r sold'( la'\cresc si' dod'' re'') dod''8.(\p re''32 si') la'8 sold' |
    \ru#2 { r16 mi''(\f dod''' mi'') r fad''(\p re''' fad'') r16 si' r dod'' r re'' r si' | }
    <dod'' la''>4:16 <si' la''>:16\cresc <dod'' la''>:16 q16\f q <si' sold''> q |
    <mi' dod'' la''>8 r mi'\p mi' la' r r4 |
  }
  \tag #'violino2 {
    fad'16\p] lad'16( si') r fad' |
    lad'16( si') r si( dod'\cresc re' mi' fad') mi'8.(\p fad'32 re') dod'8 si |
    r16 dod''(\f mi'' dod'') r re''(\p fad'' re'') r sold' r la'! r si' r sold' |
    r16 dod''(\f mi'' dod'') r re''(\p fad'' re'') r sold' r la' r si' r sold' |
    <la' mi''>4:16 <la' fad''>:16\cresc <la' mi''>:16 q16\f q <re'' mi''> q |
    <dod'' mi''>8 r dod'\p dod' <dod' mi'>8 r r4 |
  }
>>
