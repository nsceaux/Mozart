\tag #'voix1 {
  Spie -- gar -- ti non poss’ i -- o
  quan -- to il mio cor t’a -- do -- ra,
  quan -- to il mio cor t’a -- do -- ra;
  ma il cor ta -- cen -- do an -- co -- ra
  po -- trà spie -- gar -- lo ap -- pien,
  po -- trà spie -- gar -- lo ap -- pien.

  Vi -- ta dell’ al -- ma mi -- a…
  non sa pia -- cer che si -- a,
  non sa che sia di -- let -- to
  chi non pro -- vò nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  no, non sa, non sa pia -- cer che si -- a,
  non sa che sia di -- let -- to
  chi non pro -- vò __ nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  non sa pia -- cer che si -- a,
  non sa che sia di -- let -- to,
  chi non pro -- vò __ nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor.
}
\tag #'voix2 {
  Vo -- ci dell’ i -- dol mi -- o
  ah che in u -- dir -- vi io sen -- to
  d’in -- so -- li -- to con -- ten -- to
  tut -- to in -- on -- dar -- mi il sen,
  tut -- to in -- on -- dar -- mi il sen.
  
  De -- li -- zia del mio cor…
  non sa pia -- cer che si -- a, __
  non sa che sia di -- let -- to,
  chi non pro -- vò nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  no, non sa, non sa pia -- cer che si -- a,
  non sa che sia di -- let -- to
  chi non pro -- vò __ nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  non sa pia -- cer che si -- a,
  non sa che sia di -- let -- to,
  chi non pro -- vò __ nel pet -- to
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor,
  sì for -- tu -- na -- to a -- mor.
}
