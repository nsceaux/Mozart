\clef "treble" R1*9 |
r2 <>\p <<
  \tag #'(oboe1 oboi) {
    sold''2( |
    la'' sold'') |
    red''( mi''4)
  }
  \tag #'(oboe2 oboi) {
    mi''2( |
    fad'' mi'') |
    la'( sold'4)
  }
>> r4 |
r2 r8 <>\p <<
  \tag #'(oboe1 oboi) {
    si''8 si'' si'' |
    sold''2( la''4)    
  }
  \tag #'(oboe2 oboi) {
    re''8 re'' re'' |
    si'2( dod''4)
  }
>> r4 |
R1*2 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4( sol'' | fad'' sold''!) la''( sol'') | }
  { dod''2( | re'') dod'' | }
>>
<<
  \tag #'(oboe1 oboi) { fad''2 mi''8-. mi''-. mi''-. }
  \tag #'(oboe2 oboi) { re''2 dod''8-. dod''-. dod''-. }
>> r8 |
R1*6
<<
  \tag #'(oboe1 oboi) { mi''4( sold'') la'' }
  \tag #'(oboe2 oboi) { dod''( re'') dod'' }
>> r4 |
<<
  \tag #'(oboe1 oboi) { mi''4( sold'') la''8 }
  \tag #'(oboe2 oboi) { dod''4( re'') mi''8 }
>> r r4 |
R1*3 |
<<
  \tag #'(oboe1 oboi) { mi''4( sold'') la'' }
  \tag #'(oboe2 oboi) { dod''( re'') dod'' }
>> r4 |
<<
  \tag #'(oboe1 oboi) {
    mi''4( sold'' la'' re'') |
    dod''8
  }
  \tag #'(oboe2 oboi) {
    dod''4( re'' dod'' sold') |
    la'8
  }
>> r8 r4 r2 |
R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s16 la''( sold'' sol'' fad''8) s sold''( la'' si'' sold'') |
    la''4 si'' dod'''4.( si''8) |
    la''8 s mi'' mi'' la'' }
  { s16 la' sold' sol' fad'8 s si'8( dod'' re'' si') |
    dod''4 la''2~ la''8( sold'') |
    la'' s dod'' dod'' dod'' }
  { r16 s8.\cresc s8 r s2\p |
    s4 s2\cresc s4\f |
    s8 r s4.\p r8 r4 | }
>>
