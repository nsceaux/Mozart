\clef "alto" r2 r16 mi'(-.\p mi'-. mi'-.) mi'4~ |
mi'16 mi'(-. mi'-. mi'-.) mi'4~ mi'8 r mi' r |
mi'-. la( dod' mi') r re'( fad' re') |
dod'4 r <<
  { si2 | dod' } \\
  { sold2 | la }
>> r16 <<
  { re'16( si') la'-. sold'-. fad'-. mi'-. re'-. | } \\
  { si16( re') dod'-. si-. la-. sold-. si-. | }
>>
r8 <<
  { dod'8( re' fad') } \\ { la4 la8 }
>> r8 mi' mi' mi' |
mi' la la r mi' r mi' r |
fad'2\fp mi'4 mi |
r8 la r fad' r mi' r mi' |
r8 si4( la8) sold4\p r8 <<
  { sold'8( | la'4) } \\ { mi'8( fad'4) }
>> r8 <<
  { la'8( sold'4) } \\ { fad'8( mi'4) }
>> r8 <<
  { sold'8( | fad'4) } \\ { mi'8( | red'4) }
>> r8 <<
  { fad'8( sold') mi16( fad sold la si dod') |
    re'!8 fad fad fad mi2 | } \\
  { red'8( mi') mi16( red\cresc mi fad sold mi) |
    fad8\f fad fad fad mi2\p | }
>>
si2( la4) r |
r8 re'( dod' si) r mi' r mi' |
r16 dod'( la'\cresc sold' fad' mi' re' dod') r8 re'(\p si dod') |
r mi' r re' dod'4( mi') |
re'4.( si8) la8.( dod'16) mi'4 |
re'4.( fad'8) la'16( mi' dod' si la8) r |
r4 fad'2 si4~ |
si mi'2 la4~ |
la8 fad'4 fad'8 si4( mi')~ |
mi'8 la r la re'4.( red'8) |
mi'16-.( mi'-. mi'-. mi'-.) mi'4~ mi'16(-. mi'-. mi'-. mi'-.) mi'4~ |
mi'16 mi'-.( mi'-. mi'-.) mi'4( la8) r r4 |
R1 |
la4(\mp si) dod'8 mi'4 mi'8~ |
mi' mi'4 mi'8~ mi'[ r16 la]( sib\cresc si do' dod') |
re'8[ r16 si]\p mi'( re') r si mi'( re') r mi'(~ mi'\cresc re' dod' si) |
dod'8\p fad' mi' <mi' re'> <dod' mi'>4 r |
R1 |
r2 la4( si) |
dod'8 mi'4 mi' mi' mi'8~ |
mi'[ r16 la]( sib\cresc si do' dod') re'8[ r16 re']\p sol'16( fad') r re' |
sol'16( fad') r mi'8*1/2([ s16\cresc re'16 dod' si]) dod'8\p fad mi mi |
la8\f r la\p r mi' mi' mi' mi' |
la8\f r la\p r mi' mi' mi' mi' |
la'4:16 re':16\cresc mi':16 mi':16\f |
la8 r la\p la' la r r4 |
