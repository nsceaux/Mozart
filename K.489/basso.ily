\clef "bass" r2 mi4\p r |
mi r8 sold( la) r mi r |
la,4 r la, r |
la, r mi8 r mi r |
mi4 r8 mi mi4 r |
la4 fad8( re) mi8 r mi r |
la, la la, r mi r mi r |
fad2(\fp sold) |
la8 r la r sold r la r |
si r si, r mi\pp r mi r red r red r mi r mi r |
si, r si, r mi4 r |
fad8\f fad fad fad sold4\pp r |
mi2( la4) r |
fad4( mi8 red) mi r mi r |
la,4 r re8 r red r |
mi r mi r la, la la, la |
la, la la, la la, la la, la |
la, la la, la la, la la, r |
la4( lad) si4.( re8) |
mi4( sold) la4.( dod8) |
re8( dod si, la,) sold,2 |
la,8 r la, r re4.( red8) |
mi4 r mi\pp r |
mi r r2 |
R1 |
la,4( si,) dod8 dod'-. si-. sold-. |
la-. dod'-. si-. sold-. la[ r16 la,]( sib,\cresc si, do dod) |
re8. fad16(\p re8.) fad16( re8.) re16( dod\cresc si, la, sold,) |
la,8\p re mi mi la,4 r |
R1 |
r2 la,4( si,) |
dod8 dod'( si) sold( la) dod'( si) sold( |
la)[ r16 la,]( sib,\cresc si, do dod) re8. fad16(\p re8.) fad16( |
re8.) re16( dod\cresc si, la, sold,) la,8\p re mi mi |
la\f r re\p r mi mi mi mi |
la\f r re\pp r mi mi mi mi |
la4:16 re:16\cresc mi:16 mi:16\f |
la,8 r la,\p la la,8 r r4 |
