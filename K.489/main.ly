\version "2.19.80"
\include "common.ily"

\opusTitle "K.489 – Spiegarti non poss’io"

\header {
  title = \markup { Duetto \italic { Spiegarti non poss’io } }
  opus = "K.489"
  copyrightYear = "2018"
}
\includeScore "K.489"
