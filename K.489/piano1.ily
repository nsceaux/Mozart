\clef "treble" \omit TupletBracket r4\p sold'8.( la'32 si') la'8 r la'8.( si'32 dod'') |
si'8 r re''8.( mi''32 fad'') <fad'' re''>16( <mi'' dod''>) <re'' fad'>-. <mi' dod''>-. q( <re' si'>) <la' dod'>-. <si sold'>-. |
<dod' la'>8 <dod'' mi''>4( <dod'' la'>8) r re''( si' sold'!) |
r16 la'( dod'' la') mi''( dod'') la'-. la'-. r8 <si' sold'> q q |
r <la' dod''> q q r16 <si' re''>( <re'' si''>) <la'' dod''>-. <sold'' si'>-. <fad'' la'>-. <mi'' sold'>-. <re'' si'>-. |
r8 << { dod''8( re'' fad'') } \\ { la'4 la'8 } >> r8 <la' dod'> q <re' sold'> |
<la' dod'> <dod' mi'> <dod' la> r <sold' si'> r q r |
<la' red'' fad'' la''>8.\fp \tuplet 3/2 { sold''32( fad'' mi'' } red''16) <red'' la'>-.( q-. q-.) <mi'' si'>( mi' re'' mi' dod'' mi' sid' mi') |
r8 <mi' la' dod''> r <fad' si' red''> r <mi' si' mi''> r <mi' la' dod''> |
r <mi' sold'>( <fad' la'> <red' fad'>) mi'8 sold''16.([ mi''32]) si''8 <mi' sold'>( |
<fad' la'>) la''16.([ fad''32]) si''8 <fad' la'> <mi' sold'> sold''16.([ mi''32]) si''8 <mi' sold'>8( |
<red' fad'>8) fad''16.([ red''32]) la''8 <fad' red'>( <mi' sold'>) mi'16(\cresc <red' fad'> <mi' sold'> <fad' la'> <sold' si'> <mi' dod''>) |
\voiceOne <re'! la' re''!>16\f re'8 re' re' re'16 re'8\p <re'' si''> q q |
<si' re'' sold''>2( <<
  { la''16)( mi'' la'' sold'' fad'' mi'' re'' dod'') \oneVoice }
  \new Voice { \voiceTwo dod''4 r }
>>
r8 << { re''8 <mi'' dod''> <fad'' si'> } \\ { la'4 la'8 } >> r <dod' mi' la'> r <mi' sold' si'> |
r16 <dod'' mi''>(\cresc <la'' dod'''> <si'' sold''> <la'' fad''> <mi'' sold''> <re'' fad''> <dod'' mid''>) r8 <re'' fad''>\p( <si' sold''> <dod'' la''>) |
r8 <mi'! la' dod''> r <re' sold' si'> <dod' la'>16 mi''( dod'' la' sol' mi' dod' sol') |
fad'( la' fad' re' si re' mi' re') dod'( mi'' dod'' la' sol' mi' dod' <mi' sol'>) |
fad'( la' re'' la' fad' la' re'' la') dod''( la' <mi' dod'> <re' si>) <la dod'>8 r |
r <<
  { dod''8 dod'''4~ dod'''8 si'' fad''4~ |
    fad''8 si' si''4~ si''8 la'' mi''4 |
    mi''16( la' si' dod'' re'' mi'' dod'' re'') } \\
  { dod''4 mi''~ mi''8 fad'' la' |
    sold' si'4 mi''~ mi'' sol'8 |
    fad'4 }
>> re''16( sold'' si'' la'' sold'' fad'' mi'' re'') |
dod''8( <re'' fad''>4 <dod'' mi''>8) <mi'' dod''>16( <re'' si'>) <fad'' re''>( <mi'' dod''>) q( <re'' si'>) q( <dod'' la'>) |
<sold' si'>8 r sold'8.( la'32 si') la'8 r la'8.( si'32 dod'') |
si'8 r si'8.( dod''32 re'') dod''16( la' dod'' la') re''( si' re'' si') |
<dod'' mi''>4 <si' sold''> <la'' la'>16 sold' la' dod'' re'' lad' si' re'' |
mi''4 <si' sold''> <la'' la'>8 <la'' mi''>([ <sold'' re''>]) <<
  { <si' re''>8([ | <la' dod''>]) } \\ { mi'8[~ | mi'] }
>> <mi'' la''>8([ <re'' sold''>]) << { <si' re''>8([ <la' dod''>]) } \\ mi'4 >>
r16 la sib(\cresc si do' dod') |
re'8[ r16 <fad' re''>]\p <<
  { <sol' lad'>16([ <fad' si'>16]) } \\ { mi'[ re'] }
>> r16 <fad' re''> <<
  { <sol' lad'>16([ <fad' si'>16]) } \\ { mi'[ re'] }
>> r16 <<
  { <sold' si'>16 <la' dod''>([ <si' re''>16*1/2 s32\cresc <dod'' mi''>16 <re'' fad''>])\! } \\
  { <re' mi'>16 \autoBeamOff \crossStaff { mi' re' } }
>>
<dod'' mi''>8.( <fad'' re''>32 <re'' si'>) <dod'' la'>8( <sold' si'>) la'16 mi''( dod'' la' sold' si' re'' si') |
<la' dod'' mi''>4 <si' sold''> <la' la''>16 la'( dod'' la' re'' si' re'' si') |
mi''4 <si' sold''> <la' la''>16 sold'( la' dod'' re'' lad' si' re'') |
mi''8 <mi'' la''>([ <re'' sold''>]) <<
  { <si' re''>([ <la' dod''>]) } \\ { mi'4 }
>> <mi'' la''>8([ <re'' sold''>]) <mi' si' re''>( |
<mi' la' dod''>) r16 la( sib\cresc si do' dod') re'8\! r16 <<
  { <fad' si'>16 <lad' dod'' mi''>16([ <si' re''>]) } \\
  { re'16 sol'[\p fad'] }
>> r16 <<
  { <fad' si'>16 | <lad' dod'' mi''>16([ <si' re''>]) } \\
  { re'16 | sol'([ fad']) }
>> r16 <si sold'> <dod' la'>( <re' si'>\cresc <mi' dod''> <fad' re''>) <mi' dod''>8.\p <fad' re''>32 <re' si'> <dod' la'!>8( <si sold'>) |
r16\f <dod'' mi''> <mi'' dod'''> <dod'' mi''> r\p <re'' fad''> <fad'' re'''> <re'' fad''> r <sold' si'> r <la' dod''> r <si' re''> r <sold' si'> |
r16 <<
  { la''16 sold''! sol'' } \\ { <la' dod''>8 q16 }
>> r16 <<
  { fad''16 fad'' re'' } \\ { re'' re'' si' }
>> r16 <sold' si'> r <la' dod''> r <si' re''> r <sold' si'> |
<la' dod''>32[ la'' q la''] q[\cresc la'' <dod'' mi''> la''] <si' fad''>[ la'' q la''] q[ la'' q la''] <dod'' mi''>[ la'' q la''] q[\f la'' q la''] q[ la'' q la''] <si' re''>[ sold'' q sold''] |
<dod'' la''>8 r <dod'' mi''>8\p-! q-! <la' dod'' la''>-! r r4 |
