\clef "treble" \transposition la
R1*2 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do''4 }
  \tag #'(corno2 corni) { do'1~ | do'4 }
>> r4 r2 |
R1*8 |
r2 r8 <<
  \tag #'(corno1 corni) { sol'8 sol' sol' | sol'2~ sol'4 }
  \tag #'(corno2 corni) { sol8 sol sol | sol2 do'4 }
  { s4.\p | <> }
>> r4 |
R1*2 |
r2 <<
  \tag #'(corno1 corni) {
    do''2~ |
    do''1~ |
    do''2~ do''8 do''-. do''-.
  }
  \tag #'(corno2 corni) {
    do'2~ |
    do'1~ |
    do'2~ do'8 do'-. do'-.
  }
>> r8 |
R1*6 |
r2 <<
  \tag #'(corno1 corni) { do''4( re'' | mi'' re'' do''8) }
  \tag #'(corno2 corni) { mi'4( sol' | do'' sol' mi'8) }
>> r8 r4 |
R1*2 |
r2 <<
  \tag #'(corno1 corni) {
    do''4( re'' | mi'') s do''( re'' | mi'') s mi''( re'' | do''8)
  }
  \tag #'(corno2 corni) {
    mi'4( sol' | do'') s mi'( sol' | do'') s do''( sol' | mi'8)
  }
  { s2 | s4 r s2 | s4 r s2 | }
>> r8 r4 r2 |
R1*3 |
<>\f <<
  \tag #'(corno1 corni) { do''4~ do''8 }
  \tag #'(corno2 corni) { do'4~ do'8 }
>> r8 <>\p \twoVoices #'(corno1 corno2 corni) <<
  sol'2
  sol'
>> |
<<
  \tag #'(corno1 corni) { do''4 do'' mi''4. re''8 | do'' }
  \tag #'(corno2 corni) { do'4 do' do''4. sol'8 | mi' }
  { s4 s2\cresc s4\f }
>> r8 <>\p <<
  \tag #'(corno1 corni) { sol'8 sol' mi' }
  \tag #'(corno2 corni) { mi'8 mi' do' }
>> r8 r4 |
