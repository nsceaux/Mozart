\clef "bass" la16( mi') mi'-. mi'-. si( mi' re' mi') dod'( mi') mi'-. mi'-. dod'( mi' la mi') |
sold( mi') mi'-. mi'-. si( mi' sold mi') la8 r mi r |
<<
  { la,16( dod mi dod) la,( mi sol mi) la,( re fad re) la,( si, re si,) } \\
  { la,4 la, la, la, }
>>
<la, dod>4 <la dod'> <si sold! mi>2 |
<mi la dod'>2 <mi sold si>4 r4 |
la4( fad8 re) mi r mi, r |
la, la la, r mi r mi r |
<fad fad,>2( <sold, sold>) |
<la la,>8 r la r sold r la r |
si r si, r mi16( sold) si-. si-. mi( sold) si-. si-. |
\ru#2 { red( fad) si-. si-. } \ru#2 { mi( sold) si-. si-. } |
\ru#2 { si,( fad) si-. si-. } mi4 r |
<<
  { \autoBeamOff \crossStaff { s16 la8 la la la16 <sold si>8 } } \\
  { fad,8 fad fad fad mi }
>> <mi mi,>8[ q q] |
<mi, mi>2( <la, la>4) r |
fad4( mi8 red) mi r mi, r |
la,4 r re8 r red r |
mi r mi, r <<
  { la,8 dod mi la |
    la,( re fad la) la( dod mi la) |
    la,8( re fad la) } \\
  { la,2 | la, la, | la,2 }
>> la,8 la la, r |
<<
  { mi'8 mid' fad' mi'~ mi'16 red' re'4 dod'8 |
    si red' mi' re'~ re'8 dod'4 la8~ |
    la <fad re>4 q8 } \\
  { la4( lad! si4. re8) |
    mi( fad sold4 la4. dod8) |
    re dod([ si, la,]) }
>> <sold, si, mi>4 <sold si mi'>4~ |
<la mi'>8 la[ la, la] re4. red8 |
mi16( mi') mi'-. mi'-. si( mi' re' mi') dod'( mi') mi'-. mi'-. dod'( mi' dod' la) |
sold( mi') mi'-. mi'-. sold( mi' mi mi') la8 r r4 |
<< { mi'2 } \\ { dod'16 la dod' la re' si re' si } >> <dod' mi'>4 <si, mi sold>~ |
<dod mi la>16 mi'([ dod' la]) si( re' mi' re') dod'8-. dod'([ si]) sold([ |
la]) dod'([ si]) sold([ la]) r16 la, sib,( si, do dod) |
re8[ r16 si] s8 r16 si s8 r16 s dod'16( si <la dod'> <sold! si>) |
<la dod'>8 re mi mi, la, <dod la> <mi si>4 |
<<
  { mi'2~ \oneVoice <mi' dod'>4 } \\
  { dod'16( la dod' la re' si re' si) }
>> <sold mi'>4 |
<<
  { dod'16( mi' dod' la) } \\ { la8 r }
>> si16( re' mi' re') <dod' mi'>4 <<
  { <mi sold>~ | <mi la>8 } \\
  { dod4~ | dod8 }
>> dod'8([ si]) sold([ la]) dod'([ si]) sold( |
la) r16 la,( sib, si, do dod) re8 r16 s s4 |
s8 r16 <<
  { mi8 re16[ dod si,] dod8 } \\
  { re16 dod si, la, sold,! la,8 re mi mi, | }
>>
la,8 r re r mi2 |
la,8 la << { re8 re' } \\ re4 >> <mi mi'>2 |
la,16 la la la re, re re re mi, mi mi mi mi, mi mi, mi |
la,8 r <la, la>8-! q-! q-! r r4 |
