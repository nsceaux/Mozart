\piecePartSpecs
#`((oboi #:score-template "score-oboi")
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in A")
   (violino1)
   (violino2)
   (viola)
   (basso))
