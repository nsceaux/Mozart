<<
  \tag #'voix1 {
    \clef "soprano/treble" R1 |
    r2 r4 r8 mi'' |
    mi''4. dod''8 re''4. si'8 |
    la' la' r4 si' si'8. si'16 |
    dod''4. dod''8 mi'' re'' r4 |
    dod''4 re''8 mi''16([ fad'']) la'4. si'8 |
    la' la' r dod''16([ si']) si'8 dod''16([ si']) si'8 mi'' |
    mi'' red'' r red'' mi'' re'' dod'' sid' |
    dod''4 r8 red'' mi''4. dod''8 |
    si'4~ si'16([ lad']) la'([ fad']) mi'4 r |
    R1*7 |
    mi''16([ re'']) dod'' re'' mi''([ re'']) dod''([ si']) la'8 la' r4 |
    R1 |
    r4 r8 fad'' fad''8. \appoggiatura mi''32 re''16 \appoggiatura dod'' si'8. la'16 |
    sold'8 sold' r mi'' mi''8. \appoggiatura re''32 dod''16 \appoggiatura si' la'8. sol'16 |
    fad'8 fad' r4 mi''8 mi''16 mi'' mi''8 mi'' |
    mi'' re''4 dod''8 dod''16([ si']) re''([ dod'']) dod''([ si']) si'([ la']) |
    sold'4 r la' r8. dod''16 |
    \appoggiatura dod''8 si'4 r8 si' dod''8. la'16 re''8. si'16 |
    mi''16([ fad'']) mi''8 r si' dod''16([ sold']) la'([ dod'']) re''([ lad']) si'([ re'']) |
    mi''2~ mi''16[\melisma fad''32 mi''] re''[ dod'' si' la'] sold'[ la' si' dod''] re''[ si' mi'' re''] |
    dod''[ mi'' fad'' mi''] re''[ dod'' si' la'] sold'[ la' si' dod''] re''[ si' mi'' re''] dod''16[ mi'']\melismaEnd mi''4. |
    r16 re'' lad'([ si'])~ si' re'' lad'([ si'])~ si' re'' lad'! si' r si' dod'' re'' |
    dod''8.([ re''32 si'] la'!8) sold' la'4 r |
    r r8 si' dod''8. la'16 re''8. si'16 |
    mi''([ fad'']) mi''8 r si' dod''16([ sold']) la'([ dod'']) re''([ lad']) si'([ re'']) |
    mi''1~ |
    mi''16([ sol'']) sol''4. r16 fad'' dod''[ re''8] fad''16 dod''[ re'']~ |
    re'' fad'' dod'' re'' r re'' mi'' fad'' mi''8.([ fad''32 re'' dod''8]) si' |
    mi''4 r8 fad''16.([ re''32]) si'8 dod'' \grace mi''16 re''8 dod''16([ si']) |
    mi''4 r8 fad''32[ mid'' fad'' re''] si'8 dod'' \grace mi''16 re''8 dod''16[ si'] |
    dod''8. dod''16 si'32([ dod'' re'' dod'']) \grace mi''16 re''([ dod''32 si']) mi''8.([ fad''16 la'8]) sold' |
    la'4 r r2 |
  }
  \tag #'voix2 {
    \clef "tenor/G_8" R1*10 |
    si2. si8 mi' |
    mi'16([ red'] fad'4) la8 sold sold r4 |
    re'!8 re'16 re' re'8 re' si si r si |
    mi'16([ red']) mi'([ red']) mi'([ mid']) fad'([ re']) dod'8 dod' r4 |
    re'4 mi'8. fad'16 la4. si8 |
    dod'4 r fad'4 sold'8. la'16 |
    dod'4~ dod'16([ mi']) re'([ si]) la4 r |
    r2 r4 r8 la |
    la8. re'16 re'8([ mi'16]) fad' mi'4 r8 mi' |
    dod' mid' fad' mi' mi'16([ red']) re'4~ re'16 dod' |
    si8 red' mi'8. re'16 re'8 dod' r4 |
    re'8 re'16 re' re'8 re' re'2( |
    dod'8) fad'4 mi'8 mi'16([ re']) fad'([ mi']) mi'([ re']) re'([ dod']) |
    si4 r dod' r8. la16 |
    \appoggiatura la8 sold4 r r r8 si |
    dod'8. la16 re'8. si16 mi'16([ fad']) mi'8 r si dod'16([ sold]) la([ dod']) re'([ lad]) si([ re']) mi'2~ |
    mi'~ mi'16([ sol']) sol'4. |
    r16 fad' dod'([ re'])~ re' fad' dod'([ re'])~ re' fad' dod' re' r re' mi' fad' |
    mi'8.([ fad'32 re'] dod'8) si la4 r8 si |
    dod'8. la16 re'8. si16 mi'([ fad']) mi'8 r si |
    dod'16([ sold]) la([ dod']) re'([ lad]) si([ re']) mi'2~ |
    mi'16[\melisma fad'32 mi'] re'[ dod' si la] sold[ la si dod'] re'[ si mi' re'] dod'[ mi' fad' mi'] re'[ dod' si la] sold[ la si dod'] re'[ si mi' re'] |
    dod'16([ mi'])\melismaEnd mi'4. r16 re' lad[ si8] re'16 lad[ si]~ |
    si16 re' lad si r si dod' re' dod'8.([ re'32 si la8]) sold |
    dod'4 r8 re'16.([ si32]) sold8 la \grace dod'16 si8 la16([ sold]) |
    dod'4 r8 re'32[ dod' re' si] sold8 la \grace dod'16 si8 la16[ sold] |
    mi'8. mi'16 re'32([ mi' fad' mi']) \grace sold'16 fad'([ mi'32 re']) dod'8.([ re'16 dod'8]) si |
    la4 r r2 |
  }
>>
