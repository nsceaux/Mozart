\clef "treble" \transposition fa
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do''~ | do''4 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'4 }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''4
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'4
  }
>> r4 r2 |
R1*3 |
<<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol'~ |
    sol'4 do''( re'' mi'') |
    re''4 sol'2.~ |
    sol'1~ |
    sol'4 do''( re'' mi'') |
    re''2 re'' |
    re''4
  }
  \tag #'(corno2 corni) {
    sol1~ |
    sol~ |
    sol4 mi'( sol' do'') |
    sol'4 sol2.~ |
    sol1~ |
    sol4 mi'( sol' do'') |
    sol'2 do'' |
    sol'4
  }
  { s1*6 | s2 s\cresc | }
>> <>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 sol' sol' | re'' }
  { sol'4 sol' sol' | re'' }
>> r4 r2 |
R1*7 |
\tag #'corni <>^"a 2." re''1\p~ |
re''~ |
re''~ |
re''~ |
re''~ |
re''~ |
re''2 sol'\cresc |
re''4\f r r2 |
R1*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2.~ re''4~ |
    re''2~ re''~ |
    re''2.~ re''4~ |
    re''2~ re''~ |
    re''4 }
  { re''2.~ re''4~ |
    re''2~ re'' |
    sol'4 re''2 re''4~ |
    re''2~ re'' |
    sol'4 }
  { s2.\f s4\p |
    s2 s\f |
    s2. s4\p |
    s2 s\f | }
>> r4 r2 |
R1*19 |
<>\p <<
  \tag #'(corno1 corni) { re''1 | re''4 }
  \tag #'(corno2 corni) { re''1 | sol'4 }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    re''1 |
    sol'2 sol' |
    sol' sol' |
    sol'4
  }
  \tag #'(corno2 corni) {
    re''1 |
    sol2 sol |
    sol sol |
    do'4
  }
>> r4 r2 |
R1*9 | \allowPageTurn
<<
  \tag #'(corno1 corni) { re''4 re'' re'' re'' | re'' }
  \tag #'(corno2 corni) { sol'4 do'' sol' do'' | sol' }
  { s2.\cresc s4\f | }
>> r4 r2 |
R1*7 |
<<
  \tag #'(corno1 corni) { re''4 re'' re'' re'' | re'' }
  \tag #'(corno2 corni) { sol'4 do'' sol' do'' | sol' }
  { s2.\cresc s4\f | }
>> r4 r2 |
R1*3
r4 <>\p <<
  \tag #'(corno1 corni) { do''4(-. do''-. do''-.) | }
  \tag #'(corno2 corni) { mi'4-.( mi'-. sol'-.) | }
>> |
R1 |
r4 <<
  \tag #'(corno1 corni) { do''4(-. do''-. do''-.) | }
  \tag #'(corno2 corni) { mi'4-.( mi'-. sol'-.) | }
>> |
R1 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do'' |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do' |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol4
  }
>> r4 r2 |
R1 |
r4 <>\p <<
  \tag #'(corno1 corni) { sol'4 sol' sol' | sol'2 }
  \tag #'(corno2 corni) { sol4 sol sol | sol2 }
>> r2 |
r4 <>\p <<
  \tag #'(corno1 corni) { sol'4 sol' sol' | sol'2 }
  \tag #'(corno2 corni) { sol4 sol sol | sol2 }
>> r2 |
R1 |
<>\p <<
  \tag #'(corno1 corni) { re''1( | mi''4) }
  \tag #'(corno2 corni) { sol1( | do'4) }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { do''2( re'') | do''4 }
  \tag #'(corno2 corni) { mi'2( sol') | do'4 }
>> r4 r2 |
<>\p <<
  \tag #'(corno1 corni) { re''1( | mi''4) }
  \tag #'(corno2 corni) { sol1( | do'4) }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { do''2( re'') | do''1~ | do''2 }
  \tag #'(corno2 corni) { mi'2( sol') | do'1~ | do'2 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 |
    re''1 |
    do''~ |
    do''2 mi'' |
    re''1 | }
  { do''2~ |
    do'' sol' |
    do'1~ |
    do'2 do''~ |
    do'' sol' | }
>>
<>\f <<
  \tag #'(corno1 corni) { do''2 re'' | do'' re'' | }
  \tag #'(corno2 corni) { mi'2 sol' | mi' sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | re'' | mi'' | re'' | }
  { do''1~ | do''2 sol' | do''1~ | do''2 sol' }
  { s1*2\p | s1\f }
>>
<<
  \tag #'(corno1 corni) {
    do''4 do'' mi'' do'' |
    sol'1~ |
    sol'4 mi'' fa'' re'' |
    do'' s do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    mi'4 do' mi' do' |
    sol1~ |
    sol4 do'' re'' sol' |
    mi' s do' do' |
    do'2
  }
  { s1*3 | s4 r s2 | }
>> r2 |
