<<
  \tag #'voix1 {
    \clef "soprano/treble" R1*25 |
    <>^\markup\italic (arrestando il Conte.) r4 sol'8 la' si' do'' re'' si' |
    do'' do'' r4 r2 |
    r4 sol'8 la' si' do'' re'' si' |
    do'' do'' r4 r do''8 do'' |
    \appoggiatura do''8 si'4. si'8 si'4. re''8 |
    re''8. do''16 do''4 r do''8 mi'' |
    mi''( re''4) re''8 re''4. do''8 |
    si'4 r r2 |
    R1*6 |
    <>^\markup\italic\center-align\line { (Vedendo Figaro che abbraccia Marcellina) }
    r4 sol'8. sol'16 sol'8 sol' sol' sol' |
    lab' lab' r4 si'4. si'8 |
    do''4 r8 do'' re''4. re''8 |
    mib''4 r8 do'' mib''4. do''8 |
    si'4 <>^\markup\italic (vuol partire.) r r sol'8. sol'16 |
    mi''!4 do'' r2 |
    r sol'4. sol'8 |
    mi''4 do'' r2 |
    R1*4 |
    r2 <>^\markup\italic { (da uno schiaffo a Figaro.) }
    r4 sol'8.^\p do''16 |
    si'4 si' r2 |
    R1*2 |
    r4*1/2 s8^\p <>^\markup\italic (da se.) sol''8. sol''16 mi''8. mi''16 do''8. do''16 |
    si'4 si' r2 |
    r4 sol''8. sol''16 mi''8. mi''16 re''8. re''16 |
    dod''8. dod''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    si'4 do'' r2 |
    r4 do''8. do''16 si'8. si'16 si'8. si'16 |
    do''4 sol''8. sol''16 mi''8. mi''16 re''8. re''16 |
    dod''8. dod''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    si'4 do'' r2 |
    r4 do''8. do''16 si'8. si'16 si'8. si'16 |
    do''4 r4 r2 |
    r4 sol'8. sol'16 do''8. do''16 mi''8. mi''16 |
    sol''4 mi''8. mi''16 fa''8. fa''16 re''8. re''16 |
    do''4 r r2 |
    r4 sol'8. sol'16 do''8. do''16 mi''8. mi''16 |
    sol''4 mi''8. mi''16 fa''8. fa''16 re''8. re''16 |
    do''4 r r2 |
    R1*7 |
    r4 <>^\markup\italic (a Bartolo.) r8 fa' re' re' r4 |
    r <>^\markup\italic (al Conte.) r8 la' fa' fa' r4 |
    r <>^\markup\italic (a Curzio.) r8 do'' la' la' r4 |
    r <>^\markup\italic (a Marcellina.) r8 si' do'' do'' r4 |
    R1 |
    r4 <>^\markup\italic (a Figaro.) r8 sol' do''4 do'' |
    R1*3 |
    r4 <>^\markup\italic (a Bartolo.) r8 fa' re' re' r4 |
    r4 <>^\markup\italic (al Conte.) r8 la'8 fa' fa' r4 |
    r4 <>^\markup\italic (a Curzio.) r8 do'' la' la' r4 |
    r <>^\markup\italic (a Marcellina.) r8 si' do'' do'' r4 |
    R1 |
    r4 <>^\markup\italic (a Figaro.) r8 sol' do''4 do'' |
    R1*8 |
    r2 r4 la' |
    la'8([ sol' fad' sol']) do''4 do'' |
    re''8([ do'' si' do'']) fa''4 la' |
    la'8([ sol' fad' sol']) do''4 do'' |
    re''8([ do'' si' do'']) fa''4 do'' |
    fa''8([ mi'' la'' sol'']) fa''([ mi'']) re''([ do'']) |
    sib'!([ la' sol' fa']) mi'([ re']) do'([ sib]) |
    la4( fa'') re'' sib' |
    la'( sol') r2 |
    r4 sol' la' sib'8 sib' |
    sol'4 la' r2 |
    r4 la' sib' do''8 do'' |
    la'4 sib' r2 |
    r r4 reb'' |
    sol''( do'') do'' do'' |
    si'8([ do'' fa'' do'']) sold'([ la']) do''([ la']) |
    fa'2 sol'4. sol'8 |
    fa'4 r r reb'' |
    sol''( do'') do'' do'' |
    si'8([ do'' fa'' do'']) sold'([ la']) do''([ la']) |
    fa'2 sol'4. do''8 |
    fa''2 si' |
    do'' fa'4. fa'8 |
    sol'4 r do'' r8 do'' |
    fa''2 si' |
    do'' fa'4. fa'8 |
    sol'4 r do'' r8 do'' |
    fa'4 r8 do'' do''4 do''8 do'' |
    do''4 do''8 do'' do''4 do''8 do'' |
    do''2 fa'' |
    fa'' mi''4. mi''8 |
    la''2 fa'' |
    sol''2 mi''4. mi''8 |
    fa''4 r r2 |
    R1*4 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" r2 <>^\markup\italic (abbracciando Figaro)
    la'4 do'' |
    sib' sol' do''4. sol'8 |
    la'4 la' la' do'' |
    sib' sol' do''4. sol'8 |
    la'4 la' r2 |
    R1*11 |
    r4 mi''2 fa''4 |
    mi''8([ do'']) sib'4 r2 |
    R1 |
    r4 mi''2 fa''4 |
    mi''8([ do'']) sib'4 r2 |
    R1 |
    r2 fa''4. si'8 |
    do'' do'' r4 r2 |
    R1*9 |
    r4 fa''2 re''8([ si']) |
    do'' do'' r4 r2 |
    R1 |
    r4 fa''2 re''8([ si']) |
    do''4 do'' r2 |
    r fad''4. fad''8 |
    sol''4 sol' r2 |
    R1*13 |
    sol'2^\p sol' |
    la'8([ sol']) sol'4 r2 |
    sol'2. sol'4 |
    la'8([ sol']) sol'4 r2 |
    sol'2. sol''4 |
    sol'' fa'' fa'' fa'' |
    fa''( mi'') mi'' r8 mi'' |
    re''4 r8 re'' re''4 r8 re'' |
    do''4 sol'2 sol''4 |
    sol'' fa'' fa'' fa'' |
    fa''( mi'') mi'' r8 mi'' |
    re''4 r8 re'' re''4 r8 re'' |
    do''4 mi''2 do''4 |
    sol' sol' sol' sol' |
    sol' r8 do'' re''4 r8 si' |
    do''4 mi''2 do''4 |
    sol' sol' sol' sol' |
    sol' r8 do'' re''4 r8 si' |
    do''4 r r2 |
    R1 |
    r4 r8 <>^\markup\italic (a Suzanna.) do''8 do''4 la'8 do'' |
    do''4 sib'!8 do'' do''4 sol'8 do'' |
    la'4 fa'8 do'' do''4 la'8 do'' |
    do''4 sib'8 do'' do''4 sol'8 do'' |
    la'4 r8 do'' do''([ re'']) mi'' fa'' |
    fa''([ mi'']) re'' do'' do''([ sib']) la' sol' |
    fa'4 r r2 |
    R1*2 |
    r2 r4 r8 <>^\markup\italic (a Suzanna.) si' |
    do'' do'' r fa'' mi'' do'' r fa'' |
    mi''4 do'' r2 |
    R1*6 |
    r2 r4 <>^\markup\italic (a Suzanna.) si' |
    do''8 do'' r fa'' mi'' do'' r fa'' |
    mi''4 do'' r2 |
    R1*7 |
    r2 r4 fa' |
    fa'( la') do'' do'' |
    do''2 mi'4 mi' |
    fa'4.( sol'8) la'4 do'' |
    do''2 mi'4 mi' |
    fa'4.( sol'8) la'4 fa' |
    do''2 do''4 do'' |
    do''2 do''4 do'' |
    do''( la') sib' sol' |
    fa'( mi') r2 |
    r4 mi' fa' sol'8 sol' |
    mi'4 fa' r2 |
    r4 fa' sol' la'8 la' |
    fad'4 sol' r2 |
    r r4 sib' |
    do''( sib') sib' sib' |
    la'2 fa'4. fa'8 |
    do'2 mi'4. mi'8 |
    fa'4 r r sib' |
    do''( sib') sib' sib' |
    la'2 fa'4. fa'8 |
    do'2 mi'4. sib'8 |
    la'2 sold' |
    la' fa'4. fa'8 |
    fa'4 r mi' r8 sib' |
    la'2 sold' |
    la' fa'4. fa'8 |
    fa'4 r mi' r8 mi' |
    fa'4 r8 la' sib'4 sib'8 sib' |
    la'4 la'8 la' sib'4 sib'8 sib' |
    la'2 la' |
    sol' sol'4. sol'8 |
    fa''2 fa'' |
    re'' do''4. sib'8 |
    la'4 r r2 |
    R1*4 |
  }
  \tag #'voix3 {
    \clef "tenor/G_8" R1*12 |
    r2 r4 la8 fa |
    sib sib r4 r8 sib sib sol |
    do' do' r4 r do'8 la |
    re'4. sol8 sol4. sol8 |
    fa4 r r la8 fa |
    sib4 sol r8 sol do' sol |
    la4 la r2 |
    r2 r4 la8 fa |
    sib4. sol8 do'4. sib8 |
    la4 r do' r |
    r mi'8 mi' fa' fa' fa' fa' |
    mi'4 r r2 |
    R1*9 |
    sol'2. fa'4 |
    mi' sol' si do' |
    re'8([ si]) sol4 r2 |
    sol'2. fa'4 |
    mi'4 sol' si do' |
    re' re'8 re' re' re' re' re' |
    re'4 r r2 |
    R1*15 |
    r2 do'4 sol |
    r4 r8 sol fa'4 si |
    r2 r4 mi'8. mi'16 |
    mi'4 re' r re'8. re'16 |
    re'4 do' r2 |
    r4 do'8. do'16 sol'8. sol'16 fa'8. fa'16 |
    mi'4 r r mi'8. mi'16 |
    mi'4 re' r re'8. re'16 |
    re'4 do' r2 |
    r4 do'8. do'16 sol'8. sol'16 fa'8. fa'16 |
    mi'4 mi8. mi16 sol4 do' |
    r mi8. mi16 sol8. sol16 do'8. do'16 |
    mi'4 do' si fa' |
    mi' mi8. mi16 sol4 do' |
    r mi8. mi16 sol8. sol16 do'8. do'16 |
    mi'4 do' si fa' |
    mi' r r2 |
    R1*9 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 do' |
    la la r4 r2 |
    r4 r8 sol' mi' mi' r sol' |
    sol'4 mi' r2 |
    R1*5 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 do' |
    la la r4 r2 |
    r4 r8 sol' sol' mi' r sol' |
    sol'4 mi' r2 |
    R1*6 |
    r2 <>^\markup\italic sotto voce r4 r8 do'8 |
    la4 do'8. do'16 la4 do'8. do'16 |
    la4 do'8. do'16 la4 do' |
    r r8 sib sol4 mi |
    r r8 fa' do'4 la |
    r4 r8 sib sol4 mi |
    r4 r8 fa' do'4 la |
    r do' la8. la16 fa'4 |
    r mi' sol' do' |
    r do' sol re'8 re' |
    do'4 r8 do'^\f reb'4 do'8. do'16 |
    si4 do' r2 |
    r4 r8 do' reb'4 do'8. do'16 |
    si4 do' r2 |
    r4 r8 do' do'4 do'8. do'16 |
    reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
    mi'4 r r2 |
    R1 |
    r4 r8 do'^\f do'4 do'8. do'16 |
    reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
    mi'4 r r2 |
    R1 |
    r4 r8 <>^\markup\italic sotto voce do' do'([ re']) mi' do' |
    fa'4 fa'8. fa'16 fa'4 fa'8. fa'16 |
    fa'8([ sol']) la' fa' re'([ mi']) fa' re' |
    sib([ do']) re' sib do'([ re']) mi' do' |
    fa'([ do']) la([ do']) fa'4 fa'8. fa'16 |
    fa'8([ sol']) la' fa' re'([ mi']) fa' re' |
    sib([ do']) re' sib do'([ re']) mi' do' |
    fa'4 r8 fa' mi'([ fa']) sol' mi' |
    fa'4 do'8 fa' mi'([ fa']) sol' mi' |
    fa'([ sol']) la'([ fa']) re'([ mi']) fa' re' |
    sib([ do']) re' sib do'4 do'8 do' |
    fa'([ sol']) la'([ fa']) re'([ mi']) fa' re' |
    sib([ do']) re' sib do'4 do'8 do' |
    fa4 r r2 |
    R1*4 |
  }
  \tag #'voix4 {
    \clef "bass" R1*13 |
    r4 sib8 sol mi mi r4 |
    r do'8 la fa fa r4 |
    r sib,8 sib, si, si, si, si, |
    do4 r r2 |
    R1 |
    r4 do'8 do' do' do' la fa |
    mi4 do r2 |
    R1 |
    r4 do'8 do' do' do' la fa |
    do4 r8 do re4. re8 |
    do4 <>^\markup\italic (vuol partire.) r4 r2 |
    R1*8 |
    sol2. sol4 |
    sol sol sol sol |
    sol sol8 sol sol4 sol |
    si8 sol sol2 sol4 |
    sol sol sol sol |
    sol sol sol sol |
    si si8 si do' do' do' do' |
    si4 r r2 |
    R1*14 |
    r4 re'8. re'16 si8. si16 sol8. sol16 |
    do'4 do' r2 |
    r4 re'8. re'16 si8. si16 sol8. sol16 |
    do'4 r r2 |
    r4 la8. la16 re'4 re |
    r4 sol8. sol16 do'4 do |
    r la8. la16 sol8. sol16 sol8. sol16 |
    sol4 r r2 |
    r4 la8. la16 re'4 re |
    r sol8. sol16 do'4 do |
    r la8. la16 sol8. sol16 sol8. sol16 |
    sol4 do8. do16 mi8. mi16 sol8. sol16 |
    do'4 do r sol8. sol16 |
    sol4 r8 sol sol4 r8 sol |
    sol4 do8. do16 mi8. mi16 sol8. sol16 |
    do'4 do r sol8. sol16 |
    sol4 r8 sol sol4 r8 sol |
    sol4 r r2 |
    R1*8 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 sib |
    sol sol r4 r2 |
    R1 |
    r4 r8 si do' do' r si |
    do'4 do' r2 |
    R1*4 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 sib |
    sol sol r4 r2 |
    R1 |
    r4 r8 si do' do' r si |
    do'4 do' r2 |
    R1*6 |
    r2 <>^\markup\italic sotto voce r4 r8 do8 |
    fa4 do8 do fa4 do8 do |
    fa4 do8 do fa4 fa8 fa |
    mi4 do r r8 sib |
    la4 fa r r8 fa |
    mi4 do r r8 sib |
    la4 fa r do' |
    la8. fa16 fa4 r fa |
    mi( sol) sib mi |
    fa2 sib4 sib |
    do' r8 do^\f reb4 do8. do16 |
    si,4 do r2 |
    r4 r8 do reb4 do8. do16 |
    si,4 do r2 |
    r4 r8 do do4 do8. do16 |
    reb4 sib,8 sib, solb4 fa8. fa16 |
    mi4 r r2 |
    R1 |
    r4 r8 do^\f do4 do8. do16 |
    reb4 sib,8. sib,16 solb4 fa8. fa16 |
    mi4 r r2 |
    R1 |
    r4 r8 <>^\markup\italic sotto voce do8 do([ re]) mi do |
    fa4 fa8. fa16 fa4 fa8. fa16 |
    fa8([ sol]) la fa re([ mi]) fa re |
    sib,([ do]) re sib, do([ re]) mi do |
    fa([ do]) la,([ do]) fa4 fa8. fa16 |
    fa8([ sol]) la fa re([ mi]) fa re |
    sib,([ do]) re sib, do([ re]) mi do |
    fa4 r8 fa do([ re]) mi do |
    fa([ sol]) la fa do([ re]) mi do |
    fa([ sol]) la([ fa]) re([ mi]) fa re |
    sib,([ do]) re sib, do4 do8 do |
    fa([ sol]) la([ fa]) re([ mi]) fa re |
    sib,([ do]) re sib, do4 do8 do |
    fa4 r <>^\markup\italic { (Il Conte e Don Curzio partono.) } r2 |
    R1*4 |
  }
  \tag #'voix5 {
    \clef "bass" R1*8 |
    r2 <>^\markup\italic (abbracciando Figaro) fa4 fa |
    sol sol sol8([ sib]) la([ sol]) |
    fa4 fa fa fa |
    sol sol sol8([ sib]) la([ sol]) |
    fa4 r r2 |
    R1*4 |
    r4 sib2 sol8([ mi]) |
    fa8 fa r4 r2 |
    R1 |
    r4 sib2 sol8([ mi]) |
    fa fa r4 r2 |
    r fa4. fa8 |
    do' do' r4 r2 |
    R1*8 |
    r4 si2 do'4 |
    si8([ sol]) fa4 r2 |
    R1 |
    r4 si2 do'4 |
    si8([ sol]) fa4 r2 |
    R1 |
    r2 la4. la8 |
    sol4 sol r2 |
    R1*13 |
    mi2^\p sol |
    fa re |
    mi sol |
    fa re |
    mi sib! |
    la4 re lab lab |
    sol do la! r8 la |
    fa4 r8 fa sol4 r8 sol |
    do4 sib!2 sib4 |
    la re lab lab |
    sol do la! r8 la |
    fa4 r8 fa sol4 r8 sol |
    do4 mi'2 do'4 |
    sol sol sol sol |
    sol r8 sol sol4 r8 sol |
    do4 mi'2 do'4 |
    sol sol sol sol |
    sol r8 sol sol4 r8 sol |
    do4 r r2 |
    R1*7 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 sol |
    mi mi r4 r2 |
    R1*2 |
    r4 r8 re do do r re |
    do4 do r2 |
    R1*3 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 sol |
    mi mi r4 r2 |
    R1*2 |
    r4 r8 re do do r re |
    do4 do r2 |
    R1*7 |
    r2 r4 la |
    do'( la) fa do' |
    sib2 sib4 sol |
    do'( la) fa do' |
    sib2 sib4 sol |
    do'( la) fa fa |
    la2 do'4 la |
    sib2 sol4 sol |
    fa2 sib,4 sib, |
    do2 r |
    r4 sib la sol8 sol |
    sib4 la r2 |
    r4 do' sib la8 la |
    do'4 sib r2 |
    r r4 fa |
    sib( sol) sol sol |
    la2 do'4. do'8 |
    la2 sib4. sib8 |
    sib4 r r fa |
    sib( sol) sol sol |
    la2 do'4. do'8 |
    la2 sib4. mi8 |
    fa4 r r2 |
    r2 la |
    re'4 r sib r8 mi |
    fa4 r r2 |
    r2 la |
    re'4 r sib r8 sib |
    la4 r8 do' do'4 do'8 do' |
    do'4 do'8 do' do'4 do'8 do' |
    do'2 re' |
    re' do'4. do'8 |
    do'2 re' |
    sib2 do'4. do'8 |
    fa4 r r2 |
    R1*4 |
  }
  \tag #'voix6 {
    \clef "bass" R1*4 |
    r2 <>^\markup\italic (a Bartolo.) fa4. fa8 |
    mi4 r re re8 re |
    do do r4 fa4. la8 |
    sol4 sol do'4. mi8 |
    fa4 r r2 |
    R1*9 |
    r4 fa sol la |
    sol8 sol r4 r2 |
    R1 |
    r4 fa sol la |
    sol8 sol r sol sol4. sol8 |
    sol4 do r2 |
    R1*10 |
    r4 mi re do |
    sol8 sol r4 r2 |
    R1 |
    r4 mi re do |
    sol8 sol r sol re4. re8 |
    re4 sol r2 |
    R1*4 |
    r2 <>^\markup\italic (trattenendo Suzanna.) r4 si8. si16 |
    do'4 do' r2 |
    r si4. si8 |
    do'4 do' r2 |
    sol4 sol r sol |
    sol sol r2 |
    sol4 sol r2 |
    sol4 sol r2 |
    R1 |
    do2^\p mi |
    re sol, |
    do mi |
    re sol, |
    do sib,! |
    la,4 re lab, lab, |
    sol, do la,! r8 la, |
    fa4 r8 re sol4 r8 sol, |
    do4 sib,!2 sib,4 |
    la, re lab, lab, |
    sol, do la,! r8 la, |
    fa4 r8 re sol4 r8 sol, |
    do4 r r2 |
    r4 mi2 do4 |
    sol, sol, sol, sol, |
    do4 r r2 |
    r4 mi2 do4 |
    sol, sol, sol, sol, |
    do r r2 |
    R1*12 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 do' |
    re'4 sib!8 la sol4 la8 sib |
    do'4 la8 sol fa4. la,8 |
    sib,2 do4 do |
    fa r r2 |
    R1*4 |
    r2 r4 <>^\markup\italic (a Suzanna.) r8 do' |
    re'4 sib8 la sol4 la8 sib |
    do'4 la8 sol fa4 r8 la, |
    sib,2 do4. do8 |
    re4 r8 fa la4 fa8 la, |
    sib,2 do4. do8 |
    re4 r8 fa la4 fa8 la, |
    sib,2 do4. do8 |
    fa,4 r r2 |
    r r4 fa |
    fa2 fa4 fa |
    fa fa r fa |
    fa2 fa4 fa |
    fa4 fa r fa |
    fa2 la4 fa |
    sol2 do4 mi |
    fa2 sib,4 sib, |
    do2 r |
    r4 sol fa mi8 mi |
    sol4 fa r2 |
    r4 la sol fa8 fa |
    la4 sol r2 |
    r r4 fa |
    mi2 mi4 mi |
    fa2 fa4. fa8 |
    do2 do4. do8 |
    reb4 r r fa |
    mi2 mi4 mi |
    fa2 fa4. fa8 |
    do2 do4. do8 |
    fa4 r r2 |
    r2 re |
    sib,4 r do r8 do |
    fa4 r r2 |
    r2 re |
    sib,4 r do r8 do |
    fa4 r8 fa do4 do8 do |
    fa4 fa8 fa do4 do8 do |
    fa2 re |
    sib, do4. do8 |
    fa2 re |
    sib, do4. do8 |
    fa4 r r2 |
    R1*4 |
  }
>>