\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \keepWithTag #'all \global
        \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \keepWithTag #'all \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in F."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'all \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \keepWithTag #'all \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \keepWithTag #'all \global
        \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Susanne
        shortInstrumentName = \markup\character Su.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Marcelina
        shortInstrumentName = \markup\character Ma.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Don Curzio
        shortInstrumentName =  \markup\character Cu.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Il Conte
        shortInstrumentName = \markup\concat { C \super e }
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Bartolo
        shortInstrumentName =  \markup\character Ba.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix5 \includeNotes "voix"
      >> \keepWithTag #'voix5 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Figaro
        shortInstrumentName = \markup\character Fi.
      } \withLyrics <<
        \keepWithTag #'all \global \keepWithTag #'voix6 \includeNotes "voix"
      >> \keepWithTag #'voix6 \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column {
        Violoncello e Basso
      }
      shortInstrumentName = \markup\center-column { Vc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \keepWithTag #'all \global
      \includeNotes "basso"
      \origLayout {
        s1*6\pageBreak
        s1*7\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*7\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*5\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
