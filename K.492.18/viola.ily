\clef "alto" r8 do'(\fp la do' la do' la do') |
sib( do' sib do' sol do' sol do') |
la( do' la do' la do' la do') |
sib( do' sib do' sol do' sol do') |
la( do' la do') fa4 r |
fa r fa r |
fa4 r re'2~ |
re'( do'4 sib) |
la8( do' la do' la do' la do') |
sib( do' sib do' sib do' sib do') |
la( do' la do' la do' la do') |
sib( do' sib do' sib do' sib do') |
la4 r r2 |
r4 sib( sol mi) |
r do'( la fa) |
r8 fa( re fa re fa re fa) |
mi4 sol'2( fa'4) |
sol' sib( sol mi8 sol) |
fa4 la( sol fa) |
do'4 sol'2( fa'4) |
sol'4 sib( sol mi8 sol) |
fa4 la( sol fa) |
do'8 sol'4 sol'8(\cresc fa'8) fa'4 fa'8 |
sol'4 do'\f mi' do' |
sol r si8-.\p si-. do'-. do'-. |
re'4 r sol2~ |
sol4 r si8-. si-. do'-. do'-. |
re'4 r sol2~ |
sol4 r r2 |
r4 fa( re si) |
r sol( mi do') |
do'4 la2 re'4 |
re' re'2( do'4) |
re'8( si) fa2 fa4( |
mi) sol'( fa' mi') |
sol' re'2( do'4) |
fa'2 si4( re'8 si) |
do'4 sol'( fa' mi') |
sol'8 re'4 re'\cresc re' re'8 |
re'16\fp re' re' re' re' re' re' re' re'2:16 |
mib':16\fp re':16\fp |
mib':16\fp fa':16\fp |
mib':16\cresc do':16 |
re'16(\f sol fad sol fad sol fad sol fa sol fa sol fa\p sol fa sol) |
\rt#4 { mi16( sol) } <sol re'>8\f q4 q8 |
do'16 sol( fad sol fad sol fad sol fa sol fa sol fa\p sol fa sol) |
\rt#4 { mi16( sol) } <sol re'>8\f q4 q8 |
do'1\p |
si |
do' |
si |
do' |
si |
do'8( sol mi sol do sol mi sol) |
si( sol fa sol re sol fa sol) |
mi( sol mi sol do sol mi sol) |
si( sol fa sol re sol fa sol) |
mi( sol mi sol mi sol mi sol) |
mi( sol re sol re sol re sol) |
re( fa do mi) la2~ |
la( sol)~ |
sol8 sol( mi sol mi sol mi sol) |
mi( sol re sol re sol re sol) |
re( sol do mi) fa2~ |
fa( mi) |
re4 r r2 |
r4 mi'2*1/2(\mf s4\p do') |
sol sol sol sol |
do'4 r r2 |
r4 mi'2*1/2(\mf s4\p do') |
sol sol sol sol |
do'4 r do' r |
do' r do' r |
la8( do' la do' la do' la do') |
\rt#2 { sib( do' } \rt#2 { sib do') } |
\rt#2 { la( do' } \rt#2 { la do') } |
\rt#2 { sib( do' } \rt#2 { sib do') } |
la( do' la fa) la4 la |
sib sib do' do' |
fa8-. fa-. do-. fa-. fa-. sol-. re-. sol-. |
do-. do'-. mi-. la-. re-. re'-. fa-. sib-. |
sib mi'4( sol'8) fa'4 mi'8( sol') |
fa' la' re' sol' mi' do' re' sol' |
sol'\cresc sol' sol' sol' sol' sol' sol'\f sol' |
sol'4 r r r8 do'\p |
re'4( sib!8 la sol4 la8 sib) |
do'4( la8 sol fa4 sol8 la) |
re'4 re' r sib |
la8-. fa-. do-. do'-. sib-. sib-. re-. re'-. |
do' do' mi mi' re' re' fa fa' |
sib mi'( sol') sol' fa'4( mi'8 sol') |
fa'8 la' re' sol' mi' do' re' sol' |
sol'\cresc sol' sol' sol' sol' sol' sol'\f sol' |
sol'4 r r r8 do'\p |
re'4( sib!8 la sol4 la8 sib) |
do'4( la8 sol fa4 sol8 la) |
re'4 re' r sib |
la r r do' |
re' re' r sib |
la r r do' |
re' re' r sib |
la <<
  { do'2 do'4~ | do' do'2 do'4 | do'1~ | do'~ | do'~ | do' | } \\
  { la2\pp la4~ | la la2 la4 | sib1( | la) | sib( | la) | }
>>
r4 fa' r do' |
r do' r do' |
do'2( re') |
do'4 r8. do'16\f reb'4 do'8. do'16 |
si4 do' r2 |
r4 r8. do'16\f reb'4 do'8. do'16 |
si4 do' r2 |
r4 r8. do'16 do'4 do'8. do'16 |
reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
mi'4 r r2 |
R1 |
r4 r8. do'16\f do'4 do'8. do'16 |
reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
mi'4 r r2 |
R1*2 |
fa'1\p~ |
fa'2( re')~ |
re'4 re' r mi' |
fa'1~ |
fa'2( re')~ |
re'4 re' r sib |
la8\f sib do' la mi' fa' sol' mi' |
fa' do'4 fa'8 mi' fa' sol' mi' |
fa'2:8\p re':8 |
re':8 do':8 |
do':16\f re':16 |
re''2:16 do''4:16 sib':16 |
la'8 la4 la la la8( |
sib) sib4 sib sib sib8 |
la4 re' re' do' |
do' r fa fa |
fa2 r |
