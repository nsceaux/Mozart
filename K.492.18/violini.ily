\clef "treble" <>
<<
  \tag #'violino1 {
    R1 |
    r8 do'''\p do''' do''' do''' do''' do''' do''' |
    do'''8.( la''16) fa''4 r2 |
    r8 do''' do''' do''' do''' do''' do''' do''' |
    do'''8.( la''16) fa''4 r8 fa' fa' fa' |
    r mi' mi' mi' r re' re' re' |
    r do' do' do' r la' la' la' |
    r sol' sol' sol' r sol' sol' sol' |
    r do''' do''' do''' do'''( la'') la''( fa'') |
    r sol'' sol'' sol'' \appoggiatura la''16 sol''8 fa''16 mi'' \appoggiatura fa''16 mi''8 re''16 do'' |
    r8 do''' do''' do''' do'''( la'') la''( fa'') |
    r sol'' sol'' sol'' \appoggiatura la''16 sol''8 fa''16 mi'' \appoggiatura fa''16 mi''8 re''16 do'' |
    fa''4 r la'8 la' la' la' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
    re''8 sol'4 sol' sol' sol'8~ |
    sol'4 mi''2( fa''4) |
    mi''8( sib'' sol'' mi'' do'' sib' sol' mi') |
    fa'4 fa'( sol' la') |
    sol'4 mi''2( fa''4) |
    mi''8( sib'' sol'' mi'' do'' sib' sol' mi') |
    fa'4 fa'( sol' la') |
    sol'8 mi''4 mi''8(\cresc fa'') si''4 si''8( |
    do'''4)
  }
  \tag #'violino2 {
    r8 do''(\fp la' do'' la' do'' la' do'') |
    sib'( do'' sib' do'' sol' do'' sol' do'') |
    la'( do'' la' do'' la' do'' la' do'') |
    sib'( do'' sib' do'' sol' do'' sol' do'') |
    la'( do'' la' do'') r re' re' re' |
    r do' do' do' r sib sib sib |
    r la la la r fa' fa' fa' |
    r fa' fa' fa' r mi' mi' mi' |
    fa'( do'' la' do'' la' do'' la' do'') |
    sib'( do'' sib' do'' sib' do'' sib' do'') |
    la'( do'' la' do'' la' do'' la' do'') |
    sib'( do'' sib' do'' sib' do'' sib' do'') |
    la' fa' fa' fa' fa' fa' fa' fa' |
    fa'8 fa' mi' mi' sol' sol' sol' sol' |
    sol' sol' fa' fa' la' la' la' la' |
    sol'( fa' re' fa' re' fa' re' fa') |
    mi'( sol' sib' do'' sib' do'' la' do'') |
    sib'( sol' mi' sol' mi' do' sib do') |
    la( do' fa' do' mi' do' fa' do') |
    mi'( sol' sib' do'' sib' do'' la' fa') |
    sib'( sol' mi' sol' mi' do' sib do') |
    la( do' fa' do' mi' do' fa' do') |
    mi' do''4 do''8(\cresc si') fa''4 fa''8( |
    mi''4)
  }
>> \override Script.avoid-slur = #'outside
do''8.(\f\trill si'32 do'') mi''8-. mi''-. do''-. do''-. |
sol'4 <<
  \tag #'violino1 {
    r8 \appoggiatura la''16 sol''16(\p fad'' sol''8) sol''-. sol''-. sol''-. |
    sol''4 r fa'2( |
    mi'4) r8 \appoggiatura la''16 sol''16( fad'' sol''8) sol''-. sol''-. sol''-. |
    sol''4 r fa'2( |
    mi'8) do'' do'' do'' do'' do'' do'' do'' |
    do''( si') si' si' si'( re'') re'' re'' |
    re''( do'') do'' do'' do''( mi'') mi'' mi'' |
    mi''8( re''4) re'' re''( do''8) |
    si'4 si'2( do''4) |
    si'8( fa'' re'' si' sol' fa' re' si) |
    do'4 do''( re'' mi'') |
    re'' si''2( do'''4) |
    si''8( re''' si'' sol'' fa'' re'' si' re'') |
    do''4 do''( re'' mi'') |
    re''8 si''4 si''8(\cresc do''') do''' fad'' fad'' |
    sol''16\fp sol' sol' sol' sol' sol' sol' sol' sol'2:16 |
    lab':16\fp si':16\fp |
    do'':16\fp re'':16\fp |
    mib'':16\cresc fad'':16 |
    sol''4\f r sol'\f~ sol'16(\p la' si' do''32 re'') |
    mi''4 r8. do''16 si'\f do'' re'' mi'' fa'' sol'' la'' si'' |
    do'''4 r sol'4\f~ sol'16(\p la' si' do''32 re'') |
    mi''4 r8. do''16 si'\f do'' re'' mi'' fa'' sol'' la'' si'' |
    do'''4 r r8 sol'(\p mi'' do'') |
    fa''8.( re''16 si'4) r8 sol'( fa'' re'') |
    sol''8.( mi''16 do''4) r8 sol'( mi'' do'') |
    fa''8.( re''16 si'4) r8 sol'( fa'' re'') |
    sol''8.( mi''16 do''4) r8 sol'( mi'' do'') |
    fa''8.( re''16 si'4) r8 sol'( fa'' re'') |
    sol''8.( mi''16 do''4) r8 sol'( mi'' do'') |
    fa''8.( re''16 si'4) r8 sol'( fa'' re'') |
    sol''8.( mi''16 do''4) r8 sol'( mi'' do'') |
    fa''8.( re''16 si'4) r8 sol'( fa'' re'') |
    sol''8.( mi''16 do''4) r8 sol'( mi'' do'') |
    sol''8.( mi''16 fa''4) r8 fa'( fa'' re'') |
    fa''8.( re''16 mi''4) r8 mi''( do''' mi'') |
    r re''( do''' re'') r re''( si'' re'') |
    r2 r8 sol'( sol'' mi'') |
    sol''8.( mi''16 fa''4) r8 fa'( fa'' re'') |
    fa''8.( re''16 mi''4) r8 mi''( do''' mi'') |
    r8 re''( do''' re'') r re''( si'' re'') |
    do'''4
  }
  \tag #'violino2 {
    r4 re'8-.\p re'-. mi'-. mi'-. |
    fa'4 r re'2( |
    do'4) r re'8-. re'-. mi'-. mi'-. |
    fa'4 r re'2( |
    do'8) mi' mi' mi' mi'2:8 |
    fa':8 fa':8 |
    sol':8 sol':8 |
    la'1 |
    sol'8-. sol'( fa' sol' fa' sol' mi' sol') |
    fa'( re' si re' si re' si sol) |
    sol'( sol sol' do'') si'( sol' do'' sol') |
    si'( sol' fa' sol' fa' sol' mi' sol') |
    re'( fa' re' si re' fa' sol' fa') |
    mi'8( sol' do'' sol' si' sol' do'' sol') |
    si' sol''4 sol''8(\cresc fad'') fad'' do'' do'' |
    si'16\fp si si si si si si si si2:16 |
    do':16\fp sol':16\fp |
    sol':16\fp sol':16\fp |
    sol'2:16\cresc do'':16 |
    si'16(\f sol' fad' sol' fad' sol' fad' sol' fa' sol' fa' sol' fa'\p sol' fa' sol') |
    \rt#4 { mi'( sol') } <fa' sol>8\f q4 q8 |
    mi'16( sol' fad' sol' fad' sol' fad' sol' fa' sol' fa' sol' fa'\p sol' fa' sol') |
    \rt#4 { mi'( sol') } <fa' sol>8\f q4 q8 |
    mi'8(\p sol mi' sol sol' sol sol' sol) |
    fa'( sol fa' sol re' sol re' sol) |
    mi'( sol mi' sol sol' sol sol' sol) |
    fa'( sol fa' sol re' sol re' sol) |
    mi'( sol mi' sol sol' sol sol' sol) |
    fa'( sol fa' sol re' sol re' sol) |
    mi'( sol mi' do') sol'8.( mi'16 do'4) |
    r8 sol( re' si) fa'8.( re'16 si4) |
    r8 sol( mi' do') sol'8.( mi'16 do'4) |
    r8 sol( re' si) fa'8.( re'16 si4) |
    r8 sol( mi' do') sol'8.( mi'16 do'4) |
    r8 dod'( re' la) fa'8.( re'16 do'4) |
    r8 si( do' sol) r do''( mi'' do'') |
    r do''( re'' do'') r si'( re'' si') |
    r sol( mi' do') sol'8.( mi'16 re'4) |
    r8 dod'( re' la) fa'8.( re'16 do'4) |
    r8 si( do' sol) r do''( mi'' do'') |
    r do''( re'' do'') r si'( re'' si') |
    mi''4
  }
>> mi''2*1/2\mf( s4\p do'') |
sol'4 mi'2*1/2\mf( s4\p do') |
sol sol sol sol |
do'4 mi''2*1/2\mf( s4\p do'') |
sol'4 mi'2*1/2\mf( s4\p do') |
sol sol sol sol |
do'8 <<
  \tag #'violino1 {
    do''8-. do''-. do''-. do''( re'') sib'!-. sib'-. |
    sib'( do'') la'-. la'-. la'( sib') sol'-. sol'-. |
    fa'4 r r2 |
    r8 do'' do'' do'' do'' do'' do'' do'' |
    do''8.( la'16) fa'4 r2 |
    r8 do'' do'' do'' do'' do'' do'' do'' |
    do''8.( la'16) fa'8 do' do'( re') mi'-. fa'-. |
    fa'( mi') re'-. do'-. do'( sib) la-. sol-. |
    fa'8-. fa'-. la'-. fa'-. re'-. sol'(\trill sib') sol'-. |
    mi'-. la'(\trill do'') la'-. fa'-. sib'(\trill re'') sib'-. |
    sol'8-. sib'(\trill sol'') sib'-. la'-. fa''(\trill sol'') sib'-. |
    la'-. do''(\trill fa'') si'-. do''-. mi''(\trill fa'') si''-. |
    do'''4:16\cresc si'':16 do''':16 si'':16\f |
    do'''4
  }
  \tag #'violino2 {
    la'8-. la'-. la'-. la'( sib') sol'-. sol'-. |
    sol'( la') fa'-. fa'-. fa'( sol') mi'-. mi'-. |
    fa'8( do'' la' do'' la' do'' la' do'') |
    \rt#2 { sib'8( do'' } \rt#2 { sib'8 do'') } |
    \rt#2 { la'8( do'' } \rt#2 { la'8 do'') } |
    \rt#2 { sib'8( do'' } \rt#2 { sib'8 do'') } |
    la'8( do'' la') la' fa'4( do'') |
    re''8( do'') sib'-. la'-. la'( sol') fa'-. mi'-. |
    fa'8-. do'-. la-. do'-. sib-. re'-. sib-. re'-. |
    sol-. mi'-. do'-. mi'-. la-. fa'-. re'-. fa'-. |
    do'2:16 do':16 |
    do'16 do' fa' fa' la' la' re' re' sol' sol' do'' do'' si' si' fa'' fa'' |
    mi''4:16\cresc fa'':16 mi'':16 fa'':16\f |
    mi''4
  }
>> r4 r r8 do''\p |
re''4( sib'!8 la' sol'4 la'8 sib') |
do''4( la'8 sol' fa'4 sol'8 la') |
<<
  \tag #'violino1 { sol'4 sol' r sol' | }
  \tag #'violino2 { fa'4 fa' r mi' | }
>>
fa'8-. do''-. la'-. fa'-. re'-. re''-. sib'-. sol'-. |
mi' mi'' do'' la' fa' fa'' re'' sib' |
<<
  \tag #'violino1 {
    sol' sol'' mi'' sib' la' fa''(\trill sol'') sib'-. |
    la'-. do''(\trill fa'') si'-. do''-. mi''(\trill fa'') si'' |
    do'''4:16\cresc si'':16 do''':16 si'':16\f |
    do'''4
  }
  \tag #'violino2 {
    do'2:16 do':16 |
    do'16 do' fa' fa' la' la' re' re' sol' sol' do'' do'' si' si' fa'' fa'' |
    mi''4:16\cresc fa'':16 mi'':16 fa'':16\f |
    mi''4
  }
>> r4 r r8 do''\p |
re''4( sib'!8 la' sol'4 la'8 sib') |
do''4( la'8 sol' fa'4 sol'8 la') |
<<
  \tag #'violino1 {
    sol'4 sol' r sol' |
    fa' r r la' |
    sol' sol' r sol' |
    fa' r r la' |
    sol' sol' r sol' |
    fa'4 r r2 |
    R1 |
    r4 sol''(\pp mi'' do'') |
    r do'''( la'' fa'') |
    r sol''( mi'' do'') |
    r4 do'''( la'' fa'') |
    r do'' r fa'' |
    r mi'' r sib' |
    la' fa''( re'' sib') |
    la'(
  }
  \tag #'violino2 {
    fa'4 fa' r mi' |
    fa' r r fa' |
    fa' fa' r mi' |
    fa' r r fa' |
    fa' fa' r mi' |
    fa'4 fa'2\pp fa'4~ |
    fa' fa'2 fa'4 |
    mi'1( |
    fa') |
    mi'( |
    fa') |
    r4 la' r do'' |
    r sib' r sol' |
    fa' la'( sib' sol') |
    fa'4(
  }
>> mi'8)[ r16 do']\f reb'4 do'8. do'16 |
si4 do' r2 |
r4 r8. do'16\f reb'4 do'8. do'16 |
si4 do' r2 |
r4 r8. do'16\f do'4 do'8. do'16 |
reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
mi'4 r r2 |
R1 |
r4 r8. do'16\f do'4 do'8. do'16 |
reb'4 sib8. sib16 solb'4 fa'8. fa'16 |
mi'4 r r2 |
R1*2 |
<<
  \tag #'violino1 {
    fa''2(\p si') |
    do''2( fa'4) fa' |
    r sol' r do'' |
    fa''2( si') |
    do''( fa'4) fa' |
    r sol' r sol' |
    fa'4\f <fa' do'' fa''> r <mi' do'' sol''> |
    r <fa' do'' la''> r <mi' do'' sib''> |
    la''2:8\p la'':8 |
    sol'':8 sol'':8 |
    la'':16\f la'':16 |
    sol'':16 do''':16 |
    fa''8 do''' do''' do''' do''' do''' do''' do''' |
    do''' re'''16-. do'''-. sib''( la'') sol''-. fa''-. fa''( mi'') re''-. do''-. do''( sib') la'-. sol'-. |
    fa'4 la''8.(\trill sol''32 la'') sib''4 sol'' |
    fa''
  }
  \tag #'violino2 {
    la'2(\p sold') |
    la'2( la4) la |
    r fa' r sib' |
    la'2( sold') |
    la'( la4) la |
    r fa' r mi' |
    fa'16\f do' do' do' do'4:16 do'2:16 |
    do':16 do':16 |
    do''2:8\p fa'':8 |
    fa'':8 mi'':8 |
    fa'':16\f fa'':16 |
    fa'':16 mi'':16 |
    fa''8 fa'4 fa' fa' fa'8( |
    mi') mi'4 mi' mi' mi'8 |
    fa'4 fa''8.(\trill mi''32 fa'') sol''4 <mi'' sib'> |
    <la' fa''>4
  }
>> r8. <fa' la>16 q4 q |
q2 r |
