\clef "bass" fa,2\f fa4\p r |
sol r mi r |
fa r fa r |
sol r mi r |
fa r fa r |
fa r fa r |
fa r re r |
sib, r do r |
fa1~ |
fa~ |
fa~ |
fa |
fa,8 r la, r re r r4 |
sol,8 r sib, r mi r r4 |
la,8 r do r fa r r4 |
sib,2( si,) |
do4 do do do |
do do do do |
do do do do |
do do do do |
do do do do |
do do do do |
do do re\cresc re |
do do'\f mi' do' |
sol r r2 |
r2 sol\p |
do4 r r2 |
r sol |
do8 r mi r la r r4 |
re8 r fa r si r r4 |
mi8 r sol r do' r r4 |
fa2( fad) |
sol4 sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol sol sol |
sol sol la\cresc la |
sol\f r sol2\p~ |
sol8 sol sol sol fa2:8\fp |
mib:8\fp si,:8\fp |
do:8\cresc lab:8 |
sol4\f r r2 |
r2 sol8\f sol sol sol |
do4 r r2 |
r sol8\f sol sol sol |
do2(\p mi) |
re( sol,) |
do( mi) |
re( sol,) |
do( mi) |
re( sol,) |
do( mi) |
re( sol,) |
do( mi) |
re( sol,) |
do( sib,!) |
la,4( re) lab,2 |
sol,4( do) la,! r |
fa, r sol, r |
do( sib,!2.) |
la,4( re) lab,2 |
sol,4( do) la,! r |
fa, r sol, r |
do << { mi'2*1/2(^\mf s4^\p do'4) | sol } \\ { r4 r2 | r4 } >>
<>^"Bassi" mi2*1/2(\mf s4\p do) |
sol, sol, sol, sol, |
do4 << { mi'2*1/2(^\mf s4^\p do'4) | sol } \\ { r4 r2 | r4 } >>
<>^"Bassi" mi2*1/2(\mf s4\p do) |
sol, sol, sol, sol, |
do4 r do r |
do r do r |
fa r fa r |
sol r mi r |
fa r fa r |
sol r mi r |
fa fa la, la, |
sib, sib, do do |
fa,8-. la,-. fa,-. la,-. sib,-. sib,-. sol,-. sib,-. |
do-. do-. la,-. do-. re-. re-. sib,-. re-. |
mi-. sol-. do-. mi-. fa-. la-. do-. mi-. |
fa la re sol mi do re sol |
do\cresc do re re do do re\f re |
do4 r r r8 do\p |
re4( sib,!8 la, sol,4 la,8 sib,) |
do4( la,8 sol, fa,4 sol,8 la,) |
sib,4 r do r |
fa,8-. la,-. fa,-. la,-. sib,-. sib,-. sol,-. sib,-. |
do do la, do re re sib, re |
mi mi do mi fa la do mi |
fa la re sol mi do re sol |
do\cresc do re re do do re\f re |
do4 r r r8 do\p |
re4( sib,!8 la, sol,4 la,8 sib,) |
do4( la,8 sol, fa,4 sol,8 la,) |
sib,4 r do r |
re r r la, |
sib, r do r |
re r r la, |
sib, r do r |
fa1\pp~ |
fa |
fa~ |
fa~ |
fa~ |
fa |
fa2( la) |
sol( do) |
fa sib, |
do4 r8. do16\f reb4 do8. do16 |
si,4 do r2 |
r4 r8. do16\f reb4 do8. do16 |
si,4 do r2 |
r4 r8. do16\f do4 do8. do16 |
reb4 sib,8. sib,16 solb4 fa8. fa16 |
mi1\p |
fa4 r r2 |
r4 r8. do16\f do4 do8. do16 |
reb4 sib,8. sib,16 solb4 fa8. fa16 |
mi1\p |
fa4 r r2 |
do4 r do r |
fa fa fa fa |
fa fa re re |
sib, sib, do do |
fa fa fa fa |
fa fa re re |
sib, sib, do do |
fa8\f sol la fa do re mi do |
fa sol la fa do re mi do |
fa2:8\p re:8 |
sib,:8 do:8 |
fa:8\f re:8 |
sib,:8 do:8 |
fa,8 fa fa fa fa fa fa fa |
fa2:8 fa:8 |
fa4 re sib, do |
fa, r fa, fa, |
fa,2 r |
