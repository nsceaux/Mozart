% MARCELLINA (abbracciando Figaro)
\tag #'voix2 {
  Ri -- co -- no -- sci in que -- sto am -- ples -- so
  u -- na ma -- dre, a -- ma -- to fi -- glio!
}
% FIGARO (a Bartolo)
\tag #'voix6 {
  Pa -- dre mio, fa -- te lo stes -- so,
  non mi fa -- te più ar -- ros -- sir.
}
% BARTOLO (abbracciando Figaro)
\tag #'voix5 {
  Re -- si -- sten -- za, la co -- scien -- za
  far non la -- scia al tuo de -- sir.
}
% (Figaro abbraccia i genitori.)
% CURZIO
\tag #'voix3 {
  Ei suo pa -- dre, el -- la sua ma -- dre!
  l’i -- me -- neo non può se -- guir,
  ei suo pa -- dre, el -- la sua ma -- dre!
  l’i -- me -- neo non può se -- guir,
  no, l’i -- me -- neo non può se -- guir,
}
% CONTE
\tag #'voix4 {
  Son smar -- ri -- to, son stor -- di -- to,
  me -- glio è as -- sai di qua par -- tir,
  son smar -- ri -- to, son stor -- di -- to,
  me -- glio è as -- sai di qua par -- tir,
  di qua par -- tir.
}
% MARCELLINA, BARTOLO
\tag #'(voix2 voix5) {
  Fi -- glio a -- ma -- to!
  fi -- glio a -- ma -- to!
  fi -- glio a -- ma -- to!
}
%# FIGARO
\tag #'voix6 {
  Pa -- ren -- ti a -- ma -- ti!
  pa -- ren -- ti a -- ma -- ti!
  pa -- ren -- ti a -- ma -- ti!
}
% (Entra Susanna.)
% SUSANNA
\tag #'voix1 {
  Al -- to, al -- to! si -- gnor con -- te,
  mil -- le dop -- pie son qui pron -- te,
  a pa -- gar ven -- go per Fi -- ga -- ro,
  ed a por -- lo in li -- ber -- tà.
}
% MARCELLINA, BARTOLO
\tag #'(voix2 voix5) {
  Fi -- glio a -- ma -- to!
  fi -- glio a -- ma -- to!
  fi -- glio a -- ma -- to!
}
% CURZIO, CONTE
\tag #'voix3 {
  Non sap -- piam com’ è la co -- sa,
  os -- ser -- va -- te un po -- co là,
  os -- ser -- va -- te un po -- co là.
}
\tag #'voix4 {
  Non sap -- piam com’ è la co -- sa,
  com’ è la co -- sa,
  os -- ser -- va -- te un po -- co là,
  un po -- co là,
  os -- ser -- va -- te un po -- co là.
}
% FIGARO
\tag #'voix6 {
  Pa -- ren -- ti a -- ma -- ti!
  pa -- ren -- ti a -- ma -- ti!
  pa -- ren -- ti a -- ma -- ti!
}
% SUSANNA
%(vede Figaro che abbraccia Marcellina)
\tag #'voix1 {
  Già d’ac -- cor -- do col -- la spo -- sa,
  giu -- sti Dei, che in -- fe -- del -- tà,
  che in -- fe -- del -- tà!
  % (Vuol partire ma Figaro la trattiene.)
  La -- scia i -- ni -- quo!
  la -- scia i -- ni -- quo!
}
% FIGARO
\tag #'voix6 {
  No, t’ar -- re -- sta,
  no, t’ar -- re -- sta.
  Sen -- ti, oh ca -- ra, sen -- ti! sen -- ti!
}
% SUSANNA (dandogli uno schiaffo)
\tag #'voix1 {
  Sen -- ti ques -- ta!
}
% MARCELLINA, BARTOLO, FIGARO
\tag #'voix2 {
  È un ef -- fet -- to di buon co -- re,
  tut -- to a -- mo -- re è quel che fa,
  tut -- to a -- mo -- re è quel che fa,
  tut -- to a -- mo -- re è quel che fa,
  tut -- to a -- mo -- re è quel che fa,
  tut -- to a -- mo -- re è quel che fa, è quel che fa,
  tut -- to a -- mo -- re è quel che fa, è quel che fa.
}
\tag #'(voix5 voix6) {
  È un ef -- fet -- to di buon co -- re,
  tut -- to a -- mo -- re,
  tut -- to a -- mo -- re,
  tut -- to a -- mo -- re quel che fa,
  tut -- to a -- mo -- re,
  tut -- to a -- mo -- re,
  tut -- to a -- mo -- re è quel che fa,
  tut -- to a -- mo -- re è quel che fa,
  \tag #'voix5 {
    è quel che fa,
    tut -- to a -- mo -- re è quel che fa,
    è quel che fa.
  }
  \tag #'voix6 { tut -- to a -- mo -- re è quel che fa. }
}

% SUSANNA (a parte)
\tag #'voix1 {
  Fre -- mo, sma -- nio dal fu -- ro -- re,
  fre -- mo, sma -- nio dal fu -- ro -- re,
  fre -- mo, sma -- nio dal fu -- ro -- re,
  u -- na vec -- chia me la fa,
  fre -- mo, sma -- nio dal fu -- ro -- re,
  fre -- mo, sma -- nio dal fu -- ro -- re,
  u -- na vec -- chia me la fa,
  u -- na vec -- chia me la fa,
  u -- na vec -- chia me la fa,
  u -- na vec -- chia me la fa,
  u -- na vec -- chia me la fa.
}
% CONTE, CURZIO
\tag #'voix3 {
  Fre -- me, e sma -- nia,
  fre -- me e sma -- nia dal fu -- ro -- re,
  il de -- sti -- no glie -- la fa,
  fre -- me e sma -- nia dal fu -- ro -- re,
  il de -- sti -- no glie -- la fa,
  fre -- me e sma -- nia dal fu -- ro -- re,
  il de -- sti -- no glie -- la fa,
  fre -- me e sma -- nia dal fu -- ro -- re,
  il de -- sti -- no glie -- la fa.
}
\tag #'voix4 {
  Fre -- mo, sma -- nio dal fu -- ro -- re,
  il de -- sti -- no me la fa,
  fre -- mo e sma -- nio dal fu -- ro -- re,
  il de -- sti -- no me la fa,
  fre -- mo e sma -- nio dal fu -- ro -- re,
  il de -- sti -- no me la fa,
  fre -- mo e sma -- nio dal fu -- ro -- re,
  il de -- sti -- no me la fa,
  fre -- mo e sma -- nio dal fu -- ro -- re,
  il de -- sti -- no me la fa.
}
% MARCELLINA (a Susanna)
\tag #'voix2 {
  Lo sde -- gno cal -- ma -- te,
  mia ca -- ra fi -- gliuo -- la,
  sua ma -- dre ab -- brac -- cia -- te,
  che vo -- stra or sa -- rà,
  sua ma -- dre ab -- brac -- cia -- te
  che or vo -- stra sa -- rà.
}
% SUSANNA (a Bartolo)
\tag #'voix1 {
  Sua ma -- dre?
}
% BARTOLO
\tag #'voix5 {
  Sua ma -- dre!
}
% SUSANNA (al Conte)
\tag #'voix1 {
  sua ma -- dre?
}
% CONTE
\tag #'voix4 {
  Sua ma -- dre!
}
% SUSANNA (a Curzio)
\tag #'voix1 {
  sua ma -- dre?
}
% CURZIO
\tag #'voix3 {
  Sua ma -- dre!
}
% SUSANNA (a Marcellina)
\tag #'voix1 {
  sua ma -- dre?
}
% MARCELLINA
\tag #'voix2 {
  Sua ma -- dre!
}
%MARCELLINA, CURZIO, CONTE, BARTOLO
\tag #'(voix2 voix3 voix4 voix5) {
  sua ma -- dre!
  sua ma -- dre!
}
% SUSANNA (a Figaro)
\tag #'voix1 {
  tua ma -- dre?
}
% FIGARO
\tag #'voix6 {
  E quel -- lo è mio pa -- dre
  che a te lo di -- rà,
  che a te lo di -- rà.
}
% SUSANNA (a Bartolo)
\tag #'voix1 {
  Suo pa -- dre?
}
% BARTOLO
\tag #'voix5 {
  Suo pa -- dre!
}
% SUSANNA (al Conte)
\tag #'voix1 {
  suo pa -- dre?
}
% CONTE
\tag #'voix4 {
  Suo pa -- dre!
}
% SUSANNA (a Curzio)
\tag #'voix1 {
  suo pa -- dre?
}
% CURZIO
\tag #'voix3 {
  Suo pa -- dre!
}
% SUSANNA (a Marcellina)
\tag #'voix1 {
  suo pa -- dre?
}
% MARCELLINA
\tag #'voix2 {
  Suo pa -- dre!
}
% MARCELLINA, CURZIO, CONTE, BARTOLO
\tag #'(voix2 voix3 voix4 voix5) {
  suo pa -- dre!
  suo pa -- dre!
}
% SUSANNA (a Figaro)
\tag #'voix1 {
  Tuo pa -- dre?
}
% FIGARO
\tag #'voix6 {
  E quel -- la è mia ma -- dre,
  che a te lo di -- rà,
  che a te lo di -- rà,
  mia ma -- dre,
  che a te lo di -- rà,
  mio pa -- dre,
  che a te lo di -- rà.
}
% CURZIO, CONTE
\tag #'voix3 {
  Al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sa,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  al fie -- ro tor -- men -- to
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quell’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà.
}
\tag #'voix4 {
  Al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sa,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  al fie -- ro tor -- men -- to
  di que -- sto mo -- men -- to
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  al fie -- ro tor -- men -- to
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà.
}
% SUSANNA, MARCELLINA, BARTOLO, FIGARO
\tag #'(voix1 voix2 voix5 voix6){
  \tag #'(voix2 voix5) { Al dol -- ce, al dol -- ce con -- ten -- to }
  \tag #'(voix1 voix6) { Al dol -- ce con -- ten -- to }
  di que -- sto mo -- men -- to
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  al dol -- ce con -- ten -- to,
  di que -- sto mo -- men -- to,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  \tag #'(voix1 voix2) { ap -- pe -- na }
  re -- si -- ster or sà,
  \tag #'(voix1 voix2) { ap -- pe -- na }
  re -- si -- ster or sà,
  quest’ a -- ni -- ma ap -- pe -- na
  re -- si -- ster or sà,
  re -- si -- ster or sà,
  re -- si -- ster or sà.
}
