\piecePartSpecs
#`((fagotti #:tag-global all #:score-template "score-fagotti")
   (oboi #:tag-global all #:score-template "score-oboi")
   (flauti #:tag-global all #:score-template "score-flauti")
   (corni #:tag-global ()
          #:score-template "score-corni"
          #:instrument "Corni in F")
   (violino1 #:tag-global all)
   (violino2 #:tag-global all)
   (viola #:tag-global all)
   (basso #:tag-global all)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Sestetto: TACET } #}))
