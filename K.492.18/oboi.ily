\clef "treble" R1*6 |
<>\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { la'1 | sol' | fa'4 }
  { fa'1~ | fa'2( mi') | fa'4 }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { mi''1( | fa''4) }
  \tag #'(oboe2 oboi) { sib'1( | la'4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { mi''1( | fa''4) }
  \tag #'(oboe2 oboi) { sib'1( | la'4) }
>> r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2~ |
    fa''4 mi'' sol''2~ |
    sol''4 fa'' la''2 |
    sol''1~ |
    sol''4 }
  { r4 re'' |
    sib'4 sol' mi' mi'' |
    do'' la' fa' fa'' |
    fa''1( |
    mi''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    \tag #'oboi <>^\markup\concat { 1 \super o }
    r4 sib''2( sol''8 mi'') |
    fa''4 r r2 |
    R1 |
    r4 sib''2( sol''8 mi'') |
    fa''4 r r2 |
  }
  \tag #'oboe2 R1*5
>>
<<
  \tag #'(oboe1 oboi) { mi''2( fa'') | mi''4 }
  \tag #'(oboe2 oboi) { do''2( si') | do''4 }
  { s2 s\cresc }
>> <>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { \override Script.avoid-slur = #'outside
    do''8.(\trill si'32 do'') mi''8-. mi''-. do''-. do''-. |
    sol'4 }
  { \override Script.avoid-slur = #'outside
    do''8.(\trill si'32 do'') mi''8-. mi''-. do''-. do''-. |
    sol'4 }
>> r4 r2 |
R1 |
r2 <>\p <<
  \tag #'(oboe1 oboi) { re''4( mi'' | fa'') }
  \tag #'(oboe2 oboi) { si'4( do'' | re'') }
>> r4 r2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''1 |
    fa'' |
    sol'' |
    la'' |
    sol''4 }
  { do''1~ |
    do''8( si') si'4.( re''8) re''4~ |
    re''8( do'') do''4.( mi''8) mi''4~ |
    mi''8( re'') re''2.~ |
    re''4 }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(oboe1 oboi) { do''4( re'' mi'') | re'' }
  \tag #'(oboe2 oboi) { sol'4( si' do'') | si' }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(oboe1 oboi) { do''4( re'' mi'') | re''2( fad'') | sol''4 }
  \tag #'(oboe2 oboi) { sol'4( si' do'') | si'2( do'') | si'4 }
  { s2. | s2 s\cresc | s4\f }
>> r4 r2 |
R1*3 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''2~ sol''4~ sol'' |
    sol''2~ sol''~ |
    sol''~ sol''4~ sol'' |
    sol''2~ sol''~ |
    sol''4 }
  { sol''2 fa''4~ fa'' |
    mi''2( re'')( |
    do''4) sol'' fa''4~ fa'' |
    mi''2( re'') |
    do''4 }
  { s2.\f s4\p | s2 s\f | s2. s4\p | s2 s\f | }
>> r4 r2 |
R1 |
<>\p <<
  \tag #'(oboe1 oboi) {
    sol''1~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4
  }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { mi''4( fa''8) }
  \tag #'(oboe2 oboi) { dod''4( re''8) }
>> r8 r2 |
<<
  \tag #'(oboe1 oboi) { re''4( mi''8) }
  \tag #'(oboe2 oboi) { si'4( do''!8) }
>> r8 r2 |
R1*2 | \allowPageTurn
<<
  \tag #'(oboe1 oboi) { mi''4( fa''8) }
  \tag #'(oboe2 oboi) { dod''4( re''8) }
>> r8 r2 |
<<
  \tag #'(oboe1 oboi) { re''4( mi''8) }
  \tag #'(oboe2 oboi) { si'4( do''!8) }
>> r8 r2 |
R1 |
r4 <<
  \tag #'(oboe1 oboi) {
    mi'4( sol' do'') |
    mi''8 s sol'4( do'' mi'') |
    sol''8 s mi'' s fa'' s re'' s |
  }
  \tag #'(oboe2 oboi) {
    do'4( mi' sol') |
    do''8 s mi'4( sol' do'') |
    mi''8 s do'' s re'' s si' s |
  }
  { s2. | s8 r s2. | s8 r s r s r s r | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''8 } { do'' }
>> r8 <<
  \tag #'(oboe1 oboi) {
    mi'4( sol' do'') |
    mi''8 s sol'4( do'' mi'') |
    sol''8 s mi'' s fa'' s re'' s |
  }
  \tag #'(oboe2 oboi) {
    do'4( mi' sol') |
    do''8 s mi'4( sol' do'') |
    mi''8 s do'' s re'' s si' s |
  }
  { s2. | s8 r s2. | s8 r s r s r s r | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 } { do'' }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    r2 \tag #'oboi <>^\markup\concat { 1 \super o } la'4( do'' |
    sib' sol') do''4.( sol'8) |
    \appoggiatura sib'8 la'4 la' la'( do'') |
    sib'( sol' do''4. sol'8) |
    la'4 r r2 |
  }
  \tag #'oboe2 R1*5
>>
R1*4 |
r2 r4 r8 <<
  \tag #'(oboe1 oboi) { fa''8 | }
  \tag #'(oboe2 oboi) { si' | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 sol''2 sol''4 | }
  { do''4 fa'' mi'' fa'' | }
  { s2.\cresc s4\f | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 }
  \tag #'(oboe2 oboi) { mi'' }
>> r r2 |
R1*3 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { r4 fa'4.\p fad'8(\sfp sol'4)~ |
    sol'8 sold'(\sfp la'4.) la'8(\sfp sib'4)~ |
    sib'8 si'\sfp( do''2.)~ |
    do''4( fa'') mi'' }
  { R1*3 |
    r8 do''4( si'8) do''4 }
>> r4 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 sol''2 sol''4 | sol'' }
  { mi''4 fa'' mi'' fa'' | mi'' }
  { s2.\cresc s4\f | }
>> r4 r2 |
R1*3 |
r4 <>\p <<
  \tag #'(oboe1 oboi) { fa''4-.( fa''-. fa''-.) | }
  \tag #'(oboe2 oboi) { la'-.( la'-. do''-.) | }
>>
R1 |
r4 <<
  \tag #'(oboe1 oboi) { fa''4-.( fa''-. fa''-.) | }
  \tag #'(oboe2 oboi) { la'-.( la'-. do''-.) | }
>>
R1*11 |
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    sol'4( la' sib') |
    sol'( la')
  }
  \tag #'(oboe2 oboi) {
    mi'4( fa' sol') |
    mi'( fa')
  }
>> r2 |
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    la'4( sib' do'') |
    la'( sib')
  }
  \tag #'(oboe2 oboi) {
    fa'4( sol' la') |
    fad'( sol')
  }
>> r2 |
R1 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1( | la''4) }
  \tag #'(oboe2 oboi) { \tag#'oboi \once\tieDown do''1~ | do''4 }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { la'2( sib') | sib'4 }
  \tag #'(oboe2 oboi) { do'2( mi') | fa'4 }
>> r4 r2 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1( | la''4) }
  \tag #'(oboe2 oboi) { do''1~ | do''4 }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { la'2( sib') | la'4 }
  \tag #'(oboe2 oboi) { do'2( mi') | fa'4 }
>> r4 r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''1~ | fa''2 la'' | sol''1 | }
  { fa''1~ | fa''~ | fa''2 mi'' | }
>>
<>\f <<
  \tag #'(oboe1 oboi) {
    la''2 sib'' |
    la'' sib'' |
  }
  \tag #'(oboe2 oboi) {
    fa''2 sol'' |
    fa'' sol'' |
  }
>>
<>\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { la''1 | sol'' | la''1 | sol''1 | fa''4 }
  { fa''2 fa''~ | fa'' mi'' | fa''1~ | fa''2 mi'' | fa''4 }
  { s1*2 | s1\f | }
>> r4 r2 |
\override Script.avoid-slur = #'outside
r8 <<
  \tag #'(oboe1 oboi) {
    sol''8 sol'' sol'' sol'' sol'' sol'' sol'' |
    fa''4 la''8.(\trill sol''32 la'') sib''4 sol'' |
  }
  \tag #'(oboe2 oboi) {
    sib'8 sib' sib' sib' sib' sib' sib' |
    la'4 fa''8.(\trill mi''32 fa'') sol''4 mi'' |
  }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''4 } { fa'' }
>> r8. <<
  \tag #'(oboe1 oboi) {
    do''16 do''4 do'' |
    do''2
  }
  \tag #'(oboe2 oboi) {
    la'16 la'4 la' |
    la'2
  }
>> r2 |
