\clef "bass" R1*4 |
r2 <>\p <<
  \tag #'(fagotto1 fagotti) { fa2( | mi re) | }
  \tag #'(fagotto2 fagotti) { re2( | do sib,) | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do2( la) | sol1 | fa4 }
  { la,2( fa)~ | fa2( mi) | fa4 }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) { mi'1( | fa'4) }
  \tag #'(fagotto2 fagotti) { sib1( | la4) }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) { mi'1( | fa'4) }
  \tag #'(fagotto2 fagotti) { sib1( | la4) }
>> r4 <<
  \tag #'(fagotto1 fagotti) { la2 | sib1 | do' | }
  \tag #'(fagotto2 fagotti) { fa2 | sol1 | la | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'1( |
    do'4) mi'2( fa'4) |
    mi'8( do'8 sib4) r2 | }
  { sib2( si | do'4) r r2 | R1 | }
>>
R1 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { r4 mi'2( fa'4) |
    mi'8( do'8 sib4) r2 | }
  { R1*2 | }
>>
R1 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol1~ | sol4 do' mi' do' | sol }
  { do2( re) | do4 do mi do | sol, }
  { s2 s\cresc | s4 s2.\f | }
>> r4 r2 |
R1 |
r2 <>\p <<
  \tag #'(fagotto1 fagotti) { re'4( mi' | fa') }
  \tag #'(fagotto2 fagotti) { si4( do' | re') }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    do'1 |
    re' |
    mi' |
    fa'2( fad') |
    sol'4
  }
  \tag #'(fagotto2 fagotti) {
    do1 |
    re |
    mi |
    fa2( fad) |
    sol4
  }
>> r4 r2 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { r4 fa'!2( re'8 si) |
    do'4 mi( re do) |
    sol }
  { R1 |
    r4 mi( re do) |
    sol }
>> r4 r2 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { r4 fa'2( re'8 si) |
    do'4 mi( re do) |
    sol2( la) |
    sol4 }
  { R1 |
    r4 mi( re do) |
    sol2( re) |
    sol,4 }
  { s1*2 | s2 s\cresc | s4\f }
>> r4 r2 |
R1*3 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol2~ sol4~ sol |
    sol2~ sol~ |
    sol~ sol4~ sol |
    sol2~ sol~ |
    sol4 }
  { sol2 fa4~ fa |
    mi2( re)( |
    do4) sol fa4~ fa |
    mi2( re) |
    do4 }
  { s2.\f s4\p | s2 s\f | s2. s4\p | s2 s\f | }
>> r4 r2 |
R1*10 | \allowPageTurn
<<
  \tag #'(fagotto1 fagotti) { sol'4( fa'8) }
  \tag #'(fagotto2 fagotti) { mi'4( re'8) }
>> r8 r2 |
<<
  \tag #'(fagotto1 fagotti) { fa'4( mi'8) }
  \tag #'(fagotto2 fagotti) { re'4( do'8) }
>> r8 r2 |
R1*2 |
<<
  \tag #'(fagotto1 fagotti) { sol'4( fa'8) }
  \tag #'(fagotto2 fagotti) { mi'4( re'8) }
>> r8 r2 |
<<
  \tag #'(fagotto1 fagotti) { fa'4( mi'8) }
  \tag #'(fagotto2 fagotti) { re'4( do'8) }
>> r8 r2 |
R1 |
r4 <<
  \tag #'(fagotto1 fagotti) {
    mi4( sol do') |
    mi'8 s sol4( do' mi') |
    sol'8 s mi' s fa' s re' s |
  }
  \tag #'(fagotto2 fagotti) {
    do4( mi sol) |
    do'8 s mi4( sol do') |
    mi'8 s do' s re' s si s |
  }
  { s2. | s8 r s2. | s8 r s r s r s r | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'8 } { do' }
>> r8 <<
  \tag #'(fagotto1 fagotti) {
    mi4( sol do') |
    mi'8 s sol4( do' mi') |
    sol'8 s mi' s fa' s re' s |
  }
  \tag #'(fagotto2 fagotti) {
    do4( mi sol) |
    do'8 s mi4( sol do') |
    mi'8 s do' s re' s si s |
  }
  { s2. | s8 r s2. | s8 r s r s r s r | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'8 } { do' }
>> r8 <<
  \tag #'(fagotto1 fagotti) {
    do'4.( sib!8) sib4~ |
    sib8( la) la4.( sol8) sol4 |
  }
  \tag #'(fagotto2 fagotti) {
    la4.( sol8) sol4~ |
    sol8( fa) fa4.( mi8) mi4 |
  }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa4 s la( do' |
    sib sol) do'4.( sol8) |
    \appoggiatura sib8 la4 la la( do') |
    sib( sol do'4. sol8) |
    la4 r8 do' do'( re') mi'-. fa'-. | }
  { fa4 s4 r2 |
    R1*3 |
    r2 r4 r8 do' | }
  { s4 r s2 | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    fa'8( mi') re'-. do'-. do'( sib) la-. sol-. |
  }
  \tag #'(fagotto2 fagotti) {
    re'8( do') sib-. la-. la( sol) fa-. mi-. |
  }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa4 } { fa }
>> r4 r2 |
R1*2 |
r2 r4 r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol8 | do do re re do do re re | do4 }
  { sol8 | do do re re do do re re | do4 }
  { s8 | s2.\cresc s4\f | }
>> r4 r2 |
R1*3 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { r4 fa4.\p fad8\sfp( sol4)~ |
    sol8 sold(\sfp la4.) la8(\sfp sib4)~ |
    sib8 si\sfp( do'2.)~ |
    do'4 r }
  { R1*3 | r2 }
>> r4 r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol8 | do do re re do do re re | do4 }
  { sol8 | do do re re do do re re | do4 }
  { s8 | s2.\cresc s4\f | }
>> r4 r2 |
R1*3 |
r4 \tag#'fagotti <>^"a 2." re-.(\p re-. la,-.) |
R1 |
r4 re-.(\p re-. la,-.) |
R1*9 |
r4 <<
  \tag #'(fagotto1 fagotti) { fa'4( re' sib) | la( sol) }
  \tag #'(fagotto2 fagotti) { la4( sib sol) | fa( mi) }
>> r2 |
r4 <>\p <<
  \tag #'(fagotto1 fagotti) { sib4( la sol) | sib4( la) }
  \tag #'(fagotto2 fagotti) { sol4( fa mi) | sol( fa) }
>> r2 |
r4 <>\p <<
  \tag #'(fagotto1 fagotti) { do'4( sib la) | do'( sib) }
  \tag #'(fagotto2 fagotti) { la4( sol fa) | la( sol) }
>> r2 |
R1 |
<>\p <<
  \tag #'(fagotto1 fagotti) { do'1~ | do'4 }
  \tag #'(fagotto2 fagotti) { \tag#'fagotti \once\slurDown sib1( | la4) }
>> r4 r2 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa2( sol!) | fa4 }
  { do1 | reb4 }
>> r4 r2 |
<>\p <<
  \tag #'(fagotto1 fagotti) { do'1~ | do'4 }
  \tag #'(fagotto2 fagotti) { \tag#'fagotti \once\slurDown sib1( | la4) }
>> r4 r2 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa2( sol) | fa4 }
  { do1 | fa,4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'(fagotto1 fagotti) {
    fa'2( si |
    do' fa) |
    re'( sib) |
    la8
  }
  \tag #'(fagotto2 fagotti) {
    la2( sold |
    la re) |
    sib,( do) |
    fa8
  }
  { s1*3 | s8\f }
>> \tag#'fagotti <>^"a 2." sol8 la fa do re mi do |
fa sol la fa do re mi do |
fa2\p <<
  \tag #'(fagotto1 fagotti) {
    re'2 |
    re' do' |
    do' re' |
  }
  \tag #'(fagotto2 fagotti) {
    re2 |
    sib, do |
    fa re |
  }
  { s2 | s1 | s\f }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'2( do'4 sib) | }
  { sib,2 do | }
>>
<<
  \tag #'(fagotto1 fagotti) { la4 }
  \tag #'(fagotto2 fagotti) { fa }
>> r4 r2 |
r8 <<
  \tag #'(fagotto1 fagotti) {
    mi'8 mi' mi' mi' mi' mi' mi' |
    fa'4 re' sib do' |
    fa r fa fa |
    fa2
  }
  \tag #'(fagotto2 fagotti) {
    sol8 sol sol sol sol sol sol |
    la4 re sib, do |
    fa, r fa, fa, |
    fa,2
  }
  { s4. s2 | s1 | s4 r s2 | }
>> r2 |
