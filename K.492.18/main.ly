\version "2.19.80"
\include "common.ily"

\opusTitle "K.492 – N°18 – Riconosci in questo amplesso"

\header {
  title = \markup\center-column {
    \line\italic { Le nozze di Figaro }
    \line { N°18 – Sestetto: \italic { Riconosci in questo amplesso } }
  }
  opus = "K.492"
  date = "1786"
  copyrightYear = "2019"
}

\includeScore "K.492.18"
