\clef "treble" R1*6
<>\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { la''1 | sol'' | fa''4 }
  { fa''1~ | fa''2( mi'') | fa''4 }
>> r4 r2 |
R1*9 |
r4 <<
  \tag #'(flauto1 flauti) { fa''4( sol'' la'') | sol'' }
  \tag #'(flauto2 flauti) { do''4( mi'' fa'') | mi''4 }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(flauto1 flauti) {
    fa''4( sol'' la'') |
    sol''2( si'') |
    do'''4
  }
  \tag #'(flauto2 flauti) {
    do''4( mi'' fa'') |
    mi''2( fa'') |
    mi''4
  }
  { s2. | s2 s\cresc }
>> 
<>\f \twoVoices#'(flauto1 flauto2 flauti) <<
  { \override Script.avoid-slur = #'outside
    do'''8.(\trill si''32 do''') mi'''8-. mi'''-. do'''-. do'''-. |
    sol''4 }
  { \override Script.avoid-slur = #'outside
    do'''8.(\trill si''32 do''') mi'''8-. mi'''-. do'''-. do'''-. |
    sol''4 }
>> r4 r2 |
R1*2 |
r2 <>\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { si''2( |
    do'''4) s r la'' |
    fa'' re'' si' si'' |
    sol'' mi'' do'' do'''~ |
    do'''1 |
    si''4 si''2( do'''4) |
    si''8( sol'' fa''4) r2 | }
  { fa''2( | mi''4) s r2 | R1*5 }
  { s2 | s4 r }
>>
r4 <<
  \tag #'(flauto1 flauti) { do'''4( re''' mi''') | re''' }
  \tag #'(flauto2 flauti) { sol''4( si'' do''') | si'' }
>> \twoVoices#'(flauto1 flauto2 flauti) <<
  { si''2( do'''4) | si''8( sol'' fa''4) r2 | }
  { r4 r2 | R1 | }
>>
r4 <<
  \tag #'(flauto1 flauti) {
    do'''4( re''' mi''') |
    re'''2( fad''') |
    sol'''4
  }
  \tag #'(flauto2 flauti) {
    sol''4( si'' do''') |
    si''2( do''') |
    si''4
  }
  { s2. | s2 s\cresc | s4\f }
>> r4 r2 |
R1*4 |
r2 <>\f <<
  \tag #'(flauto1 flauti) { fa'''2( | mi'''4) }
  \tag #'(flauto2 flauti) { si''2( | do'''4) }
>> r4 r2 |
r2 <>\f <<
  \tag #'(flauto1 flauti) { fa'''2( | mi'''4) }
  \tag #'(flauto2 flauti) { si''2( | do'''4) }
>> r4 r2 |
R1*11 |
r2 <>\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { mi'''2 | re'''1 | do'''4 }
  { do'''2~ | do'''( si'') | do'''4 }
>> r4 r2 |
R1 |
r2 \twoVoices#'(flauto1 flauto2 flauti) <<
  { mi'''2 | re'''1 | do'''4 }
  { do'''2~ | do'''( si'') | do'''4 }
>> r4 r2 |
R1*5 | \allowPageTurn
r8 <<
  \tag #'(flauto1 flauti) {
    do'''8-. do'''-. do'''-. do'''( re''') sib''-. sib''-. |
    sib''( do''') la''-. la''-. la''( sib'') sol''-. sol''-. |
  }
  \tag #'(flauto2 flauti) {
    la''8-. la''-. la''-. la''( sib''!) sol''-. sol''-. |
    sol''( la'') fa''-. fa''-. fa''( sol'') mi''-. mi''-. |
  }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { fa''4 r4 la''( do''' |
    sib'' sol'') do'''4.( sol''8) |
    \appoggiatura sib''8 la''4 la'' la''( do''') |
    sib''( sol'' do'''4. sol''8) |
    la''4 r r2 | }
  { fa''4 r r2 | R1*4 | }
>>
r4 r8 <<
  \tag #'(flauto1 flauti) {
    do'''8 do'''( sib'') la''-. sol''-. |
  }
  \tag #'(flauto2 flauti) {
    la''8 la''( sol'') fa''-. mi''-. |
  }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { fa''4 } { fa'' }
>> r4 r2 |
R1*2 |
r2 r4 r8 <<
  \tag #'(flauto1 flauti) {
    si''8 |
    do'''4 fa''' mi''' fa''' |
    mi'''4
  }
  \tag #'(flauto2 flauti) {
    sol''8 |
    sol''4 si'' do''' si'' |
    do'''4
  }
  { s8 | s2.\cresc s4\f | }
>> r4 r2 |
R1*2 |
<>\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''1 |
    fa''8 s fa''4.\p fad''8(\sfp sol''4)~
    sol''8 sold''(\sfp la''4.) la''8(\sfp sib''4)~ |
    sib''8 si''\sfp( do'''2.)~ |
    do'''4 r }
  { fa''2( mi'') |
    fa''8 s r4 r2 |
    R1*2 |
    r2 }
  { s1 | s8 r }
>> <<
  \tag #'(flauto1 flauti) {
    sol''4( si'') |
    do'''4 fa''' mi''' fa''' |
    mi'''
  }
  \tag #'(flauto2 flauti) {
    mi''4( fa'') |
    sol'' si'' do''' si'' |
    do'''
  }
  { s2 | s2.\cresc s4\f }
>> r4 r2 |
R1*2 |
<>\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''1 | fa''4 }
  { fa''2 ( mi'' ) | fa''4 }
>> <<
  \tag #'(flauto1 flauti) { fa'''4-.( fa'''-. fa'''-.) | }
  \tag #'(flauto2 flauti) { la''-.( la''-. la''-.) | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''1 | fa''4 }
  { fa''2 ( mi'' ) | fa''4 }
>> <<
  \tag #'(flauto1 flauti) { fa'''4-.( fa'''-. fa'''-.) | }
  \tag #'(flauto2 flauti) { la''-.( la''-. la''-.) | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''1 | fa''4 }
  { fa''2 ( mi'' ) | fa''4 }
>> r4 r2 |
R1*7 |
r4 <<
  \tag #'(flauto1 flauti) {
    fa'''4( re''' sib'') |
    la''( sol'')
  }
  \tag #'(flauto2 flauti) {
    la''4( sib'' sol'') |
    fa''( mi'')
  }
>> r2 |
r4 <>\p <<
  \tag #'(flauto1 flauti) {
    sol''4( la'' sib'') |
    sol''( la'')
  }
  \tag #'(flauto2 flauti) {
    mi''4( fa'' sol'') |
    mi''( fa'')
  }
>> r2 |
r4 <>\p <<
  \tag #'(flauto1 flauti) {
    la''4( sib'' do''') |
    la''( sib'')
  }
  \tag #'(flauto2 flauti) {
    fa''4( sol'' la'') |
    fad''( sol'')
  }
>> r2 |
R1 |
<>\p <<
  \tag #'(flauto1 flauti) { do'''1~ | do'''4 }
  \tag #'(flauto2 flauti) { \tag#'flauti \once\slurDown sib''1( | la''4) }
>> r4 r2 |
<<
  \tag #'(flauto1 flauti) { fa''2( sol''!) | }
  \tag #'(flauto2 flauti) { do''2( mi'') | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { fa''4 } { fa'' }
>> r4 r2 |
<>\p <<
  \tag #'(flauto1 flauti) { do'''1~ | do'''4 }
  \tag #'(flauto2 flauti) { \tag#'flauti \once\slurDown sib''1( | la''4) }
>> r4 r2 |
<<
  \tag #'(flauto1 flauti) { fa''2( sol''!) | }
  \tag #'(flauto2 flauti) { do''2( mi'') | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { fa''4 } { fa'' }
>> r4 r2 |
R1*2 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { re'''2( si'' | do''' fa'') | }
  { la''2( sold'' | la'' fa'') }
>>
R1 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''1~ |
    do'''~ |
    do'''2 fa'''~ |
    fa''' mi''' |
    fa'''1~ |
    fa'''2 mi''' |
    fa'''4 }
  { do'''1~ |
    do'''~ |
    do'''4 la''2 la''4 |
    sol''1 |
    la'' |
    sol'' |
    la''4 }
  { s1\f | s | s\p | s | s\f | }
>> r4 r2 |
r8 <<
  \tag #'(flauto1 flauti) {
    \override Script.avoid-slur = #'outside
    mi'''8 mi''' mi''' mi''' mi''' mi''' mi''' |
    fa'''4 la''8.(\trill sol''32 la'') sib''4 sol'' |
  }
  \tag #'(flauto2 flauti) {
    \override Script.avoid-slur = #'outside
    sib''8 sib'' sib'' sib'' sib'' sib'' sib'' |
    la''4 fa''8.(\trill mi''32 fa'') sol''4 mi'' |
  }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { fa''4 s8. fa''16 fa''4 fa'' | fa''2 }
  { fa''4 s8. fa''16 fa''4 fa'' | fa''2 }
  { s4 r8. }
>> r2 |

