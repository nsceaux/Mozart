Io ti la -- scio, o ca -- ra! ad -- di -- o,
vi -- vi più fe -- li -- ce
e scor -- da -- ti di me.
Strap -- pa, strap -- pa pur dal tuo bel co -- re
quell’ af -- fet -- to, quell’ a -- mo -- re,
pen -- sa che a te non li -- ce,
il ra -- cor -- dar -- si di me.

Io ti la -- scio, o ca -- ra! ad -- di -- o,
vi -- vi più fe -- li -- ce
e scor -- da -- ti di me.
Strap -- pa, strap -- pa, strap -- pa pur dal tuo bel co -- re,
quell’ af -- fet -- to e quell’ a -- mo -- re,
pen -- sa o Di -- o che a te non li -- ce,
il ri -- cor -- dar -- si di me,
il ri -- cor -- dar -- si di me.

Io ti la -- scio, o ca -- ra ad -- di -- o,
vi -- vi più fe -- li -- ce
e scor -- da -- ti di me;
vi -- vi più fe -- li -- ce,
e scor -- da -- ti di me,
e scor -- da -- ti di me,
ti la -- scio, ad -- di -- o! ad -- di -- o! ad -- di -- o!
