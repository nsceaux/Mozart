\version "2.19.80"
\include "common.ily"

\opusTitle "K.Anh 245 – Io ti lascio o cara addio"

\header {
  title = \markup { Aria: \italic { Io ti lascio o cara addio } }
  opus = "K.Anh 245"
  copyrightYear = "2018"
}

\includeScore "K.Anh.245"
