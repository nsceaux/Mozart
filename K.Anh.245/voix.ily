\transpose do do, {
\clef "bass" r2 |
R1*4 |
r2 sol'4 fa' |
mib' sib mib' fa'8([ lab'16 do'']) |
mib'8.([ fa'16]) re'8 r sol'8. mib'16 lab'8. fa'16 |
sib'16([ si']) do''8 r16\fermata do''16([ lab' fa']) mib'8. mib'16 fa'8. sol'16 |
mib'4 r r sib'8 fa' |
r4 do''8 fa' re''8. do''16 sib'([ la']) sol'([ fa']) |
fa'8.([ mib'16]) re'8 r sol'4. fa'8 |
mi'8 mi' r4 fa'4. mib'8 |
re' re' r4 sol'8 \acciaccatura fa'8 mib'16 re'32([ do']) do''8. sib'16 |
la'8 sib' r4 r8 re' mib' mi' |
fa'4\melisma sol'8([ la'16 sib']) re'4\melismaEnd \acciaccatura fa'8 mib'!8. do'16 |
sib4 r\fermata sol'4 fa' |
mib' sib mib' fa'8([ lab'16 do'']) |
mib'8.([ fa'16]) re'8 r sol'8. mib'16 lab'8. fa'16 |
sib'16([ si']) do''8 r16\fermata do''16([ lab' fa']) mib'8. mib'16 fa'8. sol'16 |
mib'4 r sib'8 mib' r4 |
do''8 sol' r sol'16 do' do''8. do''16 do''8. sol'16 |
lab'8 lab' r4 lab'4. sol'8 |
fa'8 fa' r fa' sib'4. lab'8 |
sol' sol' r4 do''4. sib'8 |
lab' lab' r sol' fa'4. mib'8 |
re'8 re' r16 fa' fa' fa' solb'8.[ mib'16] solb'8. mib'16 |
re'8 r16 fa' fa'8 sib' la'4 do''8.\fermata sib'16 |
sib'4 r\fermata sol'! fa' |
mib' sib mib' fa'8([ lab'16 do'']) |
mib'8.([ fa'16]) re'8 r sol'8. mib'16 lab'8. fa'16 |
sib'16([ si']) do''8 r16\fermata do''16([ lab' fa']) mib'8. mib'16 fa'8. sol'16 |
mib'4 r fa'8 fa'16([ sib']) sib'8. fa'16 |
sol'8 sol' r sol' fa'8. fa'16 sib'8. fa'16 |
sol'8 r \acciaccatura sib'8 lab' r sol' r16 sol' \acciaccatura sol'8 fa' r16 fa' |
\acciaccatura do''8 sib' r r mib' lab' fa' r fa' |
sib' sol' r mib' do''8.([ lab'16]) lab'8 r |
r4 r8 re' fa' mib' r4 |
R1 |
R1^\fermataMarkup |
}