\clef "bass" r2 |
r4 fa,(\p sol, lab,) |
sib,4~ sib,8 r <>\f \ru#4 { mib16-. sib,-. } |
\ru#4 { fa-. sib,-. } sol16 sol sol sol lab lab lab lab |
sib4 sib,\p mib8[ r16. mib,32] mib,8 mib, |
mib,4 r r2 |
r4 lab,(\p sol, lab,) |
sib,4~ sib,8 r mib\< r mib\! r |
mib\mf\> lab,\! r4\fermata sib,8\p r sib, r |
mib4 r << { s4\f s4\p } \ru#4 { sib16-. fa-. } >> |
<< { s4\f s4\p } \ru#4 { la16-. fa-. } >> sib4 r |
fa4( sib,8) r mib r reb r |
do8 r sib, r la, r la, r |
sib,\> r sol, r mib,2\! |
mib8-!\mf re-! r4 sol,2(\p |
fa,8) r mib, r fa, r fa, r |
sib,4 r\fermata r2 |
r4 fa,( sol, lab,!) |
sib,~ sib,8 r mib r mib r |
mib\mf lab, r4\fermata sib,8\p r sib, r |
<< { s4 s\f } \ru#4 { mib16-. sol-. } >> sol,-.\p mib-. sol,-. mib-. fa,-.\f reb-. fa,-. reb-. |
mi,-.\p do-. mi,-. do-. do-.\f mi-. do-. mi-. mi,8\p mi\cresc mi mi |
fa4\f r sib,8\p r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r sib, r sib, r |
sib, r sib, r fa2\fermata\sf |
sib,4\p r4\fermata r2 |
r4 lab,( sol, lab,) |
sib,~ sib,8 r mib r mib r |
mib\mf lab, r4\fermata sib,8\p r sib, r |
mib r do'(\mf sib) lab\p r sib r |
mib r do'(\mf sib) lab\p r sib r |
mib r lab r sib r lab r |
sol r do r fa, r sib, r |
sol, r do r lab,2( |
sib,) mib,4 r8 sib-! |
mib'-!\< sib-! mib'-! sib-!\! mib' r mib,\f mib |
mib,4\pp r r2 |
