\clef "alto" r2 |
r4 re(\p mib lab) |
sol( fa8) r <>\f \ru#4 { mib'16-. sib-. } |
\ru#4 { fa'-. sib-. } mib'16 mib'8 mib'16 do''16 do''8( lab'16) |
sol'8 sol(\p sib8. lab16) sol8[ r16. mib32] mib8\> mib |
mib4\! r r2 |
r4 fa(\p sol do') |
sib4~ sib8 r mib' r mib' r |
mib'8\mf lab r4\fermata sib8\p r sib r |
sib4 r sib16-.\f fa-. sib-. fa-. sib-.\p fa-. sib-. fa-. |
la-.\f fa-. la-. fa-. la-.\p fa-. la-. fa-. sib4 r |
r16 fa( sol la) sib8 r sol2~ |
sol4.( mi8) fa2 |
r8 fa r sol sol2\p\< |
fa8-!\mf fa-! r4 sol2(\p |
fa8) fa-! r8 sol r fa r mib |
re4 r\fermata r2 |
r4 re( mib lab) |
sol( fa8) r mib' r mib' r |
mib'\mf lab r4\fermata sib8\p r sib r |
mib16-. sol-. mib-. sol-. mib-.\f sol-. mib-. sol-. sol-.\p mib'-. sol-. mib'-. fa-.\f reb'-. fa-. reb'-. |
mi-.\p do'-. mi-. do'-. do'-.\f mi'-. do'-. mi'-. sol16\p sol8\cresc sol sol sol16( |
fa4)\f r sib8\p r sib r |
sib r sib r sib r sib r |
sib r sib r r lab'4(\mf sol'8) |
r16 sol'( fa') fa'-! fa' fa' mib' mib' r mib'( re') re'-! r re'( do') do'-! |
sib16 re' fa' re' sib re' fa' re' sib mib' solb' mib' sib mib' solb' mib' |
sib re' fa' re' sib re' fa' re' fa'2\sf\fermata |
fa'4\p r\fermata sib( lab!) |
sol( fa sol do') |
sib~ sib8 r mib' r mib' r |
mib'\mf lab r4\fermata sib8\p r sib r |
sib r r4 do'8 r sib r |
sib r r4 do'8 r sib r |
sib r do' r sib r sib r |
sib r do' r do' r sib r |
sib r do' r mib' r do' r |
lab r fa r mib4 r8 sib' |
sib'2~ sib'8 r mib\f mib' |
mib4 r r2 |
