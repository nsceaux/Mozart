\clef "treble"
<<
  \tag #'violino1 {
    sol'4(\p fa') |
    mib'4( sib) mib'( fa'8\< lab'16 do'')\! |
    mib'8.( fa'16) re'8-! r r4 <mib' sib' sol''>8-!\f <mib' sol>8-! |
    r4 <re' sib' lab''>8-! <sib' re'>-!
  }
  \tag #'violino2 {
    re'4\p re' |
    r4 lab(\p sol do') |
    sib4~ sib8 r r4 <sib sol' mib''>8-!\f <sol mib'>-! |
    r4 <sib fa' re''>8-! <re' lab>-!
  }
>> sib''8.( lab''32 sol'') fa''([ sol'' lab'' sol''] fa''[ mib'' re'' do'']) |
<<
  \tag #'violino1 {
    sib'16( mib'') sol'8\p\<~ sol'16[ do''32( sib')]\! sib'([ lab' sol' fa']) mib'8[ r16. sib32]\> sib8 sib\! |
    sib4 r sol'(\p fa') |
    mib'( re' mib' lab') |
    sol'( fa'8) r r16 sol' sol' sol' r lab' lab' lab' |
    sib'(\mf si' do''8) r4\fermata mib'(\p re') |
    mib'8[ r16 sol'](\< fa' re' mib' do') <re' sib' sib''>8-!\f fa'8-! r4 |
    <la' do'''>8-! fa'-! r4 re''8.(\p do''16) sib'( la'! sol' fa') |
    fa'8.( mib'16) re'8 r r sol'4( fa'8) |
    r8 \once\slurDashed mi'4( sol'8) r fa'4( mib'!8) |
    r re' r re' do'2\p\< |
    do'8-!\mf re'-! r4 sib'2\p~ |
    sib'8 re'-! r mib' r re' r do' |
    sib4 r\fermata sol'( fa') |
    mib'( sib mib' fa') |
    mib'( re'8) r r16 sol' sol' sol' r lab' lab' lab' |
    sib'16(\mf si' do''8) r4\fermata mib'(\p re') |
    mib'8 r <mib' sib' sol''>8-!\f <mib' sol>-! r4 reb'''8-! reb'-! |
    r4 <sol' mi'' do'''>8-! sol'-! do'16\p do'8\cresc do' do' do'16~ |
    do'4\f r r8 lab'4(\p sol'8) |
    r16 fa'( mi' fa' sol' fa' lab' fa') r8 sib'4( lab'8) |
    r16 sol'( fad' sol' lab' sol' sib' sol') r8 do'''4( sib''8) |
    r16 sib''(\mf lab'') lab''-! lab'' lab'' sol''\> sol'' r sol''( fa'') fa''-! r fa''( mib'') mib''-! |
    re''8\p fa' r fa'' r solb'' r solb' |
    r fa' r fa'' la'2\sf\fermata |
    sib'4\p r\fermata sol'!4(\pp fa') |
    mib'4( re' mib' lab') |
    sol'( fa'8) r r16 sol' sol' sol' r lab' lab' lab' |
    sib'(\mf si' do''8) r4\fermata mib'4(\p re') |
    mib'8 r lab'(\> sol') fa'\p r fa' r |
    sol' r lab'(\> sol') fa'\p r fa' r |
    sol' r lab' r sol' r fa' r |
    sib' r sol' r lab' r fa' r |
    sib' r sol'\p r do'' r lab' r |
    fa' r re' r mib'4 r16 lab''(\mf\> fa'' re'' |
    mib'' lab'' fa'' re''\< mib'' lab'' fa'' re'' mib''8)\! r <mib' sib' sol''>8\f sib'' |
  }
  \tag #'violino2 {
    sib'16( sol') mib'8\p~ mib'16[ lab'32( sol')] sol'( fa' mib' re') mib'8[ r16. sol32]\> sol8 sol\! |
    sol4 r sib(\p lab) |
    sol( sib2 fa'4) |
    mib'( re'8) r r16 mib'\> mib' mib'\! r fa'\> fa' fa'\! |
    sol'8\mf(\> lab'\!) r4\fermata sol(\p lab) |
    sol8[ r16 mib'](\< re' sib do' la) <sib fa' re''>8-!\f\> <re' sib>-!\! r4 |
    <fa' mib''>8-! <mib' do'>-! r4 sib'8.(\p la'16) sol'( fa' mib' re') |
    re'8.( do'16) sib8 r sib4( si) |
    do'(\< reb')\! do'2\> |
    r8 sib! r sib\! sib2\ppp\< |
    la8-!\mf\> sib-!\! r4 sib2\p~ |
    sib8 sib-! r do' r sib r la |
    sib4 r\fermata sib( lab!) |
    sol( lab sol do') |
    sib4~ sib8 r r16 mib' mib' mib' r fa' fa' fa' |
    sol'8(\mf lab') r4\fermata sol(\p lab) |
    sol8 r <sib sol' mib''>8-!\f sib-! r4 <fa' reb'' lab''>8-! lab-! |
    r4 <mi' do'' sol''>8-! <mi' do'>-! sib16\p sib8\cresc sib sib sib16( |
    lab4)\f r r8 fa'4(\p mib'!8) |
    r16 re'(\< dod' re' mib'\> re' fa' re'\!) r8 sol'4( fa'8) |
    r16 mib'(\< re' mib' fa'\> mib' sol' mib'\!) r8 do''4(\mf sib'8) |
    r16 sib'(\mf\> lab') lab'-! lab' lab' sol' sol' r sol'( fa') fa'-! r fa'( mib') mib'-! |
    re'8\p re' r re'' r mib'' r mib' |
    r re' r re'' <mib' do'>2\sf\fermata |
    <re' sib>4\p r\fermata sib2\pp~ |
    sib2.( fa'4) |
    mib'4( re'8) r r16 mib' mib' mib' r fa' fa' fa' |
    sol'8(\mf lab') r4\fermata sol4(\p lab) |
    sol16 mib'-.(\mf mib'-. mib'-. mib'-. mib'-. mib'-. mib'-.) mib'(-.\p mib'-. mib'-. mib')-. re'(-. re'-. re'-. re')-. |
    mib'-.(\mf mib'-. mib'-. mib'-. mib'-. mib'-. mib'-. mib'-.) mib'(-.\p mib'-. mib'-. mib')-. re'(-. re'-. re'-. re')-. |
    mib'8 r fa' r mib' r re' r |
    mib' r mib' r fa' r re' r |
    mib' r mib' r lab' r fa' r |
    re' r lab r sol4 r16 re'( fa' lab' |
    sol'\< re' fa' lab' sol' re' fa' lab' sol'8) r <sib sol' mib''>\f\> sol'' | <>\!
  }
>>
<mib' sol>4\p r r2 |
