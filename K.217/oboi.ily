\clef "treble" \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'8( la') |
    si'4. la'16( si') do''8( si') |
    si'( la') sol'4 si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
    re''( do'') si'4-. }
  { r4 |
    sol'4. fad'16( sol') la'8( sol') |
    fad'4( sol') sol'8( la') |
    si'4~ si'16( sol' la' si') do''8( si') |
    fad'16( sol' la' fad') sol'4-. }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r4 r mi''8(\p re'') |
    re''( do'') si'4 do''8( si') |
    mi''8.( fad''16) sol''8 }
  { R2. |
    r4 r do''8( si') |
    mi''8.( fad''16) sol''8 }
  { s2. | s4 s s4\f | }
>> r8 r4 |
R2.*8 |
r8 <<
  \tag #'(oboe1 oboi) {
    sol'( si' re'') do''([ fad'']) |
    fad''4( sol'')
  }
  \tag #'(oboe2 oboi) {
    re'8( sol' si') la'([ do'']) |
    do''4( si')
  }
>> r4 |
R2.*4 |
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    re''4-.( re''-.) |
    sol''2.~ |
    sol'' |
    fad''8-. s fad''-. s fad''-. s |
    sold''-. s sold''-. s sold''-. s |
    la'' s sold'' s sold'' s |
    la''4
  }
  \tag #'(oboe2 oboi) {
    si'4-.( si'-.) |
    dod''2. mi'' |
    re''8-. s re''-. s re''-. s |
    re''8-. s re''-. s re''-. s |
    dod'' s re'' s re'' s |
    dod''4
  }
  { s2 |
    s2.*2 |
    s8 r s r s r |
    s r s r s r |
    s r s r s r | }
>> r4\fermata r |
<>\f <<
  \tag #'(oboe1 oboi) { re''1 | }
  \tag #'(oboe2 oboi) { fad' | }
>>
R1 |
<<
  \tag #'(oboe1 oboi) { sol''1 }
  \tag #'(oboe2 oboi) { mi'' }
>>
R1*3 |
<<
  \tag #'(oboe1 oboi) { sol''1 | }
  \tag #'(oboe2 oboi) { dod'' | }
  { s2.\p s4\cresc }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 sold'' | }
  { re''1 | }
>>
<<
  \tag #'(oboe1 oboi) { la''4 s s re'''8-. re'''-. | }
  \tag #'(oboe2 oboi) { dod''4 s s re''8-. fad''-. | }
  { s4\f r r }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''8([ dod''']) }
  { sol''4 }
>> r4 r2 |
r r4 <<
  \tag #'(oboe1 oboi) { re'''8-. re'''-. | }
  \tag #'(oboe2 oboi) { re''-. fad''-. | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''8([ dod''']) }
  { sol''4 }
>> r4 r2 |
R1*9 |
<>\p <<
  \tag #'(oboe1 oboi) { la''1~ | la'' | }
  \tag #'(oboe2 oboi) { la'1~ | la' | }
>>
R1*3 |
r8 \tag#'oboi <>^"a 2." la'8-.\f la'-. la'-. la'2\fp~ |
la' la'\fp~ |
la' la'\fp~ |
la' r |
R1*5 |
r4 <re'' la''>2\fp r4 |
R1 |
r4 <re'' la''>2\fp r4 |
R1*2 |
r4 <>\cresc <<
  \tag #'(oboe1 oboi) { si''4 la''8-. sol''-. fad''-. mi''-. | }
  \tag #'(oboe2 oboi) { re''4 fad''8-. mi''-. re''-. dod''-. | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 } { re'' }
>> <<
  \tag #'(oboe1 oboi) {
    la''8. la''16 la''4 la'' |
    la'' la''8. la''16 la''4 la'' |
    la''4
  }
  \tag #'(oboe2 oboi) {
    fad''8. fad''16 fad''4 fad'' |
    fad'' fad''8. fad''16 fad''4 fad'' |
    fad''4
  }
>> \tag#'oboi <>^"a 2." re''8. re''16 re''4 re'' |
re'' r r2 |
R1*4 |
<>\p <<
  \tag #'(oboe1 oboi) { fad''1( | sol''8) }
  \tag #'(oboe2 oboi) { do''!1( | si'8) }
>> r8 r4 r2 |
<<
  \tag #'(oboe1 oboi) { sol''1( | fad''8) }
  \tag #'(oboe2 oboi) { dod''1( | re''8) }
>> r8 r4\fermata r |
R2.*4 | \allowPageTurn
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r4 r8 sol' mi''( re'') |
    re''( do'') si'4 do''8( si') |
    mi''8.( fad''16) sol''8-. }
  { R2. |
    r4 r do''8( si') |
    mi''8.( fad''16) sol''8-. }
  { s2. | s2 s4\f | }
>> r8 r4 |
r r <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'8-. re'-. | }
  { re'8-. re'-. | }
>>
<<
  \tag #'(oboe1 oboi) { sol'4. la'8( si' la') | }
  \tag #'(oboe2 oboi) { re'4. fad'8( sol' fad') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 } { sol' }
>> r4 r |
R2.*3 |
r4 <<
  \tag #'(oboe1 oboi) {
    mi''4( sol'') |
    fa''2.~ |
    fa'' |
    mi''8-. s mi''-. s mi''-. s |
    dod''-. s dod''-. s dod''-. s |
    re''-. s dod''-. s sol''-. s |
    fad''4
  }
  \tag #'(oboe2 oboi) {
    do''4 do'' |
    si'2.~ |
    si' |
    do''8-. s do''-. s do''-. s |
    sol'-. s sol'-. s sol'-. s |
    fad'-. s sol'-. s dod''-. s |
    re''4
  }
  { s2 | s2.*2 |
    s8 r s r s r |
    s r s r s r |
    s r s r s r | }
>> r4\fermata r |
<>\f <<
  \tag #'(oboe1 oboi) { sol''1 | }
  \tag #'(oboe2 oboi) { si' | }
>>
R1 |
<<
  \tag #'(oboe1 oboi) { do'''1 | }
  \tag #'(oboe2 oboi) { la'' | }
>>
R1*3 |
<<
  \tag #'(oboe1 oboi) {
    do'''1 |
    si''2 sol'' |
    fad''4
  }
  \tag #'(oboe2 oboi) {
    fad''1 |
    sol''2 dod'' |
    re''4
  }
  { s1\p\cresc | s | s4\f }
>> r4 r <<
  \tag #'(oboe1 oboi) { sol''8-. sol''-. | }
  \tag #'(oboe2 oboi) { sol'8 si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8([ fad'']) }
  { do''!4 }
>> r4 r2 |
r r4 <<
  \tag #'(oboe1 oboi) { sol''8-. sol''-. | }
  \tag #'(oboe2 oboi) { sol'8 si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8([ fad'']) }
  { do''4 }
>> r4 r2 |
R1*2 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1~ | sol''4 }
  \tag #'(oboe2 oboi) { \tag#'oboi \once\slurDown mi''1( | re''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { sol''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { do'''1( | si''4) }
  \tag #'(oboe2 oboi) { fad''1( | sol''4) }
>> r4 r2 |
R1*15 |
r2\fermata r |
r8 <>\p <<
  \tag #'(oboe1 oboi) {
    re''8-. re''-. re''-. re''2~ |
    re'' re''~ |
    re''
  }
  \tag #'(oboe2 oboi) {
    re'8-. re'-. re'-. re'2~ |
    re' re'~ |
    re'
  }
  { s4. s2\fp | s s\fp | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | r2 do'''(\f | si''4) }
  { re'2~ | re'4 re''(\f mi'' fad'') | sol'' }
  { s2\fp }
>> r4 r2 |
R1*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 }
  { re''8( red'' mi''4) }
>> r4 |
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 }
  { re''8( red'' mi''4) }
>> r4 |
r4 r8 <<
  \tag #'(oboe1 oboi) {
    sol''8 \appoggiatura si''16 la''4 sol''8 fad'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    si'8 \appoggiatura re''!16 do''4 si'8 la' |
    si'4
  }
>> r4\fermata r |
R2. |
r4 r <>\p <<
  \tag #'(oboe1 oboi) {
    si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
  }
  \tag #'(oboe2 oboi) {
    sol'8( la') |
    si'4. la'16( si') do''8( si') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8( do'') si'4-. }
  { fad'16( sol' la' fad') sol'4-. }
>> r4 |
<<
  \tag #'(oboe1 oboi) {
    r4 r8 \tag#'oboi <>^\markup\concat { 1 \super o }
    sol'8-. mi''([ re'']) |
    re''( do'') si'4-. r\fermata |
  }
  \tag #'oboe2 { R2. | R^\fermataMarkup | }
>>
R1*4 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2~ | sol''4 do'''( la'' fad'') | }
  { re''2( | mi'') do'' | }
  { s2\p\cresc | s s\f }
>>
<>\p <<
  \tag #'(oboe1 oboi) { sol''4 }
  \tag #'(oboe2 oboi) { si' }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    s4 sol''( fad'') s |
    s sol''( fad'') s |
    sol''8-. s fad''-. s sol''-. s fad''-. s |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    s4 re''( do'') s |
    s re''( do'') s |
    si'8-. s do''-. s si'-. s do''-. s |
    si'4
  }
  { r4 s2 r4 |
    r4 s2 r4 |
    s8 r s r s r s r | }
>> r4 r2 |
R1*3 |
r2 r4 <>\p\cresc <<
  \tag #'(oboe1 oboi) {
    fad''4 |
    sol''( do''') si''( la'') |
  }
  \tag #'(oboe2 oboi) {
    do''4 |
    si'( mi'') sol''( fad'') |
  }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 } { sol'' }
>> <<
  \tag #'(oboe1 oboi) {
    re''8. re''16 re''4 re'' |
    re'' re''8. re''16 re''4 re'' |
    re''
  }
  \tag #'(oboe2 oboi) {
    si'8. si'16 si'4 si' |
    si' si'8. si'16 si'4 si' |
    si'
  }
>> r8 \tag#'oboi <>^"a 2." re'8-. sol'-. la'-. si'-. do''-. |
re'' mi'' re'' do'' si' la' sol' fad' |
sol'4 r <<
  \tag #'(oboe1 oboi) { re''4 }
  \tag #'(oboe2 oboi) { si' }
>> r4 |
<<
  \tag #'(oboe1 oboi) { sol''2 }
  \tag #'(oboe2 oboi) { si' }
>> r2 |
