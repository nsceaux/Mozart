\piecePartSpecs
#`((oboi #:score-template "score-oboi")
   (corni #:score-template "score-corni" #:tag-global ()
          #:instrument "Corni in G")
   (violino1)
   (violino2)
   (viola)
   (basso)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
