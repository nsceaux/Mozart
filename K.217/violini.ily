\clef "treble"
<<
  \tag #'violino1 {
    sol'8( la') |
    si'4. la'16( si') do''8( si') |
    si'( la') sol'4-. si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
    re''( do'') si'4-. do''8(\p si') |
    si'( la') sol'4-. mi''8( re'') |
    re''( do'') si'4-. do''8(\f si') |
    mi''8.( fad''16) sol''8 r do''(\p si') |
    re''16( si' re'' si') la'4-. sol'8( la') |
    si'4. la'16( si') do''8( si') |
    si'( la') sol'4-. si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
    re''( do'') si'4-. do''8( si') |
    si'( la') sol'4-. mi''8( re'') |
    re''( do'') si'4-. re''8( red'') |
    mi'' mi''~ mi''16 sol''( fad'' mi'') re''( do'' si' la') |
    sol'16(\f re' si re') sol'( si' re'' si') do''( la' do'' la') |
    la'4( sol') re'8-.\p re'-. |
    sol'16-. re'-. si-. re'-. sol'-. re'-. la'-. re'-. si'-. re'-. la'-. re'-. |
    si'-. sol'-. re'-. sol'-. si'-. re'-. do''-. re'-. re''-. re'-. do''-. re'-. |
    si'-. re'( sol' si') r sol'( si' re'') r si'( re'' sol'') |
    r do'( fad' la') r fad'( la' do'') r la'( re'' la'') |
    r re'( sol' si') r si'( re'' sol'') r re''( sol'' si'') |
    r mi'( sol' dod'') r sol'( dod'' mi'') r dod''( mi'' sol'') |
    r la( mi' sol') r mi'( sol' mi'') r la'( mi'' sol'') |
    r fad'( la' re'') r la'( re'' fad'') r re''( fad'' la'') |
    r re'( sold' si') r sold'( si' re'') r si'( re'' sold'') |
    r dod''( mi'' la'') r si'( re'' sold'') r re'''( si'' sold'') |
    la''4 r\fermata r |
  }
  \tag #'violino2 {
    sol'16( re' fad' re') |
    sol'( re' si re') sol'( re' fad' sol') la'( re' sol' re') |
    fad'( re' do' re') si( re' sol' fad') sol'( re' la' re') |
    si'( sol' re' sol') si'( sol' la' si') do''( sol' si' sol') |
    fad'( sol' la' fad') sol'( re' si sol) mi'(\p sol' re' fad') |
    do'( re' mi' do') si( do' re' si) do'( sol' si sol') |
    la( re' mi' fad') sol'( re' si sol) mi'(\f sol' re' sol') |
    sol'( fad' sol' la') re'8-. r la'(\p sol') |
    si'16( sol' si' sol') fad'4-. sol'16( re' fad' re') |
    sol'( re' si re') sol'( re' fad' sol') la'( re' sol' re') |
    fad'( re' do' re') si( re' sol' fad') sol'( re' la' re') |
    si'( sol' re' sol') si'( sol' la' si') do''( sol' si' sol') |
    fad'( sol' la' fad') sol'( re' si sol) mi'( sol' re' fad') |
    do'( re' mi' do') si( do' re' si) do'( sol' si sol') |
    la( re' mi' fad') sol'( re' si sol) sol'( fad' sol' sol) |
    sol'( fad' sol' sol) sol'-. mi''( re'' do'') si'( la' sol' fad') |
    sol'16(\f re' si re') re'( sol' si' sol') la'( fad' la' fad') |
    fad'4( sol') r |
    r r re'8-.\p re'-. |
    sol'16-. re'-. si-. re'-. sol'-. re'-. la'-. re'-. si'-. re'-. la'-. re'-. |
    <<
      { sol'16 re' re' re' re'4:16 re':16 | re'2:16 } \\
      { s16 si si si si4:16 si:16 | do'2:16 }
    >> <re' la'>4:16 |
    <sol' si>2.:16 |
    <la sol'>:16 |
    << 
      { la'2.:16 | la'4:16 la'2:16 | } \\
      { mi'2.:16 | re'4:16 fad'2:16 | }
    >>
    <si sold'>2.:16 |
    <la la'>4:16 << mi'2:16 \\ re':16 >> |
    << mi'4 \\ dod' >> r\fermata r |
  }
>>
<>\f \ru#4 { re'16-. la-. re'-. fad'-. } |
<>\p <<
  \tag #'violino1 { re'8-. la'-. re''-. fad''-. la''-. fad''-. re''-. la'-. | }
  \tag #'violino2 \ru#4 { re'16-. la'-. fad'-. la'-. }
>>
<>\f \ru#4 { mi'16-. la-. mi'-. sol'-. } |
<>\p <<
  \tag #'violino1 {
    mi'8-. la'-. dod''-. mi''-. sol''-. mi''-. dod''-. la'-. |
    mid''(\fp fad'') fad''-. fad''-. dod''(\fp re'') re''-. re''-. |
    lad'(\fp si') si'-. si'-. fad'(\fp sol') sol'-. sol'-. |
    mi'2:16\p mi''2*1/2:16 s4\cresc |
    fad''2:16 sold'':16 |
    la''4\f la''16( sol''! fad'' mi'') re''4 re'''8-. re'''-. |
    re'''( dod''') dod''-.\p re''-. mi''-. re''-. mi''-. dod''-. |
    re''4 la''16(\f sol'' fad'' mi'') re''4 re'''8-. re'''-. |
    re'''( dod''') dod''-.\p re''-. mi''-. re''-. mi''-. dod''-. |
    re''8-. re''-. do''!-. do''-. si'-. si'-. lad'-. lad'-. |
    si'-!\fp si' si' si' si' si' si' si' |
    si' r si'' r si'' r si' r |
    la'-!\fp la' la' la' la' la' la' la' |
    la' r la'' r la'' r la' r |
    sol'-!\fp sol' sol' sol' sol' sol' sol' sol' |
    sol' r sol'' r sol'' r sol' r |
    fad'8-. la'( re''4) r8 fad''( la''4) |
    r8 la'-. mi''-. sol''-. r la'-. mi''-. sol''-. |
    r la'8( re''4) r8 fad''( la''4) |
    r8 la'-. mi''-. sol''-. r la'-. mi''-. sol''-. |
    fad''16( sol'' fad'' sol'') la''8-. la''( fad'') fad''( re'') re''( |
    si') dod''( re'' red'' mi'' fad'' sol'' mi'') |
    dod''4( re''!) \appoggiatura fad''8 mi''4 re''8( dod'') |
    re''4 r sol''8(\f fad'' mi'' re'') |
    dod''(\p mi'') re''( la') sol''8(\f fad'' mi'' re'') |
    dod''(\p mi'') re''( la') sol''8(\f fad'' mi'' re'') |
    dod''(\p mi'') re''( la') sol''(\f fad'') mi''(\p fad'') |
    mi''(\f re'') dod''(\p re'') dod''(\f si') la'(\p sol') |
    fad'4 fad''8-. la''-. re''-. fad''-. si'16(\f re'') dod''( mi'') |
    re''8-. re''-. fad''-.\p la''-. re''-. fad''-. si'16(\f re'') dod''( mi'') |
    re''8-. re''-. fad''-.\p la''-. re''-. fad''-. si'16(\f re'') dod''( mi'') |
    re''8-.\p re''-. si'16(\f re'') dod''( mi'') re''8-.\p re''-. si'16(\f re'') dod''( mi'') |
    re''8(\p fad'') <la'' la'>2\f sol''8-.\p fad''-. |
    mi''-. si'-. mi''-. fad''\trill sol''( mi'') re''( dod'') |
    re''( fad'') <la'' la'>2\f sol''8-.\p fad''-. |
    mi''-. si'-. mi''-. fad''\trill sol''( mi'') re''( dod'') |
    re''-. re''-. re''( sol'') fad''-. mi''-. re''-. dod''-. |
    re''4:16\cresc si'':16 la''8-. sol''-. fad''-. mi''-. |
    re''4\f <re' la' la''> <re' la' fad''> <re' la' la''> |
    <re' la' re'''> <re' la' la''> <re' la' fad''> <re' la' la''> |
    <re' la' re'''>4
  }
  \tag #'violino2 {
    \ru#4 { mi'16-. la'-. sol'-. la'-. } |
    dod''8(\fp re'') re''-. re''-. lad'8(\fp si') si'-. si'-. |
    fad'8(\fp sol') sol'-. sol'-. red'(\fp mi') mi'-. mi'-. |
    dod'2:16\p dod''2*1/2:16 s4\cresc |
    re''2:16 re'':16 |
    dod''4\f r re'16( mi' fad' sol') la'8-. la'-. |
    la'4 mi'8-.\p fad'-. sol'-. fad'-. sol'-. mi'-. |
    fad'4 r re'16(\f mi' fad' sol') la'8-. la'-. |
    la'4 mi'8-.\p fad'-. sol'-. fad'-. sol'-. mi'-. |
    fad'2 sol'4( re') |
    re'8-!\fp re' re' re' re' re' re' re' |
    re' r si' r si' r re' r |
    re'-!\fp re' re' re' re' re' re' re' |
    re' r la' r la' r re' r |
    si-!\fp si si si si si si si |
    dod' r dod'' r dod'' r mi' r |
    re'4 r8 la'( fad'4) r8 la'( |
    sol') mi'-. dod'4-. sol'8-. mi'-. dod'4 |
    re'4 r8 la'( fad'4) r8 la'( |
    sol') mi'-. dod'4-. sol'8-. mi'-. dod'4 |
    re'16( mi' re' mi') fad'8-. fad'( la') re'( la) la'( |
    sol'4) r sol'8( la' si' sol') |
    mi'4( fad') \appoggiatura la'8 sol'4 fad'8( mi') |
    fad'4 r si'8(\f la' sol' fad') |
    mi'(\p sol') fad'( re') si'(\f la' sol' fad') |
    mi'(\p sol') fad'( re') si'(\f la' sol' fad') |
    mi'(\p sol') fad'( re') mi''(\f re'') dod''(\p re'') |
    dod''(\f si') la'(\p si') la'(\f sol') fad'(\p mi') |
    re'4 fad'8-. la'-. re'-. fad'-. si16(\f re') dod'( mi') |
    re'8-. re'-. fad'8-.\p la'-. re'-. fad'-. si16(\f re') dod'( mi') |
    re'8-. re'-. fad'8-.\p la'-. re'-. fad'-. si16(\f re') dod'( mi') |
    re'8-.\p re'-. si16(\f re') dod'( mi') re'8-.\p re'-. si16(\f re') dod'( mi') |
    re'8(\p la') <re'' re'>2\f re''8-.\p dod''-. |
    si'-. sol'-. si'-. red''\trill mi''8( sol') fad'( mi') |
    fad'( la') <re''! re'>2\f re''8-.\p dod''-. |
    si'-. sol'-. si'-. red''\trill mi''8( sol') fad'( mi') |
    re'( fad') si'-. si'-. la'-. sol'-. fad'-. mi'-. |
    re'4:16\cresc re'':16 fad''8-. mi''-. re''-. dod''-. |
    re''16-.\f la'-. fad'-. la'-. \ru#7 { re'-. la'-. fad'-. la'-. } |
    <re' la' fad''>4
  }
>> <<
  { re'8. re'16 re'4 re' | re' } \\
  { re'8. re'16 re'4 re' | re' }
>> r8 mi'\p fad'-. sol'-. la'-. si'-. |
do''!-. r la'-. r fad'-. r re'-. r |
sol'4 r8 re''-. si'-. la'-. sol'-. fad'-. |
mi'-. r-. sol'-. r dod'-. r <<
  \tag #'violino1 { dod''8-. r | re''4 }
  \tag #'violino2 { sol'8-. r | fad'4 }
>> r8 re' fad'-. sol'-. la'-. si'-. |
do''!-. r la'-. r fad'-. r re'-. r |
sol'4 r8 re''-. si'-. la'-. sol'-. fad'-. |
mi'-. r-. sol'-. r dod''-. r <<
  \tag #'violino1 { sol''8-. r | fad''-. }
  \tag #'violino2 { dod''8-. r | re''-. }
>> r8 r4\fermata <<
  \tag #'violino1 {
    sol'8( la') |
    si'4. la'16( si') do''!8( si') |
    si'( la') sol'4-. si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
    re''8( do'') si'4 do''8( si') |
    si'( la') sol'8-. sol'-. mi''( re'') |
    re''( do'') si'4 do''8(\f si') |
    mi''8.( fad''16) sol''8-. sol'-.\p do''8( si') |
    re''16( si' re'' si') la'4 r |
    r r re'8-. re'-. |
    sol'16-. re'-. si-. re'-. sol'-. re'-. la'-. re'-. si'-. re'-. la'-. re'-. |
    si'-. sol'-. re'-. sol'-. si'-. re'-. do''-. re'-. re''-. re'-. do''-. re'-. |
    si'-. re'( sol' si') r sol'( si' re'') r si'( re'' sol'') |
    r sol'( si' re'') r si'( re'' sol'') r re''( sol'' si'') |
    r sol'( do'' mi'') r do''( mi'' sol'') r mi''( sol'' do''') |
    r re'( fa' si') r fa'( si' re'') r si'( re'' fa'') |
    r re'( fa' si') r fa'( si' re'') r si'( re'' fa'') |
    r mi'( sol' do'') r sol'( do'' mi'') r do''( mi'' sol'') |
    r dod'( mi' sol') r sol'( dod'' mi'') r dod''( mi'' sol'') |
    r la'( re'' fad''!) r mi'( sol' dod'') r sol''( mi'' dod'') |
    re''4 r\fermata r |
  }
  \tag #'violino2 {
    sol'16( re' fad' re') |
    sol'( re' si re') sol'( re' fad' sol') la'( re' sol' re') |
    fad'( re' do' re') si( re' sol' fad') sol'( re' la' re') |
    si'( sol' re' sol') si'( sol' la' si') do''( sol' si' sol') |
    fad'( sol' la' fad') sol'( re' si sol) mi'( sol' re' fad') |
    do'( re' mi' do') si( do' re' si) do'( sol' si sol') |
    la( re' mi' fad') sol'( re' si sol) mi'(\f sol' re' sol') |
    sol'( fad' sol' la') re'8-. re'-.\p la'( sol') |
    si'16( sol' si' sol') fad'4 re'8-. re'-. |
    sol'16-. re'-. si-. re'-. sol'-. re'-. la'-. re'-. si'-. re'-. la'-. re'-. |
    sol'4 r re'8-. re'-. |
    sol'16-. re'-. si-. re'-. sol'-. re'-. la'-. re'-. si'-. re'-. la'-. re'-. |
    <<
      { sol'16 re' re' re' re'2:16 | re'2.:16 | mi':16 | } \\
      { s16 si si si si2:16 | si2.:16 | do':16 | }
    >>
    <fa' sol>2.:16 |
    q:16 |
    <sol mi'>:16 |
    <la sol'>:16 |
    <la fad'!>4:16 <sol' la>2:16 |
    <fad' la>4 r\fermata r |
  }
>>
<>\f \ru#4 { sol'16-. re'-. sol'-. si'-. } |
<>\p <<
  \tag #'violino1 { sol'8-. si'-. re''-. sol''-. si''-. sol''-. re''-. si'-. | }
  \tag #'violino2 \ru#4 { sol'16-. re'-. si-. re'-. }
>>
<>\f \ru#4 { la'16-. re'-. la'-. do''-. } |
<>\p <<
  \tag #'violino1 {
    la'8-. re''-. fad''-. la''-. do'''-. la''-. fad''-. do''-. |
    do''8(\fp si') si'-. si'-. mi''(\fp re'') re''-. re''-. |
    sol''(\fp fad'') fad''-. fad''-. mi''(\fp re'') re''-. re''-. |
    do''2:16 fad'':16\cresc |
    sol'':16 dod''':16 |
    re'''4\f re''16( do'' si' la') sol'4 sol''8-. sol''-. |
    sol''([ fad'']) la'-.\p si'-. do''-. la'-. re''-. do''-. |
    si'4 re''16(\f do'' si' la') sol'4 sol''8-. sol''-. |
    sol''([ fad'']) la'-.\p si'-. do''-. la'-. re''-. do''-. |
    si'8-. si'-. do''-. do''-. re''-. re''-. red''-. red''-. |
    mi''-!\fp mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi''8-. r mi''-. r mi''-. r mi''-. r |
    re''-!\fp re'' re'' re'' re'' re'' re'' re'' |
    re'' r re'' r re'' r re'' r |
    do''-!\fp do'' do'' do'' do'' do'' do'' do'' |
    do'' r do'' r do'' r do'' r |
    si'-. si'( re''4) r8 si'( re''4) |
    r8 sol''-. mi''-. do''-. r sol''-. mi''-. do''-. |
    si'-. si'( re''4) r8 si'( re''4) |
    r8 sol''-. mi''-. do''-. r sol''-. mi''-. do''-. |
    si'-!\fp si' si' si' do''-!\fp do'' do'' do'' |
    re''8-!\fp re'' re'' re'' mi'' mi'' fad'' fad'' |
    sol''(\f fad'' mi'' red'') mi''(\p re'' do'' si') |
    la'4( si') \appoggiatura re''8 do''4( si'8 la') |
    sol'8 r16 si''32(\f do''' re'''8) do'''-.\p si''-. la''-. sol''-. fa''-. |
    mi''-. re''-. do''-. si'-. do''-. mi''-. re''-. do''-. |
    si'8 r16 si''32(\f do''' re'''8) do'''-.\p si''-. la''-. sol''-. fa''-. |
    mi''-. re''-. do''-. si'-. do''-. mi''-. re''-. la'-. |
    si' r16 si''32(\f do''' re'''8) do'''-.\p si''-. la''-. sol''-. fa''-. |
    mi''-. re''-. do''-. si'-. do''-. mi''-. la'-. re''-. |
    si'4 r dod'' r |
    re''8-. r dod''-. r re''-. r sol''-. r |
    fad''4 r\fermata sol''8(\f fad'' mi'' re'') |
    do''!(\p la') si'( re'') sol''(\f fad'' mi'' re'') |
    do''(\p la') si'( re'') sol''(\f fad'' mi'' re'') |
    do''(\p la') si'( re'') sol''(\f fad'' mi'' re'') |
    do''4:16 re'':16 mi'':16 fad'':16 |
    sol''4 si''8-.\p re'''-. sol''-. si''-. mi''16(\f sol'') fad''( la'') |
    sol''8[-. sol'']-. si''8-.\p re'''-. sol''-. si''-. mi''16(\f sol'') fad''( la'') |
    sol''8[-. sol'']-. si''8-.\p re'''-. sol''-. si''-. mi''16(\f sol'') fad''( la'') |
    sol''8-.\p sol''-. mi''16(\f sol'') fad''( la'') sol''8-.\p sol''-. mi''16(\f sol'') fad''( la'') |
    sol''4\p sol''2\f fad''8(\p mi'') |
    \appoggiatura mi''16 re''4\fp do''8( si') \appoggiatura re''16 do''4\fp si'8( la') |
    re''( sol'') sol''4\f~ sol''8 sol''-. fad''-.\p mi''-. |
    \appoggiatura mi''16 re''4\fp do''8( si') \appoggiatura re''16 do''4\fp si'8( la') |
    <sol' sol>4 r\fermata sol'8(\p la') |
    si'4. la'16( si') do''8( si') |
    si'( la') sol'4-. si'8( do'') |
    re''4. do''16( re'') mi''8( re'') |
    re''( do'') si'4-. do''8( si') |
    si'( la') sol'8[-. sol']-. mi''( re'') |
    re''( do'') si'4-. r4\fermata |
    sol''16-.\p la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. do''-. la'-. la''-. sol''-. la''-. fad''-. |
    sol''16-. la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. do''-. la'-. la''-. sol''-. la''-. fad''-. |
    sol'' la'' si'' la'' sol'' fad'' mi'' re'' do'' si' do'' la' la'' sol'' la'' fad'' |
    sol''( la'' si'' sol'') fad''( sol'' la'' fad'') sol''( la'' si'' sol'') fad''( sol'' la'' fad'') |
    sol''-. la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-.
  }
  \tag #'violino2 {
    \ru#4 { la16-. re'-. do'-. re'-. } |
    la8(\fp sol) sol-. sol-. do'(\fp si) si-. si-. |
    mi'8(\fp re') re'-. re'-. do'(\fp si) si-. si-. |
    la2:16 do'':16\cresc |
    si':16 sol'':16 |
    fad''4\f r sol16( la si do') re'8-. re'-. |
    re'4 fad'8-.\p sol'-. la'-. fad'-. fad'-. la'-. |
    sol'4 r sol16(\f la si do') re'8-. re'-. |
    re'4 fad'8-.\p sol'-. la'-. fad'-. fad'-. la'-. |
    sol'4 sol'2 sol'4 |
    sol'8-!\fp sol' sol' sol' sol' sol' sol' sol' |
    sol'-. r sol'-. r sol'-. r sol'-. r |
    sol'-!\fp sol' sol' sol' sol' sol' sol' sol' |
    sol' r sol' r sol' r sol' r |
    mi'-!\fp mi' mi' mi' mi' mi' mi' mi' |
    fad' r fad' r fad' r fad' r |
    sol'-. re'-. si-. sol-. r re'-. si-. sol-. |
    mi'8-. mi'( sol'4) r8 mi'( sol'4) |
    r8 re'-. si-. sol-. r re'-. si-. sol-. |
    mi'8-. mi'( sol'4) r8 mi'( sol'4) |
    sol'8-!\fp sol' sol' sol' la'-!\fp la' la' la' |
    si'-!\fp si' si' si' do'' do'' do'' do'' |
    si'(\f re'' do'' si') do''(\p si' la' sol') |
    fad'4( sol') \appoggiatura si'8 la'4( sol'8 fad') |
    sol'4 r si2\fp |
    do'8-. re'-. mi'-. fa'-. mi'4 fad' |
    sol' r si2\fp |
    do'8-. re'-. mi'-. fa'-. mi'4 fad' |
    sol' r si2\fp |
    do'8-. re'-. mi'-. fa'-. mi'4 fad' |
    sol' r sol' r |
    fad'8-. r sol'-. r fad'-. r dod''-. r |
    re''4 r\fermata mi'8(\f re' do' si) |
    la(\p fad') sol'4-. mi'8(\f re' do' si) |
    la(\p fad') sol'4-. mi'8(\f re' do' si) |
    la(\p fad') sol'4-. mi'8(\f re' do' si) |
    la4:16 si:16 do':16 do'':16 |
    si'4 si'8-.\p re''-. sol'-. si'-. mi'16(\f sol') fad'( la') |
    sol'8[-. sol']-. si'8-.\p re''-. sol'-. si'-. mi'16(\f sol') fad'( la') |
    sol'8[-. sol']-. si'8-.\p re''-. sol'-. si'-. mi'16(\f sol') fad'( la') |
    sol'8[-.\p sol']-. mi'16(\f sol') fad'( la') sol'8[-.\p sol']-. mi'16(\f sol') fad'( la') |
    sol'8([\p si']) re''(\f red'' mi''4) re''8(\p do'') |
    \appoggiatura do''16 si'4\fp la'8( sol') \appoggiatura si'16 la'4\fp sol'8( fad') |
    sol'8([ si']) re''(\f red'' mi''4) re''8(\p do'') |
    \appoggiatura do''16 si'4\fp la'8( sol') \appoggiatura si'16 la'4\fp sol'8( fad') |
    <sol' sol>4 r4\fermata sol'16(\p re' fad' re') |
    sol'( re' si re') sol'( re' fad' sol') la'( re' sol' re') |
    fad'( re' do' re') si( re' sol' fad') sol'( re' la' re') |
    si'( sol' re' sol') si'( sol' la' si') do''( sol' si' sol') |
    fad'( sol' la' fad') sol'( re' si sol) mi'( sol' re' fad') |
    do'( re' mi' do') si( do' re' si) do'( sol' si sol') |
    la( re' mi' fad') sol'4-. r\fermata |
    si'8-.\p r re''-. r fad'-. r do''16-. si'-. do''-. la'-. |
    si'8-. r re''-. r fad'-. r do''16-. si'-. do''-. la'-. |
    si'8 r re'' r fad' r do''16 si' do'' la' |
    si'( do'' re'' si') la'( si' do'' la') si'( do'' re'' si') la'( si' do'' la') |
    si'4-. re''-.
  }
>> sol''16(\cresc fad'') mi''-. re''-. do''( si') la'-. sol'-. |
mi''-. fad''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. la'(\f si' do'' la') fad'( sol' la' fad') |
<<
  \tag #'violino1 {
    sol''16-.\p la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. do''-. la'-. la''-. sol''-. la''-. fad''-. |
    sol''16-. la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. do''-. la'-. la''-. sol''-. la''-. fad''-. |
    sol'' la'' si'' la'' sol'' fad'' mi'' re'' do'' si' do'' la' la'' sol'' la'' fad'' |
    sol''( la'' si'' sol'') fad''( sol'' la'' fad'') sol''( la'' si'' sol'') fad''( sol'' la'' fad'') |
    sol''-. la''-. si''-. la''-. sol''-. fad''-. mi''-. re''-.
  }
  \tag #'violino2 {
    sol'8-.\p r re''-. r fad'-. r do''16-. si'-. do''-. la'-. |
    si'8-. r re''-. r fad'-. r do''16-. si'-. do''-. la'-. |
    si'8-. r re''-. r fad'-. r do''16-. si'-. do''-. la'-. |
    si'16( do'' re'' si') la'( si' do'' la') si'( do'' re'' si') la'( si' do'' la') |
    si'4-. re''-.
  }
>> sol''16(\f fad'') mi''-. re''-. do''( si') la'-. sol'-. |
mi''-.\p fad''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. la'( si' do'' la') fad'( sol' la' fad') |
si'-. re''-. do''-. si'-. do''-. re''-. mi''-. fad''-. sol''(\f fad'') mi''-. re''-. do''( si') la'-. sol'-. |
mi''-.\p fad''-. sol''-. fad''-. mi''-. re''-. do''-. si'-. la'( si' do'' la') fad'( sol' la' fad') |
<<
  \tag #'violino1 {
    sol'4:16 mi'':16 re'':16\cresc fad'':16 |
    sol'':16 do''':16 si'':16 la'':16 |
    sol''\f <re'' re' sol>4 <sol re' si'> <re'' re' sol> |
    <sol re' si'> <re' re'' si''> <re' re'' re'''> <re' re'' si''> |
    <re' si' sol''>4
  }
  \tag #'violino2 {
    sol'2:16 si'4:16\cresc la':16 |
    sol':16 mi'':16 sol'':16 fad'':16 |
    sol''16\f si' re'' si' sol' re' si re' \ru#6 { sol re' si re' } |
    sol4
  }
>> r8 re'-. sol'-. la'-. si'-. do''-. |
re'' mi'' re'' do'' si' la' sol' fad' |
sol'4 r <<
  \tag #'violino1 <re' re''>4
  \tag #'violino2 <sol re' si'>4
>> r4 |
<sol re' si' sol''>2 r |

