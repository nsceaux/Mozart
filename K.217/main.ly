\version "2.19.80"
\include "common.ily"

\opusTitle "K.217 – Voi avete un cor fedele"

\header {
  title = \markup\center-column {
    Arie
    \italic Voi avete un cor fedele
  }
  subtitle = "für Sopran mit Begleitung des Orchesters"
  opus = "K.217"
  date = "1775"
  copyrightYear = "2019"
}
\paper {
  systems-per-page = #(if (symbol? (ly:get-option 'part))
                          #f
                          2)
}

\includeScore "K.217"
