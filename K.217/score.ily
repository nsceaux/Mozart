\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = "Corni in G"
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Dorina
      shortInstrumentName = \markup\character Do.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = "Bassi"
      shortInstrumentName = "B."
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s2.*4\break s2.*6\break s2.*6\pageBreak
        s2.*6\break s2.*6\break s1*5\pageBreak
        s1*6\break s1*6\break s1*6\pageBreak
        s1*7\break s1*6\break s1*7\pageBreak
        s1*7\break s1*2 s2.*4\break s2.*6\pageBreak
        s2.*6\break s2.*5\break s1*5\pageBreak
        s1*6\break s1*6\break s1*7\pageBreak
        s1*6\break s1*6\break s1*6\pageBreak
        s1*6\break s2.*6\break s2. s1*4\pageBreak
        s1*5\break s1*5\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
