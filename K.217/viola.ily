\clef "alto" sol8( la) |
si4. la16( si) do'8( si) |
si( la) sol4-. si8( do') |
re'4. do'16( re') mi'8( re') |
re'( do') si4-. r |
r4 r mi'8(\p re') |
re'( do') si4-. do'8(\f si) |
mi'8.( fad'16) sol'8-. r do'8(\p si) |
re'16( si re' si) la4-. sol8( la) |
si4. la16( si) do'8( si) |
si( la) sol4-. si8( do') |
re'4. do'16 re' mi'8( re') |
re'( do') si4-. r |
r r mi'8( re') |
re'( do') si4-. si8 si |
do' do' do' do' re'[ re'] |
re'4\f r8 <<
  { re'8 re'[ re'] | re'2 } \\
  { si8 do'[ do'] | do'4( si) }
>> r4 |
<>\p \ru#2 { << re'4 \\ si >> r r | }
sol16 sol sol sol sol2:16 |
<< fad'2:16  \\ la:16 >> fad4:16 |
<<
  { re'2.:16 |
    mi':16 |
    sol':16 |
    fad'4:16 re'2:16 |
    mi'2.:16 |
    mi'4:16 sold'2:16 |
    la'4 } \\
  { sol2.:16 |
    la:16 |
    la:16 |
    la4:16 la2:16 |
    re'2.:16 |
    dod'4:16 si2:16 |
    la4 }
>> r4\fermata r |
re'4\f re' re' re' |
re'8\p re' re' re' re' re' re' re' |
dod'4\f dod' dod' dod' |
dod'8\p dod' dod' dod' dod' dod' dod' dod' |
re'4 r r2 |
R1 |
la8\p la la la la' la' la'\cresc la' |
la' la' la' la' si' si' si' si' |
la'4\f r la re''16( dod'' si' la') |
sol'4 r la\p la |
la r la\f re''16( dod'' si' la') |
sol'4 r la\p la |
la2 sol4( fad) |
sol8-!\fp sol sol sol sol sol sol sol |
sol r sol' r sol' r sol r |
fad-!\fp fad fad fad fad fad fad fad |
fad r fad' r fad' r fad' r |
mi'-!\fp mi' mi' mi' mi' mi' mi' mi' |
mi' r mi' r mi' r dod' r |
re'4 r8 fad' re'4 r8 fad' |
dod'4 r8 la dod'4 r8 la |
re'4 r8 fad' re'4 r8 fad' |
dod'4 r8 la dod'4 r8 la |
re'2.( fad'4) |
sol' r sol'2 |
la' la |
la4 r si'8(\f la' sol' fad') |
mi'(\p dod') re'4-. si'8(\f la' sol' fad') |
mi'(\p dod') re'4-. si'8(\f la' sol' fad') |
mi'(\p dod') re'4-. la'\f r |
la' r la r |
la\p r r sol'8-.\f mi'-. |
fad'4-. r r sol'8-. mi'-. |
fad'4-. r r sol'8-. mi'-. |
fad'4-.\p sol'8-.\f mi'-. fad'4-.\p sol'8-.\f mi'-. |
fad'8\p fad' fad'\f fad' fad' fad' si'\p la' |
sol' sol' sol' fad' mi' mi' la' la' |
re' re' fad'\f fad' fad' fad' si'\p la' |
sol' sol' sol' fad' mi' mi' la' la' |
si' si' sol' sol' la' la' la' la' |
si'\cresc si' sol' sol' la' la' la' la' |
<>\f \ru#8 { re'16-. la'-. fad'-. la'-. } |
re'4 re8. re16 re4 re |
re r8 mi-.\p fad-. sol-. la-. si-. |
do'!-. r la-. r fad-. r re-. r |
sol4 r8 re'-. si-. la-. sol-. fad-. |
mi-. r sol-. r dod'-. r mi'-. r |
la4 r8 re-. fad-. sol-. la-. si-. |
do'!-. r la-. r fad-. r re-. r |
sol4 r8 re'-. si-. la-. sol-. fad-. |
mi-. r sol-. r dod'-. r mi'-. r |
re'8-. r r4\fermata sol8( la) |
si4. la16( si) do'!8( si) |
si( la) sol4-. si8( do') |
re'4. do'16( re') mi'8( re') |
re'( do') si4 r |
r r mi'8( re') |
re'( do') si4 do'8(\f si) |
mi'8.( fad'16) sol'8-. r do'8(\p si) |
re'16( si re' si) la4 re'8-. re'-. |
si4. re'8( sol' re') |
<< re'4 \\ si >> r r |
<< re'4 \\ si >> r r |
sol16 sol sol sol sol2:16 |
sol2.:16 |
sol:16 |
<<
  { re'2.:16 | re':16 | } \\
  { si:16 | si:16 | }
>>
do':16 |
<< mi':16 \\ dod':16 >> |
re'4:16 << mi'2:16 \\ dod':16 >> |
re'4 r\fermata r |
sol'4-.\f sol'-. sol'-. sol'-. |
sol8\p sol sol sol sol sol sol sol |
fad4\f fad'-. fad'-. fad'-. |
fad8\p fad fad fad fad fad fad fad |
fad(\fp sol) sol-. sol-. do'(\fp si) si-. si-. |
mi'(\fp re') re'-. re'-. do'(\fp si) si-. si-. |
fad fad fad fad la'\cresc la' la' la' |
re' re' re' re' mi' mi' mi' mi' |
re'4\f r re' sol'16( fad' mi' re') |
do'4 r re'-.\p re'-. |
re'-. r re'\f sol'16( fad' mi' re') |
do'4 r re'\p re' |
sol( la) si2 |
do'8-!\fp do' do' do' do' do' do' do' |
do'-. r do'-. r do'-. r do'-. r |
si-!\fp si si si si si si si |
si r si r si r si r |
la-!\fp la la la la la la la |
la r la r la r re' r |
re'4 r8 si sol4 r8 si |
do'4 r8 mi' do'4 r8 mi' |
sol4 r8 si sol4 r8 si |
do'4 r8 mi' do'4 r8 mi' |
sol-!\fp sol sol sol sol-!\fp sol sol sol |
sol-!\fp sol sol sol sol sol la la |
sol4\f r r2 |
re'2.(\p do'4) |
si r r2 |
sol1\fp~ |
sol4 r r2 |
sol1\fp~ |
sol4 r r2 |
R1*3 |
re'4 r\fermata mi'8(\f re' do' si) |
la\p( re') re'4-. mi'8(\f re' do' si) |
la(\p re') re'4-. mi'8(\f re' do' si) |
la(\p re') re'4-. mi'8(\f re' do' si) |
la4:16 si:16 do':16 la':16 |
sol' r r do'8 la |
si4 r r do'8-.\f la-. |
si4-. r r do'8-. la-. |
si4-.\p do'8-.\f la8-. si4\p do'8-.\f la-. |
si8\p si si\f si do' do' do'\p do' |
re'\fp re' re' re' re'\fp re' re' re' |
si si si\f si do' do' do'\p do' |
re'\fp re' re' re' re'\fp re' re' re' |
<re' sol>4 r\fermata sol8(\p la) |
si4. la16( si) do'8( si) |
si( la) sol4-. si8( do') |
re'4. do'16( re') mi'8( re') |
re'( do') si4-. r |
r r mi'8( re') |
re'( do') si4-. r\fermata |
<>\p sol'8-. r si'-. r la'-. r re'-. r |
sol'8-. r si'-. r la'-. r re'-. r |
sol' r si' r la' r re' r |
sol' r re'' r sol' r re'' r |
sol'8 sol' sol' sol' si\cresc si si si |
do' do' do' do' re'\f re' re' re' |
sol-.\p r si'-. r la'-. r re'-. r |
sol'-. r si'-. r la'-. r re'-. r |
sol' r si' r la' r re' r |
sol' r re' r sol' r re' r |
sol' sol' sol' sol' si\f si si si |
do'\p do' do' do' re' re' re' re' |
sol' sol' sol' sol' si\f si si si |
do'\p do' do' do' re' re' re' re' |
sol' sol' do' do' re'\cresc re' re' re' |
mi' mi' do' do' re' re' re' re' |
<>\f \ru#8 { sol16 re' si re' } |
sol4 r8 re'-. sol'-. la'-. si'-. do''-. |
re'' mi'' re'' do'' si' la' sol' fad' |
sol'4 r sol' r |
sol'2 r |
