\clef "treble" \transposition sol
r4 |
\tag#'corni <>^"a 2." do'4 do'' r |
sol' do' r |
do' do'' r |
sol' do' r |
R2. |
r4 r <>\f <<
  \tag #'(corno1 corni) { do''8 do'' | do''8. re''16 mi''8 }
  \tag #'(corno2 corni) { do'8 do' | do'8. sol'16 do''8 }
>> r8 r4 |
R2.*8 |
<<
  \tag #'(corno1 corni) { do''2 re''4 | re''4( mi'') }
  \tag #'(corno2 corni) { mi'2 sol'4 | sol'4( do'') }
>> r4 |
r r <>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'8 sol' | }
  { sol' sol' | }
>>
<<
  \tag #'(corno1 corni) {
    do''4. re''8( mi'' re'') |
    do''4
  }
  \tag #'(corno2 corni) {
    mi'4. sol'8( do'' sol') |
    mi'4
  }
>> r4 r |
R2. |
r4 <<
  \tag #'(corno1 corni) {
    do''4-.( do''-.) |
    do''2. |
    re'' |
    re''8 s re'' s re'' s |
  }
  \tag #'(corno2 corni) {
    mi'4-.( mi'-.) |
    do'2. |
    do'' |
    sol'8 s sol' s sol' s |
  }
  { s2 | s2.*2 | s8 r s r s r | }
>>
\tag#'corni <>^"a 2." sol'8 r sol' r sol' r |
re'' r sol' r sol' r |
re''4 r\fermata r |
<>\f <<
  \tag #'(corno1 corni) { re''1 }
  \tag #'(corno2 corni) { sol' }
>>
R1 |
<<
  \tag #'(corno1 corni) { re''1 }
  \tag #'(corno2 corni) { re'' }
>>
R1*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1~ | re''4 }
  { re''1~ | re''4 }
  { s1\p\cresc | s4\f }
>> r4 r <<
  \tag #'(corno1 corni) { re''8-. re''-. | }
  \tag #'(corno2 corni) { sol'8-. sol'-. | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 } { re'' }
>> r4 r2 |
r2 r4 <<
  \tag #'(corno1 corni) { re''8-. re''-. | }
  \tag #'(corno2 corni) { sol'8-. sol'-. | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 } { re'' }
>> r4 r2 |
R1*14 |
r8 \tag#'corni <>^"a 2." re''-.\f re''-. re''-. re''2\fp~ |
re'' re''\fp~ |
re'' re''\fp~ |
re'' r |
R1*5 |
r4 <>\fp <<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>> r4 |
R1 |
r4 <>\fp <<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>> r4 |
R1*2 |
r4 <>\cresc <<
  \tag #'(corno1 corni) { mi''4 }
  \tag #'(corno2 corni) { do'' }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' | }
  { re'' re'' | }
>>
<>\f <<
  \tag #'(corno1 corni) {
    re''4 re''8. re''16 re''4 re'' |
    re'' re''8. re''16 re''4 re'' |
    re''
  }
  \tag #'(corno2 corni) {
    sol'4 sol'8. sol'16 sol'4 sol' |
    sol' sol'8. sol'16 sol'4 sol' |
    sol'
  }
>> \tag#'corni <>^"a 2." sol'8. sol'16 sol'4 sol' |
sol' r r2 |
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { re''1( | mi''8) }
  \tag #'(corno2 corni) { sol'1( | do''8) }
>> r8 r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1( | sol'8) }
  { do'1( | sol'8) }
>> r8 r4\fermata r |
R2.*2 |
\tag#'corni <>^"a 2." do'4 do'' r |
sol' do' r |
R2. |
r4 r <>\f <<
  \tag #'(corno1 corni) { do''8 do'' | do''8. re''16 mi''8 }
  \tag #'(corno2 corni) { do'8 do' | do'8. sol'16 do''8 }
>> r8 r4 |
R2.*2 | \allowPageTurn
r4 r <>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'8 sol' | }
  { sol' sol' | }
>>
<<
  \tag #'(corno1 corni) {
    do''4. re''8( mi'' re'') |
    do''4
  }
  \tag #'(corno2 corni) {
    mi'4. sol'8( do'' sol') |
    mi'4
  }
>> r4 r |
R2. |
r4 <<
  \tag #'(corno1 corni) {
    do''4 do'' |
    do''2.~ |
    do'' |
    do''8 s do'' s do'' s |
    do'' s do'' s do'' s |
    re'' s do'' s do'' s |
  }
  \tag #'(corno2 corni) {
    do'4 do' |
    do'2.~ |
    do' |
    do'8 s do' s do' s |
    do' s do' s do' s |
    sol' s do' s do' s |
  }
  { s2 | s2.*2 |
    s8 r s r s r |
    s8 r s r s r |
    s8 r s r s r | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol'4 }
  { sol' }
>> r\fermata r |
<>\f <<
  \tag #'(corno1 corni) { do''1 | }
  \tag #'(corno2 corni) { do' | }
>>
R1 |
<<
  \tag #'(corno1 corni) { re''1 }
  \tag #'(corno2 corni) { sol' }
>>
R1*3 |
\tag#'corni <>^"a 2." sol'1\p\cresc~ sol'~ |
sol'4\f r r <<
  \tag #'(corno1 corni) { do''8 do'' | re''4 }
  \tag #'(corno2 corni) { mi'8 do' |
    sol'4 }
>> r4 r2 |
r r4 <<
  \tag #'(corno1 corni) { do''8 do'' | re''4 }
  \tag #'(corno2 corni) { mi'8 do' | sol'4 }
>> r4 r2 |
R1*15 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do''4
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do'4
  }
>> r4 r2 |
R1 |
r2\fermata r |
r8 <<
  \tag #'(corno1 corni) {
    sol'8 sol' sol' sol'2~ |
    sol' sol'~ |
    sol' sol'~ |
    sol' re'' |
    mi''4
  }
  \tag #'(corno2 corni) {
    sol8 sol sol sol2~ |
    sol sol~ |
    sol sol~ |
    sol sol' |
    do''4
  }
  { s4.\p s2\fp |
    s s\fp |
    s s\fp |
    s s\f | }
>> r4 r2 |
R1*3 |
r4 <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r4 |
R1 |
r4 <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r4 |
r4 r8 <<
  \tag #'(corno1 corni) { mi''8 re''4 mi''8 re'' | do''4 }
  \tag #'(corno2 corni) { do''8 sol'4 do''8 sol' | mi'4 }
>> r4\fermata r |
R2.*2 |
\tag#'corni <>^"a 2." do'4\p do'' r |
sol' do' r |
R2. |
R2.^\fermataMarkup |
R1*4 |
r2 <>\p\cresc <<
  \tag #'(corno1 corni) { do''2~ | do''4 }
  \tag #'(corno2 corni) { do'2~ | do'4 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''4 } { r }
>> <<
  \tag #'(corno1 corni) { re''2 | do''4 }
  \tag #'(corno2 corni) { sol'2 | mi'4 }
  { s2\f | s4\p }
>> r4 r2 |
r4 <<
  \tag #'(corno1 corni) { do''4( re'') }
  \tag #'(corno2 corni) { mi'4( sol') }
>> r4 |
r4 <<
  \tag #'(corno1 corni) { do''4( re'') }
  \tag #'(corno2 corni) { mi'4( sol') }
>> r4 |
<<
  \tag #'(corno1 corni) { do''8 s re'' s mi'' s re'' s | do''4 }
  \tag #'(corno2 corni) { mi'8 s sol' s do'' s sol' s | mi'4 }
  { s8 r s r s r s r | }
>> r4 r2 |
R1*3 |
r2 r4 <>\p\cresc <<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4( fa'') }
  { do''( re'') }
>> <<
  \tag #'(corno1 corni) {
    mi''4( re'') |
    do''4 sol'8. sol'16 sol'4 sol' |
    sol' sol'8. sol'16 sol'4 sol' |
    sol'
  }
  \tag #'(corno2 corni) {
    do''4( sol') |
    mi'4 do'8. do'16 do'4 do' |
    do' do'8. do'16 do'4 do' |
    do'
  }
  { s2 | s1\f | }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) { do''4 s do'' s | do''2 }
  \tag #'(corno2 corni) { mi'4 s mi' s | mi'2 }
  { s4 r s r | }
>> r2 |
