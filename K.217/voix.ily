\clef "soprano/treble" r4 |
R2.*7 |
r4 r sol'8 la' |
si'4. la'16([ si']) do''8 si' |
si'([ la']) sol'4 r |
r4 r8 do''16([ re'']) mi''8 re'' |
re''([ do'']) si'4 do''8 si' |
si'([ la']) sol'8 sol' mi'' re'' |
re''([ do'']) si'4 re''8 red'' |
mi''4~ mi''16([ sol'']) fad''([ mi'']) re''!([ do'']) si'([ la']) |
sol'8 sol' r4 r |
r r re'8 re' |
sol'4. la'8 si' la' |
sol' sol' r4 re''8 do'' |
si' si' r4 si'8. sol'16 |
do''8 do'' r4 re''8. la'16 |
si'8 si' r4 r |
dod''8 dod'' r4 r8 sol'' |
sol''4. mi''8 dod'' sol' |
fad'4 r la'8. la'16 |
re''8 re'' r4 re''8. sold'16 |
la'4 re''4. sold'8 |
la'4 r\fermata r |
R1 |
re''2. fad''4 |
red''8([ mi'']) mi''4 r2 |
mi''2. sol''4 |
mid''8([ fad'']) fad''4 dod''8([ re'']) re''4 |
lad'8([ si']) si'4 fad'8([ sol']) sol'4 |
mi' r mi''4. sol''8 |
fad''4 re'' si' sold' |
la' r r2 |
r4 dod''8 re'' mi'' re'' mi'' dod'' |
re'' la' r4 r2 |
r4 dod''8 re'' mi'' re'' mi'' dod'' |
re''4 do''! si' lad' |
si'4\melisma si'~ si'8[ dod''16 re''] mi''[ fad'' sol'' la''] |
si''8-.[ la''-.] sol''[\trill fad''] mi''[\trill re''] dod''[\trill si'] |
la'4 la'~ la'8[ si'16 dod''] re''[ mi'' fad'' sol''] |
la''8-.[ sol''-.] fad''[\trill mi''] re''[\trill dod''] si'[\trill la'] |
sol'4 sol'~ sol'8[ la'16 si'] dod''[ re'' mi'' fad''] |
sol''8 mi''4 dod'' la' sol''8 |
fad''16([ mi'' fad'' mi'']) re''8-. r fad''16([ mi'' fad'' mi'']) re''8-. r |
mi''16([ fad'' mi'' fad'']) sol''8[-. sol'']-. mi''16([ fad'' mi'' fad'']) sol''8[-. sol'']-. |
fad''16([ mi'' fad'' mi'']) re''8-. r fad''16([ mi'' fad'' mi'']) re''8-. r |
mi''16([ fad'' mi'' fad'']) sol''8[-. sol'']-. mi''16([ fad'' mi'' fad'']) sol''8[-. sol'']-. |
fad''16([ sol'' fad'' sol'']) la''8-.[ la'']( fad'')[ fad''( re'') re'']( |
si')[ dod''( re'' red'')]\melismaEnd mi''([ fad'']) sol''([ mi'']) |
dod''4 re''! \appoggiatura fad''8 mi''4 re''8([ dod'']) |
re''4 r r2 |
r sol''8([ fad'']) mi''([ re'']) |
dod''([ mi'']) re''([ la']) sol''([ fad'']) mi''([ re'']) |
dod''([ mi'']) re''([ la']) sol''([ fad'']) mi''([ fad'']) |
mi''([ re'']) dod''([ re'']) dod''([ si']) la'([ sol']) |
fad'4 r r si'8 dod'' |
re'' re'' r4 r si'8 dod'' |
re'' re'' r4 r si'8 dod'' |
re'' re'' si' dod'' re'' re'' si' dod'' |
re''([ fad'']) la''2 sol''8([ fad'']) |
mi''4. fad''8 sol''([ mi'']) re''([ dod'']) |
re''([ fad'']) la''2 sol''8([ fad'']) |
mi''4. fad''8 sol''([ mi'']) re''([ dod'']) |
re''4 r8 sol'' fad''([ mi'']) re''([ dod'']) |
re''4 r8 sol'' fad''([ mi'']) re''([ dod'']) |
re''4 r r2 |
R1*3 |
r2 r4 re''8 re'' |
si'4 si' r2 r r4 sol'8 fad' |
la'4 la' r2 |
r r4 re''8 re'' |
si'4 si' r2 |
r r4 sol'8 fad' |
la' la' r4\fermata sol'8 la' |
si'4. la'16([ si']) do''!8 si' |
si'([ la']) sol'4 r |
r r8 do''16([ re'']) mi''8 re'' |
re''([ do'']) si'4 do''8 si' |
si'([ la']) sol' sol' mi'' re'' |
re''([ do'']) si'4 r |
r r8 sol' do'' si' |
re''8.([ si'16]) la'4 r |
r r re'8 re' |
sol'4. la'8 si' la' |
sol' sol' r4 re''8 do'' |
si' si' r4 re''8. re''16 |
si'8 si' r4 re''8. sol'16 |
do''8 do'' r4 r |
si'8 si' r4 r8 fa'' |
fa''4. re''8 si' fa' |
mi'4 r mi''8. mi''16 |
dod''8 dod'' r4 sol'8. fad'16 |
la'4 sol''4. dod''8 |
re''4 r\fermata r |
R1 |
sol''2. si'4 |
si'8([ do'']) do''4-. r2 |
la''2. do''4 |
do''8([ si']) si'4-. mi''8([ re'']) re''4-. |
sol''8([ fad'']) fad''4 mi''8([ re'']) re''4 |
do'' r do''4. do''8 |
si'4 re'' sol'' dod'' |
re'' r r2 |
r4 la'8 si' do''! la' re'' do'' |
si' si' r4 r2 |
r4 la'8 si' do'' la' re'' do'' |
si'4 do'' re'' red'' |
mi''\melisma sol'4~ sol'8[ la'16 si'] do''[ re'' mi'' fad''] |
sol''([ fad'' mi'' fad'']) sol''8[-. sol'']-. sol''([ mi'') sol''( mi'')] |
re''4 sol'~ sol'8[ la'16 si'] do''[ re'' mi'' fad''] |
sol''8-.[ la''( si'') fad'']( sol''8)[ re''( mi'') si'] |
do''4 la'~ la'8[ si'16 do''] re''[ mi'' fad'' sol''] |
la''8[-. sol''-.] fad''\trill[ mi''] re''[\trill do''] si'[\trill la'] |
si'16([ do'' re'' mi'']) re''8-. r si'16([ do'' re'' mi'']) re''8-. r |
mi''16([ fad'' sol'' la'']) sol''8[-. sol'']-. mi''16([ fad'' sol'' la'']) sol''8[-. sol'']-. |
si'16([ do'' re'' mi'']) re''8-. r si'16([ do'' re'' mi'']) re''8-. r |
mi''16([ fad'' sol'' la'']) sol''8[-. sol'']-. mi''16([ fad'' sol'' la'']) sol''8[-. sol'']-. |
si'16([ do'' si' do'']) re''8[-. re'']-. do''16([ re'' do'' re'']) mi''8[-. mi'']-. |
re''16([ mi'' re'' mi'']) fa''8[-. fa'']-. mi''16([ fad'' sol'' mi'']) fad''([ sol'' la'' fad'']) |
sol''8([ fad'' mi'' red''])\melismaEnd mi''([ re'']) do''([ si']) |
la'4 si' \appoggiatura re''8 do''4 si'8([ la']) |
sol'4 r r2 |
r4 do''8 si' do'' mi'' re'' do'' |
si' si' r4 r2 |
r4 do''8 si' do'' mi'' re'' la' |
si' si' r4 r2 |
r4 do''8 si' do'' mi'' la' re'' |
si'4 re''8 re'' dod'' sol' sol' fad' |
la'4 sol'8 fad' la'4 sol''8 dod'' |
re''2\fermata \grace { red''8([ mi'' fad'']) } sol''8([ fad'']) mi''([ re'']) |
do''([ la']) si'([ re'']) sol''([ fad'']) mi''([ re'']) |
do''([ la']) si'([ re'']) sol''([ fad'']) mi''([ re'']) |
do''([ la']) si'([ re'']) sol''([ fad'']) mi''([ re'']) |
do''4( re'') mi'' fad'' |
sol'' r r mi''8 do'' |
re'' re'' r4 r mi''8 do'' |
re''8 re'' r4 r mi''8 do'' |
re'' re'' mi'' do'' re'' re'' mi'' do'' |
re''([ sol'']) sol''2 fad''8([ mi'']) |
\appoggiatura mi''16 re''4 do''8([ si']) \appoggiatura re''16 do''4 si'8([ la']) |
re''([ sol'']) sol''2 fad''8([ mi'']) |
\appoggiatura mi''16 re''4 do''8([ si']) \appoggiatura re''16 do''4 si'8([ la']) |
sol'4 r\fermata sol'8 la' |
si'4. la'16([ si']) do''8 si' |
si'([ la']) sol'4 r |
r r8 do''16([ re'']) mi''8 re'' |
re''([ do'']) si'4 do''8 si' |
si'([ la']) sol' sol' mi'' re'' |
re''([ do'']) si'4 r\fermata |
r2 r4 do''8. la'16 |
si'4 re'' r do''8. la'16 |
si'4 re'' r do''8. la'16 |
si'8([ re'']) do''([ la']) si'([ re'']) do''([ la']) |
si'4 r sol''4. re''8 |
mi''4 do'' la' re'' |
sol' r r do''8. la'16 |
si'4 re'' r do''8. la'16 |
si'4 re'' r do''8. la'16 |
si'8([ re'']) do''([ la']) si'([ re'']) do''([ la']) |
si'4 r sol''4. re''8 |
mi''4 do'' la' re'' |
si' r sol''4. re''8 |
mi''4 do'' la' re'' |
sol' mi'' re'' fad'' |
sol'' mi'' re'' fad' |
sol' r r2 |
R1*5 |
