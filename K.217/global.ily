\tag #'all \key sol \major
\tempo "Andantino grazioso" \midiTempo#92
\time 3/4 \partial 4 s4 s2.*28 \bar "||"
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*52 \bar "||"
\tempo "Tempo primo" \midiTempo#92
\time 3/4 s2.*21 \bar "||"
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*48 \bar "||"
\tempo "Tempo primo" \midiTempo#92
\time 3/4 s2.*7 \bar "||"
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*22 \bar "|."
