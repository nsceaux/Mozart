\clef "bass" r4 |
sol, sol r |
re sol, r |
sol, sol r |
re sol, r |
r r do'8(\p si) |
la4( sol) mi'8(\f re') |
do'4( si8) r fad(\p sol) |
re'4 re r |
sol, sol r |
re sol, r |
sol, sol r |
re sol, r |
r r do'8( si) |
la4( sol) si8( si,) |
do do do do re[ re] |
sol,4\f sol re |
re4( sol,) r |
sol\p r r |
sol r r |
sol,8-. r r4 sol8-. r |
la-. r r4 fad8-. r |
sol-. r sol,-. r sol-. r |
mi-. r r4 mi8-. r |
dod-. r r4 dod8 r |
re r r4 re8 r |
si, r r4 si,8 r |
la, r si, r si, r |
la,4 r\fermata r |
re4\f re re re |
R1 |
dod4\p dod dod dod |
R1*3 |
la8\p la la la la la la\cresc la |
la la la la la la la la |
la,4\f r fad8 fad fad fad |
mi4 r la\p la, |
re r fad8\f fad fad fad |
mi4 r la\p la, |
re la( sol fad) |
sol8-!\fp sol sol sol sol sol sol sol |
sol r sol r sol r sol r |
fad-!\fp fad fad fad fad fad fad fad |
fad r fad r fad r fad r |
mi-!\fp mi mi mi mi mi mi mi |
la, r la, r la, r la, r |
re4 r8 fad re4 r8 fad |
dod4 r8 la, dod4 r8 la, |
re4 r8 fad re4 r8 fad |
dod4 r8 la, dod4 r8 la, |
re2.( fad4) |
sol r sol2 |
la la, |
re4 r r2 |
\ru#3 { la4-. re-. r2 | }
R1 |
re4 r r sol8-.\f mi-. |
fad4-. r r sol8-. mi-. |
fad4-. r r sol8-. mi-. |
fad4-.\p sol8-.\f mi-. fad4-.\p sol8-.\f mi-. |
fad8\p fad fad\f fad fad fad si\p la |
sol sol sol fad mi mi la la |
re re fad\f fad fad fad si\p la |
sol sol sol fad mi mi la la |
si si sol sol la la la la |
si\cresc si sol sol la la la la |
re\f re re re re re re re |
re re re re re re re re |
re4 re8. re16 re4 re |
re r8 mi-.\p fad-. sol-. la-. si-. |
do'!-. r la-. r fad-. r re-. r |
sol4 r8 re'-. si-. la-. sol-. fad-. |
mi-. r sol-. r dod'-. r mi-. r |
re4 r8 re-. fad-. sol-. la-. si-. |
do'!-. r la-. r fad-. r re-. r |
sol4 r8 re'-. si-. la-. sol-. fad-. |
mi-. r sol-. r dod'-. r mi-. r |
re-. r r4\fermata r |
sol,4 sol r |
re sol, r |
sol, sol r |
re sol, r |
r r do'8( si) |
la4( sol) mi'8(\f re') |
do'4( si8) si-.\p fad( sol) |
re'4 re r |
R2. |
sol4 r r |
sol r r |
sol,8-. r r4 sol8-. r |
fa-. r r4 fa8-. r |
mi-. r mi-. r mi-. r |
re-. r r4 re8-. r |
sol,-. r r4 sol8-. r |
do-. r r4 do8-. r |
mi r r4 mi8 r |
re r mi r mi r |
re4 r\fermata r |
sol4-.\f sol-. sol-. sol-. |
R1 |
fad4-. fad-. fad-. fad-. |
R1*3 | \allowPageTurn
re8\p re re re re\cresc re re re |
re re re re re re re re |
re4\f r si8 si si si |
la4 r re\p re |
sol r si8\f si si si |
la4 r re\p re |
sol( la si) si, |
do8-!\fp do do do do do do do |
do-. r do'-. r do'-. r do'-. r |
si-!\fp si si si si si si si |
si r si r si r si r |
la-!\fp la la la la la la la |
re r re r re r re r |
sol4 r8 si sol4 r8 si |
do'4 r8 mi' do'4 r8 mi' |
sol4 r8 si sol4 r8 si |
do'4 r8 mi' do'4 r8 mi' |
sol-!\fp sol sol sol sol-!\fp sol sol sol |
sol-!\fp sol sol sol sol sol sol sol |
sol4\f r r2 |
re'2\p re |
sol1~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol4 r mi r |
re8-. r mi-. r re-. r mi-. r |
re4 r\fermata r2 |
\ru#3 { re4-. sol,-. r2 | }
re8\f re re re re re re re |
sol,4 r r do'8-. la-. |
si4 r r do'8 la |
si4 r r do'8-. la-. |
si4-.\p do'8-.\f la-. si4-.\p do'8-.\f la-. |
si8\p si, si,\f si, do do do\p do |
re\fp re re re re\fp re re re |
si, si, si,\f si, do do do\p do |
re\fp re re re re\fp re re re |
sol4 r\fermata r |
sol,4-.\p sol-. r |
re-. sol,-. r |
sol,-. sol-. r |
re-. sol,-. r |
r r do'8( si) |
la4( sol) r\fermata |
sol8-.\p r si-. r la-. r re-. r |
sol-. r si-. r la-. r re-. r |
sol r si r la r re r |
sol r re' r sol r re' r |
sol sol sol sol si,\cresc si, si, si, |
do do do do re\f re re re |
sol,-.\p r si-. r la-. r re-. r |
sol-. r si-. r la-. r re-. r |
sol r si r la r re r |
sol r re' r sol r re' r |
sol sol sol sol si,\f si, si, si, |
do\p do do do re re re re |
sol sol sol sol si,\f si, si, si, |
do\p do do do re re re re |
sol sol do do re\cresc re re re |
mi mi do do re re re re |
sol\f sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol4 r8 re-. sol-. la-. si-. do'-. |
re' mi' re' do' si la sol fad |
sol4 r sol r |
sol2 r |
