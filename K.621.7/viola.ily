\clef "alto" la'8(\p sold' |
fad' dod' si fad') |
mi'4 mi'8(\f sold') |
la' r mi'8\p( sold') |
la' r la'( sold' |
fad' dod' si fad) |
mi mi'4( dod'8) |
la( si mi mi') |
mi'4 la'8( sold' |
fad' dod' si fad) |
mi mi'4( si'8) |
la'( fad') mi' mi' |
la r mi'4 |
re'8 re mi mi |
mi r la'( sold' |
fad' dod' si fad) |
mi mi'4( dod'8) |
la( si mi mi') |
mi'4 la'8( sold' |
fad' dod' si fad) |
mi mi'4( si'8) |
la'( fad') mi' mi' |
la r mi'4 |
re'8 re mi mi |
mi r dod'4( |
si la |
re') dod' |
re'8.( si16) la( dod' mi') mi'-. |
mi' mi' mi' mi' mi' mi' mi' mi' |
mi'2:16 |
mi'4:16 <<
  { mi'4~ | mi'2 | mi'8 } \\
  { mi'4( | re' dod') | mi'8 }
>> r8^\markup\center-align "divisi" <<
  { sold'16([ mi']) mi'([ si]) |
    si8.( sold16) la( sold la si) |
    la8( sold16) r sold'([ mi']) mi'([ si]) |
    si8.( sold16) la( sold la fad) | } \\
  { mi'16([ si]) si([ sold]) |
    sold8.(_\markup\dynamic mfp mi16) fad( mi fad sold) |
    fad8( mi16) r mi'([ si]) si([ sold]) |
    sold8.(_\markup\dynamic mfp mi16) fad( mi fad red) | }
>>
mi8 mi'16-.[ mi'-.] fad'8-. r |
r16 mi' mi' mi' fad'8 r |
r mi'16-. mi'-. fad'8-. r |
r4 fad\mf\fermata |
mi8\p r\fermata la'8( sold') |
fad'( dod' si re') |
mi'( re' dod' la) |
re'( si dod' la) |
mi'4 la'8( sold') |
fad'( mi') re'16 re( mi fad) |
mi8 mi'4( si'8) |
la'( fad') mi' mi' |
la r r la |
si4 r8 mi' |
mi' r la-. fad'-. |
mi' r r4 |
r fad'8-. mi'-. |
mi'-. r r4 |
r re'( |
mi'8) r r4 |
r mi'8(\mf sold' |
la') r r4 |
r la'\p~ |
la'4.( si8) |
si4 mi'8(\f sold') |
la' r16. do'32\p mi'8( sold') |
la'-. la-. la-. r |
