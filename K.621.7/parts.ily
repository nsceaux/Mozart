\piecePartSpecs
#`((flauti)
   (oboi #:score "score-oboi"
         #:music , #{
\addQuote "violino1" \keepWithTag #'violino1 { \includeNotes "violini" }
s4 s2*3  \mmRestDown
r4 \cueDuring "violino1" #CENTER { <>^\markup\tiny "Vln" s4 s2*30 }
\break \mmRestCenter
s2*5 s4 \mmRestDown
\cueDuring "violino1" #CENTER { <>^\markup\tiny "Vln" s4 s2*7 }
\mmRestCenter
#})
   (fagotti #:score-template "score-fagotti")
   (violino1)
   (violino2)
   (viola)
   (basso)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Duetto: TACET } #}))
