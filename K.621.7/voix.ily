\clef "soprano/treble"
<<
  \tag #'voix1 {
    r4 |
    R2*13 |
    r4 dod''8 mi'' |
    la'8. si'32([ dod'']) mi''16([ re'']) dod''([ si']) |
    la'8 sold' la' dod'' |
    fad'8. sold'16 la'([ si']) dod''([ re'']) |
    dod''8 si' dod'' mi'' |
    la'8. si'32([ dod'']) mi''16([ re'']) dod''([ si']) |
    la'8 sold' mi'' mi'' |
    \tuplet 3/2 { mi''16([ dod'' la']) } \tuplet 3/2 { fad''([ re'' si']) } la'8 si'16.([ dod''32]) |
    \appoggiatura dod''16 re''8 r re'' dod'' |
    fad''16([ re'']) dod''([ si']) si'([ la']) la'([ sold']) |
    la'4 r |
    R2*5 |
    r4 mi''8. mi''16 |
    mi''16([ sold'8]) mi''16 mi''([ la'8]) mi''16 |
    re''([ dod'']) si'8 sold''16([ mi'']) mi''([ si']) |
    si'8. sold'16 la'([ sold']) la'([ si']) |
    la'8 sold' sold''16([ mi'']) mi''([ si']) |
    si'8. sold'16 la'([ sold']) la'([ fad']) |
    mi'4 r8 red''16 red'' |
    mi''([ si']) si'8 r16 red'' red'' red'' |
    mi''([ si']) si'8 r red''16 red'' |
    mi''([ si']) mi''([ sold'']) la''8.\fermata red''16 |
    \appoggiatura fad''8 mi''8.([\fermata re''!16]) dod''8 mi'' |
    la'8. si'32([ dod'']) mi''16([ re'']) dod''([ si']) |
    la'8 sold' la' dod'' |
    fad'8. sold'16 la'([ si']) dod''([ re'']) |
    dod''8 si' dod'' mi'' |
    la''8. sold''16 sold''([ fad'']) mi''([ re'']) |
    dod''8 si' mi'' mi'' |
    \tuplet 3/2 { mi''16([ dod'' la'']) } \tuplet 3/2 { la''([ fad'' re'']) } dod''8 si'16.([ dod''32]) |
    \appoggiatura mi''16 re''8 r re''8. re''16 |
    re''([ si']) si'([ mi'']) mi''8. mi''16 |
    \grace fad''32 mi''16([ re''32 dod'']) dod''8 r4 |
    r dod''8 re'' |
    si'4 r |
    r re''8 mi'' |
    dod''4 r |
    r8 r16. fad''32 dod''8 si' |
    dod''4 r |
    r8 r16. fad''32 dod''8 si' |
    la'4 r |
    R2*4 |
  }
  \tag #'voix2 {
    r4 |
    R2*3 |
    r4 dod''8 mi'' |
    la'8. si'32([ dod'']) mi''16([ re'']) dod''([ si']) |
    la'8 sold' la' dod'' |
    fad'8. sold'16 la'([ si']) dod''([ re'']) |
    dod''8 si' dod'' mi'' |
    la'8. si'32([ dod'']) mi''16([ re'']) dod''([ si']) |
    la'8 sold' mi'' mi'' |
    \tuplet 3/2 { mi''16([ dod'' la']) } \tuplet 3/2 { fad''([ re'' si']) } la'8 si'16.([ dod''32]) |
    \appoggiatura dod''16 re''8 r re'' dod'' |
    fad''16([ re'']) dod''([ si']) si'([ la']) la'([ sold']) |
    la'8 r r4 |
    R2*11 |
    r4 la'8. mi'16 |
    si'8. mi'16 dod''8. mi''32([ re'']) |
    dod''8 si' r4 |
    R2*3 |
    r4 mi''16([ si']) si'([ sold']) |
    sold'8. mi'16 fad'([ mi']) fad'([ sold']) |
    fad'8 mi' mi''16([ si']) si'([ sold']) |
    sold'8. mi'16 fad'([ mi']) fad'([ red']) |
    mi'4 r8 la'16 la' |
    sold'([ si']) sold'8 r16 la' la' la' |
    sold'([ si']) sold'8 r la'16 la' |
    sold'([ si']) si'([ mi'']) red''8.\fermata la'16 |
    \appoggiatura la'8 sold'4\fermata la'8 sold' |
    fad' dod' si re' |
    mi' re' dod' la |
    re' si dod' la |
    mi' mi' la' si' |
    dod''8. mi''16 mi''([ re'']) dod''([ si']) |
    la'8 sold' la' re'' |
    \tuplet 3/2 { dod''16([ la' dod'']) } \tuplet 3/2 { fad''([ re'' si']) } la'8 sold' |
    la'8 r r la'16 la' |
    si'([ sold']) sold'8 r re''16 re'' |
    \grace re''32 dod''16([ si'32 la']) la'8 r4 |
    r la'8 re' |
    mi'4 r |
    r si'8 mi' |
    fad'4 r |
    r8 r16. la'32 la'8 sold' |
    la'4 r |
    r8 r16. re''32 la'8 sold' |
    la'4 r |
    R2*4 |
  }
>>

