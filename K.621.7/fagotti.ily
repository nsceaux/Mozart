\clef "bass" r4 |
R2 |
r4 <>\f <<
  \tag #'(fagotto1 fagotti) { mi'8( re') | dod' }
  \tag #'(fagotto2 fagotti) { dod'8( si) | la }
>> r8 r4 |
<<
  \tag #'(fagotto1 fagotti) {
    \tag #'fagotti <>^\markup\concat { 1 \super o }
    r4 dod'8(\p mi') |
    la8.( si32 dod') mi'16( re' dod' si) |
    la8( sold) la( dod') |
    fad8.( sold16 la si dod' re') |
    dod'8( si) dod'( mi') |
    la8.( si32 dod') mi'16( re' dod' si) |
    la8( sold) mi' mi' |
    \tuplet 3/2 { mi'16([ dod' la]) } \tuplet 3/2 { fad'([ re' si]) } la8 si16.( dod'32) |
    \appoggiatura dod'16 re'8 r re'( dod') |
    fad'16( re' dod' si) si([ la]) la([ sold]) |
    la8 r r4 |
  }
  \tag #'fagotto2 R2*11
>>
R2*13 |
\tag#'fagotti <>^"a 2." r4 dod'\p( |
si la |
re') r |
R2*4 |
<>_\markup\dynamic mfp <<
  \tag #'(fagotto1 fagotti) {
    si8.( sold16) la( sold la fad) |
  }
  \tag #'(fagotto2 fagotti) {
    sold8.( mi16) fad( mi fad red) |
  }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) << { mi8 } { mi } >>
r8 \tag#'fagotti <>^\markup\center-align "a 2." r fad'-. |
mi'-. r r fad'-. |
mi'-. r r fad'-. |
mi'-. r r4\fermata |
r\fermata r |
R2*3 |
<<
  \tag #'(fagotto1 fagotti) {
    \tag #'fagotti <>^\markup\concat { 1 \super o }
    r4 dod'8( mi') |
    la8.( si32 dod') mi'16( re' dod' si) |
    la8( sold) mi' mi' |
    mi'16.( dod'32) fad'16.( re'32) dod'8 si16.( dod'32) |
    \appoggiatura mi'16 re'4
  }
  \tag #'fagotto2 { R2*4 | r4 }
>>
r8 <<
  \tag #'(fagotto1 fagotti) { la8( | sold4) s8 re'( | dod') }
  \tag #'(fagotto2 fagotti) { fad8( | mi4) s8 si( | la) }
  { s8 | s4 r8 }
>> r8 r4 |
r \tag#'fagotti <>^\markup\center-align "a 2." r8 re'-. |
mi'-. r r4 |
r r8 mi'-. |
fad'-. r r4 |
r8 r16. <<
  \tag #'(fagotto1 fagotti) { fad'32-. dod'8-. si-. | dod' }
  \tag #'(fagotto2 fagotti) { la32-. la8-. sold-. | la }
>> r8 r4 |
r8 r16. <>\p <<
  \tag #'(fagotto1 fagotti) { fad'32-. dod'8-. si-. | }
  \tag #'(fagotto2 fagotti) { la32-. la8-. sold-. | }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { la8 } { la }
>> r8 <<
  \tag #'(fagotto1 fagotti) {
    \tag #'fagotti <>^\markup\concat { 1 \super o }
    dod'8( mi') |
    la8.( si32 dod' mi'16 re' dod' si) |
    la8( sold)
  }
  \tag #'fagotto2 { r4 R2 r4 }
>> <>\f <<
  \tag #'(fagotto1 fagotti) { mi'8( re') | dod' }
  \tag #'(fagotto2 fagotti) { dod'( si) | la }
>> r8 r4 |
r8 \tag#'fagotti <>^\markup\center-align "a 2." la8-.\p la,-. r |
