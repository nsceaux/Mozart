\clef "treble" r4 |
R2 |
r4 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8( sold'') | }
  { mi''8( re'') | }
>>
<<
  \tag #'(oboe1 oboi) { la''8 }
  \tag #'(oboe2 oboi) { dod'' }
>> r8 r4 |
R2*31 |
<>_\markup\dynamic mfp <<
  \tag #'(oboe1 oboi) {
    si''8.( sold''16) la''( sold'' la'' fad'') |
  }
  \tag #'(oboe2 oboi) {
    sold''8.( mi''16) fad''( mi'' fad'' red'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8 } { mi'' }
>> r8 r <<
  \tag #'(oboe1 oboi) {
    red''8-. |
    mi''-. s s red''-. |
    mi''-. s s la''-. |
    sold''-.
  }
  \tag #'(oboe2 oboi) {
    la'8-. |
    sold'-. s s la'-. |
    sold'-. s s red'' |
    mi''-.
  }
  { s8 | s r r s | s r r s | }
>> r8 r4\fermata |
r\fermata r |
R2*7 |
r4 r8 <<
  \tag #'(oboe1 oboi) { re''8~ | re''4 s8 si''( | la'') }
  \tag #'(oboe2 oboi) { \tag#'oboi \once\slurDown la'8( | si'4) s8 re''( | dod'') }
  { s8 | s4 r }
>> r8 r4 |
r <<
  \tag #'(oboe1 oboi) { dod''8-. re''-. | si'-. }
  \tag #'(oboe2 oboi) { la'-. si'-. | sold'-. }
>> r8 r4 |
r <<
  \tag #'(oboe1 oboi) { re''8-. mi''-. | dod'' }
  \tag #'(oboe2 oboi) { si'-. sold'-. | la' }
>> r8 r4 |
r8 r16. <<
  \tag #'(oboe1 oboi) { fad''32-. dod''8-. si'-. | dod''8 }
  \tag #'(oboe2 oboi) { la'32 la'8-. sold'-. | la' }
>> r8 r4 |
r8 r16. <>\p <<
  \tag #'(oboe1 oboi) { fad''32-. dod''8-. si'-. | }
  \tag #'(oboe2 oboi) { re''32-. la'8-. sold'-. | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 } { la' }
>> r8 r4 |
R2 |
r4 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8( sold'') | }
  { mi''8( re'') | }
>>
<<
  \tag #'(oboe1 oboi) { la''8 }
  \tag #'(oboe2 oboi) { dod'' }
>> r8 r4 |
r8 <>\p <<
  \tag #'(oboe1 oboi) { mi''-. dod''-. }
  \tag #'(oboe2 oboi) { dod''-. la'-. }
>> r8 |
