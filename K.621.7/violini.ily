\clef "treble"
<<
  \tag #'violino1 {
    dod''8(\p mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') mi''8.(\f fad''64 sold'' la'' si'' |
    dod'''8) r16. fad''32\p la'8([ \grace dod''16 si'8]) |
    la' r dod''( mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') la'( dod'') |
    fad'8.( sold'16 la' si' dod'' re'') |
    dod''8( si') dod''( mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') mi'' mi'' |
    \tuplet 3/2 { mi''16([ dod'' la']) } \tuplet 3/2 { fad''([ re'' si']) } la'8 si'16.( dod''32) |
    \appoggiatura dod''16 re''8 r re''( dod'') |
    fad''16( re'' dod'' si') si'([ la']) la'([ sold']) |
    la'8 r dod''( mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') la'( dod'') |
    fad'8.( sold'16) la'( si' dod'' re'') |
    dod''8( si') dod''( mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') mi'' mi'' |
    \tuplet 3/2 { mi''16( dod'' la') } \tuplet 3/2 { fad''([ re'' si']) } la'8 si'16.( dod''32) |
    \appoggiatura dod''16 re''8 r re''( dod'') |
    fad''16( re'' dod'' si') si'([ la']) la'([ sold']) |
    la'8 r mi''8.( fad''16 |
    mi'' re'') r mi''( re'' dod'') r re''( |
    dod'' si') r8 r16 dod'( mi' la') |
    r mi'( sold' si') r la'( dod'' mi''32 re'') |
    dod''8( si'16) r mi''8.( la''32 fad'' |
    mi''16 re'') r fad''32( mi'' re''16 dod'') r mi''32( re'' |
    dod''16 si') r8 r16 la'( dod'' mi'') |
    r sold'( si' mi'') r la'( dod'' mi'') |
    re''( dod'') si'8 sold''16([ mi'']) mi''([ si']) |
    si'8.(_\markup\dynamic mfp sold'16) la'( sold' la' si') |
    la'8( sold'16) r sold''([ mi'']) mi''([ si']) |
    si'8.(_\markup\dynamic mfp sold'16) la'( sold' la' fad') |
    mi'8 mi''16[-. mi''-.] red''8-. r |
    r16 mi'' mi'' mi'' red''8 r |
    r mi''16-. mi''-. red''8-. r |
    r4 red'\mf\fermata |
    mi'8\p r\fermata dod''( mi'') |
    la'8.( si'32 dod'' mi''16 re'' dod'' si') |
    la'8( sold') la'( dod'') |
    fad'8.( sold'16 la' si' dod'' re'') |
    dod''8( si') dod''( mi'') |
    la'8.( si'32 dod'') mi''16( re'' dod'' si') |
    la'8( sold') mi''8-. mi''-. |
    \tuplet 3/2 { mi''16([ dod'' la'']) } \tuplet 3/2 { la''([ fad'' re'']) } dod''8 si'16.( dod''32) |
    \appoggiatura mi''16 re''8 r re''8. re''16 |
    re''16( si') si'( mi'') mi''8. mi''16 |
    \grace fad''32 mi''16([ re''32 dod'']) dod''8 dod''-. re''-. |
    si' r r4 |
    r4 re''8-. mi''-. |
    dod''-. r r4 |
    r fad''8.( sold''32 la'' |
    mi''8) r r4 |
    r mi''8.(\mf fad''64 sold'' la'' si'' |
    dod'''8) r r4 |
    r dod''8(\p mi'') |
    la'8.( si'32 dod'' mi''16 re'' dod'' si') |
    la'8( sold') mi''8.(\f fad''64 sold'' la'' si'' |
    dod'''8) r16. fad''32\p la'8[ \grace dod''16 si'32( la' si' dod'')] |
    la'8-. mi'-. dod'-. r |
  }
  \tag #'violino2 {
    mi'8(\p si') |
    dod'( mi') fad'( mi'16 re') |
    dod'8( si) mi''(\f re'') |
    dod'' r16. la'32\p dod'8([ \grace mi'16 re'8]) |
    dod' r mi'( si') |
    dod'( mi') fad'( mi'16 re') |
    dod'8( si) la( mi')~ |
    mi'( re') mi'16( sold' la' si') |
    la'8( sold') mi'( si') |
    dod'( mi') fad'( mi'16 re') |
    dod'8( si) la'( re'') |
    dod'' \tuplet 3/2 { la'16([ fad' re']) } dod'8 sold' |
    la' r si'( la') |
    la'16( fad' mi' re') re'([ dod']) dod'([ si]) |
    dod'8 r mi'( si') |
    dod'8( mi') fad'( mi'16 re') |
    dod'8( si) la( mi')~ |
    mi'( re') mi'16( sold' la' si') |
    la'8( sold') mi'( si') |
    dod'( mi') fad'( mi'16 re') |
    dod'8( si) la'( re'') |
    dod'' \tuplet 3/2 { la'16( fad' re') } dod'8 sold' |
    la' r si'( la') |
    la'16( fad' mi' re') re'([ dod']) dod'([ si]) |
    dod' mi' mi' mi' mi' mi' mi' mi' |
    mi'2:16 |
    mi':16 |
    mi'4:16 mi'16 mi'( la' dod''32 si') |
    la'8( sold'16) r la'4( |
    sold' la' |
    sold') la'( |
    sold') la'8.( dod''16) |
    si'( la') sold'8 mi''16([ si']) si'([ sold']) |
    sold'8.(_\markup\dynamic mfp mi'16) fad'( mi' fad' sold') |
    fad'8( mi'16) r mi''([ si']) si'([ sold']) |
    sold'8.(_\markup\dynamic mfp mi'16) fad'16( mi' fad' red') |
    mi'8 sold'16[-. sold'-.] la'8-. r |
    r16 sold' sold' sold' la'8 r |
    r sold'16-. sold'-. la'8-. r |
    r4 la\mf\fermata |
    sold8\p r\fermata la'( sold') |
    fad'( dod' si re') |
    mi'( re' dod' la) |
    re'( si dod' la) |
    mi'4 mi'8( si') |
    dod'16( la'8 sold'16) sold'( fad' mi' re') |
    dod'8( si) la'( re'') |
    \tuplet 3/2 { dod''16([ la' dod'']) } \tuplet 3/2 { fad''([ re'' si']) } la'8( sold') |
    la'8 r r la' |
    sold'[ r16 sold'] sold'( si') si'( re'') |
    \grace re''32 dod''16([ si'32 la']) la'8 la'-. si'-. |
    sold' r r4 |
    r la'8-. sold'-. |
    la'-. r r4 |
    r la'~ |
    la'8 r r4 |
    r mi''8(\mf re'' |
    dod'') r r4 |
    r mi'8(\p dod') |
    re'( sol') fad'4 |
    mi' mi''8(\f re'') |
    dod'' r16. la'32\p dod'8[ \grace mi'16 re'32( dod' re' mi')] |
    dod'8-. dod'-. la-. r |
  }
>>
