\clef "treble" r4 |
R2 |
r4 mi''8.\f( fad''64 sold'' la'' si'' |
dod'''8) r r4 |
R2*10 |
r4 dod'''8(\p mi''') |
la''8.( si''32 dod''') mi'''16( re''' dod''' si'') |
la''8( sold'') la''( dod''') |
fad''8.( sold''16) la''( si'' dod''' re''') |
dod'''8( si'') dod'''( mi''') |
la''8.( si''32 dod''') mi'''16( re''' dod''' si'') |
la''8( sold'') mi''' mi''' |
\tuplet 3/2 { mi'''16([ dod''' la'']) } \tuplet 3/2 { fad'''([ re''' si'']) } la''8 si''16.( dod'''32) |
\appoggiatura dod'''16 re'''8 r re'''( dod''') |
fad'''16( re''' dod''' si'') si''([ la'']) la''([ sold'']) |
la''8 r r4 |
R2*12 |
r4 r8 la''-. |
sold''-. r r red'''-. |
mi'''-. r r4\fermata |
r\fermata r |
R2*3 |
r4 dod'''8( mi''') |
la''8.( si''32 dod''') mi'''16( re''' dod''' si'') |
la''8( sold'') mi''' mi''' |
mi'''16.( dod'''32) fad'''16.( re'''32) dod'''8 si''16.( dod'''32) |
\appoggiatura mi'''16 re'''4 r8 la''( |
sold''4) r8 re'''( |
dod''') r r4 |
R2*6 |
r8 r16. la''32-.\p dod'''8-. si''-. |
la'' r dod'''( mi''') |
la''8.( si''32 dod''' mi'''16 re''' dod''' si'') |
la''8( sold'') mi'''(\f re''') |
dod''' r r4 |
r8 la''-.\p la''-. r |
