\clef "bass" la8(\p sold |
fad dod si, re) |
mi( re) dod(\f si,) |
la, r16. re32\p mi8 mi |
la, r la( sold |
fad dod si, re) |
mi( re dod la,) |
re( si, dod la,) |
mi4 la8( sold |
fad dod si, re) |
mi( re dod sold,) |
la,( re) mi mi |
fad r sold( la) |
re re mi mi |
la, r la( sold |
fad dod si, re) |
mi( re dod la,) |
re( si, dod la,) |
mi4 la8( sold |
fad dod si, re) |
mi( re dod sold,) |
la,( re) mi mi |
fad r sold( la) |
re re mi mi |
la, r dod'4(_"Vcl." |
si la |
re') <>_"Bassi" dod'( |
sold la) |
mi8 r dod'4(_"Vcl." |
si la |
re') <>_"Bassi" dod'( |
si la) |
mi8 r r4 |
si,2_\markup\dynamic mfp |
si,8( mi16) r r4 |
si,2 |
mi4 r |
R2*2 |
r4 si,\fermata\mf |
mi8\p r\fermata r4 |
R2*3 |
r4 la8( sold) |
fad( dod) re16( si, dod re) |
mi8( re dod sold) |
la( re) mi mi |
fad4 r8 fad( |
mi4) r8 sold( |
la) r fad-. re-. |
mi r r4 |
r si8-. mi-. |
la r r4 |
r re'4_"Vcl."( |
dod'8) r16. re32-._"Bassi" mi8 mi |
la r dod'(\mf si |
la) r16. re32-.\p mi8-. mi-. |
la, r la( sol |
fad mi re red) |
mi( re!) dod(\f si,) |
la, r16. red32\p mi8-. mi-. |
la,-. la-. la,-. r |
