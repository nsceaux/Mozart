% ANNIO
\tag #'voix2 {
  Ah! per -- do -- na al pri -- mo af -- fet -- to
  que -- sto ac -- cen -- to scon -- si -- glia -- to
  col -- pa fù del lab -- bro u -- sa -- to
  a co -- sì chi a -- mar -- ti o -- gnor,
  a co -- sì chi a -- mar -- ti o -- gnor.
}
% SERVILIA
\tag #'voix1 {
  Ah tu fo -- sti il pri -- mo og -- get -- to,
  che si -- nor fe -- del a -- ma -- i,
  e tu l’ul -- ti -- mo sa -- ra -- i
  ch’ab -- bia ni -- do in que -- sto cor,
  ch’ab -- bia ni -- do in que -- sto cor.
}
% ANNIO
\tag #'voix2 {
  Ca -- ri ac -- cen -- ti del mio be -- ne!
}
% SERVILIA
\tag #'voix1 {
  Oh mia dol -- ce, ca -- ra spe -- me!
}
Più che as -- col -- to i sen -- si tuo -- i,
in me cre -- sce più l’ar -- dor,
più che as -- col -- to i sen -- si tuo -- i,
in me cre -- sce più l’ar -- dor.
Quan -- do un’ al -- ma, e al -- tra u -- ni -- ta,
qual pia -- cer un cor ri -- sen -- te!
Ah si tol -- ga d’al -- la vi -- ta
tut -- to quel, che non è a -- mor.
Ah si tol -- ga d’al -- la vi -- ta
tut -- to quel,
tut -- to quel, che non è a -- mor,
che non è a -- mor.
