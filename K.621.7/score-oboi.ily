\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
      $(or (*score-extra-music*) (make-music 'Music))
           \keepWithTag #(*tag-global*) \global
           \keepWithTag #'oboe1 \includeNotes "oboi"
    >>
    \new Staff \with { \haraKiri } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'oboe2 \includeNotes "oboi"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
