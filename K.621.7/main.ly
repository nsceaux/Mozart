\version "2.19.80"
\include "common.ily"

\opusTitle "K.621 – N°7 – Ah! perdona al primo affetto"

\header {
  title = \markup\center-column {
    \line\italic { La clemenza di Tito }
    \line { N°7 – Duetto: \italic { Ah! perdona al primo affetto } }
  }
  opus = "K.621"
  date = "1791"
  copyrightYear = "2019"
}

\includeScore "K.621.7"
