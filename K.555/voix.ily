\clef "soprano/treble"
<<
  \tag #'voix1 {
    r4 la'2 do''4 |
    si'8([ re'' fa'' mi''] re''[ do'' si' la'] |
    sold'4. la'8 si'[ do'' re'' si'] |
    mi''4) re''8 do'' do''4( si') |
  }
  \tag #'voix2 {
    la'( sol') fa' mi' |
    re'8([ fa' sold' la'] si'[ do'' re'' mi''] |
    fa''[ mi'']) mi''4 r4 si'8 mi' |
    do''4 si'8 la' la'4( sold') |
  }
  \tag #'voix3 {
    la'2 r |
    r4 si'8([ do'']) re''([ mi'']) fa''4 |
    r re''8([ do'']) si'([ la']) sold'4 |
    la' re' mi'2 |
  }
  \tag #'voix4 {
    la4 r la'2~ |
    la'4 re' re'2~ |
    re'8[ sold'( si' la'] sold'[ fa' mi' re')] |
    do'4 fa' mi'4.( re'8) |
    do'4 \bar "|."
  }
>>

