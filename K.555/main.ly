\version "2.19.80"
\include "common.ily"

\opusTitle "K.419 – No, che non sei capace"

\header {
  title = "Canone à 4"
  subtitle = "Lacrimoso son’io"
  opus = "K.555"
  date = "1788"
  copyrightYear = "2019"
}

\includeScore "K.555"
