\score {
  <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \with {
      \remove "Time_signature_engraver"
    } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \with {
      \remove "Time_signature_engraver"
    } \withLyrics <<
      \global \keepWithTag #'voix3 \includeNotes "voix"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff \with {
      \remove "Time_signature_engraver"
    } \withLyrics <<
      \global \keepWithTag #'voix4 \includeNotes "voix"
    >> \keepWithTag #'voix4 \includeLyrics "paroles"
  >>
  \layout {
    indent = 0
    short-indent = 0
    system-count = 1
  }
  \midi { }
}
