\clef "bass" R2.*2 |
<<
  \tag #'fagotto1 {
    sib2.~ |
    sib4\p sib8-. do'-. re'-. mib'-. |
    mib'4( re') r8 mib' |
    do'( sib) sib4.( lab8) |
    sol4 r8 sol'( fa' mib') |
    do'( sib) sib4.( lab8) |
    sol4 r r8 re' |
    do'4. re'8( la sib) |
    fa4. re'8( mib' fa') |
    fa'4( sol8) mib'( fa' sol') |
    sib2~ sib8\( \appoggiatura re'16 do'8\) |
    sib4 r r |
    fa2.\pp |
    fa~ |
    fa~ |
    fa~ |
    fa4 r r8 re'\p |
    do'4. re'8( la sib) |
    do'4. re'8( mib' fa') |
    fa'4( sol8) mib'( fa' sol') |
    sib2~ sib8\( \appoggiatura re'16 do'8\) |
    sib4 r r8 re' |
    do'4. re'8( la sib) |
    fa2 r8 sib\mf |
    la2 r8\fermata r |
    R2. |
    r8 fa8(\f sib do' re' mib') |
    fa'(\p sol') sib4. \appoggiatura re'16 do'8 |
    sib4 r8 fa'(\f mib' re') |
    re'4( do'8) mib'( fa' sol') |
    sib4( la8) la(\p sib do') |
    re'( mib') re'4 \appoggiatura re'16 do'8( sib16 do') |
    sib8 r r4 r |
    R2.*3 |
    r4 r r8 fa' |
    fa'( mib') mib'2~ |
    mib'8( reb') reb'2~ |
    reb'8( do') do'4.( reb'16 sib) |
    sib4( la) r4\fermata |
  }
  \tag #'fagotto2 {
    re2.\f |
    mib4\p r r |
    R2.*6 |
    r4 r r8 re |
    mib2 r8 mib |
    fa2. |
    sib,4 r r |
    fa,2.\pp |
    fa,~ |
    fa,~ |
    fa,~ |
    fa,4 r r |
    R2. |
    r4 r r8 re-\sug\p |
    mib2 r8 mib |
    fa2. |
    sol4 r r |
    R2. |
    r4 r r8 sib,\mf |
    la,2 r8\fermata r |
    R2.*3 |
    r4 r8 re(\f mib fa) |
    fa4( mib8) mib( re do) |
    << fa2.~ { s4. s\p } >> |
    fa8( sol) fa r fa r |
    sib, r r4 r |
    R2.*4 |
    solb2.( |
    fa |
    mib2 mi4) |
    fa fa r\fermata |
  }
>>
R2.*5 |
<<
  \tag #'fagotto1 {
    r4 reb'~ reb'8 r |
    r4 reb'~ reb'8 r |
    r4 do'~ do'8 r |
    r4 do'\f~ do'8\fermata r |
  }
  \tag #'fagotto2 {
    r4 fa~ fa8 r |
    r4 mi~ mi8 r |
    r4 mib!~ mib8 r |
    r4 re!\f~ re8\fermata r |
  }
>> \allowPageTurn
R1*3 |
r2 r4 r\fermata |
<<
  \tag #'fagotto1 {
    sib4\p~ sib8 do'-. re'-. mib'-. |
    mib'4( re') r8 mib' |
    do'8( sib) sib4.( lab8) |
    sol4 r8 sol'( fa' mib') |
    do'( sib) sib4.( lab8) |
    sol4 r r8 re' |
    do'4. re'8( la sib) |
    fa4. re'8( mib' fa') |
    fa'4( sol8) mib'( fa' sol') |
    sib2~ sib8\( \appoggiatura re'16 do'8\) |
    sib4 r r |
  }
  \tag #'fagotto2 {
    R2.*7 |
    r4 r r8 re\p |
    mib2 r8 mib |
    fa2. |
    sib,4 r r |
  }
>>
R2.*3 |
sib,2.\pp~ |
sib,2 dob4 |
sib, r <<
  \tag #'fagotto1 {
    r8 sib\p |
    sib4. sol'8( fa' mib') |
    mib'4( re'8) mib'( re' reb') |
    si4( do'8) mib'( fa' sol') |
    sol'4( sib!8) do'( re'! mib') |
    mib2~ mib8\( \appoggiatura sol16 fa8\) |
    mib4 r r8 sib |
    sib4. sol'8( fa' mib') |
    mib'4( re') r8 sol'16(\mf mib') |
    \override Script.avoid-slur = #'outside
    mib'4( re'8)\fermata mib'([\p re' reb']) |
    si4( do'8) mib'( fa' sol') |
    sol'4( sib!8) do'( re' mib') |
    mib2~ mib8\( \appoggiatura sol16 fa8\) |
    mib4 r8 sib(\mf lab sol) |
    mib'8.(\p do'16) sib4.( lab8) |
    sol4 r8 fa'(\f mib' re') |
    mib'8\p do'16( lab) sol8 r fa r |
    mib4 r sol8( mib) |
    sib r r4 mib'8( sol) |
    sib r r4 r |
  }
  \tag #'fagotto2 {
    r4 |
    R2.*2 |
    r4 r r8 lab,-\sug\p |
    sol,2 r8 la, |
    sib,2. |
    do4 r r |
    R2. |
    r4 r r8 sol\mf |
    lab4~ lab8\fermata r r4 |
    r r r8 lab,\p |
    sol,2 r8 la, |
    sib,2. |
    mib,4 r8 sol(\mf fa mib) |
    do'8.(\p lab16) sol4.( fa8) |
    mib4 r8 fa(\f sol lab) |
    sol8\p lab16( fa) mib8 r re r |
    mib4 r sol,8( mib,) |
    sib, r r4 mib8( sol,) |
    sib, r r4 r |
  }
>>
R2.*2 |
<<
  \tag #'fagotto1 {
    do'2. |
    sib8 mib'4 sol' fa'8~ |
    fa'\cresc mib' mib'4( re') |
    mib'\f r8 sib( lab sol) |
    sol4( fa8) lab( sib do') |
    do'4( sib8) r r4 |
    r r sib\p |
    sib r r |
  }
  \tag #'fagotto2 {
    lab2. |
    sol8 sol4 sib lab8~ |
    lab\cresc sol fa4 sib, |
    mib4\f r8 sib,( re mib) |
    lab,4. fa,8( sol, lab,) |
    sib,2 r4 |
    r r sib,\p |
    mib r r |
  }
>>
