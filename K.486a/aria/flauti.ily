\clef "treble" R2. |
<<
  \tag #'(flauto1 flauti) { la''2. | lab''! | sol''4 }
  \tag #'(flauto2 flauti) { fa''2.~ | fa'' | mib''4 }
  { s2.\p | s\f | s4\p }
>> r4 r |
R2.*10 |
<>\pp <<
  \tag #'(flauto1 flauti) { re''2. | re'' | do'' | reb'' | do''4 }
  \tag #'(flauto2 flauti) { sib'2. | sib' | la' | sib' | la'4 }
>> r4 r |
R2.*6 |
r4 r8 <>\mf <<
  \tag #'(flauto1 flauti) { fa''8-.( fa''-. fa''-.) | fa''2 }
  \tag #'(flauto2 flauti) { fa'8-.( fa'-. fa'-.) | fa'2 }
>> r8\fermata r |
R2.*3 |
r8 <>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { sib'8( re'' fa'' mib'' re'') |
    si'4( do''8) mib''( fa'' sol'') |
    sib'4( la'8) }
  { sib'8 re'' re''( do'' sib') |
    si'4( do''8) mib''( fa'' sol'') |
    sib'4( la'8) }
>> r8 r4 |
r8 \tag#'flauti <>^"a 2." mib''8\p re''4 \appoggiatura re''16 do''8( sib'16 do'') |
sib'8 r r4 r |
R2.*7 |
r4 r r\fermata |
R2.*8 |
r4 r r8\fermata r |
R1*3 |
r2 r4 r\fermata |
R2.*11 |
<>\pp <<
  \tag #'(flauto1 flauti) { sol'2.~ | sol' | lab'! | }
  \tag #'(flauto2 flauti) { mib'2.~ | mib' | fa' | }
>>
R2.*10 |
r4 r8 <>\mf <<
  \tag #'(flauto1 flauti) {
    sib''8-.( sib''-. sib''-.) |
    sib''4~ sib''8\fermata
  }
  \tag #'(flauto2 flauti) {
    sib'8-.( sib'-. sib'-.) |
    sib'4~ sib'8\fermata
  }
>> r8 r4 |
R2.*7 |
r4 r <>\p <<
  \tag #'(flauto1 flauti) { sol''8( mib'') | sib'' }
  \tag #'(flauto2 flauti) { sol'8( mib') | sib' }
>> r8 r4 <<
  \tag #'(flauto1 flauti) { mib'''8( sol'') | sib'' }
  \tag #'(flauto2 flauti) { mib''8( sol') | sib' }
>> r8 r4 r |
R2.*3 |
r4 r r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { lab''8~ |
    lab'' sol'' fa''2 |
    mib''4 s8 sib''( lab'' sol'') |
    sol''4( fa''8) lab''( sib'' do''') |
    do'''4( sib''8) lab''( sol'' fa'') |
    mib''4( fa'' re'') |
    mib'' }
  { fa''8~ |
    fa'' mib'' mib''4( re'') |
    mib'' s8 sol''( fa'' mib'') |
    do''4. lab'8( sib' do'') |
    re''4. fa''8( mib'' re'') |
    mib''4( do'' lab') |
    sol' }
  { s8 | s2.\cresc | s4\f r8 s4. | s2.*2 | s2.\p | }
>> r4 r |
