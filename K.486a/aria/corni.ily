\clef "treble" \transposition mib
R2.*2 |
<<
  \tag #'(corno1 corni) { re''2. | do''4 }
  \tag #'(corno2 corni) { sol2. | sol4 }
  { s2.\f | s4\p }
>> r4 r |
R2.*11 |
\tag #'corni <>^"a 2." re''2.\pp~ |
re''~ |
re''~ |
re''4 r r |
R2.*6 |
r4 r8 \tag #'corni <>^"a 2." re''\mf re'' re'' |
re''2 r8\fermata r |
R2.*3 |
r4 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 | }
  { sol' | }
>>
<<
  \tag #'(corno1 corni) { mi''4~ mi''8 }
  \tag #'(corno2 corni) { do'4~ do'8 }
>> r8 r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4~ re''8 }
  { re''4~ re''8 }
>> r8 r4 |
R2. |
<>\p <<
  \tag #'(corno1 corni) {
    sol'2.~ | sol'~ | sol'~ | sol'~ |
    sol'~ | sol'~ | sol'~ | sol' |
  }
  \tag #'(corno2 corni) {
    sol2.~ | sol~ | sol~ | sol~ |
    sol~ | sol~ | sol~ | sol
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 }
  { re'' }
>> r4\fermata |
R2.*8 |
r4 r r8\fermata r |
R1*3 |
r2 r4 r\fermata |
R2.*11 |
<>\pp <<
  \tag #'(corno1 corni) { sol'2.~ | sol'~ | sol'~ | sol'~ | sol'2 }
  \tag #'(corno2 corni) { sol2.~ | sol~ | sol~ | sol~ | sol2 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { do'4 | sol }
  { do'4 | sol }
>> r4 r |
R2.*7 |
r4 r8 <>\mf <<
  \tag #'(corno1 corni) {
    sol'8 sol' sol' |
    sol'4~ sol'8\fermata
  }
  \tag #'(corno2 corni) {
    sol8 sol sol |
    sol4~ sol8\fermata
  }
>> r8 r4 |
R2.*5 |
r4 r8 <>\f <<
  \tag #'(corno1 corni) { sol'8 sol' sol' | }
  \tag #'(corno2 corni) { sol8 sol sol | }
>>
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { do'4 sol'2 | }
  { do'4 sol'2 | }
>>
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { do' }
>> r4 \tag#'corni <>^"a 2." mi'8( do') |
sol' r r4 do''8( mi') |
sol' r r4 r |
R2.*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'2.~ | sol' | }
  { sol'2.~ | sol' | }
  { s2. | s\cresc <>\f }
>>
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { do' }
>> r4 r |
<<
  \tag #'(corno1 corni) { do''4~ do''8 }
  \tag #'(corno2 corni) { do'4~ do'8 }
>> r8 r4 |
<<
  \tag #'(corno1 corni) { re''4. fa''8 mi'' re'' | do''4 }
  \tag #'(corno2 corni) { sol'4. re''8 do'' sol' | mi'4 }
>> r4 <<
  \tag #'(corno1 corni) { re''4 | do'' }
  \tag #'(corno2 corni) { sol'4 | mi' }
>> r4 r |
