\clef "soprano/treble" R2.*3 |
sib'4 sib'8 do'' re'' mib'' |
mib''4( re'') r8 mib'' |
do'' sib' sib'4.( lab'8) |
sol'4 r r8 mib'' |
do'' sib' sib'4.( lab'8) |
sol'4 r r8 sib' |
sib'4( la'8) sib' do'' re'' |
re''4( do'') r8 fa'' |
fa''4( sol') r8 sol'' |
sib'2~ sib'8\([ \appoggiatura re''16 do''8]\) |
sib'4 r r |
r r r8 re'' |
fa''4. mib''16([ re'']) do''8 sib' |
do''4 r r8 fa'' |
reb''4 r fa''8. reb''16 |
do''8 do'' r4 r8 sib' |
sib'4( la'8) sib' do'' re'' |
fa''4( mib'') r8 fa'' |
fa''4( sol') r8 sol'' |
sib'2~ sib'8[\( \appoggiatura re''16 do''8]\) |
sib'4 r r8 sib' |
sib'4( la'8) sib' do'' re'' |
re''4( do'') r8 re'' |
fa''4( mib'') r8\fermata re'' |
sol' mib'' re''4.( do''8) |
re''4 r r8 mib'' |
fa'' sol'' sib'4.\(\melisma \appoggiatura re''16 do''8\)\melismaEnd |
sib'4 r r |
R2.*3 |
r4 r r8 fa' |
reb''4. sib'8 sib' fa' |
solb' solb' r4 r8 solb' |
solb''4. mib''8 mib'' do'' |
reb'' reb'' r4 r8 fa'' |
fa''([ mib'']) mib''4. mib''8 |
mib''([ reb'']) reb''4. reb''8 |
reb''([ do'']) do''4. reb''16([ sib']) |
sib'4 la' r\fermata |
r4 r r8 do'' |
do''4. reb''16([ do'']) do''8 do'' |
do''8.([ fa''16]) do''4 r8 do'' |
do''4. reb''16([ do'']) do''8 do'' |
do''([ lab'']) lab''4. sol''16([ fa'']) |
mib''8([ reb'']) reb''4. reb''8 |
\appoggiatura mib''8 reb''4 reb'' r8 reb'' |
reb''([ do'']) do''4. do''8 |
\appoggiatura re''!16 do''4 do'' r8\fermata do'' |
sib'4 r r sib'8 do'' |
lab'! lab' r4 r sib' |
sol' r r mib''8 la' |
sib' sib' r4 r r\fermata |
sib'4 sib'8 do'' re'' mib'' |
mib''4( re'') r8 mib'' |
do''8 sib' sib'4.( lab'8) |
sol'4 r r8 mib'' |
do'' sib' sib'4.( lab'8) |
sol'4 r r8 sib' |
sib'4( la'8) sib' do'' re'' |
re''4( do'') r8 fa'' |
fa''4( sol') r8 sol'' |
sib'2~ sib'8\([ \appoggiatura re''16 do''8]\) |
sib'4 r r |
r r r8 sib' |
sib'4. do''16([ re'']) mib''8. fa''16 |
mib''4( re'') r8 sib' |
mib''4 r solb''8. mib''16 |
re''8 fa'' r4 mib''8.([ la'16]) |
sib'4 r r8 sib' |
sib'4. sol''!8 fa'' mib'' |
mib''4( re'') r8 reb'' |
si'4( do'') r8 sol'' |
sol''4( sib'!) r8 mib'' |
mib'2~ mib'8\([ \appoggiatura sol'16 fa'8]\) |
mib'4 r r8 sib' |
sib'4. sol''8 fa'' mib'' |
mib''4( re'') r8 sol''16([ mib'']) |
\override Script.avoid-slur = #'outside
mib''4( re''8)\fermata r r reb'' |
si'4( do'') r8 sol'' |
sol''4( sib'!8) do'' re'' mib'' |
mib'2~ mib'8\([ \appoggiatura sol'16 fa'8]\) |
mib'4 r r8 sol' |
mib''8. do''16 sib'4.( lab'8) |
sol'4 r r8 lab'' |
sol''16([ mib'']) do''([ lab']) sol'4( fa')\trill |
mib'4 r r |
R2. |
r4 r8 sib' mib'' sol'' |
sib''2( lab''16[ sol'']) fa''([ mib'']) |
\appoggiatura re''8 do''4 r8 do'' fa'' sol'' |
lab''2\melisma \appoggiatura sib''16 lab''8[ sol''16 fa''] |
mib''8 sol''4 sib'' lab''8~ |
lab''([ sol'']) fa''2\trill\melismaEnd |
mib''4 r r |
R2.*4 |
