\clef "bass" <>\p \ru#6 { re8 r } |
<>\f \ru#3 { re r } |
mib4\p r mib |
fa2 r8 sol |
lab( sol) fa r sib, r |
mib4 r8 mib( fa sol) |
lab( sol) fa r sib, r |
mib4 r r8 re'^"Vcl." |
do'4. re'8( la sib) |
fa2 r8 re^"Bassi" |
mib2 r8 mib |
fa r fa r fa r |
sib,4 r r |
<>\pp \ru#12 { fa,8 r } |
fa,4 r r8 re'\p ^"Vcl." |
do'4. re'8( la sib) |
do'4~ do'8 r r re^"Bassi" |
mib2 r8 mib |
fa8 r fa r fa r |
sol4 r r8 re'^"Vcl." |
do'4. re'8( la sib) |
fa2 r8 sib\mf |
la2 r8\fermata sib8\p ^"Bassi" |
mib4 fa2 |
sib,4 r8 la,(\f sib, do) |
re(\p mib) fa4 fa, |
sib,4 r8 sib,(\f do re) |
mib2 mib4 |
fa4. mib8(\p re do) |
sib, sol fa r fa r |
\ru#15 { sib,8 r } |
solb2. |
fa |
mib2( mi4) |
fa8 r fa, r r4\fermata |
r r8 fa( sol fa) |
mi4 mi mi |
fa4 r8 fa( sol fa) |
mi4 mi mi |
fa r r |
fa2. |
mi |
mib! |
re!2\f r8\fermata r |
re4\p r r2 |
re4 r r2 |
r4 mib-. do-. r |
r re-. sib,-. r\fermata |
mib4\p r mib |
fa2 r8 sol |
lab( sol) fa r sib, r |
mib4 r8 mib( fa sol) |
lab( sol) fa r sib, r |
mib4 r r8 re'^"Vcl." |
do'4. re'8( la sib) |
fa2 r8 re^"Bassi" |
mib2 r8 mib |
fa r fa r fa r |
sib4 r r |
<>\pp \ru#14 { sib,8 r } dob r |
sib,4 r r |
<>^"Vcl." sol2. |
lab4~ lab8 r r sol^"Bassi" |
lab2 lab,4 |
sol,2( la,4) |
sib,8 r sib, r sib, r |
do4 r r |
<>^"Vcl." sol2. |
fa2 r8 sol\mf |
lab4~ lab8\fermata r r sol^"Bassi"\p |
lab2 lab,4 |
sol,2( la,4) |
sib,8 r sib, r sib, r |
mib4 r8 mib(\mf fa sol) |
lab4\p sib4.( si8) |
do'4 r8 re(\f mib fa) |
mib8\p lab, sib, r sib, r |
mib,4 r r |
R2.*2 |
sol8\f sol\p sol sol sol sol |
lab4 r r |
lab8\f lab\p lab lab lab lab |
sib sib sib sib sib sib |
sib,\cresc sib, sib, sib, sib, sib, |
mib4\f r8 mib( fa sol) |
lab2 lab,4 |
sib,2 sib,4 |
do(\p lab, sib,) |
mib, r r |
