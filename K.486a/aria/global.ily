\beginMark "Aria"
\tag #'all \key mib \major
\tempo "Andantino espressivo" \midiTempo#100
\time 3/4 s2.*52 \bar "||"
\time 4/4 \beginMark "Recitativo" \tempo "Allegretto" s1*4 \bar "||"
\time 3/4 \tempo "Tempo primo" s2.*46 \bar "|."
