\piecePartSpecs
#`((flauti)
   (fagotti #:score-template "score-fagotti")
   (corni  #:tag-global ()
           #:score-template "score-corni"
           #:instrument "Corni in Es")
   (violino1 #:tag-global all #:indent 0)
   (violino2 #:tag-global all #:indent 0)
   (viola #:tag-global all #:indent 0)
   (basso #:tag-global all #:indent 0)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Aria: TACET } #}))
