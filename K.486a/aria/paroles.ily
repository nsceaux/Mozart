Ah non la -- sciar -- mi, no, __ bell’ i -- dol mi -- o, bell’ i -- dol mi -- o;
di chi __ mi fi -- de -- rò, __ se tu __ m’in -- gan -- ni?
Ah non la -- sciar -- mi, no,
Ah no, non la -- sciar -- mi;
di chi __ mi fi -- de -- rò, __ se tu __ m’in -- gan -- ni?
di chi __ mi fi -- de -- rò, __ di chi,
se tu m’in -- gan -- ni,
se tu m’in -- gan -- ni?

Di vi -- ta man -- che -- re -- i,
di vi -- ta man -- che -- re -- i
nel dir -- ti: ad -- di -- o,
nel dir -- ti: ad -- di -- o,
che vi -- ver non po -- tre -- i,
che vi -- ver non po -- tre -- i
fra tan -- ti af -- fan -- ni,
fra tan -- ti af -- fan -- ni!

Ah no, non la -- sciar -- mi!
ah no, non la -- sciar -- mi!

Ah non la -- sciar -- mi, no, __ bell’ i -- dol mi -- o, bell’ i -- dol mi -- o;
di chi __ mi fi -- de -- rò, __ se tu __ m’in -- gan -- ni?
Ah non la -- sciar -- mi, no, __
ah no, non la -- sciar -- mi;
ah __ no, di chi mi fi -- de -- rò, __
se tu __ se tu __ m’in -- gan -- ni?
di chi mi fi -- de -- rò, __ di chi, __
se tu, __ se tu, __
se tu m’in -- gan -- ni?
se tu m’in -- gan -- ni,
se tu m’in -- gan -- ni?
Ah non la -- sciar -- mi, no,
ah non la -- sciar -- mi!
