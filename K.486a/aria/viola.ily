\clef "alto" r8 re'\p r re' r re' |
r re' r re' r re' |
r <re sib>\f r q r q |
<mib sib>4\p r sib |
sib2 r4 |
r8 sib do' r re' r |
sib4 r8 sib( re' mib') |
r sib do' r re' r |
sib4 r r8 re' |
mib'4. re'8( do' sib) |
sib4( la8) r r sib |
sib2 r8 do' |
fa' r fa' r fa' r |
fa'4 r r |
r8 fa\pp r fa r fa |
r fa r fa r fa |
r fa r fa r fa |
r fa r fa r fa |
r fa r4 r8 re'\p |
mib'4. re'8( do' sib) |
do'4~ do'8 r r sib |
sib2 r8 do' |
fa' r fa' r fa' r |
sol'4 r r8 re' |
mib'4. re'8-. do'-. sib-. |
sib4( la) r8 sib\mf |
do'2 r8\fermata sib8\p |
sib4( fa'8) r fa' r |
fa'4 r8 do'(\f sib la) |
fa(\p mib) fa r fa r |
fa re'(\f fa' fa la sib) |
sol'4. sol8( si do') |
fa'4. fa8\p fa( la) |
sib( mib') fa' r fa' r |
fa' sib r sib r sib |
r sib r sib r sib |
r sib r sib r sib |
r la r la r la |
r sib r sib r sib |
<< { sib2.~ | sib } \\ { mib | fa | } >>
solb2( sol4) |
fa8 r fa r r4\fermata |
r4 r8 fa( mi fa) |
sib <sol mi>4 q q8 |
fa4 r8 fa( mi fa) |
sib <sol mi>4 q q8 |
fa2. |
lab! |
sol~ |
sol |
fad2\f r8\fermata r |
sol4\p r r2 |
sib4 r r2 |
r4 mib'-. mib-. r |
r sib-. fa'-. r\fermata |
mib'4\p r sib |
sib2 r4 |
r8 sib do' r re' r |
sib4 r8 sib( re' mib') |
r sib do' r re' r |
sib4 r r8 re' |
mib'4. re'8( do' sib) |
sib4( la8) r r sib |
sib2 r8 do' |
fa' r fa' r fa' r |
fa'4 r r |
r8 sib\pp r sib r sib |
r sib r sib r sib |
r fa r fa r fa |
r solb r solb r la |
r sib r sib r dob' |
sib4 r r |
sib2.\p |
sib4~ sib8 r r sib |
mib2 r8 do' |
sib2 r8 solb |
sol! r sol r sib r |
do'4 r r |
sib2. |
sib2 r8 mib'\mf |
fa'4~ fa'8\fermata r r sib\p |
mib2 r8 do' |
sib2 r8 solb |
sol! r sol r lab r |
sol4 r8 sib(\mf re' mib') |
mib'4\p sib4.( si8) |
do'4 r8 fa'(\f mib' re') |
mib'8.(\p do'16) sib8 r lab r |
sol4 r r |
R2.*2 |
mib'8\f mib'\p mib' mib' sib sib |
mib4 r r |
do'8\f do'\p do' do' \acciaccatura re'16 do'8 sib16 lab |
sol8 mib'4 sol' fa'8~ |
fa'\cresc mib' <mib' sib> q <re' lab> q |
<sol mib'>\f sol( sib sol lab sib) |
do'4. lab8( sol fa) |
fa2 sib8( lab) |
sol4(\p lab fa) |
mib r r |
