\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global
        \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new GrandStaff \with { \fagottiInstr } <<
        \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "fagotti" >>
        \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "fagotti" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corni in Es }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Soprano
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \vccbInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s2.*6\break s2.*8\pageBreak
        s2.*8\break s2.*8\pageBreak
        s2.*7\break s2.*8\pageBreak
        s2.*7\break s1*4 s2.*2\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
