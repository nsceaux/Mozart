\clef "treble" <>^"con sordino"
<<
  \tag #'violino1 {
    r8 la'\p r la' r la' |
    r la' r la' r la' |
    r lab'!\f r lab' r lab' |
    sol'(\p sib') sib'-. do''-. re''-. mib''-. |
    mib''4( re'') r8 mib''-. |
    do''( sib') sib'4.( lab'8) |
    sol' sib'(_\markup\italic rinf mib'' sol''\p fa'' mib'') |
    do''( sib') sib'4.( lab'8) |
    sol'4 r r8 sib' |
    sib'4( la'8) sib'-. do''-. re''-. |
    re''4( do''8) re''( mib'' fa'') |
    fa''4( sol'8) mib''( fa'' sol'') |
    sib' r sib' r la' r |
    sib'4 r r |
    r8 re'\p r re' r re' |
    r re' r re' r re' |
    r do' r do' r do' |
    r reb' r reb' r reb' |
    r do' r4 r8 sib'\p |
    sib'4( la'8) sib'-. do''-. re''!-. |
    fa''4( mib''8) re''( mib'' fa'') |
    fa''4( sol'8) mib''( fa'' sol'') |
    sib'8 r sib' r la' r |
    sib'4 r r8 sib' |
    sib'4( la'8) sib'-. do''-. re''-. |
    re''4( do'') r8 re''\mf |
    fa''4( mib'') r8\fermata re''\p |
    sol'( mib'') re'' r la' r |
    sib' fa'(\f sib' do'' re'' mib'') |
    fa''(\p sol'') sib' r la' r |
    sib'4 r8 fa''(\f mib'' re'') |
    si'4( do''8) mib''( fa'' sol'') |
    sib'4( la'8) la'(\p sib' do'') |
    re''( mib'') re''4 \appoggiatura re''16 do''8( sib'16 do'') |
    sib'8 fa' r fa' r fa' |
    r fa' r fa' r fa' |
    r solb' r solb' r solb' |
    r solb' r solb' r solb' |
    r fa' r fa' r fa' |
    r sib'4( mib''8 solb'' sib'') |
    r sib'4( reb''8 fa'' sib'') |
    r solb''4( mib''8 do'' sib') |
    sib' r la' r r4\fermata |
    do'8 do'4 do' do'8~ |
    do' do'4 do' do'8~ |
    do' do'4 do' do'8~ |
    do' do'4 do' do'8 |
    do'4( fa' lab') |
    r8 fa'-. lab'-. reb''-. lab'' r |
    r mi'-. sib'-. reb''-. sol'' r |
    r mib'!-. sol'-. do''-. sol'' r |
    r re'-.\f la'-. do''-. fad''8\fermata r |
    r8 sib'-.\p re''-. sol''-. sib''4 r |
    r8 lab'!-. re''-. fa''-. lab''!4 r |
    r4 sol''-. la'-. r |
    r fa''-. sib''-. r\fermata |
    sib'4\p~ sib'8 do''-. re''-. mib''-. |
    mib''4( re'') r8 mib''-. |
    do''8( sib') sib'4.( lab'8) |
    sol' sib'( mib'' sol'' fa'' mib'') |
    do''( sib') sib'4.( lab'8) |
    sol'4 r r8 sib' |
    sib'4( la'8) sib'-. do''-. re''-. |
    re''4( do''8) re''( mib'' fa'') |
    fa''4( sol'8) mib''( fa'' sol'') |
    sib' r sib' r la' r |
    sib'4 r r |
    r8 mib'\pp r mib' r mib' |
    r mib' r mib' r mib' |
    r <re' sib> r q r q |
    r mib' r mib' r solb' |
    r fa' r fa' r la' |
    sib'4 r r8 sib'\p |
    sib'4. sol''!8( fa'' mib'') |
    mib''4( re''8) mib''( re'' reb'') |
    si'4( do''8) mib''( fa'' sol'') |
    sol''4( sib'!8) do''( re''! mib'') |
    mib' r mib' r re' r |
    mib'4 r r8 sib' |
    sib'4. sol''8( fa'' mib'') |
    mib''4( re'') r8 sol''16(\mf mib'') |
    \override Script.avoid-slur = #'outside
    mib''4( re''8)\fermata mib''([\p re'' reb'']) |
    si'4( do''8) mib''( fa'' sol'') |
    sol''4( sib'!8) do''( re'' mib'') |
    mib' r mib' r re' r |
    mib'4 r8 sib'(\mf lab' sol') |
    mib''8.(\p do''16) sib'4.( lab'8) |
    sol'4 r8 fa''(\f sol'' lab'') |
    sol''16(\p mib'') do''( lab') sol'8 r fa' r |
    mib'4 r r |
    R2. |
    r4 r8 <>^"senza sordino" sib'-. mib''-. sol''-. |
    sib''2\fp lab''16( sol'' fa'' mib'') |
    \appoggiatura re''8 do''4 r8 do''-. fa''-. sol''-. |
    lab''8(\f fa''\p lab'' fa'') \appoggiatura sib''16 lab''8 sol''16 fa'' |
    mib''8 sol''4 sib'' lab''8~ |
    lab''\cresc sol'' fa'' fa'' fa'' fa'' |
    mib''4\f r8 sib'( lab' sol') |
    sol'4( fa'8) lab'( sib' do'') |
    do''4( sib'8) lab'( sol' fa') |
    mib'4(\p fa' re') |
    mib' r r |
  }
  \tag #'violino2 {
    r8 fad'\p r fad' r fad' |
    r fa'! r fa' r fa' |
    r <fa' sib>\f r q r q |
    mib'4\p r sol |
    lab2 r8 sib |
    do'( mib') mib' r fa' r |
    mib'4 r8 sib'( lab' sol') |
    mib'4~ mib'8 r fa' r |
    mib'4 r r8 fa' |
    fa'4. fa'8(-. fa'-. fa'-.) |
    fa'4~ fa'8 r r4 |
    R2. |
    <sib re'>8 r q r <do' mib'> r |
    <sib re'>4 r r |
    r8 sib\pp r sib r sib |
    r sib r sib r sib |
    r la r la r la |
    r sib r sib r sib |
    r la r4 r8 fa'\p |
    fa'4. fa'8(-. fa'-. fa'-.) |
    <fa' la'>4~ q8 r r4 |
    R2. |
    <sib re'>8 r q r <do' mib'> r |
    <sib re'>4 r r8 fa' |
    fa'4. fa'8(-. fa'-. fa'-.) |
    fa'4. fa'8(-.\mf fa'-. fa'-.) |
    fa'2 r8\fermata fa'\p |
    mib'( do') sib r mib' r |
    re'4 r8 mib'(\f re' do') |
    sib(\p mib') re' r mib' r |
    re'8 fa'(\f sib' re'' do'' sib') |
    fa'4( mib'8) sol'( fa' mib') |
    re'4( do'8) do'\p( sib mib') |
    re'( sib') sib' r la' r |
    <sib' re'>8 re' r re' r re' |
    r reb' r reb' r reb' |
    r mib' r mib' r mib' |
    r <mib' do'> r q r q |
    r reb' r reb' r reb' |
    r sib4( mib'8 solb' sib') |
    r sib4( reb'8 fa' sib') |
    r solb'4( mib'8 do' reb') |
    reb' r do' r r4\fermata |
    r4 r8 lab!( sib lab) |
    sol8 sib4 sib sib8 |
    lab4 r8 lab( sib lab) |
    sol sib4 sib sib8 |
    lab4( do' fa') |
    r r r8 fa' |
    sib'( sol') mi'4 r8 mi' |
    sol'( mib'!) do'4 r8 mib'\f |
    la'( fad') re'4 r8\fermata r |
    r8 sol'-.\p sib'-. re''-. sol''4 r |
    r8 fa'!-. lab'!-. re''-. fa''4 r |
    r sib'-. mib''-. r |
    r sib'-. re''-. r\fermata |
    sol'4\p r sol |
    lab2 r8 sib |
    do'8( mib') r mib' r fa' |
    mib'4 r8 sib'( lab' sol') |
    mib'4~ mib'8 r fa' r |
    mib'4 r r8 fa' |
    fa'4. fa'8-.( fa'-. fa'-.) |
    fa'4~ fa'8 r r4 |
    R2. |
    <sib re'>8 r q r <do' mib'> r |
    <sib re'>4 r r |
    r8 sol\pp r sol r sol |
    r sol r sol r sol |
    r lab r lab r lab |
    r sib r sib r <do' mib'> |
    r re' r re' r mib' |
    re'4 r r |
    mib'2(\p sol'4) |
    fa'4~ fa'8 r r4 |
    R2. |
    r4 r r8 do' |
    sib r sib r lab r |
    sol4 r r |
    mib'2( sol'4) |
    lab'4. sib8(-.\mf sib-. sib-.) |
    sib4~ sib8\fermata r r4 |
    R2. |
    r4 r r8 do'\p |
    sib r sib r sib r |
    sib4 r8 sol'(\mf fa' mib') |
    do''8.(\p lab'16) sol'4.( fa'8) |
    mib'4 r8 fa'(\f sol' lab') |
    sol'8\p lab'16( fa') mib'8 r re' r |
    mib'4 r r |
    R2. |
    r4 r8 <>^"senza sordino" sib-. mib'-. sol'-. |
    sib'2\fp lab'16( sol' fa' mib') |
    \appoggiatura re'8 do'4 r8 do'-. fa'-. sol'-. |
    lab'(\f fa'\p lab' fa') \appoggiatura sib'16 lab'8 sol'16 fa' |
    mib'8 sol'4 sib' lab'8~ |
    lab'\cresc sol' fa' fa' fa' fa' |
    mib'\f sib( mib' sol' fa' mib') |
    mib'4. do'8( sib lab) |
    <lab re'>4. fa'8( mib' re') |
    mib'4(\p do' lab) |
    sol r r |
  }
>>
