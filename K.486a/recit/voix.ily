\clef "soprano/treble" <>^\markup\character "(Didone)" sib'8 sib' r mib'' re'' re'' r4 |
fa''8. lab'16 lab'8 sib' sol' sol' r4 |
R1 |
sib'8 sib' sib' sib'16 mib'' reb''8 reb'' r16 reb'' reb'' do'' |
lab'8 lab' r4 r2 |
r r4 r8 do'' |
do'' do'' do'' fa'' mib''! mib'' r mib'' |
mib'' mib'' re''! mib'' do'' do'' r16 do'' do'' sib' |
sol'8 sol' r4 r2 |
r r4 r8 sol'16 sol' |
sol''4 re''8 sol' mib'' mib'' r do''16 do'' |
sol''4 mib''8 re'' dod'' dod'' r4 |
r8 sol' sol' fad' la' la' r4 |
