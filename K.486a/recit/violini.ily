\clef "treble" R1 |
r2 r8 <>\p <<
  \tag #'violino1 {
    sol'8( lab' sib') |
    do''8.( sib'16) lab'8 sol' fa' fa'-.( fa'-. fa'-.) |
    sib'1 |
    r8 lab'( sib' do'') reb''8.( do''16) sib'8 lab' |
    sol' sol'-.( sol'-. sol'-.) do''2~ |
    do''1~ |
    do'' |
    r8 sib'( do'' re'') mib''8.( re''16) do''8 sib' |
    lab'! lab'-.( lab'-. lab'-.) sol'2~ |
    sol'4 r r8 do''-. mib''-. r |
    r2 r8 sol''-.\f sib''-. r |
    R1 |
  }
  \tag #'violino2 {
    mib'4 mib'8 |
    mib'2~ mib'8 mib'-. re'-. re'-. |
    mib'2 sol' |
    r8 fa'4 fa'8 fa'2~ |
    fa'8 fa'-. mi'-. mi'-. fa'2~ |
    fa' mib'!~ |
    mib' la' |
    r8 sol'4 sol'8 sol'2~ |
    sol'8 sol'-. fa'-. fa'-. fa'2~ |
    fa'4 r r8 sol'-. sol'-. r |
    r2 r8 dod''-.\f dod''-. r |
    R1 |
  }
>>
