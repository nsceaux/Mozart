Ba -- sta; vin -- ce -- sti: ec -- co -- ti il fo -- glio. 
Ve -- di quan -- to t’a -- do -- ro an -- co -- ra, in -- gra -- to! 
Con un tuo sguar -- do so -- lo 
mi to -- gli o -- gni di -- fe -- sa, e mi dis -- ar -- mi. 
Ed ai cor di tra -- dir -- mi,
ed ai cor di tra -- dir -- mi?
e puoi la -- sciar -- mi?
