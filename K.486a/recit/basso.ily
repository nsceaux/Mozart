\clef "bass" mib1~ |
mib2 r8 mib(\p fa sol) |
lab8.( sib16) do'8 sib la4( lab) |
sol2 mi |
r8 fa( sol lab) sib8.( do'16) reb'8 do' |
si4( sib) la2~ |
la fad~ |
fad1 |
r8 sol( la sib) do'8.( re'16) mib'8 re' |
do'4 re'8 do' si2~ |
si4 r r8 mib-. do-. r |
r2 r8 sol-.\f mib-. r |
R1 |
