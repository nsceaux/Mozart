\clef "alto" R1 |
r2 r8 sib(\p lab sol) |
fa8.( sol16) lab8 sib do'4( sib) |
sib2 reb' |
r8 do'( sib lab) sol8.( lab16) sib8 do' |
re'!4( reb') do'2~ |
do' la'~ |
la' re'! |
r8 re'( do' sib) la8.( sib16) do'8 re' |
mib'4( re') re'2~ |
re'4 r r8 do'-. sol-. r |
r2 r8 sib'-.\f sib-. r |
R1 |
