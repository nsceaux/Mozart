\piecePartSpecs
#`((violino1 #:score-template "score-basse-voix")
   (violino2 #:score-template "score-basse-voix")
   (viola #:score-template "score-basse-voix")
   (basso #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Recitativo: TACET } #}))
