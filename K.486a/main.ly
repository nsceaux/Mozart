\version "2.19.80"
\include "common.ily"

\opusTitle "K.486a – Ah non lasciarmi, no"

\bookpart {
  \header {
    title = \markup\center-column {
      Recitativ und Arie
      \italic Ah non lasciarmi, no
    }
    subtitle = "für Sopran mit Begleitung des Orchesters"
    opus = "K.486a"
    date = "1778"
    copyrightYear = "2019"
  }
  \includeScore "K.486a/recit"
}
\bookpart {
  \paper {
    bookTitleMarkup = ##f
    systems-per-page = 2
  }
  \includeScore "K.486a/aria"
}
