\clef "treble"
<<
  \tag #'violino1 {
    re'8.\p re'16 |
    mi'4.( re'8) mi'4.( fa'8) |
    re'4 <re' si' sol''>4\f r sol'8.\p sol'16 |
    sol'4.( la'16 si') do''4.( re''8) |
    si'16( do'' re'' si') sol'4 r sol''8. sol''16 |
    sol''4.( la''16 si'') do'''4.( re'''8) |
    si''16( do''' re''' si'') sol''4 r2 |
  }
  \tag #'violino2 {
    si8.\p si16 |
    do'4.( si8) do'4.( re'8) |
    si4 <re' si' sol''>4\f r re'8.\p re'16 |
    mi'4.( re'8) mi'4.( fa'8) |
    re'4-. re'-. r si'8. si'16 |
    do''4.( si'8) do''4.( re''8) |
    si'16( do'' re'' si') sol'4-. r2 |
  }
>>
r8 \grace { fa'16( mi' re') } mi'8( sol') mi'-. do''-. sol'-. mi''-. do''-. |
r8 \grace { sol'16( fa' mi') } fa'8( do'') la'-. fa''-. do''-. la''-. do''-. |
si'-. fa'-. mi'-. do''-. re'-. do''-. sol-. si'-. |
<<
  \tag #'violino1 {
    do''4 r r sol'8. sol'16 |
    sol'4.( la'16 si') do''4.( mi''16 re'') |
    do''4( si') r sol''8. sol''16 |
    sol''4.( la''16 si'') do'''4.( mi'''16 re''') |
    do'''4( si'') r sol'8 sol' |
    mi''( re'') re''( do'') do''( re'') re''( mi'') |
    mi''4.( fa''16 re'') do''8-. sol'( do'' re'') |
    mi''( re'') re''( do'') do''( re'') re''( mi'') |
    mi''4.( fa''16 re'') do''8-. mi'( sol' do'') |
    mi''8-. mi''4( do''8) fa''-. fa''4( re''8) |
    sol''4
  }
  \tag #'violino2 {
    do'16( mi' sol' mi' do' mi' sol' mi' do' mi' sol' mi' do' mi' sol' mi') |
    re'( fa' sol' fa' re' fa' sol' fa') do'( mi' sol' mi' do' mi' fa' re') |
    do'( mi' sol' mi') si( re' sol' re' si re' sol' re' si re' sol' re') |
    re'( fa' sol' fa' re' fa' sol' fa') do'( mi' sol' mi' do' mi' fa' re') |
    do'( mi' sol' mi') si( re' sol' re') si4 sol'8 sol' |
    do''( sol') sol'( mi') mi'( sol') sol'( do'') |
    do''4.( re''16 si') do''8-. mi'( sol' si') |
    do''( sol') sol'( mi') mi'( sol') sol'( do'') |
    do''4.( re''16 si') do''8-. do'( mi' sol') |
    do''8-. do''4( sol'8) re''8-. re''4( si'8) |
    mi''4
  }
>> do'8.(\f mi'16) sol'2:8\dotFour |
<<
  \tag #'violino1 {
    sol'8 r mi'\p r fa' r re' r |
    do'8 mi''16( re'' do'' si' la' sol' fad'4) r |
    r8 sol'16( la' si' do'' re'' mi'' fad''4) r |
    r8 si''16( la'' sol'' fad'' mi'' re'' dod''4) r |
    r8 re''16( mi'' fad'' sol'' la'' si'' dod'''4) r |
    re'16( la re' fad' re' la re' fad' re' la re' fad' re' la re' fad') |
    re'16( la re' fad' re' la re' fad' re' la re' fad' re' la re' fad') |
    sol'8-. si'-. re''-. sol''-. r sol''-. si''-. re'''-. |
    r la'-. re''-. la''-. r re''-. la''-. re'''-. |
    si''8 re'4 re' re' re'8( |
    mi') mi'4 mi' mi' mi'8( |
    re') re'4 re' re' re'8( |
    mi') mi'4 mi' mi' mi'8( |
    re') sol'16( fad' sol' fad' sol' fad' sol'4) r |
    r8 si'16( lad' si' lad' si' lad' si'4) r |
    r8 re''16( dod'' re'' dod'' re'' dod'' re''4) r |
    fa''!8-.\fp fa''4( re''8) fa''8-.\fp fa''4( re''8) |
    fa''4\f r r sol'8.\p sol'16 |
    sol'4.( la'16 si') do''4.( mi''16 re'') |
    \appoggiatura do''4 si'2 r4 sol''8. sol''16 |
    sol''4.( la''16 si'') do'''4.( mi'''16 re''') |
    \appoggiatura do'''4 si''2 r4 sol''8. sol''16 |
    sol''4.*2/3( s8\cresc si''16 do''') re'''4.*2/3( s8\f re''16 fa'') |
    mi''4 r mi''-.\p mi''-. |
    fa''-. fa''-. re''-. re''-. |
    mi'' r mi'' mi'' |
    fa'' fa'' re'' re'' |
    \rt#4 { do''16( sol') } \rt#4 { mi''( do'') } |
    <>\cresc \rt#4 { mi''( do'') } \rt#4 { sol''( mi'') } |
    sol''8-.\f do'''16( si'' do''' si'' do''' si'' do'''4) r |
    r8 sol'-.\p do''-. mi''-. sol''4 r |
    r8 sib'-. re''-. fa''-. sib''4 r |
    r8 do'' mi'' sol'' do'''4 r |
  }
  \tag #'violino2 {
    sol'8 r do'\p r re' r si r |
    do'4 r r8 la'16( sol' fad' mi' re' do' |
    si4) r r8 fad'16( sol' la' si' do'' re'' |
    si'4) r r8 mi''16( re'' dod'' si' la' sol' |
    fad'4) r r8 dod''16( re'' mi'' fad'' sol'' la'' |
    fad''4) r la2~ |
    la( re') |
    re'16( si re' sol' re' si re' sol' re' si re' sol' re' si re' sol') |
    re'16( do' re' la' re' do' re' la' re' do' re' la' re' do' re' la') |
    re'8 si16( sol si sol si sol si sol si sol si sol si sol) |
    do'( sol do' sol do' sol do' sol do' sol do' sol do' sol do' sol) |
    si( sol si sol si sol si sol si sol si sol si sol si sol) |
    do'( sol do' sol do' sol do' sol do' sol do' sol do' sol do' sol) |
    si8( si16 lad si lad si lad si4) r |
    r8 sol'16( fad' sol' fad' sol' fad' sol'4) r |
    r8 si'16( lad' si' lad' si' lad' si'4) r |
    si8-.\fp si4( re'8) si8-.\fp si4( re'8) |
    si16(\fp re' sol' re' si re' sol' re' si re' sol' re' si re' sol' re') |
    re'( fa' sol' fa' re' fa' sol' fa') do'( mi' sol' mi' la do' la' do') |
    si16( re' sol' re' si re' sol' re' si re' sol' re' si re' sol' re') |
    re'( fa' sol' fa' re' fa' sol' fa') do'( mi' sol' mi' la do' la' do') |
    si( re' sol' re' si re' sol' re' si re' sol' re' si re' sol' re') |
    re'( fa' sol' fa' re'\cresc fa' sol' fa' re' fa' sol' fa') si(\f re' sol' re') |
    do'4 r do''-.\p do''-. |
    re''-. re''-. si'-. si'-. |
    do''4 r do'' do'' |
    re'' re'' si' si' |
    do''4 r \rt#4 { do''16( sol') } |
    <>\cresc \rt#4 { do''( sol') } \rt#4 { mi''( do'') } |
    mi''8-.\f mi''16( red'' mi'' red'' mi'' red'' mi''4) r |
    r8 mi'-.\p sol'-. do''-. mi''4 r |
    r8 fa'-. sib'-. re''-. fa''4 r |
    r8 sol' do'' mi'' sol''4 r |
  }
>>
r4 r8 fa'-.\f sol'-. la'-. |
sib'-. la'-. sib'-. do''-. re''-. mi''-. |
fa''4 r <<
  \tag #'violino1 {
    do''8( fa'') |
    mi'' mi''4 mi'' mi''8 |
    fa''4
  }
  \tag #'violino2 {
    la'4 |
    sib'8 sib'4 sib' sib'8 |
    la'4
  }
>> r8 fa'-. sol'-. la'-. |
sib'-. la'-. sib'-. do''-. re''-. mi''-. |
fa''4 r <<
  \tag #'violino1 {
    do''8( fa'') |
    mi'' mi''4 mi'' mi''8 |
    fa'' fa'\p fa'2:8 |
    re'2:8 mi'8 mi' |
    re'2:8 mi'8 mi' |
    re'2:8 mi'8 mi' |
    re'8 mi' re' mi'\cresc re' mi' |
    re' mi' re' mi' re' mi' |
    re'\f
  }
  \tag #'violino2 {
    la'4 |
    sib'8 sib'4 sib' sib'8 |
    la' la\p la2:8 |
    si!:8 do'8 do' |
    si2:8 do'8 do' |
    si2:8 do'8 do' |
    si8 do' si do'\cresc si do' |
    si do' si do' si do' |
    si\f
  }
>> <sol sol'>8 q2:8 |
q4 r sol'8 sol' |
<<
  \tag #'violino1 {
    mi''4. fa''8-. sol''-. mi''-. |
    do''4. re''8-. mi''-. do''-. |
    la'4. si'8-. do''-. la'-. |
    fa'4-. fa''-. r |
    r r8 fa''(\p re''' fa'') |
    r8 mi'' mi''2:8 |
    r8 re'' re'' re'' re'' re'' |
    r do'''4\f do''' do'''8 |
    r re'''\p re''' re''' si'' si'' |
    r do'''4\f do''' do'''8 |
    r re'''\p re''' re''' si'' si'' |
    do'''4 r r |
  }
  \tag #'violino2 {
    do''4. re''8-. mi''-. do''-. |
    la'4. si'8-. do''-. la'-. |
    fa'4. sol'8-. la'-. fa'-. |
    re'4-. re''-. r |
    r r8 sol'(\p fa'' sol') |
    r8 do'' do''2:8 |
    r8 do'' do'' do'' si' si' |
    r mi''4\f mi'' mi''8 |
    r fa''\p fa'' fa'' re'' re'' |
    r mi''4\f mi'' mi''8 |
    r fa''\p fa'' fa'' re'' re'' |
    mi''4 r r |
  }
>>
R2.*21 | \allowPageTurn
<<
  \tag #'violino1 {
    r8 la' la'2:8 |
    r8 si' si'2:8 |
    r8 do'' do''2:8 |
    r8 re'' re''2:8 |
    r8 mib'' mib''2:8 |
    mib'2.:16 |
    mib':16 |
    mi'!:16\cresc |
    mi'8( fa'4) fa' fa''8( |
    fad'')\f fad''4 fad'' fad''8( |
    sol''4) r r |
    R2. |
    <sol' mib'' do'''>4 r r |
    R2. |
    <sol' re'' si''>4 r r |
    r r si'8.\p si'16 |
    do''2 do''4 |
    re''2 re''4 |
    sol' r r |
    sol''2 sol''4 |
    lab''( sol'') sol'' |
    r sol'' sol'' |
    lab''( sol'') sol'' |
  }
  \tag #'violino2 {
    r8 do' do'2:8 |
    r8 sold' sold'2:8 |
    r8 la' la'2:8 |
    r8 si' si'2:8 |
    r8 do'' do''2:8 |
    reb'2.:16 |
    do':16 |
    reb':16\cresc |
    reb'8( do'4) do' do''8( |
    mib'')\f mib''4 mib''8( re''! do'') |
    si'!4 r r |
    R2. |
    <mib' do'' sol''>4 r r |
    R2. |
    <re' si' sol''>4 r r |
    r r sol'8.\p sol'16 |
    lab'2 lab'4 |
    fad'2 fad'4 |
    sol' re' re' |
    r re' re' |
    r mib' mib' |
    r mib' mib' |
    r re' re' |
  }
>>
R2.^\fermataMarkup |
<<
  \tag #'violino1 {
    do'2\f mi''4.\fp do''8 |
    sol''-. fa''-. mi''-. re''-. do''-. si'-. la'-. sol'-. |
    <fa' sol>2\f re''4.\fp si'8 |
    fa'' mi'' re'' do'' si' la' sol' fa' |
    mi' mi'\cresc fa' sol' la' sol' la' si' |
    do'' si' do'' re'' mi''\f re'' mi'' fa'' |
    sol''4 r r2 |
    sib''4\p r sib' r |
    la'4 r la'' r |
    do''' r do'' r |
    si'! r si'' r |
    re''' r re'' r |
    do'' r do''' r |
    mi''' r mi'' r |
    re'' r r2 |
    fa''4 r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi' r r2 |
  }
  \tag #'violino2 {
    do'2\f do''4.\fp sol'8 |
    mi''-. re''-. do''-. si'-. la'-. sol'-. fa'-. mi'-. |
    re'2\f si'4.\fp sol'8 |
    re'' do'' si' la' sol' fa' mi' re' |
    do' do'\cresc re' mi' fa' mi' fa' sol' |
    la' sol' la' si' do''\f si' do'' re'' |
    mi''4 r r2 |
    sol''4\p r sol' r |
    fa'4 r fa'' r |
    la'' r la' r |
    sol' r sol'' r |
    si''! r si' r |
    la' r la'' r |
    do''' r do'' r |
    si' r r2 |
    re''4 r re' r |
    do' r do'' r |
    re'' r re' r |
    do' r do'' r |
    re'' r re' r |
    do' r do'' r |
    re'' r re' r |
    do'4 r r2 |  
  }
>>
R1 |
<<
  \tag #'violino1 {
    sol''4\f sol''2 sol''4( |
    sold'') sold''2 sold''4( |
    la'') la''2 la''4~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol' sol' la' si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
    do''\fp sol' sol' sol' mi''8\fp do'' do'' do'' |
    sol''\f fa'' mi'' re'' do'' si' la' sol' |
    fa'4 r r2 |
    R1 |
    fa'8\fp re' re' re' re''\fp si' si' si' |
    fa''\f mi'' re'' do'' si' la' sol' fa' |
    mi'4 r r2 |
    R1 |
    r2 mi'4.\f mi'8\p |
    fa' mi' fa' sol' la' sol' la' si' |
    do'' sol' la'\cresc si' do'' si' do'' re'' |
    mi'' re'' do'' re'' mi''\f re'' mi'' fa'' |
    sol''4 r r2 |
    sib''4\p r sib' r |
    la' r la'' r |
    do''' r do'' r |
    si'! r si'' r |
    re'''4 r re'' r |
    do'' r do''' r |
    mi''' r mi'' r |
    re'' r r2 |
    r <sol' fa'' re'''>4\f r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 re''4\p r |
    fa'' r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi' r mi'' r |
    fa'' r fa' r |
    mi'4 r r2 |
    R1 |
    sol''4\f sol''2 sol''4( |
    sold'') sold''2 sold''4( |
    la'') la''2 la''4~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol' sol' la' si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
  }
  \tag #'violino2 {
    mi''4\f mi''2 mi''4~ |
    mi'' mi''2 mi''4( |
    fa'') fa''2 fa''4~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi' mi' fa' sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
    do''\fp mi' mi' mi' do''\fp mi' mi' mi' |
    mi''8\f re'' do'' si' la' sol' fa' mi' |
    re'4 r r2 |
    R1 |
    re'8\fp si si si si'\fp sol' sol' sol' |
    do''\f si' la' sol' sol' fa' mi' re' |
    do'4 r r2 |
    R1 |
    r2 do'4.\f do'8\p |
    re'8 do' re' mi' fa' mi' fa' re' |
    mi' mi' fa'\cresc sol' la' sol' la' si' |
    do'' si' do'' si' do''\f si' do'' re'' |
    mi''4 r r2 |
    sol''4\p r sol' r |
    fa' r fa'' r |
    la'' r la' r |
    sol' r sol'' r |
    si'' r si' r |
    la' r la'' r |
    do''' r do'' r |
    si' r r2 |
    r <sol' re'' si''>4\f r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 q4 r |
    r2 si'4\p r |
    re'' r re' r |
    do' r do'' r |
    re'' r re' r |
    do' r do'' r |
    re'' r re' r |
    do' r do'' r |
    re'' r re' r |
    do'4 r r2 |
    R1 |
    mi''4\f mi''2 mi''4~ |
    mi'' mi''2 mi''4( |
    fa'') fa''2 fa''4~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi' mi' fa' sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
  }
>>
do''4 fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { la''16( sol'' fa'' } |
mi''4) fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { fa''16( mi'' re'' } |
do''4) r mi''-. do''-. |
fa''-. re''-. sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol'' <<
  \tag #'violino1 {
    <sol' re'' si''>4 |
    <sol' mi'' do'''>4. do'''8 do'''2:8 |
    do'''4. sol''8 sol''2:8 |
    sol''4. mi''8 mi''2:8 |
    mi''4 sol'' mi'' sol'' |
    mi''2
  }
  \tag #'violino2 {
    <re' si' sol''>4 |
    <mi' do'' mi''>4. mi''8 mi''2:8 |
    mi''4. mi''8 mi''2:8 |
    mi''4. do''8 do''2:8 |
    do''4 si' do'' si' |
    do''2
  }
>> r4 r8 \tuplet 3/2 { sol16( la si } |
do'4) r8 \tuplet 3/2 { sol16( la si } do'4) r8 \tuplet 3/2 { sol16( la si } |
do'2) r |
