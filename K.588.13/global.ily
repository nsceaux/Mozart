\tag #'all \key do \major
\tempo "Allegro" \midiTempo#120
\time 4/4 \partial 4 s4 s1*53 \bar "||"
\tempo "Allegro" \time 3/4 s2.*73 \bar "||"
\tempo "Molto Allegro" \midiTempo#160
\time 2/2 s1*93 \bar "|."
