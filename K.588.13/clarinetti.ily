\clef "treble" r4 |
R1*13 |
r2 r4 <>\p \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol'8 sol' | }
  { sol'8 sol' | }
>>
<<
  \tag #'(clarinetto1 clarinetti) {
    mi''8( re'') re''( do'') do''( re'') re''( mi'') |
  }
  \tag #'(clarinetto2 clarinetti) {
    do''8( sol') sol'( mi') mi'( sol') sol'( do'') |
  }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mi''4.( fa''16 re'') do''8-. sol'( do'' re'') | }
  { do''4.( sol'8) mi'4 mi'8( sol') | }
>>
<<
  \tag #'(clarinetto1 clarinetti) {
    mi''8( re'') re''( do'') do''( re'') re''( mi'') |
  }
  \tag #'(clarinetto2 clarinetti) {
    do''8( sol') sol'( mi') mi'( sol') sol'( do'') |
  }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mi''4.( fa''16 re'') }
  { do''4.( sol'8) }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    do''8-. mi'( sol' do'') |
    mi''2( fa'') |
    sol''1~ |
    sol''4.( mi''8)
  }
  \tag #'(clarinetto2 clarinetti) {
    mi'8-. do'( mi' sol') |
    do''2( re'') |
    mi''1~ |
    mi''4.( do''8)
  }
  { s2 | s1 | s\f | s4 s\p }
>> \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''8.( do''16) re''8.( mi''16) | }
  { sol'8 r sol' r | }
>>
<<
  \tag #'(clarinetto1 clarinetti) { do''4 }
  \tag #'(clarinetto2 clarinetti) { mi' }
>> r4 r2 |
R1*14 |
<<
  \tag #'(clarinetto1 clarinetti) { re''2 re'' | re''4 }
  \tag #'(clarinetto2 clarinetti) { si'2 si' | si'4 }
  { s2\fp s\fp | s4\f }
>> r4 r2 |
R1*9 |
<<
  \tag #'(clarinetto1 clarinetti) {
    mi'1 |
    sol'~ |
    sol'8 sol' sol' sol' sol'4
  }
  \tag #'(clarinetto2 clarinetti) {
    do'1 |
    mi'~ |
    mi'8 mi' mi' mi' mi'4
  }
  { s1 | s\cresc | s2\f }
>> r4 |
R1*3 |
R2.*27 |
<>\p <<
  \tag #'(clarinetto1 clarinetti) {
    mi'4( sol' do'') |
    mi''2.~ |
    mi''4( re'') re'' |
    do''2 do''4 |
    do''( si')
  }
  \tag #'(clarinetto2 clarinetti) {
    do'4( mi' sol') |
    do''2.~ |
    do''4( si') si' |
    la'2 la'4 |
    la'( sold')
  }
>> r4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''4( mi'' red'') |
    red''( mi'') mi'' |
    mi''( re''! dod'') |
    dod''( re'') re'' |
    do''!( si' la') |
    sold' }
  { do''2.~ |
    do''4 si' sib' |
    sib'2.~ |
    sib'4 la' la' |
    red'2. |
    mi'4 }
>> r4 r |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sold'4( si' re'') | }
  { R2. }
>>
<<
  \tag #'(clarinetto1 clarinetti) {
    fa''2.~ |
    fa''2~ fa''8( mi'') |
    mi''4
  }
  \tag #'(clarinetto2 clarinetti) {
    si'2.~ |
    si'2~ si'8( do'') |
    do''4
  }
>> r4 r |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sib'4( re'' fa'' | sib' re'' fa'') | }
  { fa'2.~ | fa' | }
>>
<<
  \tag #'(clarinetto1 clarinetti) { la'2( sold'4) | la' }
  \tag #'(clarinetto2 clarinetti) { mi'2( re'4) | do' }
>> r4 r |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { fa''4( re'' sib' | fa'' re'' sib') | }
  { fa'2.~ | fa' | }
>>
<<
  \tag #'(clarinetto1 clarinetti) { la'2( sold'4) | la' }
  \tag #'(clarinetto2 clarinetti) { mi'2( re'4) | do' }
>> r4 r |
R2.*9 | \allowPageTurn
<<
  \tag #'(clarinetto1 clarinetti) {
    r4 \tag #'clarinetti <>^\markup\concat { 1 \super o }
    sol'\p sol' |
    sol'( fad' sol') |
    r sol' sol' |
    sol'( lab' sol') |
    r si' si' |
    si'( do'' si') |
    R2.*3
  }
  \tag #'clarinetto2 R2.*9
>>
<>\p <<
  \tag #'(clarinetto1 clarinetti) { re''2. | mib''~ | mib'' | re''4 }
  \tag #'(clarinetto2 clarinetti) { si'2. | do''~ | do'' | si'4 }
>> r4 r |
R2.^\fermataMarkup |
<>\f \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do'2 } { do' }
>> <<
  \tag #'(clarinetto1 clarinetti) {
    mi''4. do''8 |
    sol''-. fa''-. mi''-. re''-. do''-. si'-. la'-. sol'-. |
    fa'2 re''4. si'8 |
    fa'' mi'' re'' do'' si' la' sol' fa' |
    mi'4 fa'8 sol' la' sol' la' si' |
    do'' si' do'' re'' mi'' re'' mi'' fa'' |
    sol''4
  }
  \tag #'(clarinetto2 clarinetti) {
    do''4. sol'8 |
    mi''-. re''-. do''-. si'-. la'-. sol'-. fa'-. mi'-. |
    re'2 si'4. sol'8 |
    re'' do'' si' la' sol' fa' mi' re' |
    do'4 re'8 mi' fa' mi' fa' sol' |
    la' sol' la' si' do'' si' do'' re'' |
    mi''4
  }
  { s2\fp | s1 | s2\f s\fp | s1 | s4 s2.\cresc | s2 s\f | }
>> r4 <<
  \tag #'(clarinetto1 clarinetti) {
    \tag #'clarinetti <>^\markup\concat { 1 \super o }
    do''4\p do'' |
    re''( do'') do'' r |
    r2 do''4 do'' |
    do''( dod'' re'') r |
    r2 re''4 re'' |
    re''( red'' mi'') r |
    r mi'' mi'' mi'' |
    mi''2( fa''4) r |
    R1*2 |
    r4 \tag #'clarinetti <>^\markup\concat { 1 \super o }
    mi''2\p re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'8( mi'') mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'8( mi'') mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'
  }
  \tag #'clarinetto2 { r2 | R1*15 | r4 }
>> <>\f <<
  \tag #'(clarinetto1 clarinetti) {
    re''4 mi'' re'' |
    mi''4 re'' mi'' re'' |
    sol''1 |
    sold'' |
    la''~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol'4 la'8 si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
  }
  \tag #'(clarinetto2 clarinetti) {
    si'4 do'' si' |
    do'' si' do'' si' |
    mi''1~ |
    mi'' |
    fa''~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi'4 fa'8 sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
  }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''2 } { do'' }
>>
<<
  \tag #'(clarinetto1 clarinetti) {
    mi''2 |
    sol''8 fa'' mi'' re'' do'' si' la' sol' |
    fa'4
  }
  \tag #'(clarinetto2 clarinetti) {
    do''2 |
    mi''8 re'' do'' si' la' sol' fa' mi' |
    re'4
  }
>> r4 r2 |
R1 |
<<
  \tag #'(clarinetto1 clarinetti) {
    si'2 re'' |
    fa''8 mi'' re'' do'' si' la' sol' fa' |
    mi'4
  }
  \tag #'(clarinetto2 clarinetti) {
    re'2 si' |
    re''8 do'' si' la' sol' fa' mi' re' |
    do'4
  }
>> r4 r2 |
R1 |
r2 <<
  \tag #'(clarinetto1 clarinetti) {
    mi'4. mi'8 |
    fa'8 mi' fa' sol' la' sol' la' si' |
  }
  \tag #'(clarinetto2 clarinetti) {
    do'4. do'8 |
    re' do' re' mi' fa' mi' fa' re' |
  }
  { s4.\f s8\p | }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''8 sol' la' si' }
  { mi'4 la'8 si' }
  { s4 s\cresc }
>> <<
  \tag #'(clarinetto1 clarinetti) { do''8 si' do'' re'' | }
  \tag #'(clarinetto2 clarinetti) { la' sol' la' si' | }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mi''8 re'' do'' re'' }
  { do''8 si' do'' si' }
>> <>\f <<
  \tag #'(clarinetto1 clarinetti) { mi''8 re'' mi'' fa'' | sol''4 }
  \tag #'(clarinetto2 clarinetti) { do''8 si' do'' re'' | mi''4 }
>> r4 
<<
  \tag #'(clarinetto1 clarinetti) {
    \tag #'clarinetti <>^\markup\concat { 1 \super o }
    do''4\p do'' |
    re''( do'') do'' r |
    r2 do''4 do'' |
    do''( dod'' re'') r |
    r2 re''4 re'' |
    re''( red'' mi'') r |
    r mi'' mi'' mi'' |
    mi''2( fa''4) r |
    R1 |
  }
  \tag #'clarinetto2 { r2 | R1*8 }
>>
<<
  \tag #'(clarinetto1 clarinetti) \ru#7 { s2 si'4 s | }
  \tag #'(clarinetto2 clarinetti) \ru#7 { s2 fa'4 s | }
  { r2 s4\f r | \ru#6 { r2 s4 r | } }
>>
R1*2 |
<<
  \tag #'(clarinetto1 clarinetti) {
    r4 \tag #'clarinetti <>^\markup\concat { 1 \super o }
    mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'8( mi'') mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'8( mi'') mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8 sol' sol'4 sol' |
    sol'
  }
  \tag #'clarinetto2 { R1*6 | r4 }
>> <>\f <<
  \tag #'(clarinetto1 clarinetti) {
    re''4 mi'' re'' |
    mi'' re'' mi'' re'' |
    sol''1 |
    sold'' |
    la''~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol'4 la'8 si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
  }
  \tag #'(clarinetto2 clarinetti) {
    si'4 do'' si' |
    do'' si' do'' si' |
    mi''1~ |
    mi'' |
    fa''~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi'4 fa'8 sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
  }
>>
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''4 } { do'' }
>> \tag #'clarinetti <>^"a 2." fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { la''16( sol'' fa'' } |
mi''4) fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { fa''16( mi'' re'' } |
do''4) r mi''-. do''-. |
fa''-. re''-. sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol'' <<
  \tag #'(clarinetto1 clarinetti) {
    si''4 |
    do'''4. do'''8 do'''2:8 |
    do'''4. sol''8 sol''2:8 |
    sol''4. mi''8 mi''2:8 |
    mi''4 sol'' mi'' sol'' |
    mi''2
  }
  \tag #'(clarinetto2 clarinetti) {
    re''4 |
    mi''4. mi''8 mi''2:8 |
    mi''4. mi''8 mi''2:8 |
    mi''4. do''8 do''2:8 |
    do''4 si' do'' si' |
    do''2
  }
>> \tag#'clarinetti <>^"a 2." r4 r8 \tuplet 3/2 { sol'16( la' si' } |
do''4) r8 \tuplet 3/2 { sol'16( la' si' } do''4) r8 \tuplet 3/2 { sol'16( la' si' } |
do''2) r |
