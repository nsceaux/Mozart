<<
  \tag #'voix1 {
    \clef "soprano/treble" r4 |
    R1*47 |
    r2 mi''4. mi''8 |
    mi''4 do''8. sol''16 sol''4. sol''8 |
    sol''4 mi'' r2 |
    R1*3 |
    R2.*2 |
    r4 r do''8 fa'' |
    mi''4. mi''8 mi'' mi'' |
    fa''4 do'' r |
    R2. |
    r4 r do''8 fa'' |
    mi''4. mi''8 mi'' mi'' |
    fa''4 do'' fa''8 fa'' |
    re''4. re''8 mi'' mi'' |
    re''4. re''8 mi'' mi'' |
    re''4. re''8 mi'' mi'' |
    re''([ mi'']) re'' mi'' re'' mi'' |
    re''([ mi'']) re'' mi'' re'' mi'' |
    re''4 re'' r |
    r r sol'8 sol' |
    mi''4. fa''8 sol'' mi'' |
    do''4. re''8 mi'' do'' |
    la'4. si'8 do'' la' |
    re'4 fa'' r |
    r r fa''8 fa'' |
    mi''2 mi''4 |
    re''2 re''4 |
    mi'' r mi''8 mi'' |
    fa''4. fa''8 re'' re'' |
    mi''4 r mi''8 mi'' |
    fa''4. fa''8 re'' re'' |
    do''4 r r |
    R2.*22 |
    si'2 si'4 |
    si'8([ do'']) do''2 |
    re''2 re''4 |
    re''8([ mib'']) mib''4 r |
    mib''2 mib''4 |
    mib''2 mib''4 |
    mi''!2 mi''4 |
    mi''8([ fa'']) fa''4 fa''8 fa'' |
    fad''4. fad''8 fad'' fad'' |
    sol''4 r r |
    R2. |
    mib''4 r r |
    R2. |
    re''4 r r |
    r r si'8 si' |
    do''2 do''4 |
    re''2 re''4 |
    sol' r r |
    R2.*4 |
    R2.^\fermataMarkup |
    r2 mi''4. do''8 |
    sol''([ fa'']) mi''([ re'']) do''([ si']) la'([ sol']) |
    fa'4 fa' re''4. si'8 |
    fa''([ mi'']) re''([ do'']) si'([ la']) sol'([ fa']) |
    mi'4 fa'8([ sol']) la'([ sol']) la'([ si']) |
    do''([ si']) do''([ re'']) mi''([ re'']) mi''([ fa'']) |
    sol''4 r r2 |
    R1*8 |
    <>^\markup\italic sotto vocce fa''2. fa''4 |
    mi''4 r r mi'' |
    fa''2. fa''4 |
    mi'' mi'' r2 |
    fa''2. fa''4 |
    mi'' r mi'' r |
    fa'' r fa'' r |
    mi'' r r2 |
    R1 |
    sol''1^\f |
    sold'' |
    la''~ |
    la''8([ sol''!]) fa''([ mi'']) re''([ do'']) si'([ la']) |
    sol'4 la'8([ si']) do''([ re'']) mi''([ fa'']) |
    sol''([ mi'']) sol''([ mi'']) fa''([ re'']) fa''([ re'']) |
    do''4 r mi''4. do''8 |
    sol''8([ fa'']) mi''([ re'']) do''([ si']) la'([ sol']) |
    fa'4 fa' r2 |
    R1 |
    r2 re''4. si'8 |
    fa''([ mi'']) re''([ do'']) si'([ la']) sol'([ fa']) |
    mi'4 mi' r2 |
    R1 |
    r2 mi'4. mi'8 |
    fa'8([ mi']) fa'([ sol']) la'([ sol']) la'([ si']) |
    do''([ sol']) la'([ si']) do''([ si']) do''([ re'']) |
    mi''([ re'']) do''([ re'']) mi''([ re'']) mi''([ fa'']) |
    sol''4 r r2 |
    r2 <>^\markup\italic sotto vocce do''4 do'' |
    do''4. sib'8 la'([ sol']) fa'([ mi']) |
    re'4 re' r2 |
    R1 |
    r2 mi''4 mi'' |
    mi''4. re''8 do''([ si'!]) la'([ sol']) |
    fa'4 r r2 |
    R1 |
    r2 fa''8([^\f mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 sol' fa''8([ mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 sol' fa''8([ mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 r r2 |
    R1 |
    <>^\markup\italic sotto vocce fa''2. fa''4 |
    mi''4 r r mi'' |
    fa''2. fa''4 |
    mi''4 mi'' r2 |
    fa''2. fa''4 |
    mi'' r mi'' r |
    fa'' r fa'' r |
    mi'' r r2 |
    R1 |
    sol''1^\f |
    sold'' |
    la''~ |
    la''8([ sol''!]) fa''([ mi'']) re''([ do'']) si'([ la']) |
    sol'4 la'8([ si']) do''([ re'']) mi''([ fa'']) |
    sol''([ mi'']) sol''([ mi'']) fa''([ re'']) fa''([ re'']) |
    do''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    mi''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    do''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol' |
    do'' r r2 |
    R1*6 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" r4 |
    R1*47 |
    r2 do''4. do''8 |
    do''4 sol'8. mi''16 mi''4. mi''8 |
    mi''4 do'' r2 |
    R1*3 |
    R2.*2 |
    r4 r la'8 la' |
    sib'4. sib'8 sib' sib' |
    la'4 la' r |
    R2. |
    r4 r la'8 la' |
    sib'4. sib'8 sib' sib' |
    la'4 la' la'8 la' |
    si'!4. si'8 do'' do'' |
    si'4. si'8 do'' do'' |
    si'4. si'8 do'' do'' |
    si'([ do'']) si' do'' si' do'' |
    si'([ do'']) si' do'' si' do'' |
    si'4 si' r |
    r r sol'8 sol' |
    do''4. re''8 mi'' do'' |
    la'4. si'8 do'' la' |
    fa'4. sol'8 la' fa' |
    re'4 re'' r |
    r r re''8 re'' |
    do''2 do''4 |
    do''2 si'4 |
    do'' r do''8 do'' |
    re''4. re''8 si' si' |
    do''4 r do''8 do'' |
    re''4. re''8 si' si' |
    do''4 r r |
    R2.*22 |
    sold'2 sold'4 |
    sold'8([ la']) la'2 |
    si'2 si'4 |
    si'8([ do'']) do''4 r |
    reb''2 reb''4 |
    do''2 do''4 |
    reb''2 reb''4 |
    reb''8([ do'']) do''4 do''8 do'' |
    mib''4. mib''8 re''! do'' |
    si'!4 r r |
    R2. |
    do''4 r r |
    R2. |
    si'4 r r |
    r r sol'8 sol' |
    lab'2 lab'4 |
    fad'2 fad'4 |
    sol' r r |
    R2.*4 |
    R2.^\fermataMarkup |
    r2 do''4. sol'8 |
    mi''8([ re'']) do''([ si']) la'([ sol']) fa'([ mi']) |
    re'4 re' si'4. sol'8 |
    re''8([ do'']) si'([ la']) sol'([ fa']) mi'([ re']) |
    do'4 re'8([ mi']) fa'([ mi']) fa'([ sol']) |
    la'([ sol']) la'([ si']) do''([ si']) do''([ re'']) |
    mi''4 r r2 |
    R1*8 |
    <>^\markup\italic sotto vocce re''2. re''4 |
    do''4 r r do'' |
    re''2. re''4 |
    do'' do'' r2 |
    re''2. re''4 |
    do'' r do'' r |
    re'' r re'' r |
    do'' r r2 |
    R1 |
    mi''1^\f |
    mi'' |
    fa''~ |
    fa''8([ mi'']) re''([ do'']) si'([ la']) sol'([ fa']) |
    mi'4 fa'8([ sol']) la'([ si']) do''([ re'']) |
    mi''([ do'']) mi''([ do'']) re''([ si']) re''([ si']) |
    do''4 r do''4. sol'8 |
    mi''8([ re'']) do''([ si']) la'([ sol']) fa'([ mi']) |
    re'4 re' r2 |
    R1 |
    r2 si'4. sol'8 |
    re''([ do'']) si'([ la']) sol'([ fa']) mi'([ re']) |
    do'4 do' r2 |
    R1 |
    r2 do'4. do'8 |
    re'8([ do']) re'([ mi']) fa'([ mi']) fa'([ re']) |
    mi'4 fa'8([ sol']) la'([ sol']) la'([ si']) |
    do''4 do''8([ si']) do''([ si']) do''([ re'']) |
    mi''4 r r2 |
    R1*2 |
    r2 <>^\markup\italic sotto voce re''4 re'' |
    re''4. do''8 si'!([ la']) sol'([ fa']) |
    mi'4 mi' r2 |
    R1 |
    r2 fa''4 fa'' |
    fa''4. mi''8 re''([ do'']) si'([ la']) |
    sol'4 r r2 |
    r fa''8([^\f  mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 sol' fa''8([ mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 sol' fa''8([ mi'']) re''([ do'']) |
    si'4 si'8([ do'']) re''([ do'']) si'([ la']) |
    sol'4 r r2 |
    <>^\markup\italic sotto vocce re''2. re''4 |
    do''4 r r do'' |
    re''2. re''4 |
    do''4 do'' r2 |
    re''2. re''4 |
    do'' r do'' r |
    re'' r re'' r |
    do'' r r2 |
    R1 |
    mi''1^\f |
    mi'' |
    fa''~ |
    fa''8([ mi'']) re''([ do'']) si'([ la']) sol'([ fa']) |
    mi'4 fa'8([ sol']) la'([ si']) do''([ re'']) |
    mi''([ do'']) mi''([ do'']) re''([ si']) re''([ si']) |
    do''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    mi''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    do''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol' |
    do'' r r2 |
    R1*6 |
  }
  \tag #'voix3 {
    \clef "soprano/treble" r4 |
    R1*21 |
    r2 r4 fad'8. fad'16 |
    sol'4 sol' r do''8. re''16 |
    si'4 si' r sol'8. fad'16 |
    la'4 la' r dod''8. dod''16 |
    re''8 re'' re'' re'' re'' re'' re'' re'' |
    re'' fad' re'' re'' re'' re'' re'' re'' |
    sol'4 r8 re'' re'' sol' r4 |
    re''8 la' r4 re''8 la' r re'' |
    si' sol' r4 r2 |
    R1 |
    r2 r4 sol'8 sol' |
    do''4. sol'8 do''4. mi''8 |
    re''8([ si']) sol'4 r sol'8 la' |
    si'4 si' r si'8 do'' |
    re''4 re'' r re''8 re'' |
    fa''4. re''8 fa''4. re''8 |
    fa''4 fa''8 mi'' re'' do'' si' la' |
    sol' sol' r4 r2 |
    r4 fa''8 mi'' re'' do'' si' la' |
    sol' sol' r4 r2 |
    r4 fa''8 mi'' re'' do'' si' la' |
    sol'4 r r2 |
    r4 mi''8 sol'' do'' mi'' la' do'' |
    fa' fa' re'' fa'' si' re'' sol' sol'' |
    mi''4 mi''8 sol'' do'' mi'' la' do'' |
    fa' fa' re'' fa'' si' re'' sol' si' |
    do''4 r r2 |
    R1 |
    r2 r4 sol'8. sol'16 |
    do''8 do'' r4 r2 |
    R1*2 |
    R2.*28 |
    mi''2 mi''4 |
    mi''( re'') re'' |
    r do'' do'' |
    do'' si' r |
    fa''( mi'') red'' |
    red''( mi'') mi'' |
    mi''( re''!) dod'' |
    dod''( re'') re'' |
    do''!( si') la' |
    sold' r r |
    sold' si' re'' |
    fa''2.~ |
    fa''2~ fa''8([ mi'']) |
    mi''4 r r |
    sib'( re'') fa'' |
    sib'( re'') fa'' |
    la'2 sold'4 |
    la' r r |
    fa''( re'') sib' |
    fa''( re'') sib' |
    la'2 sold'4 |
    la' r r |
    R2.*9 |
    r4 sol' sol' |
    sol'( fad') sol' |
    r sol' sol' |
    sol'( lab') sol' |
    r si' si' |
    si'( do'') si' |
    R2.*3 |
    r4 sol' sol' |
    lab'( sol') sol' |
    r sol' sol' |
    lab'( sol') sol' |
    R2.^\fermataMarkup |
    R1*6 |
    r2 <>^\markup\italic sotto vocce do''4 do'' |
    re''( do'') do'' r |
    r2 do''4 do'' |
    do''( dod'') re'' r |
    r2 re''4 re'' |
    re''( red'') mi'' r |
    r mi'' mi'' mi'' |
    mi''2( fa''4) r |
    R1*2 |
    r4 mi''2 re''8([ do'']) |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol'8([ mi'']) mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol'8([ mi'']) mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol' r r2 |
    R1*2 |
    do''1^\f~ |
    do''2 dod'' |
    re''8([ mi'']) fa''([ mi'']) re''([ do''!]) si'([ la']) |
    sol'4 la'8([ si']) do''([ re'']) mi''([ fa'']) |
    sol''([ mi'']) sol''([ mi'']) fa''([ re'']) fa''([ re'']) |
    do''4 r r2 |
    R1 |
    r2 fa'4^\p mi' |
    fa' mi' fa' mi' |
    fa' fa' r2 |
    R1 |
    r2 mi'4 fa' |
    mi' fa' mi' fa' |
    mi' r r2 |
    R1*3 |
    r2 <>^\markup\italic sotto voce do''4 do'' |
    re''( do'') do'' r |
    r2 do''4 do'' |
    do''( dod'') re'' r |
    r2 re''4 re'' |
    re''4( red'') mi'' r |
    r mi'' mi'' mi'' |
    mi''2( fa''4) r |
    r2 si'4. si'8 |
    re''4 r si' r |
    r2 si'4. si'8 |
    re''4 r si' r |
    r2 si'4.*2/3 s8^\cresc si'8 |
    re''4 r si' r |
    r si'^\f si' si' |
    re''2 r |
    R1*2 |
    r4 <>^\markup\italic sotto vocce mi''2 re''8([ do'']) |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol'8([ mi'']) mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol'8([ mi'']) mi''2 re''8 do'' |
    \appoggiatura do''8 si'4 la'8([ sol']) sol'4 sol' |
    sol' r r2 |
    R1*2 |
    do''1^\f~ |
    do''2 dod'' |
    re''8([ mi'']) fa''([ mi'']) re''([ do''!]) si'([ la']) |
    sol'4 la'8([ si']) do''([ re'']) mi''([ fa'']) |
    sol''([ mi'']) sol''([ mi'']) fa''([ re'']) fa''([ re'']) |
    do''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    mi''4 fad''2 sol''4 |
    sold'' la'' r fa''! |
    re''2 sol'' |
    do''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol''8([ fa'']) |
    mi''4 r mi'' do'' |
    fa'' re'' sol'' sol' |
    do'' r r2 |
    R1*6 |
  }
  \tag #'voix4 {
    \clef "tenor/G_8" r4 |
    R1*10 |
    r2 mi'4. fa'8 |
    mi'4( re') r2 |
    r4 r8 re' mi'4. fa'8 |
    mi'4 re' r sol8 sol |
    mi'([ re']) re'([ do']) do'([ re']) re'([ mi']) |
    mi'4.( fa'16[ re']) do'4 sol8 sol |
    mi'([ re']) re'([ do']) do'([ re']) re'([ mi']) |
    mi'4.( fa'16[ re']) do'4 r4 |
    mi'4.( do'8) fa'4.( re'8) |
    sol'1~ |
    sol'4. mi'8 re'8.([ do'16]) re'8.([ mi'16]) |
    do'4 r r2 |
    R1*15 |
    r2 r4 si8 do' |
    re'4. re'8 mi'4. fa'8 |
    re' re' r4 r si8 do' |
    re'4. re'8 mi'4. fa'8 |
    re'8 re' r4 r si8^\f do' |
    re'4. re'8 sol'4. sol'8 |
    mi'4 r mi'^\p mi' |
    fa' fa' re' re' |
    mi'4 r mi' mi' |
    fa' fa' re' re' |
    do' r r2 |
    R1*5 |
    R2.*28 |
    do'2 do'4 |
    do'( si) si |
    r la la |
    la sold r |
    do'2 do'4 |
    do'( si) sib |
    sib2 sib4 |
    sib( la) la |
    red'2 red'4 |
    mi' r r |
    fa' re'! si |
    sold2.~ |
    sold2~ sold8([ la]) |
    la4 r r |
    fa'( re') sib |
    fa'( re') sib |
    do'2 si!4 |
    la r r |
    sib( re') fa' |
    sib( re') fa' |
    do'2 si!4 |
    la r r |
    R2.*9 |
    r4 si si |
    si( do') si |
    r mib' mib' |
    mib'( fa') mib' |
    r re' re' |
    re'( mib') re' |
    R2.*3 |
    re'2 re'4 |
    mib'2 mib'4 |
    mib'2 mib'4 |
    re' re' r |
    R2.^\fermataMarkup |
    R1*7 |
    r4 <>^\markup\italic sotto vocce mi( sol) sib4 |
    la4( fa) do'2 |
    r4 fad la do' |
    si!( sol) re'2 |
    r4 sold( si) re' |
    do'( la) mi'2 |
    r4 la do' mi' |
    re'( si fa'2) |
    R1*3 |
    r2 mi'4. mi'8 |
    fa'4 r re' r |
    r mi' mi' sol' |
    fa' re' r re'8^\f re' |
    mi'4 re' mi' re' |
    mi'4 re' mi' re' |
    mi' r r2 |
    do'1~ |
    do'2 dod' |
    re'4 re' re' re' |
    mi'4. mi'8 mi'4. mi'8 |
    mi'4. mi'8 re'4. re'8 |
    do'4 r r2 |
    R1 |
    r2 re'4^\p do' |
    re' do' re' do' |
    re' re' r2 |
    R1 |
    r2 do'4 re' |
    do' re' do' re' |
    do' r r2 |
    R1*4 |
    r4 <>^\markup\italic sotto vocce mi4( sol) sib |
    la( fa) do'2 |
    r4 fad la do' |
    si!( sol) re'2 |
    r4 sold( si) re' |
    do'( la) mi'2 |
    r4 la do' mi' |
    re'( si fa'2) |
    r re'4. re'8 |
    fa'4 r re' r |
    r re' re' re' |
    fa'4 r re' r |
    r2 re'4.^\cresc re'8 |
    fa'4 r re' r |
    r re'^\f re' re' |
    fa'2 r |
    R1*3 |
    r2 <>^\markup\italic sotto vocce mi'4. mi'8 |
    fa'4 r re' r |
    r mi' mi' sol' |
    fa' re' r re'8^\f re' |
    mi'4 re' mi' re' |
    mi' re' mi' re' |
    mi'4 r r2 |
    do'1~ |
    do'2 dod' |
    re'4 re' re' re' |
    mi'4. mi'8 mi'4. mi'8 |
    mi'4. mi'8 re'4. re'8 |
    do'4 fad'2 sol'4 |
    sold' la' r fa'! |
    re'2 sol' |
    mi'4 fad'2 sol'4 |
    sold' la' r fa'! |
    re'2 sol' |
    do'4 r mi' do' |
    fa' re' sol' sol'8([ fa']) |
    mi'4 r mi' do' |
    fa' re' sol' sol'8([ fa']) |
    mi'4 r mi' do' |
    fa' re' sol' sol |
    do' r r2 |
    R1*6 |
  }
  \tag #'voix5 {
    \clef "bass" r4 |
    R1 |
    r2 r4 sol8. sol16 |
    do'4. si8 do'4. re'8 |
    si4 sol r sol8. sol16 |
    do'4. si8 do'4. re'8 |
    si4 sol r sol8. sol16 |
    do'4 do' do'4. do'8 |
    la4 fa r fa8. la16 |
    sol4 sol sol4. la16([ si]) |
    do'4 r r2 |
    R1*19 |
    r2 r4 sol8 sol |
    do'4. sol8 do'4. mi'8 |
    re'8.([ si16]) sol4 r2 |
    R1*5 |
    r2 r4 sol8 la |
    si4. si8 do'4. re'8 |
    si8 si r4 r sol8 la |
    si4. si8 do'4. re'8 |
    si8 si r4 r sol8^\f la |
    si4. si8 re'4 si |
    do'4 r do'^\p do' |
    re' re' si si |
    do'4 r do' do' |
    re' re' si si |
    do' r r2 |
    R1*2 |
    r2 r8 do' do' do' |
    sib sib r4 r sib8 sib |
    sol sol r4 r8 sol do' do' |
    la4 fa r |
    R2.*71 |
    R2.^\fermataMarkup |
    R1*7 |
    <>^\markup\italic sotto vocce mi2 r4 mi |
    fa4 fa r2 |
    fad2 r4 fad |
    sol sol r2 |
    sold2. sold4 |
    la2. la4 |
    la2. la4 |
    si r sol sol |
    sol sol sol la8([ si]) |
    do'4 sol sol sol |
    sol sol sol la8([ si]) |
    do'4( sol) sol sol |
    sol sol sol la8([ si]) |
    do'4 sol sol sol |
    sol sol sol la8([ si]) |
    do'4 r r2 |
    R1 |
    do'1^\f~ |
    do'2 do' |
    la la |
    la2. la4 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 si4. si8 |
    do'4 r r2 |
    R1 |
    r2 si4^\p do' |
    si do' si do' |
    si si r2 |
    R1 |
    r2 sol4 sol |
    sol sol sol sol |
    sol r r2 |
    R1*4 |
    <>^\markup\italic sotto vocce mi2 r4 mi |
    fa fa r2 |
    fad2 r4 fad |
    sol sol r2 |
    sold2. sold4 |
    la2. la4 |
    la2. la4 |
    si r sol!4. sol8 |
    si4 r sol r |
    r2 sol4. sol8 |
    si4 r sol r |
    r2 sol4.*2/3 s8^\cresc sol8 |
    si4 r sol r |
    r sol^\f sol sol |
    si2 r |
    r <>^\markup\italic sotto vocce sol4 sol |
    sol sol sol la8([ si]) |
    do'4 sol sol sol |
    sol sol sol la8([ si]) |
    do'4( sol) sol sol |
    sol sol sol la8([ si]) |
    do'4 sol sol sol |
    sol sol sol la8([ si]) |
    do'4 r r2 |
    R1 |
    do'1^\f~ |
    do'2 do' |
    la la |
    la2. la4 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 si4. si8 |
    do'4 fad2 sol4 |
    sold la r fa! |
    re2 sol |
    mi4 fad2 sol4 |
    sold la r fa! |
    re2 sol |
    do4 r mi do |
    fa re sol sol8([ fa]) |
    mi4 r mi do |
    fa re sol sol8([ fa]) |
    mi4 r mi do |
    fa re sol sol |
    do r r2 |
    R1*6 |
  }
  \tag #'voix6 {
    \clef "bass" r4 |
    R1*10 |
    r2 do'4. re'8 |
    do'4( si) r2 |
    r4 r8 si do'4. re'8 |
    do'4 si r sol8 sol |
    do'([ sol]) sol([ mi]) mi([ sol]) sol([ do']) |
    do'4.( sol8) mi4 sol8 sol |
    do'([ sol]) sol([ mi]) mi([ sol]) sol([ do']) |
    do'4.( sol8) mi4 r4 |
    do'4.( sol8) re'4.( si8) |
    mi'1~ |
    mi'4. do'8 sol4 sol |
    mi r r2 |
    R1*16 |
    r4 sol8. sol16 do'8. do'16 fa8. fa16 |
    sol8 sol r4 r2 |
    r4 sol8. sol16 do'8. do'16 fa8. fa16 |
    sol8 sol r4 r2 |
    r4 sol8.^\f sol16 si8. si16 sol8. sol16 |
    do'4 r la^\p la |
    fa fa sol sol |
    do'4 r la la |
    fa fa sol sol |
    do r r2 |
    R1*5 |
    R2.*28 |
    do2 mi4 |
    sol2 sold4 la( fa) red |
    mi mi r |
    la2 la4 |
    la( sold) sol |
    sol2 sol4 |
    sol( fad) fa |
    fa2 fa4 |
    mi r r |
    R2. |
    re4 sold si |
    re'2~ re'8([ do']) |
    do'4 r r |
    re2 re4 |
    re2 re4 |
    mi2 mi4 |
    fa r r |
    re( fa) sib |
    re( fa) sib |
    mi2 mi4 |
    la r r |
    R2.*9 |
    r4 sol sol |
    sol( lab) sol |
    r do' do' |
    do'( si) do' |
    r sol sol |
    sol( fad) sol |
    R2.*3 |
    si2 si4 |
    do'2 do'4 |
    do'2 do'4 |
    si4 si r |
    R2.^\fermataMarkup |
    R1*7 |
    r4 <>^\markup\italic sotto vocce do( mi) sol |
    fa( do) la2 |
    r4 re fad la |
    sol( re) si2 |
    r4 mi( sold) si |
    la( mi) do'2 |
    r4 fa! la do' |
    si( sol! re'2) |
    R1*3 |
    r2 do'4. do'8 |
    re'4 r sol r |
    r do' do' mi' |
    re' sol r sol8^\f sol |
    do'4 sol do' sol |
    do'4 sol do' sol |
    do' r r2 |
    do'2. do'4 |
    fa2 fa |
    fa2. fa4 |
    sol4. sol8 sol4. sol8 |
    sol4. sol8 sol4. sol8 |
    do4 r r2 |
    R1 |
    r2 sol4^\p sol |
    sol sol sol sol |
    sol sol r2 |
    R1 |
    r2 do4 si, |
    do si, do si, |
    do r r2 |
    R1*4 |
    r4 <>^\markup\italic sotto voce do4( mi) sol |
    fa( do) la2 |
    r4 re fad la |
    sol( re) si2 |
    r4 mi( sold) si |
    la( mi) do'2 |
    r4 fa! la do' |
    si( sol! re'2) |
    r2 si4. si8 |
    re'4 r si r |
    r si si si |
    re'4 r si r |
    r2 si4.^\cresc si8 |
    re'4 r si r |
    r si^\f si si |
    re'2 r |
    R1*3 |
    r2 <>^\markup\italic sotto vocce do'4. do'8 |
    re'4 r sol r |
    r do' do' mi' |
    re' sol r sol8^\f sol |
    do'4 sol do' sol |
    do' sol do' sol |
    do'4 r r2 |
    do'2. do'4 |
    fa2 fa |
    fa2. fa4 |
    sol4. sol8 sol4. sol8 |
    sol4. sol8 sol4. sol8 |
    do4 fad2 sol4 |
    sold la r fa! |
    re2 sol |
    mi4 fad2 sol4 |
    sold la r fa! |
    re2 sol |
    do4 r mi do |
    fa re sol sol8([ fa]) |
    mi4 r mi do |
    fa re sol sol8([ fa]) |
    mi4 r mi do |
    fa re sol sol |
    do r r2 |
    R1*6 |
  }
>>
