\clef "bass" r4 |
R1*9 |
<<
  \tag #'(fagotto1 fagotti) {
    r2 r4 \tag#'fagotti <>^\markup\concat { 1 \super o } sol8.\p sol16 |
    sol4.( la16 si) do'4.( mi'16 re') |
    do'4( si) r2 |
    
  }
  \tag #'fagotto2 R1*3
>>
R1 |
r2 r4 <>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol8 sol | }
  { sol sol | }
>>
<<
  \tag #'(fagotto1 fagotti) { mi'8( re') re'( do') do'( re') re'( mi') | }
  \tag #'(fagotto2 fagotti) { do'8( sol) sol( mi) mi( sol) sol( do') | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'4.( fa'16 re') do'8-. sol( do' re') | }
  { do'4.( sol8) mi4 mi8( sol) | }
>>
<<
  \tag #'(fagotto1 fagotti) { mi'8( re') re'( do') do'( re') re'( mi') | }
  \tag #'(fagotto2 fagotti) { do'8( sol) sol( mi) mi( sol) sol( do') | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'4.( fa'16 re') }
  { do'4.( sol8) }
>> <<
  \tag #'(fagotto1 fagotti) {
    do'8-. mi( sol do') |
    mi'2( fa') |
    sol'1~ |
    sol'4.( mi'8)
  }
  \tag #'(fagotto2 fagotti) {
    mi8-. do( mi sol) |
    do'2( re') |
    mi'1~ |
    mi'4.( do'8)
  }
  { s2 | s1 | s\f | s4 s\p }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'8.( do'16) re'8.( mi'16) | }
  { sol8 r sol r | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    do'4 s do'8 s4. |
    si8 s4. do'8 s4. |
    si8 s4. dod'8 s4. |
    re'8 s4. dod'8 s4. |
    re'4
  }
  \tag #'(fagotto2 fagotti) {
    mi4 s fad8 s4. |
    sol8 s4. fad8 s4. |
    sol8 s4. sol8 s4. |
    fad8 s4. sol8 s4. |
    fad4
  }
  { s4 r s8 r r4 |
    s8 r r4 s8 r r4 |
    s8 r r4 s8 r r4 |
    s8 r r4 s8 r r4 | }
>> r4 r2 |
R1*3 |
<>\p <<
  \tag #'(fagotto1 fagotti) {
    re'1 |
    mi' |
    re' |
    mi'1 |
    re'8 re' re' re' re'4 s |
    s8 re' re' re' re'4 s |
    s8 re' re' re' re'4 s |
    re'2 re' |
    re'4 s2. |
    s4 re'( mi'4. sol'16 fa') |
    \appoggiatura mi'4 re'2 s |
    s4 re'( mi'4. sol'16 fa') |
    \appoggiatura mi'4 re'2 s |
  }
  \tag #'(fagotto2 fagotti) {
    si1 |
    do' |
    si |
    do' |
    si8 si si si si4 r |
    r8 si si si si4 r |
    r8 si si si si4 r |
    si2 si |
    si4 s2. |
    s4 si4( do'4. mi'16 re') |
    \appoggiatura do'4 si2 s |
    s4 si4( do'4. mi'16 re') |
    \appoggiatura do'4 si2 s |
  }
  { s1*4 |
    s2. r4 |
    r8 s4. s4 r |
    r8 s4. s4 r |
    s2\fp s\fp |
    s4\f r r2 |
    r4 s2.\p | s2 r |
    r4 s2. | s2 r | }
>>
r4 <>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { si4 re'4. sol'8 | }
  { sol2 si4 | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    mi'4 s mi'-. mi'-. |
    fa' fa' re' re' |
    mi'4 s mi' mi' |
    fa' fa' re' re' |
  }
  \tag #'(fagotto2 fagotti) {
    do'4 s do'-. do'-. |
    re' re' si si |
    do'4 s do' do' |
    re' re' si si |
  }
  { s4 r s2\p | s1 |
    s4 r s2\p | s1 | }
>>
\tag#'fagotti <>^"a 2." do2:8 do:8 |
do2:8\cresc do:8 |
do2:8\f do4 r |
R1*3 | \allowPageTurn
R2.*2 |
r4 \tag#'fagotti <>^"a 2." r fa'4\f |
sol'2. |
fa'4 r r |
R2. |
r4 r fa' |
sol'2. |
fa'\p |
<<
  \tag #'(fagotto1 fagotti) {
    re'2 mi'4 |
    re'2 mi'4 |
    re'2 mi'4 |
    re'8 mi' re' mi' re' mi' |
    re'8 mi' re' mi' re' mi' |
    re'8 sol'
  }
  \tag #'(fagotto2 fagotti) {
    si!2 do'4 |
    si2 do'4 |
    si2 do'4 |
    si8 do' si do' si do' |
    si do' si do' si do' |
    si8 sol
  }
  { s2.*3 | s4. s\cresc | s2. | s4\f }
>> \tag#'fagotti <>^"a 2." sol2:8 |
sol4 r r8 sol |
do' si do'4 r8 mi |
la sold la4 r8 do |
fa mi fa4 r8 la, |
re dod re mi fa re |
si,!4-. si-. r |
R2.*2 |
<<
  \tag #'(fagotto1 fagotti) {
    mi'2. |
    fa'2( re'4) |
    mi'2. |
    fa'2( re'4) |
  }
  \tag #'(fagotto2 fagotti) {
    do'2. |
    re'2( si4) |
    do'2. |
    re'2( si4) |
  }
  { s2.\f | s\p | s2.\f | s\p | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'4( sol mi) |
    do( mi'2)~ |
    mi'4( re') re' | }
  { do'4( sol mi) |
    do4( do'2)~ |
    do'4( si) si | }
>>
<<
  \tag #'(fagotto1 fagotti) { do'2 do'4 | do'4( si) }
  \tag #'(fagotto2 fagotti) { la2 la4 | la( sold) }
>> r4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'4( mi' red') |
    red'( mi') mi' |
    mi'( re'! dod') |
    dod'( re') re' |
    red'2. |
    mi'4 }
  { do'2.~ |
    do'4 si sib |
    sib2.~ |
    sib4 la la |
    do'!4( si! la) |
    sold }
>> r4 r |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'4( re'! si) | }
  { R2. }
>>
<<
  \tag #'(fagotto1 fagotti) {
    sold2.~ |
    sold2~ sold8( la) |
    la4
  }
  \tag #'(fagotto2 fagotti) {
    re2.~ |
    re2~ re8( do) |
    do4
  }
>> r4 r |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'4( re' sib |
    fa' re' sib) |
    do'2( si!4) | }
  { re2.~ | re | mi | }
>>
<<
  \tag #'(fagotto1 fagotti) { la4 }
  \tag #'(fagotto2 fagotti) { fa }
>> r r |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib4( re' fa' |
    sib re' fa') |
    do'2( si!4) |
    la2. | }
  { re2.~ | re | mi | la | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    si2.~ |
    si8( do') do'2 |
    re'2.~ |
    re'8( mib') mib'2~ |
    mib'2.~ |
    mib' |
    mi'!2.~ |
    mi'8( fa') fa'2 |
  }
  \tag #'(fagotto2 fagotti) {
    sold2.~ |
    sold8( la) la2 |
    si2.~ |
    si8( do') do'2 |
    reb'2. |
    do' |
    reb'~ |
    reb'8( do') do'2 |
  }
  { s2.*6 | s2.\cresc }
>>
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'2. | }
  { mib'2( re'!8 do') | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    sol'4 si! si |
    si( do' si) |
    s mib' mib' |
    mib'( fa' mib') |
    s re' re' |
    re'( mib' re') |
    
  }
  \tag #'(fagotto2 fagotti) {
    si!4 sol sol |
    sol( lab sol) |
    s do' do' |
    do'( si do') |
    s sol sol |
    sol( fad sol) |
  }
  { s4 s2\p | s2. | r4 s2 | s2. | r4 }
>>
R2.*3 |
<>\p <<
  \tag #'(fagotto1 fagotti) { re2. | mib~ | mib | re4 }
  \tag #'(fagotto2 fagotti) { si,2. | do~ | do | si,4 }
>> r4 r |
R2.^\fermataMarkup |
\tag#'fagotti <>^"a 2." do2\f r |
R1 |
sol,2\f r |
R1 |
do4\p r r2 |
R1*9 |
r2 \tag#'fagotti <>^"a 2." sol4\p sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4 <>\f <<
  \tag #'(fagotto1 fagotti) {
    re'4 mi' re' |
    mi' re' mi' re' |
  }
  \tag #'(fagotto2 fagotti) {
    si4 do' si |
    do' si do' si |
  }
>>
\tag#'fagotti <>^"a 2." do'2:8 do':8 |
do:8 do:8 |
fa:8 fa:8 |
fa:8 fa:8 |
sol:8 sol:8 |
sol:8 sol,:8 |
do4 r r2 |
do'4 r r2 |
sol4 r r2 |
R1*2 |
sol4 r r2 |
do4 r r2 |
R1*14 |
<<
  \tag #'(fagotto1 fagotti) \ru#7 { s2 si4 s | }
  \tag #'(fagotto2 fagotti) \ru#7 { s2 sol4 s | }
  \ru#7 { r2 s4 r | }
  { s2 s\f }
>>
r2 \tag#'fagotti <>^"a 2." sol4\p sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4 sol sol sol |
sol sol sol la8 si |
do'4\f <<
  \tag #'(fagotto1 fagotti) {
    re'4 mi' re' |
    mi' re' mi' re' |
  }
  \tag #'(fagotto2 fagotti) {
    si4 do' si |
    do' si do' si |
  }
>>
\tag#'fagotti <>^"a 2." do'2:8 do':8 |
do:8 do:8 |
fa:8 fa:8 |
fa:8 fa:8 |
sol:8 sol:8 |
sol:8 sol,:8 |
do4 fad2 sol4-. |
sold-. la-. r fa! |
re4 r8 \tuplet 3/2 { re16( mi fa } sol4) r8 \tuplet 3/2 { la16( sol fa } |
mi4) fad2 sol4-. |
sold-. la-. r fa! |
re4 r8 \tuplet 3/2 { re16( mi fa } sol4) r8 \tuplet 3/2 { fa16( mi re } |
do4) r mi-. do-. |
fa-. re-. sol8 la sol fa |
mi4 r mi do |
fa re sol8 la sol fa |
mi4 r mi do |
fa re sol sol, |
do8 mi sol do' do2:8 |
do8 mi sol do' do2:8 |
do8 mi sol do' do2:8 |
do4 sol, do sol, |
do2 r4 r8 \tuplet 3/2 { sol,16( la, si, } |
do4) r8 \tuplet 3/2 { sol,16( la, si, } do4) r8 \tuplet 3/2 { sol,16( la, si, } |
do2) r |
