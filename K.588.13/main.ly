\version "2.19.80"
\include "common.ily"

\opusTitle "K.588 – N°13 – Alla bella Despinetta"

\header {
  title = \markup\center-column {
    \line\italic { Così fan tutte }
    \line { N°13 – Sestetto: \italic { Alla bella Despinetta } }
  }
  opus = "K.588"
  date = "1790"
  copyrightYear = "2019"
}

\includeScore "K.588.13"
