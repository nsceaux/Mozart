\piecePartSpecs
#`((oboi #:score-template "score-oboi")
   (clarinetti #:score-template "score-clarinetti"
               #:instrument ,#{ \markup\center-column { Clarinetti in C } #})
   (fagotti #:score-template "score-fagotti")
   (trombe  #:tag-global ()
            #:score-template "score-trombe"
            #:instrument "Trombe in C")
   (timpani #:instrument ,#{\markup\center-column { Timpani in C.G. }#})
   (violino1)
   (violino2)
   (viola)
   (basso)
   (silence #:on-the-fly-markup , #{ \markup\tacet-text\line { Sestetto: TACET } #}))
