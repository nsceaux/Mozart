\clef "bass" sol8.\p sol16 |
do'4.( sol8) do'4.( fa8) |
sol4 sol,\f r sol8.\p sol16 |
do'4.( sol8) do'4.( fa8) |
sol4 sol, r sol8. sol16 |
do'4.( sol8) do'4.( fa8) |
sol4 sol, r sol8. sol16 |
do'4-. do'-. do'-. do'-. |
la8( do'16 la) fa4-. r fa |
sol sol sol sol |
do r r2 |
r4 sol8 r do' r do r |
sol r sol, r r2 |
r4 sol8 r do' r do r |
sol r sol, r sol r r4 |
do4 r r2 |
r4 do do do |
do r r2 |
r4 do do do |
do r do r |
do do8.(\f mi16) sol2:8^\dotFour |
sol4 r sol,\p r |
do r la r |
sol r la r |
sol r mi r |
re4 r mi r |
re1 |
do |
si,4 r si r |
fad r sol r |
sol8 sol sol sol sol sol sol sol |
sol2:8 sol:8 |
sol:8 sol:8 |
sol:8 sol:8 |
sol:8 sol4 r |
r8 sol sol sol sol4 r |
r8 sol sol sol sol4 r |
sol8\fp sol sol sol sol\fp sol sol sol |
sol4\f r r2 |
r4 sol8\p r do' r fa r |
sol r sol, r r2 |
r4 sol8 r do' r fa r |
sol8 r sol, r r2 |
r4 sol8\cresc r si r sol\f r |
do'4 r la\p la |
fa fa sol sol |
do'4 r la la |
fa fa sol sol |
do2:8 do:8 |
do2:8\cresc do:8 |
do2:8\f do4 r |
r8 do'-.\p sol-. mi-. do4 r |
r8 re'-. sib-. fa-. re4 r |
r8 mi' do' sol mi4 r |
r4 r8 fa-.\f sol-. la-. |
sib-. la-. sib-. do'-. re'-. mi'-. |
fa'4 r r |
R2. |
r4 r8 fa-. sol-. la-. |
sib-. la-. sib-. do'-. re'-. mi'-. |
fa'4 r r |
R2.*2 |
sol2.:8\p |
sol:8 |
sol:8 |
<< sol:8 { s4. s\cresc } >> |
sol2.:8 |
sol:8\f |
sol4 r r |
R2.*3 | \allowPageTurn
r8 dod re mi fa re |
si,!4-. si-. r |
do'\p r la |
fa r sol |
do'8\f mi' do' mi' la do' |
fa4\p r sol |
do'8\f mi' do' mi' la do' |
fa4\p r sol |
do r r |
do2( mi4) |
sol2( sold4) |
la( fa red) |
mi mi r |
la2 la4 |
la( sold sol) |
sol2 sol4 |
sol( fad fa) |
fa2 fa4 |
mi r r |
R2. |
re4( sold si) |
re'2~ re'8( do') |
do'4 r r |
<<
  { re2. | re | mi2.( | fa4) } \\
  { re4 si,\rest si,\rest | re si,\rest si,\rest | mi2.( | fa4) }
>> r r |
<<
  { re4( fa sib | re fa sib) | } \\
  { re2. | re | }
>>
<>^"Bassi" mi2. |
la4 r r |
mi r r |
la r r |
sol r r |
do' r r |
sol2.:8 |
lab:8 |
sib:8\cresc |
la!:8 |
lab:8\f |
sol4 r r |
R2. |
do'4 r r |
R2. |
sol4 r r |
R2.*4 |
sol4\p r r |
mib r r |
do r r |
sol, r r |
R2.^\fermataMarkup |
do2\f r |
R1 |
sol,2\f r |
R1 |
do4\p r r2 |
R1*2 |
mi4\p r r2 |
fa4 r r2 |
fad4 r r2 |
sol4 r r2 |
sold4 r r2 |
la4 r r2 |
la4 r r2 |
si4 r r2 |
R1 |
sol,4 r r2 |
sol4 r r2 |
sol,4 r r2 |
sol4 r r2 |
sol,4 r r2 |
sol4 r r sol\f |
do' sol do' sol |
do'4 sol do' sol |
do'2:8 do':8 |
do:8 do:8 |
fa:8 fa:8 |
fa:8 fa:8 |
sol:8 sol:8 |
sol:8 sol,:8 |
do4 r r2 |
do'4 r r2 |
sol4 r r2 |
R1*2 |
sol4 r r2 |
do4 r r2 |
R1*6 |
mi4\p r r2 |
fa4 r r2 |
fad4 r r2 |
sol4 r r2 |
sold4 r r2 |
la4 r r2 |
la4 r r2 |
si4 r r2 |
R1 |
sol4\f r r2 |
sol4 r r2 |
sol4 r r2 |
sol4 r r2 |
sol4 r r2 |
sol4 r r2 |
sol4 r r2 |
R1 |
sol,4\p r r2 |
sol4 r r2 |
sol,4 r r2 |
sol4 r r2 |
sol,4 r r2 |
sol4 r r sol\f |
do' sol do' sol |
do' sol do' sol |
do'2:8 do':8 |
do:8 do:8 |
fa:8 fa:8 |
fa:8 fa:8 |
sol:8 sol:8 |
sol:8 sol,:8 |
do4 fad2 sol4-. |
sold-. la-. r fa! |
re4 r8 \tuplet 3/2 { re16( mi fa } sol4) r8 \tuplet 3/2 { la16( sol fa } |
mi4) fad2 sol4-. |
sold-. la-. r fa! |
re4 r8 \tuplet 3/2 { re16( mi fa } sol4) r8 \tuplet 3/2 { fa16( mi re } |
do4) r mi-. do-. |
fa-. re-. sol8 la sol fa |
mi4 r mi do |
fa re sol8 la sol fa |
mi4 r mi do |
fa re sol sol, |
do8 mi sol do' do2:8 |
do8 mi sol do' do2:8 |
do8 mi sol do' do2:8 |
do4 sol, do sol, |
do2 r4 r8 \tuplet 3/2 { sol,16( la, si, } |
do4) r8 \tuplet 3/2 { sol,16( la, si, } do4) r8 \tuplet 3/2 { sol,16( la, si, } |
do2) r |
