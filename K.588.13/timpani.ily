\clef "bass" r4 |
R1*53 |
R2.*73 |
do2\f r |
R1 |
sol,2\f r |
R1*18 |
r2 r4 sol,8\f sol, |
do4 sol, do sol, |
do sol, do sol, |
do2 r4 do8 do |
do2 r4 do8 do |
do2 r |
R1 |
sol,1\startTrillSpan |
sol,4\stopTrillSpan r sol, r |
do r r2 |
do4 r r2 |
sol,4 r r2 |
R1*2 |
sol,4 r r2 |
do4 r r2 |
R1*15 |
<>\f \ru#7 { sol,4 r r2 | }
R1*6 |
r2 r4 sol,8\f sol, |
do4 sol, do sol, |
do sol, do sol, |
do2 r4 do8 do |
do2 r4 do8 do |
do2 r |
R1 |
sol,1\trill |
sol,4 r sol, r |
do r r2 |
R1*11 |
do1\startTrillSpan |
do |
do1*1/2 s\stopTrillSpan |
do4 sol, do sol, |
do2 r |
do do |
do r |
