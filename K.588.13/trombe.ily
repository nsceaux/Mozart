\clef "treble" \transposition do
r4 |
R1*19 |
r4 \tag#'trombe <>^"a 2." do'8.\f mi'16 sol'4 sol' |
sol' r r2 |
R1*8 |
<>\p <<
  \tag #'(tromba1 trombe) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'8 sol' sol' sol' sol'4 s |
    s8 sol' sol' sol' sol'4 s |
    s8 sol' sol' sol' sol'4 s |
    sol'2 sol' |
    sol'1~ |
    sol'2 s |
    sol'1~ |
    sol'2 s |
    sol'1~ |
    sol'~ |
    sol'4
  }
  \tag #'(tromba2 trombe) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol8 sol sol sol sol4 s |
    s8 sol sol sol sol4 s |
    s8 sol sol sol sol4 s |
    sol2 sol |
    sol1~ |
    sol2 s |
    sol1~ |
    sol2 s |
    sol1~ |
    sol |
    do'4
  }
  { s1*4 |
    s2. r4 |
    r8 s4. s4 r |
    r8 s4. s4 r |
    s2\fp s\fp |
    s1\fp |
    s2 r |
    s1 |
    s2 r |
    s1 |
    s4 s2.\cresc |
    s4\f }
>> r4 r2 |
R1*3 |
<<
  \tag #'(tromba1 trombe) {
    do''1~ |
    do''~ |
    do''8 do'' do'' do'' do''4
  }
  \tag #'(tromba2 trombe) {
    do'1~ |
    do'~ |
    do'8 do' do' do' do'4
  }
  { s1\p | s\cresc | s2\f }
>> r4 |
R1*3 |
R2.*9 |
<>\p <<
  \tag #'(tromba1 trombe) {
    sol'2.~ |
    sol'~ |
    sol'~ |
    sol'4 sol' sol' |
    sol' sol' sol' |
    sol'2.:8 |
    sol'4
  }
  \tag #'(tromba2 trombe) {
    sol2.~ |
    sol~ |
    sol~ |
    sol4 sol sol |
    sol sol sol |
    sol2.:8 |
    sol4
  }
  { s2.*3 | s4. s\cresc | s2. | s\f }
>> r4 r |
R2.*7 |
<>\f <<
  \tag #'(tromba1 trombe) { do''2. | }
  \tag #'(tromba2 trombe) { mi' | }
>>
R2. |
<>\f <<
  \tag #'(tromba1 trombe) { do''2. | }
  \tag #'(tromba2 trombe) { mi' | }
>>
R2.*32 | \allowPageTurn
<>\f <<
  \tag #'(tromba1 trombe) { do''2. | }
  \tag #'(tromba2 trombe) { do'2. | }
>>
<>\f <<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol4 }
>> r r |
R2. |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol4 }
>> r r |
R2. |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol4 }
>> r r |
R2.*3 |
<>\p <<
  \tag #'(tromba1 trombe) {
    sol'2.~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4
  }
  \tag #'(tromba2 trombe) {
    sol2.~ |
    sol~ |
    sol~ |
    sol~ |
    sol4
  }
>> r4 r |
R2.^\fermataMarkup |
<>\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { do'2 } { do' }
>> r2 |
R1 |
<>\f <<
  \tag #'(tromba1 trombe) { sol'2 }
  \tag #'(tromba2 trombe) { sol }
>> r2 |
R1*18 |
r2 r4 <>\f <<
  \tag #'(tromba1 trombe) {
    re''8 re'' |
    mi''4 re'' mi'' re'' |
    mi'' re'' mi'' re'' |
    do''1~ |
    do''~ |
    do''2
  }
  \tag #'(tromba2 trombe) {
    sol'8 sol' |
    do''4 sol' do'' sol' |
    do'' sol' do'' sol' |
    mi'1 |
    do'~ |
    do'2
  }
>> r2 |
R1 |
<<
  \tag #'(tromba1 trombe) {
    sol'1~ |
    sol'4 sol' sol' sol' |
    mi'
  }
  \tag #'(tromba2 trombe) {
    sol1~ |
    sol4 sol sol sol |
    do'
  }
>> r r2 |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { do' }
>> r r2 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r4 r2 |
R1*2 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r r2 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { do' }
>> r r2 |
R1*15 |
<>\f \ru#6 {
  <<
    \tag #'(tromba1 trombe) { sol'4 }
    \tag #'(tromba2 trombe) { sol }
  >> r r2 |
}
<>\p <<
  \tag #'(tromba1 trombe) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'2. re''8 re'' |
    mi''4 re'' mi'' re'' |
    mi'' re'' mi'' re'' |
    do''1~ |
    do''~ |
    do''2
  }
  \tag #'(tromba2 trombe) {
    sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol~ |
    sol2. sol'8 sol' |
    do''4 sol' do'' sol' |
    do'' sol' do'' sol' |
    mi'1 |
    do'~ |
    do'2
  }
  { s1*7 | s2. s4\f }
>> r2 |
R1 |
<<
  \tag #'(tromba1 trombe) {
    sol'1~ |
    sol'4 sol' sol' sol' |
    mi'
  }
  \tag #'(tromba2 trombe) {
    sol1~ |
    sol4 sol sol sol |
    do'
  }
>> r4 r2 |
R1*11 |
<<
  \tag #'(tromba1 trombe) {
    do''2 do''4 do'' |
    do''2 do''4 do'' |
    do''2 do''4 do'' |
    do'' re'' do'' re'' |
    do''2
  }
  \tag #'(tromba2 trombe) {
    mi'2 mi'4 mi' |
    mi'2 mi'4 mi' |
    mi'2 mi'4 mi' |
    mi' sol' mi' sol' |
    mi'2
  }
>> r2 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do'2 do' | do' }
  { do' do' | do' }
>> r |

