% DON ALFONSO
\tag #'voix5 {
  Al -- la bel -- la De -- spi -- net -- ta
  vi pre -- sen -- to a -- mi -- ci mie -- i;
  non di -- pen -- de che da le -- i,
  con -- so -- lar il vo -- stro cor.
}
% FERRANDO E GUILELMO (con tenerezza affettata)
\tag #'(voix4 voix6) {
  Per la man, che lie -- to io ba -- cio,
  per quei rai di gra -- zia pie -- ni,
  fa che vol -- ga a me se -- re -- ni
  i be -- gli oc -- chi il mio te -- sor.
}
% DESPINA (ridendo, da sè)
\tag #'voix3 {
  Che sem -- bian -- ze! che ve -- sti -- ti!
  che fi -- gu -- re! che mu -- stac -- chi!
  io non so se son Va -- lac -- chi,
  o se Tur -- chi son co -- stor,
  Va -- lac -- chi, Tur -- chi,
  Tur -- chi, Va -- lac -- chi?
}
% DON ALFONSO (piano, a Despina)
\tag #'voix5 {
  Che ti par di quell’ a -- spet -- to?
}
% DESPINA
\tag #'voix3 {
  Per par -- lar vi schiet -- to, schiet -- to,
  han -- no un mu -- so
  fuor dell’ u -- so,
  ve -- ro an -- ti -- do -- to d’a -- mor.
  Che fi -- gu -- re! che mu -- stac -- chi!
  io non so, se son Va -- lac -- chi,
  o se Tur -- chi son co -- stor,
  io non so, se son Va -- lac -- chi,
  o se Tur -- chi son co -- stor,
  io non so, se son Va -- lac -- chi,
  o se Tur -- chi son co -- stor.  
}
% FERRANDO, GUILELMO E DON ALFONSO (sottovoce)
\tag #'(voix4 voix6 voix5) {
  Or la co -- sa è ap -- pien de -- ci -- sa,
  se co -- stei non ci rav -- vi -- sa,
  non c’è più nes -- sun ti -- mor,
  non c’è più nes -- sun ti -- mor,
  non c’è più nes -- sun ti -- mor.
}
% FIORDILIGI E DORABELLA (di dentro)
\tag #'(voix1 voix2) {
  Ehi, De -- spi -- na, o -- là, De -- spi -- na!
}
% DESPINA
\tag #'voix3 {
  Le pa -- dro -- ne!
}
% DON ALFONSO (a Despina)
\tag #'voix5 {
  Ec -- co l’i -- stan -- te!
  Fa con ar -- te; io qui m’a -- scon -- do.
}
%(Si ritira.)

% FIORDILIGI E DORABELLA (escono dalla loro stanza)
\tag #'(voix1 voix2) {
  Ra -- gaz -- zac -- cia tra -- co -- tan -- te,
  che fai lì con si -- mil gen -- te;
  ra -- gaz -- zac -- cia tra -- co -- tan -- te,
  che fai lì con si -- mil gen -- te,
  con si -- mil gen -- te,
  con si -- mil gen -- te;
  fal -- li u -- sci -- re im -- man -- ti -- nen -- te, im -- man -- ti -- nen -- te, im -- man -- ti -- nen -- te,
  o ti so pen -- tir con lor,
  o ti so pen -- tir con lor,
  o ti so pen -- tir con lor.
}
% DESPINA, FERRANDO E GUILELMO (Tutti tre s’inginocchiano.)
\tag #'(voix3 voix4 voix6) {
  Ah, ma -- da -- me, per -- do -- na -- te!
  Al bel piè lan -- guir mi -- ra -- te
  due me -- schin, di vo -- stro mer -- to
  spa -- si -- man -- ti a -- do -- ra -- tor,
  spa -- si -- man -- ti a -- do -- ra -- tor.
}
% FIORDILIGI E DORABELLA
\tag #'(voix1 voix2) {
  Giu -- sti nu -- mi! co -- sa sen -- to?
  dell’ e -- nor -- me tra -- di -- men -- to,
  chi fu mai l’in -- de -- gno au -- tor?
  chi, fu,
  chi fu mai l’in -- de -- gno au -- tor?
}
% DESPINA, FERRANDO E GUILELMO
\tag #'(voix3 voix4 voix6) {
  Deh, cal -- ma -- te,
  deh, cal -- ma -- te, quel -- lo sde -- gno,
  deh, cal -- ma -- te, quel -- lo sde -- gno.
}
% FIORDILIGI E DORABELLA
\tag #'(voix1 voix2) {
  Ah, che più non ho ri -- te -- gno!
  tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror!
}
% DESPINA E DON ALFONSO (da sè)
\tag #'voix3 {
  Mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror. __
}
\tag #'voix5 {
  Mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to
  que -- sto rab -- bia e quel fu -- ror;

  mi __ da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror.
  Mi da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror.
  mi __ da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror.
}
% FERRANDO E GUILELMO (da sè)
\tag #'(voix4 voix6) {
  Qual di -- let -- to
  è a que -- sto pet -- to
  quel -- la rab -- bia e quel fu -- ror! __
  Qual di -- let -- to
  è a que -- sto pet -- to
  quel -- la rab -- bia e quel fu -- ror,
  e quel fu -- ror,
  qual di -- let -- to è a que -- sto pet -- to
  quel -- la rab -- bia e quel fu -- ror.
  Qual di -- let -- to è a que -- sto pet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  qual di -- let -- to è a que -- sto pet -- to,
  quel -- la rab -- bia e quel fu -- ror, __
  qual di -- let -- to è a que -- sto pet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  qual di -- let -- to è a que -- sto pet -- to
  quel -- la rab -- bia e quel fu -- ror,
  e quel fu -- ror,
  qual di -- let -- to è a que -- sto pet -- to
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror.
  
}
% FIORDILIGI E DORABELLA (da sè)
\tag #'(voix1 voix2) {
  Ah! per -- don, mio bel di -- let -- to,
  in -- no -- cen -- te è que -- sto cor,
  tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror.
  Ah che più non ho ri -- te -- gno,
  ah che più non ho ri -- te -- gno,
  tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror,
  tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror.
  Ah, che più non ho ri -- te -- gno,
  tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror.
  Ah! per -- don, mio bel di -- let -- to,
  in -- no -- cen -- te è que -- sto cor.
  Tut -- ta pie -- na ho l’al -- ma in pet -- to
  di di -- spet -- to e di ter -- ror,
  di di -- spet -- to e di ter -- ror,
  di di -- spet -- to e di ter -- ror,
  di di -- spet -- to e di ter -- ror,
  di di -- spet -- to e di ter -- ror,
  di di -- spet -- to e di ter -- ror.
}
\tag #'voix3 {
  Mi da un po -- co di so -- spet -- to,
  mi da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  mi __ da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror.
  Mi da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to,
  quel -- la rab -- bia e quel fu -- ror, __
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  mi da un po -- co di so -- spet -- to,
  mi da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror.
  mi __ da un po -- co di so -- spet -- to
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
  quel -- la rab -- bia e quel fu -- ror,
}
