\clef "alto" sol8.\p sol16 |
sol2 sol4.( la8) |
sol4 sol'\f r sol8.\p sol16 |
do'4.( si8) do'4.( re'8) |
si4-. si-. r re'8. re'16 |
mi'4.( re'8) mi'4.( fa'8) |
re'4-. si-. r sol8. sol16 |
do'4-. do'-. do'-. do'-. |
la8( do'16 la) fa4-. r fa |
sol sol sol sol |
do r r2 |
r4 sol8 r do' r do r |
sol r sol r r2 |
r4 sol'8 r do'' r do' r |
sol' r sol r sol r r4 |
do'4 r r2 |
r4 do' do' do' |
do' r r2 |
r4 do' do' do' |
do' r do' r |
do' do'8.(\f mi'16) sol'2:8\dotFour |
sol'4 r sol\p r |
do'8 <do' mi'> q q r <do' fad'> q q |
r <si sol'> q q r <do' fad'> q q |
r <si sol'> q q r <dod' sol'> q q |
r <re' fad'> q q r <dod' sol'> q q |
<re' fad'>4 r fad2~ |
fad( la) |
si4 r si r |
fad r fad r |
sol2:8 sol:8 |
sol2:8 sol:8 |
sol2:8 sol:8 |
sol2:8 sol:8 |
sol8 re' re' re' re'4 r |
r8 re' re' re' re'4 r |
r8 sol' sol' sol' sol'4 r |
re'8-.\fp re'4( fa'!8) re'8-.\fp re'4( fa'8) |
re'4\f r r2 |
r4 sol8\p r do' r fa r |
sol r sol r r2 |
r4 sol'8 r do'' r fa' r |
sol'8 r sol r r2 |
r4 sol'8\cresc r si' r sol'\f r |
do''4 r la'-.\p la'-. |
fa'-. fa'-. sol'-. sol'-. |
do''4 r la' la' |
fa' fa' sol' sol' |
do'2:8 do':8 |
do':8\cresc do':8 |
do':8\f do'4 r |
r8 mi'-.\p do'-. sol-. mi4 r |
r8 fa'-. re'-. sib-. fa4 r |
r8 sol' mi' do' sol4 r |
r4 r8 fa-.\f sol-. la-. |
sib-. la-. sib-. do'-. re'-. mi'-. |
fa'4 r fa' |
sol'8 sol'4 sol' sol'8 |
fa'4 r8 fa-. sol-. la-. |
sib-. la-. sib-. do'-. re'-. mi'-. |
fa'4 r fa' |
sol'8 sol'4 sol' sol'8 |
fa'8 fa\p fa2:8 |
sol2.:8 |
sol2.:8 |
sol2.:8 |
<< sol2.:8 { s4. s\cresc } >> |
sol2.:8 |
sol2.:8\f |
sol4 r r8 sol |
do' si do'4 r8 mi |
la sold la4 r8 do |
fa mi fa4 r8 la |
re'8 mi' fa' sol' la' fa' |
re'4-. re''-. r |
sol'2(\p la'4)~ |
la'2( sol'4) |
sol'2:8\f la'8 la' |
la'2(\p sol'4) |
sol'2:8\f la'8 la' |
la'2(\p sol'4) |
sol' r r |
R2.*21 | \allowPageTurn
r8 mi' mi'2:8 |
r8 mi' mi'2:8 |
r8 mi' mi'2:8 |
r8 sol' sol'2:8 |
r8 sol' sol'2:8 |
sib2.:16 |
lab:16 |
sol:16\cresc |
la!:16 |
do'8\f do'4 do'8( si! do') |
re'4 r r |
R2. |
<sol' sol>4 r r |
R2. |
<sol' sol>4 r r |
R2.*3 |
r4 si\p si |
r si si |
r do' do' |
r do' do' |
r si si |
R2.^\fermataMarkup |
do'2\f r |
R1 |
sol2\f r |
R1 |
do'4\p r r2 |
R1*2 |
mi'4\p r r2 |
fa'4 r r2 |
fad'4 r r2 |
sol'4 r r2 |
sold'4 r r2 |
la'4 r r2 |
la'4 r r2 |
si'4 r r2 |
R1 |
sol4 r r2 |
sol'4 r r2 |
sol4 r r2 |
sol'4 r r2 |
sol4 r r2 |
sol'4 r r sol\f |
do' sol do' sol |
do' sol do' sol |
do' r r2 |
do''1~ |
do''2 dod'' |
re''8 fa' fa' fa' fa'2:8 |
sol':8 sol':8 |
sol':8 sol:8 |
do'4 r r2 |
do''4 r r2 |
sol'4 r r2 |
R1*2 |
sol'4 r r2 |
do'4 r r2 |
R1*6 |
mi'4\p r r2 |
fa'4 r r2 |
fad'4 r r2 |
sol'4 r r2 |
sold'4 r r2 |
la'4 r r2 |
la'4 r r2 |
si'4 r r2 |
R1 |
<>\f \ru#7 { <sol' sol>4 r r2 | }
R1 |
sol4\p r r2 |
sol'4 r r2 |
sol4 r r2 |
sol'4 r r2 |
sol4 r r2 |
sol'4 r r sol\f |
do' sol do' sol |
do' sol do' sol |
do'4\f r r2 |
do''1~ |
do''2 dod'' |
re''8 fa' fa' fa' fa' fa' fa' fa' |
sol'2:8 sol':8 |
sol'2:8 sol:8 |
do'4 fad'2 sol'4-. |
sold'-. la'-. r fa'! |
re'4 r8 \tuplet 3/2 { re'16( mi' fa' } sol'4) r8 \tuplet 3/2 { la'16( sol' fa' } |
mi'4) fad'2 sol'4-. |
sold'-. la'-. r fa'! |
re'4 r8 \tuplet 3/2 { re'16( mi' fa' } sol'4) r8 \tuplet 3/2 { fa'16( mi' re' } |
do'4) r mi'-. do'-. |
fa'-. re'-. sol'8 la' sol' fa' |
mi'4 r mi' do' |
fa' re' sol'8 la' sol' fa' |
mi'4 r mi' do' |
fa' re' sol' sol |
do'8 mi' sol' do'' do'2:8 |
do'8 mi' sol' do'' do'2:8 |
do'8 mi' sol' do'' do'2:8 |
do'4 sol do' sol |
do'2 r4 r8 \tuplet 3/2 { sol16( la si } |
do'4) r8 \tuplet 3/2 { sol16( la si } do'4) r8 \tuplet 3/2 { sol16( la si } |
do'2) r |
