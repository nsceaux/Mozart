\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = "Clarinetti in C"
        shortInstrumentName = "Cl."
      } <<
        \global \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Trombe in C."
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = "Timpani in C.G."
        shortInstrumentName = "Tim."
      } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global
          \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global
          \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \global
        \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Fiordiligi
        shortInstrumentName = \markup\character Fio.
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Dorabella
        shortInstrumentName = \markup\character Do.
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Despina
        shortInstrumentName = \markup\character Des.
      } \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Ferrando
        shortInstrumentName =  \markup\character Fe.
      } \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Don Alfonso
        shortInstrumentName =  \markup\character D.Al.
      } \withLyrics <<
        \global \keepWithTag #'voix5 \includeNotes "voix"
      >> \keepWithTag #'voix5 \includeLyrics "paroles"
      
      \new Staff \with {
        instrumentName = \markup\character Guglielmo
        shortInstrumentName = \markup\character Gu.
      } \withLyrics <<
        \global \keepWithTag #'voix6 \includeNotes "voix"
      >> \keepWithTag #'voix6 \includeLyrics "paroles"
    >>

    \new Staff \with {
      instrumentName = \markup\center-column {
        Violoncello e Basso
      }
      shortInstrumentName = \markup\center-column { Vc. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \global
      \includeNotes "basso"
      \origLayout {
        s4 s1*7\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*4\break s1*4\pageBreak
        s1*5\break s1*4\pageBreak
        \grace s4 s1*4\pageBreak
        s1*5\break s1*3 s2.*4\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*9\break s2.*11\pageBreak
        s2.*11\break s2.*9\pageBreak
        s2.*14\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*8\pageBreak
        s1*6\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
