\clef "treble" r4 |
R1*11 |
<<
  \tag #'(oboe1 oboi) {
    r2 r4 \tag #'oboi <>^\markup\concat { 1 \super o }
    sol'8.\p sol'16 |
    sol'4.( la'16 si') do''4.( mi''16 re'') |
    do''4( si') r2 |
  }
  \tag #'oboe2 R1*3 
>>
R1*7 |
r2 <<
  \tag #'(oboe1 oboi) {
    fad''8 s4. |
    sol''8 s4. fad''8 s4. |
    sol''8 s4. dod'''8 s4. |
    re'''8 s4. dod'''8 s4. |
    re'''4
  }
  \tag #'(oboe2 oboi) {
    do''8 s4. |
    si'8 s4. do''8 s4. |
    si'8 s4. sol''8 s4. |
    fad''8 s4. sol''8 s4. |
    fad''4
  }
  { s8 r r4 |
    s8 r r4 s8 r r4 |
    s8 r r4 s8 r r4 |
    s8 r r4 s8 r r4 | }
>> r4 r2 |
R1*3 |
<>\p <<
  \tag #'(oboe1 oboi) {
    re''1 |
    mi'' |
    re'' |
    mi'' |
    re''8 sol'' sol'' sol'' sol''4 s |
    s8 sol'' sol'' sol'' sol''4 s |
    s8 sol'' sol'' sol'' sol''4 s |
    si''2 si'' |
    si''4
  }
  \tag #'(oboe2 oboi) {
    si'1 |
    do'' |
    si' |
    do'' |
    si'8 si' si' si' si'4 s |
    s8 si' si' si' si'4 s |
    s8 si' si' si' si'4 s |
    fa''!2 fa'' |
    fa''4
  }
  { s1*4 |
    s2. r4 |
    r8 s4. s4 r |
    r8 s4. s4 r |
    s2\fp s\fp | s4\f }
>> r4 r2 |
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    re''4( mi''4. sol''16 fa'') |
    \appoggiatura mi''4 re''2
  }
  \tag #'(oboe2 oboi) {
    si'4( do''4. mi''16 re'') |
    \appoggiatura do''4 si'2
  }
>> r2 |
r4 <<
  \tag #'(oboe1 oboi) {
    re''4( mi''4. sol''16 fa'') |
    \appoggiatura mi''4 re''2
  }
  \tag #'(oboe2 oboi) {
    si'4( do''4. mi''16 re'') |
    \appoggiatura do''4 si'2
  }
>> r2 |
r4 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'4 re''4. sol''8 | }
  { sol'2 si'4 | }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''4 s mi''-. mi''-. |
    fa'' fa'' re'' re'' |
    mi''4 s mi'' mi'' |
    fa'' fa'' re'' re'' |
    do''1 |
    mi''~ |
    mi''8 sol'' sol'' sol'' sol''4
  }
  \tag #'(oboe2 oboi) {
    do''4 s do''-. do''-. |
    re'' re'' si' si' |
    do''4 s do'' do'' |
    re'' re'' si' si' |
    \tag#'oboi \once\tieDown do''1~ |
    do''~ |
    do''8 mi'' mi'' mi'' mi''4
  }
  { s4 r s2\p | s1 |
    s4 r s2 |
    s1*2 |
    s1\cresc |
    s4\f }
>> r4 |
R1*3 |
R2.*2 |
r4 r <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8( fa'') | } { la'4 }
>>
<<
  \tag #'(oboe1 oboi) { mi''2. | fa''4 }
  \tag #'(oboe2 oboi) { sib'2. | la'4 }
>> r4 r |
R2. |
r4 r \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8( fa'') | } { la'4 }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''2. |
    fa''2. |
    re''2 mi''4 |
    re''2 mi''4 |
    re''2 mi''4 |
    re''8 mi'' re'' mi'' re'' mi'' |
    re'' mi'' re'' mi'' re'' mi'' |
 }
  \tag #'(oboe2 oboi) {
    sib'2. |
    la' |
    si'!2 do''4 |
    si'2 do''4 |
    si'2 do''4 |
    si'8 do'' si' do'' si' do'' |
    si'8 do'' si' do'' si' do'' |
 }
 { s2. | s\p | s2.*3 | s4. s\cresc | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 sol'' sol'2:8 |
    sol'4 s sol'8 sol' | }
  { si'8 sol'' sol'2:8 |
    sol'4 s sol'8 sol' | }
  { s2. | s4 r s | }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''4. fa''8-. sol''-. mi''-. |
    do''4. re''8-. mi''-. do''-. |
    la'4. si'8-. do''-. la'-. |
    fa'4-. fa''-. s |
    s s fa''4 |
  }
  \tag #'(oboe2 oboi) {
    do''4. re''8-. mi''-. do''-. |
    la'4. si'8-. do''-. la'-. |
    fa'4. sol'8-. la'-. fa'-. |
    re'4-. re''-. s |
    s s re'' |
  }
  { s2.*3 | s2 r4 | r r s\p | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2. | re'' | }
  { do''2.~ | do''2 si'4 | }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''2. | fa''2( re''4) |
    mi''2. | fa''2( re''4) |
  }
  \tag #'(oboe2 oboi) {
    do''2. | re''2( si'4) |
    do''2. | re''2( si'4) |
  }
  { s2.\f | s\p |
    s2.\f | s\p | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 } { do'' }
>> r4 r | \allowPageTurn
R2.*21 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'2. } { la' }
>>
<<
  \tag #'(oboe1 oboi) {
    si'2.~ |
    si'8( do'') do''2 |
    re''2.~ |
    re''8( mib'') mib''2~ |
    mib''2.~ |
    mib'' |
    mi''!~ |
    mi''8( fa'') fa''2 |
  }
  \tag #'(oboe2 oboi) {
    sold'2.~ |
    sold'8( la') la'2 |
    si'2.~ |
    si'8( do'') do''2 |
    reb''2. |
    do'' |
    reb''~ |
    reb''8( do'') do''2 |
  }
  { s2.*6 | s2.\cresc | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2. | }
  { mib''2( re''!8 do'') | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 }
  \tag #'(oboe2 oboi) { si'! }
>> r4 r |
R2. |
<<
  \tag #'(oboe1 oboi) { mib''4 }
  \tag #'(oboe2 oboi) { do'' }
>> r r |
R2. |
<<
  \tag #'(oboe1 oboi) { re''4 }
  \tag #'(oboe2 oboi) { si' }
>> r r |
r r <>\p <<
  \tag #'(oboe1 oboi) { si'4 | do''2. | re'' | }
  \tag #'(oboe2 oboi) { sol'4 | lab'2. | fad' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 }
  { sol' }
>> r4 r |
R2.*4 |
R2.^\fermataMarkup |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'2 } { do' }
>> <<
  \tag #'(oboe1 oboi) {
    mi''4. do''8 |
    sol''-. fa''-. mi''-. re''-. do''-. si'-. la'-. sol'-. |
    fa'2 re''4. si'8 |
    fa'' mi'' re'' do'' si' la' sol' fa' |
    mi'4 fa'8 sol' la' sol' la' si' |
    do'' si' do'' re'' mi'' re'' mi'' fa'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    do''4. sol'8 |
    mi''-. re''-. do''-. si'-. la'-. sol'-. fa'-. mi'-. |
    re'2 si'4. sol'8 |
    re'' do'' si' la' sol' fa' mi' re' |
    do'4 re'8 mi' fa' mi' fa' sol' |
    la' sol' la' si' do'' si' do'' re'' |
    mi''4
  }
  { s2\fp | s1 | s2\f s\fp | s1 | s4 s2.\cresc | s2 s\f | }
>> r4 r2 |
R1*8 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    fa''1( |
    mi'')( |
    fa'') |
    mi''4 re'' mi'' re'' |
    mi''4 re'' mi'' re'' |
    sol''1 |
    sold'' |
    la''~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol'4 la'8 si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
  }
  \tag #'(oboe2 oboi) {
    re''1( |
    do'')( |
    re'') |
    do''4 si' do'' si' |
    do''4 si' do'' si' |
    mi''1~ |
    mi'' |
    fa''~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi'4 fa'8 sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
  }
  { s1*3 | s1\f | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2 } { do'' }
>> <<
  \tag #'(oboe1 oboi) {
    mi''2 |
    sol''8 fa'' mi'' re'' do'' si' la' sol' |
    fa'4
  }
  \tag #'(oboe2 oboi) {
    do''2 |
    mi''8 re'' do'' si' la' sol' fa' mi' |
    re'4
  }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    si'2 re'' |
    fa''8 mi'' re'' do'' si' la' sol' fa' |
    mi'4
  }
  \tag #'(oboe2 oboi) {
    re'2 si' |
    re''8 do'' si' la' sol' fa' mi' re' |
    do'4
  }
>> r4 r2 |
R1 |
r2 <<
  \tag #'(oboe1 oboi) {
    mi'4. mi'8 |
    fa'8 mi' fa' sol' la' sol' la' si' |
  }
  \tag #'(oboe2 oboi) {
    do'4. do'8 |
    re' do' re' mi' fa' mi' fa' re' |
  }
  { s4.\f s8\p | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8 sol' la' si' }
  { mi'4 la'8 si' }
  { s4 s\cresc }
>> <<
  \tag #'(oboe1 oboi) { do''8 si' do'' re'' | }
  \tag #'(oboe2 oboi) { la' sol' la' si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8 re'' do'' re'' }
  { do''8 si' do'' si' }
>> <>\f <<
  \tag #'(oboe1 oboi) { mi''8 re'' mi'' fa'' | sol''4 }
  \tag #'(oboe2 oboi) { do''8 si' do'' re'' | mi''4 }
>> r4 r2 |
R1*8 |
<<
  \tag #'(oboe1 oboi) {
    s2 fa''4 s |
    s2 fa''4 s |
    s2 fa''4 s |
    s2 fa''4 s |
    s2 fa''4 s |
    s2 fa''4 s |
    s2 fa''4 s |
  }
  \tag #'(oboe2 oboi) {
    s2 re''4 s |
    s2 re''4 s |
    s2 re''4 s |
    s2 re''4 s |
    s2 re''4 s |
    s2 re''4 s |
    s2 re''4 s |
  }
  { r2 s4\f r |
    r2 s4 r |
    r2 s4 r |
    r2 s4 r |
    r2 s4 r |
    r2 s4 r |
    r2 s4 r |
  }
>>
R1 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    fa''1( |
    mi'' |
    fa'') |
    mi''4 re'' mi'' re'' |
    mi'' re'' mi'' re'' |
    sol''1 |
    sold'' |
    la''~ |
    la''8( sol''!) fa''-. mi''-. re''-. do''-. si'-. la'-. |
    sol'4 la'8 si' do'' re'' mi'' fa'' |
    sol'' mi'' sol'' mi'' fa'' re'' fa'' re'' |
  }
  \tag #'(oboe2 oboi) {
    re''1( |
    do'' |
    re'') |
    do''4 si' do'' si' |
    do'' si' do'' si' |
    mi''1~ |
    mi'' |
    fa''~ |
    fa''8( mi'') re''-. do''-. si'-. la'-. sol'-. fa'-. |
    mi'4 fa'8 sol' la' si' do'' re'' |
    mi'' do'' mi'' do'' re'' si' re'' si' |
  }
  { s1*3 | s1\f | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 } { do'' }
>> \tag #'oboi <>^"a 2." fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { la''16( sol'' fa'' } |
mi''4) fad''2 sol''4-. |
sold''-. la''-. r fa''! |
re''4 r8 \tuplet 3/2 { re''16( mi'' fa'' } sol''4) r8 \tuplet 3/2 { fa''16( mi'' re'' } |
do''4) r mi''-. do''-. |
fa''-. re''-. sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol''8 la'' sol'' fa'' |
mi''4 r mi'' do'' |
fa'' re'' sol'' <<
  \tag #'(oboe1 oboi) {
    si''4 |
    do'''4. do'''8 do'''2:8 |
    do'''4. sol''8 sol''2:8 |
    sol''4. mi''8 mi''2:8 |
    mi''4 sol'' mi'' sol'' |
    mi''2
  }
  \tag #'(oboe2 oboi) {
    re''4 |
    mi''4. mi''8 mi''2:8 |
    mi''4. mi''8 mi''2:8 |
    mi''4. do''8 do''2:8 |
    do''4 si' do'' si' |
    do''2
  }
>> \tag#'oboi <>^"a 2." r4 r8 \tuplet 3/2 { sol'16( la' si' } |
do''4) r8 \tuplet 3/2 { sol'16( la' si' } do''4) r8 \tuplet 3/2 { sol'16( la' si' } |
do''2) r |
