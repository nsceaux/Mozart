OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Mozart


K.196.14:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.196.14

K.Anh.245:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.Anh.245

K.217:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.217

K.419:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-trombe -dpart=trombe $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-timpani -dpart=timpani $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.419

K.420:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@- -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.420

K.432:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.432

K.527.21a:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.527.21a

K.588.13:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-trombe -dpart=trombe $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-timpani -dpart=timpani $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.588.13

K.588.21:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
.PHONY: K.588.21

K.621.7:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.621.7

K.621.9:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.621.9

K.486a:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/part.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/part.ly
.PHONY: K.486a

K.489:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.489

K.384.9:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.384.9

K.492.11:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.492.11

K.492.18:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.492.18

K.492.28:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-trombe -dpart=trombe $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-timpani -dpart=timpani $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.492.28

K.528:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-flauti -dpart=flauti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.528

K.578:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboi -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-trombe -dpart=trombe $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.578

K.620.3:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetti -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-fagotti -dpart=fagotti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.620.3

K.Anh.110:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-corni -dpart=corni $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-oboe -dpart=oboi $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-clarinetto -dpart=clarinetti $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino1 -dpart=violino1 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-violino2 -dpart=violino2 $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-viola -dpart=viola $@/main.ly
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@-basso -dpart=basso $@/main.ly
.PHONY: K.Anh.110

K.555:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)_$@ $@/main.ly
.PHONY: K.555

delivery:
	for f in $(OUTPUT_DIR)/*.pdf ; do \
		_opus=`echo $$f | sed -e 's!.*_!!' -e 's!\.pdf!!' -e 's!-.*!!'`; \
		mkdir -p $(DELIVERY_DIR)/$$_opus; \
		mv -f $$f $(DELIVERY_DIR)/$$_opus; \
	done
.PHONY: delivery

all: K.196.14 K.217 K.419 K.420 K.432 K.486a K.489 K.492.11 K.492.18 K.492.28 \
	K.528 K.578 K.588.13 K.588.21 K.621.7 K.Anh.110 K.620.3 \
	K.527.21a
.PHONY: all

