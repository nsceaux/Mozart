\clef "treble" <>\f <<
  \tag #'(oboe1 oboi) { sol'4 do'' mi'' }
  \tag #'(oboe2 oboi) { mi' sol' do'' }
>> r4 |
R1*9 |
\tag#'oboi <>^"a 2." do''8[-!\f mi'']-! sol''-! r8 r4 r8 <<
  \tag #'(oboe1 oboi) { re''8 | }
  \tag #'(oboe2 oboi) { si' | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''8[-! mi'']-! sol''-! }
  { do''[-! mi'']-! sol''-! }
>> r8 r4 r8 <<
  \tag #'(oboe1 oboi) { re''8 | mi''4 }
  \tag #'(oboe2 oboi) { si'8 | do''4 }
>> r4 r2 |
R1*5 |
<>\p <<
  \tag #'(oboe1 oboi) { mi''4 mi''8. mi''16 fa''4 fa'' | mi'' }
  \tag #'(oboe2 oboi) { do''4 do''8. do''16 re''4 re'' | do'' }
>> r4 r2 |
R1*3 |
r8 <<
  \tag #'(oboe1 oboi) { fad''8 sol'' sol'' fad'' fad'' sol'' sol'' | fad''4 }
  \tag #'(oboe2 oboi) { la'8 si' si' la' la' si' si' | la'4 }
  { s8 s4\cresc s2 | s4\f }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    \tag#'oboi <>^\markup\concat { 1 \super o }
    re''1\p~ |
    re''1\f |
    re''\p~ |
    re''4 r la' r |
    sol' r
  }
  \tag #'oboe2 { R1*4 | r2 }
>> r4 <>\p <<
  \tag #'(oboe1 oboi) { fad''4( | sol'') }
  \tag #'(oboe2 oboi) { do''( | si') }
>> r4 r2 |
R1*8 |
<<
  \tag #'(oboe1 oboi) {
    \tag#'oboi <>^\markup\concat { 1 \super o }
    si'2.\sf si'4\p |
    do''( re'' mib'' do'') |
    sol' r r2 |
    R1 |
    \tag#'oboi <>^\markup\concat { 1 \super o }
    sol''2~ sol''16( fad'' la'' sol'' fa'' mi'' re'' do'') |
    si'4 r r2 |
    R1*11 |
  }
  \tag #'oboe2 R1*17
>>
r2 r8 <>\f <<
  \tag #'(oboe1 oboi) { sol''8-! sol''-! sol''-! | sol''4 }
  \tag #'(oboe2 oboi) { re''8-! mi''-! mi''-! | re''4 }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    \tag#'oboi <>^\markup\concat { 1 \super o }
    lab''2(\p sol''4 fa'') |
    mib''( re'' mib'' do'') |
    si'4
  }
  \tag #'oboe2 { R1*2 | r4 }
>> <>\sfp <<
  \tag #'(oboe1 oboi) {
    sol''2. |
    lab''8( sol'' lab'' sol'' lab'' sol'' lab'' sol'') |
    fa''1 |
    mib''4( re'') mib''2 |
    re''4( mib'' re'' mib'') |
    re''4
  }
  \tag #'(oboe2 oboi) {
    si'2. |
    fa''8( mib'' fa'' mib'' fa'' mib'' fa'' mib'') |
    re''1 |
    do''4( si') do''2 |
    si'4( do'' si' do'') |
    si'
  }
  { s2. | s1 | s1\sfp | }
>> \tag#'oboi <>^"a 2." sol''4 mib''\cresc do'' |
sol'\! r r2 |
<<
  \tag #'(oboe1 oboi) {
    R1 |
    \tag#'oboi <>^\markup\concat { 1 \super o }
    sol''2\p~ sol''16( fad'' la'' sol'' fa'' mi'' re'' do'') |
    si'4 r r2 |
    R1 |
  }
  \tag #'oboe2 R1*4
>>
<>\p <<
  \tag #'(oboe1 oboi) { mi''1( | re''4) }
  \tag #'(oboe2 oboi) { do''1( | si'4) }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    re''1( |
    mi''4) s s fa''( |
    mi'')
  }
  \tag #'(oboe2 oboi) {
    si'1( |
    do''4) s s si'( |
    do'')
  }
  { s1 | s4 r r s | }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(oboe1 oboi) { fa''4( mi'' re'') | }
  \tag #'(oboe2 oboi) { re''4( do'' si') | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 } { do'' }
>> r4 r2 |
R1*3 |
<<
  \tag #'(oboe1 oboi) {
    r2 \tag#'oboi <>^\markup\concat { 1 \super o }
    do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' |
    do''8 mi'' sol' si' do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' |
    do''8 mi'' sol' si' do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' |
    do''4 r
  }
  \tag #'oboe2 { R1*3 | r2 }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''4 s |
    la'' s fa'' s |
    mi''
  }
  \tag #'(oboe2 oboi) {
    do''4 s |
    do'' s si' s |
    do''
  }
  { s4 r | s4 r s r | }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(oboe1 oboi) {
    sol''8. sol''16 la''4 la''8. la''16 |
    la''4 la''8. la''16 sol''4 sol''8. sol''16 |
    sol''8 sol'' mi'' sol'' mi'' sol'' mi'' sol'' |
    mi''4
  }
  \tag #'(oboe2 oboi) {
    mi''8. mi''16 mi''4 fa''8. fa''16 |
    fa''4 re''8. re''16 re''4 fa''8. fa''16 |
    mi''8 mi'' do'' mi'' do'' mi'' do'' mi'' |
    do''4
  }
  { s4 s2\cresc | s1 | s\f | }
>> \tag#'oboi <>^"a 2." mi''4 sol'' mi'' |
do'' <<
  \tag #'(oboe1 oboi) { mi''4 mi'' mi'' | mi''2 }
  \tag #'(oboe2 oboi) { do''4 do'' do'' | do''2 }
>> r2 |
