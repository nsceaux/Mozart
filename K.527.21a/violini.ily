\clef "treble" \omit TupletBracket
<<
  \tag #'violino1 {
    <sol' do'' mi''>4\f <mi' do'' sol''> do' r |
    mi'\p r mi' r |
    r8 mi'32( re' mi' fa' sol'8) sol'-! sol'( mi') sol'( mi') |
    re'1 |
    r8 mi'32( re' mi' fa' sol'8) sol'-! r8 mi'32( re' mi' fa' sol'8) sol'-! |
    r8 la'32( sol' la' si' do''8) do''-! r8 la'32( sol' la' si' do''8) do''-! |
    r8 mi'32( re' mi' fa' sol'8) sol'-! r8 mi'32( re' mi' fa' sol'8) sol'-! |
    mi'2( re') |
    mi'4
  }
  \tag #'violino2 {
    <sol mi' do''>4\f <sol' do'' mi''> do' r |
    do'\p r do' r |
    r8 do'32( si do' re' mi'8) mi'-! mi'8( do') mi'( do') |
    si1 |
    r8 do'32( si do' re' mi'8) mi'-! r8 do'32( si do' re' mi'8) mi'-! |
    r8 fa'32( mi' fa' sol' la'8) la'-! r8 fa'32( mi' fa' sol' la'8) la'-! |
    r8 do'32( si do' re' mi'8) mi'-! r8 do'32( si do' re' mi'8) mi'-! |
    do'2( si) |
    do'4
  }
>> r4 r2 |
R1 |
<sol mi' do''>8[-!\f mi'']-! sol''-! r r4 r8 si'' |
<sol' mi'' do'''>[-! mi'']-! sol''-! r r4 r8 <<
  \tag #'violino1 {
    <sol' re'' si''>8 |
    <sol' mi'' do'''>8. mi'16\p mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
    re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
    mi'8-![ r16 mi'64( re' mi' fa'] sol'8)[-! r16 do''64( re'' do'' si']
    la'8)[-! r16 re''64( mi'' re'' do''] si'8)[-! r16 mi''64( fa'' mi'' re''] |
    do''8)[-! r16 fa''64(\cresc sol'' fa'' mi''] re''8)-![ r16 sol''64( la'' sol'' fa'']
    mi''8)-![ r16 \tuplet 3/2 { sol''32(\f la'' si''] } do'''8) r |
    mi'2(\p re' |
    mi'4) r r2 |
  }
  \tag #'violino2 {
    <re' si' sol''>8 |
    <mi' do'' mi''>8. do'16\p do'8. do'16 do'8. do'16 do'8. do'16 |
    si8. si16 si8. si16 si8. si16 si8. si16 |
    do'8. do'16 do'8-![ r16 do'64( re' do' si] la8)-![ r16 re'64( mi' re' do']
    si8)-![ r16 mi'64( fa' mi' re'] |
    do'8)-![ r16 fa'64(\cresc sol' fa' mi'] re'8)-![ r16 sol'64( la' sol' fa']
    mi'8)-![ r16 \tuplet 3/2 { sol'32(\f la' si'] } do''8) r |
    do'2(\p si |
    do'4) r r2 |
  }
>>
R1 |
r2 do''4( la' |
fad') r la'( fad') |
\override Script.avoid-slur = #'outside
<<
  \tag #'violino1 {
    re'16 re' mi' mi' fad'\cresc fad' sol' sol' la' la' si' si' do'' do'' la' la' |
    si'8\p si'16(\trill la'32 si' re''8) re''16(\trill dod''32 re'' sol''8) sol''16\trill( fad''32 sol'' si''8) si''16(\trill la''32 si'') |
    la''8 la'' si''\cresc si'' la'' la'' si'' si'' |
  }
  \tag #'violino2 {
    re'16 re' dod' dod' re'\cresc re' mi' mi' fad' fad' sol' sol' la' la' fad' fad' |
    sol'\p re' sol' si' sol' re' sol' si' sol' re' sol' si' re'' si' re'' sol'' |
    fad''8 fad'' sol''\cresc sol'' fad'' fad'' sol'' sol'' |
  }
>>
re'4\f~ re'16( mi'32 fad' sol' la' si' dod'') re''4~ re''16( mi''32 fad'' sol'' la'' si'' dod''') |
re'''8. re'16\p re'8. re'16 re'8. re'16 re'8. re'16 |
do'!4~\f do'16( re'32 mi' fad' sol' la' si') do''4~ do''16( re''32 mi'' fad'' sol'' la'' si'') |
sol''8. si16\p si8. si16 si8. si16 si8. si16 |
la4 do'' r <<
  \tag #'violino1 {
    do''4 |
    si'8 sol''32( fad'' sol'' la'' si''8) si''32( la'' si'' do''' re'''4) r8 fad'( |
    sol') sol''32( fad'' sol'' la'' si''8) si''32( la'' si'' do''' re'''4) r8 fad'( |
    sol') sol''32( fad'' sol'' la'' si''8) si''32( la'' si'' do''' re'''4) r8 sol''32( la'' sol'' fad'' |
    mi''8) r do'''8.( la''16) si''8.( sol''16) la''8.( fad''16) |
    sol''4 sol'4.\sf sol'8\p sol' sol' |
    \ru#4 { lab'16 lab' sol' sol' } |
    \ru#4 { fa'8\trill fa'16 fa' } |
    mib'8 mib' r re' r mib' r do' |
    si!4 sol'4.\sf sol'8\p sol' sol' |
    \ru#4 { lab'16 lab' sol' sol' } |
    si'!\sf do'' re'' mib'' fa'' sol'' lab'' sol'' fa'' mib'' re'' do'' si'\p do'' re'' si' |
    do''8 do''16(\trill si'32 do'' re''8) re''16(\trill do''32 re'' mib''8) mib''16(\trill re''32 mib'' do''8) do''16(\trill si'32 do'' |
    si'16)
  }
  \tag #'violino2 {
    la4 |
    sol16-! re'-! si-! re'-! sol re' si re' sol re' si re' la re' do' re' |
    sol16 re' si re' sol re' si re' sol re' si re' la re' do' re' |
    sol16 re' si re' sol re' si re' sol re' si re' sol sol' re' sol' |
    do' sol' mi' sol' mi' do'' la' do'' re' si' sol' si' do' la' fad' la' |
    sol'4 si4.\sf si8\p si si |
    fa'!16 fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' |
    re'8 re'4 re' re' re'8( |
    do') do' r si r do' r sol |
    sol4 si4.\sf si8\p si si |
    fa'16 fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' |
    fa'4\sf fa'2 fa'4\p |
    mib'4( sol'2 mib'4) |
    re'16
  }
>> sol'16\sf sol' sol' r si'\p si' si' r re'' re'' re'' r sol' sol' sol' |
r16 fa'!\sf fa' fa' r si'\p si' si' r re'' re'' re'' r fa' fa' fa' |
r mi'\sf mi' mi' r sol'\p sol' sol' r do'' do'' do'' r mi' mi' mi' |
r re'\sf re' re' r fa'\p fa' fa' r si' si' si' r re' re' re' |
<<
  \tag #'violino1 {
    do'4 r8 sol'16(\trill fad'32 sol' do''8) do'' re'' re'' |
    mi''( sol'') r do''' r mi''' r mi''( |
    fa''4) r8 fa'16(\trill mi'32 fa' si'8) si' do'' do'' re''( fa'') r si'' r re''' r re''( |
    mi'') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 si''( |
    do''') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 si''( |
    do''') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 do'''32( re''' do''' si'' |
    la''8) r la''8.( fa''16) mi''8.( sol''16) si'8.( re''16) |
    do''4
  }
  \tag #'violino2 {
    do'16 sol'( fad' sol' fad' sol' fa' re') mi'-. sol'-. mi'-. do'-. si-. re'-. si-. sol-. |
    \ru#4 { do' sol do' mi' } |
    si sol( si re' fa' sol' fa' re') fa'-. sol'-. fa'-. re'-. mi'-. fa'-. mi'-. do'-. |
    \ru#4 { si re' si sol } |
    do' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' re' fa' sol' fa' |
    mi' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' re' fa' sol' fa' |
    mi' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' sol do' mi' sol' |
    re' fa' la' fa' la re' fa' re' do' mi' sol' mi' re' fa' sol' fa' |
    mi'4
  }
>> r4 do''( la' |
fad') r la'( fad') |
<<
  \tag #'violino1 {
    re'16 la' la' la' la'\cresc la' si' si' do'' do'' re'' re'' mi'' mi'' fad'' fad'' |
    sol''8\f re''16(\trill do''32 re'' mi''8) do'''-! si'' re''16(\trill do''32 re'' mi''8) do'''-! |
    si''4 sol'4.\sf sol'8\p sol' sol' |
    \ru#4 { lab'16 lab' sol' sol' } |
    \ru#4 { fa'8\trill fa'16 fa' } |
    mib'8 mib' r re' r mib' r do' |
    si4 sol'4.\sf sol'8\p sol' sol' |
    \ru#4 { lab'16 lab' sol' sol' } |
    si'16\f do'' re'' mib'' fa'' sol'' lab'' sol'' fa'' mib'' re'' do'' si'\p do'' re'' si' |
    do''8 do''16(\trill si'32 do'' re''8) re''16(\trill do''32 re'' mib''8) mib''16(\trill re''32 mib'' fad''8) fad''16(\trill mi''32 fad'' |
    sol''4) r8 fad''16(\trill mi''32 fad'' sol''4) r8 fad''16(\trill mi''32 fad'' |
    sol''16) sol'' sol'' sol'' sol'' sol'' sol'' sol'' mib''\cresc mib'' mib'' mib'' do'' do'' do'' do'' |
  }
  \tag #'violino2 {
    re'16 fad' fad' fad' fad'\cresc fad' sol' sol' la' la' si' si' do'' do'' la' la' |
    si'8\f si'16(\trill la'32 si' do''8) mi''-! re''8 si'16(\trill la'32 si' do''8) mi''-! |
    re''4 si4.\sf si8\p si si |
    fa'!16 fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' |
    re'8 re'4 re' re' re'8( |
    do') do' r si r do' r sol |
    sol4 si4.\sf si8\p si si |
    fa'16 fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' fa' fa' mib' mib' |
    fa'8\f fa'4 fa' fa' fa'8\p |
    mib'4( sol'2 mib'4) |
    re'16 re' re' re' mib' mib' mib' mib' re' re' re' re' mib' mib' mib' mib' |
    re' sol' sol' sol' sol' sol' sol' sol' mib'\cresc mib' mib' mib' do' do' do' do' |
  }
>>
sol'16\f sol' sol' sol' r si'\p si' si' r re'' re'' re'' r sol' sol' sol' |
r fa'\sf fa' fa' r si'\p si' si' r re'' re'' re'' r fa' fa' fa' |
r mi'\sf mi' mi' r sol'\p sol' sol' r do'' do'' do'' r mi' mi' mi' |
r re'\sf re' re' r fa'\p fa' fa' r si' si' si' r re' re' re' |
<<
  \tag #'violino1 {
    do'4 r8 sol'16(\trill fad'32 sol' do''8) do'' re'' re'' |
    mi''( sol'') r do''' r mi''' r mi''( |
    fa''4) r8 fa'16(\trill mi'32 fa' si'8) si' do'' do'' |
    re''8( fa'') r si'' r re''' r re''( |
    mi'') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 si''( |
    do''') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 si''( |
    do''') sol''32( la'' sol'' fa'' mi''8) mi''32( fa'' mi'' re'' do''4) r8 do'''32( re''' do''' si'' |
    la''8) r la''8.( fa''16) mi''8.( sol''16) si'8.( re''16) |
    do''4 do'''32\trill( si'' do'''8.) r4 do''32(\trill si' do''8.) |
    r4 la''32(\trill sol'' la''8.) r4 si'32(\trill la' si'8.) |
    r4 do'''32\trill( si'' do'''8.) r4 do''32(\trill si' do''8.) |
    r4 la''32(\trill sol'' la''8.) r4 si'32(\trill la' si'8.) |
    do''4 r r8 do''16(\trill si'32 do'' sol'8) si''16(\trill la''32 si'' |
    do'''4) r r8 do''16(\trill si'32 do'' sol'8) si''16(\trill la''32 si'' |
    do'''4) r r8 do''16(\trill si'32 do'' sol'8) si''16(\trill la''32 si'' |
    do'''4) r8 do''16(\trill si'32 do'' mi''4) r8 do''16(\trill si'32 do'' |
    la''4) r8 re''16(\trill dod''32 re'' fa''4) r8 fa'16(\trill mi'32 fa' |
    mi'4) r8 do''!16\trill( si'32 do'' mi''4) r8 do''16(\trill si'32 do'' |
    la''4) r8 re''16(\trill dod''32 re'' fa''4) r8 fa'16(\trill mi'32 fa' |
    mi'4) r8 sol''16(\trill fad''32 sol'' do'''4)\cresc r8 do''16(\trill si'32 do'' |
    la''4) r8 fa'16(\trill mi'32 fa' sol8) r <sol' re'' si''> r |
    <sol' mi'' do'''>4\f \ru#3 { do'''16 sol'' mi'' sol'' } |
    do'''4
  }
  \tag #'violino2 {
    do'16 sol'( fad' sol' fad' sol' fa' re') mi'-. sol'-. mi'-. do'-. si-. re'-. si-. sol-. |
    \ru#4 { do' sol do' mi' } |
    si sol( si re' fa' sol' fa' re') fa'-. sol'-. fa'-. re'-. mi'-. fa'-. mi'-. do'-. |
    \ru#4 { si re' si sol } |
    do' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' re' fa' sol' fa' |
    mi' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' re' fa' sol' fa' |
    mi' sol do' mi' sol' do'' sol' fa' mi' sol' mi' do' sol do' mi' sol' |
    re' fa' la' fa' la re' fa' re' do' mi' sol' mi' re' fa' sol' fa' |
    mi'-. sol-. do'-. re'-. mi' re' mi' fa' sol' fa' mi' re' do' si la sol |
    la si do' dod' re' dod' re' mi' fa' mi' re' mi' fa' la' sol' fa' |
    mi'16 re' do'! re' mi' re' mi' fa' sol' fa' mi' re' do' si la sol |
    la si do' dod' re' dod' re' mi' fa' mi' re' mi' fa' la' sol' fa' |
    mi' si do'! re' mi' fa' sol' fa' mi' sol' mi' do' si re' si sol |
    do' si do' re' mi' fa' sol' fa' mi' sol' mi' do' si re' si sol |
    do' si do' re' mi' fa' sol' fa' mi' sol' mi' do' si re' si sol |
    do' sol do' mi' sol' mi' sol' mi' do' la do' mi' la' mi' la' mi' |
    fa'16 re' fa' la' re'' dod'' re'' do'' si' la' sol' fa' mi' re' do' si |
    do' sol do' mi' sol' mi' sol' mi' do' la do' mi' la' mi' la' mi' |
    fa' re' fa' la' re'' dod'' re'' do'' si' la' sol' fa' mi' re' do' si |
    do' sol do' mi' sol' mi' sol' do'' mi''\cresc la do' mi' la' fa' la' do'' |
    re'' mi'' fa'' mi'' re'' mi'' re'' do'' si' do'' re'' do'' si' la' sol' fa' |
    mi'\f do' mi' sol' \ru#3 { do'' mi'' sol'' mi'' } |
    do''4
  }
>> mi''8. mi''16 sol''8. sol''16 mi''8. mi''16 |
do''4 do' do' do' |
do'2 r |
