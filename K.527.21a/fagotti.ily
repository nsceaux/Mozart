\clef "bass" \tag#'fagotti <>^"a 2." do4\f do' do r |
R1*3 |
r8 <>\p <<
  \tag #'(fagotto1 fagotti) { mi32( re mi fa sol8) sol-! }
  \tag #'(fagotto2 fagotti) { do32( si, do re mi8) mi-! }
>> r2 |
r8 <<
  \tag #'(fagotto1 fagotti) { la32( sol la si do'8) do'-! }
  \tag #'(fagotto2 fagotti) { fa32( mi fa sol la8) la-! }
>> r2 |
r8 <<
  \tag #'(fagotto1 fagotti) { mi32( re mi fa sol8) sol-! }
  \tag #'(fagotto2 fagotti) { do32( si, do re mi8) mi-! }
>> r2 |
<<
  \tag #'(fagotto1 fagotti) { mi2( re) | mi4 }
  \tag #'(fagotto2 fagotti) { do2( si,) | do4 }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    do'4 do'8. do'16 do'4
  }
  \tag #'(fagotto2 fagotti) {
    mi4 mi8. mi16 mi4
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'16( do' re' mi') | do'8[-! mi]-! sol-! }
  { sol4 | do8[-! mi]-! sol-! }
  { s4 | s\f }
>> r8 r4 r8 \tag#'fagotti <>^"a 2." sol, |
do[-! mi]-! sol-! r r4 r8 sol, |
do4 r r2 |
R1*9 |
<<
  \tag #'(fagotto1 fagotti) {
    si1 |
    la4 si la si |
    la
  }
  \tag #'(fagotto2 fagotti) {
    sol1 |
    fad4 sol fad sol |
    fad
  }
  { s1\p | s4 s2.\cresc | s4\f }
>> r4 r2 |
R1*7 |
r4 <>\p <<
  \tag #'(fagotto1 fagotti) { do'4( si la) | }
  \tag #'(fagotto2 fagotti) { la4( sol fad) | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol4 } { sol }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1 |
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    si1 |
    do'4( sol2 mib4) |
    re
  }
  \tag #'fagotto2 { R1*3 | r4 }
>> <>\sfp <<
  \tag #'(fagotto1 fagotti) {
    sol2. |
    lab8( sol lab sol lab sol lab sol) |
    fa2. fa4 |
    mib( re mib) mib( |
    re)
  }
  \tag #'(fagotto2 fagotti) {
    si,2. |
    fa8( mib fa mib fa mib fa mib) |
    re2. re4 |
    do( si, do) do( |
    si,)
  }
  { s2. | s1 | s2.\sf s4\p | }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1*2 |
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    re'2~ re'16( dod' mi' re' do' si la sol) |
    do'4 r r2 |
    R1*10 |
  }
  \tag #'fagotto2 R1*14
>> \allowPageTurn
r2 r8 <>\f <<
  \tag #'(fagotto1 fagotti) { re'8-! mi'-! mi'-! | re'4 }
  \tag #'(fagotto2 fagotti) { si8-! do'-! do'-! | si4 }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1 |
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    si1\p |
    do'4( sol2 mib8. do16) |
    si,4 r r2 |
    R1 |
  }
  \tag #'fagotto2 R1*5
>>
\tag#'fagotti <>^"a 2." lab2\sfp( sol4 fa) |
mib( sol mib do) |
sol, r8 <<
  \tag #'(fagotto1 fagotti) {
    fad'8( sol'4) s8 fad'( |
    sol'2) mib'4 do' |
    sol
  }
  \tag #'(fagotto2 fagotti) {
    fad8( sol4) s8 fad( |
    sol2) mib4 do |
    sol,
  }
  { s8 s4 r8 s | s2 s\cresc | s4\! }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1*2 |
    \tag#'fagotti <>^\markup\concat { 1 \super o }
    re'2\p~ re'16( dod' mi' re' do' si la sol) |
    do'4 r r2 |
  }
  \tag #'fagotto2 R1*4
>>
<>\p <<
  \tag #'(fagotto1 fagotti) { mi'1( | re'4) }
  \tag #'(fagotto2 fagotti) { do'1( | si4) }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) { re'1( | mi'4) }
  \tag #'(fagotto2 fagotti) { si1( | do'4) }
>> r4 r2 |
R1*2 |
r4 <<
  \tag #'(fagotto1 fagotti) { fa'4( mi' re') | }
  \tag #'(fagotto2 fagotti) { re'4( do' si) | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'4 } { do'4 }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1*3 |
    r2 \tag#'fagotti <>^\markup\concat { 1 \super o }
    do'16 re' mi' fa' sol' fa' mi' re' |
    do'8 mi' sol si do'16 re' mi' fa' sol' fa' mi' re' |
    do'8 mi' sol si do'16 re' mi' fa' sol' fa' mi' re' |
    do'4 r
  }
  \tag #'fagotto2 { R1*6 r2 }
>> <<
  \tag #'(fagotto1 fagotti) { mi'4 s | fa' s re' s | }
  \tag #'(fagotto2 fagotti) { do'4 s | la s si s | }
  { s4 r | s r s r | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'4 } { do' }
>> r4 r2 |
R1 |
r4 <<
  \tag #'(fagotto1 fagotti) {
    mi'8. mi'16 mi'4 fa'8. fa'16 |
    fa'4 fa'8. fa'16 fa'4 re'8. re'16 |
  }
  \tag #'(fagotto2 fagotti) {
    do'8. do'16 do'4 la8. la16 |
    la4 re'8. re'16 re'4 si8. si16 |
  }
  { s4 s2\cresc }
>>
\tag#'fagotti <>^"a 2." do'4\f do8 do' do do' do do' |
do4 mi sol mi |
do do do do |
do2 r |
