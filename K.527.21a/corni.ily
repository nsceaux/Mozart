\clef "treble" \transposition do
<>\f <<
  \tag #'(corno1 corni) { do''4 do'' do'' }
  \tag #'(corno2 corni) { do' do' do' }
>> r4 |
R1*3 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do''~ | do'' | }
  \tag #'(corno2 corni) { do'~ | do'~ | do' | }
>>
R1*2 |
<<
  \tag #'(corno1 corni) { do''4 do''8. do''16 do''4 re'' | }
  \tag #'(corno2 corni) { mi'4 mi'8. mi'16 mi'4 sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''8-![ mi'-!] sol'-! }
  { do'-![ mi'-!] sol'-! }
>> r8 r4 r8 \tag#'corni <>^"a 2." sol'8 |
do'[-! mi']-! sol'-! r r4 r8 sol'8 |
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { mi' }
>> r4 r2 |
R1*3 |
<>\p <<
  \tag #'(corno1 corni) { \tag #'corni \once\tieUp sol'1~ | sol'4 }
  \tag #'(corno2 corni) { sol'1 | do'4 }
>> r4 r2 |
<<
  \tag #'(corno1 corni) {
    sol'4 sol'8. sol'16 sol'4 sol' |
    sol'
  }
  \tag #'(corno2 corni) {
    sol4 sol8. sol16 sol4 sol |
    do'
  }
>> r4 r2 |
R1*3 |
\tag#'corni <>^"a 2." << { s4 s\cresc } re''1~ >> |
re''4\f r4 r2 |
R1*12 |
<>\p <<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol' |
    sol'2. sol'4 |
    sol'1~ |
    sol'4
  }
  \tag #'(corno2 corni) {
    sol1~ |
    sol |
    sol2. sol4 |
    sol1~ |
    sol4
  }
  { s1*2 | s2.\sf s4\p | }
>> r4 r2 |
R1*14 |
r2 r8 \tag#'corni <>^"a 2." sol'8-!\f sol'-! sol'-! |
sol'4 r r2 |
R1*4 |
<<
  \tag #'(corno1 corni) {
    \tag #'corni \once\tieUp sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4 sol' sol' sol' |
    sol'
  }
  \tag #'(corno2 corni) {
    sol'1 |
    sol~ |
    sol~ |
    sol~ |
    sol4 sol sol sol |
    sol
  }
  { s1\p | s\sfp | s1*2 | s2 s\cresc | s4\! }
>> r4 r2 |
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol'4 }
  \tag #'(corno2 corni) { sol1~ | sol4 }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { sol'1~ | sol'4 }
  \tag #'(corno2 corni) { sol1~ | sol4 }
>> r4 r2 |
R1*7 |
r2 r4 <<
  \tag #'(corno1 corni) {
    re''4( |
    do'') s s re''( |
    do'') s s re''( |
    do'')
  }
  \tag #'(corno2 corni) {
    sol'4( |
    mi') s s sol'( |
    mi') s s sol'( |
    mi')
  }
  { s4 | s r r s | s r r s | }
>> r4 r2 |
R1*4 |
r2 <<
  \tag #'(corno1 corni) {
    sol'4 sol'8. sol'16 |
    mi'8 sol' mi' sol' mi' sol' mi' sol' |
    mi'4 mi'' sol'' mi'' |
    do'' sol' sol' sol' |
    sol'2
  }
  \tag #'(corno2 corni) {
    sol4 sol8. sol16 |
    do'8 mi' do' mi' do' mi' do' mi' |
    do'4 mi' sol' mi' |
    do' mi' mi' mi' |
    mi'2
  }
  { s2\cresc | s1\f }
>> r2 |
