\version "2.19.80"
\include "common.ily"

\opusTitle "K.527 – N°21a – Per queste tue manine"

\header {
  title = \markup\center-column {
    \line\italic { Don Giovanni }
    \line { N°21a – Duetto: \italic { Per queste tue manine } }
  }
  opus = "K.527"
  date = "1787"
  copyrightYear = "2019"
}

\paper {
  systems-per-page = #(if (symbol? (ly:get-option 'part))
                          #f
                          2)
}
\includeScore "K.527.21A"
