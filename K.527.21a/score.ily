\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global
        \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = "Corni in C"
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Zerlina
      shortInstrumentName = \markup\character Z.
    } \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Leporello
      shortInstrumentName = \markup\character L.
    } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"

    \new Staff \with { \vccbInstr \consists "Metronome_mark_engraver" } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*3\pageBreak
        s1*5\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*3\break s1*4\pageBreak
        s1*3\break s1*3\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
