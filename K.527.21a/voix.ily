<<
  \tag #'voix1 {
    \clef "treble" R1*10 |
    r4 r8 do'' mi'' sol'' sol' si' |
    do'' do'' r do'' mi'' sol'' sol' si' |
    do'' do'' sol''4~ sol''16[ fa''] mi'' re'' do''[ si'] la'[ sol'] |
    fa'8 fa' r re'' fa''16[ mi''] re''[ do''] si'[ la'] sol'[ fa'] |
    mi'8 mi' r8. do''16 la'8. re''16 si'8. mi''16 |
    do''8. fa''16 re''8. sol''16 mi''4 r |
    sol'4 do''8. mi'16 fa'4. sol'8 |
    mi'8.([ do'16 sol'8. mi'16] do''8.[ sol'16 mi''8. do''16]) |
    sol''4 mi''8. do''16 sol'4. si'8 |
    do''4 r r2 |
    r r4 r8 la' |
    re''4. re''8 re''4. re''8 |
    si'8 re'' r re'' si' re'' sol' sol'' |
    fad'' re'' re'' sol'' fad'' re'' re'' sol'' |
    fad''4 re'' r2 |
    r re''~ |
    re''4 re''8. re''16 re''4. mi''16[ fad''] |
    sol''4 sol' r2 |
    do''4 do''8. do''16 \appoggiatura re''16 do''8([ si'16 do''] re''8) do'' |
    si' si' r re'' si' sol' fad' fad'' |
    sol'' re'' r re'' si' sol' fad' fad'' |
    sol''4 r8 re'' si' re'' sol' si' |
    mi'4 mi''8 mi'' re'' si' \appoggiatura re''16 do''8 si'16[ la'] |
    sol'4 r r2 |
    R1*3 |
    r2 r4 sol' |
    si'8.[ do''16] si'8. do''16 si'8.[ do''16] si'8. do''16 |
    re''4 re'' r r8 re'' |
    mib''4 re''8 re'' mib'' mib'' do'' do'' |
    sol'4 r r2 |
    R1*3 |
    r4 r8 sol' do'' do'' re'' re'' |
    mi''8.[ do''16] sol''4 sol''16[ fa''] mi'' re'' do''[ si'] la'[ sol'] |
    fa'8 fa' r fa' si' si' do'' do'' |
    re''8.[ si'16] fa''4 fa''16[ mi''] re'' do'' si'[ la'] sol'[ fa'] |
    mi'8 mi' r do'' mi'' do'' sol'' si' |
    do''8. sol'16 sol'8 do'' mi'' do'' sol'' si' |
    do''4 r8 do'' mi'' sol'' do'' mi'' |
    la'8. re''16 re''8 fa'' mi'' do'' sol'' si' |
    do''4 r r2 |
    R1 |
    r8 la' la' si' do'' re'' mi'' fad'' |
    sol'' re'' mi'' do'' sol'' re'' mi'' do'' |
    sol''4 sol' r2 |
    R1*3 |
    r2 r4 sol' |
    si'8.[ do''16] si'8. do''16 si'8.[ do''16] si'8. do''16 |
    re''4 re'' r r8 re'' |
    mib''4 re''8 re'' mib'' mib'' do'' do'' |
    si' re'' mib'' do'' si' re'' mib'' do'' |
    si'4 sol'' mib''8. mib''16 do''8. do''16 |
    sol'4 r r2 |
    R1*3 |
    r4 r8 sol' do'' do'' re'' re'' |
    mi''8.[ do''16] sol''4 sol''16[ fa''] mi'' re'' do''[ si'] la'[ sol'] |
    fa'8 fa' r fa' si' si' do'' do'' |
    re''8.[ si'16] fa''4 fa''16[ mi''] re'' do'' si'[ la'] sol'[ fa'] |
    mi'8 mi' r do'' mi'' do'' sol'' si' |
    do''8. sol'16 sol'8 do'' mi'' do'' sol'' si' |
    do''4 r8 do'' mi'' sol'' do'' mi'' |
    la'8. re''16 re''8 fa'' mi'' do'' sol'' si' |
    do'' mi'' sol'' mi'' do'' mi'' sol' do'' |
    la'4 r r2 |
    r8 mi'' sol'' mi'' do'' mi'' sol' do'' |
    la'4 r r r8 sol' |
    do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' do''8 mi'' sol' si' |
    do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' do''8 mi'' sol' si' |
    do''16 re'' mi'' fa'' sol'' fa'' mi'' re'' do''8 mi'' sol' si' |
    do'' do'' mi''4. do''8 mi''4~ |
    mi''8 re'' fa''4. sol'8 la' si' |
    do'' do'' mi''4. do''8 mi''4~ |
    mi''8 re'' fa''4. sol'8 la' si' |
    do'' do''^\f mi''4. do''8 fa''4~ |
    fa''8 re'' la''2 sol''16[ fa'' mi'' re''] |
    do''4 r r2 |
    R1*3 |
  }
  \tag #'voix2 {
    \clef "bass" r2 r4 r8 mi |
    mi4( do8.) sol16 sol4( mi8.) do'16 |
    do'4 sol r2 |
    fa4 fa8 fa fa4. sol8 |
    mi4 mi r sol |
    la4. fa8 do'4. la8 |
    sol4.( do'8) do'4 r |
    mi4 fa8. sol16 fa4 re |
    sol8.([ mi16 do'8. sol16] mi'8.[ do'16 sol8. mi16]) |
    do4 do8 do do4 re16[ do re mi] |
    do4 r r2 |
    R1*8 |
    r4 do'4. do'16 do' la8 la |
    fad8 fad la4. la16 la fad8 fad |
    re re r4 r2 |
    R1*2 |
    r2 re'~ |
    re'4 re8. re16 re4. re8 |
    do4 do r2 |
    sol4 si8. re'16 si4. sol8 |
    fad4 fad r2 |
    r r4 r8 re |
    sol si re' si sol sol r re |
    sol si re' si sol4 r8 si, |
    do4 r8 la, re4 r8 re |
    sol,4 sol2. |
    lab8.[ sol16] lab8.[ sol16] lab8.[ sol16] lab8.[ sol16] |
    fa!4 fa r2 |
    mib8 r re8. re16 mib8 r do r |
    si,!4 r r2 |
    R1*3 |
    r2 r4 r8 sol |
    fa!4 fa r8 fa fa fa |
    mi4 mi r r8 mi |
    re4 r8 re re4 r8 re |
    do4 do r2 |
    r r4 do' |
    si r sol r |
    sol2. r8. sol16 |
    do'8 do' r4 r sol8 r |
    sol8 r sol r sol r sol r |
    mi4 r r do8 r |
    fa r re r sol r sol, r |
    do4 do'4. do'16 do' la8 la |
    fad8 fad la4. la16 la fad8 fad |
    re re r4 r2R1 |
    r4 sol2. |
    lab8.[ sol16] lab8.[ sol16] lab8.[ sol16] lab8.[ sol16] |
    fa!4 fa r2 |
    mib8 r re8. re16 mib8 r do r |
    si,!4 r r2 |
    R1*5 |
    r2 r4 r8 sol |
    fa!4 fa r8 fa fa fa |
    mi4 mi r r8 mi |
    re4 r8 re re4 r8 re |
    do4 do r2 |
    r r4 do' |
    si r sol r |
    sol2. r8 sol |
    do' do' r4 r sol8 r |
    sol r sol r sol r sol r |
    mi4 r r do8 r |
    fa r re r sol r sol, r |
    do4 r r2 |
    r8 re fa re si, re sol, sol |
    mi do r4 r2 |
    r8 re fa re si, re sol, sol |
    mi do r4 r fa^\sf |
    mi2.^\p fa4^\sf |
    mi2.^\p fa4^\sf |
    mi4^\p do la4 r |
    fa4 r8 fa sol4 r8 sol |
    do4 r la, r |
    fa, r8 fa, sol,4 r8 sol, |
    do4. do'8^\f la4. la8 |
    fa4. fa8 sol4 sol |
    do r r2 |
    R1*3 |
  }
>>
