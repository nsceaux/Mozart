\clef "treble"
<>\f <<
  \tag #'(flauto1 flauti) { mi''4 sol'' do''' }
  \tag #'(flauto2 flauti) { do'' mi'' sol'' }
>> r4 |
R1*3 |
r2 r8 <>\p <<
  \tag #'(flauto1 flauti) { mi''32( re'' mi'' fa'' sol''8) sol''-! | }
  \tag #'(flauto2 flauti) { do''32( si' do'' re'' mi''8) mi''-! | }
>>
r2 r8 <<
  \tag #'(flauto1 flauti) { la''32( sol'' la'' si'' do'''8) do'''-! | }
  \tag #'(flauto2 flauti) { fa''32( mi'' fa'' sol'' la''8) la''-! | }
>>
r2 r8 <<
  \tag #'(flauto1 flauti) { mi''32( re'' mi'' fa'' sol''8) sol''-! | }
  \tag #'(flauto2 flauti) { do''32( si' do'' re'' mi''8) mi''-! | }
>>
R1*2 |
<<
  \tag #'(flauto1 flauti) { do'''4 do'''8. do'''16 do'''4 }
  \tag #'(flauto2 flauti) { mi''4 mi''8. mi''16 mi''4 }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { re'''16( do''' re''' mi''') | do'''8[-! mi'']-! sol''-! }
  { sol''4 | mi''8[-! mi'']-! sol''-! }
  { s4 | s\f }
>> r8 r4 r8 <<
  \tag #'(flauto1 flauti) { si''8 | }
  \tag #'(flauto2 flauti) { sol'' | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''8[-! mi'']-! sol'' }
  { sol''[-! mi'']-! sol'' }
>> r8 r4 r8 <<
  \tag #'(flauto1 flauti) { si''8 | do'''4 }
  \tag #'(flauto2 flauti) { sol''8 | sol''4 }
>> r4 r2 |
R1*3 |
<>\p <<
  \tag #'(flauto1 flauti) { mi''2( re'' | mi''4) }
  \tag #'(flauto2 flauti) { do''2( si' | do''4) }
>> r4 r2 |
<<
  \tag #'(flauto1 flauti) { do'''4 do'''8. do'''16 si''4 si'' | do''' }
  \tag #'(flauto2 flauti) { sol''4 sol''8. sol''16 sol''4 sol'' | sol'' }
>> r4 <<
  \tag #'(flauto1 flauti) {
    \tag#'flauti <>^\markup\concat { 1 \super o }
    do'''4( la'' |
    fad'') r la''( fad'') |
    re'' r r2 |
    R1 |
  }
  \tag #'flauto2 { r2 | R1*3 | }
>>
r8 \tag#'flauti <>^"a 2." re'''8 re'''\cresc re''' re''' re''' re''' re''' |
re'''4\f r r2 |
<>\p <<
  \tag #'(flauto1 flauti) {
    la''1~ |
    la'' |
    si'' |
    do'''4 s fad''! s |
    sol''
  }
  \tag #'(flauto2 flauti) {
    fad''1~ |
    fad'' |
    sol'' |
    fad''4 s do'' s |
    si'
  }
  { s1 | s\f | s\p | s4 r s r | }
>> r4 r2 |
r2 r4 <<
  \tag #'(flauto1 flauti) { do'''4( | si'') }
  \tag #'(flauto2 flauti) { fad''4( | sol'') }
>> r4 r2 |
r4 <<
  \tag #'(flauto1 flauti) { do'''4( si'' la'') | }
  \tag #'(flauto2 flauti) { la''( sol'' fad'') | }
>>
\twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''4 } { sol'' }
>> <>\sfp <<
  \tag #'(flauto1 flauti) {
    sol''2. |
    lab''8( sol'' lab'' sol'' lab'' sol'' lab'' sol'') |
    fa''1 |
    mib''4( re'' mib'' do'') |
    si'
  }
  \tag #'(flauto2 flauti) {
    si'2. |
    fa''!8( mib'' fa'' mib'' fa'' mib'' fa'' mib'') |
    re''1 |
    do''4( si' do'' sol') |
    sol'
  }
>> r4 r2 |
R1 |
<<
  \tag #'(flauto1 flauti) {
    \tag#'flauti <>^\markup\concat { 1 \super o }
    si''1*3/4\sf s4\p |
    do'''4( re''' mib''' do''') |
    sol'' r4 r2 |
  }
  \tag #'flauto2 R1*3
>>
R1*2 |
<>\fp <<
  \tag #'(flauto1 flauti) { fa''1( | mi''!4) }
  \tag #'(flauto2 flauti) { si'1( | do''4) }
>> r4 r2 | \allowPageTurn
R1*7 |
<<
  \tag #'(flauto1 flauti) {
    r2 \tag#'flauti <>^\markup\concat { 1 \super o }
    do'''4(\p la'' |
    fad'') r la''( fad'') |
    re'' r r2 |
  }
  \tag #'flauto2 R1*3
>>
r2 r8 <>\f <<
  \tag #'(flauto1 flauti) { re'''8-! mi'''-! mi'''-! | re'''4 }
  \tag #'(flauto2 flauti) { si''8-! do'''-! do'''-! | si''4 }
>> r4 r2 |
R1 |
<<
  \tag #'(flauto1 flauti) {
    \tag#'flauti <>^\markup\concat { 1 \super o }
    lab''2(\p sol''4 fa'') |
    mib''( re'' mib'' do'') |
    sol' r4 r2 |
  }
  \tag #'flauto2 R1*3
>>
R1 |
<>\sfp <<
  \tag #'(flauto1 flauti) {
    re'''1 |
    mib'''4( re''') mib'''2 |
    re'''4( mib''' re''' mib''') |
    si'' sol'''
  }
  \tag #'(flauto2 flauti) {
    si''1 |
    do'''4( si'') do'''2 |
    si''4( do''' si'' do''') |
    si'' sol''
  }
>> \tag#'flauti <>^"a 2." mib'''4\cresc do''' |
sol''4 r r2 |
R1*2 |
<>\fp <<
  \tag #'(flauto1 flauti) { fa''1( | mi''!4) }
  \tag #'(flauto2 flauti) { si'1( | do''4) }
>> r4 r2 |
R1*4 |
r2 r4 <>\p <<
  \tag #'(flauto1 flauti) { si''4( | do'''4) }
  \tag #'(flauto2 flauti) { fa''4( | mi''4) }
>> r4 r2 |
R1*3 |
<<
  \tag #'(flauto1 flauti) {
    sol''1 |
    fa'' |
    mi''4 s s si''( |
    do''') s s si''( |
    do''') s s si''( |
    do''')
  }
  \tag #'(flauto2 flauti) {
    mi''1 |
    re'' |
    do''4 s s fa''( |
    mi'') s s fa''( |
    mi'') s s fa''( |
    mi'')
  }
  { s1*2 | s4 r r s | s r r s | s r r s | }
>> r4 r2 |
R1 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''1~ | do'''2 si'' | }
  { mi''1 | re'' | }
>>
<<
  \tag #'(flauto1 flauti) {
    do'''4 mi'''8. mi'''16 mi'''4 fa'''8. fa'''16 |
    fa'''4 re'''8. re'''16 re'''4 re'''8. re'''16 |
  }
  \tag #'(flauto2 flauti) {
    mi''4 do'''8. do'''16 do'''4 do'''8. do'''16 |
    re'''4 do'''8. do'''16 si''4 si''8. si''16 |
  }
  { s2 s\cresc | }
>>
<>\f \twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''8 mi''' do''' mi''' do''' mi''' do''' mi''' | }
  { do''' do''' sol'' do''' sol'' do''' sol'' do''' | }
>>
<<
  \tag #'(flauto1 flauti) { do'''4 }
  \tag #'(flauto2 flauti) { sol'' }
>> \tag#'flauti <>^"a 2." mi'''4 sol''' mi''' |
do''' <<
  \tag #'(flauto1 flauti) { do'''4 do''' do''' | do'''2 }
  \tag #'(flauto2 flauti) { sol''4 sol'' sol'' |
    sol''2 }
>> r2 |
