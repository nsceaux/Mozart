\clef "bass" do4\f do' do r |
do\p r do r |
r do mi do |
sol,1 |
do4 r do r |
do r do r |
do r do r |
sol1 |
do4 r r2 |
sol4 r sol, r |
do8[-!\f mi]-! sol-! r r4 r8 sol, |
do[-! mi]-! sol-! r r4 r8 sol, |
do4 r do\p do |
sol, sol, sol, sol, |
do8. do16 mi8. do16 fa8. re16 sol8. mi16 |
la8. fa16\cresc si8. sol16 do'4\f r |
sol,1\p |
do4 r r2 |
R1 |
r2 do'4( la |
fad) r la( fad) |
re8 re re\cresc re re re re re |
re\p re re re re re re re |
re re re\cresc re re re re re |
re4~ re16( mi32 fad sol la si dod') re'2~ |
re'4 re\p re re |
do!4\f~ do16( re32 mi fad sol la si) do'2 |
si4 si,\p si, si, |
la, r re r |
sol, sol r re |
sol, sol r re |
sol, sol r re |
do la, re re |
sol,8 sol sol\sf sol sol\p sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol\sf sol sol\p sol sol sol |
sol sol sol sol sol sol sol sol |
sol\sf sol sol sol sol sol sol\p sol |
sol sol sol sol sol sol sol sol |
sol,8\f r si\p r re' r sol r |
fa!\f r si\p r re' r fa r |
mi\f r sol\p r do' r mi r |
re\f r fa\p r si r re r |
do4 r r2 |
do8 r do' r do r do' r |
sol4 r r2 |
sol,8 r sol r sol, r sol r |
do4 do' r sol, |
do4 do' r sol, |
do do' r mi |
fa r sol r |
do r do'( la |
fad) r la( fad) |
re8 re re\cresc re re re re re |
sol,\f sol sol, sol sol, sol sol, sol |
sol, sol sol\sf sol sol\p sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol\sf sol sol\p sol sol sol |
sol sol sol sol sol sol sol sol |
sol\sf sol sol sol sol sol sol\p sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol mib\cresc mib do do |
sol,\f r si\p r re' r sol r |
fa!\f r si\p r re' r fa r |
mi\f r sol\p r do' r mi r |
re\f r fa\p r si r re r |
do4 r r2 |
do8 r do' r do r do' r |
sol4 r r2 |
sol,8 r sol r sol, r sol r |
do4 do' r sol, |
do do' r sol, |
do do' r mi |
fa r sol r |
do r mi r |
fa r sol r |
do r mi r |
fa r sol r |
do r r sol, |
do r r sol, |
do r r sol, |
do r la r |
fa r sol r |
do r la r |
fa r sol r |
do' r la\cresc r |
fa r sol r |
do8\f do' do do' do do' do do' |
do4 mi sol mi |
do do do do |
do2 r |
