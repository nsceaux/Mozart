% LEPORELLO
\tag #'voix2 {
  Per que -- ste tue ma -- ni -- ne,
  can -- di -- de e te -- ne -- rel -- le,
  per que -- sta fre -- sca pel -- le,
  ab -- bi pie -- tà di me, __
  ab -- bi pie -- tà di __ me!
}
% ZERLINA
\tag #'voix1 {
  Non v’è pie -- tà, bric -- co -- ne,
  non v’è pie -- tà, bric -- co -- ne;
  son __ u -- na ti -- gre i -- ra -- ta,
  un a -- spi -- de, un le -- o -- ne
  no, no, pie -- tà  non v’è,
  pie -- tà  non v’è,
  no, no, pie -- tà  non v’è, __
  no, no, pie -- tà  non v’è!
}
% LEPORELLO
\tag #'voix2 {
  Ah di fug -- gir si pro -- vi,
  ah di fug -- gir si pro -- vi!
}
% ZERLINA
\tag #'voix1 {
  Sei mor -- to se ti mo -- vi,
  sei mor -- to se ti mo -- vi,
  se ti mo -- vi,
  se ti mo -- vi!
}
% LEPORELLO
\tag #'voix2 {
  Bar -- ba -- ri, in -- giu -- sti De -- i,
  bar -- ba -- ri, in -- giu -- sti De -- i,
  in ma -- no di co -- ste -- i
  chi ca -- pi -- tar mi fe’,
  chi ca -- pi -- tar mi fe’?
}
% ZERLINA
\tag #'voix1 {
  Bar -- ba -- ro tra -- di -- to -- re,
  bar -- ba -- ro tra -- di -- to -- re,
  %Lo le -- ga con u -- na cor -- da, e le -- ga la cor -- da all fi -- ne -- stra.
  del tuo pa -- dro -- ne il co -- re a -- ves -- si qui con te,
  del tuo pa -- dro -- ne il co -- re a -- ves -- si qui con te!
}
% LEPORELLO
\tag #'voix2 {
  Deh! non mi strin -- ger tan -- to,
  l’a -- ni -- ma mia sen va!
}
% ZERLINA
\tag #'voix1 {
  Sen va -- da, sen va -- da, o re -- sti: in -- tan -- to
  non par -- ti -- rai di qua!
}
% LEPORELLO
\tag #'voix2 {
  Che stret -- te, o Dei, che bot -- te,
  è gior -- no, ov -- ve -- ro è not -- te?
  Che scos -- se di tre -- muo -- to,
  che bu -- ia o -- scu -- ri -- tà,
  che bu -- ia o -- scu -- ri -- tà!
  Ah di fug -- gir si pro -- vi,
  ah di fug -- gir si pro -- vi!
  Deh non mi strin -- ger tan -- to,
  l’a -- ni -- ma mia sen va!
  Che stret -- te, o Dei, che bot -- te,
  è gior -- no, ov -- ve -- ro è not -- te?
  Che scos -- se di tre -- muo -- to,
  Che bu -- ia o -- scu -- ri -- tà,
  che bu -- ia o -- scu -- ri -- tà!
  o Dei, che stret -- te, o Dei, che bot -- te,
  è not -- te o gior -- no, è gior -- no o not -- te?
  Che scos -- se di tre -- muo -- to,
  che bu -- ia o -- scu -- ri -- tà,
  che bu -- ia o -- scu -- ri -- tà,
  che bu -- ia, bu -- ia o -- scu -- ri -- tà!
}
% ZERLINA
\tag #'voix1 {
  Di gio -- ia e di di -- let -- to
  sen -- to bril -- lar -- mi il pet -- to,
  di gio -- ia e di di -- let -- to
  sen -- to bril -- lar -- mi il pet -- to.
  Co -- sì, co -- sì cogl’ uo -- mi -- ni,
  co -- sì, co -- sì si fa,
  co -- sì, co -- sì cogl’ uo -- mi -- ni,
  co -- sì, co -- sì si fa!
  
  Sei mor -- to, mor -- to, mor -- to, mor -- to, se ti mo -- vi,
  se ti mo -- vi!
  Sen va -- da, sen va -- da, o re -- sti: in -- tan -- to
  non par -- ti -- rai di qua,
  non par -- ti -- rai,
  non par -- ti -- rai,
  non par -- ti -- rai di qua!

  Di gio -- ia e di di -- let -- to
  sen -- to bril -- lar -- mi il pet -- to,
  di gio -- ia e di di -- let -- to
  sen -- to bril -- lar -- mi il pet -- to.
  Co -- sì, co -- sì cogl’ uo -- mi -- ni,
  co -- sì, co -- sì si fa,
  co -- sì, co -- sì cogl’ uo -- mi -- ni,
  co -- sì, co -- sì si fa,
  co -- sì, co -- sì, co -- sì si fa,
  co -- sì, co -- sì, co -- sì si fa,
  co -- sì, co -- sì, co -- sì, co -- sì, co -- sì, co -- sì si fa,
  co -- sì, co -- sì, co -- sì, co -- sì, co -- sì, si fa,
  co -- sì, co -- sì, co -- sì, co -- sì, co -- sì, si fa,
  co -- sì, co -- sì, __ co -- sì, co -- sì, si fa,
  co -- sì, co -- sì, __ co -- sì, co -- sì, si fa,
  co -- sì, co -- sì, __ co -- sì, si __ fa!
}
