\clef "alto" do4\f do' do r |
sol\p r sol r |
r sol2 sol4 |
sol1~ |
sol4 r do' r |
do' r do' r |
do' r do' r |
sol1~ |
sol4 r r2 |
R1 |
do'8[-!\f mi']-! sol'-! r r4 r8 sol |
do'8[-! mi']-! sol'-! r r4 r8 sol |
do'8. sol16\p sol8. sol16 sol8. sol16 sol8. sol16 |
sol8. sol16 sol8. sol16 sol8. sol16 sol8. sol16 |
sol8. sol16 sol8. mi16 la8. fa16 si8. sol16 |
do'8. la16\cresc re'8. si16 do'4\f r |
sol1\p~ |
sol4 r r2 |
R1 |
r2 do'4( la |
fad) r la( fad) |
re8 re re\cresc re re re re re |
re re'\p re' re' re' re' re' re' |
re' re' re'\cresc re' re' re' re' re' |
re4\f~ re16( mi32 fad sol la si dod') re'4~ re'16( mi'32 fad' sol' la' si' dod'') |
re''4 re\p re re |
do4\f~ do16( re32 mi fad sol la si) do'4~ do'16( re'32 mi' fad' sol' la' si') |
si'4 si\p si si |
la fad' r fad'( |
sol') r r r8 la( |
si4) r r r8 la( |
si4) r r si |
do' la re' re' |
<si re'>4 r r2 |
sol'8\p sol' sol' sol' sol' sol' sol' sol' |
si1 |
do'4( sol2 mib4) |
re4 r r2 |
R1 |
re'4\sf re'2 re'4\p |
mib'( si! do' fad) |
sol8\f r si\p r re' r sol r |
fa!\f r si\p r re' r fa r |
mi\f r sol\p r do' r mi r |
re\f r fa\p r si r re r |
do4 r r2 |
do'8 r do'' r do' r do'' r |
sol'4 r r2 |
sol8 r sol' r sol r sol' r |
do'4 do'' r r8 re'( |
do'4) do'' r r8 re'( |
do'4) r r mi' |
fa' r sol' r |
do' r do'( la |
fad) r la( fad) re8 re' re'\cresc re' re' re' re' re' |
sol\f sol' sol sol' sol sol' sol sol' |
sol4 r r2 |
R1 |
si1\p |
do'4( sol2 mib4) |
re4 r r2 |
R1 |
re'8\f re'4 re' re' re'8\p |
do'4( si) do'2 |
si16 si si si do' do' do' do' si si si si do' do' do' do' |
si8 sol' sol' sol' mib'\cresc mib' do' do' |
sol\f r si\p r re' r sol r |
fa!\f r si\p r re' r fa r |
mi\f r sol\p r do' r mi r |
re\f r fa\p r si r re r |
do4 r r2 |
do'8 r do'' r do' r do'' r |
sol'4 r r2 |
sol8 r sol' r sol r sol' r |
do'4 r r r8 re'( |
do'4) r r r8 re'( |
do'4) r r mi' |
fa' r sol' r |
do' do' r mi |
r fa r sol |
r do r mi |
r fa r sol |
sol r r sol |
do' r r sol |
do' r r sol |
do' r la' r |
fa' r sol' r |
do' r la' r |
fa' r sol' r |
do' r la'\cresc r |
fa' r sol' r |
do'8\f do'' do' do'' do' do'' do' do'' |
do'4 mi' sol' mi' |
do' <do' do> q q |
q2 r |
