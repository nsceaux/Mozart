\version "2.19.80"
\include "common.ily"

\opusTitle "K.528 – Bella mia fiamma"

\header {
  title = \markup\center-column {
    Recitativ und Arie
    \italic Bella mia fiamma
  }
  subtitle = "für Sopran mit Begleitung des Orchesters"
  opus = "K.528"
  date = "1787"
  copyrightYear = "2018"
}

\includeScore "K.528/BellaFiammaAdio"
\includeScore "K.528/RestaOhCara"
