\clef "treble" R2.*8 |
r4 r <>\p <<
  \tag #'(corno1 corni) { do''4 | do''2.~ | do''~ | do''~ | do''4 }
  \tag #'(corno2 corni) { do'4 | do'2.~ | do'~ | do'~ | do'4 }
>> r4 r |
R2.*20 |
<>\p <<
  \tag #'(corno1 corni) { sol'2.~ | sol'~ | sol'~ | sol'~ | sol'4 }
  \tag #'(corno2 corni) { sol2.~ | sol~ | sol~ | sol~ | sol4 }
>> r4\fermata r4 |
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { sol }
>> r4\fermata r |
R2. |
r4 r <<
  \tag #'(corno1 corni) { do''4 | do''2.~ | do''~ | do''~ | do''4 }
  \tag #'(corno2 corni) { do'4 | do'2.~ | do'~ | do'~ | do'4 }
>> r4 r |
R2.*19 |
<>\p\cresc <<
  \tag #'(corno1 corni) { do''2. }
  \tag #'(corno2 corni) { do' }
>>
R2.*8\! |
<<
  \tag #'(corno1 corni) {
    do''2. |
    sol' |
    do'' |
    sol' |
    mi''2( do''4) |
    sol'2. |
  }
  \tag #'(corno2 corni) {
    do'2. |
    sol |
    do' |
    sol |
    mi'2( do'4) |
    sol2. |
  }
  { s2.\p\cresc | s\fp | s\cresc | s\fp | s8 s\cresc s2 | s2.\fp | }
>>
%%
<<
  \tag #'(corno1 corni) {
    do''1~ | do''~ | do''~ | do''~ | do''~ | do''~ | do''~ | do''~ | do''~ | do''4
  }
  \tag #'(corno2 corni) {
    do'1~ | do'~ | do'~ | do'~ | do'~ | do'~ | do'~ | do'~ | do'~ | do'4
  }
>> r4 r <<
  \tag #'(corno1 corni) {
    re''4( |
    mi'') s s re''( |
    mi'') s s re'' |
    do''1 |
  }
  \tag #'(corno2 corni) {
    sol'4( |
    do'') s s sol'( |
    do'') s s sol' |
    do'1 |
  }
  { s4 | s r r s | s r r s | s4 s2.\cresc | <>\f }
>>
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 s sol' s |
    sol' s sol' s |
    sol' }
  { sol'4 s sol' s |
    sol' s sol' s |
    sol' }
  { s4 r s r | s r s r | }
>> r4 r2 | \allowPageTurn
R1*16 |
r2 <>\f <<
  \tag #'(corno1 corni) { sol''2( | fad''4) }
  \tag #'(corno2 corni) { mi''2( | re''4) }
>> r4 r2 |
r2 <<
  \tag #'(corno1 corni) { sol''2( | fad''4) }
  \tag #'(corno2 corni) { mi''2( | re''4) }
>> r4 r2 |
R1*2 |
r2 r4 <>\p <<
  \tag #'(corno1 corni) { re''4( | mi'') }
  \tag #'(corno2 corni) { sol'4( | do''4) }
>> r4 r <<
  \tag #'(corno1 corni) { re''4( | mi'') }
  \tag #'(corno2 corni) { sol'4( | do''4) }
>> r4 r <<
  \tag #'(corno1 corni) { re''4( | mi'') }
  \tag #'(corno2 corni) { sol'4( | do''4) }
>> r4 r <<
  \tag #'(corno1 corni) { re''4 | do''1 | }
  \tag #'(corno2 corni) { sol'4 | do'1 | }
  { s4 | s s2.\cresc | <>\f }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol'4 s sol' s |
    sol' s sol' s |
    sol' }
  { sol'4 s sol' s |
    sol' s sol' s |
    sol' }
  { s4 r s r | s r s r | }
>> r4 r2 |
R1*12 |
<>\p <<
  \tag #'(corno1 corni) {
    sol'1~ | sol'~ | sol'~ | sol'~ |
    sol'~ | sol'~ | sol'~ | sol'~ |
    sol'~ | sol'~ | sol'~ | sol' |
  }
  \tag #'(corno2 corni) {
    sol1~ | sol~ | sol~ | sol~ |
    sol~ | sol~ | sol~ | sol~ |
    sol~ | sol~ | sol~ | sol |
  }
>>
R1*21 |
<<
  \tag #'(corno1 corni) { re''1( | do''4) }
  \tag #'(corno2 corni) { sol'1( | do'4) }
  { s1\f | s4\p }
>> r4 r2 |
R1*6 |
<<
  \tag #'(corno1 corni) { re''1( | do''4) }
  \tag #'(corno2 corni) { sol'1( | do'4) }
  { s1\f | s4\p }
>> r4 r2 |
R1*7 |
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''2 sol' |
    do'' do'' |
    do''4 do'' do'' do'' |
    do'' do''8. do''16 do''4 do'' |
    do'' s do'' s |
    do''2
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'2 sol |
    do' do' |
    do'4 do' do' do' |
    do' do'8. do'16 do'4 do' |
    do' s do' s |
    do'2
  }
  { s1\p | s1\cresc | s1 | s1\f | s1*3 | s4 r s r | }
>> r2 |
