\clef "alto" do'4\p do' r |
re'( sol) r |
do' do' r |
sol sol r |
sol sol r |
fa2.( |
mi4) r r |
mib'2( si4) |
do'( sol2) |
la2.( |
sol) |
fa( |
mi!2) sol4~ |
sol2. |
la4( sol fa) |
mi r r |
re' r r |
re' r r |
re' r r |
re' r r |
re'2. |
mi'( |
red'2 re'4 |
dod'2 do'4 |
si2 sib4) |
la4. la8(-. la-. la-.) |
la2 mi'4 |
re'4.( si8) dod'4 |
si2 la4~ |
la4( sol8) mib' mib' mib' |
do'2. |
fa4-.\mf mib-. r |
re'2.\p |
sol2.~ |
sol~ |
sol~ |
sol |
sol4 r\fermata r |
sol4 r\fermata r |
mib'2( si4) |
do'4( sol2) |
la2.( |
sol) |
fa( |
mi!2) sol4~ |
sol2. |
la4( sol fa) |
mi r r |
fa' r mib' |
re' r fad' |
sol' r fa'! |
mi'! r sold' |
la' mi' r |
la'2.( |
sold'2 sol'4 |
fad'2 fa'4 |
mi'2 mib'4) |
re'4. re8(-. re-. re-.) |
re2 la'4 |
sol'4.( mi'8) fad'4 |
mi'2 re'4~ |
re'( do'8) lab' lab' lab' |
lab'4-.\mf sib-. r |
lab-.\mf fa'-. r |
r8 re'(\p\cresc mib' mi' fa' fad') |
sol'4\p sol2( |
la4) r r |
r r re'8( la') |
sol'4.( mi'8) fad'( si)~ |
si8 mi'4 la re'8~ |
re'([ re'' do'']) lab'[ lab' lab'] |
lab'4-.\mf sib-. r |
lab-.\mf fa'-. r |
r8 re'(\p\cresc mib' mi' fa' fad') |
sol'2.\fp~ |
sol'4(\cresc lab' fad') |
sol'4\fp do'( si)~ |
si4*1/2( s8\cresc la4 do') |
do'2(\fp re'4) |
%%
do'4\p r r2 |
do'4 r r2 |
do'4 r r2 |
do'4 r r2 |
do'4 r r2 |
do'4 r r2 |
do'4 r do' r |
do' r do' r |
do'1 |
do'4 r r re' |
do' r r re' |
do' r r re' |
mi' do'\cresc la' fad' |
sol'\f r sol' r |
sol' r sol' r |
sol' r r2 |
mi'8(\p sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' lab' fa' lab' fa' lab' fa' lab') |
mi'2 sol' |
la'!\fp la'\fp |
la'8 la'4*1/2 s8\cresc la'4 la' la'8 |
la'2(\f fad')\p |
fa'!4( re' si2) |
sib4( sol mi2) |
fa4 r la r |
sol r fa r |
mi r dod''2(\f |
re''4) r sol4.(\p la16 si |
do'4) r dod''2(\f |
re''4) r sol4.(\p la16 si |
do'4) r la4.(\fp si16 dod' |
re'4) r sol4.(\fp la16 si |
do'!4) r r re'\p |
do' r r re' |
do' r r re' |
do' r r re' |
mi' do'\cresc la' fad' |
sol'\f r sol' r |
sol' r sol' r |
sol' r r2 |
mi'8(\p sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' lab' fa' lab' fa' lab' fa' lab') |
mi'2 sol' |
la'!4.(\fp sol'8) la'4.(\fp sol'8) |
la'8 la'4*1/2 s8\cresc la'4 la' la'8 |
la'2(\f re')\p |
sol1~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol~ |
sol |
mi'8( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' sol' |
fa' lab' fa' lab' fa' lab' fa' lab') |
mi'2 sol' |
la'!\fp la'\fp |
la'8 la'4*1/2 s8\cresc la'4 la' la'8 |
la'2(\f fad')\p |
fa'!4( re' si2) |
sib4( sol mi2) |
fa4 r la r |
sol r fa r |
mi-. mi'-. fa'-. r |
sol'-. r la'-. r |
si'-. r do''2~ |
do''4 si'2\cresc la'4~ |
la' sol'2 fa'4~ |
fa'\f re' si sol |
la8\fp la la la fa' fa' fa' fa' |
mi' mi' mi' mi' re' re' re' re' |
do'4 mi' fa' fad' |
sol' sold' la'2 |
si' do''~ |
do''4( si'\cresc sib' la')~ |
la'( sold' sol' fad') |
fa'!4\f re' si sol |
la8\fp la la la fa' fa' fa' fa' |
mi' mi' mi' mi' re' re' re' re' |
do'2 mi4. do8 |
fa2~ fa4. \tuplet 3/2 { re16( mi fa } |
sol2) sold |
la mi4. do8 |
fa2~ fa4. \tuplet 3/2 { re16( mi fa } |
sol!2) sol |
la8 la' la' la' la' la' la' la' |
lab'\cresc lab' lab' lab' lab' lab' lab' lab' |
sol' sol' sol' sol' la'! la' la' la' |
fa'\f fa' fa' fa' sol' sol' sol' sol' |
do' do' do' do' do' do' do' do' |
do' do' do' do' do' do' do' do' |
do'4 mi' sol' mi' |
do' r do' r |
do'2 r |
