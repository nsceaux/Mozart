\clef "tenor"
<<
  \tag #'(fagotto1 fagotti) {
    R2.*3 |
    r4 r r8^\markup\concat { 1 \super o } sol8\p |
    sol'8.( mi'16) do'4 r |
    R2.*3 |
  }
  \tag #'fagotto2 { R2.*8 }
>>
\clef "bass" r4 <>\p <<
  \tag #'(fagotto1 fagotti) {
    re'4( mi') |
    fa'2.( |
    mi') |
    la( |
    sol4)
  }
  \tag #'(fagotto2 fagotti) {
    si4( sib) |
    la2.( |
    sol) |
    fa( |
    mi4)
  }
>> r4 r |
R2.*5 |
r4 r <<
  \tag #'(fagotto1 fagotti) { re'8.( do'16) | do'8( sib) la4 }
  \tag #'(fagotto2 fagotti) { fad8.( la16) | la8( sol) fad4 }
>> r4 |
R2. |
\clef "tenor"
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol'2.( |
    fad'2 fa'4 |
    mi'2 mib'4 |
    re'2.) |
    dod'4 }
  {
    mi'2.( |
    red'2 re'4 |
    dod'2 do'4 |
    si2 sib4) |
    la }
>> r4 r |
R2.*7 |
\clef "bass" <>\p <<
  \tag #'(fagotto1 fagotti) {
    re'2( mib'4 |
    re'2 mib'4) |
    re'2( mib'4 |
    re'4 mib'2) |
    re'8
  }
  \tag #'(fagotto2 fagotti) {
    si2( do'4 |
    si2 do'4) |
    si2( do'4 |
    si do'2) |
    do'8
  }
>> r8 r4\fermata r |
<<
  \tag #'(fagotto1 fagotti) { re'8 }
  \tag #'(fagotto2 fagotti) { si8 }
>> r8 r4\fermata r\fermata |
R2. |
r4 <<
  \tag #'(fagotto1 fagotti) { re'4( mi') | fa'2.( | mi') | la2.( | sol4) }
  \tag #'(fagotto2 fagotti) { si4( sib) | la2.( | sol) | fa( | mi4) }
>> r4 r |
R2.*3 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'2~ do'8( dod') | re'4 s s |
    re'2~ re'8( red') | mi'4 s mi'8( re'!) |
    re'( do') si4 s | do'2.( |
    si2 sib4 | la2 lab4 |
    sol2.) | fad4 }
  { fa2( mib4) | re4 s s |
    sol2( fa!4) | mi4 s si4~ |
    si8( la) sold4 s | la2.( |
    sold2 sol4 | fad2 fa4 |
    mi2 mib4) | re }
  { s2. | s4 r r | s2. | s4 r s | s2 r4 | }
>> r4 r |
R2.*6 |
<<
  \tag #'(fagotto1 fagotti) { do2. | }
  \tag #'(fagotto2 fagotti) { do, | }
  { s2.\p\cresc <>\! }
>>
R2. |
r4 r8 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'4( mib'8) |
    re'4 s s8 re' | }
  { la8-.( la-. la-.) |
    re4 s s8 re'8 | }
  { s4.\p | s4 r r8 }
>>
<<
  \tag #'(fagotto1 fagotti) {
    s8 sol' s sol' s fad' |
    s mi' s mi' s re' |
    s re'( do')
  }
  \tag #'(fagotto2 fagotti) {
    s8 sol s do' s si |
    s mi s la s re |
    s sol( lab)
  }
  { r8 s r s r s |
    r s r s r s |
    r }
>> r8 r4 |
R2.*2 |
<<
  \tag #'(fagotto1 fagotti) { do2. | }
  \tag #'(fagotto2 fagotti) { do, | }
  { s2.\p\cresc <>\! }
>>
R2.*5 |
%%
R1*6 |
<>\p <<
  \tag #'(fagotto1 fagotti) { la2( sol) | fa'( mi') | }
  \tag #'(fagotto2 fagotti) { fa2( mi) | la( sol) | }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1( |
    do'4) s s sol |
    do s s sol |
    do s s sol |
    mi do la fad |
    sol }
  { fa1( |
    mi4) s s sol |
    do s s sol |
    do s s sol |
    mi4 do la fad |
    sol }
  { s1 |
    s4 r r s |
    s r r s |
    s r r s |
    s4 s2.\cresc |
    s4\f }
>> r4 <<
  \tag #'(fagotto1 fagotti) { mi'4 s | re' s mi' s | re' }
  \tag #'(fagotto2 fagotti) { do'4 s | si s do' s | si }
  { s4 r | s r s r | }
>> r4 r2 |
<<
  \tag #'(fagotto1 fagotti) {
    R1*4 |
    \clef "tenor" <>^\markup\concat { 1 \super o } do'2.( mi'4 |
    re' si sol si) |
    do'2.( mi'4 |
    re' fa' si re') |
    do'4 r r2 |
    R1*7 | \allowPageTurn
    \clef "bass"
  }
  \tag #'fagotto2 { R1*16 | }
>>
r2 <>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2~ | la4 }
  { la2~ | la4 }
>> r4 <>\p <<
  \tag #'(fagotto1 fagotti) { fa'2( | mi'4) }
  \tag #'(fagotto2 fagotti) { re'2( | do'4) }
>> r4 <>\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2~ | la4 }
  { la2~ | la4 }
>> r4 <>\p <<
  \tag #'(fagotto1 fagotti) { fa'2( | mi'4) }
  \tag #'(fagotto2 fagotti) { re'2( | do'4) }
>> r4  <>\fp <<
  \tag #'(fagotto1 fagotti) { sol'2( | fad'4) }
  \tag #'(fagotto2 fagotti) { mi'2( | re'4) }
>> r4  <>\fp <<
  \tag #'(fagotto1 fagotti) { fa'!2( | mi'4) }
  \tag #'(fagotto2 fagotti) { re'2( | do'4) }
>> r4 r <>^"a 2." sol4\p |
do r r sol |
do r r sol |
do r r sol |
mi do\cresc la fad |
sol4\f r <<
  \tag #'(fagotto1 fagotti) { mi'4 s | re' s mi' s | re' }
  \tag #'(fagotto2 fagotti) { do'4 s | si s do' s | si }
  { s4 r | s r s r | }
>> r4 r2 |
\clef "tenor" <<
  \tag #'(fagotto1 fagotti) {
    <>^\markup\concat { 1 \super o } do'2.( mi'4 |
    re' si sol si) |
    do'2.( mi'4 |
    re' fa' si re') |
    do'2.( mi'4 |
    re' si sol si) |
    do'2.( mi'4 |
    re' fa' si re') |
    do'4 r r2 |
    R1*4 |
  }
  \tag #'fagotto2 { R1*13 | }
>>
r2 <>\p <<
  \tag #'(fagotto1 fagotti) { fad'2( | sol'4) }
  \tag #'(fagotto2 fagotti) { do'2( | si4) }
>> r4 r2 |
r2 <<
  \tag #'(fagotto1 fagotti) { fad'2( | sol'4) }
  \tag #'(fagotto2 fagotti) { do'2( | si4) }
>> r4 r2 |
R1*6 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1( |
    do'2.)( mi'4 |
    re' si sol si) |
    do'2.( mi'4 |
    re' fa' si re') |
    do'2.( mi'4 |
    re' si sol si) |
    do'2.( mi'4 |
    re' fa' si re') |
    do'4 r r2 | }
  { si1( | do'4) r r2 | R1*8 | }
>>
<<
  \tag #'(fagotto1 fagotti) {
    R1*2 |
    \clef "bass" <>^\markup\concat { 1 \super o } r2 do'\p |
    si4.( do'16 re' fa2) |
    mi4.( fa16 sol sib,2) |
    la,4 r r2 |
    R1*6 |
    \clef "tenor"
  }
  \tag #'fagotto2 { R1*12 }
>>
<<
  \tag #'(fagotto1 fagotti) { fa'1( | mi'4) }
  \tag #'(fagotto2 fagotti) { si1( | do'4) }
  { s1\f | s4\p }
>> r4 r2 |
R1*6 |
<<
  \tag #'(fagotto1 fagotti) { fa'1( | mi'4) }
  \tag #'(fagotto2 fagotti) { si1( | do'4) }
  { s1\f | s4\p }
>> r4 r2 |
R1*4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'1 | re' | fa' | mi' |
    fad' | sol'2( mi') | re'1 | }
  { do'1~ | do' | si | do' |
    mib' | mi'!2( do')~ | do'( si) | }
  { s1\p | s1*3 | s1\cresc | s | s\f }
>>
\clef "bass" <>^"a 2." do'8 do do do do do do do |
do do do do do do do do |
do4 mi sol mi |
do r do r |
do2 r |
