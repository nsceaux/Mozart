\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global
        \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corni in C }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Soprano
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \vccbInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s2.*9\pageBreak
        s2.*8\break s2.*5\pageBreak
        s2.*6\break s2.*7\pageBreak
        s2.*8\break s2.*7\pageBreak
        \grace s16 s2.*5\break s2.*6\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*3 s1*4\break s1*8\pageBreak
        s1*8\break s1*9\pageBreak
        s1*10\break s1*8\pageBreak
        s1*9\break s1*9\pageBreak
        s1*9\break s1*9\pageBreak
        s1*8\break s1*8\pageBreak
        s1*9\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
