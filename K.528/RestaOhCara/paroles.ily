Re -- sta, oh ca -- ra! oh ca -- ra! A -- cer -- ba mor -- te
mi se -- pa -- ra, oh Di -- o, da te,
mi se -- pa -- ra, oh Di -- o, da te.

Pren -- di cu -- ra di sua sor -- te,
con -- so -- lar -- la al -- men pro -- cu -- ra.

Va -- do… ahi las -- so! ad -- di -- o, ad -- di -- o per sem -- pre…
Quest’ af -- fan -- no, que -- sto pas -- so
è ter -- ri -- bi -- le per me,
è ter -- ri -- bi -- le,
ter -- ri -- bi -- le per me.

Re -- sta, oh ca -- ra!
re -- sta, oh ca -- ra! A -- cer -- ba mor -- te
mi se -- pa -- ra, oh Di -- o, da te,
mi se -- pa -- ra, oh Di -- o, da te.

Pren -- di cu -- ra di sua sor -- te,
con -- so -- lar -- la al -- men pro -- cu -- ra,
al -- men pro -- cu -- ra.

Va -- do… ahi las -- so! ad -- di -- o, ad -- di -- o per sem -- pre…
Quest’ af -- fan -- no, que -- sto pas -- so
è ter -- ri -- bi -- le per me,
è ter -- ri -- bi -- le,
ter -- ri -- bi -- le,
è ter -- ri -- bi -- le per me.

Ah, quest’ af -- fan -- no, que -- sto pas -- so
è ter -- ri -- bi -- le per me,
è ter -- ri -- bi -- le,
ter -- ri -- bi -- le,
è ter -- ri -- bi -- le per me,
è ter -- ri -- bi -- le per me,
è ter -- ri -- bi -- le per me.

%%

Ah, dov’ è il tem -- pio? dov’ è l’a -- ra?
Vie -- ni af -- fret -- ta la ven -- det -- ta,
vie -- ni af -- fret -- ta la ven -- det -- ta,
vie -- ni af -- fret -- ta la ven -- det -- ta!
vie -- ni! vie -- ni!
Que -- sta vi -- ta co -- sì a -- ma -- ra
più sof -- fri -- bi -- le non è,
no,
più sof -- fri -- bi -- le non è.

Dov’ è il tem -- pio? dov’ è l’a -- ra?
ah do -- ve? ah do -- ve?
Vie -- ni af -- fret -- ta la ven -- det -- ta,
vie -- ni af -- fret -- ta la ven -- det -- ta!
vie -- ni! vie -- ni!
Que -- sta vi -- ta co -- sì a -- ma -- ra
più sof -- fri -- bi -- le non è.
No…

Oh ca -- ra, ad -- di -- o, ad -- di -- o, ad -- di -- o per sem -- pre! __

Ah, que -- sta vi -- ta co -- sì a -- ma -- ra
più sof -- fri -- bi -- le non è, no,
più sof -- fri -- bi -- le non è;
que -- sta vi -- ta co -- sì a -- ma -- ra
più sof -- fri -- bi -- le, sof -- fri -- bi -- le non è;
quest’ af -- fan -- no, que -- sto pas -- so,
que -- sta vi -- ta co -- sì a -- ma -- ra
più sof -- fri -- bi -- le,
sof -- fri -- bi -- le non è,
più sof -- fri -- bi -- le non è,
più sof -- fri -- bi -- le non è,
più sof -- fri -- bi -- le non è.
