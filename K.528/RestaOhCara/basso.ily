\clef "bass" do4\p do r |
R2. |
do4 do r |
sol, sol, r |
mi mi r |
fa2.( |
do4) r r |
mib2( si,4) |
do( sol,2) |
la,2.( |
sol,) |
fa,( |
mi,!2) mi4( |
re si, do) |
fa sol sol, |
do r r |
re r r |
re r r |
re r r |
re r r |
re r r |
mi r r |
red r re |
dod r do |
si, r sib, |
la,4. la8-.( la-. la-.) |
la2. |
re4( sol fad) |
si,( mi la,) |
re( mib) mib |
do2. |
re4-.\mf mib-. r |
re2.\p |
sol,4 r r |
sol, r r |
sol, r r |
sol, r r |
sol, r\fermata r |
sol, r\fermata r |
r mib( si,) |
do( sol,2) |
la,2.( |
sol,) |
fa,( |
mi,!2) mi4( |
re si, do) |
fa sol sol, |
do r r |
fa r mib |
re r fad |
sol r fa! |
mi! r sold |
la mi r |
la r r |
sold r sol |
fad r fa |
mi r mib |
re4. re'8-.( re'-. re'-.) |
re'2. |
sol4( do' si) |
mi( la re) |
sol( lab) lab |
fa4-.\mf sol-. r |
lab-.\mf re-. r |
r8 re(\p\cresc mib mi fa fad) |
sol2.\p( |
la4) r r |
re'2. |
sol4( do' si) |
mi( la re) |
sol( lab) lab |
fa4-.\mf sol-. r |
lab-.\mf re-. r |
r8 re(\p\cresc mib mi fa fad) |
sol2.\fp~ |
sol4(\cresc fa! lab) |
sol2.\fp |
sold4*1/2( s8\cresc la!4 fad) |
sol!2.\fp |
%%
do4\p r r2 |
do4 r r2 |
do4 r r2 |
do4 r r2 |
do4 r r2 |
do4 r r2 |
do4 r do r |
do r do r |
do1 |
do4 r r sol |
do r r sol |
do r r sol |
mi do\cresc la fad |
sol\f r sol r |
sol r sol r |
sol r r2 |
R1*8 | \allowPageTurn
mi'2(\p mib') |
re'4.(\fp mib'8) re'4.(\fp mib'8) |
re'-. mib'-. re'-.\cresc mib'-. re'-. mib'-. re'-. mib'-. |
re'4\f r r2 |
sol1\p( |
do) |
fa4 r fa r |
sol r sol, r |
do r dod'2(\f |
re'4) r sol,4.(\p la,16 si, |
do4) r dod'2(\f |
re'4) r sol,4.(\p la,16 si, |
do4) r la,4.(\fp si,16 dod |
re4) r sol,4.(\fp la,16 si, |
do!4) r r sol\p |
do r r sol |
do r r sol |
do r r sol |
mi do\cresc la fad |
sol\f r sol r |
sol r sol r |
sol r r2 |
R1*8 |
mi'2(\p mib') |
re'4.(\fp mib'8) re'4.(\fp mib'8) |
re'8-. mib'-. re'-.\cresc mib'-. re'-. mib'-. re'-. mib'-. |
re'4\f r r2 |
sol,1\p~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol, |
R1*8 | \allowPageTurn
mi'2( mib') |
re'4.(\fp mib'8) re'4.(\fp mib'8) |
re'8-. mib'-. re'-.\cresc mib'-. re'-. mib'-. re'-. mib'-. |
re'4\f r r2 |
sol1\p( |
do) |
fa4 r fa r |
sol r sol, r |
do-. mi-. fa-. r |
sol-. r la-. r |
si-. r do'2~ |
do'4 si2\cresc la4~ |
la sol2 fa4~ |
fa\f re si, sol, |
la,8\fp la, la, la, fa fa fa fa |
sol sol sol sol sol sol sol sol do4 mi fa fad |
sol sold la2 |
si do'~ |
do'4( si\cresc sib la)~ |
la( sold sol fad) |
fa!4\f re si, sol, |
la,8\fp la, la, la, fa fa fa fa |
sol sol sol sol sol, sol, sol, sol, |
do2 mi4. do8 |
fa2~ fa4. \tuplet 3/2 { re16([ mi fa] } |
sol2) sold |
la2 mi4. do8 |
fa2~ fa4. \tuplet 3/2 { re16([ mi fa] } |
sol!2) sol, |
la,8 la la la la la la la |
lab\cresc lab lab lab lab lab lab lab |
sol sol sol sol la! la la la |
fa\f fa fa fa sol sol sol sol |
do do do do do do do do |
do do do do do do do do |
do4 mi sol mi |
do r do r |
do2 r |

