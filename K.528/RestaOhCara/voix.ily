\clef "soprano/treble" R2.*2 |
do''8.([ sol'16]) sol'4 r8 sol' |
fa''8.([ re''16]) si'4 r |
r r do'' |
do''4.( si'16[ do''] re''[ do'' si' la']) |
sol'4 r sol' |
do''2 sol'4 |
mib' re' r |
fa'( la') do'' |
mi'!( sol') do'' |
do''4.( re''16[ do'']) si'8 la' |
sol'4 r sol'8 do'' |
si'([ fa'']) fa''4. mi''8 |
mi''16([ re'' mi'' fa'']) do''4. mi''16([ re'']) |
do''4 r r |
do''2 la'4 |
si'4. re''8 do'' si' |
re''8.([ si'16]) la'4 re''8. do''16 |
do''8([ sib']) la' re'' re'' do'' |
do''([ sib']) la'4 r |
do''8 do'' r4 r8 do'' |
do''8.([ si'!16]) si'4 r8 fa'' |
mi''8.([ dod''16]) la'4 r8 mib'' |
re''8.([ si'16]) sol'4 r8 sol'' |
sol''8.([ dod''16]) dod''2 |
r4 r dod''8 sol'' |
fad'' do''! si' fa'' mi'' lad' |
la'! red'' re''8. sold'16 sol'8. dod''16 |
do''!4 sib'4. sib'8 |
lab'2~ lab'16([ sib'32 do'' re'' mib'' fa'' sol''] |
lab''8.) lab''16 sol''4 r8. sol''16 |
sol'4. sol'8 \appoggiatura si'!16 la'8. sol'16 |
sol'4 r r |
re'' sol' r8 mib'' |
re''8.([ si'16]) sol'4 r |
re'' mib''4. do''8 |
do''4.(\fermata si'16[ la'] \grace do''16 si'8[ la'16 sol']) |
sol'4 r\fermata sol' |
do''2 sol'4 |
mib' re' r |
fa'( la') do'' |
mi'!( sol') do'' |
do''4.( re''16[ do'']) si'8 la' |
sol'4 r sol'8 do'' |
si'([ fa'']) fa''4. mi''8 |
mi''16([ re'' mi'' fa'']) do''4. mi''16([ re'']) |
do''4 r do''8 sib' |
\appoggiatura sib'16 lab'4. lab'8 lab' sol' |
fad' fad' r4 re''8 do'' |
\appoggiatura do''16 sib'4. sib'8 sib' la'! |
sold' sold' r mi'' mi'' re'' |
re''([ do'']) si'!4 r |
do''8 do'' r4 r8 fa'' |
fa''8.([ si'16]) si'4 r8 dod'' |
dod''8.([ re''16]) la'4 r8 si' |
si'8.([ do''!16]) sol'4 r8 do'' |
do''8.([ fad''16]) fad''2 |
r4 r do''8 fad'' |
fa''! si' sib' mi'' red'' la' |
sold' re''! dod''8. sol''!16 fad''8. do''16 |
si'4 do''4. do''8 |
reb''8. reb''16 mi'4 r8. mi''16 |
fa''!8. sol''32([ lab'']) si'4 r |
r r do''8. do''16 sol''4. mi''16([ do'']) sol'8. fa'16 |
mi'4 r r |
do'' r do''8 fad'' |
fa''! si' sib' mi'' red'' la'' |
sold'' re''! dod''8. sol'!16 fad'8. do''!16 |
si'4 do''4. do''8 |
reb''8. reb''16 mi'4 r8. mi''16 |
fa''8. sol''32([ lab'']) si'4 r |
r r do''8. do''16 |
sol''4.. mi''32([ do'']) sol'8. sol'16 |
sol'4 r do''8. do''16 |
sol''4~ sol''16 fa''32([ mi''] re''[ do'' si' la']) sol'8. fa'16 |
mi'4 r mib''8. do''16 |
sol''4~ sol''16 la''32([ sol''] fa''[ mi''! re'' do'']) fa''8. si'16 |
%%
do''4 r r2 |
re''4 r r2 |
mi''2. mi''4 |
mi''( fa'') fa'' r |
fad''2. fad''4 |
fad''( sol'') sol'' r |
la''2 sol'' |
fa''! mi'' |
re''2. mi''4 |
do'' do'' sol''8([ fa'']) re''([ si']) |
do''([ sol']) do''([ mi'']) sol''([ fa'']) re''([ si']) |
do''([ sol']) do''([ mi'']) sol''([ fa'']) re''([ si']) |
do''([ mi'']) la''([ sol'']) fa''([ mi'']) re''([ do'']) |
si'4 sol' r2 |
re''4 sol' r2 |
sol''2 sol'4 r |
R1*2 |
do''2 mi'' |
sol''4( si') si' r |
R1*2 |
do''2 sol'' |
lab''4( si') si' r |
do''2 sol'' |
fad''4. sol''8 fad''4. sol''8 |
fad''4 r r2 |
la''!4 r r2 |
R1 |
mi''2. mi''4 |
fa''!2. sol''8([ la'']) |
do''2 si' |
do''4 r r2 |
r fa''4. re''8 |
do''4 do'' r2 |
r fa''4. re''8 |
do''4 do'' sol''2 |
fad''4 re'' fa''2 |
mi''4 do'' r2 |
r sol''8([ fa'']) re''([ si']) |
do''([ sol']) do''([ mi'']) sol''([ fa'']) re''([ si']) |
do''([ sol']) do''([ mi'']) sol''([ fa'']) re''([ si']) |
do''([ mi'']) la''([ sol'']) fa''([ mi'']) re''([ do'']) |
si'4 sol' r2 |
re''4 sol' r2 |
sol''2 sol'4 r |
R1*2 |
do''2 mi'' |
sol''4( si') si' r |
R1*2 |
do''2 sol'' |
lab''4( si') si' r |
do''2 sol'' |
fad''4. sol''8 fad''4. sol''8 |
fad''4 r r2 |
la''!4 r r2 |
R1 |
r2 re'' |
mi'' re'' |
r re'' |
mi'' re'' |
r re'' |
do'' si' |
r do'' |
mi'' re'' |
r mi'' |
sol'' fa''!2~ |
fa''1 |
R1*2 |
do''1 |
sol''2. si'4 |
do''2( mi''4. re''8) |
re''1 |
sol'' |
lab''2. si'4 |
do'' do'' do''4. sol''8 |
fad''4. sol''8 fad''4. sol''8 |
fad''4 r r2 |
la''!4 r r2 |
R1 |
mi''2. mi''4 |
fa''!2. sol''8([ la'']) |
do''2 si' |
do''4 r la' re'' |
si' sol' r2 |
r mi''4 la'' |
re'' sol'' r2 |
r la'4 re'' |
si'4. re''8 fa'4. fa'8 |
mi'2 la'' |
sol'' si' |
do''4 r la'8([ fa']) re''([ do'']) |
si'([ sol']) mi''([ re'']) do''([ la']) fa''([ mi'']) |
re''([ si']) sol''([ fa'']) mi''([ do'']) la''([ sol'']) |
fad''([ re'']) sol''([ fa'']) mi''([ do'']) fa''([ mi'']) |
red''([ si']) mi''([ re'']) dod''([ la']) re''([ do'']) |
si'([ do'']) re''([ mi'']) fa''([ re'']) si'([ fa']) |
mi'2 la'' |
sol'' si' |
do''4 r do''4. do''8 |
re''2. fa''4 |
sol'2 fa' |
mi'4 r sol''4. do''8 |
la''2.. \tuplet 3/2 { sol''16([ fa'' mi'']) } |
re''2 sol'' |
mi''1 |
fad''2. fad''4 |
sol''2 la'' |
fa''! sol'' |
do'' r |
R1*4 |
