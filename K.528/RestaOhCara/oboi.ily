\clef "treble" <>
<<
  \tag #'(oboe1 oboi) {
    R2.*3 |
    r4 r r8-\tag #'oboi ^\markup\concat { 1 \super o } sol'\p |
    sol''8.( mi''16) do''4 r |
    la''2.( |
    sol''4) r r |
    R2. |
    r4 re''( mi'') |
    fa''2.( |
    mi'') |
    la''( |
    sol''4) r r |
    R2.*6 |
    r4 r fad''8.( la''16) |
    la''8( sol'') fad''4 r |
  }
  \tag #'oboe2 { R2.*21 }
>>
<<
  \tag #'(oboe1 oboi) {
    s4 mi''( sol'') |
    s red''( sold'') |
    s dod''( fad'') |
    s si'( re'') |
    s4. mi''8(-. mi''-. mi''-.) |
    mi''8.( sol''16)
  }
  \tag #'(oboe2 oboi) {
    s4 do''( mi'') |
    s si'( fa''!) |
    s la'( mib'') |
    s sol'( sib') |
    s4. dod''8-.( dod''-. dod''-.) |
    dod''8.( mi''16)
  }
  { r4 s2 | r4 s2 | r4 s2 | r4 s2 | r4 r8 }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2( |
    fad''4 fa'' mi'' |
    red'' re'' dod'') |
    do''!( sib') r | }
  { mi''4 r |
    R2.*3 | }
>>
R2.*3 |
<<
  \tag #'(oboe1 oboi) {
    s4 s fad''( |
    sol'') s fad''( |
    sol'') s fad''( |
    sol'') fad''2 |
    sol''8
  }
  \tag #'(oboe2 oboi) {
    s4 s do''( |
    si') s do''( |
    si') s do''( |
    si') do''2 |
    do''8
  }
  { r4 r s\p | s r s | s r s }
>> r8 r4\fermata r |
<<
  \tag #'(oboe1 oboi) { re''8 }
  \tag #'(oboe2 oboi) { si' }
>> r8 r4\fermata r\fermata |
<<
  \tag #'(oboe1 oboi) {
    R2. |
    r4-\tag #'oboi ^\markup\concat { 1 \super o } re''( mi'') |
    fa''2.( |
    mi'') |
    la''( |
    sol''4) r r |
    R2.*8 |
  }
  \tag #'oboe2 { R2.*14 }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { s4 do''( fa'') |
    s si'( dod'') |
    s fad'( re'') |
    s sol'!( do'') | }
  { s4 la'( do'') |
    s sold'( mi'') |
    s la'( si') |
    s mi'( sol') | }
  { r4 s s |
    r s s |
    r s s |
    r s s | }
>>
r4 r8 <<
  \tag #'(oboe1 oboi) {
    do''8-.( do''-. do''-.) |
    do''8.( la''16) la''4
  }
  \tag #'(oboe2 oboi) {
    la'8-.( la'-. la'-.) |
    la'8.( fad''16) fad''4
  }
>> r4 |
R2.*5 |
<>\p\cresc <<
  \tag #'(oboe1 oboi) { do'''2. | }
  \tag #'(oboe2 oboi) { do''2. | }
>>
R2.\! |
r4 r8 <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8( la'' sol'') | }
  { do''8-.( do''-. do''-.) | }
>>
<<
  \tag #'(oboe1 oboi) {
    fad''8.( do''16) do''4 s8 fad'' |
    s fa''! s mi'' s red'' |
    s re''! s dod'' s do'' |
  }
  \tag #'(oboe2 oboi) {
    do''8.( la'16) la'4 s8 re'' |
    s re'' s do'' s si' |
    s si' s la' s la' |
  }
  { s2 r8 s |
    r s r s r s |
    r s r s r s | }
>>
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'8[( do'')] }
  { sol'8([ do'']) }
>> r8 r4 |
R2.*2 |
<<
  \tag #'(oboe1 oboi) { do'''2. | }
  \tag #'(oboe2 oboi) { do'' | }
  { s2.\p\cresc }
>>
R2.*5\! | \allowPageTurn
%%
R1*6 |
<>\p <<
  \tag #'(oboe1 oboi) {
    la''2( sol'') |
    fa''( mi'') |
  }
  \tag #'(oboe2 oboi) {
    fa''2( mi'') |
    la'( sol') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1( | do''4) }
  { si'1( | do''4) }
>> r4 r <<
  \tag #'(oboe1 oboi) {
    fa''4( |
    mi'') s s fa''( |
    mi'')
  }
  \tag #'(oboe2 oboi) {
    si'4( |
    do'') s s si'( |
    do'') 
  }
  { s4 | s r r }
>> r4 r
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4( | sol'' mi'' fa'' la'') | }
  { si'4( | do''1) | }
  { s4 | s4 s2.\cresc <>\f }
>>
<<
  \tag #'(oboe1 oboi) {
    re''4 s sol'' s |
    sol'' s sol'' s |
    sol''
  }
  \tag #'(oboe2 oboi) {
    si'4 s mi'' s |
    re'' s mi'' s |
    re''
  }
  { s4 r s r | s r s r }
>> r4 r2 | \allowPageTurn
R1*13 |
<<
  \tag #'(oboe1 oboi) {
    \tag #'oboi <>^\markup\concat { 1 \super o } sol''1\p |
    fa''2( re'') |
    do''( si') |
    do''4 r
  }
  \tag #'oboe2 { R1*3 | r2 }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''2( |
    fad''4) s fa''2( |
    mi''4)  s sol''2( |
    fad''4) s fa''2( |
    mi''4)  s sol''2( |
    fad''4)  s fa''2( |
    mi''4)  s s fa''( |
    mi'') s s fa''( |
    mi'') s s fa''( |
    mi'')
  }
  \tag #'(oboe2 oboi) {
    mi''2( |
    re''4) s re''2( |
    do''4) s mi''2( |
    re''4) s re''2( |
    do''4) s mi''2( |
    re''4) s re''2( |
    do''4) s s si'( |
    do'') s s si'( |
    do'') s s si'( |
    do'')
  }
  { s2\f |
    s4 r s2\p |
    s4 r s2\f |
    s4 r s2\p |
    s4 r s2\fp |
    s4 r s2\fp |
    s4 r r s\p |
    s r r s |
    s r r s | }
>> r4 r \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4( | sol'' mi'' fa'' la'') | }
  { si'4( | do''1) | }
  { s4 | s s2.\cresc <>\f }
>>
<<
  \tag #'(oboe1 oboi) {
    re''4 s sol'' s |
    sol'' s sol'' s |
    sol''
  }
  \tag #'(oboe2 oboi) {
    si'4 s mi'' s |
    re'' s mi'' s |
    re''
  }
  { s4 r s r | s r s r | }
>> r4 r2 |
R1*13 |
r2 <<
  \tag #'(oboe1 oboi) {
    fad''2( |
    sol''4) s s2 |
    s fad''( |
    sol''4)
  }
  \tag #'(oboe2 oboi) {
    do''2( |
    si'4) s s2 |
    s do''2( |
    si'4)
  }
  { s2\p | s4 r r2 | r2 }
>> r4 r2 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'2~ |
    la'( sol' |
    si' la' |
    do'' si' re'' do''4 dod'' |
    mi''2 re'') |
    si'1( |
    do''2.)( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2.( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''4 r r2 |
  }
  { fad'2~ |
    fad'( sol') |
    R1*4 |
    fa'!1( |
    mi'4) r r2 |
    R1*8 | }
>>
<<
  \tag #'(oboe1 oboi) {
    R1*4 |
    \tag #'oboi <>^\markup\concat { 1 \super o } sol''1\p |
    fa''2( re'') |
    do''( si') |
    do''4 r r2 |
    R1*4 |
  }
  \tag #'oboe2 { R1*12 }
>>
<<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
  { s1\f | s4\p }
>> r4 r2 |
R1*6 |
<<
  \tag #'(oboe1 oboi) { fa''1( | mi''4) }
  \tag #'(oboe2 oboi) { re''1( | do''4) }
  { s1\f | s4\p }
>> r4 r2 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''1 |
    re''~ |
    re''2( fa'') |
    mi''1 |
    re'' |
    fa'' |
    mi'' |
    do''' |
    do'''~ |
    do'''2( si'') |
    do'''1~ |
    do'''4 sol'' mi'' do'' | }
  { do''1~ |
    do'' |
    si' |
    do''~ |
    do'' |
    si' |
    do'' |
    fad'' |
    sol''2( mi'') |
    la''( sol''4 fa''!) |
    mi''1 |
    sol''4 mi'' do'' sol' | }
  { s1\p | s1*6 | s1\cresc | s1 | s1\f | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol'4 sol'8. sol'16 sol'4 sol' |
    sol' s sol'' s |
    mi''2
  }
  \tag #'(oboe2 oboi) {
    mi'4 mi'8. mi'16 mi'4 mi' |
    mi' s mi'' s |
    do''2
  }
  { s1 | s4 r s r | }
>> r2 |
