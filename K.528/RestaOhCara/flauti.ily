\clef "treble" R2.*5 |
la''2.(\p |
sol''4) r r |
R2.*2 |
do'''2.~ |
do'''~ |
do'''~ |
do'''4 r r |
R2.*6 |
r4 r re'''8.( do'''16) |
do'''8( sib'') la''4 r |
r sol''( do''') |
r fad''( si''!) |
r mi''( la'') |
r re''( sol'') |
r r8 sol''-.( sol''-. sol''-.) |
sol''8.( dod'''16) dod'''2( |
do'''!4 si'' lad'' |
la''! sold'' sol'') |
fad''( sol'') r |
R2.*7 |
re''8 r r4\fermata r |
sol''8 r r4\fermata r |
R2.*2 |
do'''2.~ |
do'''~ |
do'''~ |
do'''4 r r |
R2.*8 |
r4 fa''( do''') |
r mi''( sib'') |
r re''( lab'') |
r do''( sol'') |
r r8 fad''-.( fad''-. fad''-.) |
fad''8.( do'''16) do'''2( |
si''4 sib'' la'' |
sold'' sol'' fad'') |
fa''!( mib'') r |
R2.*2 |
do'''2.\p\cresc |
R2. |
r4 r8 do'''-.(\p do'''-. do'''-.) |
do'''8.( fad''16) fad''4 r8 do''' |
r si'' r sib'' r la'' |
r sold'' r sol'' r fad'' |
r fa''!( mib'') r r4 |
R2.*2 |
do'''2.\p\cresc |
R2.*5\! |
%%
R1*7 |
fa'''2(\p mi''') |
re'''1( |
do'''4) r r2 |
R1*3 |
sol''4\f r do''' r |
si'' r do''' r |
si'' r r2 |
R1*13 |
sib''1\p |
la''2( fa'') |
mi''( re'') |
do''4 r la''2\f~ |
la''4 r r2 |
r la''2~ |
la''4 r r2 |
r la''2\fp~ |
la''4 r sol''2\fp~ |
sol''4 r r2 |
R1*4 |
sol''4\f r do''' r |
si'' r do''' r |
si'' r r2 |
R1*4 |
do'''2.(\p mi'''4 |
re''' si'' sol'' si'') |
do'''2.( mi'''4 |
re''' fa''' si'' re''') |
do''' r r2 |
R1*6 |
r2 do'''2(\p |
si''4) r r2 |
r re''( |
do'' si' |
re'' do'' |
mi'' re'' |
fa'' mi'' |
sol'' fa'') |
R1*5 |
do'''2.( mi'''4 |
re''' si'' sol'' si'') |
do'''2.( mi'''4 |
re''' fa''' si'' re''') |
do'''4 r r2 |
R1*4 |
sib''1\p |
la''2( fa'') |
mi''( re'') |
do''4 r r2 |
R1*4 |
si''1(\f |
do'''4)\p r r2 |
R1*6 |
si''1(\f |
do'''4)\p r r2 |
R1*7 |
do'''1\p |
mib'''\cresc |
mi'''! |
re'''\f |
do'''2 sol''' |
mi'''4 do''' sol'' mi'' |
do''4 do''8. do''16 do''4 do'' |
do'' r mi''' r |
do'''2 r |
