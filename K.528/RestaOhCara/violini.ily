\clef "treble"
<<
  \tag #'violino1 {
    sol'8.(\p mi'!16) sol'4 r8 do'' |
    do''8.( si'16) fa''4 r |
    sol' sol' r |
    fa' fa' r |
    mi' mi' r |
    do'2.~ |
    do'4 r r |
    sol'2. |
    mib'4( re' mi') |
    fa'2.( |
    mi') |
    la'( |
    sol'8) sol''( fa'' mi'' re'' do'') |
    si'4 fa'4.( mi'8) |
    r8 fa' r mi' r re' |
    r8 mi'32( re' mi' fa') sol'16( mi') do''-. do''-. do''( sol') mi''-. mi''-. |
    r8 fad'32( mi' fad' sol') la'16( fad') la'-. la'-. la'( fad') do''-. do''-. |
    r8 sol'32( fad' sol' la') si'16( sol') re''8( do'' si') |
    r8 si'32( do'' re'' si') la'16( fad') la'-. la'-. la'( fad') la'-. la'-. |
    r8 sib'32( do'' re'' sib') la'16( fad') la'-. la'-. la'( fad') la'-. la'-. |
    r8 sib'32( do'' re'' sib') la'8 la'4( si'8 |
    do'') sol'-. do''-. mi''-. sol''( do''') |
    r8 fad'-. si'-. red''-. sold''( si'') |
    r mi'-. la'-. dod''-. fad''( la'') |
    r re'!-. sol'!-. si'-. re''( sol'') |
    sol''4. sol'8-.( sol'-. sol'-.) |
    sol'2 dod''4( |
    do''! si' lad' |
    la' sold' sol') |
    fad'( sol'8) sol' sol' sol' |
    lab'2. |
    si!4-.\mf do'-. r |
    sol'2(\p fad'4) |
    r8 sol'( si' re'') mib''([ fad']) |
    r8 sol'( si' re'') mib''([ fad']) |
    r8 sol'( si' re'') mib''([ fad']) |
    r8 sol'( fad' la' do'' fad'') |
    sol''4 r\fermata r |
    sol' r\fermata r |
    sol'2. |
    mib'4( re' mi') |
    fa'2.( |
    mi') |
    la'2.( |
    sol'8) sol''( fa'' mi'' re'' do'') |
    si'4 fa'4.( mi'8) |
    r8 fa' r mi' r re' |
    r8 mi'32( re' mi' fa') sol'16( mi') sol'-. sol'-. sol'( mi') do''-. do''-. |
    r8 lab'32( sol' lab' sib') do''8( lab'4 sol'8) |
    r8 fad'32( mi' fad' sol') la'!16( fad') la'-. la'-. la'( fad') re''-. re''-. |
    r8 sib'32( la' sib' do'') re''8( sib'4 la'8) |
    r8 sold'32( fad' sold' la') si'!16( sold') si'-. si'-. si'( sold') re''-. re''-. |
    r8 do''32( re'' mi'' do'') si'8( mi''4 re''8) |
    do'' mi'-. la'-. do''-. fa''8([ do''']) |
    r re'-. fa'-. si'-. dod''8([ sib'']) |
    r8 re'-. fad'-. la'-. si'!([ lab'']) |
    r8 do'-. mi'-. sol'-. do''([ sol'']) |
    fad''4. <do'' re'>8-.( q-. q-.) |
    q2 fad''4( |
    fa'' mi'' red'' |
    re''! dod'' do'') |
    si'4( do''8) do'' do'' do'' |
    reb''4-.\mf mi'-. r |
    fa'-.\mf lab''-. r |
    R2. | \allowPageTurn
    mi'2(\p re'4) |
    do' r r |
    r r fad''8( do''') |
    si''( fa''! mi'' sib' la' red') |
    re'!( sold' sol' dod'' do'' fad'') |
    fa''!( si'' do''') do''[ do'' do''] |
    reb''4-.\mf mi'-. r |
    fa'-.\mf lab''4-. r |
    R2. | \allowPageTurn
    mi'2(\fp fa'4) |
    mi'(\cresc re' mib') |
    mi'!4(\fp fa'2) |
    << { s8 s\cresc }  mi'2( >> la'4) |
    sol'2(\fp si'4) |
  }
  \tag #'violino2 {
    mi'!8.(\p do'16) mi'4 r |
    fa'( si') r |
    mi' mi' r |
    <re' si> q r |
    do' do' r |
    la2.( |
    sol4) r r |
    do'8( sol do' sol re' sol) |
    do'( sol si sol sib sol) |
    do'( la do' la do' la) |
    do'( sol do' sol do' sol) |
    do'( la do' la do' la) |
    do'( sol' fa' mi' re' do') |
    si4 re'4.( do'8) |
    r re' r do' r si |
    r do'32( si do' re') mi'16( do') mi'-. mi'-. mi'( do') do''-. do''-. |
    r8 re'32( dod' re' mi') fad'16( re') fad'-. fad'-. fad'( re') la'-. la'-. |
    r8 si32( la si do'!) re'16( si) si'( re' la' re' sol' re') |
    r8 sol'32( la' si' sol') fad'16( re') fad'-. fad'-. fad'( re') fad'-. fad'-. |
    r8 sol'32( la' sib' sol') fad'16( re') fad'-. fad'-. fad'( re') fad'-. fad'-. |
    r8 sol'32( la' sib' sol') fad'16( re') fad'-. fad'-. fad'( re') fa'-. fa'-. |
    r16( sol' mi' sol' mi' sol' mi' sol' mi' sol' mi' sol') |
    r16 fad'( red' fad' red' fad' red' fad' re' fa' re' fa') |
    r16 mi'( dod' mi' dod' mi' dod' mi' do' mib' do' mib') |
    r16 re'( si re' si re' si re' sib re' sib re') |
    <dod' mi'!>4. q8-.( q-. q-.) |
    q2 r16 sol'( la' sol') |
    r fad'( sol' fad') r fa'( sol' fa') r mi'( fad' mi') |
    r red'( mi' red') r re'( mi' re') r dod'( re' dod') |
    do'!4( sib8) sib sib sib |
    mib'2. |
    lab4-.\mf sol-. r |
    si!2(\p do'4) |
    si16( re' si re' si re' si re' do' mib' do' mib') |
    si( re' si re' si re' si re' do' mib' do' mib') |
    si( re' si re' si re' si re' do' mib' do' mib') |
    si( re' si re' do' mib' do' mib' do' mib' do' mib') |
    <do' re'>4 r\fermata r |
    <si re'>4 r\fermata r |
    do'8( sol do' sol re' sol) |
    do'( sol si sol sib sol) |
    do'( la do' la do' la) |
    do'( sol do' sol do' sol) |
    do'( la do' la do' la) |
    do'( sol' fa' mi' re' do') |
    si4 re'4.( do'8) |
    r8 re' r do' r si |
    r do'32( si do' re') mi'16( do') mi'-. mi'-. mi'( do') mi'-. mi'-. |
    r8 fa'32( mi' fa' sol') lab'16( do') do'-. do'-. do' do' dod' dod' |
    r8 re'32( dod' re' mi') fad'16( re') fad'-. fad'-. fad'( re') la'!-. la'-. |
    r8 sol'32( fad' sol' la') sib'16( re') re'-. re'-. re' re' red' red' |
    r8 mi'32( red' mi' fad') sold'16( mi') sold'-. sold'-. sold'( mi') si'!-. si'-. |
    r8 la'32( si' do'' la') sold'16( mi') sold'-. sold'-. sold'( mi') sold'-. sold'-. |
    r16 do''( la' do'' la' do'' la' do'' la' do'' la' do'') |
    r16 si'( sold' si' sold' si' sold' si' sol' sib' sol' sib') |
    r16 la'( fad' la' fad' la' fad' la' fa' lab' fa' lab') |
    r16 sol'( mi' sol' mi' sol' mi' sol' mib' sol' mib' sol') |
    <la'! do''>4. <fad' la'>8-.( q-. q-.) |
    q2 r16 do''( re'' do'') |
    r si'( do'' si') r sib'( do'' sib') r la'( si' la') |
    r sold'( la' sold') r sol'( la' sol') r fad'( sol' fad') |
    fa'!4( mib'8) mib' mib' mib' |
    fa'4-.\mf reb'-. r |
    do'-.\mf si'-. r |
    R2. | \allowPageTurn
    do'2(\p si4) |
    do' r r |
    r r r16 do''( la' fad') |
    r fa''!( re'' si') r sib'( sol' mi') r red'( fad' la') |
    r sold( si! re'!) r dod'( mi' sol'!) r fad'( la' do''!) |
    r( si' re'' fa''!) mib''8 mib'[ mib' mib'] |
    fa'4-.\mf reb'-. r |
    do'-.\mf si'-. r |
    R2. | \allowPageTurn
    do'2(\fp si4) |
    do'2.\cresc |
    do'4(\fp re'2)~ |
    re'4*1/2( s8\cresc do'4 mib') |
    mi'!2(\fp fa'4) |
  }
>>
%%
<<
  \tag #'violino1 {
    do''8\p do' do' do' do' do' do' do' |
    r re' re' re' re' re' re' re' |
    r mi' mi' mi' mi' mi' mi' mi' |
    r mi'( fa') fa'-. fa' fa' fa' fa' |
    r fad' fad' fad' fad' fad' fad' fad' |
    r fad'( sol') sol'-. sol' sol' sol' sol' |
    la'' fa''! do'' la'' sol'' mi'' do'' sol'' |
    fa'' do'' la' fa''  mi'' do'' sol' mi'' |
    re'' re'' re'' re'' sol''( fa'') mi''-. re''-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''8( mi'') la''\cresc sol'' fa'' mi'' re'' do'' |
    si'\f re'' si' sol' mi'' sol'' mi'' do'' |
    si' re'' si' sol' mi'' sol'' mi'' do'' |
    <re' si' sol''>4 r r2 |
    do''2.(\p mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2.( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2 sol'' |
    fad''4.(\fp sol''8) fad''4.(\fp sol''8) |
    fad''8-. sol''-. fad''-.\cresc sol''-. fad''-. sol''-. fad''-. sol''-. |
    fad''2(\f do''')\p |
    si''4.( do'''16 re''' fa''!2) |
    mi''4.( fa''16 sol'' sib'2) |
    la'4-. la'-. r fa' |
    r mi' r re' |
    do' r <la' sol''>2\f( |
    fad''4) re''' sol4.(\p la16 si |
    do'4) r <la' sol''>2(\f |
    fad''4) re''' sol4.(\p la16 si |
    do'4) r la4.(\fp si16 dod' |
    re'4) r sol4.(\fp la16 si |
    do'!8) sol'-.[\p do''-. mi''-.] sol''( fa'') re''-. si'-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''-. sol'-. do''-. mi''-. sol''( fa'') re''-. si'-. |
    do''( mi'') la''\cresc sol'' fa'' mi'' re'' do'' |
    si'\f re'' si' sol' mi'' sol'' mi'' do'' |
    si' re'' si' sol' mi'' sol'' mi'' do'' |
    <re' si' sol''>4 r r2 |
    do''2.(\p mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2.( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2 sol'' |
    fad''4.(\fp sol''8) fad''4.(\fp sol''8) |
    fad''8-. sol''-. fad''-.\cresc sol''-. fad''-. sol''-. fad''-. sol''-. |
    fad''2(\f do''')\p |
    r4 si'( do'' fad'') |
    sol''4.( fad''16 mi'' re''4) r |
    r si'( do'' fad'') |
    sol''4.( fad''16 mi'' re''4) r |
    r si'( do'' fad'') |
    sol''4.( fad''16 mi'' re''2) |
    r4 do'( si) si( |
    re') re'( do') do'( |
    mi') mi'( re') re'( |
    fa'!) fa'( mi') mi'( |
    sol') sol'( fa') fa''~ |
    fa''8( sol'' la'' sol'' fa'' mi'' fa'' re'') |
    do''2.( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2.( mi''4 |
    re'' si' sol' si') |
    do''2.( mi''4 |
    re'' fa'' si' re'') |
    do''2 sol'' |
    fad''4.\fp( sol''8) fad''4.(\fp sol''8) |
    fad''8-. sol''-. fad''-.\cresc sol''-. fad''-. sol''-. fad''-. sol''-. |
    fad''2(\f do''')\p |
    si''4.( do'''16 re''' fa''!2) |
    mi''4.( fa''16 sol'' sib'2) |
    la'4-. la'-. r fa' |
    r mi' r re' |
  }
  \tag #'violino2 {
    mi'8\p do' do' do' do' do' do' do' |
    r si si si si si si si |
    r do' do' do' do' do' do' do' |
    r dod'( re') re'-. re' re' re' re' |
    r red' red' red' red' red' red' red' |
    r red'( mi') mi'-. mi' mi' mi' mi' |
    fa'! la' do'' fa' mi' sol' do'' mi' |
    la do' fa' la sol do' mi' sol |
    fa' mi' re' do' si( re') do'-. si-. |
    do'4 r r <fa' sol> |
    <sol mi'> r r <fa' sol> |
    <sol mi'> r r <fa' sol> |
    sol'4 mi'\cresc do'' la' |
    <sol' sol>4\f r <sol mi' do''> r |
    <sol re' si'>4 r <sol mi' do''> r |
    <sol re' si'>4 r r2 |
    mi'8(\p sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' lab' fa' lab' fa' lab' fa' lab') |
    sol'2 do'' |
    do''\fp do''\fp |
    do''8 do''4*1/2 s8\cresc do''4 do'' do''8 |
    do''2\f~ do''\p |
    si'4.( do''16 re'' fa'2) |
    mi'4.( fa'16 sol' sib2) |
    la4-. fa'-. r re' |
    r do' r si! |
    do' r <la' mi''>2\f( |
    re''4) r sol4.(\p la16 si |
    do'4) r <la' mi''>2( |
    re''4) r sol4.(\p la16 si |
    do'4) r la4.(\fp si16 dod' |
    re'4) r sol4.(\fp la16 si |
    do'!4) r r <sol fa'>\p |
    <mi' sol> r r <sol fa'> |
    <mi' sol> r r <sol fa'> |
    <mi' sol> r r <sol fa'> |
    sol'4 mi'\cresc do'' la' |
    <sol' sol>4\f r <sol mi' do''> r |
    <sol re' si'>4 r <sol mi' do''> r |
    <sol re' si'> r r2 |
    mi'8(\p sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' lab' fa' lab' fa' lab' fa' lab') |
    sol'2 do'' |
    do''\fp do''\fp |
    do''8 do''4*1/2 s8\cresc do''4 do'' do''8 |
    do''2(\f fad')\p |
    \ru#6 { sol8( si re' si la do' re' do') | }
    la2( sol4) sol( |
    si) si( la) la( |
    do') do'( si) si( |
    re') re'( do') dod'( |
    mi') mi'( re'2) |
    R1 |
    mi'8( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' sol' |
    fa' lab' fa' lab' fa' lab' fa' lab') |
    sol'2 do'' |
    do''\fp do''\fp |
    do''8 do''4*1/2 s8\cresc do''4 do'' do''8 |
    do''2~\f do''\p |
    si'4.( do''16 re'' fa'2) |
    mi'4.( fa'16 sol' sib2) |
    la4-. fa'-. r re' |
    r do' r si! |
  }
>>
do'8-. sol'-. do''-. si'-. la' fa' re'' do'' |
si' sol' mi'' re'' do'' la' fa'' mi'' |
re'' si' sol'' fa'' mi'' do'' la'' sol'' |
fad'' re'' sol''\cresc fa'' mi'' do'' fa'' mi'' |
red'' si' mi'' re'' dod'' la' re'' do'' |
<<
  \tag #'violino1 {
    si'\f do'' re'' mi'' fa'' sol'' la'' si'' |
    do'''\fp do''' do''' do''' la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' si' si' si' si' |
    do''
  }
  \tag #'violino2 {
    si'\f la' si' do'' re'' mi'' fa'' fa'' |
    mi''\fp mi'' mi'' mi'' do'' do'' do'' do'' |
    do'' do'' do'' do'' fa' fa' fa' fa' |
    mi'
  }
>> sol' do'' si' la' fa' re'' do'' |
si' sol' mi'' re'' do'' la' fa'' mi'' |
re'' si' sol'' fa'' mi'' do'' la'' sol'' |
fad'' re'' sol''\cresc fa'' mi'' do'' fa'' mi'' |
red'' si' mi'' re'' dod'' la' re'' do'' |
<<
  \tag #'violino1 {
    si'\f do'' re'' mi'' fa'' sol'' la'' si'' |
    do'''\fp do''' do''' do''' la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' si' si' si' si' |
    do''16
  }
  \tag #'violino2 {
    si'8\f la' si' do'' re'' mi'' fa'' fa'' |
    mi''\fp mi'' mi'' mi'' do'' do'' do'' do'' |
    do'' do'' do'' do'' fa' fa' fa' fa' |
    mi'16
  }
>> do'16 do' do' do'4:16 do'2:16 |
<<
  \tag #'violino1 {
    re'2:16 re':16 |
    re':16 re':16 |
    do':16 mi':16 |
    re':16 re':16 |
    fa':16 fa':16 |
    mi':16 mi':16 |
    do''':16\cresc do''':16 |
    do''':16 do''':16 |
    do''':16\f si'':16 |
    do'''4..( sol''16) sol''4..( mi''16) |
  }
  \tag #'violino2 {
    do'2:16 do':16 |
    si:16 si:16 |
    do':16 do':16 |
    do':16 do':16 |
    <si re'>:16 q:16 |
    do':16 do':16 |
    mib'':16\cresc mib'':16 |
    mi''!:16 mi'':16 |
    re'':16\f re'':16 |
    mi''4..( sol''16) sol''4..( mi''16) |
  }
>>
mi''8( do'') do''( sol') sol'( mi') mi'( do') |
do'4 do'8. do'16 do'4 do' |
do' r <<
  \tag #'violino1 { <sol' mi'' do'''> }
  \tag #'violino2 { <sol' do'' mi''> }
>> r4 |
do'2 r |
