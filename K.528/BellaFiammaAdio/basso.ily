\clef "bass" R1 |
mi4(\p do8 la,) sold,2~ |
sold,8 r r4 r2 |
r4 mi( do8[ sold,]) la,[-. la,]-. |
la,2 lad,~ |
lad,4 r r si,( |
mi8) r fad r si, r do r |
la,! r si, r mi4 r |
R1 |
la,1~ |
la,~ |
la,2~ la,8 r r4 |
r2 re8 r mi r |
la, r sib, r sol, r la, r |
re4 r re8 re re r |
r2 do8 do do r |
r2 fa8 fa fa fa |
fa4 r r2 |
fad2(\fp sol8) r sol r |
fa!1( |
mib8) r do r la,2~ |
la,1~ |
la,2( sib,8) r do r |
fad r sol r mib r fa r |
sib,1 |
sib8 r do' r fa\f r si,! r |
si,1\p~ |
si,~ |
si,2 r |
do8 r re r sol r lab r |
fa r sol r do4 r |
r2 do'8 r sib r |
re\f r mib r mi2\fp~ |
mi2( do8) r r4 |
r2 r8 fa fa r |
r re re r r do do r |
r lab lab r r4 sol |
