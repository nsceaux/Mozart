\clef "soprano/treble" R1 |
r2 fa''4 re''8. do''16 |
si'8 si' r mi'' do'' do'' r4R1 |
r8 red'' dod'' si' mi'' mi'' r sol'' |
mi'' mi'' red'' mi'' mi'' si' r4 |
R1 |
r2 si'4 sol'8 la' |
si' si' r si'16 si' mi''4 re''8 mi'' |
dod''8 dod'' r dod''16 dod'' dod''4 re''8 mi'' |
mi'' la' r la' la' la' la' sib' |
sol'4 sol'8 sol'16 sol' dod''8 dod'' r dod''16 re'' |
mi''4 mi''8 fa'' re''4 r |
R1 |
r4 re''8 la' r2 |
fa''4 re''8. do''16 sib'4 r |
sib'4 do''8. sol'16 la'8 la' r4 |
la'8 la'16 la' la'8 sib' do'' do'' r do''16 do'' |
mib''4 mib''8. re''16 sib'8 sib' r4 |
r re''8 re'' fa''4 fa''8. mib''16 |
do''8 do'' r mib'' solb''8.([ la'16]) la'8 r16 la' |
do''8 do'' do'' do''16 re'' mib''8 mib'' r mib'' |
mib'' mi'' fa'' do'' re'' re'' r4 |
R1 |
r4 r8 sib' re''8. re''16 do''8 re'' |
sib'4 r r2 |
r8 sol' sol' lab' lab' fa' r fa' |
fa' fa' fa' fa' si'! si' r8 si' si'16 si' si' do'' re''8 re'' r re'' fa'' mib'' |
do''8 do'' r4 r2 |
r r8 sol' do'' do'' |
r do'' mib''16 mib'' re'' mib'' do''8 do'' r4 |
r2 r4 reb'' |
sib'8 sib'16 sib' sib' sib' do'' reb'' mi'!4 r |
sib'4 do''8. sol'16 lab'8 lab' r4 |
si'!8 si'16 si' r8 sol' mib'' mib'' r16 mib'' mib'' mib'' |
do''8 do'' r do'' do'' sol' r4 |
