\clef "treble"
<<
  \tag #'violino1 {
    si'\p( sol'8 mi') red'( fad')~ fad'16( la'8 do''16) |
    si'-. mi'-. mi''8~ mi''16[ re''!32( do''] si'[ do'' re'' mi'']) fa''!2( |
    mi''8) r r4 mi''( do''8 sold') |
    la'( si') do''8( re''32 do'' re'' si') mi''16(-. mi''-. mi''-. mi''-.) mi''( fa''!) fa''( mi'') |
    red''2 mi''~ |
    mi''4 r r fad'( |
    sol'8) sol''4( fad''8) r red''4( mi''8) |
    r16 do''( mi'' do'') r fad'( la' fad') sol'4 r |
    R1 |
    sol'1~ |
    sol'~ |
    sol'2~ sol'8 r r4 |
    r2 r8 fa''!4( mi''8) |
    r8 dod''4( re''8) r16 sol'( sib' sol') r mi'( sol' mi') |
    fa'4 r r16 fa'-. fa'-. sol'-. sold'( la') r8 |
    r2 r16 mi''-. mi''-. fa''-. fad''( sol'') r8 |
    r2 r16 la'-. la'-. sib'-. si'( do'') do''-. do''-. |
    do''4 r r2 | \allowPageTurn
    mib'2\fp r16 sib'-. sib'-. do''-. dod''( re'') re''-. re''-. |
    re''1 |
    r16 mib''-. mib''-. fa''-. fa''( solb'') solb''-. solb''-. solb'2~ |
    solb' fa'~ |
    fa' r8 re''4( do''8) |
    r8 la''4( sib''8) r16 mib'( sol' mib') r do'( mib' do') |
    re'1 |
    r8 re'''4( do'''8) r8 lab'4\f( sol'8) |
    sol'4.(\p fa'8) fa'2~ |
    fa'1~ |
    fa'2 r | \allowPageTurn
    r8 mib''4( re''8) r si'4( do''8) |
    r16 fa'( lab' fa') r re'( fa' re') mib'4 r |
    r2 r8 mib''4( re''8) |
    r8 << { lab'4( sol'8) } \\ sib4.\f >> reb'2\fp~ |
    reb'( mi'8) r r4 |
    r2 r8 do'( lab') r |
    r fa'( si'!) r r sol'( mib'') r |
    r fad''( do''') r r4 sol' |
  }
  \tag #'violino2 {
    r2 la'4(\p fad'8 red') |
    mi'4~ mi'16.([ fad'32] sold'[ la' si' do'']) si'2~ |
    si'8 r r4 r mi'( |
    do'8[ sold]) la([ si]) do' si'~ si'16( la') do''(-. do''-.) |
    si'2 dod''~ |
    dod''4 r r red'( |
    mi'8) si'4( lad'8) r la'4( sol'8) |
    r16( la' do'' la') r red'( fad' red') mi'4 r |
    R1 |
    dod'1~ |
    dod'~ |
    dod'2~ dod'8 r r4 |
    r2 r8 la'4( sold'8) |
    r8 sol'!4( fa'!8) r16 mi'( sol' mi') r dod'( mi' dod') |
    re'4 r r16 re'-. re'-. mi'-. mi'( fa') r8 |
    r2 r16 sol'-. sol'-. la'-. la'( sib') r8 |
    r2 r16 fa'-. fa'-. sol'-. sold'( la') la'-. la'-. |
    la'4 r r2 | \allowPageTurn
    do'2\fp r16 sol'-. sol'-. la'-. la'( sib') sib'-. sib'-. |
    si'1 |
    r16 do''-. do''-. re''-. re''( mib'') mib''-. mib''-. mib'2~ |
    mib'1~ |
    mib'2 r8 fa'4( mi'8) |
    r mib''4( re''8) r16 do'( mib' do') r la( do' la) |
    sib1 |
    r8 fa''4( mib''8) r mib'(\f re') re'-. |
    re'1\p~ |
    re'~ |
    re'2 r | \allowPageTurn
    r8 sol'4( fad'8) r fa'4( mib'8) |
    r16 reb'( fa' reb') r si!( re' si) do'4 r |
    r2 r8 sol'4( lab'8) |
    r8 fa'4\f( mib'8) sib2-\sug\fp~ |
    sib~ sib8 r r4 |
    r2 r8 lab( do') r |
    r sol( fa') r r mib'( sol') r |
    r do''( fad'') r r4 fa'! |
  }
>>
