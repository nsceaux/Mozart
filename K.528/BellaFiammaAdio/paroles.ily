Bel -- la mia fiam -- ma, ad -- di -- o; non piac -- que al cie -- lo
di ren -- der -- ci fe -- li -- ci. Ec -- co re -- ci -- so.
Pri -- ma d’es -- ser com -- pi -- to,
quel pu -- ris -- si -- mo no -- do,
che strin -- se -- ro fra lor gli a -- ni -- mi no -- stri
 
con il so -- lo vo -- ler.
Vi -- vi! Ce -- di al de -- stin! Ce -- di al do -- ve -- re!
Del -- la giu -- ra -- ta fe -- de
la mia mor -- te t’as -- sol -- ve;
a più de -- gno con -- sor -- te… o pe -- ne! u -- ni -- ta
vi -- vi più lie -- ta e più fe -- li -- ce vi -- ta.
Ri -- cor -- da -- ti di me, ma non mai tur -- bi
d’un in -- fe -- li -- ce spo -- so
la ra -- ra ri -- mem -- bran -- za il tuo ri -- po -- so!
Re -- gi -- na, io va -- do ad ub -- bi -- dir -- ti. Ah, tut -- to
fi -- ni -- sca il mio fu -- ror col mo -- rir mi -- o.
Ce -- re -- re, Al -- fe -- o, di -- let -- ta spo -- sa, ad -- di -- o!
