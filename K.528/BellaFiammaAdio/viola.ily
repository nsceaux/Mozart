\clef "alto" r2 do'4(\p la8 fad) |
sol( sold la4) re'2~ |
re'8 r r4 r2 |
mi'4( do'8 sold) la8( re') do' la'16( sol'!) |
fad'2 sol'~ |
sol'4 r r si~ |
si8 r fad' r si r do' r |
la! r si r si4 r |
R1 |
mi'1~ |
mi'~ |
mi'2~ mi'8 r r4 |
r2 re'8 r mi' r |
la r sib r sol r la r |
la4 r re'8 re' re' r |
r2 do'8 do' do' r |
r2 fa'8 fa' fa' fa' |
fa'4 r r2 |
la2\fp( sol8) r sol' r |
sol'1~ |
sol'8 r mib' r do'2~ |
do'1~ |
do'2( sib8) r do' r |
r do''4( sib'8) sol r fa r |
fa1 |
sib'8 r do'' r r do(\f fa) fa-. |
fa2\p lab~ |
lab sol~ |
sol r |
do'8 r re' r sol r lab r |
fa r sol r sol4 r |
r2 do'8 r sib r |
re\f r mib r sol2\fp~ |
sol~ sol8 r r4 |
r2 r8 fa fa r |
r si!( sol') r r do' do' r |
r mib' mib' r r4 si |
